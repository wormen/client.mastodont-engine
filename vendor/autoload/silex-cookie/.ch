find . -type f -print0 | xargs -0 chmod 666
find . -type d -print0 | xargs -0 chmod 777
find . -type f -name '.ch' -exec chmod 777 {} \;
