<?php

namespace Autoload\SilexService\Cookie;

use Silex\Application;
use Silex\ServiceProviderInterface;
use Symfony\Component\HttpFoundation\Cookie as SymfonyCookie;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class CookieServiceProvider implements ServiceProviderInterface {

    public function register(Application $app) {

        $this->app = $app;
        $app['cookie'] = $this;
        $this->expire = time() + 60 * 60 * 1;
    }

    public function boot(Application $app) {
        
    }

    public function get($key, $update = false) {

        $value = $this->app['request']->cookies->get($key);

        if (true == $update)
            $this->set($key, $value);

        return $value;
    }

    public function set($key, $value = null, $expire = null, $path = '/', $domain = null, $secure = false, $httpOnly = true) {

        if (empty($expire))
            $expire = $this->expire;

        if ((isset($_SERVER['HTTPS']) && 'on' == $_SERVER['HTTPS']) || $_SERVER['SERVER_PORT'] == 443)
            $secure = true;

        $response = new SymfonyResponse();

        $cookie = new SymfonyCookie($key, $value, $expire, $path, $domain, $secure, $httpOnly);

        $response->headers->setCookie($cookie);

        $response->sendHeaders();
    }

    public function delete($key) {

        $response = new SymfonyResponse();

        $response->headers->clearCookie($key);

        $response->sendHeaders();
    }

}
