<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 03.02.2016
 * Time: 9:26
 */

namespace Cli;

class Service
{
    
    function __construct(){
    }

    /**
     * Системные сервисы
     * @return array
     */
    public function System(){
        $list = [
            [
                'name'=>'backup',
                'sysName'=>'Backup',
                'config' => [

                ]
            ],
            [
                'name'=>'cron',
                'sysName'=>'Cron',
                'config' => [

                ]
            ],
            [
                'name'=>'queues',
                'sysName'=>'Queues',
                'config' => [

                ]
            ],
            [
                'name'=>'email',
                'sysName'=>'Email',
                'config' => [

                ]
            ],
            [
                'name'=>'user-state',  // проверка состояний пользователей
                'sysName'=>'User-State',
                'config' => [

                ]
            ],
            [
                'name'=>'updater', // проверка и установка обновлений
                'sysName'=>'Updater',
                'config' => [
                    'hourInterval'=>1,
                    'nextCheck'=>0
                ]
            ],
        ];

        foreach ($list as $k => $v) {
            $v['pid'] = 0;
            $v['type'] = 'demon';
            $v['p'] = 'system';
            $v['state'] = 'run';
            $v['descr'] = '';
            $v['config'] = array_merge([], $v['config']);
            $list[$k] = $v;
        }

        return$list;
    }

    /**
     * Пользовательские сервисы
     * @return array
     */
    public function Custom(){
        $files = [];
        foreach(glob(ServiceDir.'/*.php') as $file){
            $f = str_replace([ServiceDir.'/', '.php'], '', $file);

            foreach($this->System() as $s)
                if($s['name'] == $f) continue 2;

            $s = [
                'pid'=>0,
                'name'=>$f,
                'sysName'=>ucfirst($f),
                'type'=>'demon',
                'p'=>'user',
                'state'=>'stop',
                'descr'=>'',
                'config'=>[]
            ];

            $files[] = $s;
        }

        return$files;
    }

    public function fullList(){
        $list = array_merge($this->System(), $this->Custom());

        $collection = \Core\DB::get('SDB')->list;
        $slist = $collection->find([], [
            '_id'=>true,
            'name'=>true,
            'sysName'=>true,
            'p'=>true,
            'state'=>true,
            'descr'=>true,
            'type'=>true,
        ])->toArray();

        if(!$slist || (is_array($slist) && count($slist)==0) ){
            foreach($list as $l)
                $collection->insert($l);

        }else{
            foreach($list as $l){
                $isInsert = true;

                foreach($slist as $s){
                    if($l['name'] == $s['name']){
                        $isInsert = false;
                        break;
                    }
                }

                if($isInsert)
                    $collection->insert($l);
            }

            $list = $slist;
        }

        return$list;
    }

    public function getService($name){
        $collection = \Core\DB::get('SDB')->list;
        return$collection->findOne(['name'=>$name]);
    }

    public function update($id, $data){
        $collection = \Core\DB::get('SDB')->list;
        if(isset($data['_id'])) unset($data['_id']);
        return$collection->update(['_id'=>$id], $data);
    }

    public function delete($id){
        $state = false;

        try{
            $collection = \Core\DB::get('SDB')->list;
            $s = $collection->findOne(['_id'=>$id]);

            if($s['p'] != 'system'){
                $file = ServiceDir.'/'.$s['name'].'.php';
                if(file_exists($file))
                    unlink($file);

                $state = (boolean)$collection->remove(['_id'=>$id]);
                \Core\DB::get('SDB')->dropCollection($s['name']);
            }

            return$state;
        }catch(\Exception $e){
            return$state;
        }

    }


    /**
     *  Запускаем указанный сервис в CLI MODE
     * @param $service
     */
    public function RunAsDeamon($service){
        $php = 'php';//путь к php
        $script = $service;

        try{

            if (stripos(PHP_OS,'win')===0){ // это у нас windows систем
                $PATH = explode(';', $_SERVER['PATH']);
                foreach($PATH as $P)
                    if(file_exists($P.'\\php.exe'))
                        $php = $P.'\\php.exe';

                pclose(popen("start /b $php -f $script run", 'w'));
            }

            // для *.nix систем
            else
                exec("$php -f $script > /dev/null &");

        }catch(\Exception $e){
            $GLOBALS['Log']->set($e, 'cli');
        }
    }

    /**
     *  Запускаем указанный сервис как SERVER MODE
     * @param $service
     */
    public function RunAsServer($service){

    }

    /**
     *  проверка маркера поведения сервиса ( stop | restart )
     * @param $service
     */
    public function CheckState($service){

    }

    /**
     * Ищем запущенный процесс по PID
     * @param $pid
     * @return bool
     */
    public function isRuning($pid){
        $launch = false;

        if(stripos(PHP_OS,'win')===0){
            $cmd = "tasklist /nh /fi \"PID eq $pid\"";
            $output = array_filter(explode(" ", trim(shell_exec($cmd)) ));
            array_pop($output);
            foreach($output as $t)
                if((int)$t == $pid){
                    $launch=true;
                    break;
                }

        }else{
            if(shell_exec("ps -e | grep " . $pid . " | wc -l") > 0) $launch = false;
            else $launch=true;
        }

        return$launch;
    }

}
