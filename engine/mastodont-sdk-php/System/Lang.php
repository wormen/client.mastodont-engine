<?php

/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 28.01.2016
 * Time: 12:25
 */

namespace Mastodont\System;

require_once __DIR__ .'/../ApiClient/Sender.php';
use \Mastodont\ApiClient\Sender;

class Lang
{
    protected
        $Sender,
        $module = 'system',
        $localeFile,
        $locale = 'rus';

    function __construct(){
//        $this->config = $this->Setting();
        $this->Sender = new Sender('v1', null, false);
    }

    /**
     * Отдаем локализацию в интерфейс
     * @param string $locale
     * @return array
     */
    public function getData($locale = 'rus'){
        $this->locale = $locale;
        return $this->getFileData();
    }

    /**
     * Получаем локализацию из файла, загружаем при необходимости, или по истечению времени кэша
     * @return array
     */
    protected function getFileData(){
        $file = $this->_localeFile();

        $data = [];
        if(!file_exists($file))
            $data = $this->loadData();
        
        else{

            $time = filemtime($file);
            $lifeTime = time() - $time;
            $next = $lifeTime < \Core\Time::Minute(60);

            if(!$next)
                $data = $this->loadData();

            else
                $data = json_decode(file_get_contents($file), true);

        }

        return$data;
    }

    /**
     * Получаем путь к файлу локализации
     * @param $locale
     * @return string
     */
    protected function _localeFile(){
        $this->localeFile = implode(DIRECTORY_SEPARATOR, [CacheDir, $this->locale.'.locale']);
        return$this->localeFile;
    }

    /**
     * Загружаем данные с сервера
     * @param $locale
     * @return array
     */
    protected function loadData(){
        $data = $this->Sender->get($this->module, 'getLang', null, ['locale'=>$this->locale]);
        $this->saveToFile($data);
        return \Core\Lib::objectToArray($data);
    }

    /**
     * Сохраняем данные в файл
     * @param $data
     * @param $locale
     */
    protected function saveToFile($data){
        $file = $this->_localeFile();
        $data = json_encode($this->checkData($data));
        file_put_contents($file, $data);
    }

    /**
     * Замена нежелательных строк
     * @param $data
     * @return mixed|string
     */
    protected function checkData($data){
        $data = json_encode($data);

        $data = str_replace(['_empty_'], [''], $data);

        $data = json_decode($data, true);
        return$data;
    }
}