<?php

/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 28.01.2016
 * Time: 12:25
 */

namespace Mastodont\System;

require_once __DIR__ .'/../ApiClient/Sender.php';
use \Mastodont\ApiClient\Sender;

class Menu
{
    protected
        $Sender,
        $module = 'system';

    function __construct(){
//        $this->config = $this->Setting();
        $this->Sender = new Sender('v1', null, false);
    }

    public function fullList(){
//        $list = $this->Sender->get($this->module, 'getSysMenu');
//        return$this->checkActive($list);

        return $this->getFileData();
    }

    /**
     * Получаем локализацию из файла, загружаем при необходимости, или по истечению времени кэша
     * @return array
     */
    protected function getFileData(){
        $file = $this->menuFile();

        $data = [];
        if(!file_exists($file))
            $data = $this->loadData();

        else{

            $time = filemtime($file);
            $lifeTime = time() - $time;
            $next = $lifeTime < \Core\Time::Minute(10);

            if(!$next)
                $data = $this->loadData();

            else
                $data = json_decode(file_get_contents($file), true);

        }

        return$data;
    }

    /**
     * Путь к файлу кэша
     * @return string
     */
    protected function menuFile(){
        return implode(DIRECTORY_SEPARATOR, [CacheDir, 'menu.system']);
    }

    /**
     * Загружаем данные с сервера
     * @return array
     */
    protected function loadData(){
        $list = $this->Sender->get($this->module, 'getSysMenu');
        $list = $this->checkActive($list);
        $this->saveToFile($list);
        return$list;
    }

    /**
     * Сохраняем данные в файл
     * @param $data
     */
    protected function saveToFile($data){
        $file = $this->menuFile();
        $data = json_encode($data);
        file_put_contents($file, $data);
    }

    /**
     * Удаление неактивных пунктов
     * @param array $list
     * @return array
     */
    protected function checkActive($list = []){
        $arr = [];

        foreach ($list as $k=>$item){
            if($item->enabled){
                if(isset($item->sub))
                    $item->sub->list = $this->checkActive($item->sub->list);

                $arr[] = \Core\Lib::objectToArray($item);
            }
        }

        return$arr;
    }



}