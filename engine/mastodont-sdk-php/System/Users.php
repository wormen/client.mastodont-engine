<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 28.01.2016
 * Time: 17:56
 */

namespace Mastodont\System;


class Users
{
    public function SysMenu($user_id=null){
        if(!$user_id) return false;

        $collection = \Core\DB::get()->users;
        $d=$collection->findOne(['_id'=>$user_id], [ 'sysMenu'=>true ]);
        return$d['sysMenu'];
    }
}