<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 20.01.2016
 * Time: 9:40
 */

namespace Mastodont\System;

use Rhumsaa\Uuid\Console\Exception;

class Files
{
    protected
        $Fm;

    function __construct(){
        $this->Fm = new \Core\Fm();
    }

    // ---------------------- временные файлы в БД ----------------------

    public function getID($id){
        $collection = \Core\DB::get('FDB')->list;
    }

    /**
     * Получаем документы по названию файла
     * @param $name - название файла
     * @param array $fields
     * @return mixed
     */
    public function getName($name, $fields = []){
        $collection = \Core\DB::get('FDB')->list;
        $fields['domain_id'] = false;

        return$collection->find(['name'=>$name, 'domain_id'=>$GLOBALS['domain']['_id']], $fields)->toArray();
    }

    /**
     * Получаем документы по названию временного каталога
     * @param $dirName - название каталога
     * @param array $fields - поля, которые необходимо вывести/исключить
     * @return mixed
     */
    public function getDir($dirName, $fields = []){
        $collection = \Core\DB::get('FDB')->list;
        $fields['domain_id'] = false;

        return$collection->find(
            [
                '$or'=> [
                    ['dir' => $dirName],
                    ['tmp' => $dirName]
                ],
                'domain_id'=>$GLOBALS['domain']['_id']
            ], $fields)->toArray();
    }

    /**
     * Добавляем запись о файле во временную таблицу
     * @param $data - данные о файле
     * @return mixed
     */
    public function Add($data){
        $collection = \Core\DB::get('FDB')->list;

        $data['primary'] = false;
        $data['modify'] = $data['created'] = time();
        $data['domain_id'] = $GLOBALS['domain']['_id'];
        $collection->insert($data);

        if(isset($data['domain_id']))
            unset($data['domain_id']);

        return$data;
    }

    /**
     * Обновляем запись о файле п=во временной таблице
     * @param $params - параметры поиска
     * @param $data - данные о файле
     * @return mixed
     */
    public function update($params, $data){
        $collection = \Core\DB::get('FDB')->list;
        $data['modify'] = time();
        $data['domain_id'] = $GLOBALS['domain']['_id'];

        $data['primary'] = array_key_exists('primary', $data) ? $data['primary'] : false;

        return $collection->update($params, $data);
    }

    /**
     * Удаляем запись о файле из временной таблицы, включая все файлы указанные в документе
     * @param $id
     * @return bool
     */
    public function delete($id, $delFiles = true){
        $collection = \Core\DB::get('FDB')->list;
        $r = false;
        try{
            $L = $collection->findOne(['_id'=>$id]);

            if($delFiles){
                if(stripos($L['type'], 'image') === 0)
                    $r = $this->deleteImages($L);
                else
                    $r = $this->deleteOtherFiles($L);
            }

//            if($r)
                return (boolean)$collection->remove(['_id'=>$id]);

        }catch(Exception $e){
            return false;
        }

    }

    /**
     * Удаляем документы по названию файлы, относитнльно домена, включая файлы которые указаны в документе
     * @param $name - название файла
     * @return bool
     */
    public function deleteName($name){
        $collection = \Core\DB::get('FDB')->list;

        $r = false;
        try{
            $L = $collection->findOne([
                'name'=>$name,
                'domain_id'=>$GLOBALS['domain']['_id']
            ]);

            if(stripos($L['type'], 'image') === 0)
                $r = $this->deleteImages($L);

            //todo удаление прочих типов файлов

            if($r)
                return (boolean)$collection->remove(['_id'=>$L['_id']]);

        }catch(Exception $e){
            return false;
        }

    }

    public function setPrimary($file_id, $data){

        $this->update([
            'dir' => $data['dir'],
            'module' => $data['module']
        ], ['primary' => false]);

        $this->update(['_id' => $file_id], ['primary' => true]);

        $this->ModuleUpdate($data['module'], ['doc_id'=>$data['doc_id']], ['primary'=>false]);
        $this->ModuleUpdate($data['module'], ['_id'=>$file_id], ['primary'=>true]);

        return true;
    }

//----------------------------------------------------------------------------------------------------------------------

    protected function deleteImages($data){
        $r = false;

        if(!isset($data['originalUrl'])) return$r;

        $file = RootDir . $data['originalUrl'];
        if(file_exists($file))
            $r = $this->Fm->delete($file);

        if(isset($data['thumbs'])){
            foreach($data['thumbs'] as $k=>$f) {
                $r = false;
                $file = RootDir . $f;

                if(file_exists($file))
                    $r = $this->Fm->delete($file);
            }
        }

        return$r;
    }

    protected function deleteOtherFiles($data){
        return true;
    }

//----------------------------------------------------------------------------------------------------------------------


    // ---------------------- файлы от модулей в БД ----------------------

    /**
     * Добавляем запись о файле в таблицу модуля
     * @param $module - название модуля
     * @param $data - данные о файле
     * @return mixed
     */
    public function ModuleAdd($module, $data){
        $collection = \Core\DB::get('FDB')->{$module};

        $data['modify'] = $data['created'] = time();
        $data['domain_id'] = $GLOBALS['domain']['_id'];
        $data['primary'] = false;

        $collection->insert($data);

        if(isset($data['domain_id']))
            unset($data['domain_id']);

        return$data;
    }

    /**
     * Обновление информации о файле модуля
     * @param $module - название модуля
     * @param $params - параметры поиска
     * @param $data - данные о файле
     * @return mixed
     */
    public function ModuleUpdate($module, $params, $data){
        $collection = \Core\DB::get('FDB')->{$module};
        $data['modify'] = time();
        $data['domain_id'] = $GLOBALS['domain']['_id'];
        $data['primary'] = array_key_exists('primary', $data) ? $data['primary'] : false;

        return $collection->update($params, $data);
    }

    /**
     * @param $module - название модуля
     * @param $name - название файла
     * @param array $fields - поля которые нужно вывести/иключить
     * @return mixed
     */
    public function ModuleGetName($module, $name, $fields = []){
        $collection = \Core\DB::get('FDB')->{$module};
        $fields['domain_id'] = false;

        return$collection->find(['name'=>$name, 'domain_id'=>$GLOBALS['domain']['_id']], $fields)->toArray();
    }

    /**
     * @param $module - название модуля
     * @param $id - ID документа
     * @param array $fields - поля которые нужно вывести/иключить
     * @return mixed
     */
    public function ModuleGetDocID($module, $id, $fields = []){
        $collection = \Core\DB::get('FDB')->{$module};

        $fields['domain_id'] = false;

        return$collection->find(['doc_id'=>$id], $fields)->toArray();
    }

    /**
     * Удаление записи о файле в таблице модуля
     * @param $module - название модуля
     * @param $id - документ
     * @return bool
     */
    public function moduleDelete($module, $id){
        $collection = \Core\DB::get('FDB')->{$module};
        $r = false;
        try{
            $L = $collection->findOne(['_id' => $id]);

            if(stripos($L['type'], 'image') === 0)
                $r = $this->deleteImages($L);
            else
                $r = $this->deleteOtherFiles($L);

            if($r)
                return (boolean)$collection->remove(['_id'=>$id]);

        }catch(Exception $e){
            return false;
        }

    }

    /**
     * Удаление записей о файле в модуле, по названию файла
     * @param $module - название модуля
     * @param $name - название файла
     * @return mixed
     */
    public function moduleDeleteName($module, $name){
        $collection = \Core\DB::get('FDB')->{$module};
        $r = false;
        try{
            $L = $collection->findOne([
                'name'=>$name,
                'domain_id'=>$GLOBALS['domain']['_id']
            ]);

            if(stripos($L['type'], 'image') === 0)
                $r = $this->deleteImages($L);

            //todo удаление прочих типов файлов

            if($r)
                return (boolean)$collection->remove(['_id'=>$L['_id']]);

        }catch(Exception $e){
            return false;
        }

    }
}