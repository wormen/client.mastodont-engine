<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 14.02.2016
 * Time: 1:50
 */

namespace Mastodont\Api\v1;


class Letters
{

    protected
        $module = 'letters';
    
    function __construct(){
    }

    public function miniList(){
        $collection = \Core\DB::get()->letters;
        $list = $collection->find([], [
            '_id'=>true,
            'name'=>true,
            'sysName'=>'true',
            'enabled'=>true,
        ])->toArray();
        return$list;
    }

    public function edit($id){
        $collection = \Core\DB::get()->letters;
        $d=$collection->findOne(['_id'=>$id], ['_id'=>false]);
        return$d;
    }

    public function save($data){
        $collection = \Core\DB::get()->letters;
        $r = (boolean)$collection->insert($data);
        return$r;
    }

    public function update($params, $data){
        $collection = \Core\DB::get()->letters;
        return$collection->update($params, $data);
    }

    public function delete($id){
        $collection = \Core\DB::get()->letters;
        return (boolean)$collection->remove(['_id'=>$id]);
    }

    public function getName($name){
        $collection = \Core\DB::get()->letters;
        $d=$collection->findOne(['sysName'=>$name], ['_id'=>false]);
        return ($d ? $d : false);
    }

    /**
     * @param array $data - ассоциативный массив
     * @param $text - текст шаблона
     * @return string
     */
    public function ReplaceVars($data, $ltTPL){
        $C = $GLOBALS['Setting']->get();

        $vars = [
            'email'=>($C['general']['emailAdmin'] ? $C['general']['emailAdmin'] : "no-reply@{$_SERVER['HTTP_HOST']}"),
//            'userFullName'=>'ФИО пользователя',
            'site'=>$C['site']['name'],
            'siteLink'=>$_SERVER['HTTP_HOST'],
//            'paymentSumm'=>'Сумма платежа',
//            'adsID'=>'ID оъявления',
//            'adsLink'=>'Ссылка на объявление',
        ];

        if(isset($data['login']))
            $vars['userLogin'] = $data['login'];

        if(isset($data['email']))
            $vars['userEmail'] = $data['email'];

        if(isset($data['password']))
            $vars['password'] = $data['password'];

        if(isset($data['uuid']))
            $vars['activateProfileLink'] = "{$_SERVER['HTTP_HOST']}/{$C['modules']['account']['linkModule']}/activate?code={$data['uuid']}";

        $arr1 = [];
        $arr2 = [];
        foreach($ltTPL['vars'] as $k=>$v){
            array_push($arr1, '{'.$v.'}');
            array_push($arr2, $vars[$v]);
        }

        $text = str_replace($arr1, $arr2, $ltTPL['description']);

        $ltTPL['vars'] = $vars;
        return [
            'tpl'=>$ltTPL,
            'text'=>$text
        ];
    }
}