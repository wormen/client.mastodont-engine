<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 25.01.2016
 * Time: 16:48
 */

namespace Mastodont\Api\v1;


class StaticPage
{
    protected
        $Fm,
        $module = 'static';

    function __construct(){
        $this->Fm = new \Core\Fm();
    }

    public function loadList(){
        $collection = \Core\DB::get()->static;
        $list = $collection->find(
            [
                'domain_id'=>$GLOBALS['domain']['_id']
            ],
            [
                '_id'=>true,
                'name'=>true,
                'enabled'=>true,
            ]
        )->toArray();

        return$list;
    }

    public function checkUrl($url, $locale='rus'){
        $collection = \Core\DB::get()->static;
        $list = $collection->find(
            [
                'url'=>$url,
                'locale'=>$locale,
                'domain_id'=>$GLOBALS['domain']['_id']
            ], ['_id'=>true])->toArray();

        return count($list)>0;
    }

    public function edit($id){
        $collection = \Core\DB::get()->static;
        $d=$collection->findOne(['_id'=>$id], ['_id'=>false]);

        $dir = implode(DIRECTORY_SEPARATOR, [UploadsDir, 'static', $id]);
        if(!file_exists($dir)) $this->Fm->MkDir($dir, 0755, true);

        return$d;
    }

    public function save($data){
        $collection = \Core\DB::get()->static;

        $tmp = $data['tmp'];
        unset($data['tmp']);

        $data['modify'] = $data['created'] = time();
        $data['domain_id'] = $GLOBALS['domain']['_id'];

        $r = (boolean)$collection->insert($data);

        $collection->update(['_id'=>$data['_id']], [
            'description'=>str_replace($tmp, $data['_id'], $data['description'])
        ]);

        $dir = UploadsDir.DIRECTORY_SEPARATOR.'static';
        $old = $dir.DIRECTORY_SEPARATOR.$tmp;
        $new = $dir.DIRECTORY_SEPARATOR.$data['_id'];

        if(file_exists($old))
            $this->Fm->Rename($old, $new);

        return$r;
    }

    public function update($params, $data){
        $collection = \Core\DB::get()->static;

        $data['modify'] = time();
        $data['domain_id'] = $GLOBALS['domain']['_id'];

        return$collection->update($params, $data);
    }

    public function delete($id){
        $collection = \Core\DB::get()->static;

        $dir = UploadsDir.DIRECTORY_SEPARATOR.'static'.DIRECTORY_SEPARATOR.$id;
        if(file_exists($dir)) $this->Fm->deleteDir($dir);

        return (boolean)$collection->remove(['_id'=>$id]);
    }

    public function FindUrl($url, $locale = 'rus'){
        $locale = !$locale ? $GLOBALS['Core']->getCookie('mst_locale') : $locale;
        $collection = \Core\DB::get()->static;
        $r = $collection->findOne(
            [
                'url'=>$url,
                'locale'=>$locale,
                'domain_id'=>$GLOBALS['domain']['_id'],
            ],
            [
                'domain_id'=>false
            ]);

//        \Core\Cache::Set($locale.':static/'.$url, $r);
        return$r;
    }

    public function findContentUrl($name, $config){
        $collection = \Core\DB::get()->static;
        $params = ['title'=> ['$regex'=>"$name"] ];

        $params['domain_id']=$GLOBALS['domain']['_id'];

        $result = $collection->find($params, ['_id'=>false, 'title'=>true, 'url'=>true, 'domain_id'=>false])->toArray();

        //todo прикрутить учет локализации

        $list = [];
        if($result){
            foreach($result as $item){
                $item['url'] = "/{$config['linkModule']}/".$item['url'];
                $list[] = $item;
            }
        }

        return$list;
    }
}