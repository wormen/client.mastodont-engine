<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 25.01.2016
 * Time: 12:14
 */

namespace Mastodont\Api\v1;


class Users
{
    protected
        $Queue,
        $Groups,
        $Letters,
        $Session,
        $module = 'users';

    function __construct(){
        $this->Queue = new \Core\Queue();
        $this->Session = new \Core\Session();
        $this->Letters = new \Mastodont\Api\v1\Letters();
        $this->Groups = new \Mastodont\Api\v1\Groups();
    }

    /**
     * Список пользователей
     * @return mixed
     */
    public function loadList(){
        $collection = \Core\DB::get()->users;

        $params = [
            'domain_id'=>$GLOBALS['domain']['_id']
        ];

        if(isset($_REQUEST['debug']) && $_REQUEST['debug'] == 1)
            $params = [];

        $list = $collection->find($params, [
            '_id'=>true,
            'login'=>true,
            'email'=>true,
            'isRoot'=>true,
            'enabled'=>true,
            'domain_id'=>false
        ])->toArray();

        $arr = [];

        foreach ($list as $user)
            if(!$this->checkRoot($user['login']))
                $arr[] = $user;

        return$arr;
    }

    /**
     * Получение полных данных пользователя для редактирования
     * @param $id
     * @return mixed
     */
    public function edit($id){
        $collection = \Core\DB::get()->users;
        $d=$collection->findOne(['_id'=>$id],
            [
                '_id'=>false,
                'password'=>false,
                'passwordHash'=>false
            ],
            [
                'domain_id'=>false
            ]);

        $d = \Core\Lib::array_update($d, $this->isDefault());

        return$d;
    }

    /**
     * Данные пользователя по логину
     * @param $login
     * @return mixed
     */
    public function getLogin($login){
        $collection = \Core\DB::get()->users;
        $d=$collection->findOne([
            'login'=>$login,
            'domain_id'=>$GLOBALS['domain']['_id']
        ],
            [
                '_id'=>false,
                'password'=>false,
                'passwordHash'=>false,
                'domain_id'=>false
            ]);


        return $this->getClearData($d);
    }

    /**
     * Данные пользователя по ID
     * @param $id
     * @return mixed
     */
    public function getID($id){
        $d = $this->edit($id);

        if($d['domain_id'])
            unset($d['domain_id']);

        return $this->getClearData($d);
    }

    /**
     * Данные польователя по UUID
     * @param $uuid
     * @return mixed
     */
    public function getUUID($uuid){
        $collection = \Core\DB::get()->users;
        $d=$collection->findOne([
            'uuid'=>$uuid,
            'domain_id'=>$GLOBALS['domain']['_id']
        ],
            [
                '_id'=>true,
                'password'=>false,
                'passwordHash'=>false,
                'domain_id'=>false
            ]);

        return$d;
    }

    /**
     * Сохранение пользователя
     * @param $data
     * @return bool
     */
    public function save($data){
        $collection = \Core\DB::get()->users;

        $data['domain_id'] = $GLOBALS['domain']['_id'];

        if(!$this->checkLogin($data['login']))
            return(boolean)$collection->insert($this->ClearUserData($data));
        else
            return false;
    }

    /**
     * Обновление пользователя
     * @param $params
     * @param $data
     * @return mixed
     */
    public function update($params, $data){
        $collection = \Core\DB::get()->users;

        $data['domain_id'] = $GLOBALS['domain']['_id'];
        return $collection->update($params, $this->ClearUserData($data));
    }

    /**
     * Удвленеи пользователя
     * @param $id
     * @return bool
     */
    public function delete($id){
        $collection = \Core\DB::get()->users;

        $this->RemoveAllNotify($id);
        return (boolean)$collection->remove(['_id'=>$id]);
    }

    /**
     * Проверка валидности пароля (для пользовательской части)
     * @param $user_id
     * @param $pass
     */
    public function checkPassword($user_id, $pass){
        $status = 'INVALID';

        if(!$user_id || !$pass)
            return ['status'=>$status];

        $collection = \Core\DB::get()->users;
        $u=$collection->findOne(['_id'=>$user_id],
            [
                '_id'=>true,
                'passwordHash'=>true
            ]);

        if($u){
            $isPass = password_verify($pass, $u['passwordHash']);
            $status = $isPass ? 'OK' : 'INVALID';
        }

        return ['status'=>$status];
    }

    /**
     * Изменение пароля пользователя
     * @param $user_id
     * @param $newPassword
     * @return mixed
     */
    public function ChangePassword($user_id, $newPassword){
        $collection = \Core\DB::get()->users;

        $data = ['passwordHash' => $this->getPassHash($newPassword)];
        return$collection->update(['_id'=>$user_id], $data);
    }

    /**
     * Добавление уведомлений для пользователя
     * @param $userID
     * @param $data
     * @return bool
     */
    public function AddNotify($userID, $type='system',$data){
        $collection = \Core\DB::get('MSGDB')->messages;
        $arr = [
            'user_id'=>$userID,
            'create'=>time(),
            'sysType'=>$type,
            'type'=>null, // тип ообщения (например response)
            'autor_id'=>null, // пользователь, который написал сообщение
            'autor_data'=>[], // данные пользователя
            'data'=>$data
        ];

        return (boolean) $collection->insert($arr);
    }

    /**
     * Получить уведомления пользователя
     * @param $userID
     * @return mixed
     */
    public function getNotify($userID){
        $collection = \Core\DB::get('MSGDB')->messages;
        $list = $collection->find(['user_id'=>$userID], [])->toArray();
        return$list;
    }

    /**
     * Удалить уведомление пользователя
     * @param $userID
     * @param $idMsg
     * @return mixed
     */
    public function RemoveNotify($id_msg){
        $collection = \Core\DB::get('MSGDB')->messages;
        $list = $collection->remove(['_id'=>$id_msg]);
        return$list;
    }

    /**
     * Удаление всех уведомлений пользователя
     * @param $userID
     * @param $idMsg
     * @return mixed
     */
    public function RemoveAllNotify($userID){
        $collection = \Core\DB::get('MSGDB')->messages;
        return$collection->remove(['user_id'=>$userID]);
    }

    /**
     * Активация пользователя
     * @param $uuid
     * @return bool
     */
    public function Activate(){

        if(isset($_GET['code'])){
            $collection = \Core\DB::get()->users;

            $uuid = $GLOBALS['app']->escape($_GET['code']);

            if(!$uuid)
                $state = 'INACTIVE_LINK';
            else{
                $U=$collection->findOne([
                    'uuid'=>$uuid,
                    'domain_id'=>$GLOBALS['domain']['_id']
                ],['domain_id'=>false]);

                if(!$U || $U['delTime']<time())
                    $state = 'INACTIVE_LINK';

                else{
                    if($U['isTmp'] == false)
                        $state = 'IS_ACTIVE_PROFILE';
                    else{
                        $id = $U['_id'];

                        $password = null;
                        if(isset($U['password'])){
                            $password = $U['password'];
                            unset($U['password']);
                        }

                        unset($U['_id']);

                        $U['delTime'] = 0;
                        $U['isTmp'] = false;
                        $U['enabled'] = true;

                        $collection->update(['_id'=>$id], $U);

                        //авторизация
                        $A = $this->auth($U['login'], $password, false);

                        $state = $A == true ? 'OK' : 'INACTIVE_LINK';

                        if($state == 'OK'){
                            // Генерирования письма на основе шаблона
                            $t = $this->Letters->ReplaceVars($U, $this->Letters->getName('register.success.email'));
                            $this->Queue->Add('email', $t);
                        }
                    }
                }

            }

            return $state;

        }else
            return 'INACTIVE_LINK';

    }

    /**
     * Восстановление пароля пользователя
     * @param $data
     * @return array
     */
    public function repairPass($data){
        $collection = \Core\DB::get()->users;

        $u=$collection->findOne([
            '$or'=> [
                [ 'login'=>$data ],
                [ 'email'=>$data ]
            ],
            'domain_id'=>$GLOBALS['domain']['_id']
        ], ['domain_id'=>false]);

        $code = 'OK';
        if($u){
            $u['passwordHash'] = $this->getPassHash($GLOBALS['Security']->GenerateHash());

            // отправляем письмо с новым паролем
            $t = $this->Letters->ReplaceVars($u, $this->Letters->getName('repair.password.email'));
            $this->Queue->Add('email', $t);

            $collection = \Core\DB::get()->users;
            $collection->update(['_id'=>$u['_id']], ['passwordHash'=>$u['passwordHash']]);

        }else
            $code = 'USER_NOT_FOUND';

        return [
            'code'=>$code,
            'text'=>$GLOBALS['lang']['repairPass'][$code],
        ];
    }

    /**
     *  Удаление временных пользователей
     */
    public function deleteTmp(){
        $collection = \Core\DB::get()->users;
//        $u=$collection->find([
//            '$and'=> [
//                [ 'login'=>$user ],
//                [ 'email'=>$user ]
//            ]
//        ],
//            [
//                'login'=>true,
//                'passwordHash'=>true,
//                'access'=>true, //Доступ в закрытый раздел
//                'is_ban'=>true, //Заблокирован
//                'enabled'=>true,
//                'isRoot'=>true,
//            ]);

//        $apdateArr = [
//            'delTime'=>0,
//            'isTmp'=>false
//        ];
    }

    /**
     * Добавление пользователя из пользовательской части/софта
     * @param array $data - входные данные (логин, пароль, email)
     *      ['login'=>'userLogin', 'email'=>'userEmail', 'password'=>'userPassword']
     *
     * @return array
     */
    public function newUser($data){

        if(!isset($data['login']) || ($data['login'] && mb_strlen($data['login']) == 0) ){
            return false;
        }

        $Conf = $GLOBALS['Setting']->get();
        $C = $Conf['modules']['account']; // конфиг

        $data['isTmp'] = true; // временный пользователь
        $uuid = $GLOBALS['Security']->getUUID();

        if(isset($C['regActiveTime'])){
            $data['delTime'] = strtotime( '+'.$C['regActiveTime'].' hours' );
        }

        $data['uuid'] = $uuid;
        $data['enabled'] = true;
        $data['domain_id'] = $GLOBALS['domain']['_id']; // привязка к домену

        // группа по умолчанию
        $G = $this->Groups->FindName(isset($Conf['site']['userGroupDefault']) ? $Conf['site']['userGroupDefault'] : null);
        $data['groups'] = [$G ? $G['_id'] : ''];


        $et = 'register.success.email'; // шаблон письма для уведомления о регистрации
        $statusCode = 'ok';
        $code = null;

        switch($C['regType']){
            case 'none':
                $data['isTmp'] = false;
                $data['delTime'] = 0;
                $code = 'IS_AUTH';
                break;

            case 'email':
                $et = 'profile.email.activate';
                $data['enabled'] = false;
                $code = 'EMAIL_ACTIVATE';
                break;

            case 'admin':
                $data['isTmp'] = false;
                $data['enabled'] = false;
                $code = 'ADMIN_ACTIVATE';
                break;
        }

        // Генерирования письма на основе шаблона
        $t = $this->Letters->ReplaceVars($data, $this->Letters->getName($et));

        // добавляем в очередь Email об успешной регистрации
        $this->Queue->Add('email', $t);

        // coхраняем пользователя
        if($this->save($data)){

            // ищем пользователя по UUID
            $u = $this->getUUID($uuid);

            // уведомление в ЛК о необходимости заполнить профиль
            $t = $this->Letters->ReplaceVars($data, $this->Letters->getName('profile.fill.notify'));

            $this->AddNotify($u['_id'], 'system', $t['text']);
        }

        return [
            'statusCode'=>$statusCode,
            'code'=>$code,
            'text'=>(isset($GLOBALS['lang']['registry'][$code]) ? $GLOBALS['lang']['registry'][$code] : ''),
        ];

    }


    /**
     * Проверка существования логина
     * @param $login
     * @return bool
     */
    public function checkLogin($login){
        $collection = \Core\DB::get()->users;
        $list = $collection->find([
            'login'=>$login,
            'domain_id'=>$GLOBALS['domain']['_id']
        ], ['login'=>true, 'domain_id'=>false])->toArray();

        return count($list)>0;
    }

    /**
     * Проверка существования Email
     * @param $email
     * @return bool
     */
    public function checkEmail($email){
        $collection = \Core\DB::get()->users;
        $list = $collection->find([
            'email'=>$email,
            'domain_id'=>$GLOBALS['domain']['_id']
        ], ['login'=>true, 'domain_id'=>false])->toArray();

        return count($list)>0;
    }

    /**
     * Авторизация пользователя
     * @param $user
     * @param $pass
     * @param $isAdmin
     * @return bool|string
     */
    public function auth($user, $pass, $isAdmin){
        $collection = \Core\DB::get()->users;
        $C = $GLOBALS['Setting']->get();
        $module = $C['modules']['account'];

        $params = [
            '$or' => [ ['login' => $user], ['email' => $user] ]
        ];
        
        if($user != 'root')
            $params['domain_id'] = $GLOBALS['domain']['_id'];
        

        $u = $collection->findOne($params,
            [
                'login' => true,
                'passwordHash' => true,
                'access' => true, //Доступ в закрытый раздел
                'is_ban' => true, //Заблокирован
                'enabled' => true, // активен ли профиль пользователя
                'isRoot' => true,
                'groups' => true, // список групп пользователя
                'domain_id' => false
            ]);


        if(!is_array($u) || $u == null) return 'NOT_FOUND';

        // проверка корневого пользователя
        if($u['login'] == 'root'){
            $u['isRoot'] = $u['access'] = $u['enabled'] = true;
            $u['is_ban'] = false;

        }else
            if(isset($u['domain_id']) && $u['domain_id'] != $GLOBALS['domain']['_id'])
                $u = null;


        if(!is_array($u) || $u == null) return 'NOT_FOUND';

        // проверяем, лимит попыток авторизации, и разрешено ли авторизовываться

        // проверяем время разблокировки
        if(isset($u['isAuth']['next']) && $u['isAuth']['next']>time()) // пользователь еще заблокирован
            return 'BLOCKED';

        if($isAdmin){
            // проверяем параметры групп пользователя

            // вынести проверку в функцию
        }

        // проверка пароля
        $isPass = password_verify($pass, $u['passwordHash']);
        if(!$isPass){
            $u['isAuth']['count'] = isset($u['isAuth']['count']) ? $u['isAuth']['count'] : 0;
            $u['isAuth']['count']++;

            // превышен лимит попыток авторизаций
            if($u['isAuth']['count'] >= $module['antibruteCnt']){
                $time = isset($module['antibruteCnt']) ? $module['antibruteCnt'] : 15;

                $UD = [
                    'isAuth'=>[
                        'count'=>$u['isAuth']['count'],
                        'next'=>time()+($time*60),
                    ]
                ];
                $collection->update(['_id'=>$u['_id']], $UD);
                return 'BLOCKED';
            }
            else
                return 'WRONG_PASSWORD';

        }
        else{
            $UD = [
                'isAuth'=>[
                    'count'=>0,
                    'next'=>0,
                ]
            ];
            $collection->update(['_id'=>$u['_id']], $UD);
        }



        //todo фиксируем кол-во попыток


//        modules.account.antibruteCnt
//modules.account.antibruteTime

        if(!isset($u['isRoot'])){
            if(isset($u['is_ban']) && $u['is_ban'] || !$u['enabled'] && (isset($u['isTmp']) && !$u['isTmp'])) return 'BLOCKED';
            if($isAdmin && !$u['access']) return 'ACCESS_DENIED';
        }

        // данные пользователя
        $ud = json_encode([
            'u'=>$u['_id'],
            's'=>session_id(),
        ]);

        $type = (isset($_REQUEST['type']) && $_REQUEST['type'] == 'admin' ? 'admin' : 'user');
        $GLOBALS['Core']->setCookie('mst_'.$type, base64_encode($ud));

        //todo изменить способ хранения
        $_SESSION['USER_ID'] = $u['_id'];
        $_SESSION['LOGIN'] = $u['login'];
        $_SESSION['IS_ROOT'] = (isset($u['isRoot']) && $u['isRoot']);

        $this->Session->auth($u['_id'], [
            'isRoot'=>$_SESSION['IS_ROOT']
        ]);
        return true;
    }

    /**
     * Получание данных пользователя из куки
     * @param $type
     * @return mixed
     */
    public function decodeData($type){
        $c = $GLOBALS['Core']->getCookie('mst_'.$type);
        $user = base64_decode($c);
        return json_decode($user, true);
    }

    /**
     * @param $type - admin / user
     * @return bool
     */
    public function logOut($type){
        session_regenerate_id();
        $GLOBALS['Core']->deleteCookie("mst_$type");
        return false;
    }

    public function getPermistions($user = false){

        $default = [
            'access_cp' => false,
            'captcha' => false,
            'moderate' => false,
            'sh_cls' => false,
            'group_discount' => 0,
        ];

        if($user){
            $gl = is_array($user) ? $user['groups'] : [];

            foreach ($gl as $gid){
                $gd = $this->Groups->edit($gid);

                if($gd['access_cp']) $default['access_cp'] = $gd['access_cp'];
                if($gd['captcha']) $default['captcha'] = $gd['captcha'];
                if($gd['moderate']) $default['moderate'] = $gd['moderate'];
                if($gd['sh_cls']) $default['sh_cls'] = $gd['sh_cls'];
                if((int)$gd['group_discount']>0) $default['group_discount'] = $gd['group_discount'];
            }

        }

        return $default;
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * Проверка отображения логина
     * @param $login
     * @return bool
     */
    protected function checkRoot($login){
        return (strcasecmp($login, 'root') === 0 ? true : false);
    }

    /**
     * Генерируем хэш пароля
     * @param $pass
     * @return mixed
     */
    private function getPassHash($pass){
        return password_hash($pass, PASSWORD_BCRYPT, [ 'cost' => 15 ]);
    }

    /**
     * Очищаем пользоватлеские данные при сохранении, дополняем данными по умолчанию, если их нет
     * @param $data
     * @return mixed
     */
    private function ClearUserData($data){
        $C = $GLOBALS['Setting']->get();

//        if(!isset($data['enabled']))
//            $data['enabled'] = false;

        if(!array_key_exists('enabled', $data) && !isset($_REQUEST['id']))
            $data['enabled'] = false;

        // если в данных есть пароль, генерируем для него хэш
        if(array_key_exists('password', $data) && strlen($data['password'])>0)
            $data['passwordHash'] = $this->getPassHash($data['password']);

        // вероятно, пользователь создавался не из адмики, доавим дефолтные поля
        if(count($data)>=3)
            $data = array_merge($this->isDefault(), $data);

        // todo группа пользователей по умолчанию
        // проверяем у пользователя, указана ли группа
        // получаем список групп пользователей
        // сравниваем с настройками
        // добавляем дефолтную группу


        if($C['modules']['account']['regType'] != "email" || isset($_REQUEST['type']) && $_REQUEST['type'] == 'admin')
            unset($data['password']);

        if(isset($data['repassword'])) unset($data['repassword']);
        if(isset($data['password'])) unset($data['password']);

        if(!array_key_exists('uuid', $data))
            $data['uuid'] = $GLOBALS['Security']->getUUID();

        return$data;
    }

    /**
     * Очистка данных пользователя от служебной информации, перед выводом на клиент
     * @param $data
     * @return mixed
     */
    private function getClearData($data){
        // поля, которые удаляем
        $fields = [
            'sysMenu', 'protectSetting', 'modulesSetting', 'mainRoot', 'is_ban', 'access',
            'isRoot', 'isAuth', 'groups', 'favorites', 'enabled', 'domains', 'defaultLocale'
        ];

        foreach($fields as $f)
            if(isset($data[$f]))
                unset($data[$f]);

        return$data;
    }

    /**
     * Дефолтные даныне пользователя
     * этими данными дополняется профиль, если каких-то полей нет
     * @return array
     */
    private function isDefault(){
        $arr = [
            'balans'=>0,
            'login' => '',
            'surname' => '',
            'name' => '',
            'password' => '',
            'repassword' => '',
            'pantonumic' => '',
            'gender' => 'male',
            'avatar' => '',
            'email' => '',
            'type_face' => 'fiz',
            'access' => false,
            'protectSetting' => false,
            'mainRoot' => false,
            'is_ban' => false,
            'enabled' => true,
            'isRoot' => false, // главный юзер
            'groups' => [],
            'domains' => [],
            'connect' => [
                'phone' => '',
                'fax' => '',
                'jabber' => '',
                'skype' => '',
                'icq' => '',
                'vk' => '',
                'ok' => '',
                'facebook' => '',
                'twitter' => ''
            ],
            'fullAddress'=>'',
            'address' => [
                'region' => '',
                'district' => '',
                'city' => '',
                'street' => '',
                'korp' => '',
                'dom' => '',
                'kv' => '',
                'zip' => ''
            ],
            'ur_data' => [
                'company_name' => '',
                'ogrn' => '',
                'inn' => '',
                'bik' => '',
                'kpp' => '',
                'bank_name' => '',
                'rs' => '',
                'krs' => '',
                'u_address' => '',
                'f_address' => ''
            ],
            'defaultLocale' => 'rus',
            'isAuth'=>[
                'count'=>0,
                'next'=>0
            ],
            'favorites'=>[
                'ads'=>[],
                'users'=>[]
            ],
            'rating' => [
                'plus' => 0,
                'minus' => 0
            ],
            'modulesSetting' => [
                'shop' => [
                    'discount' => 0
                ]
            ],
            'map'=>[
                'type'=> 'yandex',
                'position'=>[0, 0],
                'zoom'=>12,
            ],
            'sysMenu' => [],
        ];

        return$arr;
    }
}