<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 13.02.2016
 * Time: 21:22
 */

namespace Mastodont\Api\v1;

class Contacts
{

    protected
        $module = 'contacts';
    
    public function get(){
        $collection = \Core\DB::get()->contacts;

        $dt = $collection->find(['domain_id'=>$GLOBALS['domain']['_id']], ['domain_id'=>false])->sort(["pos"=>'ask'])->toArray();
        return$dt;
    }

    public function getID($id){
        $collection = \Core\DB::get()->contacts;
        $dt = $collection->findOne(['_id'=>$id], ['_id'=>false]);
        return ($dt ? $dt : false);
    }

    public function set($data){
        $collection = \Core\DB::get()->contacts;

        foreach ($data as $d) {
            $d['domain_id'] = $GLOBALS['domain']['_id'];

            if (isset($d['_id']))
                $collection->update(['_id' => $d['_id']], $d);
            else
                $collection->insert($d);
        }

        return true;
    }

    public function delete($id){
        $collection = \Core\DB::get()->contacts;
        return$collection->remove(['_id'=>$id]);
    }
}