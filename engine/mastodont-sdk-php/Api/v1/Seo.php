<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 05.02.2016
 * Time: 15:35
 */

namespace Mastodont\Api\v1\Seo;


class Redirects
{
    protected
        $module = 'seoRedirects';

    function __construct(){
    }

    private function file(){
        $file = base64_encode($GLOBALS['Setting']->getDomain()).'.site-redirects';
        $dir = ConfigDir.'/redirects/';
        if(!file_exists($dir)) mkdir($dir, 0755);

        if($dir.$file)
            return $dir.$file;
        else
            return [];
    }

    public function Go(){

        $list = $this->get();

        if(is_array($list) && count($list)>0){
            foreach($list as $R){
                if($R['oldUrl'] == $_SERVER['REQUEST_URI'] && $R['statusCode'] == 404){
                    http_response_code(404);
                    die();
                }

                if($R['oldUrl'] == $_SERVER['REQUEST_URI']){
                    header('Location: '.$R['newUrl'], true, $R['statusCode']);
                    die();
                }
            }
        }

        return true;
    }

    public function get(){
        if(file_exists($this->file()))
            return json_decode(file_get_contents($this->file()), true);
        else
            return [];
    }

    public function save($data){
        return file_put_contents($this->file(), json_encode($data));
    }
}