<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 24.01.2016
 * Time: 0:11
 */

namespace Mastodont\Api\v1;


class Menu
{

    protected
        $module = 'menu';
    
    function __construct(){
    }

    private function BuildTree($list=[], $parent=0){
        $arr = [];

        foreach($list as $l){
            if($l['parent_id'] == $parent){
                $l['children'] = $this->BuildTree($list, $l['_id']);

                $arr[] = $l;
            }
        }

        return$arr;
    }

    public function miniList($isTree=false){
        $collection = \Core\DB::get()->menu;
        $list=$collection->find(
            [
                'domain_id'=>$GLOBALS['domain']['_id']
            ],
            [
                'parent_id'=>true,
                'title'=>true,
                'pos'=>true,
                'enabled'=>true,
                'domain_id'=>false
            ]
        )->toArray();

        if($isTree)
            $list = $this->BuildTree($list);

        return$list;
    }

    public function fullList($isTree=false){
        $collection = \Core\DB::get()->menu;
        $list=$collection->find([
            'enabled'=>true,
            'domain_id'=>$GLOBALS['domain']['_id']
        ], ['domain_id'=>false])->toArray();

        if($isTree)
            $list = $this->BuildTree($list);

        return$list;
    }

    public function getID($id){
        if(!$this) return [];

        $list = $this->fullList(false);

        $newArr = [];
        foreach($list as $item){
            if($item['_id'] == $id)
                return $this->BuildTree( $list, $id );
        }

        return$newArr;
    }

    public function edit($id){
        $collection = \Core\DB::get()->menu;
        $m=$collection->findOne(['_id'=>$id]);
        return$m;
    }

    public function save($data){
        $collection = \Core\DB::get()->menu;
        $data['domain_id'] = $GLOBALS['domain']['_id'];
        return(boolean)$collection->insert($data);
    }

    public function update($params, $data){
        $collection = \Core\DB::get()->menu;
        $data['domain_id'] = $GLOBALS['domain']['_id'];
        return$collection->update($params, $data);
    }

    public function delete($id){
        $collection = \Core\DB::get()->menu;
        return (boolean)$collection->remove(['_id'=>$id]);
    }

    /**
     * Поиск ссылок на контент в определенном модуле
     * @param $name
     * @param $module
     * @return array
     */
    public function findContentUrl($name, $module){
        $result = [];
        $C = $GLOBALS['Setting']->get()['modules'];

        switch($module){
            case 'static':
                $Static = new \Mastodont\Api\v1\StaticPage();
                $result = $Static->findContentUrl($name, $C['static']);
                break;
        }

        return$result;
    }
}