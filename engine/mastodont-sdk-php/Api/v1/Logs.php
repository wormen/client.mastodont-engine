<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 30.01.2016
 * Time: 17:28
 */

namespace Mastodont\Api\v1;


class Logs
{

    public function init(){
        $collections = [];
        $lc = \Core\DB::get('LDB')->listCollections();

        foreach($lc as $k=>$v){
            $collection = \Core\DB::get('LDB')->$k;

            $collections[] = [
                'table'=>$k,
                'list'=>$collection->find([], [])->toArray()
            ];
        }

        return$collections;
    }

    public function delete($id, $k){
        $collection = \Core\DB::get('LDB')->$k;
        return (boolean)$collection->remove(['_id'=>$id]);
    }
    
}