<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 16.02.2016
 * Time: 16:04
 */

namespace Mastodont\Api\v1;

use \JasonGrimes\Paginator;

class Video
{

    protected
        $post = 'videoRollers',
        $cat = 'videoCategory',
        $module = 'video';

    /**
     * настройки модуля
     * @return mixed
     */
    protected function Setting(){
        return $GLOBALS['Setting']->get()['modules'][$this->module];
    }


    private function BuildTree($list=[], $parent=0){
        $arr = [];

        foreach($list as $l){
            if($l['parent_id'] == $parent){
                $l['children'] = $this->BuildTree($list, $l['_id']);

                $arr[] = $l;
            }
        }

        return$arr;
    }

    public function categoryMiniList($isTree=false, $params = [], $get = []){

        if(count($get) == 0)
            $get = [
                'parent_id'=>true,
                'name'=>true,
                'pos'=>true,
                'url'=>true,
                'enabled'=>true,
            ];

        if(isset($_REQUEST['mini']))
            $get = [
                'parent_id'=>true,
                'name'=>true,
            ];

        if(!isset($get['parent_id']))
            $get['parent_id'] = true;

        if(isset($_REQUEST['user']))
            $params = ['enabled'=>true];
        
        if(isset($_REQUEST['parent_id']))
            $params['parent_id'] = $_REQUEST['parent_id'];

        $params['domain_id'] = $GLOBALS['domain']['_id'];
        $get['domain_id'] = false;

        $collection = \Core\DB::get()->{$this->cat};
        $list=$collection->find($params, $get)->toArray();

        if($isTree)
            $list = $this->BuildTree($list);

        return$list;
    }

    public function fullList($isTree=false, $type = null){
        $collection = \Core\DB::get()->{$this->cat};

        $params = [
            'domain_id' => $GLOBALS['domain']['_id']
        ];

        if($type != 'full')
            $params['enabled'] = true;

        $list=$collection->find($params, ['domain_id' => false])->toArray();

        if($isTree)
            $list = $this->BuildTree($list);

        return$list;
    }

    public function getID($id){
        $list = $this->fullList(false);

        $newArr = [];
        foreach($list as $item){
            if($item['_id'] == $id)
                return $this->BuildTree( $list, $id );//[0]['children']
        }

        return$newArr;
    }

    public function categoryEdit($id){
        $collection = \Core\DB::get()->{$this->cat};
        return $collection->findOne(['_id'=>$id], ['domain_id' => false]);
    }

    public function categoryCheckUrl($url, $locale='rus'){
        $collection = \Core\DB::get()->{$this->cat};

        $list = $collection->find([
            'url' => $url,
            'locale' => $locale,
            'domain_id' => $GLOBALS['domain']['_id']
        ], ['_id' => true,'domain_id' => false])->toArray();

        return count($list)>0;
    }

    public function categorySave($data){
        $collection = \Core\DB::get()->{$this->cat};

        $data['domain_id'] = $GLOBALS['domain']['_id'];

        return (boolean) $collection->insert($this->ClearCat($data));
    }

    public function categoryUpdate($params, $data){
        $collection = \Core\DB::get()->{$this->cat};

        $data['domain_id'] = $GLOBALS['domain']['_id'];

        return (boolean) $collection->update($params, $this->ClearCat($data));
    }

    public function categoryDelete($id){
        $collection = \Core\DB::get()->{$this->cat};
        return$collection->remove(['_id'=>$id]);
    }

    protected function ClearCat($data){

        foreach ($data['channals'] as $k=>$v)
            if(strlen($v['id']) == 0)
                unset($data['channals'][$k]);

        return$data;
    }

// ---------------------------------------------------------------------------------------------------------------------

    /**
     * Редактирование объявления
     * @param $id
     * @return mixed
     */
    public function postEdit($id){
        $collection = \Core\DB::get()->{$this->post};
        return $collection->findOne(['_id'=>$id], ['domain_id' => false]);
    }

    /**
     * Список объявлений со всеми параметрами
     * @param array $fields
     * @param array $params
     * @return mixed
     */
    public function postFullList(array $fields = [], array $params = [], $sort = 'desc'){
        global$Tpl;

        // настройки модуля
        $S = $this->Setting();

        $params['domain_id'] = $GLOBALS['domain']['_id'];

        if(!$Tpl->CheckAdminPanel())
            $params['enabled'] = true;

        $fields['domain_id'] = false;

        $collection = \Core\DB::get()->{$this->post};

        if($Tpl->CheckAdminPanel()){
            $urlPattern = "/api/v1/ads?type=post&page=(:num)";
        }else{
            $params['enabled'] = true;
            $urlPattern = "";
        }

        $totalItems = $collection->find($params, $fields)->count();
        $currentPage = isset($_REQUEST['page']) ? $_REQUEST['page'] : 1;
        $itemsPerPage = $S['inPageAdmin'];

        $sort = (int)($sort === 'desc' ? -1 : 1);
        $list=$collection->find($params, $fields)
            ->sort(['create' => $sort])
            ->skip($currentPage-1)
            ->limit($itemsPerPage)
            ->toArray();

        foreach ($list as $k=>$v){

            $v['url'] = "/{$S['linkModule']}/{$S['links']['ad']}/{$v['url']}";
            $list[$k] = $v;
        }

        $paginator = new Paginator($totalItems, $itemsPerPage, $currentPage, $urlPattern);
        $paginator->setMaxPagesToShow($Tpl->Paginate['maxPagesToShow']); // кол-во отображаемых страниц в пагинаторе
        
        return [
            'list'=>$list,
            'paginator'=>$Tpl->Paginator($paginator),
        ];
    }

    /**
     * Список объявлений с клатким набором параметров
     * @param array $params
     * @param array $get
     * @return mixed
     */
    public function postMiniList(array $params = [], array $fields = [], $sort = 'desc'){

        // настройки модуля
        $S = $this->Setting();

        if(count($fields) == 0)
            $fields = [
                'id'=>true,
                '_id'=>true,
                'url'=>true,
                'name'=>true,
                'moderateStatus'=>true,
                'enabled'=>true,
            ];

        $params['domain_id'] = $GLOBALS['domain']['_id'];

        if($GLOBALS['Tpl']->CheckAdminPanel()){
            $urlPattern = "/api/v1/ads?type=post&page=(:num)";
        }else{
            $params['enabled'] = true;
            $urlPattern = "";
        }

        $fields['domain_id'] = false;

        $collection = \Core\DB::get()->{$this->post};

        $totalItems = $collection->find($params, $fields)->count();
        $currentPage = isset($_REQUEST['page']) ? $_REQUEST['page'] : 1;
        $itemsPerPage = $S['inPageAdmin'];


        $sort = (int)($sort === 'desc' ? -1 : 1);
        $list=$collection->find($params, $fields)
            ->sort(['create' => $sort])
            ->skip($currentPage-1)
            ->limit($itemsPerPage)
            ->toArray();

        foreach ($list as $k=>$v){

            $v['url'] = "/{$S['linkModule']}/{$S['links']['ad']}/{$v['url']}";
            $list[$k] = $v;
        }

        return [
            'list'=>$list,
            'paginator'=>$GLOBALS['Tpl']->Paginator(new Paginator($totalItems, $itemsPerPage, $currentPage, $urlPattern)),
        ];
    }

    /**
     * Поиск объявления по URL
     * @param $url
     * @return mixed
     */
    public function postFindUrl($url){
        // настройки модуля
        $S = $this->Setting();

        $collection = \Core\DB::get()->{$this->post};

        $param = [ 'url'=>$url ];
        if(isset($_GET['user-edit']))
            $param = [ '_id'=>$url ];

        $p = $collection->findOne($param, ['domain_id' => false]);

        foreach($p['categoryTree'] as $k=>$C){
            $cat = $this->categoryEdit($C['_id']);
            $p['categoryTree'][$k]['url'] = "/{$S['linkModule']}/{$S['links']['category']}/{$cat['url']}";
        }

        $p['isEdit'] = $p['user_id'] == $GLOBALS['Tpl']->getUserData()['u'];

        $p['imgs'] = $this->postGetImg($p['_id']);

        $User = new \Mastodont\Api\v1\Users();
        $u = $User->getID($p['user_id']);

        if(isset($u['balans'])) unset($u['balans']);

        $p['userData'] = $u;

        return$p;
    }

    /**
     * Последние объявления пользователя
     * @param $user_id
     * @param int $count
     * @return mixed
     */
    public function postUserLast($user_id, $count = 1){

        // настройки модуля
        $S = $this->Setting();

        $collection = \Core\DB::get()->{$this->post};
        $list = $collection->find(
            [
                'user_id'=>$user_id,
                'moderateStatus'=>'published'
            ],
            ['domain_id' => false])
            ->sort(['create' => -1])
            ->skip(0)
            ->limit($count)
            ->toArray();

        foreach ($list as $k=>$v){

            $v['url'] = "/{$S['linkModule']}/{$S['links']['ad']}/{$v['url']}";
            $list[$k] = $v;
        }

        return$list;
    }

    // получаем список объявлений по статусу
    public function postGetStatus($user_id, $status, $count = 5){
        // настройки модуля
        $S = $this->Setting();

        $collection = \Core\DB::get()->{$this->post};
        $list = $collection->find(
            [
                'user_id'=>$user_id,
                'moderateStatus'=>$status
            ],
            ['domain_id' => false])
            ->sort(['create' => -1])
            ->skip(0)
            ->limit($count)
            ->toArray();

        foreach ($list as $k=>$v){

            $v['url'] = "/{$S['linkModule']}/{$S['links']['ad']}/{$v['url']}";
            $v['imgs'] = $this->postGetImg($v['_id']);

            $list[$k] = $v;
        }

        return$list;
    }

    /**
     * Сохранение объявления
     * @param $data
     * @return bool
     */
    public function postSave($data){
        $collection = \Core\DB::get()->{$this->post};

        // настройки модуля
        $S = $this->Setting();
        
        $tmp = $data['tmp'];
        unset($data['tmp']);

        $data['id'] = $GLOBALS['Core']->Counter('ads', 'post');
        $data['create'] = $data['modify'] = time();
        $data['domain_id'] = $GLOBALS['domain']['_id'];

        $data['status'] = 'published';


        // статус модерации по умолчанию
        $data['moderateStatus'] = $S['moderateStatus'];

        //дата окончания показа, для платнх услуг
        $data['isViewTime'] = false;
        $data['viewTime'] = 0;

        // рейтинг объявления
        $data['rating'] = [
            'plus'=>0,
            'minus'=>0
        ];

        $data['enabled'] = true;

        $isSave = (boolean) $collection->insert($data);

        $data['url'] = $GLOBALS['Core']->Translit($data['name']).'-'.$GLOBALS['Security']->getUUID();
        $data['url'] = str_replace([' '], '-', $data['url']);

        $collection->update(['_id'=>$data['_id']], ['url'=>$data['url']]);

        $this->postSaveImg($data['_id'], $tmp);

        return$isSave;
    }

    /**
     * Обновление объявления
     * @param $params
     * @param $data
     * @return bool
     */
    public function postUpdate($params, $data){
        $collection = \Core\DB::get()->{$this->post};

        // настройки модуля
        $S = $this->Setting();

        $tmp = $data['tmp'];
        unset($data['tmp']);


        if($data['_id']) unset($data['_id']);
        if($data['isEdit']) unset($data['isEdit']);
        if($data['imgs']) unset($data['imgs']);
        if($data['userData']) unset($data['userData']);


        $data['domain_id'] = $GLOBALS['domain']['_id'];

        if(count($data)>3){
            $data['modify'] = time();

        }

        // статус модерации по умолчанию
        $data['moderateStatus'] = $S['moderateStatus'];

        $isSave = (boolean) $collection->update($params, $data);

        $this->postSaveImg($params['_id'], $params['_id']);

        return$isSave;
    }


    /**
     * Удаление объявления
     * @param $id
     * @return mixed
     */
    public function postDelete($id){
        $collection = \Core\DB::get()->{$this->post};
        return$collection->remove(['_id'=>$id]);
    }


    /**
     * Сохраняем/обновляем список изображений объявления
     * @param $ad_id
     */
    protected function postSaveImg($ad_id, $tmp = null){
        $Files = new \Mastodont\System\Files();
        $FM = new \Core\Fm();

        if($tmp){
            $list = $Files->getDir($tmp);

            // переименовываем каталог
            $dir = UploadsDir.DIRECTORY_SEPARATOR.$this->module.DIRECTORY_SEPARATOR;
            $FM->Rename($dir.$tmp, $dir.$ad_id);

            // переносим записи файлов из временной таблицы в таблицу модуля
            foreach ($list as $F){

                $F['originalUrl'] = str_replace($tmp, $ad_id, $F['originalUrl']);

                foreach ($F['thumbs'] as $k=>$v)
                    $F['thumbs'][$k] = str_replace($tmp, $ad_id, $v);


                if(isset($F['dir'])) unset($F['dir']);
                if(isset($F['isTmp'])) unset($F['isTmp']);
                if(isset($F['module'])) unset($F['module']);

                $fid = $F['_id'];
                unset($F['_id']);

                $F['doc_id'] = $ad_id;

                // переносим записи о файлах из временной таблицы, в таблицу модуля
                $Files->ModuleAdd($this->module, $F);

                // удаляем из временной таблицы
                if($ad_id != $tmp)
                    $Files->delete($fid);
            }
            
        }

    }

    protected function postGetImg($ad_id){
        $Files = new \Mastodont\System\Files();

        return $Files->ModuleGetDocID($this->module, $ad_id);
    }

}