<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 16.02.2016
 * Time: 16:04
 */

namespace Mastodont\Api\v1;

use \JasonGrimes\Paginator;

class Ads
{

    protected
        $config,
        $post = 'adsPosts',
        $cat = 'adsCategory',
        $module = 'ads';

    function __construct(){
        $this->config = $this->setting();
    }

    function get($property)
    {
        if(array_key_exists($property, $this->config))
            return $this->config[$property];
        else
            throw new \Exception('Not found property - ' . $property);
    }

    /**
     * настройки модуля
     * @return mixed
     */
    function Setting(){
        return $GLOBALS['Setting']->get()['modules'][$this->module];
    }

    /**
     * Генерируем дерево категорий
     * @param array $list - список категорий
     * @param int $parent - родительская категория
     * @return array
     */
    private function BuildTree($list=[], $parent=0){
        $arr = [];

        foreach($list as $l){
            if($l['parent_id'] == $parent){
                $l['children'] = $this->BuildTree($list, $l['_id']);

                $arr[] = $l;
            }
        }

        return$arr;
    }

    /**
     * Краткий список категорий
     * @param bool $isTree - метка для формирования дерева категорий
     * @param array $params - параметры для получения данных
     * @param array $fields - поля, которые надо получить
     * @return array
     */
    public function categoryMiniList($isTree=false, $params = [], $fields = []){

        if(count($fields) == 0)
            $fields = [
                'parent_id'=>true,
                'name'=>true,
                'pos'=>true,
                'url'=>true,
                'enabled'=>true,
            ];

        if(isset($_REQUEST['mini']))
            $fields = [
                'parent_id'=>true,
                'name'=>true,
            ];

        if(!isset($fields['parent_id']))
            $fields['parent_id'] = true;

        if(isset($_REQUEST['user']))
            $params = ['enabled'=>true];

        if(isset($_REQUEST['parent_id']))
            $params['parent_id'] = $_REQUEST['parent_id'];

        $params['domain_id'] = $GLOBALS['domain']['_id'];
        $fields['domain_id'] = false;

        $collection = \Core\DB::get()->{$this->cat};
        $list=$collection->find($params, $fields)->toArray();

        if($isTree)
            $list = $this->BuildTree($list);

        return$list;
    }

    /**
     * Получаем список категорий, с полным набором данных
     * @param bool $isTree - метка для формирования дерева категорий
     * @return array
     */
    public function fullList($isTree=false){
        $collection = \Core\DB::get()->{$this->cat};

        $list=$collection->find([
            'enabled'=>true,
            'domain_id' => $GLOBALS['domain']['_id']
        ], ['domain_id' => false])->toArray();

        if($isTree)
            $list = $this->BuildTree($list);

        return$list;
    }

    /**
     * Получаем категорию с дочерними категориями, с построением дерева
     * @param $id
     * @return array
     */
    public function getID($id){
        $list = $this->fullList(false);

        $newArr = [];
        foreach($list as $item){
            if($item['_id'] == $id)
                return $this->BuildTree( $list, $id );//[0]['children']
        }

        return$newArr;
    }

    /**
     * Поиск категории по URL
     * @param $url
     * @return mixed
     */
    public function CategoryFindUrl($url){
        $collection = \Core\DB::get()->{$this->cat};
        $p = [];

        $param = [
            'url'=>$url,
            'domain_id'=>$GLOBALS['domain']['_id']
        ];

        $p = $collection->findOne($param, ['domain_id' => false]);

        if($p['parent_id'] != 0){
            $pr = $this->categoryEdit($p['parent_id']);
            $p['parent'] = [
                '_id'=>$pr['_id'],
                'name'=>$pr['name'],
                'url'=>"/{$this->get('linkModule')}/{$this->get('links')['category']}/{$pr['url']}"
            ];
        }

        return$p;
    }

    /**
     * Получение данных для редактирования категории
     * @param $id
     * @return mixed
     */
    public function categoryEdit($id){
        $collection = \Core\DB::get()->{$this->cat};
        return $collection->findOne(['_id'=>$id], ['domain_id' => false]);
    }

    /**
     * Проверка URL категории, на наличие дубликатов
     * @param $url
     * @param string $locale
     * @return bool
     */
    public function categoryCheckUrl($url, $locale='rus'){
        $collection = \Core\DB::get()->{$this->cat};

        $list = $collection->find([
            'url' => $url,
            'locale' => $locale,
            'domain_id' => $GLOBALS['domain']['_id']
        ], ['_id' => true,'domain_id' => false])->toArray();

        return count($list)>0;
    }

    /**
     * Соханяем категорию
     * @param $data
     * @return bool
     */
    public function categorySave($data){
        $collection = \Core\DB::get()->{$this->cat};

        $data['domain_id'] = $GLOBALS['domain']['_id'];

        return (boolean) $collection->insert($data);
    }

    /**
     * Обновление категории
     * @param $params - параметры для поиска категории
     * @param $data - данные для обновления
     * @return bool
     */
    public function categoryUpdate($params, $data){
        $collection = \Core\DB::get()->{$this->cat};

        $data['domain_id'] = $GLOBALS['domain']['_id'];
        return (boolean) $collection->update($params, $data);
    }

    /**
     * Удаление категории
     * @param $id
     * @return mixed
     */
    public function categoryDelete($id){
        $collection = \Core\DB::get()->{$this->cat};
        return$collection->remove(['_id'=>$id]);
    }

// ---------------------------------------------------------------------------------------------------------------------

    /**
     * Редактирование объявления
     * @param $id
     * @return mixed
     */
    public function postEdit($id){
        $collection = \Core\DB::get()->{$this->post};
        return $collection->findOne(['_id'=>$id], ['domain_id' => false]);
    }

    /**
     * Список объявлений со всеми параметрами
     * @param array $fields
     * @param array $params
     * @return mixed
     */
    public function postFullList(array $fields = [], array $params = [], $sort = 'desc'){
        global$Tpl, $T;

        $params['domain_id'] = $GLOBALS['domain']['_id'];
        $isAdminPane = $Tpl->CheckAdminPanel();


        if(!$isAdminPane)
            $params['enabled'] = true;

        $fields['domain_id'] = false;

        $collection = \Core\DB::get()->{$this->post};

        if($isAdminPane){
            $urlPattern = "/api/v1/ads?type=post&page=(:num)";
            $itemsPerPage = $this->get('inPageAdmin');
        }else{
            $itemsPerPage = $this->get('inPage');
            $params['enabled'] = true;
            $urlPattern = "/{$this->get('linkModule')}/{$this->get('links')['category']}/{$T['module']['item']}?page=(:num)";
        }

        $totalItems = $collection->find($params, $fields)->count();
        $currentPage = (isset($_REQUEST['page']) ? $_REQUEST['page'] : 1)-1;

        $sort = (int)($sort === 'desc' ? -1 : 1);
        $list=$collection->find($params, $fields)
            ->sort(['create' => $sort])
            ->skip($currentPage == 0 ? $currentPage : $currentPage * $itemsPerPage)
            ->limit($itemsPerPage)
            ->toArray();

        foreach ($list as $k=>$v){
            $v['url'] = "/{$this->get('linkModule')}/{$this->get('links')['ad']}/{$v['url']}";

            if(!$isAdminPane)
                $v['imgs'] = $this->postGetImg($v['_id']);

            $list[$k] = $v;
        }

        $paginator = new Paginator($totalItems, $itemsPerPage, $currentPage+1, $urlPattern);
        $paginator->setMaxPagesToShow($Tpl->Paginate['maxPagesToShow']); // кол-во отображаемых страниц в пагинаторе

        return [
            'list'=>$list,
            'paginator'=>$Tpl->Paginator($paginator),
        ];
    }

    /**
     * Список объявлений с кратким набором параметров
     * @param array $params
     * @param array $get
     * @return mixed
     */
    public function postMiniList(array $params = [], array $fields = [], $sort = 'desc'){
        global$Tpl;

        if(count($fields) == 0)
            $fields = [
                'id'=>true,
                '_id'=>true,
                'url'=>true,
                'name'=>true,
                'moderateStatus'=>true,
                'enabled'=>true,
            ];

        $params['domain_id'] = $GLOBALS['domain']['_id'];

        if($Tpl->CheckAdminPanel()){
            $urlPattern = "/api/v1/ads?type=post&page=(:num)";
            $itemsPerPage = $this->get('inPageAdmin');
        }else{
            $params['enabled'] = true;
            $urlPattern = "";
            $itemsPerPage = $this->get('inPage');
        }

        $fields['domain_id'] = false;

        $collection = \Core\DB::get()->{$this->post};

        $totalItems = $collection->find($params, $fields)->count();
        $currentPage = isset($_REQUEST['page']) ? $_REQUEST['page'] : 1;


        $sort = (int)($sort === 'desc' ? -1 : 1);
        $list=$collection->find($params, $fields)
            ->sort(['create' => $sort])
            ->skip($currentPage-1)
            ->limit($itemsPerPage)
            ->toArray();

        foreach ($list as $k=>$v){

            $v['url'] = "/{$this->get('linkModule')}/{$this->get('links')['ad']}/{$v['url']}";
            $list[$k] = $v;
        }



        $paginator = new Paginator($totalItems, $itemsPerPage, $currentPage, $urlPattern);
        $paginator->setMaxPagesToShow($Tpl->Paginate['maxPagesToShow']); // кол-во отображаемых страниц в пагинаторе

        return [
            'list'=>$list,
            'paginator'=>$Tpl->Paginator($paginator),
        ];
    }

    /**
     * Список последних объявлений
     * @param array $params - критерии выборки
     * @return mixed
     */
    public function postLast(array $params = []){
        $params['moderateStatus'] = 'ok';
        $params['enabled'] = true;

        $data = $this->postFullList([], $params);

        foreach ($data['list'] as $a){
            unset($a['moderateStatus']);

            $a['imgs'] = $this->postGetImg($a['_id']);
        }

        return$data;
    }

    public function postCountInCategory($category_id, $params = []){
        if(!isset($category_id) || !$category_id) return 0;

        $params['domain_id'] = $GLOBALS['domain']['_id'];
        $params['category_id'] = $category_id;

        $params['moderateStatus'] = 'ok';
        $params['enabled'] = true;

        $collection = \Core\DB::get()->{$this->post};
        $totalItems = $collection->find($params, [])->count();

        return $totalItems;
    }

    /**
     * Поиск объявления по URL
     * @param $url
     * @return mixed
     */
    public function postFindUrl($url, $isUser = false){

        $collection = \Core\DB::get()->{$this->post};

        $param = [ 'url'=>$url ];
        if($isUser)
            $param = [ '_id'=>$url ];

        $p = $collection->findOne($param, ['domain_id' => false]);

        foreach($p['categoryTree'] as $k=>$C){
            $cat = $this->categoryEdit($C['_id']);
            $p['categoryTree'][$k]['url'] = "/{$this->get('linkModule')}/{$this->get('links')['category']}/{$cat['url']}";
        }

        $p['isEdit'] = (isset($GLOBALS['Tpl']->getUserData()['u']) && $p['user_id'] == $GLOBALS['Tpl']->getUserData()['u']);

        $p['imgs'] = $this->postGetImg($p['_id']);

        $User = new \Mastodont\Api\v1\Users();
        $u = $User->getID($p['user_id']);

        if(isset($u['balans'])) unset($u['balans']);

        $p['userData'] = $u;

        return$p;
    }

    /**
     * Последние объявления пользователя
     * @param $user_id
     * @param int $count
     * @return mixed
     */
    public function postUserLast($user_id, $count = 1){

        $collection = \Core\DB::get()->{$this->post};
        $list = $collection->find(
            [
                'user_id'=>$user_id,
                'moderateStatus'=>'published'
            ],
            ['domain_id' => false])
            ->sort(['create' => -1])
            ->skip(0)
            ->limit($count)
            ->toArray();

        foreach ($list as $k=>$v){

            $v['url'] = "/{$this->get('linkModule')}/{$this->get('links')['ad']}/{$v['url']}";
            $list[$k] = $v;
        }

        return$list;
    }

    // получаем список объявлений по статусу
    public function postGetStatus($user_id, $status, $count = 5){

        $collection = \Core\DB::get()->{$this->post};
        $list = $collection->find(
            [
                'user_id'=>$user_id,
                'moderateStatus'=>$status
            ],
            ['domain_id' => false])
            ->sort(['create' => -1])
            ->skip(0)
            ->limit($count)
            ->toArray();

        foreach ($list as $k=>$v){

            $v['url'] = "/{$this->get('linkModule')}/{$this->get('links')['ad']}/{$v['url']}";
            $v['imgs'] = $this->postGetImg($v['_id']);

            $list[$k] = $v;
        }

        return$list;
    }

    /**
     * Сохранение объявления
     * @param $data
     * @return bool
     */
    public function postSave($data){
        $collection = \Core\DB::get()->{$this->post};

        $tmp = $data['tmp'];
        unset($data['tmp']);

        $data['id'] = $GLOBALS['Core']->Counter('ads', 'post');
        $data['create'] = $data['modify'] = time();
        $data['domain_id'] = $GLOBALS['domain']['_id'];

        $data['status'] = 'published';


        // статус модерации по умолчанию
        $data['moderateStatus'] = $this->get('moderateStatus');

        //дата окончания показа, для платнх услуг
        $data['isViewTime'] = false;
        $data['viewTime'] = 0;

        // рейтинг объявления
        $data['rating'] = [
            'plus'=>0,
            'minus'=>0
        ];

        $data['enabled'] = true;

        $isSave = (boolean) $collection->insert($data);

        $data['url'] = $GLOBALS['Core']->Translit($data['name']).'-'.$GLOBALS['Security']->getUUID();
        $data['url'] = str_replace([' '], '-', $data['url']);

        $collection->update(['_id'=>$data['_id']], ['url'=>$data['url']]);

        $this->postSaveImg($data['_id'], $tmp);

        return$isSave;
    }

    /**
     * Обновление объявления
     * @param $params
     * @param $data
     * @return bool
     */
    public function postUpdate($params, $data){
        $collection = \Core\DB::get()->{$this->post};

        $tmp = $data['tmp'];
        unset($data['tmp']);


        if($data['_id']) unset($data['_id']);
        if($data['isEdit']) unset($data['isEdit']);
        if($data['imgs']) unset($data['imgs']);
        if($data['userData']) unset($data['userData']);


        $data['domain_id'] = $GLOBALS['domain']['_id'];

        if(count($data)>3){
            $data['modify'] = time();

            // статус модерации по умолчанию
            $data['moderateStatus'] = $this->get('moderateStatus');
        }

        $isSave = (boolean) $collection->update($params, $data);

        if($isSave){
            //todo отправляем мыло пользователю, добавляем уведомление в ЛК
        }

        if(count($data)>3)
            $this->postSaveImg($params['_id'], $params['_id']);

        return$isSave;
    }


    /**
     * Удаление объявления
     * @param $id
     * @return mixed
     */
    public function postDelete($id){
        $collection = \Core\DB::get()->{$this->post};
        return$collection->remove(['_id'=>$id]);
    }


    /**
     * Сохраняем/обновляем список изображений объявления
     * @param $ad_id
     */
    protected function postSaveImg($ad_id, $tmp = null){
        $Files = new \Mastodont\System\Files();
        $FM = new \Core\Fm();

        if($tmp){
            $list = $Files->getDir($tmp);

            // переименовываем каталог
            $dir = UploadsDir.DIRECTORY_SEPARATOR.$this->module.DIRECTORY_SEPARATOR;
            $FM->Rename($dir.$tmp, $dir.$ad_id);

            // переносим записи файлов из временной таблицы в таблицу модуля
            foreach ($list as $F){

                $F['originalUrl'] = str_replace($tmp, $ad_id, $F['originalUrl']);

                foreach ($F['thumbs'] as $k=>$v)
                    $F['thumbs'][$k] = str_replace($tmp, $ad_id, $v);


                if(isset($F['dir'])) unset($F['dir']);
                if(isset($F['isTmp'])) unset($F['isTmp']);
                if(isset($F['module'])) unset($F['module']);

                $fid = $F['_id'];
                unset($F['_id']);

                $F['doc_id'] = $ad_id;

                // переносим записи о файлах из временной таблицы, в таблицу модуля
                $Files->ModuleAdd($this->module, $F);

                // удаляем из временной таблицы
                $Files->delete($fid, (strcasecmp($ad_id, $tmp) === 0 ? false : true));

            }

        }

    }

    protected function postGetImg($ad_id){
        $Files = new \Mastodont\System\Files();

        return $Files->ModuleGetDocID($this->module, $ad_id);
    }

}