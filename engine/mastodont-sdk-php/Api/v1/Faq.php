<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 10.02.2016
 * Time: 7:45
 */

namespace Mastodont\Api\v1;


class Faq
{
    protected
        $module = 'faq',
        $post = 'faqPost',
        $category = 'faqCategory';

    function __construct(){

    }

    public function FullTree(){
        $locale = $GLOBALS['Core']->getCookie('mst_locale');
        $t = $this->category;
        $collection = \Core\DB::get()->$t;
        $Clist=$collection->find([
            'enabled'=>true,
            'locale'=>$locale,
            'domain_id'=>$GLOBALS['domain']['_id']
        ], ['_id'=>true, 'name'=>true, 'domain_id'=>false])->toArray();

        $t = $this->post;
        $collection = \Core\DB::get()->$t;
        $Plist=$collection->find([
            'enabled'=>true,
            'locale'=>$locale,
            'domain_id'=>$GLOBALS['domain']['_id']
        ], ['locale'=>false, 'enabled'=>false, 'domain_id'=>false])->toArray();

        $newArr = [];
        foreach($Clist as $C){
            $C['list'] = [];

            foreach ($Plist as $P)
                if($C['_id'] == $P['category_id'])
                    $C['list'][] = $P;

            $newArr[] = $C;
        }

        return$newArr;
    }



    public function categoryFullList($params = []){
        $t = $this->category;
        $collection = \Core\DB::get()->{$t};

        $params['domain_id'] = $GLOBALS['domain']['_id'];
        $list=$collection->find($params, ['domain_id'=>false])->toArray();
        return ($list ? $list : []);
    }

    public function categoryMiniList($params = []){
        $t = $this->category;
        $collection = \Core\DB::get()->$t;

        $params['domain_id'] = $GLOBALS['domain']['_id'];
        $list=$collection->find($params, [
            'name'=>true,
            'enabled'=>true,
            'domain_id'=>false
        ])->toArray();

        return ($list ? $list : []);
    }

    public function categoryEdit($id){
        $t = $this->category;
        $collection = \Core\DB::get()->{$t};
        $m=$collection->findOne(['_id'=>$id], ['_id'=>false]);
        return$m;
    }

    public function categorySave($data){
        $t = $this->category;
        $collection = \Core\DB::get()->$t;

        $data['domain_id'] = $GLOBALS['domain']['_id'];
        return(boolean)$collection->insert($data);
    }

    public function categoryUpdate($params, $data){
        $t = $this->category;
        $collection = \Core\DB::get()->$t;

        $data['domain_id'] = $GLOBALS['domain']['_id'];
        return$collection->update($params, $data);
    }

    public function categoryDelete($id){
        $t = $this->category;
        $collection = \Core\DB::get()->$t;
        return (boolean)$collection->remove(['_id'=>$id]);
    }

    public function postFullList($params = []){
        $t = $this->post;
        $collection = \Core\DB::get()->$t;

        $params['domain_id'] = $GLOBALS['domain']['_id'];
        $list=$collection->find($params, ['domain_id'=>false])->toArray();
        return ($list ? $list : []);
    }

    public function postMiniList($params = []){
        $t = $this->post;
        $collection = \Core\DB::get()->$t;

        $params['domain_id'] = $GLOBALS['domain']['_id'];
        $list=$collection->find($params, [
            'name'=>true,
            'enabled'=>true,
            'domain_id'=>false
        ])->toArray();

        return ($list ? $list : []);
    }

    public function postEdit($id){
        $t = $this->post;
        $collection = \Core\DB::get()->$t;
        $m=$collection->findOne(['_id'=>$id], ['_id'=>false]);
        return$m;
    }

    public function postSave($data){
        $t = $this->post;
        $collection = \Core\DB::get()->$t;

        $data['domain_id'] = $GLOBALS['domain']['_id'];
        return(boolean)$collection->insert($data);
    }

    public function postUpdate($params, $data){
        $t = $this->post;
        $collection = \Core\DB::get()->$t;

        $data['domain_id'] = $GLOBALS['domain']['_id'];
        return$collection->update($params, $data);
    }

    public function postDelete($id){
        $t = $this->post;
        $collection = \Core\DB::get()->$t;
        return (boolean)$collection->remove(['_id'=>$id]);
    }
}