<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 25.01.2016
 * Time: 8:10
 */

namespace Mastodont\Api\v1;


class Groups
{
    protected
        $module = 'groups';
    
    public function loadList(){
        $collection = \Core\DB::get()->groups;
        $list = $collection->find([
            'domain_id'=>$GLOBALS['domain']['_id']
        ], [
            '_id'=>true,
            'name'=>true,
            'sys_name'=>true,
            'enabled'=>true,
            'protect'=>true,
            'domain_id'=>false
        ])->toArray();

        return$list;
    }

    public function miniList(){
        $collection = \Core\DB::get()->groups;
        $list = $collection->find([
            'enabled'=>true,
            'domain_id'=>$GLOBALS['domain']['_id']
        ], [
            '_id'=>true,
            'name'=>true,
            'sys_name'=>true,
            'domain_id'=>false
        ])->toArray();

        return$list;
    }

    public function FindName($sysName = 'users'){
        $collection = \Core\DB::get()->groups;
        $list = $collection->findOne([
            'sys_name'=>$sysName,
            'domain_id'=>$GLOBALS['domain']['_id']
        ], [ 'domain_id'=>false ]);

        return$list;
    }

    public function edit($id){
        $collection = \Core\DB::get()->groups;
        $d=$collection->findOne(['_id'=>$id]);
        unset($d['_id']);
        return$d;
    }

    public function save($data){
        $collection = \Core\DB::get()->groups;

        $data['domain_id'] = $GLOBALS['domain']['_id'];
        return (boolean)$collection->insert($data);
    }

    public function update($params, $data){
        $collection = \Core\DB::get()->groups;

        $data['domain_id'] = $GLOBALS['domain']['_id'];
        return$collection->update($params, $data);
    }

    public function delete($id){
        $collection = \Core\DB::get()->groups;
        return (boolean)$collection->remove(['_id'=>$id]);
    }

    public function CheckPermitions(array $groups = []){

        $list = [];
        foreach ($groups as $k=>$v){
            $P = $this->edit($v);
            $list[$P['sys_name']] = (!$P['is_ban'] && $P['enabled'] && $P['access_cp'] && $P['moderate']);
        }
        return$list;
    }
}