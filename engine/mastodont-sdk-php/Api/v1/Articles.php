<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 16.02.2016
 * Time: 16:04
 */

namespace Mastodont\Api\v1;

use \JasonGrimes\Paginator;

class Articles
{
    protected
        $module = 'articles';
    
    function __construct(){
    }

    private function BuildTree($list=[], $parent=0){
        $arr = [];

        foreach($list as $l){
            if($l['parent_id'] == $parent){
                $l['children'] = $this->BuildTree($list, $l['_id']);

                $arr[] = $l;
            }
        }

        return$arr;
    }

    private function BuildTreeOptions($list=[], $parent=0, $level_delim = ''){
        $arr = $child = [];
        $num = count($list);
        for ($i = 0; $i < $num; $i++) {
            if ($list[$i]['parent_id'] == $parent) {
                $arr[] = ['_id' => $list[$i]['_id'], 'name' => $level_delim . $list[$i]['name']];
                $child = $this->BuildTreeOptions($list, $list[$i]['_id'], $level_delim . '-');
            }
        }

        $arr = array_merge($arr, $child);
        return $arr;
    }

    public function categoryMiniList($isTree=false, $isOptions = false){
        $collection = \Core\DB::get()->articlesCategory;
        $list=$collection->find(
            [
                'domain_id'=>$GLOBALS['domain']['_id']
            ],
            [
                'parent_id'=>true,
                'name'=>true,
                'pos'=>true,
                'url'=>true,
                'enabled'=>true,
            ])->toArray();

        if($isTree){
            if($isOptions)
                $list = $this->BuildTreeOptions($list);
            else
                $list = $this->BuildTree($list);
        }
        
        return$list;
    }

    public function fullList($isTree=false){
        $collection = \Core\DB::get()->articlesCategory;
        $list=$collection->find([
            'enabled'=>true,
            'domain_id'=>$GLOBALS['domain']['_id']
        ], ['domain_id'=>false])->toArray();

        if($isTree)
            $list = $this->BuildTree($list);

        return$list;
    }

    public function getID($id){
        $list = $this->fullList(false);

        $newArr = [];
        foreach($list as $item){
            if($item['_id'] == $id)
                return $this->BuildTree( $list, $id );//[0]['children']
        }

        return$newArr;
    }

    public function categoryEdit($id){
        $collection = \Core\DB::get()->articlesCategory;
        return $collection->findOne(['_id'=>$id]);
    }

    public function categoryCheckUrl($url, $locale='rus'){
        $collection = \Core\DB::get()->articlesCategory;
        $list = $collection->find([
            'url'=>$url,
            'locale'=>$locale,
            'domain_id'=>$GLOBALS['domain']['_id']
        ], ['_id'=>true, 'domain_id'=>false])->toArray();

        return count($list)>0;
    }

    public function categorySave($data){
        $collection = \Core\DB::get()->articlesCategory;
        if(!isset($data['locale'])) $data['locale'] = 'rus';

        $data['domain_id'] = $GLOBALS['domain']['_id'];
        return (boolean) $collection->insert($data);
    }

    public function categoryUpdate($params, $data){
        $collection = \Core\DB::get()->articlesCategory;
        if(!isset($data['locale'])) $data['locale'] = 'rus';

        $data['domain_id'] = $GLOBALS['domain']['_id'];
        return (boolean) $collection->update($params, $data);
    }

    public function categoryDelete($id){
        $collection = \Core\DB::get()->articlesCategory;
        return$collection->remove(['_id'=>$id]);
    }

    public function CategoryFindUrl($url, $locale = null){
        $locale = !$locale ? $GLOBALS['Core']->getCookie('mst_locale') : $locale;
        $collection = \Core\DB::get()->articlesCategory;
        $r = $collection->findOne(['url'=>$url, 'locale'=>$locale, 'domain_id'=>$GLOBALS['domain']['_id']], ['domain_id'=>false]);
        return$r;
    }

    //------------------------------------------------------------------------------------------------------------------

    public function postMiniList($params = [], $get = [], $limit = 1000){
        $collection = \Core\DB::get()->articlesPost;

        if(count($get) == 0)
            $get = [
                'name'=>true,
                'url'=>true,
                'enabled'=>true,
            ];

        $params['domain_id'] = $GLOBALS['domain']['_id'];
        $get['domain_id'] = false;

        $list=$collection->find($params, $get)
            ->sort(['create'=>-1])
            ->limit($limit)
            ->toArray();

        return$list;
    }

    public function postEdit($id){
        $collection = \Core\DB::get()->articlesPost;
        return $collection->findOne(['_id'=>$id]);
    }

    public function postCheckUrl($url, $locale='rus'){
        $collection = \Core\DB::get()->articlesPost;

        $list = $collection->find([
            'url'=>$url,
            'locale'=>$locale,
            'domain_id'=>$GLOBALS['domain']['_id']
        ], ['_id'=>true, 'domain_id'=>false])->toArray();

        return count($list)>0;
    }

    public function postSave($data){
        $collection = \Core\DB::get()->articlesPost;

        if(!isset($data['locale'])) $data['locale'] = 'rus';

        $data['create'] = $data['modify'] = time();
        $data['domain_id'] = $GLOBALS['domain']['_id'];
        return (boolean) $collection->insert($data);
    }

    public function postUpdate($params, $data){
        $collection = \Core\DB::get()->articlesPost;

        if(!isset($data['locale'])) $data['locale'] = 'rus';

        $data['modify'] = time();
        $data['domain_id'] = $GLOBALS['domain']['_id'];
        return (boolean) $collection->update($params, $data);
    }

    public function postDelete($id){
        $collection = \Core\DB::get()->articlesPost;
        return$collection->remove(['_id'=>$id]);
    }

    public function postFindUrl($url, $locale = 'rus'){
        $locale = !$locale ? $GLOBALS['Core']->getCookie('mst_locale') : $locale;
        $collection = \Core\DB::get()->articlesPost;
        $r = $collection->findOne(['url'=>$url, 'locale'=>$locale,'domain_id'=>$GLOBALS['domain']['_id']],['domain_id'=>false]);
        return$r;
    }

    public function TagFindUrl($url, $locale = 'rus'){
        $locale = !$locale ? $GLOBALS['Core']->getCookie('mst_locale') : $locale;
        $collection = \Core\DB::get()->articlesTags;
        $r = $collection->findOne(['url'=>$url, 'locale'=>$locale,'domain_id'=>$GLOBALS['domain']['_id']],['domain_id'=>false]);
        return$r;
    }

    public function ModuleMainPage(){
        global$Tpl, $Setting, $domain;

        $C = $Setting->get()['modules']['articles'];

        $collection = \Core\DB::get()->articlesPost;
        $totalItems = $collection->find(['enabled'=>true,'domain_id'=>$domain['_id']],['domain_id'=>false])->count();

        $currentPage = isset($_REQUEST['page']) ? $_REQUEST['page'] : 1;
        $urlPattern = '/'.$C['linkModule'].'?page=(:num)';
        $itemsPerPage = $C['inPage'];

        $list = $collection->find(
            [
                'enabled' => true,
                'domain_id'=>$domain['_id']
            ],
            [
                'name' => true,
                'url' => true,
                'anonce' => true,
                'create' => true,
                'domain_id'=>false,
            ])
            ->sort(['create' => -1])
            ->skip($currentPage-1)
            ->limit($itemsPerPage)
            ->toArray();

        $paginator = new Paginator($totalItems, $itemsPerPage, $currentPage, $urlPattern);
        $paginator->setMaxPagesToShow($Tpl->Paginate['maxPagesToShow']); // кол-во отображаемых страниц в пагинаторе
        
        return [
            'list'=>$list,
            'paginator'=>$Tpl->Paginator($paginator),
        ];
    }
}