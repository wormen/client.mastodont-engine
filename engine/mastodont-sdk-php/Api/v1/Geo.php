<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 11.02.2016
 * Time: 12:08
 */

namespace Mastodont\Api\v1;


class Geo
{
    protected
        $module = 'geo';
    
    function __construct(){
    }

    public function init($district_id=4){

        $data = [
            'districts'=>$this->getDistricts(),
            'regions'=>$this->getDistrictRegions($district_id),
            'cityList'=>$this->getDistrictCity($district_id),
        ];

        return$data;
    }

    /**
     * Список всех округов
     * @return mixed
     */
    public function getDistricts(){
        $collection = \Core\DB::get('GDB')->district;
        return$collection->find([], ['_id'=>false])->sort(["title"=>'ask'])->toArray();
    }

    /**
     * Регионы округа
     * @param int $district_id
     * @return mixed
     */
    public function getDistrictRegions($district_id=0){
        $collection = \Core\DB::get('GDB')->regions;
        $r = $collection->find(['district_id'=>$district_id], ['_id'=>false])->sort(["title"=>'ask'])->toArray();
        return ($r ? $r : []);
    }

    /**
     * Города округа
     * @param int $district_id
     * @return mixed
     */
    public function getDistrictCity($district_id=0){
        $collection = \Core\DB::get('GDB')->city;
        $r = $collection->find(['district_id'=>$district_id], ['_id'=>false])->sort(["title"=>'ask'])->toArray();
        return ($r ? $r : []);
    }

    /**
     * Города региона
     * @param int $region_id
     * @return mixed
     */
    public function getRegionCity($region_id=0){
        $collection = \Core\DB::get('GDB')->city;
        $r = $collection->find(['region_id'=>$region_id], ['_id'=>false])->sort(["title"=>'ask'])->toArray();
        return ($r ? $r : []);
    }

    public function getDistrictID($id){
        $collection = \Core\DB::get('GDB')->district;
        $r = $collection->findOne(['id'=>$id], ['_id'=>false]);
        return ($r ? $r : []);
    }

    public function getRegionID($id){
        $collection = \Core\DB::get('GDB')->regions;
        $r = $collection->findOne(['id'=>$id], ['_id'=>false]);
        return ($r ? $r : []);
    }

    public function getCityID($id){
        $collection = \Core\DB::get('GDB')->city;
        $r = $collection->findOne(['id'=>$id], ['_id'=>false]);
        return ($r ? $r : []);
    }

    public function getData(){
        $s = $GLOBALS['Core']->getCookie('mst_geo');
        try{

            if($s){
                $arr = [
                    'allCountry'=>true,
                    'isCountry'=>true,
                    'isDistrict'=>false,
                    'isRegion'=>false,
                    'isCity'=>false,
                    'districtName'=>null
                ];

                $G = json_decode( base64_decode($s), true);
                $G = (object) array_merge(($G ? $G : []) , $arr);

                if($G->district_id == 0 || $G->region_id == 0 || $G->city_id == 0)
                    $G->allCountry = true;
                else
                    $G->isCountry = false;

                if($G->district_id>0){
                    $G->districtName = $this->getDistrictID($G->district_id)['title'];
                    $G->isDistrict = true;
                }

                if($G->region_id>0){
                    $G->regionName = $this->getRegionID($G->region_id)['title'];
                    $G->isDistrict = false;
                    $G->isRegion = true;
                }

                if($G->city_id>0 && $G->allRegionCity == false){
                    $G->cityName = $this->getCityID($G->city_id)['title'];
                    $G->isDistrict = false;
                    $G->isRegion = false;
                    $G->isCity = true;
                }

                return$G;
            }
            else
                return false;
        }catch(Exception $e){
            return false;
        }
    }
}