<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 17.02.2016
 * Time: 18:07
 */

namespace Mastodont\Api\v1;

use \JasonGrimes\Paginator;

class Comments
{
    protected
        $module = 'comments';
    
    function __construct(){
    }

    public function Add($data = []){
        $collection = \Core\DB::get('MSGDB')->comments;
    }

    // для админки
    public function fullList(){
        global$Tpl, $Setting;
        
        //todo проверить откуда запрос

        $C = $Setting->get()['modules']['comments'];

        $collection = \Core\DB::get('MSGDB')->comments;
        $totalItems = $collection->find([])->count();

        $currentPage = isset($_REQUEST['page']) ? $_REQUEST['page'] : 1;
        $urlPattern = '/'.$C['linkModule'].'?page=(:num)';
        $itemsPerPage = $C['AdminInPage'];

        $list = $collection->find(
            [],
            [
                'msg' => true,
                'module' => true,
                'section' => true,
                'create' => true,
            ])
            ->sort(['create' => -1])
            ->skip($currentPage-1)
            ->limit($itemsPerPage)
            ->toArray();

        $paginator = new Paginator($totalItems, $itemsPerPage, $currentPage, $urlPattern);
        $paginator->setMaxPagesToShow($Tpl->Paginate['maxPagesToShow']); // кол-во отображаемых страниц в пагинаторе
        
        return [
            'list'=>$list,
            'paginator'=>$Tpl->Paginator($paginator),
        ];
    }

    public function getList($params = []){
        $collection = \Core\DB::get('MSGDB')->comments;
    }

    public function update($id, $data = []){
        $collection = \Core\DB::get('MSGDB')->comments;
        return$collection->update(['_id'=>$id], $data);
    }

    public function delete($id){
        $collection = \Core\DB::get('MSGDB')->comments;
        return (boolean)$collection->remove(['_id'=>$id]);
    }
}