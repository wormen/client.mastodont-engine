<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 10.04.2016
 * Time: 17:48
 */

namespace Mastodont\External;


class Youtube
{
    protected
        $config,
        $Youtube,
        $module = 'video';
    
    function __construct(){
        $this->config = $this->Setting();



        $this->Youtube = new \Madcoda\Youtube([
            'key'=>$this->get('youtubeAPI'),
        ]);
    }

    function get($property)
    {
        if(array_key_exists($property, $this->config))
            return $this->config[$property];
        else
            throw new \Exception('Not found property - ' . $property);
    }

    /**
     * настройки модуля
     * @return array
     */
    function Setting(){
        return $GLOBALS['Setting']->get()['modules'][$this->module];
    }


    public function getVideoInfo($id){
        return$this->Youtube->getVideoInfo($id);
    }


//----------------------------------------------------------------------------------------------------------------------



}