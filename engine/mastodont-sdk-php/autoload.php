<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 20.01.2016
 * Time: 19:16
 */


if ($files = glob(implode(DIRECTORY_SEPARATOR, [__DIR__, 'Api', 'v1', '*.php'])))
    foreach($files as $f)
        require_once $f;

if ($files = glob(implode(DIRECTORY_SEPARATOR, [__DIR__, 'ApiClient', 'v1', '*.php'])))
    foreach($files as $f)
        require_once $f;

if ($files = glob(implode(DIRECTORY_SEPARATOR, [__DIR__, 'System', '*.php'])))
    foreach($files as $f)
        require_once $f;

if ($files = glob(implode(DIRECTORY_SEPARATOR, [__DIR__, 'External', '*.php'])))
    foreach($files as $f)
        require_once $f;