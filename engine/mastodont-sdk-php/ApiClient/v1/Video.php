<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 09.04.2016
 * Time: 15:50
 */


namespace Mastodont\ApiClient\v1;

require_once __DIR__ .'/../Sender.php';
use \Mastodont\ApiClient\Sender;

class Video
{

    protected
        $Sender,
        $config,
        $module = 'video';

    function __construct(){
        $this->config = $this->Setting();
        $this->Sender = new Sender('v1', $this->get('token'));
    }

    function get($property)
    {
        if(array_key_exists($property, $this->config))
            return $this->config[$property];
        else
            throw new \Exception('Not found property - ' . $property);
    }

    /**
     * настройки модуля
     * @return array
     */
    function Setting(){
        return $GLOBALS['Setting']->get()['modules'][$this->module];
    }

// ---------------------------------------------------------------------------------------------------------------------
//  Работа с локальными данными



// ---------------------------------------------------------------------------------------------------------------------
//  Обработка данных



// ---------------------------------------------------------------------------------------------------------------------
//  Отправка запросов в API

    /**
     * Получение тематики по ID
     * @param $id
     * @return mixed
     */
    public function getSubject($id){
        return $this->Sender->get($this->module, 'getSubject', $id);
    }

    /**
     * Получение списка тематик
     * @return mixed
     */
    public function getSubjectList(){
        return $this->Sender->get($this->module, 'getSubjectList');
    }

    /**
     * Получение списка видео роликов
     * @param array $params - параметры зарпоса
     * @return mixed
     */
    public function getList($params = []){
        if(!array_key_exists('limit', $params))
            $params['limit'] = $this->get('inPage');

        return $this->Sender->get($this->module, 'getList', null, $params);
    }

    /**
     * Получение списка категорий
     * @param array $params
     * @return mixed
     */
    public function getCategoryList($params = []){
        return $this->Sender->get($this->module, 'getCategoryList', null, $params);
    }

    /**
     * Возвращает данные категории
     * @param $id
     * @return mixed
     */
    public function getCategory($id){
        return $this->Sender->get($this->module, 'getCategory', $id);
    }


}