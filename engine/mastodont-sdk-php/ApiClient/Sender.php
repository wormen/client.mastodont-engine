<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Отправка запросов в API системы Mastodont CMS
 */

namespace Mastodont\ApiClient;


class Sender
{
    protected
        $debug,
        $version, // используемая версия API
        $token = null, // токен для запросов
        $URL = 'https://api.mastodont-engine.ru',
        $URL_debug = 'http://localhost:11052',
        $USERAGENT = 'Mastodont ApiClient';

    function __construct($version = null, $token = null, $debug = false){
        if(!$version)
            throw new \InvalidArgumentException('Not specified API version');

//        if(!$token)
//            throw new \InvalidArgumentException('Invalid token');
        
        $this->version = $version;
        $this->token = $token;
        $this->debug = $debug;
    }

    // проверка валидности токена
    function checkValidToken(){
        // проверяем наличие файла состояния токена
        // запрашиваем данные о токене каждые N часов
        // проверяем валидность
        // проверяем, может ли этот токен работать с текущим прложением
        // сохраняем состояние в файл
    }

    /**
     * Запрос на получение данных
     * @param $module - название модуля
     * @param $method - название метода
     * @param null|string|integer $id
     * @param array $params - url query string
     * @return mixed
     */
    public function get($module, $method, $id = null, $params = [])
    {
        $id = $id ? "/{$id}" : '';
        $params = $this->checkParams($params);
        $apiUrl = $this->debug ? $this->URL_debug : $this->URL;

        $url = "{$apiUrl}/{$this->version}/{$module}/{$method}{$id}{$params}";
        return $this->getData($url);
    }

    /**
     * запрос на сохранение данных
     * @param array $data - данные для сохранения
     * @param $module - название модуля
     * @param $method - название метода
     * @param array $params - url query string
     * @return array|object|string
     */
    public function save(array $data, $module, $method, $params = [])
    {
        $params = $this->checkParams($params);
        $url = "{$this->URL}/{$this->version}/{$module}/{$method}{$params}";
        return $this->saveData($url, $data);
    }

    /**
     * Запрос на удаление данных
     * @param array $data - данные для обновления
     * @param $module - название модуля
     * @param $method - название метода
     * @param null|string|integer $id
     * @param array $params - url query string
     * @return array|object|string
     */
    public function Put(array $data, $module, $method, $id = null, $params = [])
    {
        $id = $id ? "/{$id}" : '';
        $params = $this->checkParams($params);
        $url = "{$this->URL}/{$this->version}/{$module}/{$method}{$id}{$params}";
        return $this->putData($url, $data);
    }

    /**
     * Запрос на удаление данных
     * @param $module - название модуля
     * @param $method - название метода
     * @param null|string|integer $id
     * @param array $params - url query string
     * @return array|object|string
     */
    public function delete($module, $method, $id = null, $params = [])
    {
        $id = $id ? "/{$id}" : '';
        $params = $this->checkParams($params);
        $url = "{$this->URL}/{$this->version}/{$module}/{$method}{$id}{$params}";
        return $this->deleteData($url);
    }



    /**
     * Генерирование строки параметров
     * @param $params
     * @return string
     */
    protected function checkParams($params){
        $params['token'] = $this->token;
        return '?'.http_build_query($params);
    }

    /**
     * Отправка запроса на получение данных
     * @param $url
     * @return array|object|string
     */
    protected function getData($url){

        $defaults = [
            CURLOPT_URL => $url,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_USERAGENT => $this->USERAGENT,
            CURLOPT_SSL_VERIFYPEER => false
        ];

        $ch = curl_init();
        curl_setopt_array($ch, $defaults);

        $output = curl_exec($ch);
        curl_close($ch);

        return $this->checkOutput($output);

    }

    /**
     * Сохранение данных
     * @param $url
     * @param array $data
     * @return array|object|string
     */
    protected function saveData($url, array $data){
        $defaults = [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_USERAGENT => $this->USERAGENT,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => http_build_query($data),
            CURLOPT_SSL_VERIFYPEER => false
        ];

        $ch = curl_init();
        curl_setopt_array($ch, $defaults);

        $output = curl_exec($ch);
        curl_close($ch);

        return $this->checkOutput($output);
    }

    /**
     * Обновление данных
     * @param $url
     * @param array $data
     * @return array|object|string
     */
    protected function putData($url, array $data){
        $defaults = [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_USERAGENT => $this->USERAGENT,
            CURLOPT_CUSTOMREQUEST => 'PUT',
            CURLOPT_POSTFIELDS => http_build_query($data),
            CURLOPT_SSL_VERIFYPEER => false
        ];

        $ch = curl_init();
        curl_setopt_array($ch, $defaults);

        $output = curl_exec($ch);
        curl_close($ch);

        return $this->checkOutput($output);
    }

    /**
     * Удаление данных
     * @param $url
     * @return array|object|string
     */
    protected function deleteData($url){
        $defaults = [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_USERAGENT => $this->USERAGENT,
            CURLOPT_CUSTOMREQUEST => 'DELETE',
            CURLOPT_SSL_VERIFYPEER => false
        ];

        $ch = curl_init();
        curl_setopt_array($ch, $defaults);

        $output = curl_exec($ch);
        curl_close($ch);

        return $this->checkOutput($output);
    }



    /**
     * Преобразование выходных данных
     * @param string $str
     * @return array|object|string
     */
    protected function checkOutput($str){

        if (strlen($str) > 0) {
            $s = substr($str, 0, 1);

            // проверяем, является ли строка объектом или массивом
            if (strcasecmp('{', $s) == 0 || strcasecmp('[', $s) == 0)
                $str = json_decode($str);
        }

        return$str;
    }
}