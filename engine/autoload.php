<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 20.01.2016
 * Time: 19:16
 */

$path = [
    implode(DIRECTORY_SEPARATOR, [EngineDir, 'events', '*.php']),
    EngineDir.DIRECTORY_SEPARATOR.'Cli.php',
    CoreDir.DIRECTORY_SEPARATOR.'*.php',
    implode(DIRECTORY_SEPARATOR, [EngineDir, 'cli', '*.php']),
    implode(DIRECTORY_SEPARATOR, [EngineDir, 'payment', '*.php']),
    SdkDir.DIRECTORY_SEPARATOR.'autoload.php',
];

foreach($path as $p){
    if($files=glob($p))
        if(count($files)>0){
            foreach($files as $f)
                require_once $f;
        }

    else
        require_once $p;
}
