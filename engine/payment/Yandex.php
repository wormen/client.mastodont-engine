<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 13.03.2016
 * Time: 12:01
 */

namespace Mastodont\Api\v1\Payment;


class Yandex
{

    protected
        $config,
        $user_id,
        $shopPassword,
        $Base,
        $system = 'yandex';

    function __construct(){
        $this->Base = new \Mastodont\Api\v1\Payment\Index();

        $this->config = $this->Base->Config();
        $this->user_id = $this->Base->UserID();
        
        $this->shopPassword = $this->config['shopPassword'];
    }

    /**
     *  Получаем ссылку для совершения платежа
     * @param $data
     */
    public function getLink($data){

        $data['payment_satus'] = null;
        $data['isTest'] = $this->config['isTest'];

        //сохраняем данные
        $uid = $this->user_id;
        $data['user_id'] = $uid;
        $id = $this->Base->preSave($data);

        $query = [
            'scid'=>$this->config['scid'],
            'ShopID'=>$this->config['shopId'],
//            'shopArticleId',
            'CustomerNumber'=>$uid,//Идентификатор клиента/Номер заказа
            'Sum'=>$data['pay_summ'], //Сумма
            'paymentType'=>$data['payment_method'], //Способ оплаты
            'shopSuccessURL'=>$this->Base->successURL.'&system='.$this->system,
            'shopFailURL'=>$this->Base->failURL.'&system='.$this->system,
            'history_id'=>$id, // ID транзакции
        ];

        if(isset($data['email'])) $query['cps_email'] = $data['email'];
        if(isset($data['phone'])) $query['cps_phone'] = $data['phone'];
        if(isset($data['fio'])) $query['custName'] = $data['fio'];
        if(isset($data['address'])) $query['custAddr'] = $data['address'];
        if(isset($data['descr'])) $query['orderDetails'] = $data['descr'];

        $query = http_build_query($query);
        $url = "{$this->config['formAction'][$this->config['isTest'] ? 'test' : 'production']}?{$query}";

        return [
            'type'=>'go',
            'link'=>$url
        ];
    }


    protected function checkMD5($data){
        $hash = [];
        $F = ['action', 'orderSumAmount', 'orderSumCurrencyPaycash', 'orderSumBankPaycash', 'shopId', 'invoiceId', 'customerNumber'];

        foreach ($F as $f)
            $hash[] = $data[$f];

        $hash[] = $this->shopPassword;

        $hash = strtoupper(md5(implode(';', $hash)));
        $md5 = strtoupper($hash);

        return $hash == $md5 ? true : false;
    }
}