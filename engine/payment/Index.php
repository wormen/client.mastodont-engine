<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 13.03.2016
 * Time: 19:53
 */

namespace Mastodont\Api\v1\Payment;


class Index
{

    protected
        $config;
    
    public
        $failURL = '/payment?status=fail',
        $successURL = '/payment?status=success';

    /**
     * Получаем конфиг платежной системы
     * @return mixed
     */
    public function Config(){
        $system = isset($_REQUEST['system']) ? $_REQUEST['system'] : 'yandex';

        $C = $GLOBALS['Setting']->get();

        foreach ($C['system']['payment']['list'] as $P)
            if($P['name'] == $system){
                $this->config = $P;
                break;
            }

        $host = \Core\Lib::getFullHost();
        $this->successURL = $host.$C['system']['payment']['successURL'];
        $this->failURL = $host.$C['system']['payment']['failURL'];

        //todo pending
        
        return$this->config;
    }

    public function UserID(){
        $User = new \Mastodont\Api\v1\Users();
        return $User->decodeData('user')['u'];
    }

    /**
     * Сохранение транзакции в БД
     * @param $data
     */
    public function preSave($data){
        $collection = \Core\DB::get()->transactions;

        $data['create'] = time();
        $data['domain_id'] = $GLOBALS['domain']['_id'];

        $collection->insert($data);

        return$data['_id'];
    }
}