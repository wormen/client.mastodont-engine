<?php

/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 20.01.2016
 * Time: 9:38
 */

namespace Core;

class Cache
{
    
    function __construct(){
    }

    private function file($str){
        $str = $str . '-' . implode('-', $_GET);
        return CacheDir . '/' . base64_decode(strtolower(password_hash($str, PASSWORD_DEFAULT, ['cost' => 15]))) . '.cache';
    }

    private function CheckTime($days=0){
        $C = $GLOBALS['Setting']->get();

        //получаем время кэша из настроек
        $M = (int)$C['site']['cacheTimeout'];
        $time = $M*60;

        if($days>0)
            $time = $time+\Core\Time::Days($days);

        return$time;
    }

    /**
     * @param array $data - массив данных
     * @param $str - название кэша
     */
    public function set($str, $data=[]){
        file_put_contents($this->file($str), json_encode($data));
    }

    /**
     * @param $str - название кэша
     * @return array - массив данных
     */
    public function get($str){
        if(file_exists($this->file($str)))
            return json_decode(file_get_contents($this->file($str)), true);
        else
            return [];
    }

    /**
     *  Удаление кэша
     */
    public function delete($isCli = false){
        foreach(glob(CacheDir.'/*.cache') as $file){
            try{
                $isDel = false;

                if($isCli){
                    // прибавляем 1 день, к времени настроек
                    $T = $this->CheckTime(1);

                    // проверяем возраст файла
                    $fileTime = time() - filemtime($file);

                    if (file_exists($file))
                        $isDel = $fileTime < $T;
                }

                if(file_exists($file) || $isDel)
                    unlink($file);

            }catch(\Exception $e){}

        }
    }
}