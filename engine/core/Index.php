<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 20.01.2016
 * Time: 9:45
 */


namespace Core;


class Index
{

    protected
        $Contacts,
        $Redirects;

    public function __construct(array$config=[]){
        $this->Contacts = new \Mastodont\Api\v1\Contacts();
        $this->Redirects = new \Mastodont\Api\v1\Seo\Redirects();

    }

    public function init($app){
        global$T;
        $this->CheckInstall();
        $this->Headers();

        if(!$this->getCookie('mst_locale') )
            $this->setCookie('mst_locale', 'rus');

        $T['locale'] = $this->getCookie('mst_locale') ? $this->getCookie('mst_locale') : 'rus';

        $config = $GLOBALS['Setting']->get();
        $config['modules']['contacts']['data'] = $this->Contacts->get();

        $T['vars'] = array_merge($T['vars'], $config);

        // проверяем наличие шаблона из настроек
        $T['tplPath'] = $GLOBALS['Tpl']->CheckTemplate($config['site']['template']);

        //режим отладки
        $app['debug'] = $config['general']['debug'];

        return$app;
    }

    private function CheckInstall(){

        $instDir = implode(DIRECTORY_SEPARATOR, [RootDir, 'install']);
        $lockFile = $instDir .DIRECTORY_SEPARATOR.'install.lock';
        if(file_exists($instDir) && !file_exists($lockFile)){
            header('Location: /install/');
            exit;
        }
    }

    public function Headers($type=null){

        $Partners = ['https://x-tiger.ru'];
        $arr = [
            'Access-Control-Allow-Origin'=>'*',
            "Access-Control-Allow-Methods"=> "GET,PUT,POST,DELETE,HEAD,OPTIONS",
            'X-Content-Type-Options'=>'nosniff',
            'X-XSS-Protection'=>'1; mode=block',
            "X-Frame-Options"=>"SAMEORIGIN",
            'X-Power-By'=>sprintf('Mastodont ( %s ) / Partners: ( %s )', 'https://mastodont-engine.ru', implode(' , ', $Partners) )

        ];

//        if($type==null){
//            $host = str_replace('www.', '', $_SERVER['HTTP_HOST']);
//            $protocol = stripos($_SERVER['SERVER_PROTOCOL'],'https') === true ? 'https://' : 'http://';
//
//            $default = [
//                'default-src' ,'self', 'unsafe-inline', 'unsafe-eval',
//                $host,
//                'www.'.$host,
//                '*.'.$host,
//                'x-sells.com', '*.x-sells.com',
//                'mastodont-engine.ru', '*.mastodont-engine.ru',
//
////                'https://yastatic.net',
////                'https://system.admify.ru',
////                'wss://webasr.yandex.net',
////                'https://tts.voicetech.yandex.net',
////                'blob: *.yandex.ru',
////                'yandex.ru',
////                '*.yandex.net',
////                'yandex.st',
////                'yastatic.net',
////                '*.yastatic.net',
////                'wss://portal-xiva.yandex.net',
////                'wss://push.yandex.ru'
//            ];
//            $default = implode(' ', $default).';';
//
//            $img = [
//                'img-src data: self',
//                $protocol.$host,
//                $protocol.'www.'.$host,
//                '*.'.$host,
//                'x-sells.com', '*.x-sells.com',
//                'mastodont-engine.ru', '*.mastodont-engine.ru',
//
////                'https://yastatic.net',
////                '*.yandex.ru yandex.ru',
////                '*.tns-counter.ru',
////                '*.gemius.pl',
////                'yandex.st',
////                '*.yandex.net',
////                'yastatic.net',
////                '*.yastatic.net',
//            ];
//            $img = implode(' ', $img).';';
//
//            $report = [
//                'report-uri',
//                    'https://csp.yandex.net/csp?from=big.ru&showid=22882.28243.1453274861.49587&h=n52&yandexuid=1212216541451257170&uid=55839446'
//            ];
//            $report = implode(' ', $report).';';
//
//            $arr = array_merge($arr, [
//                'Content-Security-Policy' => implode(' ', [$default, $img, /*$report*/])
//            ]);
//        }

        if($type==null)
            $this->Redirects->Go(); // проверяем редиректы

        foreach($arr as $k=>$v)
            header(sprintf("%s: %s", $k, $v));

    }

    public function setCookie($key, $value){
        $CookieInfo = session_get_cookie_params();
        setcookie($key, $value, strtotime( '+2 days' ), $CookieInfo['path'], $CookieInfo['domain']);
    }

    public function getCookie($key){
//        $n=self::$vars['cookie_prefix'].$n;
        return isset($_COOKIE[$key]) ? $_COOKIE[$key] : false;
    }

    public function deleteCookie($key){
        unset($_COOKIE[$key]);
    }

    /**
     * Счетчик, в основном используется для указания ID в модулях
     * @param $module - название модуля
     * @param null $section - название раздела
     * @param int $step - шаг
     * @return int
     */
    public function Counter($moduleName, $sectionName = null, $step = 1){
        $collection = \Core\DB::get()->counter;

        $data = ['module' => $moduleName, 'section' => $sectionName, 'num' => 1];

        $p = $collection->findOne(['module' => $moduleName, 'section' => $sectionName]);

        if($p){
            $p['num'] += $step;
            $collection->update(['_id'=>$p['_id']], ['num'=>$p['num']]);

            $data['num'] = $p['num'];

        }else
            $collection->insert($data);

        return$data['num'];
    }

    public function Translit($str, $rep = null) // из кирилицы в латинницу
    {
        $tr = [
            "А" => "a","Б" => "b", "В" => "v", "Г" => "g", "Д" => "d", "Е" => "e",
            "Ж" => "j", "З" => "z", "И" => "i", "Й" => "y", "К" => "k", "Л" => "l",
            "М" => "m", "Н" => "n", "О" => "o","П" => "p","Р" => "r","С" => "s",
            "Т" => "t","У" => "u","Ф" => "f","Х" => "h","Ц" => "ts","Ч" => "ch",
            "Ш" => "sh","Щ" => "sch","Ъ" => "","Ы" => "yi","Ь" => "","Э" => "e",
            "Ю" => "yu","Я" => "ya","а" => "a","б" => "b","в" => "v","г" => "g",
            "д" => "d","е" => "e","ж" => "j","з" => "z","и" => "i","й" => "y",
            "к" => "k","л" => "l","м" => "m","н" => "n","о" => "o","п" => "p",
            "р" => "r","с" => "s","т" => "t","у" => "u","ф" => "f","х" => "h",
            "ц" => "ts","ч" => "ch","ш" => "sh","щ" => "sch","ъ" => "y","ы" => "yi",
            "ь" => "","э" => "e","ю" => "yu","я" => "ya"," " => "-",
            "/" => "-",
        ];

        $res = strtr($str, $tr);

        $res=str_replace('&','and',$res);
        $res=preg_replace('/[^a-zA-Z0-9\.\-_]/i',$rep,$res);
        $res=preg_replace('/\-+/','-',$res);
//        $res=preg_replace('/_+/','_',$res);
        $res=trim($res,'-_');
        $qrep=preg_quote($rep,'#');
        $s=preg_replace('#('.$qrep.')+#',$rep,$res);

        return preg_replace('#^('.$qrep.')+|('.$qrep.')+$#','',$s);
    }


    public function Copyright(){

        
        return [
            'text'=>'Copyright &copy; by Mastodont',
            'date'=>'2014 - '.date('Y'),
            'ver'=>$this->checkVersion(),
        ];
    }
    
    protected function checkVersion(){
        $ver = '1.5.2';
        $versionFile = implode(DIRECTORY_SEPARATOR, [ConfigDir, 'version.lock']);

        if(file_exists($versionFile)){
            $versionFile = json_decode($versionFile);
            
            if($versionFile)
                $ver = $versionFile->v;
        }

        return $ver;
    }
}
