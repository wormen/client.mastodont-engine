<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 22.01.2016
 * Time: 15:45
 */

namespace Core;

class Log
{
    
    public function __construct(){

    }

    public function set($info, $type='system'){
        $collection = \Core\DB::get('LDB')->$type;

        $data = [
            'date'=>date("d-m-Y H:i:s", time()),
            'info'=>$info
        ];

        $collection->insert($data);
    }

    public function delete($id, $type){
        $collection = \Core\DB::get('LDB')->$type;
        $collection->remove(['_id'=>$id]);
    }


}