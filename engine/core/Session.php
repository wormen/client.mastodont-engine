<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 27.01.2016
 * Time: 8:38
 */

namespace Core;


class Session
{

    // запись автоирзации в БД
    public function auth($user_id, $data = []){
        $Scollection = \Core\DB::get()->session;
        $isOk = false;

        $data = [
            'sid'=>session_id(),
            'user_id'=>$user_id,
            'endTime'=>time()+$GLOBALS['st'],
            'data'=>$data
        ];

        $us=$Scollection->findOne( ['user_id'=>$user_id] );

//        $Ucollection = \Core\DB::Get()->users;
//        $u=$Ucollection->findOne( ['_id'=>$uid], ['enabled'=>true, 'is_ban'=>true] );

        //todo добавить поля

//        $data['isAdmin'] = false;
//        $data['isModerator'] = false;
//        $data['isSeo'] = false;

        if($us == null)
            $Scollection->insert($data);
        else
            $Scollection->update(['user_id'=>$user_id], $data);

        return$isOk;
    }

    public function get(){
        $collection = \Core\DB::get()->session;
        $d = $collection->findOne(['sid'=>session_id()]);

        return ($d ? $d : []);
    }

    // проверка авторизации
    public function checkAuth($user_id){
        if(!$user_id ) return false;

        $isOk = true;
        $Scollection = \Core\DB::get()->session;
        $us=$Scollection->findOne( ['user_id'=>$user_id] );

        $Ucollection = \Core\DB::get()->users;
        $u=$Ucollection->findOne( ['_id'=>$user_id], ['enabled'=>true, 'is_ban'=>true] );

        //todo проверка данных пользователя (ограничения)

        if (
            $us['sid'] != session_id() ||
            isset($us['endTime']) && $us['endTime'] < time() ||
            $u['enabled'] == false ||
            isset($u['is_ban']) && $u['is_ban'] == true
        ) return false;

        $us['adminMenu'] = (isset($u['sysMenu']) && is_array($u['sysMenu']) && count($u['sysMenu'])>0 ? $u['sysMenu'] : []);
        $Scollection->update(['user_id'=>$user_id], $us);

        return$isOk;
    }
}