<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 29.05.2016
 * Time: 20:20
 */

namespace Core;

require_once __DIR__ .'/../mastodont-sdk-php/ApiClient/Sender.php';
use \Mastodont\ApiClient\Sender;

class Widgets{

    protected
        $Sender,
        $module = 'system';

    function __construct(){
//        $this->config = $this->Setting();
        $this->Sender = new Sender('v1', null, true);
    }

    /**
     * Получаем список виджетов для главной страницы
     * @return mixed
     */
    public function getList(){
        $list = $this->Sender->get($this->module, 'getWidgetsList');

        return$list;
    }

    /**
     * Получаем список областей и виджетов
     * @return mixed
     */
    public function getData(){
        $collection = \Core\DB::get()->widgets;

        $m=$collection->findOne(['domain_id'=>$GLOBALS['domain']['_id']], ['_id'=>false]);
        return (isset($m['list']) ? $m['list'] : $m);
    }

    /**
     * Сохраняем данные виджетов
     * @param $data
     * @return bool
     */
    public function saveData($data){
        $collection = \Core\DB::get()->widgets;

        $data['domain_id'] = $GLOBALS['domain']['_id'];
        $d=$collection->findOne(['domain_id'=>$GLOBALS['domain']['_id']], []);

        if(!isset($d['_id']))
            return(boolean)$collection->insert($data);
        else
            return(boolean)$collection->update(['_id'=>$d['_id']], $data);
    }

    /**
     * Получаем контент для всех областей и виджетов
     * @return array
     */
    public function getContent(){
        $list = $this->getData();
        $data = [];

        foreach ($list as $wl){
            foreach ($wl['widgetList'] as $k=>$v){
                unset($wl['widgetList'][$k]['collapsed']);
                $wl['widgetList'][$k]['data'] = $this->getModuleData($v);
            }

            $data[] = $this->clearAreaData($wl);
        }

        return$data;
    }

    /**
     * Получаем поктент для определенной области
     * @param $areaName - название области
     * @return array
     */
    public function getAreaContent($areaName){
        $list = $this->getData();
        $data = [];

        foreach ($list as $wl){

            if(strcasecmp($wl['name'], $areaName) === 0){
                foreach ($wl['widgetList'] as $k=>$v){
                    unset($wl['widgetList'][$k]['collapsed']);
                    $wl['widgetList'][$k]['data'] = $this->getModuleData($v);
                }

                $data[] = $this->clearAreaData($wl);
            }

        }

        return$data;
    }

// ---------------------------------------------------------------------------------------------------------------------

    /**
     * Удаляем служебные данные из области
     * @param $area
     * @return mixed
     */
    protected function clearAreaData($area){
        unset($area['protected']);
        unset($area['collapse']);

        return$area;
    }

    /**
     * Получаем контент для виджетов
     * @param $widget
     * @return array
     */
    protected function getModuleData($widget){

        $data = [];

        if(isset($widget['data']['title']))
            $data['title'] = $widget['data']['title'];


        switch ($widget['name']){

            case 'last-news':
                break;

            case 'last-articles':
                $count = (int)$widget['data']['count'];
                if(is_string($count)) $count = 5;
                $data['list'] = $this->lastArticles($count);
                
                break;

            case 'custom-menu':
                $FullMenu = new \Mastodont\Api\v1\Menu();
                if($widget['data']['menu_id'] != 0)
                    $data['list'] = $FullMenu->getID($widget['data']['menu_id']);

                break;

        }


        return$data;
    }

// ---------------------------------------------------------------------------------------------------------------------

    /**
     * Последние статьи
     * @param $count
     */
    protected function lastArticles($count){
        global $T;

        //todo добавить определение локализации в URL

        $Articles = new \Mastodont\Api\v1\Articles();
        $lastArticles = $Articles->postMiniList(['enabled'=>true], [
            'name'=>true,
            'url'=>true,
            'anonce'=>true,
        ], $count);

        $list = [];
        $modules = $T['vars']['modules'];
        $art = $modules['articles'];

        if(count($lastArticles)>0){

            foreach ($lastArticles as $item){
                $item['url'] = '/'.$art['linkModule'].'/'.$art['links']['post'].'/'.$item['url'];
                $item['anonce'] = strip_tags($item['anonce']);
                $list[] = $item;
            }
        }

        return$list;
    }

}