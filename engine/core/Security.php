<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 20.01.2016
 * Time: 18:34
 */

namespace Core;

use \Firebase\JWT\JWT;
use \Rhumsaa\Uuid\Uuid;
use \Rhumsaa\Uuid\Exception\UnsatisfiedDependencyException;

class Security
{
    protected
        $masterKey = 'quA5Nx2dHhn0bj6YqHsDlWlnAgsg5DnDU', // универсальный ключ для токена
        $key;

    public function __construct(){
        $this->key = str_replace('www.', '', $_SERVER['HTTP_HOST']);
    }

    /**
     * @param array $data
     * @param string $key
     * @param bool $isMaster - использовать мастер ключ
     * @return string
     */
    public function setToken($data = [], $key = null, $isMaster = false){
        $key = $key == null ? $this->key : $key;

        if($isMaster) $key = $this->masterKey;

        $data = array_merge($data, [
            'sid'=>session_id(),
            'tokenUpdated'=>time(),
            'type'=>'site',
        ]);

        $token = JWT::encode($data, $key, 'HS256');

//        setcookie('token', $token);
//        $_SESSION['token'] = $token;

        return$token;
    }

    /**
     * @param string $token
     * @param string $key - ключ для расшифровки токена
     * @param bool $isMaster - использовать мастер-ключ
     * @param int $state - состояние повторной проверки, используется только внутри функции
     * @return bool|object|array
     */
    public function DecodeToken($token = '', $key = '', $isMaster = false, $state = 0){
        $token = $key == null ? $_SESSION['token'] : $token;
        $key = mb_strlen($key)==0 ? $this->key : $key;
        if($isMaster) $key = $this->masterKey;

        try{
            return JWT::decode($token, $key, ['HS256']);
        }catch(\Exception $e){
            // todo проверить еще с мастер-ключом
            if($state == 0)
                return $this->DecodeToken($token, $key, true, 1);
            else
                return false;
        }
    }

    /**
     * Шифруем данные
     * @param $data
     * @return string
     */
    public function setData($data){
        $token = JWT::encode($data, $this->masterKey, 'HS256');
        return$token;
    }

    /**
     * Расшифровываем данные
     * @param $str
     * @return bool|object
     */
    public function getData($str){

        try{
            return JWT::decode($str, $this->masterKey, ['HS256']);
        }catch(\Exception $e){
            return false;
        }
    }


    public function CheckCsrf($token){
        $ok = false;
        if(isset($_SESSION['token']) && $_SESSION['token'] == $token) $ok = true;
        return$ok;
    }

    //проверяем IP адрес, забанен он или нет
    public function CheckBanList(){
        $IP = $this->CheckIP();

        $isOk = true;
        $isNext = false;

        $white = implode(DIRECTORY_SEPARATOR, [ConfigDir, 'white-list.json']);
        $black = implode(DIRECTORY_SEPARATOR, [ConfigDir, 'black-list.json']);

        if(file_exists($white))
            $white = json_decode(file_get_contents($white));
        else
            $white = [];

        if(file_exists($black))
            $black = json_decode(file_get_contents($black));
        else
            $black = [];

        foreach($white as $w)
            if($w == $IP){
                $isNext = true;
                break;
            }

        if(!$isNext){
            foreach($black as $b)
                if($b == $IP){
                    $isOk = false;
                    break;
                }
        }

        return $isOk;
    }

    private function CheckIP(){
        return $_SERVER["REMOTE_ADDR"];
    }

    public function getUUID(){
        $uuid5 = Uuid::uuid4();
        return $uuid5->toString();
    }


    public function GenerateHash($length = 10){
        return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
    }

}