<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer=>Oleg Bogdanov
 * Contacts=>olegbogdanov86@gmail.com
 * -------------------------------------
 * Date=>03.02.2016
 * Time=>19:19
 */

namespace Core;

require_once __DIR__ .'/../mastodont-sdk-php/ApiClient/Sender.php';
use \Mastodont\ApiClient\Sender;

class Setting
{

    protected
        $Sender,
        $module = 'system';

    function __construct(){
        $this->Sender = new Sender('v1', null, false);
    }

    /**
     * Получаем настройки
     * @return array|mixed
     */
    public function get(){
        $domain = $this->getDomainDATA();

        if (count($domain) > 0) {
            $data = $GLOBALS['Security']->getData($domain['data']);
            if(!$data)
                // загружаем дефолтные настройки
                return $this->isDefault();
            else
                // сливаем текущие настройки с шаблоном настроек
                return $this->updateConfig(json_decode($data, true), $this->isDefault());
        }

        // если домен не найден, то сохраняем настройки по умолчанию
        else{
            $d = $this->isDefault();
            $this->set($d);
            return$d;
        }
    }

    /**
     * Сохраняем настройки
     * @param $data
     * @return bool
     */
    public function set($data){
        $collection = \Core\DB::get()->setting;

        $technical = ( isset($data['general']['technicalDomain']) ? $this->getDomain($data['general']['technicalDomain']) : '');

        $f = $data['system']['files'];

        $f['max_width'] = (int)$f['max_width'];
        $f['max_height'] = (int)$f['max_height'];
        $f['min_width'] = (int)$f['min_width'];
        $f['min_height'] = (int)$f['min_height'];

        $data['system']['files'] = $f;

        if(!isset($data['uuid']))
            $data['uuid'] = $GLOBALS['Security']->getUUID();

        $data = json_encode(array_merge(self::isDefault(), $data));
        $data = [
            'domain'=>$this->getDomain(), // основной домен
            'technical'=>$technical,// технический домен
            'data'=>$GLOBALS['Security']->setData($data)
        ];

        $d=$collection->findOne(['domain'=>$this->getDomain()]);
        if($d)
            $r = $collection->update(['domain'=>$this->getDomain()], $data);
        else
            $r = $collection->insert($data);

        return (boolean) $r;
    }

//----------------------------------------------------------------------------------------------------------------------

    public function getDefaultSetting(){
        $list = $this->Sender->get($this->module, 'getDefaultSetting');
        $list = \Core\Lib::objectToArray($list);

        return$list;
    }

    /**
     * Настройки по умолчанию
     * @return array
     */
    protected function isDefault(){
        $list = $this->Sender->get($this->module, 'getDefaultSetting');
        $list = \Core\Lib::objectToArray($list);
        return$list;
    }

    /**
     * Обновление текущих настроек с шаблоном
     * @param $array1 - массив который надо обновить
     * @param $array2 - применяемый массив
     * @return mixed
     */
    protected function updateConfig($array1, $array2){
        return \Core\Lib::array_update($array1, $array2);
    }

    /**
     * Получаем хэш домена
     * @param null $domain
     * @return mixed
     */
    public function getDomain($domain = null){
        $d = str_replace('www.', '', $domain ? $domain : $_SERVER['HTTP_HOST']);
        $d = base64_encode($d);
        return$d;
    }

    /**
     * Получаем данные домена
     * @return array
     */
    public function getDomainDATA(){
        $domain = $this->getDomain();

        $collection = \Core\DB::get()->setting;
        $list=$collection->find([]);

        $d = [];
        foreach ($list as $D){
            if($D['domain'] == $domain || $D['technical'] == $domain){
                $d = $D;
                break;
            }

            if( isset($D['ext']) && in_array($domain, $D['ext'], true) ){
                $d = $D;
                break;
            }
        }

        return$d;
    }


}

