<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 27.01.2016
 * Time: 13:14
 */

namespace Core;

use \Nette\Mail\Message;
use \Nette\Mail\SmtpMailer;
use \Nette\Mail\SendmailMailer;

class Email
{

    function __construct(){
    }

    private function Headers(){

        $arr = [
            'MIME-Version' => '1.0',
            'Date' => date('r'),
            'X-Priority' => 3,
            'X-CMS' => 'Mastodont',
            'X-Mailer'=>'PHP/' . phpversion()
        ];

        return$arr;
    }

    /**
     * @param $to - от кого
     * @param $from - кому
     * @param $subject - тема сообщения
     * @param $text - текст сообщения
     * @param array $files - массив файлов
     */
    public function Send($to, $from, $subject = null, $text = null, $files = []){
        $mail = new Message;

        // выставляем заголовки
        foreach ($this->Headers() as $k => $v)
            $mail->setHeader($k, $v);

//        if($a['cc'])
//            foreach($a['cc'] as $c)
//                $mail->setHeader('Cc', $c);

        if($to)
            $mail->addTo($to);

        if(count($files)>0){
            foreach($files as $f)
                $mail->addAttachment($f);
        }

        $mail->setFrom($from);

        if($subject)
            $mail->setSubject($subject);

        if($text)
            $mail->setHTMLBody($text);

        try{
//            $mailer = new SmtpMailer(array(
//                'host' => 'smtp.gmail.com',
//                'username' => 'john@gmail.com',
//                'password' => '*****',
//                'port'=>25,
//                'secure' => 'ssl',
//            ));

            $mailer = new SendmailMailer;

            return $mailer->send($mail);

        }catch(Exception $e){
            $GLOBALS['Log']->set($e, 'email');
            return false;
        }
    }
}