<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 01.04.2016
 * Time: 9:43
 */

namespace Core;

use \Symfony\Component\HttpFoundation\Response;

class Lib
{

    public static
        $fatalMsg;

    function __construct(){
        static::$fatalMsg = json_encode([ 'status'=>'error', 'statusCode'=>500, 'text'=>'Method Not found' ]);
    }

    /**
     * Обновление одного массива другим
     * @param $orig - массив который надо обновить
     * @param $add - применяемый массив
     * @return array
     */
    public static function array_update($orig, $add){
        foreach($orig as $key => $value)
        {
            if(is_array($value))
            {
                if(!isset($add[$key]))
                    $add[$key] = $value;

                elseif(!is_array($add[$key]))
                    $add[$key] = $value;

                else
                {
                    $new_diff = static::array_update($value, $add[$key]);
                    if($new_diff != FALSE)
                        $add[$key] = $new_diff;
                }
            }

            elseif(!isset($add[$key]) || $add[$key] != $value)
                $add[$key] = $value;
        }

        return $add;
    }

    /**
     * Обрезка строки до определенного кол-ва символов
     * @param $string - входная строка
     * @param $length - длина строки
     * @return string
     */
    public static function CropString($string, $length){

        if (mb_strlen($string) >= $length) {
            $string = strip_tags($string); // Первым делом, уберём все html элементы
            $string = substr($string, 0, $length); // Теперь обрежем его на определённое количество символов
            $string = rtrim($string, "!,.-"); // Затем убедимся, что текст не заканчивается восклицательным знаком, запятой, точкой или тире
            $string = substr($string, 0, strrpos($string, ' ')); // Напоследок находим последний пробел, устраняем его и ставим троеточие
            return $string . " …";

        } else
            return $string;
    }

    /**
     * Склонение слова, в зависимости от числа
     * @param $n - число
     * @param $s1 - вариант 1
     * @param $s2 - вариант 2
     * @param $s3 - вариант 3
     * @param bool $b - выделять или нет склоненное слово
     * @return string
     */
    public static function Sklonen($number, $titles) {
        $cases = array (2, 0, 1, 1, 1, 2);
        return $titles[ ($number%100>4 && $number%100<20)? 2 : $cases[min($number%10, 5)] ];
    }

    /**
     * Получаем массив параметров из URL
     * @return array
     */
    public static function getParams(){
        $arr = [];

        if(isset($_REQUEST['page']))
            unset($_REQUEST['page']);

        foreach ($_REQUEST as $k=>$v){
            if(strlen($k) == 0 || strlen($v)==0) continue;
            $arr[$k] = $v;
        }

        return$arr;
    }

    /**
     * Получение домена с протоколом
     * @return string
     */
    public static function getFullHost(){
        $protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
        return $protocol . $_SERVER['HTTP_HOST'];
    }

    /**
     * Рекурсивное конвертирование объекта в массив
     * @param $obj
     * @return array
     */
    public static function objectToArray($obj) {
        if(is_object($obj))
            $obj = (array) $obj;

        if(is_array($obj)) {
            $new = [];

            foreach($obj as $key => $val)
                $new[$key] = static::objectToArray($val);
        }
        else
            $new = $obj;

        return $new;
    }

// ---------------------------------------------------------------------------------------------------------------------
//  генерирование тела ответа


    protected static function checkDataResponse($data, $type){
        return $data ||
        (is_array($data) && count($data) == 0) ||
        ($type == 'get' && gettype($data) == 'boolean');
    }

    public static function getResponse($data){
        return static::checkDataResponse($data, 'get')
            ? new Response(json_encode([ 'status' => 'success', 'statusCode' => 200, 'result' => $data]), 200)
            : new Response(json_encode([ 'status'=>'error', 'statusCode'=>400, 'text'=>$GLOBALS['lang']['incorrectData'], ]), 400);
    }

    public static function postResponse($data){
        return static::checkDataResponse($data, 'post')
            ? new Response( (is_array($data) ? json_encode([ 'status' => 'success', 'statusCode' => 201, 'result' => $data]) : ''), 201)
            : new Response(json_encode([ 'status'=>'error', 'statusCode'=>400, 'text'=>$GLOBALS['lang']['incorrectData'], ]), 400);
    }

    public static function putResponse($data){
        return static::checkDataResponse($data, 'put')
            ? new Response('', 200)
            : new Response(json_encode([ 'status'=>'error', 'statusCode'=>400, 'text'=>$GLOBALS['lang']['incorrectData'], ]), 400);
    }

    public static function deleteResponse($data){
        return static::checkDataResponse($data, 'delete')
            ? new Response('', 200)
            : new Response(json_encode([ 'status'=>'error', 'statusCode'=>400, 'text'=>$GLOBALS['lang']['incorrectData'], ]), 400);
    }

    public static function fatalResponse($text = ''){
        return new Response($text, 500);
    }
}