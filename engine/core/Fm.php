<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 25.01.2016
 * Time: 17:52
 */

namespace Core;


class Fm
{
    public function MkDir($path, $mode=0755, $recursive = false){
        return mkdir($path, $mode, $recursive);
    }

    public function Rename($old, $new){
        try{

            if(file_exists($old))
                rename($old, $new);
            
        }catch (\Exception $e){
            $GLOBALS['Log']->set($e, 'FM');
        }

    }

    public function delete($path, $nocheck = false){
        return $this->deleteDir($path, $nocheck);
    }

    public function deleteDir($path, $nocheck = false){
        if (is_dir($path)) {
            $f = __function__;
            $entries = array_diff(scandir($path), array('.', '..'));

            foreach ($entries as $entry)
                if (!$this->$f($path . DIRECTORY_SEPARATOR . $entry, true))
                    return true;

            return rmdir($path);
        }
        
        #Если ссылка битая, file_exists её не определяет, поэтому $check
        return $nocheck || file_exists($path) ? unlink($path) : true;
    }

}