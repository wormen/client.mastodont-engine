<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 22.01.2016
 * Time: 16:50
 */

namespace Core;

class DB
{

    private static
        $user = null,
        $password = null;

    protected static $list = [
        'DB' => 'dataBase', // Основная БД сайта(ов)
        'MSGDB' => 'msgBase', // База личных сообщений пользователей
        'LDB' => 'logBase', // База логов
        'SDB' => 'serviceBase', // База сервисов

        'PARSER'=>'parserBase', // база данных дл различных парсеров
        'APIDB'=>'apiBase', // База, для кэширования из API

        //todo синхрнизация с API
        'GDB' => 'geoBase', // Гео База
        
        'QDB' => 'queueBase', // База очередей
        'FDB' => 'filesBase', // База загруженных файлов
    ];

    public function __construct($config = []){
        static::$user = 'root';
        static::$password = '7M5WZJy5JPk6';
    }

    /**
     * Инициализация БД
     * @return \MongoLite\Client
     */
    protected static function Client(){
        return new \MongoLite\Client(DataDir, static::$user, static::$password, [], 'mastodont_db');
    }

    /**
     * Подключение к БД
     * @param string $DB - используемая база данных
     * @return bool|object
     */
    public static function get($DB = 'DB')
    {
        $client = static::Client();

        foreach (static::$list as $k => $v)
            if ($DB == $k)
                return $client->{$v};

        return false;
    }

}

