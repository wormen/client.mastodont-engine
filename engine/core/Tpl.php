<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 22.01.2016
 * Time: 15:07
 */

namespace Core;

class Tpl
{

    protected
        $Users;

    public
        $Paginate = [
        'maxPagesToShow'=>5
    ];

    function __construct(){
        $this->Users = new \Mastodont\Api\v1\Users();
    }




    /**
     * Данные для построения шаблонов страниц
     * @param $req
     * @param null $module
     * @param null $section
     * @param null $item
     * @return mixed
     */
    public function getDataTpl($req, $module = null, $section = null, $item = null){
        global$T,$tpl;
        $isAdmin = false;

        //todo получаем данные о пользователе
        $user = $this->Users->decodeData('user');

        $T['auth'] = ($user == false ? false : true);

        if($user){
            $s = $this->Users->edit($user['u']);
//        $s = \Core\Session::CheckAuth($user['u']);

            $dArr = ['isRoot', 'mainRoot', 'protectSetting', 'domains', 'sysMenu', 'isTmp', 'delTime'];
            foreach($dArr as $a)
                if(isset($s[$a])) unset($s[$a]);

            $T['user'] = (!$s ? [] : $s);
            $T['permisions'] = $this->Users->getPermistions($s);
        }


        //todo получаем шаблон страницы на основе конфига и URL
        $Td = $this->CheckModuleTpl($req, $T['vars']['modules'], $module, $section, $item);

        $_SESSION['module'] = $T['module'];

        // пишем часть данных шаблона в куки, для доступа из JS, или после протухания сессии
        $this->setData($T['module']);

        // ID документа из контента
        if(isset($T['content']['_id']))
            $_SESSION['module']['_id'] = $T['content']['_id'];
        else
            $_SESSION['module']['_id'] = null;


        if($T['module']['name'] != 'account')
            if($T['is404'] == true) $Td['tplPage'] = 404;

        if($T['is500'] == true) $Td['tplPage'] = 500;

        // todo проверить группу пользователя
        if($T['vars']['site']['closed'] && !$isAdmin)
            $Td['tplPage'] = 'denied';

        $T['tplPage'] = $Td['tplPage'];

        $tplFile = implode(DIRECTORY_SEPARATOR, [$tpl['path'], 'pages', "{$Td['tplPage']}.php"]);
        if(!file_exists($tplFile))
            $this->generateTplFile($tplFile);

        $T['pageContent'] = include $tplFile;

        return $T;
    }

    /**
     * Контент текущего модуля
     * @param $D
     * @return array
     */
    private function getContent($D){
        $data = [];

        if(isset($D['name'])){
            $asAdmin = false;

            switch($D['name']){
                case 'account':
                    $uid = isset($this->getUserData()['u']) ? $this->getUserData()['u'] : null;

                    switch ($D['section']){

                        case 'messages':
                            $User = new \Mastodont\Api\v1\Users();
                            $data = ['count'=>0, 'list'=>[]];
                            $data['list'] = $User->getNotify($uid);
                            $data['count'] = count($data['list']);
                            break;

                        case 'ads':
                            if($uid){
                                $Ads = new \Mastodont\Api\v1\Ads();
                                $statusList = ['moderation', 'ok', 'archive', 'fail'];

                                foreach ($statusList as $s)
                                    $data[$s] = $Ads->postGetStatus($uid, $s, 5);
                            }else
                                $data = [];
                            break;
                    }

                    break;

                case 'static':
                    $Static = new \Mastodont\Api\v1\StaticPage();
                    $data = $Static->FindUrl($D['url']);
                    if(!$data || $data['enabled'] == false)
                        $this->ExitPage(404);
                    break;

                case 'articles':
                    $Articles = new \Mastodont\Api\v1\Articles();

                    switch($D['section']){
                        case 'main':
                            $data = $Articles->ModuleMainPage();
                            break;

                        case 'category':
                            $data = $Articles->CategoryFindUrl($D['url']); //todo пагинатор
                            if(!$data || $data['enabled'] == false)
                                $this->ExitPage(404);
                            break;

                        case 'post':
                            $data = $Articles->postFindUrl($D['url']);
                            if(!$data || $data['enabled'] == false)
                                $this->ExitPage(404);
                            break;

                        case 'tag':
                            $data = $Articles->TagFindUrl($D['url']); //todo пагинатор
                            if(!$data || $data['enabled'] == false)
                                $this->ExitPage(404);
                            break;

                        default:
                            $this->ExitPage(404);
                            break;
                    }

                    break;

                case 'faq':
                    $Faq = new \Mastodont\Api\v1\Faq();
                    $data = $Faq->FullTree();
                    break;

                case 'ads':
                    $Ads = new \Mastodont\Api\v1\Ads();

                    switch ($D['section']){
                        case 'ad':
                            $data = $Ads->postFindUrl($D['item']);
                            $asAdmin = $this->IsViewAsAdmin();
                            break;

                        case 'category':
                            $data = $Ads->CategoryFindUrl($D['item']);
                            $asAdmin = $this->IsViewAsAdmin();

                            if(!$data || $data['enabled'] == false)
                                $this->ExitPage(404);
                            else
                                $data['ads'] = $Ads->postFullList([
                                    'categoryTree'=>false,
                                ], [
                                    'category_id'=>$data['_id'],
                                    'moderateStatus'=>'ok',
                                ]);
                            break;
                    }

                    if(!$data  || (!$asAdmin && $data['enabled'] == false) )
                        $this->ExitPage(404);
                    break;
            }
        }

        return$data;
    }

    /**
     * Генерирование данных файла шаблона на основе входных данных
     * @param $req - данные запроса
     * @param $modules - список модулей
     * @param $module - текущий модуль
     * @param $section - раздел модуля
     * @param $item - параметр модуля (обычно URL)
     * @return array
     */
    private function CheckModuleTpl($req, $modules, $module, $section, $item){
        global$T;
        $DS = DIRECTORY_SEPARATOR;
        $getContent = false;
        $congig = [];
        $moduleLinks = [];
        $url = $urlParse = parse_url($_SERVER['REQUEST_URI']);

        if($urlParse['path'] == '/' || $urlParse['path'] == '')
            $module = 'main';

        // разбираем текущий URL
        $U = explode('/', trim($urlParse['path'], '/'));

        if(count($U) == 1 && strlen($U[0]) == 0)
            $module = 'main';

        else{

            $module = $tplPage = 404;

            foreach($modules as $k=>$v){

                // чекаем название модуля
                if(isset($v['linkModule']) && strcasecmp($U[0], $v['linkModule']) === 0){
                    $module = $k; // текущий модуль
                    $congig = $v; // конфиг модуля

                    if(isset($v['links']))
                        $moduleLinks = $v['links'];

                    // проверяем активность модуля
                    if( !isset($v['enabled']) || (isset($v['enabled']) && $v['enabled'] == false) )
                        $module = $tplPage = 404;
                    break;
                }

            }

        }


        if($req->attributes->get('_route') == 'GET_module')
            $req->attributes->set('_route', $module);

        // ищем шаблон раздела на основе URL
        if(isset($U[1])){

            foreach($moduleLinks as $t=>$link){
                if($link == $U[1]){
                    $section = strtolower($t); // прменяем шаблон раздела, в соответствии с конфигом
                    break;
                }
            }

        }

        // проверки шаблонов страниц
        $tplPage = $module;
        switch($module){
            case 'main':
                $tplPage = $module = 'main';
                $section = null;
                break;

            case 'search':
                $tplPage = $module = 'search';
                $section = null;
                break;

            case 'contacts':
                $tplPage = $module = 'contacts';
                $section = null;
                $T['content'] = $modules['contacts']['data'];
                break;

            case 'faq':
                $section = null;
                $getContent = true;
                break;

            case 'articles':

                $url = isset($U[2]) ? $U[2] : null;
                $section = ($section == null ? 'main' : $section);
                $tplPage = $module.$DS.$section;
                $getContent = true;

                break;

            case 'static':
                if(isset($U[1]) && mb_strlen($U[1])>3){
                    $url = $U[1];
                    $section = null;
                    $getContent = true;
                }else
                    $module = $tplPage = 404;
                break;

            case 'ads':
                $section = ($section == null ? 'main' : $section);
                $tplPage = $module.$DS.$section;

                switch($section){
                    case 'ad':
                    case 'category':
                        $getContent = true;
                        break;

                    default:
                        $getContent = false;
                        break;
                }

                break;

            case 'account':
                $section = ($section == null ? 'main' : $section);
                $noAuth = true;

                // страницы с доступом без авторизации
                $pages = ['activate', 'register', 'lostpass', 'user-view', 'user-view-ads'];

                foreach($pages as $p){
                    if($section == $p){

                        if($T['auth'] == false && $section == $p )
                            $noAuth = false;

                        break;
                    }
                }


                if($T['auth'] == false && $noAuth)
                    $section = 'no-auth';

                // аккаунт отключен
                if($congig['on'] == false)
                    $section = 'off';

                switch($section){

                    case 'messages':
                    case 'ads':
                        $tplPage = $module.$DS.$section;
                        $getContent = true;
                        break;

                    // проверяем, указан ли логин пользователя
                    case 'user-view':
                    case 'user-view-ads':
                        if($item == null)
                            $tplPage = 404;
                        else
                            $tplPage =$module.$DS.$section;
                        break;

                    default:
                        $tplPage = $module.$DS.$section;
                        break;
                }

                break;

            case 'video':
                $section = ($section == null ? 'main' : $section);

                // todo контент на основе конфига

                $tplPage = $module.$DS.$section;
                break;

            default:
                $module = 404;
                $section = null;
                break;
        }

        if($item == $url)
            $item = null;

        $T['module'] = [
            'name'=>$module,
            'section'=>$section,
            'item'=>$item,
            'url'=>$url,
        ];



        if($getContent){
            $T['content'] = $this->getContent($T['module']);
            if(!is_array($T['content']) || $T['content'] == null)
                $tplPage = 404;
        }

        $GLOBALS['ev']->emit('set:module:data', $T['module']);

        return [
            'tplPage'=>$tplPage,
        ];
    }

    /**
     * @param $srcArr - массив файлов
     * @return string
     */
    public function AddHeadScript($srcArr){
        $list='';
        foreach($srcArr as $s)
            $list .= '<script type="text/javascript" src="'.$s.'?ver='.$GLOBALS['Core']->Copyright()['ver'].'"></script>';

        return$list;
    }

    /**
     * @param $hrefArr - массив файлов
     * @return string
     */
    public function AddHeadCss($hrefArr){
        $list='';
        foreach($hrefArr as $s)
            $list .= '<link rel="stylesheet" type="text/css" href="'.$s.'?ver='.$GLOBALS['Core']->Copyright()['ver'].'">';

        return$list;
    }

    /**
     * Проверка пльзовательского шаблона
     * @param string $tplName
     * @return array
     */
    public function CheckTemplate($tplName = 'default'){
        $tplDir = implode(DIRECTORY_SEPARATOR, [TemplateDir, $tplName]);
        if(file_exists($tplDir))
            return [
                'name' => $tplName,
                'url' => '/template/' . $tplName,
                'path' => $tplDir
            ];
        else
            return $this->CheckTemplate();
    }

    /**
     * Страница ошибки
     * @param int $code - 404 / 500
     */
    public function ExitPage($code = 404){

        if($code == 404)
            $GLOBALS['T']['is404'] = true;

        if($code == 500)
            $GLOBALS['T']['is500'] = true;
    }

    /**
     * Получить Title
     * @return mixed
     */
    public function getTitle(){
        return$GLOBALS['title'];
    }

    /**
     * Установить Title страницы
     * @param $value
     * @param bool $isMainPage
     * @return string
     */
    public function setTitle($value, $isMainPage = false){
        $S = $GLOBALS['Setting']->get()['site'];
        if($value)
            $title = $value.$S['defis'].$S['name'];
        else
            $title = $S['name'];

        if($isMainPage)
            $title = mb_strlen($S['mainPageTitle']) > 0 ? $S['mainPageTitle'] : $value;

        if($isMainPage && mb_strlen($S['mainPageTitle']) == 0 && !$value) $title = $S['name'];

        $GLOBALS['title'] = $title;
        return$title;
    }

    /**
     * Установить мета данные шаблона
     * @param $name
     * @param $value
     */
    public function setMeta($name, $value){
        $GLOBALS['mеta'][$name] = $value;
    }

    /**
     * Получить метаданные шаблона
     * @param $name
     * @return string
     */
    public function getMeta($name){
        $v = isset($GLOBALS['mеta'][$name]) ? $GLOBALS['mеta'][$name] : '';
        return '<meta name="'.$name.'" content="'.$v.'">';
    }

    /**
     * Получаем из кук данные о шаблоне
     * @return array|mixed
     */
    public function getData(){
        $data = [];

        $d = $GLOBALS['Core']->getCookie('mst_tpl');
        if($d)
            $data = json_decode(base64_decode($d), true);

        return$data;
    }

    /**
     * Сохраняем в куку служебные данные о пользователе
     * @return array|mixed
     */
    public function getUserData(){
        $data = [];

        $d = $GLOBALS['Core']->getCookie('mst_user');
        if($d)
            $data = json_decode(base64_decode($d), true);

        return$data;
    }

    /**
     * Сохраняем в куку служебные данные по шаблону
     * @param array $data
     */
    public function setData($data = []){
        $GLOBALS['Core']->setCookie('mst_tpl', base64_encode(json_encode($data)));
    }

    /**
     * Формирование данных для пагинатора
     * @param $data
     * @return array
     */
    public function Paginator($data){

        return [
            'total'=>$data->getTotalItems(),
            'prev'=>$data->getPrevUrl(),
            'next'=>$data->getNextUrl(),
            'pages'=>$data->getPages(),
        ];
    }

    /**
     * Проверка, находимся ли мы в панели администратора
     * @return bool
     */
    public function CheckAdminPanel(){
        return (isset($_SERVER['HTTP_REFERER']) && stripos($_SERVER['HTTP_REFERER'], 'admin') >0) ? true : false;
    }

    /**
     * Проверка прав доступности модерации контента
     * @return bool
     */
    public function isModerate(){
        global$T;

        $M = ($T['permisions']['moderate'] && isset($_GET['moderation']));

        return$M;
    }

    /**
     * Статус контента
     * @param $data - массив с данными контента
     * @return array
     *          status - статус модерации
     *          enabled - статус активности документа
     *          isView - разрешен ли просмотр документа
     */
    public function contentStatus($data){
        $enabled = false;
        $status = null;
        $isView = false;

        if(isset($data['enabled']))
            $enabled = $data['enabled'];

        if(isset($data['moderateStatus']))
            $status = $data['moderateStatus'];

        if($status == 'ok' && $enabled)
            $isView = true;

        if($_SESSION['IS_ROOT'])
            $isView = true;

        return [
            'status'=>$status,
            'enabled'=>$enabled,
            'isView'=>$isView
        ];
    }

// ---------------------------------------------------------------------------------------------------------------------

    /**
     * Просмотр шаблона, как админ
     * @return bool
     */
    protected function IsViewAsAdmin(){
        $G = new \Mastodont\Api\v1\Groups();
        $gl = isset($GLOBALS['T']['user']['groups']) ? $GLOBALS['T']['user']['groups'] : [];
        $Perms = $G->CheckPermitions($gl);

        $isView = false;
        foreach ($Perms as $p=>$v)
            if($p == 'administrators' || 'moderators')
                $isView = true;

        return$isView;
    }

    /**
     * Автоматическая генерация файла шаблона, при его отсутствии
     * @param $path - путь к файлу
     */
    protected function generateTplFile($path){
        $P = explode(DIRECTORY_SEPARATOR, $path);
        $FM = new \Core\Fm();

        unset( $P[count($P)-1] );

        $dir = implode(DIRECTORY_SEPARATOR, $P);
        if(!file_exists($dir) && strlen($dir)>0)
            $FM->MkDir($dir, 0755, true);

        $pd = addcslashes(str_replace(RootDir.DIRECTORY_SEPARATOR, '', $path), "\\");
        $fileData = <<<HTML
<?php

return "auto-generated template file <br> path: {$pd}";

HTML;

        file_put_contents($path, utf8_encode($fileData));
    }

}