<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 14.02.2016
 * Time: 13:43
 */

namespace Core;

/**
 *  addTime - время добавления в очередь
 *  data - данные
 *  status - added / success / fail
 */

class Queue
{
    /**
     * Получение очереди определенного типа
     * @param string $type
     * @return array
     */
    public function getList($type = 'email', $params = []){
        $collection = \Core\DB::get('QDB')->$type;
        $list = $collection->find($params, ['addTime'=>false])->sort(["addTime"=>'ask'])->toArray();
        return ($list ? $list : []);
    }

    /**
     * Добавление в очередь
     * @param string $type
     * @param array $data
     * @return bool
     */
    public function Add($type = 'email', $data){
        $collection = \Core\DB::get('QDB')->$type;

        $data = [
            'addTime'=>time(),
            'data'=>$data,
            'status'=>'added'
        ];

        return (boolean) $collection->insert($data);
    }

    public function update($type = 'email', $id, $data){
        $collection = \Core\DB::get('QDB')->$type;
        if(isset($data['_id'])) unset($data['_id']);
        return (boolean)$collection->update(['_id'=>$id], $data);
    }

    /**
     * Удаление элемента очереди
     * @param $id
     * @param $type
     * @return bool
     */
    public function delete($id, $type = 'email'){
        $collection = \Core\DB::get('QDB')->$type;
        return (boolean)$collection->remove(['_id'=>$id]);
    }
}