<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 29.06.2016
 * Time: 9:21
 */

namespace Core;


class Time
{
    /**
     * Получаем значение равное кол-ву минут
     * @param $val - кол-во минут
     * @return int
     */
    public static function Minute($val){
        return $val * 60;
    }

    /**
     * Получаем значение равное кол-ву часов
     * @param $val - кол-во часов
     * @return int
     */
    public static function Hours($val){
        return $val * static::Minute(60);
    }

    /**
     * Получаем значение равное кол-ву дней
     * @param $val
     * @return int
     */
    public static function Days($val){
        return $val * static::Hours(24);
    }

    public static function Unix(){
        return time();
    }
}