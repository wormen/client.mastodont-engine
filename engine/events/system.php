<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 11.02.2016
 * Time: 18:24
 */

use \Pagon\EventEmitter;
$ev = new EventEmitter();

$ev->on('set:title', function($str){
    $GLOBALS['title'] = $str;
});

$ev->on('set:lang', function(){
    $Lang = new \Mastodont\System\Lang();
    $locale = $GLOBALS['T']['locale'];
//    $dir = implode(DIRECTORY_SEPARATOR, [LangDir, $locale]);

//    $system = include $dir . '/system.php';
//    $user = include $dir . '/user.php';
//
//    $GLOBALS['lang'] = array_merge($system, $user);

    $GLOBALS['lang'] = $Lang->getData($locale);
});

$ev->on('set:module:data', function($data){
    $GLOBALS['T']['module'] = $data;
});

$ev->on('is404', function($val){
    $GLOBALS['T']['is404'] = $val;
});

$ev->on('is500', function($val){
    $GLOBALS['T']['is500'] = $val;
});

// ---------------------------------------------------------------------------------------------------------------------

