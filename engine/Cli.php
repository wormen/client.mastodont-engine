<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 02.02.2016
 * Time: 20:15
 */


class Cli
{

    protected 
        $S;
    
    function __construct(){
        $this->S = new \Cli\Service();
    }

    /**
     *  Инициализация менеджера сервисов
     */
    public function init(){

        $service = 'manager';
        $manager = implode(DIRECTORY_SEPARATOR, [CliDir, "cli-$service.php"]);
        $stopFile = implode(DIRECTORY_SEPARATOR, [CliDir, "state", "$service.stop"]);
        $lifetime = implode(DIRECTORY_SEPARATOR, [CliDir, "config", "$service.lifetime"]);

        $PIDfile = implode(DIRECTORY_SEPARATOR, [PidDir, "$service.pid"]);
        $PID = 0;
        if(file_exists($PIDfile))
            $PID = (int)file_get_contents(implode(DIRECTORY_SEPARATOR, [PidDir, "$service.pid"]));

        if(file_exists($lifetime)){
            $t = time() - (int)file_get_contents($lifetime);
            if($t < 30)
                return false;
        }

        if( $this->S->isRuning($PID) || file_exists($stopFile)) return false;

        $this->S->RunAsDeamon($manager);

        return true;
    }



}