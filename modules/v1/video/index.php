<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 24.01.2016
 * Time: 0:00
 */

use \Symfony\Component\HttpFoundation\Response;

$Video = new \Mastodont\Api\v1\Video();

$data = [];
$params = \Core\Lib::getParams();

switch($_SERVER['REQUEST_METHOD']){
    case 'GET':

        switch ($method){

            case 'categoryMiniList':
                $data = $Video->{$method}(isset($params['tree']));
                break;

            case 'categoryEdit':
                $data = $Video->categoryEdit($app->escape($id));
                break;

            case 'videoEdit':
                $data = $Video->postEdit($app->escape($id));
                break;

            case 'miniList':
                $data = $Video->postMiniList();
                break;

            case 'fullList':
                $data = $Video->fullList(isset($params['tree']), $params['type']);
                break;

            case 'checkUrl':
                $data = $Video->categoryCheckUrl($params['url'], $params['locale']);
                break;

            default:
                return \Core\Lib::fatalResponse(\Core\Lib::$fatalMsg);
        }

        $data = \Core\Lib::getResponse($data);
        
//        if(isset($_GET['type'])){
//            switch($app->escape($_GET['type'])){
//                case 'category':
//                    
//                case 'post':
//                    if(isset($_GET['id']))
//
//
//                    else if(isset($_GET['user-edit']))
//                        $data = $Video->postFindUrl($app->escape($_GET['user-edit']));
//
//                    else
//
//
//                    break;
//            }
//        }

        break;

    case 'POST':


        switch ($method){

            case 'categorySave':
                $data = $Video->categorySave(json_decode($req->getContent(), true));
                break;

            case 'videoSave':
                $data = $Video->postSave(json_decode($req->getContent(), true));
                break;

            default:
                return \Core\Lib::fatalResponse(\Core\Lib::$fatalMsg);
        }

        $data = \Core\Lib::postResponse($data);
        break;

    case 'PUT':

        switch ($method){

            case 'categoryUpdate':
                $data = $Video->categoryUpdate(['_id'=>$app->escape($id)], json_decode($req->getContent(), true));
                break;

            case 'videoUpdate':
                $data = $Video->postUpdate(['_id'=>$app->escape($id)], json_decode($req->getContent(), true));
                break;

            default:
                return \Core\Lib::fatalResponse(\Core\Lib::$fatalMsg);
        }

        $data = \Core\Lib::putResponse($data);
        break;

    case 'DELETE':

        switch ($method) {

            case 'categoryRemove':
                $data = $Video->categoryDelete($app->escape($id));
                break;

            case 'videoRemove':
                $data = $Video->postDelete($app->escape($id));
                break;

            default:
                return \Core\Lib::fatalResponse(\Core\Lib::$fatalMsg);
        }

        $data = \Core\Lib::deleteResponse($data);
        break;
}

return$data;