<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 11.02.2016
 * Time: 12:24
 */

$Geo = new \Mastodont\Api\v1\Geo();

$data = [];
$params = \Core\Lib::getParams();

if(isset($_GET)){

    switch ($method){

        case 'districtInit':
            $did = (isset($params['district']) ? $did = preg_replace("/[^0-9]/", '', $params['district']) : null);
            $data = $Geo->init($did);
            break;

        case 'getRegionsList':
            $did = preg_replace("/[^0-9]/", '', $_GET['did']);
            $data = [
                'regions'=>$Geo->getDistrictRegions($did),
                'cityList'=>$Geo->getDistrictCity($did)
            ];
            break;

        case 'getCityList':
            $rid = preg_replace("/[^0-9]/", '', $_GET['rid']);
            $data = [
                'cityList'=>$Geo->getRegionCity($rid),
                'popularCity'=>[],
            ];
            break;
        
        default:
            return \Core\Lib::fatalResponse(\Core\Lib::$fatalMsg);
    }

    return \Core\Lib::getResponse($data);

}else
    return \Core\Lib::fatalResponse(\Core\Lib::$fatalMsg);