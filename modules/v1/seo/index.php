<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 05.02.2016
 * Time: 15:37
 */

use \Symfony\Component\HttpFoundation\Response;
use \Mastodont\Api\v1\Seo;

$Redirects = new Seo\Redirects();

$data = [];
$params = \Core\Lib::getParams();

switch($_SERVER['REQUEST_METHOD']){
    case 'GET':

        switch ($method){

            case 'redirectsLoad':
                $data = $Redirects->get();
                break;
            
            default:
                return \Core\Lib::fatalResponse(\Core\Lib::$fatalMsg);
        }

        $data = \Core\Lib::getResponse($data);
        break;

    case 'POST':

        switch ($method){

            case 'redirectsSave':
                $data = $Redirects->save(json_decode($req->getContent(), true)['r']);
                break;

            default:
                return \Core\Lib::fatalResponse(\Core\Lib::$fatalMsg);
        }

        $data = \Core\Lib::postResponse($data);
        break;

}

return$data;