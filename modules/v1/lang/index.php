<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 24.01.2016
 * Time: 3:53
 */

$locale = isset($_REQUEST['locale']) ? $_REQUEST['locale'] : 'rus';
$dir = implode(DIRECTORY_SEPARATOR, [LangDir, $locale]);

$Lang = new \Mastodont\System\Lang();

//$system = include $dir . '/system.php';
//$user = include $dir . '/user.php';
//$data = array_merge($system, $user);

$data = $Lang->getData($locale);

$res = json_encode([ 'status'=>'error', 'statusCode'=>500, 'text'=>'Method Not found' ]);

switch($_SERVER['REQUEST_METHOD']){
    case 'GET':
        switch ($method){
            case 'getData':
                return \Core\Lib::getResponse($data);

            default:
                return \Core\Lib::fatalResponse($res);
        }

    default:
        return \Core\Lib::fatalResponse($res);
}


