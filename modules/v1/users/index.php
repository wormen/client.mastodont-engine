<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 25.01.2016
 * Time: 7:31
 */

use \Symfony\Component\HttpFoundation\Response;


$Users = new \Mastodont\Api\v1\Users();

$data = [];
$params = \Core\Lib::getParams();

switch($_SERVER['REQUEST_METHOD']){
    case 'GET':

        switch ($method){

            case 'loadList':
                $data = $Users->loadList();
                break;

            case 'edit':
                $data = $Users->edit($id);
                break;

            case 'checkLogin':
                $data = $Users->checkLogin($params['check']);
                break;

            case 'checkEmail':
                $data = $Users->checkEmail($params['check']);
                break;

            case 'getID':// получаем данные пользователя по id
                $type = isset($params['type']) ? $params['type'] : 'user';
                $u = $Users->decodeData($type);

                if($u['u'] == $id)
                    $data = $Users->getID($app->escape($id));
                else
                    $data = '';
                break;

            default:
                return \Core\Lib::fatalResponse(\Core\Lib::$fatalMsg);
        }

        $data = \Core\Lib::getResponse($data);
        break;

    case 'POST':
        $d = json_decode($req->getContent(), true);

        switch ($method){

            case 'save':
                $data =  $data = $Users->save($d);
                break;

            case 'checkPass':
                $data = $Users->checkPassword($d['uid'], $d['password']);
                break;

            case 'repairPass':
                $data = $Users->repairPass($d['d']);
                break;

            case 'register':
                $data = $Users->newUser($d);
                return json_encode($data);

            default:
                return \Core\Lib::fatalResponse(\Core\Lib::$fatalMsg);
        }

        $data = \Core\Lib::postResponse($data);
        break;

    case 'PUT':

        switch ($method){

            case 'update':
                $data = $Users->update(['_id'=>$id], json_decode($req->getContent(), true));
                break;

            default:
                return \Core\Lib::fatalResponse(\Core\Lib::$fatalMsg);
        }

        $data = \Core\Lib::putResponse($data);
        break;

    case 'DELETE':

        switch ($method){

            case 'remove':
                $data = $Users->delete($id);
                break;

            default:
                return \Core\Lib::fatalResponse(\Core\Lib::$fatalMsg);
        }

        $data = \Core\Lib::deleteResponse($data);
        break;
}


return$data;