<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 08.02.2016
 * Time: 10:33
 */

use \Symfony\Component\HttpFoundation\Response;

$Service = new \Cli\Service();

$data = [];
$params = \Core\Lib::getParams();

switch($_SERVER['REQUEST_METHOD']){
    case 'GET':

        switch ($method){

            case 'loadList':
                $data = $Service->fullList();
                break;

            default:
                return \Core\Lib::fatalResponse(\Core\Lib::$fatalMsg);
        }

        $data = \Core\Lib::getResponse($data);
        break;

    case 'POST':
        switch ($method) {

//            case 'delete':
//                $data =  $Class->{$method}($id);
//                break;

            default:
                return \Core\Lib::fatalResponse(\Core\Lib::$fatalMsg);
        }

        $data = \Core\Lib::postResponse($data);
        break;

    case 'PUT':
        switch ($method) {

//            case 'delete':
//                $data =  $Class->{$method}($id);
//                break;

            default:
                return \Core\Lib::fatalResponse(\Core\Lib::$fatalMsg);
        }

        $data = \Core\Lib::putResponse($data);
        break;

    case 'DELETE':

        switch ($method) {

            case 'remove':
                $data =  $Class->delete($id);
                break;

            default:
                return \Core\Lib::fatalResponse(\Core\Lib::$fatalMsg);
        }

        $data = \Core\Lib::deleteResponse($data);
        break;
}


return$data;