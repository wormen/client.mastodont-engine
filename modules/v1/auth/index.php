<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 27.01.2016
 * Time: 9:48
 */

use \Symfony\Component\HttpFoundation\Response;

$Class = new \Mastodont\Api\v1\Users();
$Session = new \Core\Session();
$params = \Core\Lib::getParams();


$data = [];

try{

    switch($_SERVER['REQUEST_METHOD']){
        case 'GET':

            switch ($method){

                case 'singIn':

                    $isAdmin = (isset($params['type']) && $params['type'] == 'admin' ? true : false);
                    $checkAuth = $Class->auth($app->escape($_SERVER['PHP_AUTH_USER']), $app->escape($_SERVER['PHP_AUTH_PW']), $isAdmin);


                    if(gettype($checkAuth) == 'boolean')
                        return json_encode([ 'statusCode'=>200, 'result'=>'ok']);

                    else{
                        $data = [
                            'statusCode'=>401,
                            'error'=>$checkAuth,
                            'text'=>(isset($lang['auth'][$checkAuth]) ? $lang['auth'][$checkAuth] : ''),
                        ];

                        switch($checkAuth){
                            case 'NOT_FOUND':
                                $status = 401;
                                $res = new Response(json_encode($data), $status);
                                $res->send();
                                die;

                            case 'WRONG_PASSWORD':
                            case 'ACCESS_DENIED':
                            case 'BLOCKED':
                                $status = 403;
                                $data['statusCode'] = $status;
                                $res = new Response(json_encode($data), $status);
                                $res->send();
                                die;

                        }
                    }
                    break;


                case 'check':
                    $type = ( isset($params['type']) && $params['type'] == 'admin' ? 'admin' : 'user');
                    $user = $Class->decodeData($type);

                    $s = $Session->checkAuth($user['u']);
                    if($s) return 'ok';
                    else{
                        $Class->logOut(isset($params['type']) ? $params['type'] : 'admin');
                        $status = 403;
                        $res = new Response(json_encode([ 'statusCode'=>$status, 'error'=>'SESSION_END', ]), $status);
                        $res->send();
                        die;
                    }
                    break;

                case 'logout':
                    $data = $Class->logOut(isset($params['type']) ? $params['type'] : 'admin');
                    break;

                default:
                    return \Core\Lib::fatalResponse(\Core\Lib::$fatalMsg);
            }

            $data = \Core\Lib::getResponse($data);
            break;

    }

    return$data;

}catch(\Exception $e){

    return \Core\Lib::fatalResponse(\Core\Lib::$fatalMsg);

}







//if(isset($params['logout']))
//    return $Class->logOut(isset($params['type']) ? $params['type'] : 'admin');

//if(isset($_REQUEST['type']) && ($_REQUEST['type'] == 'admin' || $_REQUEST['type'] == 'check-user')){

//    $type = ($_REQUEST['type'] == 'admin' ? 'admin' : 'user');
//    $user = $Class->decodeData($type);
//
//    $s = $Session->checkAuth($user['u']);
//    if($s) return 'ok';
//    else{
//        $Class->logOut(isset($_REQUEST['type']) ? $_REQUEST['type'] : 'admin');
//        $status = 403;
//        $res = new Response(json_encode([ 'statusCode'=>$status, 'error'=>'SESSION_END', ]), $status);
//        $res->send();
//        die;
//    }

//}else{

//    $isAdmin = (isset($_REQUEST['type']) && $_REQUEST['type'] == 'admin' ? true : false);
//    $checkAuth = $Class->auth($app->escape($_SERVER['PHP_AUTH_USER']), $app->escape($_SERVER['PHP_AUTH_PW']), $isAdmin);
//
//    if(gettype($checkAuth) == 'boolean')
//        return [ 'statusCode'=>200, 'result'=>'ok'];
//
//    else{
//        $data = [
//            'statusCode'=>401,
//            'error'=>$checkAuth,
//            'text'=>(isset($lang['auth'][$checkAuth]) ? $lang['auth'][$checkAuth] : ''),
//        ];
//
//        switch($checkAuth){
//            case 'NOT_FOUND':
//                $status = 401;
//                $res = new Response(json_encode($data), $status);
//                $res->send();
//                die;
//
//            case 'WRONG_PASSWORD':
//            case 'ACCESS_DENIED':
//            case 'BLOCKED':
//                $status = 403;
//                $data['statusCode'] = $status;
//                $res = new Response(json_encode($data), $status);
//                $res->send();
//                die;
//
//        }
//    }

//}
