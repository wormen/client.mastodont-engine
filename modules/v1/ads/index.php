<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 24.01.2016
 * Time: 0:00
 */

use \Symfony\Component\HttpFoundation\Response;

$Ads = new \Mastodont\Api\v1\Ads();

$data = [];
$params = \Core\Lib::getParams();


try{

    switch($_SERVER['REQUEST_METHOD']){
        case 'GET':

            switch ($method){

                case 'categoryEdit':
                    $data = $Ads->categoryEdit($app->escape($id));
                    break;

                case 'categoryCheckUrl':
                    $data = $Ads->categoryCheckUrl($params['url'], $params['locale']);
                    break;

                case 'categoryMiniList':
                    $data = $Ads->categoryMiniList(isset($params['tree']));
                    break;

                case 'adEdit':
                    $data = $Ads->postEdit($app->escape($id));
                    break;

                case 'adUserEdit':
                    $data = $Ads->postFindUrl($app->escape($id), true);
                    break;

                case 'adFindUrl':
                    $data = $Ads->postFindUrl($app->escape($_GET['user-edit']));
                    break;

                case 'adMiniList':
                    $data = $Ads->postMiniList();
                    break;

                default:
                    return \Core\Lib::fatalResponse(\Core\Lib::$fatalMsg);
            }

            $data = \Core\Lib::getResponse($data);



//        if(isset($_GET['type'])){
//            switch($app->escape($_GET['type'])){
//                case 'category':
//                    if(isset($_GET['id']))
//                        ++$data = $Ads->categoryEdit($app->escape($_GET['id']));
//                    else if(isset($_GET['check-url']))
//                        ++$data = $Ads->categoryCheckUrl($_GET['check-url'], $_GET['locale']);
//                    else
//                        ++$data = $Ads->categoryMiniList(isset($_GET['tree']));
//                    break;
//
//                case 'post':
//                    if(isset($_GET['id']))
//                        ++$data = $Ads->postEdit($app->escape($_GET['id']));
//
//                    else if(isset($_GET['user-edit']))
//                        ++$data = $Ads->postFindUrl($app->escape($_GET['user-edit']));
//
//                    else
//                        ++$data = $Ads->postMiniList();
//
//                    break;
//            }
//        }

            break;

        case 'POST':

            switch ($method){

                case 'categorySave':
                    $data = $Ads->categorySave(json_decode($req->getContent(), true));
                    break;

                case 'adSave':
                    $data = $Ads->postSave(json_decode($req->getContent(), true));
                    break;

                default:
                    return \Core\Lib::fatalResponse(\Core\Lib::$fatalMsg);
            }

            $data = \Core\Lib::postResponse($data);



//        $type = $req->get('type');
//        $id = $req->get('id');
//
//        if( isset($type) ){
//
//            switch($type){
//                case 'category':
//                    if(isset($id))
//                        $data = $Ads->categoryUpdate(['_id'=>$id], json_decode($req->getContent(), true));
//                    else
//                        $data = $Ads->categorySave(json_decode($req->getContent(), true));
//                    break;
//
//                case 'post':
//                    if(isset($id))
//                        $data = $Ads->postUpdate(['_id'=>$id], json_decode($req->getContent(), true));
//                    else
//                        $data = $Ads->postSave(json_decode($req->getContent(), true));
//                    break;
//            }
//
//            $data = $data ? new Response('', 201) : new Response($lang['incorrectData'], 400);
//        }else
//            $data = new Response('', 500);

            break;

        case 'PUT':

            switch ($method) {

                case 'categoryUpdate':
                    $data = $Ads->categoryUpdate(['_id' => $app->escape($id)], json_decode($req->getContent(), true));
                    break;

                case 'adUpdate':
                    $data = $Ads->postUpdate(['_id' => $app->escape($id)], json_decode($req->getContent(), true));
                    break;

                default:
                    return \Core\Lib::fatalResponse(\Core\Lib::$fatalMsg);
            }

            $data = \Core\Lib::putResponse($data);






//        $type = $req->get('type');
//        if( isset($type) ){
//
//            switch($type){
//                case 'category':
//                    $data = $Ads->categoryUpdate(['_id'=>$app->escape($req->get('id'))], json_decode($req->getContent(), true));
//                    break;
//
//                case 'post':
//                    $data = $Ads->postUpdate(['_id'=>$app->escape($req->get('id'))], json_decode($req->getContent(), true));
//                    break;
//            }
//
//            $data = $data ? new Response('', 200) : new Response($lang['incorrectData'], 400);
//
//        }else
//            $data = new Response('', 500);

            break;

        case 'DELETE':

            switch ($method) {

                case 'categoryDelete':
                    $data = $Ads->categoryDelete($app->escape($id));
                    break;

                case 'adDelete':
                    $data = $Ads->postDelete($app->escape($id));
                    break;

                default:
                    return \Core\Lib::fatalResponse(\Core\Lib::$fatalMsg);
            }

            $data = \Core\Lib::deleteResponse($data);


//        $type = $req->get('type');
//        if( isset($type) ){
//            switch($type){
//                case 'category':
//                    $data = $Ads->categoryDelete($app->escape($req->get('id')));
//                    break;
//
//                case 'post':
//                    $data = $Ads->postDelete($app->escape($req->get('id')));
//                    break;
//            }
//
//
//            $data = $data ? new Response('', 200) : new Response(json_encode([
//                'code'=>'notFound',
//                'statusCode'=>400,
//                'error'=>$lang['notFound']
//            ]), 404);
//        }
            break;
    }

    return$data;
    
}catch(\Exception $e){

    return \Core\Lib::fatalResponse(\Core\Lib::$fatalMsg);

}