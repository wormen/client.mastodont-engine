<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 13.02.2016
 * Time: 11:52
 */

use \Symfony\Component\HttpFoundation\Response;
$Class = new \Mastodont\Api\v1\Contacts();

$data = [];
$params = \Core\Lib::getParams();

switch($_SERVER['REQUEST_METHOD']){
    case 'GET':

        switch ($method){

            case 'getData':
                $data = $Class->get();
                break;

            default:
                return \Core\Lib::fatalResponse(\Core\Lib::$fatalMsg);
        }

        $data = \Core\Lib::getResponse($data);
        break;

    case 'POST':

        switch ($method){

            case 'saveData':
                $data =  $Class->set(json_decode($req->getContent(), true)['all']);
                break;

            default:
                return \Core\Lib::fatalResponse(\Core\Lib::$fatalMsg);
        }

        $data = \Core\Lib::postResponse($data);
        break;

    case 'DELETE':

        switch ($method) {

            case 'deleteBlock':
                $data =  $Class->delete($id);
                break;

            default:
                return \Core\Lib::fatalResponse(\Core\Lib::$fatalMsg);
        }

        $data = \Core\Lib::deleteResponse($data);
        break;

}

return$data;