<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 24.01.2016
 * Time: 0:00
 */

$Class = new \Mastodont\Api\v1\Articles();

$data = [];
$params = \Core\Lib::getParams();

if(!method_exists($Class, $method))
    return \Core\Lib::fatalResponse(json_encode([ 'status'=>'error', 'statusCode'=>500, 'text'=>'Method Not found' ]));

try{

    switch($_SERVER['REQUEST_METHOD']){
        case 'GET':
            
            switch ($method){
                case 'categoryMiniList':
                    $data =  $Class->{$method}(isset($params['tree']), isset($params['options']));
                    break;

                case 'categoryEdit':
                case 'postEdit':
                    $data = $Class->{$method}($id);
                    break;

                case 'categoryCheckUrl':
                case 'postCheckUrl':
                    $data =  $Class->{$method}($params['url'], $params['locale']);
                    break;

                case 'postMiniList':
                    $data =  $Class->{$method}();
                    break;

                default:
                    return \Core\Lib::fatalResponse(\Core\Lib::$fatalMsg);
            }

            $data = \Core\Lib::getResponse($data);
            break;

        case 'POST':

            switch ($method){

                case 'postSave':
                case 'categorySave':
                    $data =  $Class->{$method}(json_decode($req->getContent(), true));
                    break;

                default:
                    return \Core\Lib::fatalResponse(\Core\Lib::$fatalMsg);
            }

            $data = \Core\Lib::postResponse($data);
            break;

        case 'PUT':

            switch ($method) {

                case 'postUpdate':
                case 'categoryUpdate':
                    $data = $Class->{$method}(['_id' => $id], json_decode($req->getContent(), true));
                    break;

                default:
                    return \Core\Lib::fatalResponse(\Core\Lib::$fatalMsg);
            }

            $data = \Core\Lib::putResponse($data);
            break;

        case 'DELETE':

            switch ($method) {

                case 'categoryDelete':
                case 'postDelete':
                    $data =  $Class->{$method}($id);
                    break;

                default:
                    return \Core\Lib::fatalResponse(\Core\Lib::$fatalMsg);
            }

            $data = \Core\Lib::deleteResponse($data);
            break;
    }

    return$data;
    
}catch(\Exception $e){

    return \Core\Lib::fatalResponse(\Core\Lib::$fatalMsg);

}
