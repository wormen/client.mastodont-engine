<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 09.02.2016
 * Time: 19:25
 */

use \Symfony\Component\HttpFoundation\Response;
$Faq = new \Mastodont\Api\v1\Faq();

$data = [];
$params = \Core\Lib::getParams();

switch($_SERVER['REQUEST_METHOD']){
    case 'GET':

        switch ($method){

            case 'categoryMiniList':
            case 'categoryFullList':
            case 'postMiniList':
            case 'postFullList':
                $data = $Faq->{$method}($params);
                break;

            case 'categoryEdit':
            case 'postEdit':
            $data = $Faq->{$method}($id);
                break;

            default:
                return \Core\Lib::fatalResponse(\Core\Lib::$fatalMsg);
        }

        $data = \Core\Lib::getResponse($data);
        break;

    case 'POST':

        switch ($method){

            case 'categorySave':
            case 'postSave':
                $data = $Faq->{$method}(json_decode($req->getContent(), true));
                break;

            default:
                return \Core\Lib::fatalResponse(\Core\Lib::$fatalMsg);
        }

        $data = \Core\Lib::postResponse($data);
        break;

    case 'PUT':

        switch ($method) {

            case 'postUpdate':
            case 'categoryUpdate':
                $data = $Class->{$method}(['_id' => $id], json_decode($req->getContent(), true));
                break;

            default:
                return \Core\Lib::fatalResponse(\Core\Lib::$fatalMsg);
        }

        $data = \Core\Lib::putResponse($data);
        break;

    case 'DELETE':

        switch ($method) {

            case 'categoryDelete':
            case 'postDelete':
                $data =  $Class->{$method}($id);
                break;

            default:
                return \Core\Lib::fatalResponse(\Core\Lib::$fatalMsg);
        }

        $data = \Core\Lib::deleteResponse($data);
        break;
}

return$data;