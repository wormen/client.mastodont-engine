<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 14.02.2016
 * Time: 1:50
 */

$Class = new \Mastodont\Api\v1\Letters();

$data = [];
$params = \Core\Lib::getParams();

switch($_SERVER['REQUEST_METHOD']){
    case 'GET':

        switch ($method){

            case 'edit':
                $data = $Class->{$method}($id);
                break;

            case 'miniList':
                $data = $Class->{$method}();
                break;

            default:
                return \Core\Lib::fatalResponse(\Core\Lib::$fatalMsg);
        }

        $data = \Core\Lib::getResponse($data);
        break;

    case 'POST':

        switch ($method){

            case 'save':
                $data =  $Class->{$method}(json_decode($req->getContent(), true));
                break;

            default:
                return \Core\Lib::fatalResponse(\Core\Lib::$fatalMsg);
        }

        $data = \Core\Lib::postResponse($data);
        break;

    case 'PUT':

        switch ($method) {

            case 'update':
                $data = $Class->{$method}(['_id' => $id], json_decode($req->getContent(), true));
                break;

            default:
                return \Core\Lib::fatalResponse(\Core\Lib::$fatalMsg);
        }

        $data = \Core\Lib::putResponse($data);
        break;

    case 'DELETE':

        switch ($method) {

            case 'delete':
                $data =  $Class->{$method}($id);
                break;

            default:
                return \Core\Lib::fatalResponse(\Core\Lib::$fatalMsg);
        }

        $data = \Core\Lib::deleteResponse($data);
        break;
}


return$data;