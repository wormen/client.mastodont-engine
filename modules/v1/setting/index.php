<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 24.01.2016
 * Time: 0:00
 */

use \Symfony\Component\HttpFoundation\Response;

$Setting = new \Core\Setting();
$data = [];

switch($_SERVER['REQUEST_METHOD']){
    case 'GET':

        switch ($method){

            case 'getData':
                $data = $Setting->get();
                break;

            case 'getTemplates':
                $list = glob(TemplateDir.DIRECTORY_SEPARATOR.'*');
                foreach($list as $l){
                    $item = str_replace(TemplateDir.DIRECTORY_SEPARATOR, '', $l);
                    if($item != 'default')
                        $data[] = ['v'=>$item, 'l'=>ucfirst($item)];
                }
                break;

            default:
                return \Core\Lib::fatalResponse(\Core\Lib::$fatalMsg);
        }

        $data = \Core\Lib::getResponse($data);
        break;

    case 'POST':

        switch ($method){

            case 'saveData':
                $Setting->set(json_decode($req->getContent(), true));
                break;

            default:
                return \Core\Lib::fatalResponse(\Core\Lib::$fatalMsg);
        }

        $data = \Core\Lib::postResponse($data);
        break;

}

return$data;