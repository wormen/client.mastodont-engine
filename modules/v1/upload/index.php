<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 25.01.2016
 * Time: 20:38
 */

use \Symfony\Component\HttpFoundation\Response;

$Files = new \Mastodont\System\Files();

error_reporting(E_ALL);

$data = '';

switch($_SERVER['REQUEST_METHOD']){
    case 'GET':
    case 'POST':
        $upload_handler = new \Mastodont\System\Uploader\UploadHandler();
        break;

    case 'PUT':
        if( $app->escape($req->get('set-prime')) ){
            $data = $Files->setPrimary($app->escape($req->get('set-prime')), json_decode($req->getContent(), true));

        }else
            $data = false;

        $data = $data ? new Response('', 200) : new Response(json_encode([
            'code'=>'incorrectData',
            'statusCode'=>400,
            'error'=>$lang['incorrectData']
        ]), 400);
        break;

    case 'DELETE':
        $data = $Files->delete($app->escape($req->get('id')));
        $data2 = false;

        if(isset($_REQUEST['module']))
            $data2 = $Files->moduleDelete($_REQUEST['module'], $req->get('id'));

        $data = ($data || $data2) ? new Response('', 200) : new Response(json_encode([
            'code'=>'notFound',
            'statusCode'=>404,
            'error'=>$lang['notFound']
        ]), 404);
        break;
}


return$data;