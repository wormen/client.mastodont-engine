<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 25.01.2016
 * Time: 7:31
 */

use \Symfony\Component\HttpFoundation\Response;

$Fm = new \Core\Fm();
$Static = new \Mastodont\Api\v1\StaticPage();

$data = [];
$params = \Core\Lib::getParams();


switch($_SERVER['REQUEST_METHOD']){
    case 'GET':

        switch ($method){

            case 'edit':
                $data = $Static->{$method}($id);
                break;

            case 'loadList':
                $data = $Static->{$method}();
                break;

            case 'checkUrl':
                $data = $Static->checkUrl($params['url'], $params['locale']);
                break;

            default:
                return \Core\Lib::fatalResponse(\Core\Lib::$fatalMsg);
        }

        $data = \Core\Lib::getResponse($data);
        break;

    case 'POST':

        switch ($method){

            case 'save':
                $data = $Static->{$method}(json_decode($req->getContent(), true));
                break;

            case 'createTmpDir':
                $Fm->MkDir(implode(DIRECTORY_SEPARATOR, [UploadsDir, 'static', $app->escape($req->get('dir'))]));
                break;

            default:
                return \Core\Lib::fatalResponse(\Core\Lib::$fatalMsg);
        }

        $data = \Core\Lib::postResponse($data);
        break;

    case 'PUT':

        switch ($method) {

            case 'update':
                $data = $Static->{$method}(['_id' => $id], json_decode($req->getContent(), true));
                break;

            default:
                return \Core\Lib::fatalResponse(\Core\Lib::$fatalMsg);
        }

        $data = \Core\Lib::putResponse($data);
        break;

    case 'DELETE':

        switch ($method) {

            case 'remove':
                $data = $Static->delete($id);
                break;

            default:
                return \Core\Lib::fatalResponse(\Core\Lib::$fatalMsg);
        }

        $data = \Core\Lib::putResponse($data);
        break;
}


return$data;