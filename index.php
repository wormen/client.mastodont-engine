<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 19.01.2016
 * Time: 20:41
 */

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

$DS = DIRECTORY_SEPARATOR;
$runCli = true;
require_once implode($DS, [__DIR__, 'config', 'constants.php']);

//----------------------------------------------------------------------------------------------------------------------

$app = new Silex\Application();
$req = new Request();
$res = new Response();

try{

    // проверяем пользователя в черном списке
    $Security->CheckBanList();

    $Core->init($app);
    $tpl = $Tpl->CheckTemplate($T['vars']['site']['template']);

    //главная страница
    $app->get('/', function (Request $req) use ($app, $tpl, $T) {
        $T = $GLOBALS['Tpl']->getDataTpl($req);
        return include implode(DIRECTORY_SEPARATOR, [$tpl['path'], 'index.php']);
    })->bind('main');

    // страница поиска
    $app->get('/search?q={q}', function(Request $req, $q) use ($app, $tpl, $T) {
        $T = $GLOBALS['Tpl']->getDataTpl($req);
        return include implode(DIRECTORY_SEPARATOR, [$tpl['path'], 'index.php']);
    })->bind('search');

    $app->get('/{module}', function(Request $req, $module) use ($app, $tpl, $T) {
        $T = $GLOBALS['Tpl']->getDataTpl($req, $module);
        return include implode(DIRECTORY_SEPARATOR, [$tpl['path'], 'index.php']);
    });

    $app->get('/{module}/{section}', function(Request $req, $module, $section) use ($app, $tpl, $T) {
        $T = $GLOBALS['Tpl']->getDataTpl($req, $module, $section);
        return include implode(DIRECTORY_SEPARATOR, [$tpl['path'], 'index.php']);
    });

    $app->get('/{module}/{section}/{item}', function(Request $req, $module, $section, $item) use ($app, $tpl, $T) {
        $T = $GLOBALS['Tpl']->getDataTpl($req, $module, $section, $item);
        return include implode(DIRECTORY_SEPARATOR, [$tpl['path'], 'index.php']);
    });


//    Request::setTrustedProxies([$ip]);
    $app->run();

}catch(Exception $e){
    $Log->set($e, 'userInterface');
}


//todo прикрутить оформление, встраивание в контент
//    $T['tplPage'] = 404;
//    ExitPage();

function ExitPage($code=404){
    $res = new Response(include implode(DIRECTORY_SEPARATOR, [$GLOBALS['tpl']['path'], 'pages', $code.'.php']), $code);
    $res->send();
    die;
}