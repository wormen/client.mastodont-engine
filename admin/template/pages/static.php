<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 25.01.2016
 * Time: 15:56
 */

$moduleUrl = '/admin/?module=static';

if(isset($_GET['action']) && ($_GET['action'] == 'add' || $_GET['action'] == 'edit')){

    $multilang = '';//'<locale-content :lang.sync="lang" :locale="activeLocale"></locale-content>';

    $cityList = '';//'<city-list :lang.sync="lang" :city="page[activeLocale].city_id" :locale="activeLocale"></city-list>';

    $pageContent = <<<HTML
<div class="btn-group bottom-15">
    <a class="btn btn-default" href="{$moduleUrl}&action=add">{$lang['btns']['add']}</a>
</div>

<div class="btn-group bottom-15">
    <a class="btn btn-default" href="{$moduleUrl}">{$lang['static']['listPages']}</a>
</div>

<div class="clearfix"></div>
<hr class="short margin-top-none">

<div>
    <div class="thumbnail">
        <div class="caption">

            <div class="col-md-12">

                <div class="row clearfix">
                    <div class="col-xs-5">
                        <div class="form-group">
                            <label>{$lang['static']['name']}</label>
                            <input class="form-control" type="text" v-model="page.name" @keyup="transtaleUrl | debounce 150">
                        </div>

                        <div class="form-group">
                            <label>{$lang['static']['url']}</label>

                            <!--todo валидация URL-->
                            <input class="form-control" type="text" v-model="page.url" @keyup="checkLink | debounce 150">
                        </div>

                        <div class="form-group">
                            <label>{$lang['static']['title']}</label>
                            <input class="form-control" type="text" v-model="page.title">
                        </div>

                        <div class="form-group">
                            <label>{$lang['static']['meta_description']}</label>
                            <input class="form-control" type="text" v-model="page.meta_description">
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label" style="padding-top: 10px;"> {$lang['static']['enabled']}</label>
                            <div class="col-lg-2">
                                <switch-box :value.sync="page.enabled"></switch-box>
                            </div>
                        </div>


                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="row clearfix top-20">

                    <div class="form-group col-xs-12">
                        <label>{$lang['static']['description']}: </label>

                        <fm-btn :dir="tmpDir" label="{$lang['btns']['fm']}"></fm-btn>

                        <editor :value.sync="page.description" name="description"></editor>
                    </div>
                </div>

            </div>
            <div class="clearfix"></div>

        </div>
    </div>


    <!-- FootLine -->
    <div class="submit-pane">
        <button type="submit" class="btn btn-success" @click="onSave">
            <b>{$lang['btns']['save']}</b>
        </button>

        {$multilang}
        {$cityList}

    </div>
    <!-- FootLine [E] -->

</div>

HTML;
}else{

    $pageContent = <<<HTML
<div class="col-md-12">
    <div class="btn-group bottom-15">
        <a class="btn btn-default" href="{$moduleUrl}&action=add">{$lang['btns']['add']}</a>
    </div>
    <div class="clearfix"></div>
    <hr class="short margin-top-none">
</div>

<div class="col-md-12">
    <div class="panel panel-visible">
        <div class="panel-body">

            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>{$lang['label']['name']}</th>
                        <th class="col-xs-1">{$lang['label']['functions']}</th>
                    </tr>
                </thead>
                <tbody>

                    <tr v-for="S in pageList" style="display:none;" v-show="pageList.length>0">
                        <td>{{S.name}}</td>
                        <td class="function">
                            <a class="btn btn-xs btn-info" href="{$moduleUrl}&action=edit&id={{S._id}}">
                                <i class="glyphicons glyphicons-pencil"></i>
                            </a>
                            <a class="btn btn-xs btn-danger" href="#" @click="onDelete(S._id, \$event)">
                                <i class="glyphicon glyphicon-remove"></i>
                            </a>
                        </td>
                    </tr>

                </tbody>
            </table>

        </div>
    </div>
</div>
HTML;
}

return$pageContent;
