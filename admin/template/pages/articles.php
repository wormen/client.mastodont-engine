<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 17.02.2016
 * Time: 10:06
 */

$moduleUrl = '/admin/?module=articles';

$tabList = [
    ["nameBlock"=> "category", "isActive"=> true],
    ["nameBlock"=> "posts", "isActive"=> false],
];

$pageContent = <<<HTML
<div class="spinner" v-show="isLoading" style="display:none;"></div>
<div class="col-md-12">
    <div class="btn-group bottom-15">
        <a class="btn btn-default dropdown-toggle" data-toggle="dropdown">
            {$lang['btns']['add']}
            <span class="caret"></span>
        </a>
        <ul class="dropdown-menu checkbox-persist pull-left text-left" role="menu">
            <li><a href="{$moduleUrl}&action=add-category"> {$lang['articles']['addCategory']} </a></li>
            <li><a href="{$moduleUrl}&action=add-post"> {$lang['articles']['addPost']} </a></li>
        </ul>
    </div>
</div>

<div class="clearfix"></div>
<hr class="short margin-top-none">
HTML;

if(isset($_GET['action'])){

    if($_GET['action'] == 'add-category' || $_GET['action'] == 'edit-category'){

        $pageContent .= <<<HTML
<div>
    <div class="thumbnail">
        <div class="caption">

            <div class="col-md-12">

                <div class="row clearfix">
                    <div class="col-xs-5">
                        <div class="form-group">
                            <label>{$lang['articles']['name']}</label>
                            <input class="form-control" type="text" v-model="category.name" @keyup="transtaleCategoryUrl | debounce 150">
                        </div>

                        <div class="form-group">
                            <label>H1</label>
                            <input class="form-control" type="text" v-model="category.h1">
                        </div>

                        <div class="form-group">
                            <label>{$lang['articles']['urlС']}</label>
                            <input class="form-control" type="text" v-model="category.url" @keyup="onCheckUrlCategory | debounce 150">
                        </div>

                        <div class="form-group">
                            <label>{$lang['articles']['title']}</label>
                            <input class="form-control" type="text" v-model="category.title">
                        </div>

                        <div class="form-group">
                            <label>{$lang['articles']['meta_description']}</label>
                            <input class="form-control" type="text" v-model="category.meta_description">
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label" style="padding-top: 10px;"> {$lang['articles']['enabled']}</label>
                            <div class="col-lg-2">
                                <switch-box :value.sync="category.enabled"></switch-box>
                            </div>
                        </div>


                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="row clearfix top-20">

                    <div class="form-group col-xs-12">
                        <label>{$lang['articles']['description']}: </label>
                        <editor :value.sync="category.description" name="description"></editor>
                    </div>
                </div>

            </div>
            <div class="clearfix"></div>

        </div>
    </div>


    <!-- FootLine -->
    <div class="submit-pane">
        <button type="submit" class="btn btn-success" @click="onSaveCategory">
            <b>{$lang['btns']['save']}</b>
        </button>
    </div>
    <!-- FootLine [E] -->

</div>
HTML;

    }

    if($_GET['action'] == 'add-post' || $_GET['action'] == 'edit-post'){

        $pageContent .= <<<HTML
<div>
    <div class="thumbnail">
        <div class="caption">

            <div class="col-md-12">

                <div class="row clearfix">
                    <div class="col-xs-5">
                        <div class="form-group">
                            <label>{$lang['articles']['name']}</label>
                            <input class="form-control" type="text" v-model="post.name" @keyup="transtalePostUrl | debounce 150">
                        </div>

                        <div class="form-group">
                            <label>H1</label>
                            <input class="form-control" type="text" v-model="post.h1">
                        </div>

                        <div class="form-group">
                            <label>{$lang['articles']['urlP']}</label>
                            <input class="form-control" type="text" v-model="post.url" @keyup="onCheckUrlCategory | debounce 150">
                        </div>

                        <div class="form-group">
                            <label>{$lang['articles']['title']}</label>
                            <input class="form-control" type="text" v-model="post.title">
                        </div>

                        <div class="form-group">
                            <label>{$lang['articles']['meta_description']}</label>
                            <input class="form-control" type="text" v-model="post.meta_description">
                        </div>

                        <div class="form-group">
                            <label>{$lang['articles']['category']}</label>
                            <select class="form-control" v-model="post.category_id">
                                <option value="0"> ------ </option>
                                <option v-for="C in categoryList" :value="C._id"> {{C.name}} </option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label class="col-xs-2 control-label" style="padding-top: 10px;"> {$lang['articles']['enabled']}</label>
                            <div class="col-xs-7">
                                <switch-box :value.sync="post.enabled"></switch-box>
                            </div>
                        </div>


                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="row clearfix top-20">
                    <div class="form-group col-xs-12">
                        <label>{$lang['articles']['anonce']}: </label>
                        <editor :value.sync="post.anonce" name="anonce"></editor>
                    </div>
                </div>

                <div class="row clearfix top-20">
                    <div class="form-group col-xs-12">
                        <label>{$lang['articles']['description']}: </label>
                        <editor :value.sync="post.description" name="description"></editor>
                    </div>
                </div>

            </div>
            <div class="clearfix"></div>

        </div>
    </div>


    <!-- FootLine -->
    <div class="submit-pane">
        <button type="submit" class="btn btn-success" @click="onSavePost">
            <b>{$lang['btns']['save']}</b>
        </button>
    </div>
    <!-- FootLine [E] -->

</div>
HTML;

    }


}else{

    $tabs = '<ul class="nav nav-tabs" role="tablist">';
    $tabContent = '<div class="tab-content">';
    foreach($tabList as $t){

        $tabs .= '<li class="'.($t['isActive'] ? 'active' : '').'">
                            <a href="#'.$t['nameBlock'].'-block" data-toggle="tab">'.$lang['tabs']['articles'][$t['nameBlock']].'</a>
                        </li>';

        $tabContent .= '<div id="'.$t['nameBlock'].'-block" class="tab-pane'.($t['isActive'] ? ' active' : '').'">';
        $tabContent .= include implode(DIRECTORY_SEPARATOR, [AdminTemplate, 'blocks', 'articles', $t['nameBlock'].'.php']);
        $tabContent .= '</div>';

    }
    $tabContent .= '</div>';
    $tabs .= '</ul>';

    $pageContent .= <<<HTML
<div class="col-md-12">
    <div class="thumbnail">
        <div class="caption">

            {$tabs}
            {$tabContent}

        </div>
    </div>
</div>

HTML;

}


return$pageContent;