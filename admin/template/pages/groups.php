<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 24.01.2016
 * Time: 19:18
 */

$moduleUrl = '/admin/?module=groups';

if(isset($_GET['action']) && ($_GET['action'] == 'add' || $_GET['action'] == 'edit')){

    $protect = '';
    if($_GET['action'] == 'add' || (isset($_GET['debug']) && $_GET['debug'] == 1)){
        $sysName = '<input type="text" class="form-control" name="sys_name" value="" v-model="group.sys_name">';

        $protect = '<div class="form-group">
                        <label class="col-lg-4 control-label">'.$lang['groups']['protect'].'</label>
                        <div class="col-lg-4">
                            <switch-box :value.sync="group.protect"></switch-box>
                        </div>
                    </div>';
    }
    else{
        $sysName = '<div class="top-10" style="display:none;" v-show="group.sys_name">{{group.sys_name}}</div>';
    }



    $pageBlock = <<<HTML
<div class="col-md-12">
    <div class="btn-group bottom-15">
        <a class="btn btn-default" href="{$moduleUrl}">{$lang['groups']['list']}</a>
    </div>
    <div class="btn-group bottom-15">
        <a class="btn btn-default" href="{$moduleUrl}&action=add">{$lang['btns']['add']}</a>
    </div>
</div>
<div class="clearfix"></div>
<hr class="short margin-top-none">

<div class="col-md-12">

    <div class="thumbnail">
        <div class="caption">
            <form>

                <div class="col-md-6 col-lg-6">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="panel profile-panel">
                                <div class="panel-heading panel-visible">
                                    <div class="panel-title">
                                        <span class="glyphicons glyphicons-cogwheels"></span>
                                        &nbsp;{$lang['groups']['baseOptions']}
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="row">

                                        <div class="col-md-12">
                                            <div class="form-horizontal">

                                                <div class="form-group">
                                                    <label class="col-lg-3 control-label">{$lang['groups']['name']}</label>
                                                    <div class="col-lg-9">
                                                        <input type="text" class="form-control" name="name" value="" v-model="group.name">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-lg-3 control-label">{$lang['groups']['sysName']}</label>
                                                    <div class="col-lg-9">
                                                        {$sysName}
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>

                            <div class="panel profile-panel">
                                <div class="panel-heading panel-visible">
                                    <div class="panel-title"> <span class="glyphicons glyphicons-unlock"></span>{$lang['groups']['globalPermissions']}</div>
                                </div>
                                <div class="panel-body">
                                    <div class="row">

                                        <div class="col-md-12">
                                            <div class="form-horizontal">

                                                {$protect}

                                                <div class="form-group">
                                                    <label class="col-lg-4 control-label">{$lang['groups']['access_cp']}</label>
                                                    <div class="col-lg-4">
                                                        <switch-box :value.sync="group.access_cp"></switch-box>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-lg-4 control-label">{$lang['groups']['is_ban']}
                                                        <span class="glyphicons glyphicons-circle_question_mark" tooltip-placement="right" tooltip="Пользователи этой группы будут забанены"></span>
                                                    </label>
                                                    <div class="col-lg-4">
                                                        <switch-box :value.sync="group.is_ban"></switch-box>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-lg-4 control-label">{$lang['groups']['captcha']}
                                                        <span class="glyphicons glyphicons-circle_question_mark" tooltip-placement="right" tooltip="Включить отображение специального изображения, на котором пользователю необходимо прочитать надпись, доказав тем самым что он не бот."></span>
                                                    </label>
                                                    <div class="col-lg-4">
                                                        <switch-box :value.sync="group.captcha"></switch-box>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-lg-4 control-label"> {$lang['groups']['moderate']}
                                                        <span class="glyphicons glyphicons-circle_question_mark" tooltip-placement="right" tooltip="Включить отображение специального изображения, на котором пользователю необходимо прочитать надпись, доказав тем самым что он не бот."></span>
                                                    </label>
                                                    <div class="col-lg-4">
                                                        <switch-box id="moderate" :value.sync="group.moderate"></switch-box>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-lg-4 control-label">{$lang['groups']['sh_cls']}
                                                        <span class="glyphicons glyphicons-circle_question_mark" tooltip-placement="right" tooltip="Включить отображение специального изображения, на котором пользователю необходимо прочитать надпись, доказав тем самым что он не бот."></span>
                                                    </label>
                                                    <div class="col-lg-4">
                                                        <switch-box id="sh-cls" :value.sync="group.sh_cls"></switch-box>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-lg-4 control-label"> {$lang['groups']['enabled']}
                                                        <span class="glyphicons glyphicons-circle_question_mark"></span>
                                                    </label>
                                                    <div class="col-lg-4">
                                                        <switch-box id="enabled" :value.sync="group.enabled"></switch-box>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-lg-6">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="panel profile-panel">
                                <div class="panel-heading panel-visible">
                                    <div class="panel-title">
                                        <span class="glyphicons glyphicons-cogwheel"></span>
                                        {$lang['groups']['modulesData']}
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="row">

                                        <div class="col-md-12">
                                            <div class="form-horizontal">

                                                <div class="form-group">
                                                    <label class="col-lg-3 col-xs-5 control-label">{$lang['groups']['shopDiscount']}</label>
                                                    <div class="col-lg-2 col-xs-3">
                                                        <input type="text" class="form-control" name="group_discount" v-model="group.group_discount">
                                                    </div>
                                                    <div style="margin-top: 8px;">%</div>
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>

                            <div class="panel profile-panel">
                                <div class="panel-heading panel-visible">
                                    <div class="panel-title"> <span class="glyphicons glyphicons-notes"></span> {$lang['groups']['groupDescription']}</div>
                                </div>
                                <div class="panel-body">
                                    <div class="row">

                                        <div class="col-md-12">
                                            <div class="form-horizontal">
                                                <textarea class="form-control" name="description" v-model="group.description"></textarea>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>

            </form>
            <div class="clearfix"></div>
        </div>
    </div>


    <!-- FootLine -->
    <div class="submit-pane">
        <button type="submit" class="btn btn-success" @click="onSave()">
            <b>{$lang['btns']['save']}</b>
        </button>
    </div>
    <!-- FootLine [E] -->

</div>
HTML;

}else{

    $pageBlock = <<<HTML
<div class="col-md-12">
    <div class="btn-group bottom-15">
        <a class="btn btn-default" href="{$moduleUrl}&action=add">{$lang['btns']['add']}</a>
    </div>
</div>
<div class="clearfix"></div>
<hr class="short margin-top-none">

<div class="col-md-12">
    <div class="panel panel-visible">
        <div class="panel-heading">
            <div class="panel-title hidden-xs">
                <span class="glyphicon glyphicon-tasks"></span>
                {$lang['label']['listGroups']}
            </div>
        </div>

        <div class="panel-body padding-bottom-none">
            <table class="table table-striped table-bordered table-hover">

                <thead>
                    <tr>
                        <th>{$lang['label']['name']}</th>
                        <th class="col-xs-1 text-center">{$lang['label']['status']}</th>
                        <th class="col-xs-1 text-center">{$lang['label']['functions']}</th>
                    </tr>
                </thead>

                <tbody>

                    <tr v-for="L in list" track-by="\$index" style="display:none;" v-show="list.length>0">
                        <td>{{L.name}}</td>
                        <td>

                            <div class="btn btn-xs btn-default list status" :class="{active: L.enabled, inactive: !L.enabled}" @click="OnOff(L)"></div>
                        </td>
                        <td class="functions text-center">
                            <div class="btn-group">
                                <a class="btn btn-xs btn-default btn-gradient edit" href="{$moduleUrl}&action=edit&id={{L._id}}">
                                    <span class="glyphicon glyphicon-pencil"></span>
                                </a>
                            </div>
                            <div class="btn-group">
                                <a class="btn btn-xs btn-danger btn-gradient delete">
                                    <span class="glyphicon glyphicon-remove" @click="onDelete(L, \$event)"></span>
                                </a>
                            </div>
                        </td>
                    </tr>

                </tbody>

            </table>
        </div>
    </div>
</div>

HTML;
}

return$pageBlock;
