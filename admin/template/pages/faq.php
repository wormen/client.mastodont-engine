<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 09.02.2016
 * Time: 18:55
 */

$moduleUrl = '/admin/?module=faq';

$pageContent = <<<HTML
<div class="col-md-12">
    <div class="btn-group bottom-15">
        <a class="btn btn-default dropdown-toggle" data-toggle="dropdown">
            {$lang['btns']['add']}
            <span class="caret"></span>
        </a>
        <ul class="dropdown-menu checkbox-persist pull-left text-left" role="menu">
            <li><a href="{$moduleUrl}&action=add-category"> {$lang['faq']['addCategory']} </a></li>
            <li><a href="{$moduleUrl}&action=add-post"> {$lang['faq']['addPost']} </a></li>
        </ul>
    </div>
</div>

<div class="clearfix"></div>
<hr class="short margin-top-none">
HTML;


if(isset($_GET['action']) && ($_GET['action'] == 'add-category' || $_GET['action'] == 'edit-category')){

    $pageContent .= <<<HTML
<div>
    <div class="thumbnail">
        <div class="caption">

            <div class="col-md-12">

                <div class="row clearfix">
                    <div class="col-xs-5">
                        <div class="form-group">
                            <label>{$lang['faq']['name']}</label>
                            <input class="form-control" type="text" v-model="category.name">
                        </div>

<!--
                        <div class="form-group">
                            <label>{$lang['static']['url']}</label>
                            <input class="form-control" type="text" v-model="category.url" @keyup="checkLink | debounce 150">
                        </div>

                        <div class="form-group">
                            <label>{$lang['faq']['title']}</label>
                            <input class="form-control" type="text" v-model="category.title">
                        </div>

                        <div class="form-group">
                            <label>{$lang['faq']['meta_description']}</label>
                            <input class="form-control" type="text" v-model="category.meta_description">
                        </div>
-->

                        <div class="form-group">
                            <label class="col-lg-3 control-label" style="padding-top: 10px;"> {$lang['faq']['enabled']}</label>
                            <div class="col-lg-2">
                                <switch-box :value.sync="category.enabled"></switch-box>
                            </div>
                        </div>


                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="row clearfix top-20">

                    <div class="form-group col-xs-12">
                        <label>{$lang['faq']['description']}: </label>

                        <fm-btn :dir="tmpDir" label="{$lang['btns']['fm']}"></fm-btn>

                        <editor :value.sync="category.description" name="description"></editor>
                    </div>
                </div>

            </div>
            <div class="clearfix"></div>

        </div>
    </div>


    <!-- FootLine -->
    <div class="submit-pane">
        <button type="submit" class="btn btn-success" @click="onSaveCat">
            <b>{$lang['btns']['save']}</b>
        </button>
    </div>
    <!-- FootLine [E] -->

</div>
HTML;


}

else if(isset($_GET['action']) && ($_GET['action'] == 'add-post' || $_GET['action'] == 'edit-post')){

    $pageContent .= <<<HTML
<div>
    <div class="thumbnail">
        <div class="caption">

            <div class="col-md-12">

                <div class="row clearfix">
                    <div class="col-xs-5">
                        <div class="form-group">
                            <label>{$lang['faq']['name']}</label>
                            <input class="form-control" type="text" v-model="page.name" @keyup="transtaleUrl | debounce 150">
                        </div>

<!--
                        <div class="form-group">
                            <label>{$lang['static']['url']}</label>
                            <input class="form-control" type="text" v-model="page.url" @keyup="checkLink | debounce 150">
                        </div>

                        <div class="form-group">
                            <label>{$lang['faq']['title']}</label>
                            <input class="form-control" type="text" v-model="page.title">
                        </div>

                        <div class="form-group">
                            <label>{$lang['faq']['meta_description']}</label>
                            <input class="form-control" type="text" v-model="page.meta_description">
                        </div>
-->

                        <div class="form-group">
                            <label>{$lang['faq']['category']}</label>
                            <select class="form-control" v-model="page.category_id">
                                <option value="0"> ------ </option>
                                <option v-for="C in categoryList" :value="C._id"> {{C.name}} </option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label" style="padding-top: 10px;"> {$lang['faq']['enabled']}</label>
                            <div class="col-lg-2">
                                <switch-box :value.sync="page.enabled"></switch-box>
                            </div>
                        </div>


                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="row clearfix top-20">

                    <div class="form-group col-xs-12">
                        <label>{$lang['faq']['description']}: </label>

                        <fm-btn :dir="tmpDir" label="{$lang['btns']['fm']}"></fm-btn>

                        <editor :value.sync="page.description" name="description"></editor>
                    </div>
                </div>

            </div>
            <div class="clearfix"></div>

        </div>
    </div>


    <!-- FootLine -->
    <div class="submit-pane">
        <button type="submit" class="btn btn-success" @click="onSavePost">
            <b>{$lang['btns']['save']}</b>
        </button>
    </div>
    <!-- FootLine [E] -->

</div>
HTML;

}

else{

    $tabList = [
        ["nameBlock" => "category"],
        ["nameBlock" => "posts"],
    ];

    $btns = '<ul class="nav panel-tabs">';
    $bloks = '';
    foreach($tabList as $k=>$t){
        $t['isActive'] = $k==0;

        $btns .= '<li'.($t['isActive'] ? ' class="active"' : '').'>
                <a href="#'.$t['nameBlock'].'-block" data-toggle="tab">'.$lang['tabs']['faq'][$t['nameBlock']].'</a>
            </li>';

        $bloks .= '<div id="'.$t['nameBlock'].'-block" class="tab-pane'.($t['isActive'] ? ' active' : '').'">';
        $bloks .= include implode(DIRECTORY_SEPARATOR, [AdminTemplate, 'blocks', 'faq', $t['nameBlock'].'.php']);
        $bloks .= '</div>';
    }

    $btns .= '</ul>';


    $pageContent .= <<<HTML
<div class="panel">

    <div class="panel-heading">
        <div class="panel-title"></div>
        {$btns}
    </div>

    <div class="panel-body">
        <div class="tab-content padding-none border-none">{$bloks}</div>
    </div>

</div>
HTML;

}

return$pageContent;
