<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 25.01.2016
 * Time: 12:43
 */

return <<<HTML

<div class="col-md-12">
    <div class="btn-group bottom-15">
        <a class="btn btn-default" href="javascript:;" data-uk-modal="{target:'#menu'}" @click="onAdd">{$lang['btns']['add']}</a>
    </div>
    <div class="clearfix"></div>
    <hr class="short margin-top-none">
</div>

<div class="col-md-12">
    <div class="panel panel-visible">
        <div class="panel-body">

            <ul id="list" class="uk-nestable">
                <menu-item v-for="I in list | orderBy 'pos'" :item.sync="I" track-by="\$index"></category-item>
            </ul>

        </div>
    </div>
    
</div>

<div id="menu" class="uk-modal" aria-hidden="true" style="display: none; overflow-y: scroll;">
    <div class="uk-modal-dialog">
        <a href="" class="uk-modal-close uk-close"></a>

        <ul class="uk-tab" data-uk-tab>
            <li class="uk-active"><a href="#general" @click="TabActive">{$lang['menu']['general']}</a></li>
            <li><a href="#links" @click="TabActive" v-show="isViewLinksTab" style="display:none;">{$lang['menu']['searchDoc']}</a></li>
        </ul>
        <div class="clearfix"></div>

        <div id="general" class="form-horizontal tab-block top-10">

            <div class="form-group" v-show="edit._id">
                <label class="col-xs-12">ID: {{edit._id}}</label>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix" v-show="edit._id"></div>

            <div class="form-group">
                <label class="col-xs-12">{$lang['menu']['title']}</label>
                <div class="clearfix"></div>
                <div class="col-xs-6">
                    <input class="form-control" type="text" v-model="edit.title"/>
                </div>
            </div>
            <div class="clearfix"></div>

            <div class="form-group top-10">
                <label class="col-xs-12">{$lang['menu']['url']}</label>
                <div class="clearfix"></div>
                <div class="col-xs-6">
                    <input class="form-control" type="text" v-model="edit.url"/>
                </div>
            </div>
            <div class="clearfix"></div>

            <div class="form-group top-10">
                <div class="col-xs-1">
                    <switch-box :value.sync="edit.enabled"></switch-box>
                </div>
                <label class="col-xs-7 text-left control-label">{$lang['menu']['enabled']}</label>
            </div>
            <div class="clearfix"></div>

            <div class="form-group top-10">
                <label class="col-xs-12">{$lang['menu']['type_']}</label>
                <div class="clearfix"></div>
                <div class="col-xs-6">
                    <select class="form-control" v-model="edit.type">
                        <option v-for="m in modules" :value="m.value" :selected="m.value==edit.type">{{lang.menu.type[m.title]}}</option>
                    </select>
                </div>
            </div><div class="clearfix"></div>

            <div class="form-group top-10">
                <label class="col-xs-12">{$lang['menu']['pos']}</label>
                <div class="clearfix"></div>
                <div class="col-xs-6">
                    <input class="form-control" type="text" v-model="edit.pos"/>
                </div>
            </div>

        </div>
        <div class="clearfix"></div>

        <div id="links" class="tab-block" style="display:none;">

            <div class="form-group top-10">
                <label class="col-xs-12">{$lang['menu']['docName']}</label>
                <div class="clearfix"></div>
                <div class="col-xs-6">
                    <input class="form-control" type="text" v-model="docName"/>
                </div>
                <div class="col-xs-3 spinner-block">
                    <button type="button" class="uk-button" @click="findContent()">{$lang['menu']['search']}</button>
                    <div class="spinner mini" v-show="isLoading" style="display:none;"></div>
                </div>
            </div>
            <div class="clearfix"></div>

            <ul class="module-content" v-if="ModuleList.length>0">
                <li v-for="M in ModuleList" @click="setContentUrl(M.url)">
                    <span>{{M.title}}</span>
                </li>
            </ul>

            <div class="alert alert-info top-15" role="alert" v-if="ModuleList.length==0">
                {$lang['menu']['notFound']}
            </div>

        </div>
        <div class="clearfix"></div>


        <div class="uk-modal-footer uk-text-right">
            <button type="button" class="uk-button uk-modal-close">{$lang['btns']['cancel']}</button>
            <button type="button" class="uk-button uk-button-primary" @click="onSave">{$lang['btns']['save']}</button>
        </div>
    </div>
</div>

HTML;
