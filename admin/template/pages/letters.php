<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 14.02.2016
 * Time: 1:40
 */

$moduleUrl = '/admin/?module=letters';
$pageContent = $delBtn = '';

if(isset($_GET['debug']) && $_GET['debug'] == 1){
    $pageContent .= <<<HTML
<div class="btn-group bottom-15">
    <a class="btn btn-default" href="{$moduleUrl}&action=add">{$lang['btns']['add']}</a>
</div>

<div class="clearfix"></div>
<hr class="short margin-top-none">
HTML;

$sysName = <<<HTML
<div class="form-group">
    <label>{$lang['letters']['sysName']}</label>
    <input class="form-control" type="text" v-model="page.sysName">
</div>
HTML;

$delBtn = <<<HTML
<a class="btn btn-xs btn-danger" href="#" @click="onDelete(S._id, \$event)">
    <i class="glyphicon glyphicon-remove"></i>
</a>
HTML;

$vars = '<table class="table table-hover">
            <thead>
                <tr>
                    <th>'.$lang['letters']['var'].'</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="v in page.vars" track-by="$index">
                    <td>
                        <div class="col-xs-6">
                            <span class="label label-info" @click="insertEditor(v)">{{{v}}}</span>
                        </div>
                        <div class="col-xs-6">
                            <input class="form-control" type="text" v-model="v" @keyup="checkLength($index, v)" maxlength="30">
                        </div>
                    </td>
                    <td class="function">
                        <a class="btn btn-xs btn-danger btn-gradient" @click="deleteVar($index)">
                            <i class="glyphicon glyphicon-remove"></i>
                        </a>
                        <a class="btn btn-xs btn-success btn-gradient" @click="addVar()" v-show="$index==(page.vars.length-1)">
                            <i class="glyphicon glyphicon-plus"></i>
                        </a>
                    </td>
                </tr>
            </tbody>
        </table>';

}else{
    $vars = '<label>'.$lang['letters']['var'].'</label>
    <ul>
        <li v-for="v in page.vars" track-by="$index" v-if="v!=\'new\'"><span class="label label-info" @click="insertEditor(v)">{{v}}</span> - {{lang.letters.vars[v]}}</li>
    </ul>';

    $sysName = <<<HTML
<div class="form-group">
    <label>{$lang['letters']['sysName']} :</label>
    <span v-show="page.sysName" style="display:none;">{{page.sysName}}</span>
</div>
HTML;
}




if(isset($_GET['action']) && ($_GET['action'] == 'add' || $_GET['action'] == 'edit')){


    $pageContent .= <<<HTML

<div>
    <div class="thumbnail">
        <div class="caption">

            <div class="col-md-12">

                <div class="row clearfix">
                    <div class="col-xs-5">
                        <div class="form-group">
                            <label>{$lang['letters']['name']}</label>
                            <input class="form-control" type="text" v-model="page.name">
                        </div>

                        <div class="form-group">
                            <label>{$lang['letters']['subject']}</label>
                            <input class="form-control" type="text" v-model="page.subject">
                        </div>

                        {$sysName}

                        <div class="form-group">
                            <label class="col-xs-2 control-label" style="padding-top: 10px;"> {$lang['letters']['enabled']}</label>
                            <div class="col-xs-3">
                                <switch-box :value.sync="page.enabled"></switch-box>
                            </div>
                        </div>

                    </div>

                    <div class="col-xs-5">
                        <div class="form-group">
                            $vars
                        </div>
                    </div>

                    <div class="clearfix"></div>
                </div>

                <div class="row clearfix top-20">

                    <div class="form-group col-xs-12">
                        <label>{$lang['letters']['description']}: </label>
                        <editor :value.sync="page.description" name="description"></editor>
                    </div>
                </div>

            </div>
            <div class="clearfix"></div>

        </div>
    </div>


    <!-- FootLine -->
    <div class="submit-pane">
        <button type="submit" class="btn btn-success" @click="onSave">
            <b>{$lang['btns']['save']}</b>
        </button>
    </div>
    <!-- FootLine [E] -->

</div>

HTML;

}else{

    $pageContent .= <<<HTML

<div class="col-md-12">
    <div class="panel panel-visible">
        <div class="panel-body">

            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>{$lang['label']['name']}</th>
                        <th>{$lang['letters']['sysName']}</th>
                        <th class="col-xs-1">{$lang['label']['functions']}</th>
                    </tr>
                </thead>
                <tbody>

                    <tr v-for="S in list" style="display:none;" v-show="list.length>0">
                        <td>{{S.name}}</td>
                        <td>{{S.sysName}}</td>
                        <td class="function">
                            <a class="btn btn-xs btn-info" href="{$moduleUrl}&action=edit&id={{S._id}}">
                                <i class="glyphicons glyphicons-pencil"></i>
                            </a>
                            {$delBtn}
                        </td>
                    </tr>

                </tbody>
            </table>

        </div>
    </div>
</div>
HTML;

}


return$pageContent;