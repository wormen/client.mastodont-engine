<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 28.01.2016
 * Time: 12:15
 */

$types = '<div class="input-group-btn">
        <div class="btn btn-default uk-nestable-handle">
            <i class="uk-nestable-handle uk-icon uk-icon-bars uk-margin-small-right"></i>
        </div>

        <div class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i :class="checkTypeIco(P.type)"></i>
            <span class="caret"></span>
        </div>
        <ul class="dropdown-menu">';

foreach($lang['contacts']['types'] as $k=>$v)
    $types .= "<li><a href=\"#\" @click=\"chanceType(\$event, \$index, '{$k}')\">$v</a></li>";

$types .= '</ul></div>';


return <<<HTML
<div class="col-md-12">
    <div class="panel panel-visible">
        <div class="panel-body">

            <div class="col-xs-6">

                <div class="btn-group bottom-15">
                    <a class="btn btn-default" href="#" @click="addBlock">
                        <i class="glyphicon glyphicon-plus"></i>
                        {$lang['contacts']['addBlock']}
                    </a>
                </div>

                <div class="clearfix"></div>
                <hr class="short margin-top-none">

                <div class="spinner" v-show="isLoading"></div>

                <div  class="contacts-list-wrap" v-sortable="{ handle: '.uk-nestable-handle', animation: 200, onEnd: endSortBlock }">
                    <div v-for="C in all | orderBy 'pos'" 
                            v-show="all.length>0" 
                            style="display:none;" 
                            class="contacts-list" 
                            data-idx="{{\$index}}">
                    
                    
                        <div class="uk-nestable-handle">
                            <i class="uk-nestable-handle uk-icon uk-icon-bars uk-margin-small-right"></i>
                        </div>

                        <div class="thumbnail">
                            <div class="caption">

                                <div class="function">
                                    <a class="btn btn-xs btn-{{C.enabled ? 'success' : 'danger'}} btn-gradient" @click="onOffBlock(C)">
                                        <i class="glyphicon glyphicon-{{C.enabled ? 'eye-open' : 'eye-close'}}"></i>
                                    </a>
                                    <a class="btn btn-xs btn-default" href="#" @click="editBlock(\$event, \$index)">
                                        <i class="glyphicon glyphicon-pencil"></i>
                                    </a>
                                    <a class="btn btn-xs btn-danger" href="#" @click="delBlock(\$event, \$index)">
                                        <i class="glyphicon glyphicon-remove"></i>
                                    </a>
                                </div>

                                <h4>{{C.nameBlock}}</h4>

                                <ul v-for="L in C.list | orderBy 'pos'">
                                    <li>
                                        <span class="ico"><i :class="checkTypeIco(L.type)"></i></span>
                                        <span class="value">{{L.value}}</span>
                                    </li>
                                </ul>

                            </div>
                        </div>
                    </div>
                </div>               

            </div>


            <div class="col-xs-6 contact-block">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">{$lang['contacts']['settingBlock']}</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-horizontal">

                            <div class="form-group">
                                <label class="col-lg-3 control-label">{$lang['contacts']['nameBlock']}</label>
                                <div class="col-lg-9">
                                    <input class="form-control" type="text" v-model="edit.nameBlock">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-3 control-label">{$lang['contacts']['pos']}</label>
                                <div class="col-lg-9">
                                    <input class="form-control" type="text" v-model="edit.pos">
                                </div>
                            </div>


                            <table class="table table-hover">
                                <caption>{$lang['contacts']['listContacts']}</caption>

                                <thead>
                                    <tr>
                                        <th class="col-xs-2"></th>
                                        <th></th>
                                        <th class="col-xs-2"></th>
                                    </tr>
                                </thead>
                                <tbody class="uk-nestable-list" v-sortable="{ handle: '.uk-nestable-handle', animation: 200, onEnd: endSortContacts }">
                                    <tr v-for="P in edit.list" data-idx="{{\$index}}" v-show="edit.list.length>0" style="display:none;" class="uk-nestable-item">
                                        <td class="ico">
                                            {$types}
                                        </td>
                                        <td>
                                            <input class="form-control" type="text" v-model="P.value">
                                        </td>
                                        <td class="function">                                           
                                            <a class="btn btn-xs btn-danger btn-gradient" @click="onDeleteContact(\$index)">
                                                <i class="glyphicon glyphicon-remove"></i>
                                            </a>
                                            <a class="btn btn-xs btn-default btn-gradient" 
                                                v-show="isMap(\$index)" 
                                                style="display:none;" 
                                                @click="openModal" 
                                                data-uk-modal="{target:'#select-map'}">
                                                
                                                <i class="glyphicon glyphicon-search"></i>
                                            </a>
                                            <a class="btn btn-xs btn-success btn-gradient add" @click="onAddContact">
                                                <i class="glyphicon glyphicon-plus"></i>
                                            </a>
                                        </td>
                                    </tr>                  
                                                                       
                                </tbody>
                            </table>             

                            <div class="clearfix top-150"></div>
                            

                            <!--
                            <div class="pull-right">
                                <button type="button" class="btn btn-success" @click="saveBlock()">{$lang['contacts']['saveBlock']}</button>
                            </div>
                            <div class="clearfix"></div>
                            -->
                        </div>
                    </div>
                </div>

            </div>

            <div class="clearfix"></div>

        </div>
    </div>

    <!-- FootLine -->
    <div class="submit-pane">
        <button type="submit" class="btn btn-success" @click="onSave">
            <b>{$lang['btns']['save']}</b>
        </button>
    </div>
    <!-- FootLine [E] -->
    
    <div id="select-map" class="uk-modal">
        <div class="uk-modal-dialog uk-modal-dialog-large">
            <a class="uk-modal-close uk-close"></a>
            
            <div class="clearfix"></div>
            
            <div class="col-xs-12 form-horizontal map-conf">
                <div class="form-group">
                    <label class="col-sm-2 control-label">{$lang['contacts']['map']}</label>
                    <div class="col-sm-10">
                        <select class="form-control" v-model="modalMap.type">
                            <option v-for="M in mapList" track-by="\$index" :value="M"> {{M | capitalize}} </option>
                        </select>
                    </div>
                </div>
                
                <!--
                <div class="form-group" style="display:none;">
                    <label class="col-sm-2 control-label">{$lang['contacts']['apiKey']}</label>
                    <div class="col-sm-10">
                        <input class="form-control" type="text" v-model="apiKey[modalMap.type]"/>
                    </div>
                </div>
                -->
                
                <div class="form-group">
                    <label class="col-sm-2 control-label">{$lang['contacts']['address']}</label>
                    <div class="col-sm-10">
                        <input class="form-control" type="text" v-model="modalMap.config.address" @keyup="\$broadcast('checkAddress') | debounce 500"/>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-sm-2 control-label">{$lang['contacts']['hint']}</label>
                    <div class="col-sm-10">
                        <input class="form-control" type="text" v-model="modalMap.config.hint" @keyup="\$broadcast('setHint') | debounce 300"/>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-sm-2 control-label">{$lang['contacts']['center']}</label>
                    <div class="col-sm-5">
                        <input class="form-control" type="number" min="0" step="0.001" number v-model="modalMap.config.center[0]"/>
                    </div>
                    <div class="col-sm-5">
                        <input class="form-control" type="number" min="0" step="0.001" number v-model="modalMap.config.center[1]"/>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-sm-2 control-label">{$lang['contacts']['zoom']}</label>
                    <div class="col-sm-2">
                        <input class="form-control" type="number" min="0" step="1" number v-model="modalMap.config.zoom"/>
                    </div>
                </div>                                   
            
                <component :is="modalMap.type" :config="modalMap.config" :key="apiKey[modalMap.type]"></component>
                
            </div>
            
            <div class="clearfix"></div>
            
            <div class="uk-modal-footer top-10 uk-text-right">
                <button type="button" class="uk-button uk-modal-close">{$lang['btns']['cancel']}</button>
                <button type="button" class="uk-button uk-modal-close uk-button-primary" @click="onSaveMap">{$lang['btns']['save']}</button>
            </div>
    
        </div>
    </div>

</div>
HTML;
