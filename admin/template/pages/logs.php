<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 30.01.2016
 * Time: 17:02
 */



return <<<HTML

<div class="col-md-12">
    <div class="btn-group bottom-15">
        <a class="btn btn-default" href="#" @click="loadData">
            <i class="glyphicon glyphicon-refresh"></i>
        </a>
    </div>
</div>
<div class="clearfix"></div>
<hr class="short margin-top-none">


<div class="col-md-12">
    <div class="panel panel-visible">
        <div class="panel-body">
            <div class="spinner" v-show="isLoading"></div>


            <div class="panel-heading" v-show="all.length>0" style="display:none;">
                <div class="panel-title"></div>

                <ul class="nav panel-tabs">
                    <li v-for="T in all" class="{{\$index==0 ? 'active' : ''}}">
                        <a href="#{{T.table}}-block" data-toggle="tab">{{T.table | capitalize }}</a>
                    </li>
                </ul>

            </div>

            <div class="panel-body" v-show="all.length>0" style="display:none;">
                <div class="tab-content padding-none border-none">

                    <div v-for="B in all" id="{{B.table}}-block" class="tab-pane {{\$index==0 ? 'active' : ''}}">
                        <div class="col-sm-12">
                            <div class="row">

                                <ul v-show="B.list.length>0">
                                    <li v-for="L in B.list">
                                        <div class="date">{{L.date}}</div>
                                        <div class="clearfix"></div>
                                        <div class="info"><pre>{{checkInfo(L.info)}}</pre></div>

                                        <div class="function">
                                            <div class="btn-group">
                                                <a class="btn btn-xs btn-danger btn-gradient delete" @click="onDelete(B.list, \$index, L._id, B.table, \$event)">
                                                    <span class="glyphicon glyphicon-remove"></span>
                                                </a>
                                            </div>
                                        </div>

                                    </li>
                                </ul>

                                <div class="alert alert-warning" v-show="B.list.length==0">No logs</div>
                                <div class="clearfix"></div>

                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                </div>
            </div>
            <div class="clearfix"></div>

            <div class="col-sm-6" v-show="all.length==0">
                <div class="alert alert-warning">
                  No logs
                </div>
            </div>

        </div>
    </div>
</div>
HTML;
