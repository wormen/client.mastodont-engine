<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 04.06.2016
 * Time: 9:35
 */

$module= 'widgets';

$arr = [
    'areaList'=>'area',
    'widgetList'=>'widget-list'
];

foreach ($arr as $k=>$v)
    ${'b_'.$k} = include implode(DIRECTORY_SEPARATOR, [AdminTemplate, 'blocks', $module, $v . '.php']);


return <<<HTML
<div class="col-md-12">
    <div class="row">
    
        <div class="col-md-5 col-md-push-7">
            <div class="panel panel-default">
                <div class="panel-heading area-head">
                    {$lang['widgets']['areaT']}
                
                    <button class="btn btn-success btn-xs add" @click="showAreaForm"><i class="fa fa-plus"></i></button>
                </div>
                <div class="panel-body area-list">{$b_areaList}</div>
            </div>
            
            <div class="clearfix"></div>
        </div>
      
        <div class="col-md-7 col-md-pull-5">
            <div class="panel panel-default">
                <div class="panel-heading">{$lang['widgets']['sw']}</div>
                <div class="panel-body widget-list-wrap">
                    {$b_widgetList}
                    <div class="clearfix"></div>
                </div>
            </div>
            
            <div class="clearfix"></div>
        </div>
    
    </div>
</div>

<div class="clearfix"></div>



<!-- FootLine -->
<div class="submit-pane">
    <button type="submit" class="btn btn-success" @click="onSave()">
        <b>{$lang['btns']['save']}</b>
    </button>
</div>
<!-- FootLine [E] -->

HTML;
