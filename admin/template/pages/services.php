<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 08.02.2016
 * Time: 10:40
 */


return <<<HTML

<div class="col-md-12">
    <div class="btn-group bottom-15">
        <a class="btn btn-default" href="#" @click="onRefresh">{$lang['services']['refresh']}</a>
    </div>
    <div class="clearfix"></div>
    <hr class="short margin-top-none">
</div>

<div class="col-md-12">
    <div class="panel panel-visible">
        <div class="panel-body">
            <div class="spinner" v-show="isLoading"></div>

            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>{$lang['label']['name']}</th>
                        <th class="col-xs-2">{$lang['label']['functions']}</th>
                    </tr>
                </thead>
                <tbody>

                    <tr v-for="S in list" style="display:none;" v-show="list.length>0">
                        <td>
                            <div class="name">{{S.sysName}}</div>
                            <div class="descr">{{S.descr}}</div>
                        </td>
                        <td class="function">
                            <span style="padding: 1px 5px;font-size: 16px;position: relative;top: 2px;">
                                <i class="{{S.p == 'system' ? 'fa fa-cog' : 'glyphicon glyphicon-user'}}"></i>
                            </span>

                            <a class="btn btn-xs btn-{{S.state == 'run' ? 'success' : 'danger'}}" href="#" @click="onChangeState(S, \$event)">
                                <i class="fa fa-{{S.state == 'run' ? 'play' : 'stop'}}"></i>
                            </a>

                            <a class="btn btn-xs btn-info" href="#" @click="onEdit(S, \$event)">
                                <i class="glyphicons glyphicons-pencil"></i>
                            </a>
                            <a class="btn btn-xs btn-danger" href="#" @click="onDelete(S, \$event)">
                                <i class="glyphicon glyphicon-remove"></i>
                            </a>
                        </td>
                    </tr>

                </tbody>
            </table>

        </div>
    </div>
</div>
HTML;
