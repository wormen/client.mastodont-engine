<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 16.02.2016
 * Time: 12:21
 */

$moduleUrl = '/admin/?module=ads';

$tabList = [
    ["nameBlock"=> "category", "isActive"=> true],
    ["nameBlock"=> "ad", "isActive"=> false],
];



if(isset($_GET['action'])){



}else{

    $tabs = '<ul class="nav nav-tabs" role="tablist">';
    $tabContent = '<div class="tab-content">';
    foreach($tabList as $t){

        $tabs .= '<li class="'.($t['isActive'] ? 'active' : '').'">
                            <a href="#'.$t['nameBlock'].'-block" data-toggle="tab">'.$lang['tabs']['ads'][$t['nameBlock']].'</a>
                        </li>';

        $tabContent .= '<div id="'.$t['nameBlock'].'-block" class="tab-pane'.($t['isActive'] ? ' active' : '').'">';
        $tabContent .= include implode(DIRECTORY_SEPARATOR, [AdminTemplate, 'blocks', 'ads', $t['nameBlock'].'.php']);
        $tabContent .= '</div>';

    }
    $tabContent .= '</div>';
    $tabs .= '</ul>';

    $pageContent = <<<HTML
<div class="col-md-12">

    <div class="thumbnail">
        <div class="caption">

            {$tabs}
            {$tabContent}

        </div>
    </div>

</div>

HTML;

}

$pageContent .= <<<HTML
<div id="category-item" class="uk-modal" aria-hidden="true" style="display: none; overflow-y: scroll;">
    <div class="uk-modal-dialog uk-modal-dialog-large">
        <a href="" class="uk-modal-close uk-close"></a>

        <ul class="uk-tab" data-uk-tab>
            <li class="uk-active"><a href="#general" @click="TabActive">{$lang['ads']['general']}</a></li>
            <li><a href="#ext" @click="TabActive">{$lang['ads']['ext']}</a></li>
        </ul>
        <div class="clearfix"></div>

        <div id="general" class="form-horizontal tab-block top-10">

            <div class="form-group" v-show="category._id">
                <label class="col-xs-12">ID: {{category._id}}</label>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix" v-show="category._id"></div>

            <div class="form-group">
                <label class="col-xs-12">{$lang['ads']['name']}</label>
                <div class="clearfix"></div>
                <div class="col-xs-6">
                    <input class="form-control" type="text" v-model="category.name" @keyup="transtaleCategoryUrl | debounce 150" />
                </div>
            </div>
            <div class="clearfix"></div>

            <div class="form-group">
                <label class="col-xs-12">H1</label>
                <div class="clearfix"></div>
                <div class="col-xs-6">
                    <input class="form-control" type="text" v-model="category.h1" />
                </div>
            </div>
            <div class="clearfix"></div>

            <div class="form-group">
                <label class="col-xs-12">{$lang['ads']['title']}</label>
                <div class="clearfix"></div>
                <div class="col-xs-6">
                    <input class="form-control" type="text" v-model="category.title"/>
                </div>
            </div>
            <div class="clearfix"></div>

            <div class="form-group">
                <label class="col-xs-12">{$lang['ads']['meta_description']}</label>
                <div class="clearfix"></div>
                <div class="col-xs-6">
                    <input class="form-control" type="text" v-model="category.meta_description"/>
                </div>
            </div>
            <div class="clearfix"></div>

            <div class="form-group top-10">
                <label class="col-xs-12">{$lang['ads']['url']}</label>
                <div class="clearfix"></div>
                <div class="col-xs-6">
                    <input class="form-control" type="text" v-model="category.url" @keyup="onCheckUrlCategory | debounce 150"/>
                </div>
            </div>
            <div class="clearfix"></div>

            <div class="form-group top-10">
                <div class="col-xs-1">
                    <switch-box :value.sync="category.enabled"></switch-box>
                </div>
                <label class="col-xs-7 text-left control-label">{$lang['ads']['enabled']}</label>
            </div>
            <div class="clearfix"></div>

            <div class="form-group top-10">
                <label class="col-xs-12">{$lang['ads']['pos']}</label>
                <div class="clearfix"></div>
                <div class="col-xs-6">
                    <input class="form-control" type="text" v-model="category.pos"/>
                </div>
            </div>
            <div class="clearfix"></div>

            <div class="form-group top-10">
                <label class="col-xs-12">{$lang['ads']['pos']}</label>
                <div class="clearfix"></div>
                <div class="col-xs-12">
                    <editor :value.sync="category.description" name="description"></editor>
                </div>
            </div>
            <div class="clearfix"></div>

        </div>
        <div class="clearfix"></div>


        <div id="ext" class="tab-block" style="display:none;">

            <div class="form-group top-10">
                <label class="col-xs-12">{$lang['ads']['ico']}</label>
                <div class="clearfix"></div>
                <div class="col-xs-6">
                    <input class="form-control" type="text" v-model="category.ico"/>
                </div>
            </div>
            <div class="clearfix"></div>

            <div class="form-group top-10">
                <label class="col-xs-12">Inner style</label>
                <div class="clearfix"></div>
                <div class="col-xs-6">
                    <textarea class="form-control" v-model="category.style"/></textarea>
                </div>
            </div>
            <div class="clearfix"></div>

        </div>
        <div class="clearfix"></div>


        <div class="uk-modal-footer uk-text-right">
            <div class="spinner mini" v-show="isSave" display:none;></div>
            <button type="button" class="uk-button uk-modal-close">{$lang['btns']['cancel']}</button>
            <button type="button" class="uk-button uk-button-primary" @click="onSaveCategory">{$lang['btns']['save']}</button>
        </div>
    </div>
</div>
HTML;



return$pageContent;