<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 20.01.2016
 * Time: 16:19
 */

$at = '/admin/template';

return <<<HTML
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>{$lang['auth']['title']}</title>

        <!-- Core CSS  -->
        <link rel="stylesheet" type="text/css" href="{$at}/vendor/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="{$at}/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="{$at}/fonts/glyphicons_pro/glyphicons.min.css">

        <!-- Theme CSS -->
        <link rel="stylesheet" type="text/css" href="{$at}/css/theme.css">
        <link rel="stylesheet" type="text/css" href="{$at}/css/pages.css">
        <link rel="stylesheet" type="text/css" href="{$at}/css/plugins.css">
        <link rel="stylesheet" type="text/css" href="{$at}/css/responsive.css">

        <!-- Boxed-Layout CSS -->
        <link rel="stylesheet" type="text/css" href="{$at}/css/boxed.css">

        <!-- Your Custom CSS -->
        <link rel="stylesheet" type="text/css" href="{$at}/css/custom.css">

        <!-- Favicon -->
        <link rel="shortcut icon" href="{$at}/img/favicon.png">

    </head>
    <body id="app" class="dashboard login-page">
        <div id="main" class="auth">
            <div class="container">

                <div class="row">
                    <div id="page-logo">
                        <img src="{$at}/img/logos/logo_text.png" class="img-responsive" alt="logo">
                    </div>
                </div>

                <div class="row">
                    <div class="panel auth">
                        <div class="spinner" v-show="isLoading" style="display:none;"></div>

                        <div class="panel-heading">
                            <div class="panel-title"> <span class="glyphicon glyphicon-lock"></span> {$lang['auth']['title']} </div>
                        </div>

                        <div class="panel-body">

                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-user"></span>
                                    </span>
                                    <input type="text" class="form-control phone" name="login" placeholder="{$lang['auth']['login']}" v-model="user.login">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <span class="glyphicons glyphicons-keys"></span>
                                    </span>
                                    <input type="password" class="form-control product" name="password" autocomplete="off" placeholder="{$lang['auth']['pass']}" v-model="user.pass">
                                </div>
                            </div>

                        </div>

                        <div class="login-alert" v-show="!isAuth" style="display: none;">
                            <div class="alert alert-danger">{{alert}}</div>
                        </div>

                        <div class="panel-footer">
                            <div class="form-group margin-bottom-none">
                                <button class="btn btn-primary pull-right" type="button" @keyup.enter="onSingIn" @click="onSingIn">
                                    {$lang['btns']['login']}
                                </button>
                                <div class="clearfix"></div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <script src="{$at}/assets/jquery.min.js" type="text/javascript"></script>
        <script src="{$at}/js/auth.js" type="text/javascript"></script>

    </body>
</html>


HTML;
