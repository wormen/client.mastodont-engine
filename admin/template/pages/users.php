<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 24.01.2016
 * Time: 16:02
 */

$tabList = [
    ["nameBlock"=> "general", "isActive"=> true],
    ["nameBlock"=> "extend", "isActive"=> false],
    ["nameBlock"=> "access", "isActive"=> false]
];

$type_face = [
    ["val"=> "fiz", "isActive"=> true],
    ["val"=> "ur", "isActive"=> false]
];

$permisionList = [
    ["val"=> "root", "isActive"=> true],
    ["val"=> "partner", "isActive"=> false],
    ["val"=> "adminDomain", "isActive"=> false],
    ["val"=> "SEO", "isActive"=> false],
    ["val"=> "admin", "isActive"=> false],
    ["val"=> "user", "isActive"=> false]
];


$moduleUrl = '/admin/?module=users';


if(isset($_GET['action']) && ($_GET['action'] == 'add' || $_GET['action'] == 'edit')){

    $tabs = '<ul class="nav nav-tabs" role="tablist">';
    $tabContent = '<div class="tab-content">';
    foreach($tabList as $t){

        if(!$_SESSION['IS_ROOT']){
            if($t['nameBlock'] == 'access') continue;
        }

        $tabs .= '<li class="'.($t['isActive'] ? 'active' : '').'">
                            <a href="#'.$t['nameBlock'].'-block" data-toggle="tab">'.$lang['tabs']['users'][$t['nameBlock']].'</a>
                        </li>';

        $tabContent .= '<div id="'.$t['nameBlock'].'-block" class="tab-pane'.($t['isActive'] ? ' active' : '').'">';
        $tabContent .= include implode(DIRECTORY_SEPARATOR, [AdminTemplate, 'blocks', 'users', $t['nameBlock'].'.php']);
        $tabContent .= '</div>';

    }
    $tabContent .= '</div>';
    $tabs .= '</ul>';


    $pageData = <<<HTML
<div class="col-md-12">
    <div class="btn-group bottom-15">
        <a class="btn btn-default" href="{$moduleUrl}">{$lang['users']['listLabel']}</a>
    </div>
</div>
<div class="clearfix"></div>
<hr class="short margin-top-none">

<div class="col-md-12">

    <div class="thumbnail">
        <div class="caption">

            {$tabs}
            {$tabContent}

        </div>
    </div>


    <!-- FootLine -->
    <div class="submit-pane">
        <button type="submit" class="btn btn-success" @click="onSave">
            <b>{$lang['btns']['save']}</b>
        </button>
    </div>
    <!-- FootLine [E] -->

</div>
HTML;
}else{

    $pageData = <<<HTML

<div class="col-md-12">
    <div class="btn-group bottom-15">
        <a class="btn btn-default" href="{$moduleUrl}&action=add">{$lang['btns']['add']}</a>
    </div>
    <div class="form-group pull-right col-xs-3">
        <input class="form-control" type="text" v-model="searchUser" placeholder="{$lang['placeholder']['searchUser']}">
    </div>
</div>
<div class="clearfix"></div>
<hr class="short margin-top-none">

<div class="col-md-12">
    <div class="panel panel-visible">
        <div class="panel-heading">
            <div class="panel-title">
                <span class="glyphicon glyphicon-tasks"></span>
                {$lang['users']['listLabel']}
            </div>
        </div>

        <div class="panel-body padding-bottom-none">
            <table class="table table-striped table-bordered table-hover">

                <thead>
                    <tr>
                        <th class="col-xs-1">#</th>
                        <th>{$lang['users']['login']}</th>
                        <th class="col-xs-3">{$lang['users']['email']}</th>
                        <th class="col-xs-1 text-center">{$lang['label']['status']}</th>
                        <th class="col-xs-1 text-center">{$lang['label']['functions']}</th>
                    </tr>
                </thead>

                <tbody>

                    <tr v-for="L in list | filterBy searchUser in 'login' 'email'" style="display:none;" v-show="list.length>0">
                        <td>{{\$index+1}}</td>
                        <td>{{L.login}}</td>
                        <td>{{L.email}}</td>
                        <td>
                            <div class="btn btn-xs btn-default list status" :class="{active: L.enabled, inactive: !L.enabled}" @click="OnOff(L)"></div>
                        </td>
                        <td class="functions text-center">
                            <div class="btn-group">
                                <a class="btn btn-xs btn-default btn-gradient edit" @click="onEdit(false, L)">
                                    <span class="glyphicon glyphicon-pencil"></span>
                                </a>
                            </div>
                            <div class="btn-group">
                                <a class="btn btn-xs btn-danger btn-gradient delete" @click="onDelete(L, \$event)">
                                    <span class="glyphicon glyphicon-remove"></span>
                                </a>
                            </div>
                        </td>
                    </tr>

                </tbody>

            </table>
        </div>
    </div>
</div>

HTML;
}

return$pageData;
