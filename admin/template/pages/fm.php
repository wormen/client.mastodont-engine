<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 28.01.2016
 * Time: 1:50
 */

return <<<HTML
<div class="col-md-12">
    <div class="panel panel-visible">
        <div class="panel-body">

            <fm-block></fm-block>

        </div>
    </div>
</div>
HTML;
