<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 28.01.2016
 * Time: 2:21
 */

return <<<HTML
<div class="col-md-12">
    <div class="panel panel-visible">
        <div class="panel-body">

            <div class="col-xs-6">

                <table class="table table-hover">
                    <caption>{$lang['seo']['listRedirects']}</caption>
                    <thead>
                        <tr>
                            <th>{$lang['seo']['oldUrl']}</th>
                            <th>{$lang['seo']['newUrl']}</th>
                            <th class="col-xs-2">{$lang['seo']['statusCode']}</th>
                            <th class="col-xs-2"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="P in redirects" v-show="redirects.length>0" style="display:none;">
                            <td>{{P.oldUrl}}</td>
                            <td>{{P.newUrl}}</td>
                            <td>{{P.statusCode}}</td>
                            <td>
                                <a class="btn btn-xs btn-default btn-gradient" @click="onEdit(P)">
                                    <i class="glyphicon glyphicon-pencil"></i>
                                </a>
                                <a class="btn btn-xs btn-danger btn-gradient" @click="onDelete(\$index)">
                                    <i class="glyphicon glyphicon-remove"></i>
                                </a>
                            </td>
                        </tr>
                    </tbody>
                </table>

            </div>

            <div class="col-xs-6">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">{$lang['seo']['settingPath']}</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-horizontal">

                            <div class="form-group">
                                <label class="col-lg-3 control-label">{$lang['seo']['oldUrl']}</label>
                                <div class="col-lg-9">
                                    <input class="form-control" type="text" placeholder="/old-path" v-model="path.oldUrl">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-3 control-label">{$lang['seo']['newUrl']}</label>
                                <div class="col-lg-9">
                                    <input class="form-control" type="text" placeholder="/new-path" v-model="path.newUrl" :disabled="path.statusCode == 404">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-3 control-label">{$lang['seo']['statusCode']}</label>
                                <div class="col-lg-3">

                                    <select class="form-control" v-model="path.statusCode">
                                        <option v-for="(i, v) in codeList" :value="v">{{v}}</option>
                                    </select>

                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <div class="pull-right">
                                <button type="button" class="btn btn-default" @click="onAdd()">{$lang['btns']['add']}</button>
                                <button type="button" class="btn btn-success" @click="onSave()">{$lang['btns']['save']}</button>
                            </div>

                        </div>
                    </div>
                </div>

            </div>

            <div class="clearfix"></div>

        </div>
    </div>
</div>
HTML;
