<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 */

$moduleUrl = '/admin/?module=form-generator';


$editBlock = <<<HTML
<div class="col-xs-5 none-left" v-show="isEdit" style="display:none;">
    
    <div class="panel panel-visible">
        <div class="panel-heading">
            <div class="panel-title">Настройка формы</div>     
        </div>
        
        <div class="panel-body">
            <div class="form-group">
                <label>Элемент</label>
                <select class="form-control" v-model="editItemType">
                    <option v-for="T in fieldTypes" :value="T">{{lang['form-generator'].types[T]}}</option>
                </select>
            </div>
            
            
            <div class="text-right">
                <button class="btn btn-default top-15" @click="onAdd">{$lang['btns']['add']}</button>
            </div>
            <hr/>
            <div class="checkbox">
                <label> 
                    <input type="checkbox" v-model="formOptions.validateAfterLoad"> Проверять после загрузки
                </label>
            </div>
            <div class="checkbox">
                <label> 
                    <input type="checkbox" v-model="formOptions.validateAfterChanged"> Проверять при изменении формы
                </label>
            </div>
            
        </div>
        <div class="clearfix"></div>
    </div>
    
    <div class="clearfix"></div>
</div>

<div class="col-xs-7 none-right" v-show="isEdit" style="display:none;">

    <div class="panel panel-visible">
        <div class="panel-heading">
            <div class="panel-title">Предпросмотр</div>     
        </div>
        
        <div class="panel-body">
            <vue-form-generator :schema="schema" :model="formModel" :options="formOptions"></vue-form-generator>
        </div>
        <div class="clearfix"></div>
    </div>
    
    <div class="clearfix"></div>
</div>

<div class="clearfix"></div>
<pre>formModel<br/>{{editItem | json}}</pre>
<pre>formModel<br/>{{formModel | json}}</pre>
<pre>formOptions<br/>{{formOptions | json}}</pre>
<pre>schema<br/>{{schema | json}}</pre>
HTML;


return <<<HTML
<div class="col-md-12">
    <div class="btn-group bottom-15">
        <a class="btn btn-default" href="javascript:;" @click="onCreate">{$lang['btns']['create']}</a>
    </div>
    <div class="clearfix"></div>
    <hr class="short margin-top-none">
</div>


<div class="col-md-12">
    <div class="panel panel-visible">
        <div class="panel-body">
                   
            <table class="table table-striped table-bordered table-hover" v-show="!isEdit">
                <thead>
                    <tr>
                        <th>{$lang['label']['name']}</th>               
                        <th class="col-xs-1">{$lang['label']['functions']}</th>
                    </tr>
                </thead>
                <tbody>

                    <tr v-for="S in list" style="display:none;" v-show="list.length>0">
                        <td>{{S.name}}</td>             
                        <td class="function">
                            <a class="btn btn-xs btn-info" href="{$moduleUrl}&action=edit&id={{S._id}}">
                                <i class="glyphicons glyphicons-pencil"></i>
                            </a>                          
                        </td>
                    </tr>

                </tbody>
            </table>
            
            {$editBlock}          
        
        </div>
    </div>
</div>


<div v-show="isEdit" style="display:none;">
    <!-- FootLine -->
    <div class="submit-pane">
        <button type="submit" class="btn btn-success" @click="onSave">
            <b>{$lang['btns']['save']}</b>
        </button>
    </div>
    <!-- FootLine [E] -->
</div>

HTML;
