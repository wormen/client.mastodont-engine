<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 20.01.2016
 * Time: 14:50
 */
$tabList = [
    ["nameBlock" => "general", "isActive" => true],
    ["nameBlock" => "site", "isActive" => false],
    ["nameBlock" => "modules", "isActive" => false],
    ["nameBlock" => "system", "isActive" => false],
    ["nameBlock" => "api", "isActive" => false]
];


$btns = '<ul class="nav panel-tabs">';
$bloks = '';
foreach($tabList as $t){
    $btns .= '<li'.($t['isActive'] ? ' class="active"' : '').'>
                <a href="#'.$t['nameBlock'].'-block" data-toggle="tab">'.$lang['tabs']['setting'][$t['nameBlock']].'</a>
            </li>';

    $bloks .= '<div id="'.$t['nameBlock'].'-block" class="tab-pane'.($t['isActive'] ? ' active' : '').'">';

    $fileBlock = implode(DIRECTORY_SEPARATOR, [AdminTemplate, 'blocks', 'setting', $t['nameBlock'].'.php']);
    if($fileBlock)
        $bloks .= include $fileBlock;

    $bloks .= '</div>';
}

$btns .= '</ul>';



return <<<HTML
<div class="col-md-12">
    <div class="panel">
    
        <div class="panel-heading">
            <div class="panel-title"></div>
            {$btns}
        </div>
    
        <div class="panel-body">
            <div class="tab-content padding-none border-none">{$bloks}</div>
        </div>
    
        <!-- FootLine -->
        <div class="submit-pane">
            <button type="submit" class="btn btn-success" @click="onSave()">
                <b>{$lang['btns']['save']}</b>
            </button>
        </div>
        <!-- FootLine [E] -->
    
    </div>
</div>
HTML;
