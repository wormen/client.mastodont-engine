<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 23.01.2016
 * Time: 20:18
 */

return <<<HTML
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8">

<title>{$lang['title']}</title>

<!-- Favicon -->
<link rel="shortcut icon" href="/admin/template/img/favicon.png">

{$css}
HTML;
