<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 04.06.2016
 * Time: 13:37
 */

return <<<HMTL
<div class="widget-overlay" :class="{show: showOverlay}"></div>

<div class="col-xs-6">

    <widget-list-item v-for="W in widgetList[0]" :name="W"></widget-list-item>

</div>

<div class="col-xs-6">

    <widget-list-item v-for="W in widgetList[1]" :name="W"></widget-list-item>
    
</div>

HMTL;
