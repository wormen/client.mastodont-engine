<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 04.06.2016
 * Time: 13:35
 */

return <<<HTML
<div class="widget-overlay" :class="{show: showOverlay2}"></div>

<div id="new-area" v-show="showOverlay2" style="display:none;">
    <div class="form-group">
        <label>{$lang['widgets']['title']}</label>
        <input type="text" class="form-control" v-model="area.title" @keyup="validSysName('title')">
    </div>
    <div class="form-group">
        <label>{$lang['widgets']['sysName']}</label>
        <input type="text" class="form-control" v-model="area.name" @keyup="validSysName('name')">
    </div>
    <div class="uk-text-right">
        <button class="btn btn-default btn-sm" @click="cancelArea">{$lang['btns']['cancel']}</button>
        <button class="btn btn-success btn-sm" @click="addArea">{$lang['btns']['save']}</button>
    </div>
    
</div>


<div v-for="W in areaList" class="widgets-holder-wrap" :class="[W.collapse ? 'closed' : '']">
    <div :id="W.id" class="widgets-sortables">
        
        <div class="sidebar-name">
            <div class="sidebar-name-arrow">
                <button class="uk-button uk-button-mini" @click="onCollapse(W)">
                    <i class="fa fa-caret-{{W.collapse ? 'left' : 'down'}}"></i>
                </button>
                <button class="uk-button uk-button-mini uk-button-danger" @click="removeArea(\$index)" v-if="!W.protected">
                    <i class="fa fa-remove"></i>
                </button>
            </div>
			<h3 @click="onCollapse(W)">{{W.title}}</h3>
		</div>
		
		<div class="inner-list" v-sortable="{ handle: '.handle',animation: 200, onEnd: endSort }">
		
            <div v-for="C in W.widgetList" class="widget in-area" :class="{closed: C.collapsed}" data-area-idx="{{\$parent.\$index}}">
            
                <header @click="widgetCollapse(C)">
                    <h3><i class="fa fa-arrows handle"></i> {{C.title}}</h3>                                       
                </header>
                
                <div class="function">
                    <button class="uk-button uk-button-mini" @click="widgetCollapse(C)">
                        <i class="fa fa-caret-{{C.collapsed ? 'left' : 'down'}}"></i>
                    </button>
                    <button class="uk-button uk-button-mini uk-button-danger" @click="widgetRemove(\$parent.\$index, \$index, \$event)">
                        <i class="fa fa-remove"></i>
                    </button>
                </div>  
                
                <div class="widget-inside">                
                    <component :is="C.name" :name="C.name" :title="C.title" :data.sync="C.data"></component>                    
                </div>
            </div>
            
		</div>
	
    </div>	
</div>

HTML;
