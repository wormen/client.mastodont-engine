<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 17.02.2016
 * Time: 10:10
 */


return <<<HTML

<div class="col-md-12">

    <ul id="list" class="uk-nestable">
        <articles-category-item v-for="I in categoryList" :item.sync="I" track-by="\$index"></articles-category-item>
    </ul>

    <div class="clearfix"></div>

</div>
<div class="clearfix"></div>
HTML;
