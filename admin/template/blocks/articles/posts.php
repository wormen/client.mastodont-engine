<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 17.02.2016
 * Time: 10:10
 */

return <<<HTML
<table class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th>{$lang['label']['name']}</th>
            <th class="col-xs-1 text-center">{$lang['label']['status']}</th>
            <th class="col-xs-1">{$lang['label']['functions']}</th>
        </tr>
    </thead>
    <tbody>

        <tr v-for="S in postList" style="display:none;" v-show="postList.length>0">
            <td>{{S.name}}</td>
            <td>
                <div class="btn btn-xs btn-default list status" :class="{active: S.enabled, inactive: !S.enabled}" @click="OnOff(S)"></div>
            </td>
            <td class="function">
                <a class="btn btn-xs btn-info" href="{$moduleUrl}&action=edit-post&id={{S._id}}">
                    <i class="glyphicons glyphicons-pencil"></i>
                </a>
                <a class="btn btn-xs btn-danger" href="#" @click="onDelete(S._id, \$event)">
                    <i class="glyphicon glyphicon-remove"></i>
                </a>
            </td>
        </tr>

    </tbody>
</table>
<div class="clearfix"></div>
HTML;
