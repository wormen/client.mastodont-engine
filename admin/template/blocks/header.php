<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 23.01.2016
 * Time: 20:31
 */

$authBlock = include implode(DIRECTORY_SEPARATOR, [AdminTemplate, 'blocks', 'auth.php']);
$sb = implode(DIRECTORY_SEPARATOR, [AdminTemplate, 'blocks', 'sidebar']);
$alerts = '';//include $sb . '/alerts.php';
$messages = '';//include $sb . '/messages.php';

$langs = '<div class="locale-menu">
            <button class="btn btn-sm dropdown-animate" type="button" @click="onLangList">
                <span class="active-locale">
                    <img src="/admin/template/img/lang/24/{$locale}.png"/>
                </span>&nbsp;
            </button>
        </div>';

if(!$config['site']['multilang']) $langs = '';

$login = isset($_SESSION['LOGIN']) ? $_SESSION['LOGIN'] : '';
$goSite = '//'.$_SERVER['HTTP_HOST'];

return <<<HTML
<header class="navbar navbar-fixed-top" style="display: block;">
    <div class="pull-left">
        <a class="navbar-brand" href="/">
            <div class="navbar-logo">
                <img src="/admin/template/img/logos/logo_text.png" class="img-responsive" alt="">
            </div>
        </a>
    </div>

    <div class="pull-right header-btns">

        <div>
            <a href="{$goSite}" target="_blank">
                <button type="button" class="btn btn-sm ">
                    <span class="glyphicons glyphicons-globe"></span>
                </button>
            </a>
        </div>

        {$messages}
        {$alerts}
        {$langs}

        <div class="btn-group user-menu" @click="onUserMenu()">
            <button type="button" class="btn btn-sm dropdown-toggle">
                <span class="glyphicons glyphicons-user"></span> <b>{$login}</b>
            </button>
        </div>

    </div>

    {$authBlock}
</header>
HTML;
