<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 16.02.2016
 * Time: 13:54
 */

return <<<HTML

<div class="pull-left top-10">
    <button class="btn btn-default" @click="loadAdList">
        <i class="glyphicon glyphicon-refresh"></i>
    </button>
</div>

<div class="pull-right top-10">

    <div class="form-inline">
        <div class="form-group">
            <label>{$lang['label']['filterStatus']}</label>
            
            <select class="form-control" v-model="filter.status">
                <option value=""> {$lang['btns']['all']} </option>
                <option v-for="(k, v) in lang.ads.moderateStatus" :value="k"> {{v}} </option>
            <select>
            
        </div>
    </div>
           
</div>
<div class="clearfix"></div>
<hr/>

<table class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th class="col-xs-1">ID</th>
            <th>{$lang['label']['name']}</th>
            <th class="col-xs-1"></th>
            <!-- <th class="col-xs-1 text-center">{$lang['label']['status']}</th> -->
            <th class="col-xs-1">{$lang['label']['functions']}</th>
        </tr>
    </thead>
    <tbody>

        <tr v-for="A in adsList.list | filterBy filter.status in 'moderateStatus'" track-by="\$index" style="display:none;" v-show="adsList.list.length>0">
            <td>{{A.id}}</td>
            <td>{{A.name}}</td>
            <td class="text-center">
                               
                <div class="dropdown">
                    <button class="btn btn-{{adStatus(A.moderateStatus).class}} btn-xs dropdown-toggle" type="button" id="dropdownMenu-{{\$index}}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        {{lang.ads.moderateStatus[A.moderateStatus]}}
                        <span class="caret"></span>
                    </button>
                
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu-{{\$index}}">
                        
                        <li v-for="(k, v) in lang.ads.moderateStatus" v-if="k != A.moderateStatus" @click="adSetStatus(A, k, \$event)">
                            <a href="#"><i class="{{adStatus(k).ico}}"></i> {{v}}</a>
                        </li>
                                  
                    </ul>
                    
                </div>
                
            </td>
            
            <!--
            <td>
                <div class="btn btn-xs btn-default list status" :class="{active: A.enabled, inactive: !A.enabled}" @click="OnOff(A)"></div>
            </td>
            -->
            
            <td class="function">
                <a class="btn btn-xs btn-default" href="{{A.url}}?moderation=true" target="_blank">
                    <i class="glyphicons glyphicons-link"></i>
                </a>
                
                <!--
                <a class="btn btn-xs btn-danger" href="#" @click="onDelete(A._id, \$event)">
                    <i class="glyphicon glyphicon-remove"></i>
                </a>
                -->
            </td>
                       
        </tr>

    </tbody>
</table>
<div class="clearfix"></div>

<paginator :data.sync="adsList"></paginator>
HTML;
