<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 16.02.2016
 * Time: 13:54
 */

return <<<HTML

<div class="col-md-12">
    <div class="btn-group bottom-15">
        <a class="btn btn-default" href="javascript:;" data-uk-modal="{target:'#category-item'}" @click="onAdd">{$lang['btns']['add']}</a>
    </div>
    <div class="clearfix"></div>
    <hr class="short margin-top-none">
</div>

<div class="col-md-12">
    <div class="panel panel-visible">
        <div class="panel-body">

            <ul id="list" class="uk-nestable">
                <ads-category-item v-for="I in categoryList" :item.sync="I" track-by="\$index"></ads-category-item>
            </ul>

            <div class="clearfix"></div>
        </div>
    </div>
</div>
<div class="clearfix"></div>

HTML;
