<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 08.02.2016
 * Time: 18:26
 */

$uid = isset($_SESSION['USER_ID']) ? $_SESSION['USER_ID'] : '';
return <<<HTML
<ul class="account-menu">
    <li>
        <div class="item-icon"><i class="fa fa-envelope-o"></i> </div>
        <a class="item-message" href="javascript:void(0);">{$lang['sidebar']['messages']}</a>
    </li>

    <li class="border-bottom-none">
        <div class="item-icon"><i class="fa fa-cog"></i> </div>
        <a class="item-message" href="/admin/?module=users&action=edit&id={$uid}">{$lang['sidebar']['setting']}</a>
    </li>

    <li class="padding-none">
        <div class="dropdown-lockout">
            <!--<i class="fa fa-lock"></i>-->
            <a>&nbsp;</a>
        </div>

        <div class="dropdown-signout" @click="LogOut()">
            <i class="fa fa-sign-out"></i>
            <a href="javascript:void(0);">{$lang['btns']['LogOut']}</a>
        </div>
    </li>

</ul>
HTML;
