<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 08.02.2016
 * Time: 19:58
 */

return <<<HTML
<div class="messages-menu">
    <button type="button" class="btn btn-sm dropdown-animate" data-toggle="dropdown">
        <span class="glyphicon glyphicon-comment" style=""></span>
        <b>4</b>
    </button>

    <ul class="dropdown-menu checkbox-persist animated-short animated flipInY" role="menu">
        <li class="menu-arrow">
            <div class="menu-arrow-up"></div>
        </li>
        <li class="dropdown-header">Recent Messages <span class="pull-right glyphicons glyphicons-comment"></span></li>
        <li>
            <ul class="dropdown-items">
                <li>
                    <div class="item-avatar">
                        <!--<img src="/img/avatars/header/2.jpg" class="img-responsive" alt="avatar">-->
                    </div>
                    <div class="item-message"><b>Maggie</b> - <small class="text-muted">12 minutes ago</small><br>
                        Great work Yesterday!</div>
                </li>
                <li>
                    <div class="item-avatar">
                        <!--<img src="/img/avatars/header/3.jpg" class="img-responsive" alt="avatar">-->
                    </div>
                    <div class="item-message"><b>Robert</b> - <small class="text-muted">3 hours ago</small><br>
                        Is the Titan Project still on?</div>
                </li>
                <li>
                    <div class="item-avatar">
                        <!--<img src="/img/avatars/header/1.jpg" class="img-responsive" alt="avatar">-->
                    </div>
                    <div class="item-message"><b>Cynthia</b> - <small class="text-muted">14 hours ago</small><br>
                        Don't forget about the staff meeting tomorrow</div>
                </li>
                <li>
                    <div class="item-avatar">
                        <!--<img src="/img/avatars/header/4.jpg" class="img-responsive" alt="avatar">-->
                    </div>
                    <div class="item-message"><b>Matt</b> - <small class="text-muted">2 days ago</small><br>
                        Thor Project cancelled, Sorry.</div>
                </li>
            </ul>
        </li>
        <li class="dropdown-footer"><a href="messages.html">View All Messages <i class="fa fa-caret-right"></i> </a></li>
    </ul>
</div>
HTML;
