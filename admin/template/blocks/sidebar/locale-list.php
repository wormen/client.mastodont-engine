<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 08.02.2016
 * Time: 18:51
 */

return <<<HTML
<div class="locale-menu">
    <ul class="items">
        <li v-for="el in localeList" @click="onSelectLang(el.locale)" class="{{locale==el.locale ? 'active' : ''}}">

            <div class="flag">
                <img :src="'/admin/template/img/lang/32/'+el.locale+'.png'"/>
            </div>
            <div class="lang-name">{{el.title}}</div>
        </li>
    </ul>
</div>
HTML;
