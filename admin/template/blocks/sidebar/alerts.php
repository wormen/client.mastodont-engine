<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 08.02.2016
 * Time: 19:57
 */

return <<<HTML
<div class="alerts-menu">
    <button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown"> <span class="glyphicons glyphicons-bell"></span> <b>3</b> </button>

    <ul class="dropdown-menu checkbox-persist animated-short animated flipInY" role="menu">
        <li class="menu-arrow">
            <div class="menu-arrow-up"></div>
        </li>
        <li class="dropdown-header">Recent Alerts <span class="pull-right glyphicons glyphicons-bell"></span></li><li>
        <ul class="dropdown-items">
            <li>
                <div class="item-icon"><i style="color: #0066ad;" class="fa fa-facebook"></i> </div>
                <div class="item-message"><a href="#">Facebook likes reached <b>8,200</b></a></div>
            </li>
            <li>
                <div class="item-icon"><i style="color: #5cb85c;" class="fa fa-check"></i> </div>
                <div class="item-message"><a href="#">Robert <b>completed task</b> - Client SEO Revamp</a></div>
            </li>
            <li>
                <div class="item-icon"><i style="color: #f0ad4e" class="fa fa-user"></i> </div>
                <div class="item-message"><a href="#"><b>Marko</b> logged 12 hours</a></div>
            </li>
        </ul>
    </li>
        <li class="dropdown-footer"><a href="#">View All Alerts <i class="fa fa-caret-right"></i> </a></li>
    </ul>
</div>
HTML;
