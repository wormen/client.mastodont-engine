<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 16.02.2016
 * Time: 13:54
 */

return <<<HTML

<div class="top-10">
    <button class="btn btn-default" data-uk-modal="{target:'#video-item'}" @click="onAddVideo">
        {$lang['btns']['add']}
    </button>
    
    <button class="btn btn-default" @click="loadVideoList">
        <i class="glyphicon glyphicon-refresh"></i>
    </button>
    
</div>

<hr/>

<table class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th>ID</th>
            <th>{$lang['label']['name']}</th>
            <th class="col-xs-1"></th>
            <th class="col-xs-1 text-center">{$lang['label']['status']}</th>
            <th class="col-xs-1">{$lang['label']['functions']}</th>
        </tr>
    </thead>
    <tbody>

        <tr v-for="A in videoList.list" style="display:none;" v-show="videoList.list.length>0">
            <td>{{A.id}}</td>
            <td>{{A.name}}</td>
            <td class="text-center">
                <span class="label {{adStatus(A.moderateStatus)}}">{{lang.ads.moderateStatus[A.moderateStatus]}}</span>
            </td>
            <td>
                <div class="btn btn-xs btn-default list status" :class="{active: A.enabled, inactive: !A.enabled}" @click="OnOff(A)"></div>
            </td>
            <td class="function">
            
                <a class="btn btn-xs btn-default" href="{{A.url}}?moderation=true" target="_blank">
                    <i class="glyphicons glyphicons-link"></i>
                </a>                           
                <a class="btn btn-xs btn-danger" href="#" @click="onDelete(A._id, \$event)">
                    <i class="glyphicon glyphicon-remove"></i>
                </a> 
                       
            </td>
                       
        </tr>

    </tbody>
</table>
<div class="clearfix"></div>

<paginator :data.sync="videoList"></paginator>
HTML;
