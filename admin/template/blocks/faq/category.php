<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 09.02.2016
 * Time: 19:07
 */

return <<<HTML
<table class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th>{$lang['label']['name']}</th>
            <th class="col-xs-1">{$lang['label']['functions']}</th>
        </tr>
    </thead>
    <tbody>

        <tr v-for="S in categoryList" style="display:none;" v-show="categoryList.length>0">
            <td>{{S.name}}</td>
            <td class="function">
                <a class="btn btn-xs btn-info" href="{$moduleUrl}&action=edit-category&id={{S._id}}">
                    <i class="glyphicons glyphicons-pencil"></i>
                </a>
                <a class="btn btn-xs btn-danger" href="#" @click="onDeleteCat(S._id, \$event)">
                    <i class="glyphicon glyphicon-remove"></i>
                </a>
            </td>
        </tr>

    </tbody>
</table>
HTML;
