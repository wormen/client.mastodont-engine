<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 24.01.2016
 * Time: 5:00
 */

return <<<HTML
<div class="container">
    <div class="row">

        <div class="col-md-6">
            <div class="form-horizontal">

                <div class="form-group">
                    <label class="col-lg-3 control-label">{$lang['setting']['licKey']}</label>
                    <div class="col-lg-9">
                        <textarea class="form-control" v-model="general.licKey"></textarea>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-lg-3 control-label">{$lang['setting']['technicalDomain']}</label>
                    <div class="col-lg-9">
                        <input class="form-control" v-model="general.technicalDomain" placeholder="site.com" />
                    </div>
                </div>

<!--
                <div class="form-group">
                    <label class="col-lg-3 control-label">
                        <i class="info-ico glyphicons glyphicons-circle_question_mark" data-uk-tooltip="{pos:'right'}" title="{$lang['tooltip'][21]}"></i>
                        {$lang['setting']['expires']}
                    </label>
                    <div class="col-lg-4">
                        <input class="form-control" type="text" v-model="general.expires">
                    </div>
                </div>
-->

<!--
                <div class="form-group">
                    <label class="col-lg-3 control-label">
                        <i class="info-ico glyphicons glyphicons-circle_question_mark" data-uk-tooltip="{pos:'right'}" title="{$lang['tooltip'][22]}"></i>
                        {$lang['setting']['debug']}
                    </label>
                    <div class="col-lg-4">
                        <switch-box :value.sync="general.debug"></switch-box>
                    </div>
                </div>
-->

                <div class="clearfix"></div>
                <hr/>

                <div class="form-group">
                    <label class="col-lg-3 control-label">{$lang['setting']['botsEnable']}</label>
                    <div class="col-lg-4">
                        <switch-box :value.sync="general.botsEnable"></switch-box>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label">
                        <i class="info-ico glyphicons glyphicons-circle_question_mark" data-uk-tooltip="{pos:'right'}" title="{$lang['tooltip'][33]}"></i>
                        {$lang['setting']['botsList']}
                    </label>
                    <div class="col-lg-9">
                        <textarea class="form-control" v-model="general.botsList" style="min-height: 180px;"></textarea>
                    </div>
                </div>

            </div>
            <div class="clearfix"></div>
        </div>


        <div class="col-md-6">
            <div class="form-horizontal">

                <div class="form-group">
                    <label class="col-lg-4 control-label">
                        <i class="info-ico glyphicons glyphicons-circle_question_mark" data-uk-tooltip="{pos:'right'}" title="{$lang['tooltip'][20]}"></i>
                        {$lang['setting']['emailAdmin']}
                    </label>
                    <div class="col-lg-8">
                        <input class="form-control" type="email" v-model="general.emailAdmin" placeholder="admin@site.com" :style="{'border-color': CE(general.emailAdmin) ? '' : 'red'}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-4 control-label">{$lang['setting']['emailSupport']}</label>
                    <div class="col-lg-8">
                        <input class="form-control" type="email" v-model="general.emailSupport" placeholder="support@site.com" :style="{'border-color': CE(general.emailSupport) ? '' : 'red'}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-4 control-label">{$lang['setting']['personalDepartment']}</label>
                    <div class="col-lg-8">
                        <input class="form-control" type="email" v-model="general.personalDepartment" placeholder="personal@site.com" :style="{'border-color': CE(general.personalDepartment) ? '' : 'red'}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-4 control-label">{$lang['setting']['salesDepartment']}</label>
                    <div class="col-lg-8">
                        <input class="form-control" type="email" v-model="general.salesDepartment" placeholder="sales@site.com" :style="{'border-color': CE(general.salesDepartment) ? '' : 'red'}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-4 control-label">{$lang['setting']['wholesaleDepartment']}</label>
                    <div class="col-lg-8">
                        <input class="form-control" type="email" v-model="general.wholesaleDepartment" placeholder="opt@site.com" :style="{'border-color': CE(general.wholesaleDepartment) ? '' : 'red'}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-4 control-label">{$lang['setting']['informationDepartment']}</label>
                    <div class="col-lg-8">
                        <input class="form-control" type="email" v-model="general.informationDepartment" placeholder="info@site.com" :style="{'border-color': CE(general.informationDepartment) ? '' : 'red'}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-4 control-label">{$lang['setting']['marketingDepartment']}</label>
                    <div class="col-lg-8">
                        <input class="form-control" type="email" v-model="general.marketingDepartment" placeholder="marketing@site.com" :style="{'border-color': CE(general.marketingDepartment) ? '' : 'red'}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-4 control-label">{$lang['setting']['advertisementDepartment']}</label>
                    <div class="col-lg-8">
                        <input class="form-control" type="email" v-model="general.advertisementDepartment" placeholder="advertising@site.com" :style="{'border-color': CE(general.advertisementDepartment) ? '' : 'red'}">
                    </div>
                </div>


            </div>
            <div class="clearfix"></div>
        </div>

    </div>
</div>
HTML;
