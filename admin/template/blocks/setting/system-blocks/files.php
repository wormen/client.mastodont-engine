<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 26.02.2016
 * Time: 14:31
 */



return <<<HTML

<div class="col-xs-12">
    <label class="control-label">{$lang['setting']['configPreview']}</label>
    <div class="clearfix"></div>
    <div class="alert alert-info top-15">{$lang['setting']['preview']['info']}</div>
</div>

<div class="clearfix"></div>
<hr class="short margin-top-none">
    
<div class="col-xs-6">

    <div class="btn-group bottom-15">
        <a class="btn btn-default" href="#" @click.prevent="addParamBlock">
            <i class="glyphicon glyphicon-plus"></i>
            {$lang['setting']['preview']['addParam']}
        </a>
    </div>

    <div class="clearfix"></div>
    <hr class="short margin-top-none">


    <div  class="contacts-list-wrap">
        <div v-for="T in system.files.thumbs" class="contacts-list">

            <div class="thumbnail">
                <div class="caption">

                    <div class="function">
                        <a class="btn btn-xs btn-{{T.enabled ? 'success' : 'danger'}} btn-gradient" @click.prevent="onOffParamBlock(T)">
                            <i class="glyphicon glyphicon-{{T.enabled ? 'eye-open' : 'eye-close'}}"></i>
                        </a>
                        <a class="btn btn-xs btn-default" href="#" @click.prevent="editParamBlock(\$index)">
                            <i class="glyphicon glyphicon-pencil"></i>
                        </a>
                        <a class="btn btn-xs btn-danger" href="#" @click.prevent="deleteParamBlock(\$index)">
                            <i class="glyphicon glyphicon-remove"></i>
                        </a>
                    </div>

                    <h4>
                        <span v-show="T.name || T.width && T.height">{{T.name ? T.name : T.width+'x'+T.height}}</span>
                    </h4>

                    <ul>
                        <li>
                            <span>{$lang['setting']['preview']['width']}: </span>
                            <span class="value">{{T.width ? T.width+'px' : ''}}</span>
                        </li>
                        <li>
                            <span>{$lang['setting']['preview']['height']}: </span>
                            <span class="value">{{T.height ? T.height+'px' : ''}}</span>
                        </li>
                    </ul>
                                
                </div>
            </div>
        </div>
    </div>

</div>


<div class="col-xs-6 contact-block">

    <div class="form-group">
        <label class="col-xs-6 control-label">{$lang['setting']['preview']['auto_orient']}</label>
        <div class="col-xs-4">
            <switch-box :value.sync="system.files.auto_orient"></switch-box>
        </div>
    </div>
    <div class="clearfix"></div>
    
    <div class="form-group top-10">
        <label class="col-xs-4 control-label">{$lang['setting']['preview']['max_width']}</label>
        <div class="col-xs-8">
            <input class="form-control" type="text" v-model="system.files.max_width">
        </div>
    </div>
    <div class="clearfix"></div>
    
    <div class="form-group top-10">
        <label class="col-xs-4 control-label">{$lang['setting']['preview']['min_width']}</label>
        <div class="col-xs-8">
            <input class="form-control" type="text" v-model="system.files.min_width">
        </div>
    </div>
    <div class="clearfix"></div>
    
    <div class="form-group top-10">
        <label class="col-xs-4 control-label">{$lang['setting']['preview']['max_height']}</label>
        <div class="col-xs-8">
            <input class="form-control" type="text" v-model="system.files.max_height">
        </div>
    </div>
    <div class="clearfix"></div>
    
    <div class="form-group top-10">
        <label class="col-xs-4 control-label">{$lang['setting']['preview']['min_height']}</label>
        <div class="col-xs-8">
            <input class="form-control" type="text" v-model="system.files.min_height">
        </div>
    </div>
    <div class="clearfix"></div>
    
    
    

    <div class="panel panel-default top-20">
        <div class="panel-heading">
            <h3 class="panel-title">{$lang['setting']['preview']['conf']}</h3>
        </div>
        <div class="panel-body">
            <div class="alert alert-info">{$lang['setting']['preview']['maxInfo']}</div>
            <div class="clearfix"></div>
            
            <div class="form-horizontal top-15">

                <div class="form-group">
                    <label class="col-xs-4 control-label">{$lang['setting']['preview']['name']}</label>
                    <div class="col-xs-8">
                        <input class="form-control" type="text" v-model="thumbEdit.name">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-xs-4 control-label">{$lang['setting']['preview']['width']}</label>
                    <div class="col-xs-8">
                        <input class="form-control" type="text" v-model="thumbEdit.width">
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-xs-4 control-label">{$lang['setting']['preview']['height']}</label>
                    <div class="col-xs-8">
                        <input class="form-control" type="text" v-model="thumbEdit.height">
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-xs-4 control-label">{$lang['setting']['preview']['crop']}</label>
                    <div class="col-xs-4">
                        <switch-box :value.sync="thumbEdit.crop"></switch-box>
                    </div>
                </div>
                                

            </div>
        </div>
    </div>

</div>

HTML;
