<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 13.03.2016
 * Time: 12:14
 */

return <<<HTML

<div class="col-xs-6">
    
    <div  class="contacts-list-wrap">
        <div v-for="P in system.payment.list" class="payment-list">

            <div class="thumbnail">
                <div class="caption">

                    <div class="function">
                        <a class="btn btn-xs btn-{{P.enabled ? 'success' : 'danger'}} btn-gradient" @click.prevent="paymentOnOff(P)">
                            <i class="glyphicon glyphicon-{{P.enabled ? 'eye-open' : 'eye-close'}}"></i>
                        </a>
                        <a class="btn btn-xs btn-default" href="#" @click.prevent="paymentEdit(P)">
                            <i class="glyphicon glyphicon-pencil"></i>
                        </a>
                    </div>

                    <h4>
                        <span>{{lang.setting.payment.list[P.name].name}}</span>
                    </h4>
                               
                </div>
            </div>
        </div>
    </div>
    
</div>


<div class="col-xs-6">

<div class="form-horizontal">

    <div class="form-group top-10">
        <label class="col-xs-3 control-label">Success URL</label>
        <div class="col-xs-9">
            <input class="form-control" type="text" v-model="system.payment.successURL"/>
        </div>
    </div>
    <div class="clearfix"></div>
    
    <div class="form-group top-10">
        <label class="col-xs-3 control-label">Fail URL</label>
        <div class="col-xs-9">
            <input class="form-control" type="text" v-model="system.payment.failURL"/>
        </div>
    </div>
    <div class="clearfix"></div>
    
    

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">{$lang['setting']['payment']['setting']}</h3>
        </div>
        <div class="panel-body">                    
            <div class="form-horizontal">

                <div v-for="Pl in system.payment.list" v-show="activeEditPayment == Pl.name">
                    <component                      
                        :is="Pl.name" 
                        :lang="lang.setting.payment" 
                        :item.sync="Pl">
                    </component>
                </div>
                              
            </div>
        </div>
    </div>   
    
</div>

HTML;
