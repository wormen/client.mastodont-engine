<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 24.01.2016
 * Time: 5:01
 */

$modules = [
    ["nameBlock" => "account", "isActive" => true],
    ["nameBlock" => "news", "isActive" => false],
    ["nameBlock" => "static", "isActive" => true],
    ["nameBlock" => "ads", "isActive" => true],
    ["nameBlock" => "video", "isActive" => true],
    ["nameBlock" => "articles", "isActive" => true],
    ["nameBlock" => "shop", "isActive" => false],
    ["nameBlock" => "support", "isActive" => false],
    ["nameBlock" => "comments", "isActive" => true],
    ["nameBlock" => "brands", "isActive" => false],
    ["nameBlock" => "services", "isActive" => false],
    ["nameBlock" => "employees", "isActive" => false],
    ["nameBlock" => "gallary", "isActive" => false]
];

$list = '';
foreach($modules as $m){
    $ml = $lang['setting']['modules'][$m['nameBlock']];
    if($m['isActive']){
        $fileBlock = implode(DIRECTORY_SEPARATOR, [ __DIR__, 'module-blocks', $m['nameBlock'].'.php']);
        if(file_exists($fileBlock))
            $list .= include $fileBlock;
    }
}

return <<<HTML
<div class="col-sm-12">
    <div class="row">

        $list
        <div class="clearfix"></div>

    </div>
</div>
HTML;
