<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 26.02.2016
 * Time: 13:57
 */

$StabList = [
    ["nameBlock" => "files", "isActive" => true],
    ["nameBlock" => "payment", "isActive" => false],
    ["nameBlock" => "service-manager", "isActive" => false],
];

$Sbtns = '<ul class="nav panel-tabs sub">';
$Sblocks = '';
foreach($StabList as $st){

    $Sbtns .= '<li'.($st['isActive'] ? ' class="active"' : '').'>
                <a href="#'.$st['nameBlock'].'-sub-block" data-toggle="tab">'.$lang['setting']['system'][$st['nameBlock']].'</a>
            </li>';

    $Sblocks .= '<div id="'.$st['nameBlock'].'-sub-block" class="tab-pane sub'.($st['isActive'] ? ' active' : '').'">';

    $fileBlock = implode(DIRECTORY_SEPARATOR, [__DIR__, 'system-blocks', $st['nameBlock'].'.php']);
    if(file_exists($fileBlock))
        $Sblocks .= include $fileBlock;

    $Sblocks .= '<div class="clearfix"></div>';
    $Sblocks .= '</div>';

}
$Sbtns .= '</ul>';


return <<<HTML
<div class="col-sm-12">
    <div class="row">
        <div class="panel">
            
            <div class="panel-heading">
                <div class="panel-title"></div>
                {$Sbtns}
            </div>
            
            <div class="panel-body">
                <div class="tab-content padding-none border-none">
                    {$Sblocks}
                    <div class="clearfix"></div>
                </div>
            </div>
                       
            <div class="clearfix"></div>
        
        </div>
    </div>
</div>
HTML;
