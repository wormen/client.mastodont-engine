<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 24.01.2016
 * Time: 5:01
 */

$langList = '<select class="form-control" v-model="site.defaultLang">';
foreach($lang['langs'] as $k=>$v)
    $langList .= '<option value="'.$k.'"> '.$v.' </option>';
$langList .= '</select>';

return <<<HTML
<div class="container">
    <div class="row">

        <div class="col-md-6">
            <div class="form-horizontal">

                <div class="form-group">
                    <label class="col-lg-3 control-label">{$lang['setting']['siteName']}</label>
                    <div class="col-lg-9">
                        <input class="form-control" type="text" v-model="site.name">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label">{$lang['setting']['siteDefis']}</label>
                    <div class="col-lg-9">
                        <input class="form-control" type="text" v-model="site.defis">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label">{$lang['setting']['siteDescription']}</label>
                    <div class="col-lg-9">
                        <textarea class="form-control" v-model="site.description"></textarea>
                    </div>
                </div>

                <div class="clearfix"></div>
                <hr/>

                <div class="form-group">
                    <label class="col-lg-3 control-label">{$lang['setting']['template']}</label>
                    <div class="col-lg-5">
                        <select class="form-control" v-model="site.template">
                            <option v-for="T in templateList" :value="T.v"> {{T.l}} </option>
                        </select>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-lg-3 control-label">{$lang['setting']['userGroupDefault']}</label>
                    <div class="col-lg-5">
                        <select class="form-control" v-model="site.userGroupDefault">
                            <option v-for="G in userGroupList" :value="G.sys_name"> {{G.name}} </option>
                        </select>
                    </div>
                </div>

                <div class="clearfix"></div>
                <hr/>

                <div class="form-group">
                    <label class="col-lg-3 control-label">{$lang['setting']['cacheOn']}</label>
                    <div class="col-lg-4">
                        <switch-box :value.sync="site.cacheOn"></switch-box>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label">
                        <i class="info-ico glyphicons glyphicons-circle_question_mark" data-uk-tooltip="{pos:'right'}" title="{$lang['tooltip'][30]}"></i>
                        {$lang['setting']['cacheTimeout']}
                    </label>
                    <div class="col-lg-3">
                        <input class="form-control" type="number" min="1" v-model="site.cacheTimeout">
                    </div>
                </div>

                <div class="clearfix"></div>
                <hr/>
                
                <div class="form-group">
                    <label class="col-lg-3 control-label">{$lang['setting']['scriptsHead']}</label>
                    <div class="col-lg-9">
                        <textarea class="form-control" v-model="site.scripts.head"></textarea>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-lg-3 control-label">{$lang['setting']['scriptsBody']}</label>
                    <div class="col-lg-9">
                        <textarea class="form-control" v-model="site.scripts.body"></textarea>
                    </div>
                </div>
                
                
                <div class="clearfix"></div>
                <hr/>

                <div class="form-group">
                    <label class="col-lg-3 control-label">{$lang['setting']['mainPageTitle']}</label>
                    <div class="col-lg-9">
                        <input class="form-control" type="text" v-model="site.mainPageTitle">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label">{$lang['setting']['mainPageSeoTitle']}</label>
                    <div class="col-lg-9">
                        <input class="form-control" type="text" v-model="site.mainPageSeoTitle">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label">{$lang['setting']['mainPageSeoText']}</label>
                    <div class="col-lg-9">
                        <editor :value.sync="site.mainPageSeoText"></editor>
                    </div>
                </div>


<!--
                <div class="form-group">
                    <label class="col-lg-3 control-label">{$lang['setting']['moduleNoPrefix']}</label>
                    <div class="col-lg-5">
                        <select class="form-control" v-model="site.moduleNoPrefix" options="modulesPrefix"></select>
                    </div>
                </div>

                <div class="clearfix"></div>
                <hr/>



                <div class="form-group">
                    <label class="col-lg-3 control-label">
                        <i class="info-ico glyphicons glyphicons-circle_question_mark" data-uk-tooltip="{pos:'right'}" title="{$lang['tooltip'][31]}"></i>
                        {$lang['setting']['cookieDomain']}
                    </label>
                    <div class="col-lg-3">
                        <input class="form-control" type="text" min="1" v-model="site.cookieDomain">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label">
                        <i class="info-ico glyphicons glyphicons-circle_question_mark" data-uk-tooltip="{pos:'right'}" title="{$lang['tooltip'][32]}"></i>
                        {$lang['setting']['cookiePrefix']}
                    </label>
                    <div class="col-lg-3">
                        <input class="form-control" type="text" min="1" v-model="site.cookiePrefix">
                    </div>
                </div>
-->

            </div>
            <div class="clearfix"></div>
        </div>


        <div class="col-md-6">
            <div class="form-horizontal">

                <div class="form-group">
                    <label class="col-lg-3 control-label">{$lang['setting']['multicity']}</label>
                    <div class="col-lg-4">
                        <switch-box :value.sync="site.multicity"></switch-box>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label">{$lang['setting']['multilang']}</label>
                    <div class="col-lg-4">
                        <switch-box :value.sync="site.multilang"></switch-box>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label">{$lang['setting']['defaultLang']}</label>
                    <div class="col-lg-5">{$langList}</div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label">
                        <i class="info-ico glyphicons glyphicons-circle_question_mark" data-uk-tooltip="{pos:'right'}" title="{$lang['tooltip'][26]}"></i>
                        {$lang['setting']['siteClose']}
                    </label>
                    <div class="col-lg-4">
                        <switch-box id="enabled" :value.sync="site.closed"></switch-box>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label">
                        <i class="info-ico glyphicons glyphicons-circle_question_mark" data-uk-tooltip="{pos:'right'}" title="{$lang['tooltip'][25]}"></i>
                        {$lang['setting']['siteClose_mes']}
                    </label>
                    <div class="col-lg-9">
                        <editor :value.sync="site.close_mes"></editor>
                    </div>
                </div>

            </div>
            <div class="clearfix"></div>
        </div>

    </div>
</div>
HTML;
