<?php

return <<<HTML
<div class="panel panel-default">
    <div class="panel-heading" role="tab">
        <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#{$m['nameBlock']}-module" class="module-list btn btn-xs btn-default btn-gradient collapsed">
                <i class="fa fa-angle-double-up"></i>
                <i class="fa fa-angle-double-down"></i>
            </a>

            {$ml['title']}
        </h4>
       
        <div class="btn btn-xs btn-default list status" :class="{active: modules.{$m['nameBlock']}.enabled, inactive: !modules.{$m['nameBlock']}.enabled}" @click="OnOff(modules.{$m['nameBlock']})"></div>
      
    </div>
    <div id="{$m['nameBlock']}-module" class="panel-collapse collapse" role="tabpanel">
        <div class="panel-body">

            <div class="col-md-6">
                <div class="col-md-12">
                    <div class="form-horizontal">

                        <div class="form-group">
                            <label class="col-lg-4 control-label">{$ml['curencyDefault']}</label>
                            <div class="col-lg-5">
                                <select class="form-control" v-model="modules.shop.curencyDefault" options="curencyList"></select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">
                                <i class="info-ico glyphicons glyphicons-circle_question_mark" data-uk-tooltip="{pos:'right'}" title="{$lang['tooltip'][9]}"></i>
                                {$ml['autoReMath']}
                            </label>
                            <div class="col-lg-8">
                                <switch-box :value.sync="modules.shop.autoReMath"></switch-box>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <hr/>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">{$ml['linkModule']}</label>
                            <div class="col-lg-3">
                                <input class="form-control" type="text" v-model="modules.shop.linkModule">
                            </div>
                            <div class="col-lg-5">
                                <label class="control-label">site.com/shop/<span class="go-link">{{modules.shop.linkModule}}</span></label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">{$ml['categoryModuleLink']}</label>
                            <div class="col-lg-3">
                                <input class="form-control" type="text" v-model="modules.shop.links.category">
                            </div>
                            <div class="col-lg-5">
                                <label class="control-label">site.com/shop/<span class="go-link">{{modules.shop.links.category}}</span>/</label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">{$ml['productModuleLink']}</label>
                            <div class="col-lg-3">
                                <input class="form-control" type="text" v-model="modules.shop.links.product">
                            </div>
                            <div class="col-lg-5">
                                <label class="control-label">site.com/shop/<span class="go-link">{{modules.shop.links.product}}</span>/</label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">
                                <i class="info-ico glyphicons glyphicons-circle_question_mark" data-uk-tooltip="{pos:'right'}" title="{$lang['tooltip'][4]}"></i>
                                {$ml['cartLink']}
                            </label>
                            <div class="col-lg-3">
                                <input class="form-control" type="text" v-model="modules.shop.links.cart">
                            </div>
                            <div class="col-lg-5">
                                <label class="control-label">site.com/shop/<span class="go-link">{{modules.shop.links.cart}}</span></label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">
                                <i class="info-ico glyphicons glyphicons-circle_question_mark" data-uk-tooltip="{pos:'right'}" title="{$lang['tooltip'][5]}"></i>
                                {$ml['orderLink']}
                            </label>
                            <div class="col-lg-3">
                                <input class="form-control" type="text" v-model="modules.shop.links.order">
                            </div>
                            <div class="col-lg-5">
                                <label class="control-label">site.com/shop/<span class="go-link">{{modules.shop.links.order}}</span></label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">
                                <i class="info-ico glyphicons glyphicons-circle_question_mark" data-uk-tooltip="{pos:'right'}" title="{$lang['tooltip'][6]}"></i>
                                {$ml['cancelOrderLink']}
                            </label>
                            <div class="col-lg-3">
                                <input class="form-control" type="text" v-model="modules.shop.links.cancelOrder">
                            </div>
                            <div class="col-lg-5">
                                <label class="control-label">site.com/shop/<span class="go-link">{{modules.shop.links.cancelOrder}}</span>/</label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">
                                <i class="info-ico glyphicons glyphicons-circle_question_mark" data-uk-tooltip="{pos:'right'}" title="{$lang['tooltip'][7]}"></i>
                                {$ml['newProductsLink']}
                            </label>
                            <div class="col-lg-3">
                                <input class="form-control" type="text" v-model="modules.shop.links.newProducts">
                            </div>
                            <div class="col-lg-5">
                                <label class="control-label">site.com/shop/<span class="go-link">{{modules.shop.links.newProducts}}</span></label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">
                                <i class="info-ico glyphicons glyphicons-circle_question_mark" data-uk-tooltip="{pos:'right'}" title="{$lang['tooltip'][8]}"></i>
                                {$ml['sellsLink']}
                            </label>
                            <div class="col-lg-3">
                                <input class="form-control" type="text" v-model="modules.shop.links.sells">
                            </div>
                            <div class="col-lg-5">
                                <label class="control-label">site.com/shop/<span class="go-link">{{modules.shop.links.sells}}</span></label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">{$ml['collectionsLink']}</label>
                            <div class="col-lg-3">
                                <input class="form-control" type="text" v-model="modules.shop.links.collections">
                            </div>
                            <div class="col-lg-5">
                                <label class="control-label">site.com/shop/<span class="go-link">{{modules.shop.links.collections}}</span></label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">{$ml['collectionLink']}</label>
                            <div class="col-lg-3">
                                <input class="form-control" type="text" v-model="modules.shop.links.collection">
                            </div>
                            <div class="col-lg-5">
                                <label class="control-label">site.com/shop/<span class="go-link">{{modules.shop.links.collection}}</span>/</label>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <hr/>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">{$ml['inPage']}</label>
                            <div class="col-lg-3">
                                <input class="form-control" type="text" v-model="modules.shop.inPage">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">{$ml['inPageSearch']}</label>
                            <div class="col-lg-3">
                                <input class="form-control" type="text" v-model="modules.shop.inPageSearch">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">{$ml['inPageNewProduct']}</label>
                            <div class="col-lg-3">
                                <input class="form-control" type="text" v-model="modules.shop.inPageNewProduct">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">
                                <i class="info-ico glyphicons glyphicons-circle_question_mark" data-uk-tooltip="{pos:'right'}" title="{$lang['tooltip'][2]}"></i>
                                {$ml['cancelTimeOrder']}
                            </label>
                            <div class="col-lg-3">
                                <input class="form-control" type="text" v-model="modules.shop.cancelTimeOrder">
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <hr/>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">{$ml['ratingOn']}</label>
                            <div class="col-lg-8">
                                <switch-box :value.sync="modules.shop.ratingOn"></switch-box>
                            </div>
                        </div>

                        <span v-show="modules.shop.ratingOn">

                            <div class="form-group">
                                <label class="col-lg-4 control-label">{$ml['lowmark']}</label>
                                <div class="col-lg-3">
                                    <input class="form-control" type="text" v-model="modules.shop.lowmark">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label">{$ml['highmark']}</label>
                                <div class="col-lg-3">
                                    <input class="form-control" type="text" v-model="modules.shop.highmark">
                                </div>
                            </div>

                        </span>

                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="col-md-12">
                    <div class="form-horizontal">

                        <div class="form-group">
                            <label class="col-lg-4 control-label">
                                <i class="info-ico glyphicons glyphicons-circle_question_mark" data-uk-tooltip="{pos:'right'}" title="{$lang['tooltip'][10]}"></i>
                                {$ml['commentOn']}
                            </label>
                            <div class="col-lg-8">
                                <switch-box :value.sync="modules.shop.commentOn"></switch-box>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">{$ml['one_click']}</label>
                            <div class="col-lg-8">
                                <switch-box :value.sync="modules.shop.one_click"></switch-box>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">
                                <i class="info-ico glyphicons glyphicons-circle_question_mark" data-uk-tooltip="{pos:'right'}" title="{$lang['tooltip'][11]}"></i>
                                {$ml['viewChildCat']}
                            </label>
                            <div class="col-lg-8">
                                <switch-box :value.sync="modules.shop.viewChildCat"></switch-box>
                            </div>
                        </div>

                        <span v-show="modules.shop.autoReMath">
                            <div class="clearfix"></div>
                            <hr/>

                            <div class="form-group">
                                <label class="col-lg-4 col-xs-4 control-label">
                                    <i class="info-ico glyphicons glyphicons-circle_question_mark" data-uk-tooltip="{pos:'right'}" title="{$lang['tooltip'][28]}"></i>
                                    {$ml['markUpRate']}
                                </label>
                                <div class="col-lg-3 col-xs-3">
                                    <input class="form-control" type="text" v-model="modules.shop.markUpRate" v-bind:disabled="!modules.shop.autoReMath">
                                </div>
                                <span class="top-7" style="display: inline-block;"> %</span>
                            </div>

                        </span>

                        <div class="clearfix"></div>
                        <hr/>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">{$ml['grossMarginOn']}</label>
                            <div class="col-lg-8">
                                <switch-box :value.sync="modules.shop.grossMarginOn"></switch-box>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">
                                <i class="info-ico glyphicons glyphicons-circle_question_mark" data-uk-tooltip="{pos:'right'}" title="{$lang['tooltip'][3]}"></i>
                                {$ml['grossMargin']}
                            </label>
                            <div class="col-lg-3">
                                <input class="form-control" type="text" v-model="modules.shop.grossMargin" v-bind:disabled="!modules.shop.grossMarginOn">
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <hr/>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">
                                <i class="info-ico glyphicons glyphicons-circle_question_mark" data-uk-tooltip="{pos:'right'}" title="{$lang['tooltip'][3]}"></i>
                                {$ml['totalDiscount']}
                            </label>
                            <div class="col-lg-3">
                                <input class="form-control" type="text" v-model="modules.shop.totalDiscount" v-bind:disabled="!modules.shop.totalDiscountOn">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">{$ml['totalDiscountOn']}</label>
                            <div class="col-lg-8">
                                <switch-box :value.sync="modules.shop.totalDiscountOn"></switch-box>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">{$ml['priorityDiscountsOn']}</label>
                            <div class="col-lg-8">
                                <switch-box :value.sync="modules.shop.priorityDiscountsOn"></switch-box>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">{$ml['setDiscountsOn']}</label>
                            <div class="col-lg-8">
                                <switch-box :value.sync="modules.shop.setDiscountsOn"></switch-box>
                            </div>
                        </div>

                        <div class="form-group" v-show="modules.shop.priorityDiscountsOn">
                            <label class="col-lg-4 control-label">{$ml['priorityDiscount']}</label>
                            <div class="col-lg-5">
                                <select class="form-control" v-model="modules.shop.priorityDiscount" options="lang.setting.priorityDiscountVals"></select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">{$ml['summarizeDiscounts']}</label>
                            <div class="col-lg-8">
                                <switch-box :value.sync="modules.shop.summarizeDiscounts"></switch-box>
                            </div>
                        </div>

                        <span v-show="modules.shop.setDiscountsOn">
                            <div class="clearfix"></div>
                            <hr/>

                            <table class="table disount-list">
                                <caption><b>{$ml['setDiscounts']}:</b></caption>
                                <thead>
                                <tr>
                                    <th class="col-xs-3">% {$ml['discounts']}</th>
                                    <th>{$ml['amountOf']}</th>
                                    <th>{$ml['sumUp']}</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr v-for="D in discountList">
                                    <th scope="row"><input class="form-control" type="text" v-model="D.percent"></th>
                                    <td><input class="form-control" type="text" v-model="D.summFrom"></td>
                                    <td><input class="form-control" type="text" v-model="D.summTo"></td>
                                    <td class="function">
                                        <a class="btn btn-xs btn-danger btn-gradient" @click="onDiscountDeleteItem(\$index)">
                                            <i class="glyphicon glyphicon-remove"></i>
                                        </a>
                                        <a class="btn btn-xs btn-success btn-gradient" @click="onDiscountAddItem()" v-show="\$index==(discountList.length-1)">
                                            <i class="glyphicon glyphicon-plus"></i>
                                        </a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </span>

                        <span v-show="modules.shop.summarizeDiscounts">
                            <hr/>
                            <label class="bottom-25">{$ml['summarizeDiscounts_']}</label>
                            <div class="clearfix"></div>

                            <div class="form-group" v-show="modules.shop.priorityDiscountsOn">
                                <label class="col-lg-4 control-label">
                                    <i class="info-ico glyphicons glyphicons-circle_question_mark" data-uk-tooltip="{pos:'right'}" title="{$lang['tooltip'][29]}"></i>
                                    {$ml['priorityDiscount']}
                                </label>
                                <div class="col-lg-8">
                                    <switch-box :value.sync="modules.shop.SummarizeDiscounts.priorityDiscount"></switch-box>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label">{$ml['categoryProducts']}</label>
                                <div class="col-lg-8">
                                    <switch-box :value.sync="modules.shop.SummarizeDiscounts.category"></switch-box>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label">{$ml['product']}</label>
                                <div class="col-lg-8">
                                    <switch-box :value.sync="modules.shop.SummarizeDiscounts.product"></switch-box>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label">{$ml['setDiscounts']}</label>
                                <div class="col-lg-8">
                                    <switch-box :value.sync="modules.shop.SummarizeDiscounts.setDiscounts"></switch-box>
                                </div>
                            </div>

                        </span>

                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
HTML;
