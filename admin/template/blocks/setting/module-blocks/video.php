<?php

return <<<HTML
<div class="panel panel-default">
    <div class="panel-heading" role="tab">
        <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#{$m['nameBlock']}-module" class="module-list btn btn-xs btn-default btn-gradient collapsed">
                <i class="fa fa-angle-double-up"></i>
                <i class="fa fa-angle-double-down"></i>
            </a>

            {$ml['title']}
        </h4>
   
        <div class="btn btn-xs btn-default list status" :class="{active: modules.{$m['nameBlock']}.enabled, inactive: !modules.{$m['nameBlock']}.enabled}" @click="OnOff(modules.{$m['nameBlock']})"></div>
  
    </div>
    <div id="{$m['nameBlock']}-module" class="panel-collapse collapse" role="tabpanel">
        <div class="panel-body">


            <div class="col-md-6">
                <div class="col-md-12">
                    <div class="form-horizontal">

                        <div class="form-group">
                            <label class="col-lg-4 control-label">{$ml['linkModule']}</label>
                            <div class="col-lg-3">
                                <input class="form-control" type="text" v-model="modules.{$m['nameBlock']}.linkModule">
                            </div>
                            <div class="col-lg-5">
                                <label class="control-label">site.com/<span class="go-link">{{modules.{$m['nameBlock']}.linkModule}}</span>/</label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">{$ml['links']['category']}</label>
                            <div class="col-lg-3">
                                <input class="form-control" type="text" v-model="modules.{$m['nameBlock']}.links.category">
                            </div>
                            <div class="col-lg-5">
                                <label class="control-label">site.com/{{modules.{$m['nameBlock']}.linkModule}}/<span class="go-link">{{modules.{$m['nameBlock']}.links.category}}</span>/url</label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">{$ml['links']['video']}</label>
                            <div class="col-lg-3">
                                <input class="form-control" type="text" v-model="modules.{$m['nameBlock']}.links.video">
                            </div>
                            <div class="col-lg-5">
                                <label class="control-label">site.com/<span class="go-link">{{modules.{$m['nameBlock']}.links.video}}</span>/url</label>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            
            <div class="col-md-6">
                <div class="col-md-12">
                    <div class="form-horizontal">
                    

                         <div class="form-group">
                            <label class="col-lg-4 control-label">{$ml['inPage']}</label>
                            <div class="col-lg-3">
                                <input class="form-control" type="text" v-model="modules.{$m['nameBlock']}.inPage">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-lg-4 control-label">
                                <i class="info-ico glyphicons glyphicons-circle_question_mark" data-uk-tooltip="{pos:'right'}" title="{$lang['tooltip'][40]}"></i>
                                {$ml['useSystemApi']}
                            </label>
                            <div class="col-lg-8">
                                <switch-box :value.sync="modules.{$m['nameBlock']}.useSystemApi"></switch-box>
                            </div>
                        </div>
    
                        <div class="form-group">
                            <label class="col-lg-4 control-label">Youtube API key</label>
                            <div class="col-lg-8">
                                <textarea class="form-control" v-model="modules.{$m['nameBlock']}.youtubeAPI"></textarea>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-lg-4 control-label">{$ml['token']}</label>
                            <div class="col-lg-8">
                                <textarea class="form-control" v-model="modules.{$m['nameBlock']}.token"></textarea>
                            </div>
                        </div>
                    
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
HTML;
