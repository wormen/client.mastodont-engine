<?php

return <<<HTML
<div class="panel panel-default">
    <div class="panel-heading" role="tab">
        <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#{$m['nameBlock']}-module" class="module-list btn btn-xs btn-default btn-gradient collapsed">
                <i class="fa fa-angle-double-up"></i>
                <i class="fa fa-angle-double-down"></i>
            </a>

            {$ml['title']}
        </h4>

        <div class="btn btn-xs btn-default list status" :class="{active: modules.{$m['nameBlock']}.enabled, inactive: !modules.{$m['nameBlock']}.enabled}" @click="OnOff(modules.{$m['nameBlock']})"></div>

    </div>
    <div id="{$m['nameBlock']}-module" class="panel-collapse collapse" role="tabpanel">
        <div class="panel-body">




            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.



        </div>
    </div>
</div>
HTML;
