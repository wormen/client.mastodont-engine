<?php

return <<<HTML
<div class="panel panel-default">
    <div class="panel-heading" role="tab">
        <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#{$m['nameBlock']}-module" class="module-list btn btn-xs btn-default btn-gradient collapsed">
                <i class="fa fa-angle-double-up"></i>
                <i class="fa fa-angle-double-down"></i>
            </a>

            {$ml['title']}
        </h4>

        <div class="btn btn-xs btn-default list status" :class="{active: modules.{$m['nameBlock']}.enabled, inactive: !modules.{$m['nameBlock']}.enabled}" @click="OnOff(modules.{$m['nameBlock']})"></div>

    </div>
    <div id="{$m['nameBlock']}-module" class="panel-collapse collapse" role="tabpanel">
        <div class="panel-body">


            <div class="col-md-6">
                <div class="col-md-12">
                    <div class="form-horizontal">

                        <div class="form-group">
                            <label class="col-lg-4 control-label">{$ml['linkModule']}</label>
                            <div class="col-lg-3">
                                <input class="form-control" type="text" v-model="modules.account.linkModule">
                            </div>
                            <div class="col-lg-5">
                                <label class="control-label">site.com/<span class="go-link">{{modules.account.linkModule}}</span></label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">{$ml['registerLink']}</label>
                            <div class="col-lg-3">
                                <input class="form-control" type="text" v-model="modules.account.links.register">
                            </div>
                            <div class="col-lg-5">
                                <label class="control-label">site.com/{{modules.account.linkModule}}/<span class="go-link">{{modules.account.links.register}}</span></label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">{$ml['lostpassLink']}</label>
                            <div class="col-lg-3">
                                <input class="form-control" type="text" v-model="modules.account.links.lostpass">
                            </div>
                            <div class="col-lg-5">
                                <label class="control-label">site.com/{{modules.account.linkModule}}/<span class="go-link">{{modules.account.links.lostpass}}</span></label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">{$ml['changepassLink']}</label>
                            <div class="col-lg-3">
                                <input class="form-control" type="text" v-model="modules.account.links.changepass">
                            </div>
                            <div class="col-lg-5">
                                <label class="control-label">site.com/{{modules.account.linkModule}}/<span class="go-link">{{modules.account.links.changepass}}</span></label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">{$ml['settingsLink']}</label>
                            <div class="col-lg-3">
                                <input class="form-control" type="text" v-model="modules.account.links.settings">
                            </div>
                            <div class="col-lg-5">
                                <label class="control-label">site.com/{{modules.account.linkModule}}/<span class="go-link">{{modules.account.links.settings}}</span></label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">{$ml['ordersLink']}</label>
                            <div class="col-lg-3">
                                <input class="form-control" type="text" v-model="modules.account.links.orders">
                            </div>
                            <div class="col-lg-5">
                                <label class="control-label">site.com/{{modules.account.linkModule}}/<span class="go-link">{{modules.account.links.orders}}</span></label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">{$ml['reservationLink']}</label>
                            <div class="col-lg-3">
                                <input class="form-control" type="text" v-model="modules.account.links.reservation">
                            </div>
                            <div class="col-lg-5">
                                <label class="control-label">site.com/{{modules.account.linkModule}}/<span class="go-link">{{modules.account.links.reservation}}</span></label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">
                                <i class="info-ico glyphicons glyphicons-circle_question_mark" data-uk-tooltip="{pos:'right'}" title="{$lang['tooltip'][34]}"></i>
                                {$ml['wishlistLink']}
                            </label>
                            <div class="col-lg-3">
                                <input class="form-control" type="text" v-model="modules.account.links.wishlist">
                            </div>
                            <div class="col-lg-5">
                                <label class="control-label">site.com/{{modules.account.linkModule}}/<span class="go-link">{{modules.account.links.wishlist}}</span></label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">{$ml['messagesLink']}</label>
                            <div class="col-lg-3">
                                <input class="form-control" type="text" v-model="modules.account.links.messages">
                            </div>
                            <div class="col-lg-5">
                                <label class="control-label">site.com/{{modules.account.linkModule}}/<span class="go-link">{{modules.account.links.messages}}</span></label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">{$ml['payService']}</label>
                            <div class="col-lg-3">
                                <input class="form-control" type="text" v-model="modules.account.links['pay-service']">
                            </div>
                            <div class="col-lg-5">
                                <label class="control-label">site.com/{{modules.account.linkModule}}/<span class="go-link">{{modules.account.links['pay-service']}}</span></label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">{$ml['payHistory']}</label>
                            <div class="col-lg-3">
                                <input class="form-control" type="text" v-model="modules.account.links['pay-history']">
                            </div>
                            <div class="col-lg-5">
                                <label class="control-label">site.com/{{modules.account.linkModule}}/<span class="go-link">{{modules.account.links['pay-history']}}</span></label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">{$ml['adsLink']}</label>
                            <div class="col-lg-3">
                                <input class="form-control" type="text" v-model="modules.account.links.ads">
                            </div>
                            <div class="col-lg-5">
                                <label class="control-label">site.com/{{modules.account.linkModule}}/<span class="go-link">{{modules.account.links.ads}}</span></label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">{$ml['createAdLink']}</label>
                            <div class="col-lg-3">
                                <input class="form-control" type="text" v-model="modules.account.links['create-ad']">
                            </div>
                            <div class="col-lg-5">
                                <label class="control-label">site.com/{{modules.account.linkModule}}/<span class="go-link">{{modules.account.links['create-ad']}}</span></label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">{$ml['subscriptionLink']}</label>
                            <div class="col-lg-3">
                                <input class="form-control" type="text" v-model="modules.account.links.subscription">
                            </div>
                            <div class="col-lg-5">
                                <label class="control-label">site.com/{{modules.account.linkModule}}/<span class="go-link">{{modules.account.links.subscription}}</span></label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">{$ml['supportLink']}</label>
                            <div class="col-lg-3">
                                <input class="form-control" type="text" v-model="modules.account.links.support">
                            </div>
                            <div class="col-lg-5">
                                <label class="control-label">site.com/{{modules.account.linkModule}}/<span class="go-link">{{modules.account.links.support}}</span></label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">{$ml['userView']}</label>
                            <div class="col-lg-3">
                                <input class="form-control" type="text" v-model="modules.account.links['user-view']">
                            </div>
                            <div class="col-lg-5">
                                <label class="control-label">site.com/{{modules.account.linkModule}}/<span class="go-link">{{modules.account.links['user-view']}}</span>/login</label>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="col-md-12">
                    <div class="form-horizontal">

                        <div class="form-group">
                            <label class="col-lg-4 control-label">{$ml['on']}</label>
                            <div class="col-lg-8">
                                <switch-box :value.sync="modules.account.on"></switch-box>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">{$ml['antibruteСnt']}</label>
                            <div class="col-lg-3">
                                <input class="form-control" type="text" v-model="modules.account.antibruteCnt">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">
                                <i class="info-ico glyphicons glyphicons-circle_question_mark" data-uk-tooltip="{pos:'right'}" title="{$lang['tooltip'][35]}"></i>
                                {$ml['antibruteTime']}
                            </label>
                            <div class="col-lg-3">
                                <input class="form-control" type="text" v-model="modules.account.antibruteTime">
                            </div>
                        </div>

                        <span v-show="modules.account.on">
                            <div class="clearfix"></div>
                            <hr/>

                            <div class="form-group">
                                <label class="col-lg-4 control-label">{$ml['loginLength']}</label>
                                <div class="col-lg-3">
                                    <input class="form-control" type="text" v-model="modules.account.loginLength">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label">{$ml['passLength']}</label>
                                <div class="col-lg-3">
                                    <input class="form-control" type="text" v-model="modules.account.passLength">
                                </div>
                            </div>
                        </span>

                        <div class="clearfix"></div>
                        <hr/>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">{$ml['regType']}</label>
                            <div class="col-lg-5">
                                <select class="form-control" v-model="modules.account.regType">
                                    <option v-for="RT in lang.setting.regTypes" :value="RT.value">{{RT.text}}</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">{$ml['regUnactivated']}</label>
                            <div class="col-lg-5">
                                <select class="form-control" v-model="modules.account.regUnactivated">
                                    <option v-for="RU in lang.setting.regUnactivated" :value="RU.value">{{RU.text}}</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">
                                <i class="info-ico glyphicons glyphicons-circle_question_mark" data-uk-tooltip="{pos:'right'}" title="{$lang['tooltip'][36]}"></i>
                                {$ml['regActiveTime']}
                            </label>
                            <div class="col-lg-3">
                                <input class="form-control" type="text" v-model="modules.account.regActiveTime">
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
HTML;
