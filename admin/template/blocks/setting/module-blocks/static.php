<?php

return <<<HTML
<div class="panel panel-default">
    <div class="panel-heading" role="tab">
        <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#{$m['nameBlock']}-module" class="module-list btn btn-xs btn-default btn-gradient collapsed">
                <i class="fa fa-angle-double-up"></i>
                <i class="fa fa-angle-double-down"></i>
            </a>

            {$ml['title']}
        </h4>

        <div class="btn btn-xs btn-default list status" :class="{active: modules.{$m['nameBlock']}.enabled, inactive: !modules.{$m['nameBlock']}.enabled}" @click="OnOff(modules.{$m['nameBlock']})"></div>

    </div>

    <div id="{$m['nameBlock']}-module" class="panel-collapse collapse" role="tabpanel">
        <div class="panel-body">

            <div class="col-md-6">
                <div class="col-md-12">
                    <div class="form-horizontal">

                        <div class="form-group">
                            <label class="col-lg-4 control-label">{$ml['linkModule']}</label>
                            <div class="col-lg-3">
                                <input class="form-control" type="text" v-model="modules.static.linkModule">
                            </div>
                            <div class="col-lg-5">
                                <label class="control-label">site.com/<span class="go-link">{{modules.static.linkModule}}</span>/url</label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">{$ml['onRating']}</label>
                            <div class="col-lg-8">
                                <switch-box :value.sync="modules.static.onRating"></switch-box>
                            </div>
                        </div>


                    </div>
                </div>
            </div>


        </div>
    </div>
</div>
HTML;
