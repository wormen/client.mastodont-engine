<?php

return <<<HTML
<div class="panel panel-default">
    <div class="panel-heading" role="tab">
        <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#{$m['nameBlock']}-module" class="module-list btn btn-xs btn-default btn-gradient collapsed">
                <i class="fa fa-angle-double-up"></i>
                <i class="fa fa-angle-double-down"></i>
            </a>

            {$ml['title']}
        </h4>
   
        <div class="btn btn-xs btn-default list status" :class="{active: modules.{$m['nameBlock']}.enabled, inactive: !modules.{$m['nameBlock']}.enabled}" @click="OnOff(modules.{$m['nameBlock']})"></div>
  
    </div>
    <div id="{$m['nameBlock']}-module" class="panel-collapse collapse" role="tabpanel">
        <div class="panel-body">


            <div class="col-md-6">
                <div class="col-md-12">
                    <div class="form-horizontal">

                        <div class="form-group">
                            <label class="col-lg-4 control-label">{$ml['linkModule']}</label>
                            <div class="col-lg-3">
                                <input class="form-control" type="text" v-model="modules.articles.linkModule">
                            </div>
                            <div class="col-lg-5">
                                <label class="control-label">site.com/<span class="go-link">{{modules.articles.linkModule}}</span>/</label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">{$ml['linkCategoryModule']}</label>
                            <div class="col-lg-3">
                                <input class="form-control" type="text" v-model="modules.articles.links.category">
                            </div>
                            <div class="col-lg-5">
                                <label class="control-label">site.com/{{modules.articles.linkModule}}/<span class="go-link">{{modules.articles.links.category}}</span>/</label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">{$ml['linkPostModule']}</label>
                            <div class="col-lg-3">
                                <input class="form-control" type="text" v-model="modules.articles.links.post">
                            </div>
                            <div class="col-lg-5">
                                <label class="control-label">site.com/{{modules.articles.linkModule}}/<span class="go-link">{{modules.articles.links.post}}</span>/</label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">{$ml['linkTagModule']}</label>
                            <div class="col-lg-3">
                                <input class="form-control" type="text" v-model="modules.articles.links.tag">
                            </div>
                            <div class="col-lg-5">
                                <label class="control-label">site.com/{{modules.articles.linkModule}}/<span class="go-link">{{modules.articles.links.tag}}</span>/</label>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-lg-4 control-label">{$ml['inPage']}</label>
                            <div class="col-lg-3">
                                <input class="form-control" type="text" v-model="modules.articles.inPage">
                            </div>
                        </div>


                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
HTML;
