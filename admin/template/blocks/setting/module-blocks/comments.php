<?php

return <<<HTML
<div class="panel panel-default">
    <div class="panel-heading" role="tab">
        <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#{$m['nameBlock']}-module" class="module-list btn btn-xs btn-default btn-gradient collapsed">
                <i class="fa fa-angle-double-up"></i>
                <i class="fa fa-angle-double-down"></i>
            </a>

            {$ml['title']}
        </h4>
       
        <div class="btn btn-xs btn-default list status" :class="{active: modules.{$m['nameBlock']}.enabled, inactive: !modules.{$m['nameBlock']}.enabled}" @click="OnOff(modules.{$m['nameBlock']})"></div>
    
    </div>
    <div id="{$m['nameBlock']}-module" class="panel-collapse collapse" role="tabpanel">
        <div class="panel-body">


            <div class="col-md-6">
                <div class="col-md-12">
                    <div class="form-horizontal">

                        <div class="form-group">
                            <label class="col-lg-4 control-label">{$ml['commentsSort']}</label>
                            <div class="col-lg-5">
                                <select class="form-control" v-model="modules.{$m['nameBlock']}.commentsSort" options="lang.setting.commentsSort">
                                    <option v-for="S in lang.setting.commentsSort" :value="S.value">{{S.text}}</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">{$ml['inPage']}</label>
                            <div class="col-lg-3">
                                <input class="form-control" type="text" v-model="modules.{$m['nameBlock']}.inPage">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">{$ml['AdminInPage']}</label>
                            <div class="col-lg-3">
                                <input class="form-control" type="text" v-model="modules.{$m['nameBlock']}.AdminInPage">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">
                                <i class="info-ico glyphicons glyphicons-circle_question_mark" data-uk-tooltip="{pos:'right'}" title="{$lang['tooltip'][1]}"></i>
                                {$ml['commentsTimeLimit']}
                            </label>
                            <div class="col-lg-3">
                                <input class="form-control" type="text" v-model="modules.{$m['nameBlock']}.commentsTimeLimit">
                            </div>
                        </div>

                    </div>
                </div>
            </div>


            <div class="col-md-6">
                <div class="col-md-12">
                    <div class="form-horizontal">

                        <div class="form-group">
                            <label class="col-lg-4 control-label">{$ml['on']}</label>
                            <div class="col-lg-8">
                                <switch-box :value.sync="modules.{$m['nameBlock']}.on"></switch-box>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">{$ml['preModerationOn']}</label>
                            <div class="col-lg-8">
                                <switch-box :value.sync="modules.{$m['nameBlock']}.preModerationOn"></switch-box>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">{$ml['addingGuests']}</label>
                            <div class="col-lg-8">
                                <switch-box :value.sync="modules.{$m['nameBlock']}.addingGuests"></switch-box>
                            </div>
                        </div>

                    </div>
                </div>
            </div>


        </div>
    </div>
</div>
HTML;
