<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 24.01.2016
 * Time: 16:40
 */

return <<<HTML
<div class="container">
    <div class="row">

        <div class="col-md-6 col-lg-6">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-horizontal">

                        <% if(data.creatorSystem){ %>

                            <div class="form-group">
                                <label class="col-lg-3 control-label"><%= data.lang.users.protectSetting %></label>
                                <div class="col-lg-9">
                                    <switch-box id="access" :value.sync="userData.protectSetting"></switch-box>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-3 control-label"><%= data.lang.users.mainRoot %></label>
                                <div class="col-lg-9">
                                    <switch-box id="access" :value.sync="userData.mainRoot"></switch-box>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-3 control-label"><%= data.lang.users.Additional %></label>
                                <div class="col-lg-5">
                                    <select class="form-control" name="permision" v-model="userData.permision">
                                        <% data.ind.permisionList.forEach(function(el, index) {  %>
                                            <option value="<%= el.val%>"> <%= data.lang.users.permisionList[el.val] %> </option>
                                        <% }); %>
                                    </select>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <hr/>
                        <% } %>

                        <% if(data.creatorSystem || data.isPartner || data.isRoot || data.isAdminDomain){ %>
                        <div id="sys-menu">
                            <div class="btn-group">
                                <a class="btn btn-default" href="#" @click="onUseDomainMenu"><%= data.lang.users.useDomainMenu %></a>
                            </div>
                            <setting-menu :list.sync="userData.sysMenu']"></setting-menu>
                        </div>
                        <% } %>

                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

        <div class="col-md-6 col-lg-6">
            <div class="row">
                <div class="col-md-12">

                    <% if(data.creatorSystem || data.isPartner || data.isRoot || data.isAdminDomain){ %>

                    <div class="form-group">
                        <label class="col-lg-3 control-label"><%= data.lang.users.trustedDomains %></label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" v-model="searchDomain" placeholder="<%= data.lang.users.FilterByDomain%>" v-on:keyup="onSearchDomain">
                            <select class="form-control top-10" multiple size="15" v-model="userData.domains" :options="domains"></select>
                        </div>
                    </div>
                    <% } %>


                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

    </div>
</div>
HTML;
