<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 24.01.2016
 * Time: 16:39
 */

$gender = '<select class="form-control" name="gender" v-model="userData.gender" v-on:change="onChangeGender()">';
foreach($lang['users']['gender'] as $v=>$g)
    $gender .= '<option value="'.$v.'"> '.$g.' </option>';
$gender .= '</select>';

$type_face = '<select class="form-control" name="type_face" v-model="userData.type_face">';
foreach($lang['users']['type_face'] as $v=>$t)
    $type_face .='<option value="'.$v.'"> '.$t.' </option>';
$type_face .='</select>';


//todo только если включена мультиязычность
$defaultLocale = '<div class="form-group">
                    <label class="col-lg-3 control-label">Язык по умолчанию</label>
                        <div class="col-lg-5">
                            <select class="form-control" name="defaultLocale" v-model="userData.defaultLocale">';

foreach($lang['langs'] as $v=>$l)
    $defaultLocale .= '<option value="'.$v.'"> '.$l.' </option>';

$defaultLocale .= '</select></div></div>';


return <<<HTML
<div class="container">
    <div class="row">

        <div class="col-md-6 col-lg-6">
            <div class="row">
                <div class="col-md-12">

                    <div class="panel profile-panel">
                        <div class="panel-heading panel-visible">
                            <div class="panel-title"> <span class="glyphicon glyphicon-user"></span> {$lang['users']['account']}</div>
                        </div>
                        <div class="panel-body">
                            <div class="row">

                                <div class="col-md-12">
                                    <div class="form-horizontal">

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">{$lang['users']['login']}</label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control" name="login" v-model="userData.login" @keyup="onCheckLogin | debounce 150">
                                                <div class="top-10 error" id="login-overlap" v-show="isLogin" style="display: none;">{$lang['users']['login_is_not_available']}</div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">{$lang['users']['password']}</label>
                                            <div class="col-lg-9">
                                                <input type="{{pfType ? 'text' : 'password'}}" name="password" class="form-control" v-model="userData.password">

                                                <span id="generate-btns">
                                                    <span type="button" class="btn btn-default btn-xs" @click="showPass">
                                                        <i class="glyphicon glyphicon-eye-{{pfType ? 'open' : 'close'}}"></i>
                                                    </span>
                                                    <span type="button" class="btn btn-default btn-xs" @click="generatePass">
                                                        <i class="glyphicon glyphicon-random"></i>
                                                    </span>
                                                </span>

                                                <div class="top-10 error" id="pass-length"
                                                     v-show="userData.password.length > 0 && userData.password.length<7"
                                                     style="display: none;">{$lang['users']['passLength']}</div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">{$lang['users']['repassword']}</label>
                                            <div class="col-lg-9">
                                                <input type="{{pfType ? 'text' : 'password'}}" class="form-control" id="repeat-pass" v-model="userData.repassword">
                                                <div class="top-10 error"
                                                     id="overlap"
                                                     v-show="userData.repassword != userData.password"
                                                     style="display: none;">{$lang['users']['passwordMismatch']}</div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">{$lang['users']['surname']}</label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control" name="surname" v-model="userData.surname">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">{$lang['users']['name']}</label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control" name="name" v-model="userData.name">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">{$lang['users']['pantonumic']}</label>
                                            <div class="col-lg-9">
                                                <input type="text" name="pantonumic" class="form-control" v-model="userData.pantonumic">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">{$lang['users']['gender_']}</label>
                                            <div class="col-lg-5">{$gender}</div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">{$lang['users']['email']}</label>
                                            <!-- todo валидация -->
                                            <div class="col-lg-9">
                                                <input type="email" name="email" class="form-control" v-model="userData.email" @keyup="onCheckEmail | debounce 150">
                                                <div class="top-10 error"
                                                     id="email-overlap"
                                                     v-show="isEmail"
                                                     style="display: none;">{$lang['users']['email_is_not_available']}</div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">{$lang['users']['group']}</label>
                                            <div class="col-lg-9">
                                                <select multiple class="form-control" v-model="userData.groups" options="groups">
                                                    <option v-for="g in groups" :value="g._id">{{g.name}}</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">&nbsp;</label>
                                            <div class="col-lg-5">{$type_face}</div>
                                        </div>

                                        {$defaultLocale}

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">{$lang['users']['access']}</label>
                                            <div class="col-lg-9">
                                                <switch-box id="access" :value.sync="userData.access"></switch-box>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">{$lang['users']['is_ban']}</label>
                                            <div class="col-lg-9">
                                                <switch-box id="is-ban" :value.sync="userData.is_ban"></switch-box>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">{$lang['users']['enabled']}</label>
                                            <div class="col-lg-9">
                                                <switch-box id="enabled" :value.sync="userData.enabled"></switch-box>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="col-md-6 col-lg-6">
            <div class="row">
                <div class="col-md-12">

                    <div class="panel profile-panel">
                        <div class="panel-heading panel-visible">
                            <div class="panel-title">
                                <span class="glyphicons glyphicons-magic"></span>
                                {$lang['users']['avatar']}
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row">

                                <div class="col-md-12">
                                    <div class="avatar-block">
                                        <img :src="userData.avatar ? userData.avatar : defaultAvatar"/>
                                    </div>
                                </div>

                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>

                    <div class="panel profile-panel">
                        <div class="panel-heading panel-visible">
                            <div class="panel-title">
                                <span class="fa fa-money"></span>
                                {$lang['users']['balansT']}
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row">

                                <div class="col-md-12">
                                    <div class="form-horizontal">

                                        <div class="form-group">
                                            <label class="col-xs-2 control-label">{$lang['users']['balans']}</label>
                                            <div class="col-xs-3">
                                                <input type="number" class="form-control" v-model="userData.balans">
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>

                    <div class="panel profile-panel">
                        <div class="panel-heading panel-visible">
                            <div class="panel-title">
                                <span class="fa fa-bar-chart"></span>
                                {$lang['users']['rating']['title']}
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row">

                                <div class="col-md-12">
                                    <div class="form-horizontal">

                                        <div class="col-xs-6 form-group">
                                            <label class="control-label">{$lang['users']['rating']['plus']}</label>
                                            <div class="col-xs-4">
                                                <input type="number" class="form-control" v-model="userData.rating.plus">
                                            </div>
                                        </div>

                                        <div class="col-xs-6 form-group">
                                            <label class="control-label">{$lang['users']['rating']['minus']}</label>
                                            <div class="col-xs-4">
                                                <input type="number" class="form-control" v-model="userData.rating.minus">
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>

                    <div class="panel profile-panel">
                        <div class="panel-heading panel-visible">
                            <div class="panel-title">
                                <span class="glyphicons glyphicons-cogwheel"></span>
                                {$lang['users']['modulesSetting']}
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row">

                                <div class="col-md-12">
                                    <div class="form-horizontal">

                                        <div class="form-group">
                                            <label class="col-lg-3 col-xs-5 control-label">{$lang['users']['shopDiscount']}</label>
                                            <div class="col-lg-2 col-xs-3">
                                                <input type="number" class="form-control" v-model="userData.modulesSetting.shop.discount">
                                            </div>
                                            <label class="top-10">%</label>
                                        </div>


                                    </div>
                                </div>

                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>

                    <div class="panel profile-panel">
                        <div class="panel-heading panel-visible">
                            <div class="panel-title">
                                <span class="glyphicons glyphicons-wifi_alt"></span>
                                {$lang['users']['dataCommunication']}
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row">

                                <div class="col-md-12">
                                    <div class="form-horizontal">

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">{$lang['users']['phone']}</label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control" name="connect[phone]" v-model="userData.connect.phone">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">{$lang['users']['fax']}</label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control" name="connect[fax]" v-model="userData.connect.fax">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">Jabber</label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control" name="connect[jabber]" v-model="userData.connect.jabber">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">Skype</label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control" name="connect[skype]" v-model="userData.connect.skype">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">ICQ</label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control" name="connect[icq]" v-model="userData.connect.icq">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">
                                                <i class="info-ico glyphicons glyphicons-circle_question_mark" data-uk-tooltip="{pos:'right'}" title="{$lang['tooltip'][37]}"></i>
                                                {$lang['users']['vk']}
                                            </label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control" name="connect[vk]" v-model="userData.connect.vk">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">
                                                <i class="info-ico glyphicons glyphicons-circle_question_mark" data-uk-tooltip="{pos:'right'}" title="{$lang['tooltip'][37]}"></i>
                                                Facebook
                                            </label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control" name="connect[facebook]" v-model="userData.connect.facebook">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">
                                                <i class="info-ico glyphicons glyphicons-circle_question_mark" data-uk-tooltip="{pos:'right'}" title="{$lang['tooltip'][37]}"></i>
                                                Twitter
                                            </label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control" name="connect[twitter]" v-model="userData.connect.twitter">
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>
HTML;
