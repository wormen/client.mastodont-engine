<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 24.01.2016
 * Time: 16:39
 */

return <<<HTML
<div class="container">
    <div class="row">

        <div class="col-md-6 col-lg-6">
            <div class="row">
                <div class="col-md-12">

                    <div class="panel profile-panel">
                        <div class="panel-heading panel-visible">
                            <div class="panel-title"> <span class="glyphicons glyphicons-globe_af"></span> {$lang['users']['regionSetting']}</div>
                        </div>
                        <div class="panel-body">
                            <div class="row">

                                <div class="col-md-12">
                                    <div class="form-horizontal">

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">{$lang['users']['region']}</label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control" value="" name="address[region]" v-model="userData.address.region">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">{$lang['users']['district']}</label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control" value="" name="address[district]" v-model="userData.address.district">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">{$lang['users']['city']}</label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control" value="" name="address[city]" v-model="userData.address.city">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">{$lang['users']['street']}</label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control" value="" name="address[street]" v-model="userData.address.street">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">{$lang['users']['korp']}</label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control" value="" name="address[korp]" v-model="userData.address.korp">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">{$lang['users']['dom']}</label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control" value="" name="address[dom]" v-model="userData.address.dom">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">{$lang['users']['kv']}</label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control" value="" name="address[kv]" v-model="userData.address.kv">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">{$lang['users']['zip']}</label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control" value="" name="address[zip]" v-model="userData.address.zip">
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>

                    <div class="clearfix"></div>

                </div>
            </div>
        </div>

        <div class="col-md-6 col-lg-6">
            <div class="row">
                <div class="col-md-12">

                    <div class="panel profile-panel">
                        <div class="panel-heading panel-visible">
                            <div class="panel-title"> <span class="glyphicons glyphicons-vcard"></span> {$lang['users']['urLabel']}</div>
                        </div>
                        <div class="panel-body">
                            <div class="row">

                                <div class="col-md-12">
                                    <div class="form-horizontal">

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">{$lang['users']['company_name']}</label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control" value="" name="ur_data[company_name]" v-model="userData.ur_data.company_name">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">{$lang['users']['ogrn']}</label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control" value="" name="ur_data[ogrn]" v-model="userData.ur_data.ogrn">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">{$lang['users']['inn']}</label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control" value="" name="ur_data[inn]" v-model="userData.ur_data.inn">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">{$lang['users']['bik']}</label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control" value="" name="ur_data[bik]" v-model="userData.ur_data.bik">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">{$lang['users']['kpp']}</label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control" value="" name="ur_data[kpp]" v-model="userData.ur_data.kpp">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">{$lang['users']['bank_name']}</label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control" value="" name="ur_data[bank_name]" v-model="userData.ur_data.bank_name">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">{$lang['users']['rs']}</label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control" value="" name="ur_data[rs]" v-model="userData.ur_data.rs">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">{$lang['users']['krs']}</label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control" value="" name="ur_data[krs]" v-model="userData.ur_data.krs">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">{$lang['users']['u_address']}</label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control" value="" name="ur_data[u_address]" v-model="userData.ur_data.u_address">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">{$lang['users']['f_address']}</label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control" value="" name="ur_data[f_address]" v-model="userData.ur_data.f_address">
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>

                    <div class="clearfix"></div>

                </div>
            </div>
        </div>

    </div>
</div>
HTML;
