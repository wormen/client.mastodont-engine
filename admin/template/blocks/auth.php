<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 27.01.2016
 * Time: 20:26
 */

return <<<HTML
<div id="auth-block" class="uk-modal">
    <div class="uk-modal-dialog">
        <div class="spinner mini" v-show="isLoading" style="display:none;"></div>

        <div class="uk-modal-header">
            <h4>{$lang['auth']['SESSION_END']}</h4>
        </div>

       <div class="panel-body">

            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-user"></span>
                    </span>
                    <input type="text" class="form-control phone" name="login" placeholder="{$lang['auth']['login']}" v-model="user.login">
                </div>
            </div>

            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon">
                        <span class="glyphicons glyphicons-keys"></span>
                    </span>
                    <input type="password" class="form-control product" name="password" autocomplete="off" placeholder="{$lang['auth']['pass']}" v-model="user.pass">
                </div>
            </div>

        </div>

        <div class="login-alert" v-show="!isAuth" style="display: none;">
            <div class="alert alert-danger">{{alert}}</div>
        </div>

        <div class="uk-modal-footer uk-text-right">
            <button type="button" class="uk-button uk-button-primary" @keyup.enter="onSingIn" @click="onSingIn">
                {$lang['btns']['login']}
            </button>
        </div>
    </div>
</div>
HTML;
