<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 20.01.2016
 * Time: 14:44
 */

namespace Lib;

class Index{

    protected 
        $Users,
        $Session;
    
    function __construct(){
        $this->Users = new \Mastodont\Api\v1\Users();
        $this->Session = new \Core\Session();
    }

    public function CheckPage(){
        if(session_status() === 1) session_start();

        $module = isset($_GET['module']) ? $_GET['module'] : 'main';
        $section = isset($_GET['section']) ? DIRECTORY_SEPARATOR.$_GET['section'] : '';

        $page = $module.$section;

        $user = $this->Users->decodeData('admin');
        $page = $this->Session->checkAuth($user['u']) == true ? $page : 'auth';
        $file = $this->getPathPage($page);

        return [
            'page'=>$page,
            'jsPage'=>$module.(isset($_GET['section']) ? $_GET['section'] : ''),
            'path'=>$file
        ];
    }

    public function getPathPage($page){
        $file = implode(DIRECTORY_SEPARATOR, [AdminTemplate, 'pages', $page]).'.php';
        if(!file_exists($file))
            $file = $this->getPathPage('main');
        return$file;
    }
}