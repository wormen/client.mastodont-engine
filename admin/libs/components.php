<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 20.01.2016
 * Time: 15:02
 */

namespace Components;

class Index {

    protected
        $Menu;

    function __construct(){
        $this->Menu = new \Mastodont\System\Menu();
    }

    public function LeftMenu(){

        $str = '<ul class="nav sidebar-nav">';
//        $list = $this->Menu->fullList();

        //todo проверка меню пользователя

//        foreach($list as $item)
//            $str .= $this->LeftMenuItem($item);

        $str .= $this->LeftMenuItemTpl();
        $str .= '</ul>';

        $str .= '<div class="menu-loader" v-show="isLoading"></div>';

        return$str;
    }

    protected function LeftMenuItem($item){
        if(!$item['enabled']) return '';
        $sub = '';

        if(isset($item['sub'])){
            $sub .= '<ul id="'.$item['sub']['name'].'" class="nav sub-nav">';

            foreach($item['sub']['list'] as $C){
                if(!$C['enabled']) continue;
                $subC = '';

                if(isset($C['sub'])){
                    $subC = '<ul class="nav sub-nav">';

                    foreach($C['sub']['list'] as $Cc){
                        if(!$Cc['enabled']) continue;
                        $subC .= '<li><a href="/admin/'.$Cc['uri'].'"><span class="'.$Cc['ico'].'"></span>
                                    '.$GLOBALS['lang']['leftMenu'][$Cc['langTitle']].'</a></li>';
                    }

                    $subC .= '</ul>';
                }

                $sub .= '<li'
                    .(isset($C['isActive']) && $C['isActive'] ? ' class="active"' : '')
                    .(isset($C['sub']) && $C['sub'] ? ' @click="setState(\''.$item['langTitle'].'\', 2)"' : '')
                    .'>

                        <a href="'.($C['uri'] == '#' ? $C['uri'] : '/admin/'.$C['uri']).'" class="'.(isset($C['sub']) && $C['sub'] ? 'accordion-toggle' : '').(isset($C['isActive']) && $C['isActive'] ? ' menu-open' : '').'">
                            <span style="width: 15px;" class="'.$C['ico'].'"></span>
                            '.(isset($C['sub']) ? '<span class="caret"></span>' : '').'
                            '.$GLOBALS['lang']['leftMenu'][$C['langTitle']].'
                        </a>
                        '.$subC.'
                    </li>';
            }

            $sub .= '</ul>';
        }

        $str = '<li'
            .(isset($item['isActive']) && $item['isActive'] ? ' class="active"' : '')
            .(isset($item['sub']) && $item['sub'] ? ' @click="setState(\''.$item['langTitle'].'\', 1)"' : '').'>

                    <a href="'.($item['uri'] == '#' ? $item['uri'] : '/admin/'.$item['uri']).'" class="'.(isset($item['sub']) && $item['sub'] ? 'accordion-toggle' : '').(isset($item['isActive']) && $item['isActive'] ? ' menu-open' : '').'">
                        <span class="'.$item['ico'].'"></span>
                        <span class="sidebar-title">'.$GLOBALS['lang']['leftMenu'][$item['langTitle']].'</span>
                        '.(isset($item['sub']) ? '<span class="caret"></span>' : '').'
                    </a>
                    '.$sub.'
                </li>';


        return$str;
    }

    protected function LeftMenuItemTpl(){

        return <<<HTML
        
        <li v-for="item in list" v-show="list.length>0" style="display:none;">

            <a :href="getUrl(item.uri)" 
            :class="[(item.sub ? 'accordion-toggle' : ''), {'menu-open': checkState(item.langTitle, 1)}]"
             @click="setState((item.sub ? true : false), item.langTitle, 1)">
            
                <span class="{{item.ico}}"></span>
                <span class="sidebar-title">{{lang.leftMenu[item.langTitle]}}</span>
                <span class="caret" v-if="item.sub"></span>
            </a>
            
            <ul v-if="item.sub" :id="item.sub.name" class="nav sub-nav">
                
                <li v-for="item2 in item.sub.list">

                    <a :href="getUrl(item2.uri)" 
                    :class="[(item2.sub ? 'accordion-toggle' : ''), {'menu-open': checkState(item2.langTitle, 2)}]" 
                    @click="setState((item2.sub ? true : false), item2.langTitle, 2)">
                        
                        <span style="width: 15px;" :style="item2.style" class="{{item2.ico}}"></span>
                        <span class="caret" v-if="item2.sub"></span>
                        {{lang.leftMenu[item2.langTitle]}}                          
                    </a>

                    <ul v-if="item2.sub" class="nav sub-nav">
                        <li v-for="item3 in item2.sub.list">
                            <a :href="getUrl(item3.uri)">
                                <span class="{{item3.ico}}"></span>
                                {{lang.leftMenu[item3.langTitle]}}
                            </a>
                        </li>
                    </ul>
                    
                </li>
            </ul>           
        </li>
        
HTML;
    }

    public function Breams(){
        $lang = $GLOBALS['lang'];

        $str = '<div id="topbar"><ol class="breadcrumb">';

        $url = '/admin/';
        $list = [
            ['href'=>$url, 'ico'=>'glyphicon glyphicon-home'],
            ['href'=>$url, 'title'=>$lang['topbar']['']]
        ];

        if(isset($_GET['module'])){
            $url .= '?module='.$_GET['module'];
            $list[] = ['href'=>$url, 'title'=>(isset($lang['topbar'][$_GET['module']]) ? $lang['topbar'][$_GET['module']] : '') ];
        }

        if(isset($_GET['section'])){
            $url .= '&section='.$_GET['section'];
            $list[] = ['href'=>$url, 'title'=>$lang['topbar'][$_GET['section']]];
        }

        if(isset($_GET['action']))
            $list[] = ['title'=>$lang['topbar'][$_GET['action']]];

        $c = count($list)-1;
        foreach($list as $k=>$v){
            if($k==0)
                $str .= '<li><a href="'.$v['href'].'"><span class="'.$v['ico'].'"></span></a></li>';
            else{
                if($k == $c)
                    $str .= '<li class="active"> '.$v['title'].' </li>';
                else
                    $str .= '<li><a href="'.$v['href'].'"> '.$v['title'].' </a></li>';
            }
        }

        $str .= '</ol></div>';
        return$str;
    }

}