<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 20.01.2016
 * Time: 13:39
 */


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


$runCli = true;
$DS = DIRECTORY_SEPARATOR;
require_once implode($DS, [__DIR__, '..', 'config', 'constants.php']);
require implode($DS, [AdminDir, 'libs', 'index.php']);
require implode($DS, [AdminDir, 'libs', 'components.php']);

$Lib = new \Lib\Index();
$Components = new \Components\Index();

$locale = $T['locale'];
$config = $Setting->get();


try{

    // проверяем пользователя в черном списке
    $Security->CheckBanList();
    $Core->Headers();

    $p = $Lib->CheckPage();
    $pageBlock = include $p['path'];
    $page = $p['page'];



    $jsF1 = [
        '/assets/jquery.min.js',
        '/vendor/jquery-ui/jquery-ui.min.js',
    ];
    foreach($jsF1 as $k=>$j)
        $jsF1[$k] = str_replace('\\', '/', str_replace(RootDir, '', AdminTemplate.$j));

    $jsF1[] = '/fm/js/elfinder.min.js';

    if(!isset($_COOKIE['mst_locale']) || $_COOKIE['mst_locale'] == 'rus')
        $jsF1[] = '/fm/js/i18n/elfinder.ru.js';

    switch($p['page']){
        case 'list-cities':
            $p['jsPage'] = 'city';
            break;

        default:
            $p['jsPage'] = str_replace(['-'], '', $p['jsPage']);
            break;
    }

    $jsF2 = [
        '/assets/uikit/js/uikit.min.js',
        '/assets/uikit/js/core/modal.min.js',
        '/assets/uikit/js/components/notify.min.js',
        '/assets/uikit/js/components/tooltip.min.js',
        '/assets/uikit/js/components/nestable.min.js',
        '/vendor/bootstrap/js/bootstrap.min.js',

        '/js/sidebar.js',
        '/js/app.js',
        '/js/leftMenu.js',
        '/js/'.$p['jsPage'].'.js',
    ];

    foreach($jsF2 as $k=>$j)
        $jsF2[$k] = str_replace('\\', '/', str_replace(RootDir, '', AdminTemplate.$j));

    $js = $Tpl->AddHeadScript(array_merge($jsF1, $jsF2));



    $cssF = [
        '/vendor/bootstrap/css/bootstrap.min.css',
        '/css/pages.css',
        '/fonts/glyphicons_pro/glyphicons.min.css',
        '/fonts/iconsweets/style.css',
        '/fonts/icomoon/style.css',
        '/css/msp-general.css',
        '/css/msp-style.css',
        '/css/msp-components.css',
        '/css/notify.gradient.mod.min.css',
        '/css/tooltip.gradient.mod.min.css',
        '/css/uikit.gradient.mod.css',
        '/css/theme.css',
        '/css/animate.css',
        '/css/plugins.css',
        '/css/responsive.css',
        '/css/boxed.css',
        '/css/custom.css',
        '/css/fm.css',
        '/css/context.bootstrap.css',
        '/css/context.standalone.css',
        '/vendor/jquery-ui/jquery-ui.min.css',
        '/assets/uikit/css/components/nestable.gradient.min.css',
        '/css/font-awesome.min.css',
    ];
    foreach($cssF as $k=>$c)
        $cssF[$k] = str_replace('\\', '/', str_replace(RootDir, '', AdminTemplate.$c));

    $cssF = array_merge($cssF, [
        '/fm/css/elfinder.min.css',
        '/fm/themes/moono/css/theme.css',
    ]);

    $css = $Tpl->AddHeadCss($cssF);


    $head = include implode(DIRECTORY_SEPARATOR, [AdminTemplate, 'blocks', 'head.php']);
    $header = include implode(DIRECTORY_SEPARATOR, [AdminTemplate, 'blocks', 'header.php']);

    $module = isset($_GET['module']) ? ' '.$_GET['module'] : ' main';
    $section = isset($_GET['section']) ? ' '.$_GET['section'] : '';
    $copyright = $Core->Copyright();

    $bream = $Components->Breams();
    $leftMenu = $Components->LeftMenu();

    $sbDir = AdminTemplate.'/blocks/sidebar';

    $lsb = '';
    $rsb = include $sbDir.'/account-menu.php';
    $rsb .= include $sbDir.'/locale-list.php';


    if($p['page'] != 'auth')
        echo <<<HTML
    <!DOCTYPE html>
    <html lang="en">
    <head>{$head}</head>
    <body style="zoom: 1;" id="app" class="dashboard index-load">

        <div id="loader-big">
            <div class="cssload-box cssload-loading"></div>
        </div>

        <div id="app-container">
            <!-- Start: Header -->{$header}<!-- End: Header -->

            <!-- Start: Main -->
            <div id="main" class="" style="display: block;">

                <!-- Start: Sidebar -->
                <aside id="sidebar" class="refresh">{$leftMenu}</aside>
                <!-- End: Sidebar -->

                <!-- Start: Content -->
                <section id="content" class="refresh{$module}{$section}">
                    <div id="connect-info">
                        <div class="success"><%= data.lang.appOnline %></div>
                        <div class="false"><%= data.lang.appOffline %></div>
                    </div>

                    {$bream}

                    <div class="container">
                        <div class="row">

                            {$pageBlock}

                            <div class="clearfix"></div>
                            <hr class="short margin-top-none">
                            <div class="col-xs-12">
                                {$copyright['text']} / {$copyright['date']} / version: {$copyright['ver']}
                            </div>

                        </div>
                    </div>

                </section>
                <!-- End: Content -->

            </div>
            <!-- End: Main -->
        </div>

        <div id="sidebar-blocks">
            <sidebar :show.sync="sidebarLeft.show" :placement="sidebarLeft.placement" :header="sidebarLeft.title" :width="sidebarLeft.width" :extclass.sync="sidebarLeft.extclass">
                {$lsb}
            </sidebar>

            <sidebar :show.sync="sidebarRight.show" :placement="sidebarRight.placement" :header="sidebarRight.title" :width="sidebarRight.width" :extclass.sync="sidebarRight.extclass">
                {$rsb}
            </sidebar>
        </div>

        {$js}

        <div id="page-loader"><span class="glyphicon glyphicon-cog fa-spin cog-1"></span></div>
        
    </body>
    </html>
HTML;

    else
        echo $pageBlock;

}catch(Exception $e){
    $Log->set($e);
}
