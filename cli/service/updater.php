<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 08.02.2016
 * Time: 9:06
 */
exit(0);
$run = false;
if (count($argv) > 0){
    foreach ($argv as $arg)
        if($arg == 'run') $run = true;
}

if(!$run) exit(0);

//----------------------------------------------------------------------------------------------------------------------

$pid = getmypid();
$title = 'Mastodont Cli Manager - pid:'.$pid;

if (!cli_set_process_title($title)) {
    echo "Unable to set process title for PID $pid...\n";
    exit(1);
}

//----------------------------------------------------------------------------------------------------------------------

use \Core\Log as Log;
use \Cli\Service as S;

$runCli = false;
$DS = DIRECTORY_SEPARATOR;
require_once __DIR__.'/../../config/constants.php';

$service = 'updater';
$lifeTimeFile = CliDir . "/config/$service.lifetime";
$jobFile = CliDir . "/config/$service.next";
$PIDfile = PidDir . "/$service.pid";
$stopFile = CliDir."/state/$service.stop";
$restartFile = CliDir."/state/$service.restart";

exit(0);