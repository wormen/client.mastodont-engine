<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 15.02.2016
 * Time: 9:09
 */

$run = false;
if (count($argv) > 0){
    foreach ($argv as $arg)
        if($arg == 'run') $run = true;
}

if(!$run) exit(0);

//----------------------------------------------------------------------------------------------------------------------

$pid = getmypid();
$title = 'Mastodont Cli Manager - pid:'.$pid;

if (!cli_set_process_title($title)) {
    echo "Unable to set process title for PID $pid...\n";
    exit(1);
}

//----------------------------------------------------------------------------------------------------------------------
try{

$runCli = false;
$DS = DIRECTORY_SEPARATOR;
require_once __DIR__.'/../../config/constants.php';

$Service = new \Cli\Service();
$Queue = new \Core\Queue();
$Email = new \Core\Email();


$service = 'email';

    echo "get service data\n";
    $S = $Service->getService($service);

    // проверяем состояние
    if($S['state'] == 'restart') $S['state'] = 'run';

    echo "check PID: {$S['pid']}\n";
    $ps = $S['pid']>0 && $Service->isRuning($S['pid']) ? 'ok' : 'no';
    echo "is run PID --> {$ps}\n";

    if($ps == 'ok' || $S['state'] == 'stop') exit(0);

    // обновляем данные сервиса после старта
    $S['pid'] = $pid;

    echo "update service data\n";
    $Service->update($S['_id'], $S);

    for ($i = 0; ; $i++){
        $S = $Service->getService($service); // получаем данные повторно, т.к. они могут измениться из админки
        if($S['state'] == 'stop' || $S['state'] == 'restart') exit(0);  // проверяем состояние

        // получаем список из очереди
        echo "get list emails\n";
        foreach($Queue->getList($service, ['status'=>'added']) as $L){

            $vars = $L['data']['tpl']['vars'];

            // отправляем Email
            echo "sending email ID: {$L['_id']}\n";
            $E = $Email->Send($vars['email'], $vars['userEmail'], $L['data']['tpl']['subject'], $L['data']['text']);

            // удаляем из очереди
            if($E == null){
                echo "send email ID: {$L['_id']} --> ok\n";
                $Queue->delete($L['_id'], $service);
            }

            // письмо не было отправлено, меняем статус в очереди
            else{
                echo "send email ID: {$L['_id']} --> fail\n";
                $L['status'] = 'fail';
                $Queue->update($service, $L['_id'], $L);
            }

        }

//        S::Update($S['_id'], $S);
        gc_collect_cycles(); // собираем мусор

        // следующий цикл через (по умолчанию 10 сек)
        sleep(isset($S['config']['interval']) ? $S['config']['interval'] : 3);
    }

}catch(Exception $e){
    $Log->set($e, 'email-service');
}
