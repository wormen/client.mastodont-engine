<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 02.02.2016
 * Time: 19:36
 */
exit(0);
$run = false;
if (count($argv) > 0){
    foreach ($argv as $arg)
        if($arg == 'run') $run = true;
}

if(!$run) exit(0);

//----------------------------------------------------------------------------------------------------------------------

$pid = getmypid();
$title = 'Mastodont Cli Manager - pid:'.$pid;

if (!cli_set_process_title($title)) {
    echo "Unable to set process title for PID $pid...\n";
    exit(1);
}

//----------------------------------------------------------------------------------------------------------------------
try{

$runCli = false;
$DS = DIRECTORY_SEPARATOR;
require_once __DIR__.'/../../config/constants.php';

$Service = new \Cli\Service();
$Queue = new \Core\Queue();

$service = 'backup';

$next = \Core\Time::Days(1); // раз в сутки
$lifeTimeFile = CliDir . "/config/$service.lifetime";
$jobFile = CliDir . "/config/$service.next";
$PIDfile = PidDir . "/$service.pid";
$stopFile = CliDir."/state/$service.stop";
$restartFile = CliDir."/state/$service.restart";

exit(0);



$config = [

];

    // проверка меток
    if(file_exists($restartFile)) unlink($restartFile);

    // проверка, запущен ли у нас менеджер
    if($Service->isRuning( (file_get_contents($PIDfile)) || file_exists($stopFile)) ) exit(0);

    // записываем свой PID
    file_put_contents($PIDfile, $pid);

    for ($i = 0; ; $i++) {
        if (file_exists($restartFile) || file_exists($stopFile)) exit(0); // маркер остановки | рестарта

        $time = 0;
        if(file_exists($jobFile))
            $time = file_get_contents($jobFile);


        // проверяем время следующего запуска
        if($time <= time()){



//            $saveDir = BackupDir;
//            $rd = RootDir;
//            $cmd = "tar -zvcpf $saveDir/backup-$time.tar.gz --directory $saveDir --exclude=$saveDir --exclude=$rd/0 $rd";
//            echo$cmd;
//
////            exec($cmd);
//            pclose(popen($cmd, 'w'));

            file_put_contents($jobFile, time()+$next);
        }

        // фиксируем свой lifetime, на основе которого принимаем решение, жив процесс или нет, это как доп. параметра для PID
        file_put_contents($lifeTimeFile, time());
        sleep(\Core\Time::Minute(30));
    }

}catch(Exception $e){
    $Log->set($e, 'backup');
}