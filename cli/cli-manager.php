<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 03.02.2016
 * Time: 9:07
 */

$run = false;
if (count($argv) > 0){
    foreach ($argv as $arg)
        if($arg == 'run') $run = true;
}

if(!$run) exit(0);

//----------------------------------------------------------------------------------------------------------------------

$pid = getmypid();
$title = 'Mastodont Cli Manager - pid:'.$pid;

if (!cli_set_process_title($title)) {
    echo "Unable to set process title for PID $pid...\n";
    exit(1);
}

//----------------------------------------------------------------------------------------------------------------------
try{

$runCli = false;
$DS = DIRECTORY_SEPARATOR;
require_once __DIR__.'/../config/constants.php';

$Cache = new \Core\Cache();
$Service = new \Cli\Service();

$service = 'manager';
$lifeTimeFile = CliDir . "/config/$service.lifetime";
$PIDfile = PidDir . "/$service.pid";
$stopFile = CliDir . "/state/$service.stop";
$restartFile = CliDir . "/state/$service.restart";
$licState = CliDir . "/config/lic-check.lifetime";

    // проверка меток
    echo "check restart-file\n";
    if(file_exists($restartFile)) unlink($restartFile);

    // проверка, запущен ли у нас менеджер
    echo "check pid and stop-file\n";
    if($Service->isRuning(file_get_contents($PIDfile)) || file_exists($stopFile)) exit(0);

    // записываем свой PID
    echo "update pid\n";
    file_put_contents($PIDfile, $pid);

    $checkLic = 0; // следующее время проверки лицензии
    if(file_exists($licState))
        $checkLic = (int)file_get_contents($licState);

    echo "init --->\n";

    for ($i = 0; ; $i++){
        echo "check restart-file or stop-file\n";
        if(file_exists($restartFile) || file_exists($stopFile)) exit(0); // маркер остановки | рестарта

        // проверяем состояние сервисов
        echo "get full list services\n";
        foreach($Service->fullList() as $s){
            try{
                $sf = ServiceDir . '/' . $s['name'] . '.php';

                echo "check service: {$s['name']}\n";

                $es = file_exists($sf) ? 'ok' : 'fail';
                echo "file service exist: {$es}\n";

                if($s['state'] == 'run'){
                    if($s['type'] == 'demon'){
                        echo "--> Run service: {$s['name']}\n";
                        $Service->RunAsDeamon($sf);
                    }

                    //            if($s['type'] == 'server')
//                S::RunAsServer($sf);
                }

            }catch(Exception $e){
                echo "list services error --->\n";
                var_dump($e);
                echo "<--- list services error\n";
                $Log->set($e, 'cli-manager');
            }

        }

        // удаляем старый кэш
        echo "clear old cache\n";
        $Cache->delete(true);

        // проверка лицензии, раз в час
        if($checkLic <= time()){
            echo "check license\n";
            file_put_contents($licState, time()+3600);

            //todo отправляем запрос для проверки состояния лицензии
        }

        // фиксируем свой lifetime, на основе которого принимаем решение, жив процесс или нет, это как доп. параметра для PID
        file_put_contents($lifeTimeFile, time());

        echo "clear garbage\n";
        gc_collect_cycles();
        sleep(15);
    }


}catch(Exception $e){
    echo "error --->\n";
    var_dump($e);
    echo "<--- error\n";
    $Log->set($e, 'cli-manager');
}

//----------------------------------------------------------------------------------------------------------------------

