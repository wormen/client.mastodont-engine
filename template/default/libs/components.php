<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 09.02.2016
 * Time: 16:18
 */

class Components{

    public function AddAdsBtn(){
        $account = $GLOBALS['T']['vars']['modules']['account'];
        $url = '#';

        $classes = '';
        if($GLOBALS['T']['auth']){
            $url = "/{$account['linkModule']}/{$account['links']['createAd']}";
            $classes .= 'auth ';
        }
        $classes .= 'login menu-item menu-item-type-post_type menu-item-object-page make-a-advertisement';

        return '<li id="add-advertisement" class="'.$classes.'">
                    <a href="'.$url.'">
                    <i class="icon-plus"></i>Добавить объявление</a>
                </li>';
    }

    public function GeoBlock(){
        $G = \Mastodont\Api\v1\Geo::getData();
        $text = 'Вся Россия';

        if($G){
            if($G->isCountry || $G->allCountry)
                $text = 'Вся Россия';

            if($G->isDistrict)
                $text = $G->districtName;

            if($G->isRegion){
                $text = $G->regionName;
            }

            if($G->isCity)
                $text = $G->cityName;
        }


        return '<div id="choose-city-in" class="choose-city-in top-location">
                <a href="#"><i class="icon-location"> </i>'.$text.'</a>
            </div>';
    }

    public function Infographic(){
        return '<div class="infograph">
                        <span class="h2 caps strong">Инфографика</span>
                    </div>';
    }

    public function Breadcrums($title, $isMain = false){
        \Core\Tpl::setTitle($title, $isMain);

        $list = ['<a href="/">Главная</a>'];

        $list = implode('<span class="delimiter">/</span>', $list);
        return '<div class="container">
                    <div class="row">
                        <div class="col-md-12">'.$list.'</div>
                    </div>
                </div>';
    }

    public function LkBreadcrums($title, $isMain = false){
        \Core\Tpl::setTitle($title);

        $list = ['<a href="/">Главная</a>'];
        if($isMain)
            $list[] = '<span>Личный кабинет</span>';
        else{
            $list[] = '<a href="/account">Личный кабинет</a>';
            $list[] = '<span>'.$title.'</span>';
        }

        $list = implode('<span class="delimiter">/</span>', $list);

        return '<div class="container">
                    <div class="row">
                        <div class="col-md-12">'.$list.'</div>
                    </div>
                </div>';
    }

    public function HeadLkMenu($list){
        $items = '';
        foreach($list as $l){
            $b = '';
            if(isset($l['isMsgCounter']))
                $b = '<span v-el:msgCount>0</span>';


            $items .= '<li class="'.$l['class'].'">
                            <a href="'.$l['href'].'"><i class="'.$l['ico'].'"></i>'.$l['title'].$b.'</a>
                        </li>';
        }

        return '<div id="accordion-user" class="accordion-section-content" rel="auth">
                    <ul>
                    '.$items.'
                    <li>
                        <a href="javascript:void(0);" @click="onLogOut"><i class="fa fa-sign-out"></i>Выйти</a>
                    </li>
                    </ul>
                </div>';
    }

    public function LkLeftMenu($list){

        $items = '';
        foreach($list as $l){
            $b = '';
            if(isset($l['isMsgCounter']))
                $b = '<span v-el:msgCount>0</span>';


            $items .= '<li class="'.$l['class'].'">
                            <a href="'.$l['href'].'"><i class="'.$l['ico'].'"></i>'.$l['title'].$b.'</a>
                        </li>';
        }

        return '<div class="nav-buttons-inner">
                    <nav class="nav-buttons">
                        <ul>'.$items.'</ul>
                    </nav>
                </div>';
    }

    public function LkUserInfo($account){
        global$T;

        return '<div class="user-login">
                        <span><i class="fa fa-user"></i>'.(isset($T['user']['login']) ? $T['user']['login'] : '').'</span>
                        <div class="user-wallet">
                            <span>Баланс: '.(isset($T['user']['balans']) ? $T['user']['balans'] : 0).'<i class="fa fa-rub"></i></span>
                            <a href="'.$account.'/pay-service" class="get-money">Пополнить счет</a>
                        </div>
                    </div>';
    }


}