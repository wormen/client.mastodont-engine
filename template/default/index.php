<?php
die();

global$tpl;

/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 20.01.2016
 * Time: 11:52
 */

/**
 *  $T - массив с основными параметрами шаблона
 *      vars - основной конфиг сайта
 *      page - текущий шаблон страницы
 *      url - текущий url
 *
 */

require_once __DIR__.'/libs/components.php';
$Component = new Components();

$blocks = $tpl['path'] . DIRECTORY_SEPARATOR . 'blocks';

$head = include $blocks.'/head.php';
$header = include $blocks.'/header.php';

$pluso = include $blocks.'/pluso.php';

$footer = include $blocks.'/footer.php';
$modals = include implode(DIRECTORY_SEPARATOR, [$tpl['path'], 'modals', 'index.php']);

$accJS = $rel = '';
if($T['module']['name'] == 'account'){
    $rel = 'rel="account"';
    $accJS = "<script type='text/javascript' src='/addons/js/account.js'></script>";
}

//$T['vars']['defis']
//var_dump($T);
//var_dump($tpl);
//var_dump($req->getBasePath());


return <<<HTML
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" lang="en-US">
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" lang="en-US">
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html lang="en-US">
<!--<![endif]-->

{$head}

<body class="home page {$T['module']['name']}">
    <div id="page" class="hfeed site">
        {$header}

        <!-- #masthead -->
        <div {$rel}>
            {$T['pageContent']}
        </div>
        <!-- #main -->

        {$footer}
        <!-- #colophon -->
    </div>

    <!-- #page -->

    {$modals['login']}
    {$modals['advertisement']}
    {$modals['city']}

    <!-- <script type='text/javascript' src='/addons/vendor/modernizr.js'></script> -->

    <script type='text/javascript' src='{$tpl['url']}/js/jquery-1.11.3.min.js'></script>
    <script type='text/javascript' src='{$tpl['url']}/js/jquery/jquery-migrate.min.js'></script>

    <script type='text/javascript' src='{$tpl['url']}/js/revslider/rs-plugin/js/jquery.themepunch.tools.min.js'></script>
    <script type='text/javascript' src='{$tpl['url']}/js/revslider/rs-plugin/js/jquery.themepunch.revolution.min.js'></script>
    <script type='text/javascript' src='{$tpl['url']}/js/jobify.min.js'></script>
    <script type='text/javascript' src='{$tpl['url']}/js/accordion.js'></script>

    <script type='text/javascript' src='/addons/js/core.js'></script>
    <script type='text/javascript' src='/addons/js/auth.js'></script>
    <script type='text/javascript' src='/addons/js/geo.js'></script>

    {$accJS}

    <script type='text/javascript' src='{$tpl['url']}/js/custom.js'></script>

    <script type='text/javascript'>
    /* <![CDATA[ */
    var jobifySettings = {
            "i18n":[],
            "pages":{
                "is_widget_home":true,
                "is_job":false,
                "is_resume":false,
                "is_testimonials":false
            },
            "widgets":{
                "jobify_widget_slider_generic":{"animate":0},
                "jobify_widget_jobs":{"animate":0},
                "jobify_widget_stats":{"animate":1},
                "jobify_widget_companies":{"animate":0},
                "jobify_widget_testimonials":{"animate":1},
                "jobify_widget_video":{"animate":1},
                "jobify_widget_price_table_wc":{"animate":0},
                "jobify_widget_callout":{"animate":0},
                "jobify_widget_blog_posts":{"animate":0}
            }
        };
    /* ]]> */
    </script>

    <script>
    $('a[href^="#"], a[href^="."]').click(function() {
        var scroll_el = $(this).attr('href');
        if ($(scroll_el).length != 0) {
            $('html, body').animate({
                scrollTop: $(scroll_el).offset().top
            }, 500);
        }
        return false;
    });
    </script>



</html>
<!-- Localized -->
HTML;

