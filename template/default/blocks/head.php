<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 20.01.2016
 * Time: 11:51
 */

$title = \Core\Tpl::getTitle();
$meta_desctiption = \Core\Tpl::getMeta('desctiption');

return <<<HTML

<title>{$title}</title>
{$meta_desctiption}

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width" />
<meta name="viewport" content="initial-scale=1">

<!--[if lt IE 9]>
<script src="{$tpl['url']}/html5.js" type="text/javascript"></script>
<![endif]-->

<link href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Ubuntu:400,300,300italic,400italic,500,500italic,700,700italic&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

<link rel='stylesheet' href='{$tpl['url']}/css/style.css' type='text/css' media='all' />
<link rel='stylesheet' href='{$tpl['url']}/css/blue.css' type='text/css' media='all' />
<link rel='stylesheet' href='{$tpl['url']}/css/responsive.css' type='text/css' media='all' />

<link rel='stylesheet' href='{$tpl['url']}/css/custom.css' type='text/css' media='all' />
HTML;
