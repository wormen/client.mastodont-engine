<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 09.02.2016
 * Time: 16:18
 */

$menu = \Mastodont\Api\v1\Menu::getID('56a8eef041fe0doc24165');
$menu = array_merge([
    [
        'title'=>'Главная',
        'url'=>'/',
    ]
], $menu);

$lt = '';
foreach($menu as $m)
    $lt .= '<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="'.$m['url'].'">'.$m['title'].'</a></li>';

return <<<HTML
<nav class="site-primary-navigation slide-left">
    <a href="#" class="primary-menu-toggle"><i class="icon-cancel-circled"></i> <span>Close</span></a>
    <div class="menu-main-menu-container">
        <ul id="menu-main-menu" class="nav-menu-primary">
            {$lt}
            {$Component->AddAdsBtn()}
        </ul>
    </div>
</nav>
HTML;
