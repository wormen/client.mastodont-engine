<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 20.01.2016
 * Time: 11:51
 */

$top = include __DIR__ .'/../pages/account/blocks/top.php';
$menu = include 'topmenu.php';

require_once __DIR__.'/../libs/components.php';
$Component = new Components();

$userMenu = $Component->GeoBlock();

if($T['auth']){
    $balans = 0;
    $login = isset($T['user']['login']) ? $T['user']['login'] : '';
//todo подключаем скрипт аккаунта

    $userMenu .= <<<HTML

<div class="user-wallet">
    <a href="{$top['pay']}" class="user menu-item menu-item-type-post_type menu-item-object-page">{$balans}<i class="fa fa-rub"></i></a>
</div>

<div class="user-messages">
    <a href="">
        <i class="fa fa-comment"></i>
        <span v-el:msgCount>0</span>
    </a>
</div>

<div class="active-user accordion">
    <div class="accordion-section">

        <a class="accordion-section-title user menu-item menu-item-type-post_type menu-item-object-page" href="#accordion-user">
            <i class="fa fa-user"> </i>
            {$login}
        </a>

        {$top['headMenu']}

    </div>
</div>

HTML;

}else{
    $userMenu .= '<div id="login-modal" class="top-login">
                <a href="#" class="login menu-item menu-item-type-post_type menu-item-object-page">
                    <i class="icon-login"></i>Войти
                </a>
            </div>';
}


return <<<HTML
<header id="masthead" class="site-header" role="banner">

    <div class="top-header">
        <div class="container">
            <div class="logo">
                <a href="/">
                    <img src="{$tpl['url']}/images/logo.png" alt="" />
                </a>
            </div>

            <div class="top-search">
                <form role="search" method="get" id="searchform" action="/search">
                    <input type="text" name="q" placeholder="Найти услугу..." />
                    <button type="submit" id="searchsubmit" class="top-search-button"><i class="icon-search"></i></button>
                </form>
            </div>

            {$userMenu}
        </div>
    </div>

    <div class="nav-header">
        <div class="container">
            {$menu}
            <a href="#" class="primary-menu-toggle in-header"><i class="icon-menu"></i></a>
        </div>
    </div>

</header>
HTML;
