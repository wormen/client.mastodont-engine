<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 20.01.2016
 * Time: 11:51
 */

$copy = (date('Y') == 2016 ? 2016 : '2016 - '.date('Y')).' ServiceCab ';
$menu = include 'footmenu.php';
$pluso = include 'pluso.php';

$blocks = [
    'left'=>\Mastodont\Api\v1\Menu::getID('56b997764d29fdoc11821'),
    'right'=>\Mastodont\Api\v1\Menu::getID('56b9977ccfdc7doc3938'),
];

$leftBlock = '';
foreach($blocks['left'] as $l)
    $leftBlock .= '<li class="page_item"><a href="'.$l['url'].'">'.$l['title'].'</a></li>';

$rightBlock = '';
foreach($blocks['right'] as $r)
    $rightBlock .= '<li class="page_item"><a href="'.$r['url'].'">'.$r['title'].'</a></li>';


return <<<HTML
<div class="footer-cta duplicate-menu">
    <div class="container">
        {$menu}
    </div>
</div>

<footer id="colophon" class="site-footer" role="contentinfo">

    <div class="footer-widgets">
        <div class="container">
            <div class="row">

                <aside class="footer-widget widget_recent_entries col-md-4 col-sm-6 col-xs-12">
                    <ul>{$leftBlock}</ul>
                </aside>

                <aside class="footer-widget widget_recent_entries col-md-4 col-sm-6 col-xs-12">
                    <ul>{$rightBlock}</ul>
                </aside>

                <aside class="footer-widget widget_text pluso-widget col-md-4 col-sm-6 col-xs-12">{$pluso}</aside>
            </div>
        </div>
    </div>

    <div class="copyright">
        <div class="container">

            <div class="site-info">
                 &copy; {$copy} |
                 <span class="powered">Powered by <a href="//x-tiger.ru" target="_blank">X- Tiger</a></span>
            </div>

            <!-- .site-info -->
            <a href="#masthead" class="btt"><i class="icon-up-circled"></i></a>
            <div class="footer-social"><a href=""><span class="screen-reader-text">facebook</span></a>
                <a href=""><span class="screen-reader-text">twitter</span></a>
                <a href=""><span class="screen-reader-text">google</span></a>
                <a href=""><span class="screen-reader-text">instagram</span></a>
                <a href=""><span class="screen-reader-text">linkedin</span></a>
            </div>

        </div>
    </div>
</footer>
HTML;
