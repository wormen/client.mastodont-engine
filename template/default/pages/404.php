<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 22.01.2016
 * Time: 11:54
 */

return <<<HTML
<div id="main" class="site-main">
    <div id="primary" class="content-area">
        <div id="content" class="homepage-content" role="main">
            <section class="breadcrums">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <a href="">Главная</a>
                            <span class="delimiter">/</span>
                            <span>Страница не найдена</span>
                        </div>
                    </div>
                </div>
            </section>
            <section id="jobify_widget_jobs-3" class="homepage-widget jobify_widget_jobs">
                <div class="container">
                   <div class="error-inner">
                       <div class="num-of-error">
                           404
                       </div>
                       <div class="descr-of-error">
                           <p>Товарищи! постоянный количественный рост и сфера нашей активности представляет собой интересный эксперимент проверки дальнейших направлений развития.</p>
                           <p>Пройдите по <a href="">ссылке</a></p>
                       </div>
                   </div>
                </div>
            </section>
        </div>
        <!-- #content -->
    </div>
    <!-- #primary -->
</div>
HTML;
