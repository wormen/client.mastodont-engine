<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 20.01.2016
 * Time: 9:27
 *
 * главная страница сайта
 */

//var_dump($T);
\Core\Tpl::setTitle(null, true);

require_once __DIR__.'/../libs/components.php';
$Component = new Components();

return <<<HTML
<div id="main" class="site-main">
    <div id="primary" class="content-area">
        <div id="content" class="homepage-content" role="main">
            <section id="jobify_widget_jobs-3" class="homepage-widget jobify_widget_jobs">
                <div class="container">

                    <div class="row">
                        <div class="infograph-inner col-md-12">
                            <div class="h2 caps block-title">
                                Уникальный сервис для поиска любых видов услуг
                            </div>

                            {$Component->Infographic()}

                        </div>
                    </div>

                    <div class="row">
                        <div class="recent-jobs  has-spotlight col-lg-8 col-md-7 col-sm-12">
                            <div class="choose-city">
                                <span class="h6">Выберите ваш город: </span><a href=""><i class="icon-location"> </i>Владивосток</a> <a href="" class="show-new-ads">Показать новые объявления</a>
                            </div>
                            <div class="row">
                                <div class="home-category-inner col-xs-12 col-sm-6 col-md-6">
                                    <div class="home-category">
                                        <div class="h4 category-title">
                                            <i class="fa fa-birthday-cake"></i> Организация праздников
                                        </div>
                                        <div class="subcategories">
                                            <ul>
                                                <li>
                                                    <a href="">Аренда оборудования <span class="amount-ads">(14)</span></a>
                                                </li>
                                                <li>
                                                    <a href="">Артисты и шоу-программы <span class="amount-ads">(23)</span></a>
                                                </li>
                                                <li>
                                                    <a href="">Ведущие, конферансье, координаторы <span class="amount-ads">(14)</span></a>
                                                </li>
                                                <li>
                                                    <a href="">Декор и оформление мероприятий <span class="amount-ads">(118)</span></a>
                                                </li>
                                                <li>
                                                    <a href="">Кейтеринг <span class="amount-ads">(44)</span></a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="more-info">
                                            <a href="">Все подкатегории...</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="home-category-inner col-xs-12 col-sm-6 col-md-6">
                                    <div class="home-category">
                                        <div class="h4 category-title">
                                            <i class="fa fa-car"></i> Авто и мото
                                        </div>
                                        <div class="subcategories">
                                            <ul>
                                                <li>
                                                    <a href="">GT и тюнинг <span class="amount-ads">(64)</span></a>
                                                </li>
                                                <li>
                                                    <a href="">Автозвук <span class="amount-ads">(29)</span></a>
                                                </li>
                                                <li>
                                                    <a href="">Доставка авто и запчастей  <span class="amount-ads">(109)</span></a>
                                                </li>
                                                <li>
                                                    <a href="">Замена тех. жидкостей <span class="amount-ads">(35)</span></a>
                                                </li>
                                                <li>
                                                    <a href="">Оформление документов <span class="amount-ads">(80)</span></a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="more-info">
                                            <a href="">Все подкатегории...</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="home-category-inner col-xs-12 col-sm-6 col-md-6">
                                    <div class="home-category">
                                        <div class="h4 category-title">
                                            <i class="fa fa-bed"></i> Мебель
                                        </div>
                                        <div class="subcategories">
                                            <ul>
                                                <li>
                                                    <a href="">Доставка мебели <span class="amount-ads">(122)</span></a>
                                                </li>
                                                <li>
                                                    <a href="">Изготовление мебели <span class="amount-ads">(34)</span></a>
                                                </li>
                                                <li>
                                                    <a href="">Изготовление стекол, зеркал <span class="amount-ads">(20)</span></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="home-category-inner col-xs-12 col-sm-6 col-md-6">
                                    <div class="home-category">
                                        <div class="h4 category-title">
                                            <i class="fa fa-paw"></i> Животные
                                        </div>
                                        <div class="subcategories">
                                            <ul>
                                                <li>
                                                    <a href="">Гостиницы, передержка животных <span class="amount-ads">(67)</span></a>
                                                </li>
                                                <li>
                                                    <a href="">Доставка, перевозка животных <span class="amount-ads">(93)</span></a>
                                                </li>
                                                <li>
                                                    <a href="">Дрессировка, хендлинг <span class="amount-ads">(8)</span></a>
                                                </li>
                                                <li>
                                                    <a href="">Изготовление клеток, домиков, аквариумов <span class="amount-ads">(13)</span></a>
                                                </li>
                                                <li>
                                                    <a href="">Услуги зооспециалистов <span class="amount-ads">(33)</span></a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="more-info">
                                            <a href="">Все подкатегории...</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="home-category-inner col-xs-12 col-sm-6 col-md-6">
                                    <div class="home-category">
                                        <div class="h4 category-title">
                                            <i class="fa fa-birthday-cake"></i> Организация праздников
                                        </div>
                                        <div class="subcategories">
                                            <ul>
                                                <li>
                                                    <a href="">Аренда оборудования <span class="amount-ads">(14)</span></a>
                                                </li>
                                                <li>
                                                    <a href="">Артисты и шоу-программы <span class="amount-ads">(23)</span></a>
                                                </li>
                                                <li>
                                                    <a href="">Ведущие, конферансье, координаторы <span class="amount-ads">(14)</span></a>
                                                </li>
                                                <li>
                                                    <a href="">Декор и оформление мероприятий <span class="amount-ads">(118)</span></a>
                                                </li>
                                                <li>
                                                    <a href="">Кейтеринг <span class="amount-ads">(44)</span></a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="more-info">
                                            <a href="">Все подкатегории...</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="home-category-inner col-xs-12 col-sm-6 col-md-6">
                                    <div class="home-category">
                                        <div class="h4 category-title">
                                            <i class="fa fa-car"></i> Авто и мото
                                        </div>
                                        <div class="subcategories">
                                            <ul>
                                                <li>
                                                    <a href="">GT и тюнинг <span class="amount-ads">(64)</span></a>
                                                </li>
                                                <li>
                                                    <a href="">Автозвук <span class="amount-ads">(29)</span></a>
                                                </li>
                                                <li>
                                                    <a href="">Доставка авто и запчастей  <span class="amount-ads">(109)</span></a>
                                                </li>
                                                <li>
                                                    <a href="">Замена тех. жидкостей <span class="amount-ads">(35)</span></a>
                                                </li>
                                                <li>
                                                    <a href="">Оформление документов <span class="amount-ads">(80)</span></a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="more-info">
                                            <a href="">Все подкатегории...</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="home-category-inner col-xs-12 col-sm-6 col-md-6">
                                    <div class="home-category">
                                        <div class="h4 category-title">
                                            <i class="fa fa-bed"></i> Мебель
                                        </div>
                                        <div class="subcategories">
                                            <ul>
                                                <li>
                                                    <a href="">Доставка мебели <span class="amount-ads">(122)</span></a>
                                                </li>
                                                <li>
                                                    <a href="">Изготовление мебели <span class="amount-ads">(34)</span></a>
                                                </li>
                                                <li>
                                                    <a href="">Изготовление стекол, зеркал <span class="amount-ads">(20)</span></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="home-category-inner col-xs-12 col-sm-6 col-md-6">
                                    <div class="home-category">
                                        <div class="h4 category-title">
                                            <i class="fa fa-paw"></i> Животные
                                        </div>
                                        <div class="subcategories">
                                            <ul>
                                                <li>
                                                    <a href="">Гостиницы, передержка животных <span class="amount-ads">(67)</span></a>
                                                </li>
                                                <li>
                                                    <a href="">Доставка, перевозка животных <span class="amount-ads">(93)</span></a>
                                                </li>
                                                <li>
                                                    <a href="">Дрессировка, хендлинг <span class="amount-ads">(8)</span></a>
                                                </li>
                                                <li>
                                                    <a href="">Изготовление клеток, домиков, аквариумов <span class="amount-ads">(13)</span></a>
                                                </li>
                                                <li>
                                                    <a href="">Услуги зооспециалистов <span class="amount-ads">(33)</span></a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="more-info">
                                            <a href="">Все подкатегории...</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="home-category-inner col-xs-12 col-sm-6 col-md-6">
                                    <div class="home-category">
                                        <div class="h4 category-title">
                                            <i class="fa fa-birthday-cake"></i> Организация праздников
                                        </div>
                                        <div class="subcategories">
                                            <ul>
                                                <li>
                                                    <a href="">Аренда оборудования <span class="amount-ads">(14)</span></a>
                                                </li>
                                                <li>
                                                    <a href="">Артисты и шоу-программы <span class="amount-ads">(23)</span></a>
                                                </li>
                                                <li>
                                                    <a href="">Ведущие, конферансье, координаторы <span class="amount-ads">(14)</span></a>
                                                </li>
                                                <li>
                                                    <a href="">Декор и оформление мероприятий <span class="amount-ads">(118)</span></a>
                                                </li>
                                                <li>
                                                    <a href="">Кейтеринг <span class="amount-ads">(44)</span></a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="more-info">
                                            <a href="">Все подкатегории...</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="home-category-inner col-xs-12 col-sm-6 col-md-6">
                                    <div class="home-category">
                                        <div class="h4 category-title">
                                            <i class="fa fa-car"></i> Авто и мото
                                        </div>
                                        <div class="subcategories">
                                            <ul>
                                                <li>
                                                    <a href="">GT и тюнинг <span class="amount-ads">(64)</span></a>
                                                </li>
                                                <li>
                                                    <a href="">Автозвук <span class="amount-ads">(29)</span></a>
                                                </li>
                                                <li>
                                                    <a href="">Доставка авто и запчастей  <span class="amount-ads">(109)</span></a>
                                                </li>
                                                <li>
                                                    <a href="">Замена тех. жидкостей <span class="amount-ads">(35)</span></a>
                                                </li>
                                                <li>
                                                    <a href="">Оформление документов <span class="amount-ads">(80)</span></a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="more-info">
                                            <a href="">Все подкатегории...</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="home-category-inner col-xs-12 col-sm-6 col-md-6">
                                    <div class="home-category">
                                        <div class="h4 category-title">
                                            <i class="fa fa-bed"></i> Мебель
                                        </div>
                                        <div class="subcategories">
                                            <ul>
                                                <li>
                                                    <a href="">Доставка мебели <span class="amount-ads">(122)</span></a>
                                                </li>
                                                <li>
                                                    <a href="">Изготовление мебели <span class="amount-ads">(34)</span></a>
                                                </li>
                                                <li>
                                                    <a href="">Изготовление стекол, зеркал <span class="amount-ads">(20)</span></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="home-category-inner col-xs-12 col-sm-6 col-md-6">
                                    <div class="home-category">
                                        <div class="h4 category-title">
                                            <i class="fa fa-paw"></i> Животные
                                        </div>
                                        <div class="subcategories">
                                            <ul>
                                                <li>
                                                    <a href="">Гостиницы, передержка животных <span class="amount-ads">(67)</span></a>
                                                </li>
                                                <li>
                                                    <a href="">Доставка, перевозка животных <span class="amount-ads">(93)</span></a>
                                                </li>
                                                <li>
                                                    <a href="">Дрессировка, хендлинг <span class="amount-ads">(8)</span></a>
                                                </li>
                                                <li>
                                                    <a href="">Изготовление клеток, домиков, аквариумов <span class="amount-ads">(13)</span></a>
                                                </li>
                                                <li>
                                                    <a href="">Услуги зооспециалистов <span class="amount-ads">(33)</span></a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="more-info">
                                            <a href="">Все подкатегории...</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="commercial-banner">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <img src="{$tpl['url']}/images/banner2.png">
                                    </div>
                                </div>
                            </div>
                            <section id="jobify_widget_stats-2" class="homepage-widget jobify_widget_stats">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <article>
                                            <header>
                                                <div class="h3 homepage-widget-title">Кратко о портале: </div>
                                            </header>
                                            <p class="homepage-widget-description">Идейные соображения высшего порядка, а также дальнейшее развитие различных форм деятельности обеспечивает широкому кругу (специалистов) участие в формировании существенных финансовых и административных условий. </p>
                                            <p class="homepage-widget-description">Идейные соображения высшего порядка, а также постоянный количественный рост и сфера нашей активности играет важную роль в формировании модели развития. Значимость этих проблем настолько очевидна, что дальнейшее развитие различных форм деятельности способствует подготовки и реализации дальнейших направлений развития. Значимость этих проблем настолько очевидна, что консультация с широким активом представляет собой интересный эксперимент проверки форм развития. </p>
                                            <p class="homepage-widget-description">Идейные соображения высшего порядка, а также постоянный количественный рост и сфера нашей активности играет важную роль в формировании модели развития. Значимость этих проблем настолько очевидна, что дальнейшее развитие различных форм деятельности способствует подготовки и реализации дальнейших направлений развития. Значимость этих проблем настолько очевидна, что консультация с широким активом представляет собой интересный эксперимент проверки форм развития. </p>
                                        </article>
                                    </div>
                                </div>
                            </section>
                        </div>
                        <aside class="sidebar job-spotlight col-lg-4 col-md-5 col-sm-12">
                            <div class="popular-ads">
                                <div class="h3">Популярные предложения:</div>
                                <article class="post ad-post">
                                    <header class="entry-header">
                                        <figure>
                                            <a href="" rel="bookmark" class="featured-image"><img src="https://demo.astoundify.com/jobify-darker/wp-content/uploads/sites/16/2013/07/Stocksy_txp15d4e891770000_Medium_28012-400x200.jpg" class="attachment-content-grid size-content-grid wp-post-image" alt="Stocksy_txp15d4e891770000_Medium_28012"></a>
                                        </figure>
                                        <div class="h4">
                                            <a href="" rel="bookmark">Очень длинный заголовок объявления</a>
                                        </div>
                                    </header>
                                    <!-- .entry-header -->
                                    <div class="entry">
                                        <div class="entry-summary">
                                            <p>Разнообразный и богатый опыт укрепление и развитие структуры представляет собой интересный эксперимент проверки дальнейших направлений развития. </p>
                                        </div>
                                    </div>
                                    <footer>
                                        <div class="ad-rating">
                                            Рейтинг: <span class="positive-rating">66.7</span> из 100
                                        </div>
                                    </footer>
                                </article>
                                <article class="post ad-post">
                                    <header class="entry-header">
                                        <figure>
                                            <a href="" rel="bookmark" class="featured-image"><img src="https://demo.astoundify.com/jobify-darker/wp-content/uploads/sites/16/2013/07/Stocksy_txp15d4e891770000_Medium_28012-400x200.jpg" class="attachment-content-grid size-content-grid wp-post-image" alt="Stocksy_txp15d4e891770000_Medium_28012"></a>
                                        </figure>
                                        <div class="h4">
                                            <a href="" rel="bookmark">Очень длинный заголовок объявления</a>
                                        </div>
                                    </header>
                                    <!-- .entry-header -->
                                    <div class="entry">
                                        <div class="entry-summary">
                                            <p>Разнообразный и богатый опыт укрепление и развитие структуры представляет собой интересный эксперимент проверки дальнейших направлений развития. </p>
                                        </div>
                                    </div>
                                    <footer>
                                        <div class="ad-rating">
                                            Рейтинг: <span class="null-rating">0</span> из 100
                                        </div>
                                    </footer>
                                </article>
                                <article class="post ad-post">
                                    <header class="entry-header">
                                        <figure>
                                            <a href="" rel="bookmark" class="featured-image"><img src="https://demo.astoundify.com/jobify-darker/wp-content/uploads/sites/16/2013/07/Stocksy_txp15d4e891770000_Medium_28012-400x200.jpg" class="attachment-content-grid size-content-grid wp-post-image" alt="Stocksy_txp15d4e891770000_Medium_28012"></a>
                                        </figure>
                                        <div class="h4">
                                            <a href="" rel="bookmark">Очень длинный заголовок объявления</a>
                                        </div>
                                    </header>
                                    <!-- .entry-header -->
                                    <div class="entry">
                                        <div class="entry-summary">
                                            <p>Разнообразный и богатый опыт укрепление и развитие структуры представляет собой интересный эксперимент проверки дальнейших направлений развития. </p>
                                        </div>
                                    </div>
                                    <footer>
                                        <div class="ad-rating">
                                            Рейтинг: <span class="negative-rating">-30</span> из 100
                                        </div>
                                    </footer>
                                </article>
                            </div>
                            <div class="commercial-banner">
                                <img src="{$tpl['url']}/images/banner1.jpg">
                            </div>
                            <div class="popular-seller">
                                <div class="h3">Лучшие специалисты:</div>
                                <div class="seller status-publish">
                                    <div class="row">
                                        <div class="seller-listing-link">
                                            <figure class="logo-seller col-xs-3 col-sm-4 col-md-4 col-lg-4">
                                                <a href=""><img class="seller-logo" src="{$tpl['url']}/images/seller-avatar.jpg" alt=""></a>
                                            </figure>
                                            <div class="descr-seller col-xs-9 col-sm-8 col-md-8 col-lg-8">
                                                <a class="h5" href=""><span class="user-mark">@</span><span class="user-login">KateCat</span></a>
                                                <div class="specialisation">
                                                    <a href="">Аренда оборудования</a> от <span class="mid-price">1000<i class="fa fa-rub"></i>/час</span>
                                                </div>
                                                <p class="about-user"><span class="user-ads">14</span> объявлений, рейтинг: <span class="user-rating positive-rating">4.3</span> из 100</p>
                                                <a class="more-info" href="">все объявления пользователя <i class="fa fa-angle-double-right"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="seller status-publish">
                                    <div class="row">
                                        <div class="seller-listing-link">
                                            <figure class="logo-seller  no-avatar col-xs-3 col-sm-4 col-md-4 col-lg-4">
                                                <a href=""><img class="seller-logo" src="{$tpl['url']}/images/noavatar.png" alt=""></a>
                                            </figure>
                                            <div class="descr-seller col-xs-9 col-sm-8 col-md-8 col-lg-8">
                                                <a class="h5" href=""><span class="user-mark">@</span><span class="user-login">KateCat</span></a>
                                                <div class="specialisation">
                                                    <a href="">Дрессировка, хендлинг</a> от <span class="mid-price">340<i class="fa fa-rub"></i>/час</span>
                                                </div>
                                                <p class="about-user"><span class="user-ads">14</span> объявлений, рейтинг: <span class="user-rating null-rating">0</span> из 100</p>
                                                <a class="more-info" href="">все объявления пользователя <i class="fa fa-angle-double-right"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="seller status-publish">
                                    <div class="row">
                                        <div class="seller-listing-link">
                                            <figure class="logo-seller col-xs-3 col-sm-4 col-md-4 col-lg-4">
                                                <a href=""><img class="seller-logo" src="{$tpl['url']}/images/seller-avatar.jpg" alt=""></a>
                                            </figure>
                                            <div class="descr-seller col-xs-9 col-sm-8 col-md-8 col-lg-8">
                                                <a class="h5" href=""><span class="user-mark">@</span><span class="user-login">KateCat</span></a>
                                                <div class="specialisation">
                                                    <a href="">Изготовление клеток, домиков, аквариумов</a> от <span class="mid-price">800<i class="fa fa-rub"></i></span>
                                                </div>
                                                <p class="about-user"><span class="user-ads">14</span> объявлений, рейтинг: <span class="user-rating negative-rating">-4.3</span> из 100</p>
                                                <a class="more-info" href="">все объявления пользователя <i class="fa fa-angle-double-right"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="sidebar-articles">
                                <div class="h3">Статьи:</div>
                                <article class="announcement">
                                    <header>
                                        <a href="" class="h3">Очень длинный заголовк статьи</a>
                                    </header>
                                    <p class="descr">
                                        Разнообразный и богатый опыт дальнейшее развитие различных форм деятельности в значительной степени обуславливает создание направлений прогрессивного развития...
                                    </p>
                                </article>
                                <article class="announcement">
                                    <header>
                                        <a href="" class="h3">Очень длинный заголовк статьи</a>
                                    </header>
                                    <p class="descr">
                                        Разнообразный и богатый опыт дальнейшее развитие различных форм деятельности в значительной степени обуславливает создание направлений прогрессивного развития...
                                    </p>
                                </article>
                                <a href="" class="more-info">Перейти ко всем статьям <i class="fa fa-angle-double-right"></i></a>
                            </div>
                        </aside>
                    </div>
                </div>
            </section>
        </div>
        <!-- #content -->
    </div>
    <!-- #primary -->
</div>
HTML;
