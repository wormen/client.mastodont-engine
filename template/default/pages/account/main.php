<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 09.02.2016
 * Time: 10:46
 * -------------------------------------
 * Главная страница ЛК
 */

$top = include __DIR__ .'/blocks/top.php';

if(isset($T['user']['avatar']) && strlen($T['user']['avatar'])>0){
    $defaultAva = '';

    $ava = <<<HTML
    <fieldset>
        <label><i class="fa fa-photo"></i>Загрузите аватар</label>
        <input type="file" name="file" class="custom-file-input" value="Выберите файл">
        <div class="gfield_description validation_message hint">*Размер изображение не должен превышать 500x500px.</div>
    </fieldset>
HTML;

}else{

    $ava = <<<HTML
    <fieldset>
        <label><i class="fa fa-photo"></i>Загрузите аватар</label>
        <input type="file" name="file" class="custom-file-input" value="Выберите файл">
        <div class="gfield_description validation_message hint">*Размер изображение не должен превышать 500x500px.</div>
    </fieldset>
HTML;

}

return <<<HTML
<div id="main" class="site-main">
    <div id="primary" class="content-area">
        <div id="content" class="homepage-content" role="main">

            <section class="breadcrums">{$top['breams']}</section>

            <section id="jobify_widget_jobs-3" class="homepage-widget jobify_widget_jobs">
                <div class="container">
                    <div class="row">
                        <aside class="sidebar personal-sidebar job-spotlight col-lg-4 col-md-5 col-sm-12">

                            {$top['info']}

                            <div class="sidebar-navigation">{$top['leftMenu']}</div>

                        </aside>
                        <div class="recent-jobs has-spotlight personal-inner col-lg-8 col-md-7 col-sm-12">
                            <div class="h2 title">Данные пользователя:</div>
                            <form action="" method="post" id="personalform" name="loginform" class="manager-form personalform">
                                <div class="row">

                                    <div class="col-xs-12 col-sm-12 col-md-6">
                                        <fieldset class="login-username">
                                            <label><i class="fa fa-user"></i>ФИО</label>
                                            <input type="text" name="log" class="input">
                                            <div class="gfield_description validation_message">Это поле должно быть заполнено</div>
                                        </fieldset>
                                        <fieldset class="email-username">
                                            <label><i class="fa fa-envelope"></i>Email</label>
                                            <input type="text" name="email" class="input">
                                        </fieldset>
                                        <fieldset class="phone-username">
                                            <label><i class="fa fa-phone"></i>Телефон</label>
                                            <input type="text" name="phone" class="input">
                                        </fieldset>
                                        <fieldset class="skype-username">
                                            <label><i class="fa fa-skype"></i>Skype</label>
                                            <input type="text" name="skype" class="input">
                                        </fieldset>
                                    </div>

                                    <div class="col-xs-12 col-sm-12 col-md-6">

                                        {$ava}

                                        <div class="social-networks">
                                            <div class="h5 title">Добавьте соцсети:</div>
                                            <fieldset class="vk-username">
                                                <input type="checkbox" name="vk" class="check-box-with-input" checked="checked">
                                                <label><i class="fa fa-vk"></i>Вконтакте</label>
                                                <input type="text" name="link" class="hide-input visible" placeholder='Вставьте ссылку на вашу страницу "Вконтакте"'>
                                            </fieldset>
                                            <fieldset class="ok-username">
                                                <input type="checkbox" name="ok" class="check-box-with-input">
                                                <label><i class="fa fa-odnoklassniki"></i>Одноклассники</label>
                                                <input type="text" name="link" class="hide-input" placeholder='Вставьте ссылку на вашу страницу "Одноклассники"'>
                                            </fieldset>
                                            <fieldset class="fb-username">
                                                <input type="checkbox" name="fb" class="check-box-with-input">
                                                <label><i class="fa fa-facebook"></i>Facebook</label>
                                                <input type="text" name="link" class="hide-input" placeholder='Вставьте ссылку на вашу страницу "Facebook"'>
                                            </fieldset>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>

                                    <div class="col-md-12">
                                        <div class="choose-city">
                                            <span class="h6">Ваш город: </span><a href=""><i class="icon-location"> </i>Владивосток</a>
                                        </div>
                                        <div class="map">
                                            <script type="text/javascript" charset="utf-8" src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=gEc9r0syEINhwNiEK8IKyq-7efcS-m8b&width=100%&height=200&lang=ru_RU&sourceType=constructor"></script>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>

                                    <div class="col-xs-12 col-sm-12 col-md-6 change-pass-inner">
                                        <div class="change-password accordion">
                                            <div class="accordion-section">
                                                <a class="accordion-section-title user menu-item menu-item-type-post_type menu-item-object-page change-pass" href="#accordion-pass"><i class="fa fa-shield"></i>Сменить пароль</a>
                                                <div id="accordion-pass" class="accordion-section-content">
                                                    <fieldset class="login-password">
                                                        <label>Cтарый пароль</label>
                                                        <input type="password" name="pwd" class="input">
                                                    </fieldset>
                                                    <fieldset class="login-new-password">
                                                        <label>Новый пароль</label>
                                                        <input type="password" name="pwd" class="input">
                                                    </fieldset>
                                                    <fieldset class="login-new-repeat-password">
                                                        <label>Повторите пароль</label>
                                                        <input type="password" name="pwd" class="input">
                                                        <div class="gfield_description validation_message">Пароли не совпадают</div>
                                                    </fieldset>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <fieldset class="save-changes">
                                    <button type="submit" name="wp-submit" class="button-primary">Сохранить</button>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <!-- #content -->
    </div>
    <!-- #primary -->
</div>
HTML;
