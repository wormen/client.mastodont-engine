<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 11.02.2016
 * Time: 21:05
 * -------------------------------------
 * Пополнение баланса в ЛК
 */

$top = include __DIR__ .'/blocks/top.php';
$minSumm = 30; // минимальная сумма

return <<<HTML
<div id="main" class="site-main">
    <div id="primary" class="content-area">
        <div id="content" class="homepage-content" role="main">

            <section class="breadcrums">{$top['breams']}</section>

            <section id="jobify_widget_jobs-3" class="homepage-widget jobify_widget_jobs">
                <div class="container">
                    <div class="row">
                        <aside class="sidebar personal-sidebar job-spotlight col-lg-4 col-md-5 col-sm-12">

                            {$top['info']}

                            <div class="sidebar-navigation">{$top['leftMenu']}</div>

                        </aside>
                        <div class="recent-jobs has-spotlight personal-inner col-lg-8 col-md-7 col-sm-12">
                            <div class="h2 title">Пополнение счета</div>
                            <div id="personal-pay" class="personal-pay">
                                <p>
                                    Выберите сумму для пополнения счета.
                                </p>

                                <div class="select-sum">
                                    <a class="sum-block" @click="selectSumm(\$event, 100)">
                                        100 <i class="fa fa-rub"></i>
                                    </a>
                                    <a class="sum-block" @click="selectSumm(\$event, 200)">
                                        200 <i class="fa fa-rub"></i>
                                    </a>
                                    <a class="sum-block active" @click="selectSumm(\$event, 300)">
                                        300 <i class="fa fa-rub"></i>
                                    </a>
                                    <a class="sum-block" @click="selectSumm(\$event, 500)">
                                        500 <i class="fa fa-rub"></i>
                                    </a>
                                    <a class="sum-block" @click="selectSumm(\$event, 1000)">
                                        1000 <i class="fa fa-rub"></i>
                                    </a>
                                </div>

                                <div class="other-sum-inner">
                                    <button id="other-sum-form-open" class="other-sum">Другая сумма...</button>
                                    <form class="other-sum-form manager-form">
                                        <fieldset>
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <input type="text" v-model="paySumm">
                                                </div>
                                            </div>
                                            <div class="gfield_description validation_message hint">*Минимальная сумма платежа {$minSumm} рублей.</div>
                                        </fieldset>
                                    </form>
                                </div>
                                <div class="pay-button">
                                    <button type="submit" name="wp-submit" class="button-primary">Пополнить счет</button>
                                </div>
                                <div class="page-notify error" v-show="alert.isMinSumm" style="display:none;">
                                   <p>Вы не можете пополнить баланс меньше чем на {$minSumm} рублей.</p>
                               </div>
                                <div class="dop-margin-top40" v-show="alert.isMinSumm" style="display:none;"></div>

                                <div class="pay-info">
                                    <p>
                                        Оплата производится через сиcтему <a href="https://kassa.yandex.ru/" target="_blank">Яндекс-Касса</a>.
                                    </p>
                                    <p>
                                        Ваш платеж поступит на счет в течении 5 минут.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <!-- #content -->
    </div>
    <!-- #primary -->
</div>
HTML;
