<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 13.02.2016
 * Time: 15:53
 * -------------------------------------
 * Страница для неавторизванных пользователей
 */

$lang = $GLOBALS['lang'];
$top = include __DIR__ .'/blocks/top.php';

return <<<HTML
<div id="main" class="site-main">
    <div id="primary" class="content-area">
        <div id="content" class="homepage-content" role="main">

            <section class="breadcrums">{$top['breams']}</section>

            <section id="jobify_widget_jobs-3" class="homepage-widget jobify_widget_jobs">
                <div class="container">

                    <div class="col-xs-12 col-sm-6 col-md-6 login-modal-inner">
                        <div id="loginform" name="loginform" class="manager-form">
                            <fieldset class="login-username">
                                <label for="user_login">Логин или email</label>
                                <input type="text" name="log" id="user_login" class="input" value="" size="20" />
                            </fieldset>

                            <fieldset class="login-password">
                                <label for="user_pass">Пароль</label>
                                <input type="password" name="pwd" id="user_pass" class="input" value="" size="20" />
                            </fieldset>

                            <fieldset class="has-account"><i class="icon-help-circled"></i> <a href="">Забыли пароль?</a>
                                <button type="submit" name="wp-submit" class="button-primary wp-submit">Войти</button>
                            </fieldset>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-6 forget-pass-inner" style="display:none;">
                        <div id="lostpass" name="loginform" class="manager-form">
                            <fieldset class="login-username">
                                <label for="user_login">{$lang['account']['login']} или email</label>
                                <input type="text" name="log" id="user_login" class="input" value="" size="20" />
                            </fieldset>

                            <fieldset class="has-account"><a href="#">{$lang['btns']['logIn']}</a>
                                <button class="button-primary wp-submit">{$lang['btns']['repair']}</button>
                            </fieldset>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-6">
                        <div class="dop-margin-top40"></div>
                        <div class="page-notify error">
                            <p>Вам необходимо авторизоваться, чтобы войти в личный кабинет</p>
                        </div>

                        <div class="page-notify error">
                            <p>Вывод ошибки/уведомления</p>
                        </div>
                        <div class="dop-margin-top40"></div>
                    </div>

                </div>
            </section>
        </div>
        <!-- #content -->
    </div>
    <!-- #primary -->
</div>
HTML;
