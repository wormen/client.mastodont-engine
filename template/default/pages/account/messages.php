<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 11.02.2016
 * Time: 21:03
 * -------------------------------------
 * Сообщения пользователя
 */

$top = include __DIR__ .'/blocks/top.php';

return <<<HTML
<div id="main" class="site-main">
    <div id="primary" class="content-area">
        <div id="content" class="homepage-content" role="main">

            <section class="breadcrums">{$top['breams']}</section>

            <section id="jobify_widget_jobs-3" class="homepage-widget jobify_widget_jobs">
                <div class="container">
                    <div class="row">
                        <aside class="sidebar personal-sidebar job-spotlight col-lg-4 col-md-5 col-sm-12">

                            {$top['info']}

                            <div class="sidebar-navigation">{$top['leftMenu']}</div>

                        </aside>
                        <div class="recent-jobs has-spotlight personal-inner col-lg-8 col-md-7 col-sm-12">
                            <div class="h2 title">Мои сообщения</div>
                            <div id="personal-messages-list" class="personal-messages-list">
                                <article class="your-message new">
                                    <header>
                                        <div class="h5">Объявление "Валяю валенки" (id:678454) прошло модерацию</div>
                                        <div class="date">22.08.2016</div>
                                    </header>
                                    <div class="shot-descr">
                                        С другой стороны постоянное информационно-пропагандистское обеспечение нашей деятельности требуют определения и уточнения направлений прогрессивного развития.
                                        <button class="more-info" id="open-message-more-info1">Раскрыть</button>
                                        <button class="more-info close-message" id="close-message-more-info1">Скрыть</button><span class="message-more-info" id="message-more-info1">Задача организации, в особенности же постоянное информационно-пропагандистское обеспечение нашей деятельности требуют от нас анализа направлений прогрессивного развития. Значимость этих проблем настолько очевидна, что сложившаяся структура организации обеспечивает широкому кругу (специалистов) участие в формировании соответствующий условий активизации.
                                            <p>Разнообразный и богатый опыт новая модель организационной деятельности в значительной степени обуславливает создание новых предложений. Товарищи! начало повседневной работы по формированию позиции играет важную роль в формировании соответствующий условий активизации. Разнообразный и богатый опыт сложившаяся структура организации играет важную роль в формировании форм развития. Повседневная практика показывает, что новая модель организационной деятельности требуют от нас анализа направлений прогрессивного развития. Задача организации, в особенности же дальнейшее развитие различных форм деятельности обеспечивает широкому кругу (специалистов) участие в формировании модели развития.
                                            </p>
                                        </span>
                                    </div>
                                </article>
                                <article class="your-message new">
                                    <header>
                                        <div class="h5">Сообщение от пользователя <a href="" class="user-login">SuperUser</a></div>
                                        <div class="date">22.08.2016</div>
                                    </header>
                                    <div class="shot-descr">
                                        С другой стороны постоянное информационно-пропагандистское обеспечение нашей деятельности требуют определения и уточнения направлений прогрессивного развития.
                                        <button class="more-info" id="open-message-more-info2">Раскрыть</button>
                                        <button class="more-info close-message" id="close-message-more-info2">Скрыть</button><span class="message-more-info" id="message-more-info2">Задача организации, в особенности же постоянное информационно-пропагандистское обеспечение нашей деятельности требуют от нас анализа направлений прогрессивного развития. Значимость этих проблем настолько очевидна, что сложившаяся структура организации обеспечивает широкому кругу (специалистов) участие в формировании соответствующий условий активизации.
                                            <p>Разнообразный и богатый опыт новая модель организационной деятельности в значительной степени обуславливает создание новых предложений. Товарищи! начало повседневной работы по формированию позиции играет важную роль в формировании соответствующий условий активизации. Разнообразный и богатый опыт сложившаяся структура организации играет важную роль в формировании форм развития. Повседневная практика показывает, что новая модель организационной деятельности требуют от нас анализа направлений прогрессивного развития. Задача организации, в особенности же дальнейшее развитие различных форм деятельности обеспечивает широкому кругу (специалистов) участие в формировании модели развития.
                                        </p>
                                        </span>
                                    </div>
                                    <div class="answer-link">
                                        <div class="messages-answer accordion">
                                            <div class="accordion-section">
                                                <a class="accordion-section-title user menu-item menu-item-type-post_type menu-item-object-page" href="#accordion-answer1"><i class="fa fa-pencil"></i>Ответить</a>
                                                <div id="accordion-answer1" class="accordion-section-content">
                                                    <form action="" method="post" name="answerform" class="manager-form">
                                                        <fieldset class="message">
                                                            <label>Текст сообщения</label>
                                                            <textarea rows="3"></textarea>
                                                        </fieldset>
                                                        <fieldset class="has-account">
                                                            <button type="submit" class="button-primary">Отправить</button>
                                                        </fieldset>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                                <article class="your-message ">
                                    <header>
                                        <div class="h5">Объявление "Валяю валенки" (id:678454) прошло модерацию</div>
                                        <div class="date">22.08.2016</div>
                                    </header>
                                    <div class="shot-descr">
                                        С другой стороны постоянное информационно-пропагандистское обеспечение нашей деятельности требуют определения и уточнения направлений прогрессивного развития.
                                        <button class="more-info" id="open-message-more-info3">Раскрыть</button>
                                        <button class="more-info close-message" id="close-message-more-info3">Скрыть</button><span class="message-more-info" id="message-more-info3">Задача организации, в особенности же постоянное информационно-пропагандистское обеспечение нашей деятельности требуют от нас анализа направлений прогрессивного развития. Значимость этих проблем настолько очевидна, что сложившаяся структура организации обеспечивает широкому кругу (специалистов) участие в формировании соответствующий условий активизации.
                                            <p>Разнообразный и богатый опыт новая модель организационной деятельности в значительной степени обуславливает создание новых предложений. Товарищи! начало повседневной работы по формированию позиции играет важную роль в формировании соответствующий условий активизации. Разнообразный и богатый опыт сложившаяся структура организации играет важную роль в формировании форм развития. Повседневная практика показывает, что новая модель организационной деятельности требуют от нас анализа направлений прогрессивного развития. Задача организации, в особенности же дальнейшее развитие различных форм деятельности обеспечивает широкому кругу (специалистов) участие в формировании модели развития.
                                            </p>
                                        </span>
                                    </div>
                                </article>
                            </div>
                        </div>
                        <script>
                        $(document).ready(function() {
                            $('#open-message-more-info1').bind('click', function() {
                                $("#message-more-info1").slideDown("slow");
                                $('#open-message-more-info1').addClass('no-visible');
                                $('#close-message-more-info1').show();
                            });
                            $('#close-message-more-info1').bind('click', function() {
                                $("#message-more-info1").slideUp("slow");
                                $('#open-message-more-info1').removeClass('no-visible');
                                $('#close-message-more-info1').hide();
                            });
                            $('#open-message-more-info2').bind('click', function() {
                                $("#message-more-info2").slideDown("slow");
                                $('#open-message-more-info2').addClass('no-visible');
                                $('#close-message-more-info2').show();
                            });
                            $('#close-message-more-info2').bind('click', function() {
                                $("#message-more-info2").slideUp("slow");
                                $('#open-message-more-info2').removeClass('no-visible');
                                $('#close-message-more-info2').hide();
                            });
                            $('#open-message-more-info3').bind('click', function() {
                                $("#message-more-info3").slideDown("slow");
                                $('#open-message-more-info3').addClass('no-visible');
                                $('#close-message-more-info3').show();
                            });
                            $('#close-message-more-info3').bind('click', function() {
                                $("#message-more-info3").slideUp("slow");
                                $('#open-message-more-info3').removeClass('no-visible');
                                $('#close-message-more-info3').hide();
                            });
                        });
                        </script>
                    </div>
                </div>
            </section>
        </div>
        <!-- #content -->
    </div>
    <!-- #primary -->
</div>
HTML;
