<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 11.02.2016
 * Time: 21:00
 */

$top = include __DIR__ .'/blocks/top.php';

return <<<HTML
<div id="main" class="site-main">
    <div id="primary" class="content-area">
        <div id="content" class="homepage-content" role="main">

            <section class="breadcrums">{$top['breams']}</section>

            <section id="jobify_widget_jobs-3" class="homepage-widget jobify_widget_jobs">
                <div class="container">
                    <div class="row">
                        <aside class="sidebar personal-sidebar job-spotlight col-lg-4 col-md-5 col-sm-12">

                            {$top['info']}

                            <div class="sidebar-navigation">{$top['leftMenu']}</div>

                        </aside>
                        <div class="recent-jobs has-spotlight personal-inner col-lg-8 col-md-7 col-sm-12">
                            <div class="h2 title">Мои объявления</div>
                            <div class="personal-ads-list moderate-ads-inner">
                                <div class="h5 title"><i class="fa fa-eye"></i>Объявления на модерации:</div>
                                <div class="list-of-ads">

                                    <article class="one-line-ad">
                                        <div class="row">
                                            <figure class="col-xs-3 col-sm-2 col-md-2">
                                                <a href="">
                                                    <img src="{$tpl['url']}/images/get-exposure-for-your-biz.jpg" alt="">
                                                </a>
                                            </figure>
                                            <div class="col-xs-8 col-sm-9 col-md-9">
                                                <header>
                                                    <a href="" class="h6">Очищение котиков и песиков от пыли и грязи недорого</a>
                                                </header>
                                                <p class="descr">
                                                    Равным образом рамки и место обучения кадров позволяет оценить значение существенных финансовых и административных условий.
                                                </p>
                                            </div>
                                            <div class="col-xs-1 col-sm-1 col-md-1">
                                                <div class="icon-inner disabled-icon">
                                                    <a href=""><i class="fa fa-level-up"></i></a>
                                                </div>
                                                <div class="icon-inner">
                                                    <a href="" data-hint="Редактировать" class="hint--right"><i class="fa fa-pencil"></i></a>
                                                </div>
                                                <div class="icon-inner">
                                                    <a href="" data-hint="Удалить" class="hint--right"><i class="fa fa-trash"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                    <article class="one-line-ad">
                                        <div class="row">
                                            <figure class="col-xs-3 col-sm-2 col-md-2">
                                                <a href="">
                                                    <img src="{$tpl['url']}/images/get-exposure-for-your-biz.jpg" alt="">
                                                </a>
                                            </figure>
                                            <div class="col-xs-8 col-sm-9 col-md-9">
                                                <header>
                                                    <a href="" class="h6">Очищение котиков и песиков от пыли и грязи недорого</a>
                                                </header>
                                                <p class="descr">
                                                    Равным образом рамки и место обучения кадров позволяет оценить значение существенных финансовых и административных условий.
                                                </p>
                                            </div>
                                            <div class="col-xs-1 col-sm-1 col-md-1">
                                                <div class="icon-inner disabled-icon">
                                                    <a href=""><i class="fa fa-level-up"></i></a>
                                                </div>
                                                <div class="icon-inner">
                                                    <a href="" data-hint="Редактировать" class="hint--right"><i class="fa fa-pencil"></i></a>
                                                </div>
                                                <div class="icon-inner">
                                                    <a href="" data-hint="Удалить" class="hint--right"><i class="fa fa-trash"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                </div>
                                <a href="" class="more-info">Показать все объявления на модерации <i class="fa fa-angle-double-right"></i></a>
                            </div>
                            <div class="personal-ads-list actual-ads-inner">
                                <div class="h5 title"><i class="fa fa-thumb-tack"></i>Актуальные объявления:</div>
                                <div class="list-of-ads">
                                    <article class="one-line-ad">
                                        <div class="row">
                                            <figure class="col-xs-3 col-sm-2 col-md-2">
                                                <a href="">
                                                    <img src="{$tpl['url']}/images/get-exposure-for-your-biz.jpg" alt="">
                                                </a>
                                            </figure>
                                            <div class="col-xs-8 col-sm-9 col-md-9">
                                                <header>
                                                    <a href="" class="h6">Очищение котиков и песиков от пыли и грязи недорого</a>
                                                </header>
                                                <p class="descr">
                                                    Равным образом рамки и место обучения кадров позволяет оценить значение существенных финансовых и административных условий.
                                                </p>
                                                <footer>
                                                <div class="ad-rating">
                                                    Рейтинг: <span class="positive-rating">33.9</span> из 100
                                                </div>
                                            </footer>
                                            </div>
                                            <div class="col-xs-1 col-sm-1 col-md-1">
                                                <div class="icon-inner">
                                                    <a href="" data-hint="Поднять объявление" class="hint--right"><i class="fa fa-level-up"></i></a>
                                                </div>
                                                <div class="icon-inner">
                                                    <a href="" data-hint="Редактировать" class="hint--right"><i class="fa fa-pencil"></i></a>
                                                </div>
                                                <div class="icon-inner">
                                                    <a href="" data-hint="Удалить" class="hint--right"><i class="fa fa-trash"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                    <article class="one-line-ad">
                                        <div class="row">
                                            <figure class="col-xs-3 col-sm-2 col-md-2">
                                                <a href="">
                                                    <img src="{$tpl['url']}/images/get-exposure-for-your-biz.jpg" alt="">
                                                </a>
                                            </figure>
                                            <div class="col-xs-8 col-sm-9 col-md-9">
                                                <header>
                                                    <a href="" class="h6">Очищение котиков и песиков от пыли и грязи недорого</a>
                                                </header>
                                                <p class="descr">
                                                    Равным образом рамки и место обучения кадров позволяет оценить значение существенных финансовых и административных условий.
                                                </p>
                                                <footer>
                                                <div class="ad-rating">
                                                    Рейтинг: <span class="positive-rating">33.9</span> из 100
                                                </div>
                                            </footer>
                                            </div>
                                            <div class="col-xs-1 col-sm-1 col-md-1">
                                                <div class="icon-inner">
                                                    <a href="" data-hint="Поднять объявление" class="hint--right"><i class="fa fa-level-up"></i></a>
                                                </div>
                                                <div class="icon-inner">
                                                    <a href="" data-hint="Редактировать" class="hint--right"><i class="fa fa-pencil"></i></a>
                                                </div>
                                                <div class="icon-inner">
                                                    <a href="" data-hint="Удалить" class="hint--right"><i class="fa fa-trash"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                </div>
                                <a href="" class="more-info">Показать все актуальные объявления <i class="fa fa-angle-double-right"></i></a>
                            </div>
                            <div class="personal-ads-list archive-ads-inner">
                                <div class="h5 title"><i class="fa fa-archive"></i>Архив объявлений:</div>
                                <div class="list-of-ads">
                                    <article class="one-line-ad">
                                        <div class="row">
                                            <figure class="col-xs-3 col-sm-2 col-md-2">
                                                <a href="">
                                                    <img src="{$tpl['url']}/images/get-exposure-for-your-biz.jpg" alt="">
                                                </a>
                                            </figure>
                                            <div class="col-xs-8 col-sm-9 col-md-9">
                                                <header>
                                                    <a href="" class="h6">Очищение котиков и песиков от пыли и грязи недорого</a>
                                                </header>
                                                <p class="descr">
                                                    Равным образом рамки и место обучения кадров позволяет оценить значение существенных финансовых и административных условий.
                                                </p>
                                                <footer>
                                                <div class="ad-rating">
                                                    Рейтинг: <span class="positive-rating">33.9</span> из 100
                                                </div>
                                            </footer>
                                            </div>
                                            <div class="col-xs-1 col-sm-1 col-md-1">
                                                <div class="icon-inner">
                                                    <a href="" data-hint="Поднять объявление" class="hint--right"><i class="fa fa-level-up"></i></a>
                                                </div>
                                                <div class="icon-inner">
                                                    <a href="" data-hint="Редактировать" class="hint--right"><i class="fa fa-pencil"></i></a>
                                                </div>
                                                <div class="icon-inner">
                                                    <a href="" data-hint="Восстановить" class="hint--right"><i class="fa fa-history"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                    <article class="one-line-ad">
                                        <div class="row">
                                            <figure class="col-xs-3 col-sm-2 col-md-2">
                                                <a href="">
                                                    <img src="{$tpl['url']}/images/get-exposure-for-your-biz.jpg" alt="">
                                                </a>
                                            </figure>
                                            <div class="col-xs-8 col-sm-9 col-md-9">
                                                <header>
                                                    <a href="" class="h6">Очищение котиков и песиков от пыли и грязи недорого</a>
                                                </header>
                                                <p class="descr">
                                                    Равным образом рамки и место обучения кадров позволяет оценить значение существенных финансовых и административных условий.
                                                </p>
                                                <footer>
                                                <div class="ad-rating">
                                                    Рейтинг: <span class="positive-rating">33.9</span> из 100
                                                </div>
                                            </footer>
                                            </div>
                                            <div class="col-xs-1 col-sm-1 col-md-1">
                                                <div class="icon-inner">
                                                    <a href="" data-hint="Поднять объявление" class="hint--right"><i class="fa fa-level-up"></i></a>
                                                </div>
                                                <div class="icon-inner">
                                                    <a href="" data-hint="Редактировать" class="hint--right"><i class="fa fa-pencil"></i></a>
                                                </div>
                                                <div class="icon-inner">
                                                    <a href="" data-hint="Восстановить" class="hint--right"><i class="fa fa-history"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                </div>
                                <a href="" class="more-info">Показать все объявления в архиве <i class="fa fa-angle-double-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <!-- #content -->
    </div>
    <!-- #primary -->
</div>
HTML;
