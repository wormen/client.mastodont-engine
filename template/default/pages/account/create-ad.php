<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 11.02.2016
 * Time: 21:08
 * -------------------------------------
 * Страница создания объявления
 */

$top = include __DIR__ .'/blocks/top.php';

return <<<HTML
<div id="main" class="site-main">
    <div id="primary" class="content-area">
        <div id="content" class="homepage-content" role="main">

            <section class="breadcrums">{$top['breams']}</section>

            <section id="jobify_widget_jobs-3" class="homepage-widget jobify_widget_jobs">
                <div class="container">
                    <div class="row">
                        <aside class="sidebar personal-sidebar job-spotlight col-lg-4 col-md-5 col-sm-12">
                            <div class="instruction">
                                <p>Перед созданием объявления, ознакомьтесь с <a href="">правилами сайта</a>.</p>
                                <p>Не подавайте одно и то же объявление повторно. <a href="">Почему?</a></p>
                                <p>Не указывайте телефон, электронную почту или адрес сайта в описании или на фото.</p>
                                <p>Не предлагайте <a href="">запрещённые услуги</a>. </p>
                            </div>

                            {$top['info']}

                            <div class="sidebar-navigation">
                                {$top['leftMenu']}
                            </div>
                        </aside>
                        
                        <div class="recent-jobs has-spotlight create-ad-inner col-lg-8 col-md-7 col-sm-12">
                            <div class="h2 title">Создание объявления</div>
                            <div class="create-ad-block">
                                <form action="" method="post" name="createad" class="manager-form create-ad">
                                    <fieldset class="message">
                                        <label>Заголовок объявления<span class="gfield_required">*</span></label>
                                        <input type="text" name="header" class="medium">
                                        <div class="gfield_description validation_message">Это поле обязательно для зaполнения</div>
                                    </fieldset>
                                    <div class="disabled-fiels">
                                        <div class="ad-id">
                                            ID: <span>679759</span>
                                        </div>
                                        <div class="ad-date">
                                            Дата публикации: <span>12 сентября 2016</span>
                                        </div>
                                    </div>
                                    <div class="category-of-new-ad">
                                        <label>Выберите категорию объявления<span class="gfield_required">*</span></label>
                                        <div class="checked-category">
                                            <label>Выбрано: </label><a href="">Авто и мото</a><span class="delimiter">/</span><a href="">Заказ авто и запчастей</a><span class="delimiter">/</span><a href="">Доставка авто и запчастей</a><span class="delimiter">/</span><a href="">Доставка запчастей</a>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-3 col-md-3 category-col category-col1">
                                                <div class="col-content">
                                                    <div class="title">
                                                        Категория
                                                    </div>
                                                    <ul class="select-category">
                                                        <li>
                                                            <input type="radio" id="radio1" name="radio-lvl-1" value="all" checked>
                                                            <label for="radio1">Авто и мото</label>
                                                        </li>
                                                        <li>
                                                            <input type="radio" id="radio2" name="radio-lvl-1" value="false">
                                                            <label for="radio2">Водная техника</label>
                                                        </li>
                                                        <li>
                                                            <input type="radio" id="radio3" name="radio-lvl-1" value="true">
                                                            <label for="radio3">Бизнес</label>
                                                        </li>
                                                        <li>
                                                            <input type="radio" id="radio4" name="radio-lvl-1" value="all">
                                                            <label for="radio4">Дети и материнство</label>
                                                        </li>
                                                        <li>
                                                            <input type="radio" id="radio5" name="radio-lvl-1" value="false">
                                                            <label for="radio5">Животные</label>
                                                        </li>
                                                        <li>
                                                            <input type="radio" id="radio6" name="radio-lvl-1" value="true">
                                                            <label for="radio6">Недвижимость</label>
                                                        </li>
                                                        <li>
                                                            <input type="radio" id="radio7" name="radio-lvl-1" value="all">
                                                            <label for="radio7">Спорт, туризм, охота</label>
                                                        </li>
                                                        <li>
                                                            <input type="radio" id="radio8" name="radio-lvl-1" value="false">
                                                            <label for="radio8">Хобби</label>
                                                        </li>
                                                        <li>
                                                            <input type="radio" id="radio9" name="radio-lvl-1" value="true">
                                                            <label for="radio9">Путешествие и отдых</label>
                                                        </li>
                                                        <li>
                                                            <input type="radio" id="radio10" name="radio-lvl-1" value="all">
                                                            <label for="radio10">Свадьба</label>
                                                        </li>
                                                        <li>
                                                            <input type="radio" id="radio11" name="radio-lvl-1" value="false">
                                                            <label for="radio11">Дом и ремонт</label>
                                                        </li>
                                                        <li>
                                                            <input type="radio" id="radio12" name="radio-lvl-1" value="true">
                                                            <label for="radio12">Красота и здоровье</label>
                                                        </li>
                                                        <li>
                                                            <input type="radio" id="radio13" name="radio-lvl-1" value="all">
                                                            <label for="radio13">Компьютеры, IT, интернет</label>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-3 col-md-3 category-col category-col2">
                                                <button class="modal-back">
                                                    <i class="fa fa-arrow-circle-o-left"></i>Назад
                                                </button>
                                                <div class="col-content">
                                                    <div class="title">
                                                        Категория
                                                    </div>
                                                    <ul class="select-category">
                                                        <li>
                                                            <input type="radio" id="radio1-1" name="radio-lvl-2" value="all">
                                                            <label for="radio1-1">GT и тюнинг</label>
                                                        </li>
                                                        <li>
                                                            <input type="radio" id="radio1-2" name="radio-lvl-2" value="false">
                                                            <label for="radio1-2">Автозвук</label>
                                                        </li>
                                                        <li>
                                                            <input type="radio" id="radio1-3" name="radio-lvl-2" value="true">
                                                            <label for="radio1-3">Аренда</label>
                                                        </li>
                                                        <li>
                                                            <input type="radio" id="radio1-4" name="radio-lvl-2" value="all" checked>
                                                            <label for="radio1-4">Заказ авто и запчастей</label>
                                                        </li>
                                                        <li>
                                                            <input type="radio" id="radio1-5" name="radio-lvl-2" value="false">
                                                            <label for="radio1-5">Замена тех. жидкостей</label>
                                                        </li>
                                                        <li>
                                                            <input type="radio" id="radio1-6" name="radio-lvl-2" value="true">
                                                            <label for="radio1-6">Оформление документов</label>
                                                        </li>
                                                        <li>
                                                            <input type="radio" id="radio1-7" name="radio-lvl-2" value="all">
                                                            <label for="radio1-7">Ремонт</label>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-3 col-md-3 category-col category-col3">
                                                <button class="modal-back">
                                                    <i class="fa fa-arrow-circle-o-left"></i>Назад
                                                </button>
                                                <div class="col-content">
                                                    <div class="title">
                                                        Категория
                                                    </div>
                                                    <ul class="select-category">
                                                        <li>
                                                            <input type="radio" id="radio1-4-1" name="radio-lvl-3" value="all">
                                                            <label for="radio1-4-1">Аукционы</label>
                                                        </li>
                                                        <li>
                                                            <input type="radio" id="radio1-4-2" name="radio-lvl-3" value="false">
                                                            <label for="radio1-4-2">Заказ авто через агента</label>
                                                        </li>
                                                        <li>
                                                            <input type="radio" id="radio1-4-3" name="radio-lvl-3" value="true" checked>
                                                            <label for="radio1-4-3">Доставка авто и запчастей</label>
                                                        </li>
                                                        <li>
                                                            <input type="radio" id="radio1-4-4" name="radio-lvl-3" value="all">
                                                            <label for="radio1-4-4">Оформление документов</label>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-3 col-md-3 category-col category-col4">
                                                <button class="modal-back">
                                                    <i class="fa fa-arrow-circle-o-left"></i>Назад
                                                </button>
                                                <div class="col-content">
                                                    <div class="title">
                                                        Категория
                                                    </div>
                                                    <ul class="select-category">
                                                        <li>
                                                            <input type="radio" id="radio1-4-3-1" name="radio-lvl-4" value="all">
                                                            <label for="radio1-4-3-1">Доставка авто</label>
                                                        </li>
                                                        <li>
                                                            <input type="radio" id="radio1-4-3-2" name="radio-lvl-4" value="false" checked>
                                                            <label for="radio1-4-3-2">Доставка запчастей</label>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="report-about-category" class="category-not-found">
                                            Не нашли нужную категорию? <a href="">Напишите нам</a>
                                        </div>
                                    </div>
                                    <fieldset class="ad-images">
                                        <label class="label-witth-input">Загрузите фотографии для вашего объявления</label>
                                        <input type="file" name="file" multiple="multiple" class="custom-file-input" value="Выберите изображения">
                                        <div class="dop-img-hint">
                                            <div class="gfield_description validation_message hint">*Убедитесь, что ваше изображение весит не более 1,5 Мб. Рекомендуемый размер изображения от 800px до 1500px. </div>
                                        </div>
                                        <div class="drop-zone">
                                            <div class="title">Перетащите фотографии на это поле</div>
                                            <div class="photo-miniature-inner">
                                                <div class="photo-miniature loading">
                                                    <a href=""><img src="images/mini-ad-img1.jpg"></a>
                                                    <div class="meter nostripes">
                                                        <span style="width: 25%"></span>
                                                    </div>
                                                </div>
                                                <div class="photo-miniature loading">
                                                    <a href="#"><img src="images/mini-ad-img2.jpg"></a>
                                                    <div class="meter nostripes">
                                                        <span style="width: 75%"></span>
                                                    </div>
                                                </div>
                                                <div class="photo-miniature loading">
                                                    <a href="#"><img src="images/mini-ad-img3.jpg"></a>
                                                    <div class="meter nostripes">
                                                        <span style="width: 99%"></span>
                                                    </div>
                                                </div>
                                                <div class="photo-miniature ready">
                                                    <a href="#" class="active"><img src="images/mini-ad-img4.jpg"></a>
                                                    <div class="meter nostripes">
                                                        <span style="width: 100%"></span>
                                                    </div>
                                                </div>
                                                <div class="photo-miniature ready">
                                                    <a href="#"><img src="images/mini-ad-img5.jpg"></a>
                                                    <div class="meter nostripes">
                                                        <span style="width: 100%"></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="photo-full-size-inner">
                                                <label>Установите миниатюры для выбранных фотографий:</label>
                                                <div class="photo-full-size">
                                                    <div class="full-size-img">
                                                        <img src="images/ad-img4.jpg">
                                                        <div id="follow-miniature">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset class="choose-city">
                                        <label>Укажите города, в которых актуальна ваша услуга:</label>
                                        <div class="list-of-cities">
                                            <button>Добавить город +</button><span class="chosen-city">Владивосток<a href="" class="remove-city"><i class="fa fa-times-circle"></i></a></span><span class="chosen-city">Артем<a href="" class="remove-city"><i class="fa fa-times-circle"></i></a></span><span class="chosen-city">Находка<a href="" class="remove-city"><i class="fa fa-times-circle"></i></a></span>
                                        </div>
                                    </fieldset>
                                    <fieldset class="ad-price">
                                        <label>Укажите стоимость вашей услуги<span class="gfield_required">*</span></label>
                                        <div class="price-no-range">
                                            <input type="text">
                                        </div>
                                        <div class="price-range">
                                            <label>от</label>
                                            <input type="text">
                                            <label> - до</label>
                                            <input type="text">
                                        </div>
                                        <div class="check-price-range">
                                            <input id="check-price-range" type="checkbox">
                                            <label>Диапазон цен</label>
                                        </div>
                                        <!--  <div class="gfield_description validation_message">Это поле обязательно для зaполнения</div> -->
                                    </fieldset>
                                    <fieldset class="full-descr">
                                        <label>Текст вашего объявления<span class="gfield_required">*</span></label>
                                        <textarea rows="7"></textarea>
                                        <!-- <div class="gfield_description validation_message">Это поле обязательно для зaполнения</div> -->
                                    </fieldset>
                                    <fieldset class="seller-contacts-inner">
                                        <label>Укажите свои контактные данные<span class="gfield_required">*</span></label>
                                        <div class="seller-contacts">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-6">
                                                    <div class="h5 title">Личные данные:</div>
                                                    <fieldset class="login-username">
                                                        <label><i class="fa fa-user"></i>ФИО</label>
                                                        <input type="text" name="log" class="input">
                                                        <div class="gfield_description validation_message">Это поле должно быть заполнено</div>
                                                    </fieldset>
                                                    <fieldset class="email-username">
                                                        <label><i class="fa fa-envelope"></i>Email</label>
                                                        <input type="text" name="email" class="input">
                                                    </fieldset>
                                                    <fieldset class="phone-username">
                                                        <label><i class="fa fa-phone"></i>Телефон</label>
                                                        <input type="text" name="phone" class="input">
                                                    </fieldset>
                                                    <fieldset class="skype-username">
                                                        <label><i class="fa fa-skype"></i>Skype</label>
                                                        <input type="text" name="skype" class="input">
                                                    </fieldset>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-6">
                                                    <div class="social-networks">
                                                        <div class="h5 title">Cоцсети:</div>
                                                        <fieldset class="vk-username">
                                                            <input type="checkbox" name="vk" class="check-box-with-input" checked="checked">
                                                            <label><i class="fa fa-vk"></i>Вконтакте</label>
                                                            <input type="text" name="link" class="hide-input visible" placeholder="Вставьте ссылку на вашу страницу &quot;Вконтакте&quot;">
                                                        </fieldset>
                                                        <fieldset class="ok-username">
                                                            <input type="checkbox" name="ok" class="check-box-with-input">
                                                            <label><i class="fa fa-odnoklassniki"></i>Одноклассники</label>
                                                            <input type="text" name="link" class="hide-input" placeholder="Вставьте ссылку на вашу страницу &quot;Одноклассники&quot;">
                                                        </fieldset>
                                                        <fieldset class="fb-username">
                                                            <input type="checkbox" name="fb" class="check-box-with-input">
                                                            <label><i class="fa fa-facebook"></i>Facebook</label>
                                                            <input type="text" name="link" class="hide-input" placeholder="Вставьте ссылку на вашу страницу &quot;Facebook&quot;">
                                                        </fieldset>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset class="create-ad-button">
                                        <button type="submit">Создать объявление</button>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <!-- #content -->
    </div>
    <!-- #primary -->
</div>
HTML;
