<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 11.02.2016
 * Time: 21:26
 */

require_once __DIR__.'/../../../libs/components.php';
$Component = new Components();

$account = '/account';

$title = '';
$isMain = false;
switch($T['module']['section']){
    case 'ads':
        $title = 'Объявления пользователя';
        break;

    case 'create-ad':
        $title = 'Создать объявление';
        break;

    case 'messages':
        $title = 'Объявления пользователя';
        break;

    case 'pay-service':
        $title = 'Пополнение счета';
        break;

    default:
        $title = 'Личный кабинет';
        $isMain = true;
        break;
}

$leftMenu = [
    [
        'href'=>$account,
        'ico'=>'fa fa-key',
        'class'=>'',
        'title'=>'Личный кабинет',
    ],
    [
        'href'=>$account.'/ads',
        'ico'=>'fa fa-list-alt',
        'class'=>'',
        'title'=>'Мои объявления',
    ],
    [
        'href'=>$account.'/messages',
        'ico'=>'fa fa-comment',
        'class'=>'comment',
        'title'=>'Сообщения',
        'isMsgCounter'=>true,
    ],
];

$headMenu = [
    [
        'href'=>$account.'/ads',
        'ico'=>'fa fa-list-alt',
        'class'=>'',
        'title'=>'Мои объявления',
    ],
    [
        'href'=>$account.'/messages',
        'ico'=>'fa fa-comment',
        'class'=>'messages',
        'title'=>'Сообщения',
        'isMsgCounter'=>true,
    ],
    [
        'href'=>$account,
        'ico'=>'fa fa-user',
        'class'=>'',
        'title'=>'Личные данные',
    ],
];

return [
    'breams'=>$Component->LkBreadcrums($title, $isMain),
    'pay'=>$account.'/pay-service',
    'account'=>$account,
    'info'=>$Component->LkUserInfo($account),
    'leftMenu'=>$Component->LkLeftMenu($leftMenu),
    'headMenu'=>$Component->HeadLkMenu($headMenu),
];