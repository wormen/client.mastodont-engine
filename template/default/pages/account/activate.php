<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 14.02.2016
 * Time: 20:42
 * -------------------------------------
 * Страница активации профиля
 */

$lang = $GLOBALS['lang'];

$title = 'Активация профиля';
\Core\Tpl::setTitle($title);

$LS = \Mastodont\Api\v1\Users::Activate();
$state = $LS != 'OK' ? 'error' : 'success';

$goURL = "//{$_SERVER['HTTP_HOST']}/{$T['vars']['modules']['account']['linkModule']}";

$resultText = '';

//состояния активации профиля
$iText = '';
switch($LS){
    case 'OK':
        $resultText .= "<meta http-equiv=\"refresh\" content=\"5;{$goURL}\">";
        break;

    case 'INACTIVE_LINK':
        break;

    case 'IS_ACTIVE_PROFILE':
        break;
}


$resultText .= <<<HTML

<div id="main" class="site-main">
    <div id="primary" class="content-area">
        <div id="content" class="homepage-content" role="main">

            <section class="breadcrums">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <a href="/">Главная</a>
                            <span class="delimiter">/</span>
                            <span>{$title}</span>
                        </div>
                    </div>
                </div>
            </section>

            <section id="jobify_widget_jobs-3" class="homepage-widget jobify_widget_jobs">
                <div class="container">

                    <div class="col-xs-12">
                        <div class="dop-margin-top40"></div>
                        <div class="page-notify {$state}">
                            <p>{$lang['account']['activateState'][$LS]}</p>
                        </div>

                        <div class="dop-margin-top40"></div>

                        <div class="up-layer-notify success"><p>{$lang['account']['activateText'][$LS]}</p></div>
                        <div class="dop-margin-top40"></div>
                    </div>

                </div>
            </section>
        </div>
        <!-- #content -->
    </div>
    <!-- #primary -->
</div>
HTML;

return$resultText;