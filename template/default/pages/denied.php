<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 05.02.2016
 * Time: 13:52
 * -------------------------------------
 * Заглушка для сайта. когда сайт закрыт
 */


return $T['vars']['site']['close_mes'];