<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 09.02.2016
 * Time: 11:15
 */

$title = 'Помощь (Вопрос-ответ)';
\Core\Tpl::setTitle($title);

$list = $qc = '';
foreach($T['content'] as $F){

    $cid = uniqid('category-of-question1-');
    $Plist = '';

    foreach($F['list'] as $p){
        $pid = uniqid();

        $Plist .= <<<HTML
<article class="accordion one-question">
    <div class="accordion-section">
        <header>
            <a class="accordion-section-title" href="#accordion-{$pid}"><i class="fa fa-question-circle"></i>{$p['name']}</a>
        </header>
        <div id="accordion-{$pid}" class="accordion-section-content">{$p['description']}</div>
    </div>
</article>
HTML;
    }


    $list .= <<<HTML
<div id="{$cid}" class="category-of-question">
    <div class="h3">{$F['name']}</div>
    {$Plist}
</div>
HTML;

    $qc .= '<li><a href="#'.$cid.'">'.$F['name'].'</a></li>';
}


return <<<HTML
<div id="main" class="site-main">
    <div id="primary" class="content-area">
        <div id="content" class="homepage-content" role="main">

            <section class="breadcrums">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <a href="">Главная</a>
                            <span class="delimiter">/</span>
                            <span>{$title}</span>
                        </div>
                    </div>
                </div>
            </section>

            <section id="jobify_widget_jobs-3" class="homepage-widget jobify_widget_jobs">
                <div class="container">
                    <div class="row">

                        <div class="recent-jobs has-spotlight faq-inner col-lg-8 col-md-7 col-sm-12">
                            <div class="h2 title">{$title}</div>
                            {$list}
                        </div>

                        <aside class="sidebar job-spotlight col-lg-4 col-md-5 col-sm-12">
                            <div class="faq-category-sidebar">
                                <div class="h3">Категории вопросов:</div>
                                <ul>{$qc}</ul>
                            </div>

                            <div class="popular-ads">
                                <div class="h3">Популярные предложения:</div>
                                <article class="post ad-post">
                                    <header class="entry-header">
                                        <figure>
                                            <a href="" rel="bookmark" class="featured-image"><img src="https://demo.astoundify.com/jobify-darker/wp-content/uploads/sites/16/2013/07/Stocksy_txp15d4e891770000_Medium_28012-400x200.jpg" class="attachment-content-grid size-content-grid wp-post-image" alt="Stocksy_txp15d4e891770000_Medium_28012"></a>
                                        </figure>
                                        <div class="h4">
                                            <a href="" rel="bookmark">Очень длинный заголовок объявления</a>
                                        </div>
                                    </header>
                                    <!-- .entry-header -->
                                    <div class="entry">
                                        <div class="entry-summary">
                                            <p>Разнообразный и богатый опыт укрепление и развитие структуры представляет собой интересный эксперимент проверки дальнейших направлений развития. </p>
                                        </div>
                                    </div>
                                    <footer>
                                        <div class="ad-rating">
                                            Рейтинг: <span class="positive-rating">66.7</span> из 100
                                        </div>
                                    </footer>
                                </article>
                                <article class="post ad-post">
                                    <header class="entry-header">
                                        <figure>
                                            <a href="" rel="bookmark" class="featured-image"><img src="https://demo.astoundify.com/jobify-darker/wp-content/uploads/sites/16/2013/07/Stocksy_txp15d4e891770000_Medium_28012-400x200.jpg" class="attachment-content-grid size-content-grid wp-post-image" alt="Stocksy_txp15d4e891770000_Medium_28012"></a>
                                        </figure>
                                        <div class="h4">
                                            <a href="" rel="bookmark">Очень длинный заголовок объявления</a>
                                        </div>
                                    </header>
                                    <!-- .entry-header -->
                                    <div class="entry">
                                        <div class="entry-summary">
                                            <p>Разнообразный и богатый опыт укрепление и развитие структуры представляет собой интересный эксперимент проверки дальнейших направлений развития. </p>
                                        </div>
                                    </div>
                                    <footer>
                                        <div class="ad-rating">
                                            Рейтинг: <span class="null-rating">0</span> из 100
                                        </div>
                                    </footer>
                                </article>
                            </div>

                            <div class="sidebar-articles">
                                <div class="h3">Статьи:</div>
                                <article class="announcement">
                                    <header>
                                        <a href="" class="h3">Очень длинный заголовк статьи</a>
                                    </header>
                                    <p class="descr">
                                        Разнообразный и богатый опыт дальнейшее развитие различных форм деятельности в значительной степени обуславливает создание направлений прогрессивного развития...
                                    </p>
                                </article>
                                <article class="announcement">
                                    <header>
                                        <a href="" class="h3">Очень длинный заголовк статьи</a>
                                    </header>
                                    <p class="descr">
                                        Разнообразный и богатый опыт дальнейшее развитие различных форм деятельности в значительной степени обуславливает создание направлений прогрессивного развития...
                                    </p>
                                </article>
                                <a href="" class="more-info">Перейти ко всем статьям <i class="fa fa-angle-double-right"></i></a>
                            </div>

                        </aside>
                    </div>
                </div>
            </section>
        </div>
        <!-- #content -->
    </div>
    <!-- #primary -->
</div>
HTML;
