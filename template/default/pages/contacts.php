<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 12.02.2016
 * Time: 19:42
 */

$title = 'Контакты';
\Core\Tpl::setTitle($title);
$data = $T['vars']['modules']['contacts']['data'];

$blocks = '';
foreach($data as $d){
    if(!$d['enabled']) continue;

    $list = '<ul>';

    usort($d['list'], function($l, $r) {
        return strcmp($l["pos"], $r["pos"]);
    });

    foreach($d['list'] as $l){
        $ico = $href = '';
        switch($l['type']){

            case 'fax':
                $ico = 'fa fa-fax';
                $href = '#';
                break;

            case 'phone':
                $ico = 'fa fa-phone';
                $href = 'tel:'.$l['value'];
                break;

            case 'email':
                $ico = 'fa fa-envelope';
                $href = 'mailto:'.$l['value'];
                break;

            case 'address':
                $ico = 'fa fa-map-marker';
                $href = '#';
                break;

        }

        $list .= '<li class="'.$l['type'].'">
                    <a href="'.$href.'"><i class="'.$ico.'"></i>'.$l['value'].'</a>
                </li>';
    }
    $list .= '</ul>';

    $blocks .= '<div class="contact-card">
                <div class="h3">'.$d['nameBlock'].'</div>
                <div class="contact-ul">'.$list.'</div>
            </div>';
}



return <<<HTML
<div id="main" class="site-main">
    <div id="primary" class="content-area">
        <div id="content" class="homepage-content" role="main">

            <header class="page-header">
                <h1 class="page-title">{$title}</h1>
            </header>

            <section class="breadcrums">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <a href="/">Главная</a>
                            <span class="delimiter">/</span>
                            <span>{$title}</span>
                        </div>
                    </div>
                </div>
            </section>

            <section id="jobify_widget_jobs-3" class="homepage-widget jobify_widget_jobs">
                <div class="container">
                    <div class="row contacts">

                        <div class="col-xs-12 col-sm-6 col-md-6 contact-card-inner">
                            {$blocks}
                        </div>

                        <div class="col-xs-12 col-sm-6 col-md-6 contact-form-inner gform_wrapper">
                            <div class="h3">Есть вопросы? Напишите нам:</div>
                            <form method="post">
                                <div class="validation_error">Для отправки сообщения должны быть заполнены все поля</div>
                                <div class="gform_body">
                                    <fieldset>
                                        <label class="gfield_label" for="input_3_1">Ваше имя<span class="gfield_required">*</span></label>
                                        <div class="ginput_container ginput_container_name">
                                            <input name="input_1" id="input_3_1" type="text" value="" class="medium" tabindex="1">
                                        </div>
                                        <div class="gfield_description validation_message">Это поле обязательно для заполнения.</div>
                                    </fieldset>

                                    <fieldset>
                                        <label class="gfield_label" for="input_3_2">Email<span class="gfield_required">*</span></label>
                                        <div class="ginput_container ginput_container_email">
                                            <input name="input_2" id="input_3_2" type="email" value="" class="medium" tabindex="2">
                                        </div>
                                    </fieldset>
                                    <fieldset class="field-select">
                                         <label class="gfield_label" for="input_3_2">Тема сообщения<span class="gfield_required">*</span></label>
                                        <select name='search_categories' id='search_categories' class='postform'>
                                            <option value='0'>Выберите тему сообщения</option>
                                            <option class="level-0" value="59">Тема сообщения</option>
                                            <option class="level-0" value="53">Тема сообщения</option>
                                            <option class="level-0" value="52">Тема сообщения</option>
                                            <option class="level-0" value="51">Тема сообщения</option>
                                        </select>
                                    </fieldset>
                                    <fieldset>
                                        <label class="gfield_label" for="input_3_3">Текст сообщения<span class="gfield_required">*</span></label>
                                        <div class="ginput_container ginput_container_textarea">
                                            <textarea name="input_3" id="input_3_3" class="textarea small" tabindex="3" rows="4"></textarea>
                                        </div>
                                    </fieldset>
                                    <fieldset>
                                        <button type="submit">Отправить
                                        </button>
                                    </fieldset>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </section>
        </div>
        <!-- #content -->
    </div>
    <!-- #primary -->
</div>
HTML;
