<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 09.02.2016
 * Time: 13:15
 */


$S = $T['content'];
$date = date('d.m.Y', $S['modify']);
\Core\Tpl::setTitle(strlen($S['title'])>0 ? $S['title'] : $S['name']);

$pluso = include __DIR__.'/../blocks/pluso.php';


return <<<HTML
<div id="main" class="site-main">
    <div id="primary" class="content-area">
        <div id="content" class="homepage-content" role="main">

            <section class="breadcrums">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <a href="/">Главная</a>
                            <span class="delimiter">/</span>
                            <span>{$S['name']}</span>
                        </div>
                    </div>
                </div>
            </section>

            <section id="jobify_widget_jobs-3" class="homepage-widget jobify_widget_jobs">
                <div class="container">
                    <div class="row">

                        <div class="recent-jobs has-spotlight articles-inner col-lg-8 col-md-7 col-sm-12">
                            <article class="post full-post">
                                <div class="entry">

                                    <header>
                                        <div class="h2 title">
                                            <i class="fa fa-bookmark"></i> {$S['name']}
                                        </div>
                                        <div class="post-date">
                                            <span>{$date}</span>
                                        </div>
                                    </header>

                                    <div class="entry-summary">{$S['description']}</div>

                                    <footer>
                                        <div class="share">{$pluso}</div>
                                    </footer>
                                </div>
                            </article>

                            <div id="ad-comments" class="ad-comments">
                                <div class="h3">
                                    <i class="fa fa-comments"></i> Комментарии:
                                </div>
                                <div class="ad-comment-form">
                                    <form method="post" id="comment-form" action="">
                                        <div class="row form-level1">
                                            <div class="col-xs-12 col-sm-6 col-md-6">
                                                <fieldset>
                                                    <label>Ваше имя</label>
                                                    <input type="text" name="name">
                                                </fieldset>
                                            </div>
                                            <div class="col-xs-12 col-sm-6 col-md-6">
                                                <fieldset>
                                                    <label>Email</label>
                                                    <input type="text" name="name">
                                                </fieldset>
                                            </div>
                                        </div>
                                        <div class="row form-level2">
                                            <div class="col-md-12">
                                                <label>Комментарий</label>
                                                <textarea rows="3"></textarea>
                                                <button type="submit" class="send-comment-button">
                                                    Отправить комментарий
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="all-comments">
                                    <div class="comment-inner">
                                        <article>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-3 col-md-4 col-lg-3">
                                                    <div class="full-login-user">
                                                        <a class="h5" href=""><span class="user-login"><i class="fa fa-user"></i> Toto1980</span></a>
                                                    </div>
                                                    <div class="full-name-user">
                                                        Егор Цыганов
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-9 col-md-8 col-lg-9">
                                                    <div class="user-message">
                                                        <p>Товарищи! рамки и место обучения кадров представляет собой интересный эксперимент проверки дальнейших направлений развития. Разнообразный и богатый опыт дальнейшее развитие различных форм деятельности влечет за собой процесс внедрения и модернизации новых предложений.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </article>
                                    </div>
                                    <div class="comment-inner">
                                        <article>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-3 col-md-4 col-lg-3">
                                                    <div class="full-name-user">
                                                        Валентина Тихарева
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-9 col-md-8 col-lg-9">
                                                    <div class="user-message">
                                                        <p>Все очень плохо!</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </article>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <aside class="sidebar job-spotlight col-lg-4 col-md-5 col-sm-12">
                            <div class="popular-ads">
                                <div class="h3">Популярные предложения:</div>
                                <article class="post ad-post">
                                    <header class="entry-header">
                                        <figure>
                                            <a href="" rel="bookmark" class="featured-image"><img src="https://demo.astoundify.com/jobify-darker/wp-content/uploads/sites/16/2013/07/Stocksy_txp15d4e891770000_Medium_28012-400x200.jpg" class="attachment-content-grid size-content-grid wp-post-image" alt="Stocksy_txp15d4e891770000_Medium_28012"></a>
                                        </figure>
                                        <div class="h4">
                                            <a href="" rel="bookmark">Очень длинный заголовок объявления</a>
                                        </div>
                                    </header>
                                    <!-- .entry-header -->
                                    <div class="entry">
                                        <div class="entry-summary">
                                            <p>Разнообразный и богатый опыт укрепление и развитие структуры представляет собой интересный эксперимент проверки дальнейших направлений развития. </p>
                                        </div>
                                    </div>
                                    <footer>
                                        <div class="ad-rating">
                                            Рейтинг: <span class="positive-rating">66.7</span> из 100
                                        </div>
                                    </footer>
                                </article>
                                <article class="post ad-post">
                                    <header class="entry-header">
                                        <figure>
                                            <a href="" rel="bookmark" class="featured-image"><img src="https://demo.astoundify.com/jobify-darker/wp-content/uploads/sites/16/2013/07/Stocksy_txp15d4e891770000_Medium_28012-400x200.jpg" class="attachment-content-grid size-content-grid wp-post-image" alt="Stocksy_txp15d4e891770000_Medium_28012"></a>
                                        </figure>
                                        <div class="h4">
                                            <a href="" rel="bookmark">Очень длинный заголовок объявления</a>
                                        </div>
                                    </header>
                                    <!-- .entry-header -->
                                    <div class="entry">
                                        <div class="entry-summary">
                                            <p>Разнообразный и богатый опыт укрепление и развитие структуры представляет собой интересный эксперимент проверки дальнейших направлений развития. </p>
                                        </div>
                                    </div>
                                    <footer>
                                        <div class="ad-rating">
                                            Рейтинг: <span class="null-rating">0</span> из 100
                                        </div>
                                    </footer>
                                </article>
                            </div>
                            <div class="sidebar-articles">
                                <div class="h3">Статьи:</div>
                                <article class="announcement">
                                    <header>
                                        <a href="" class="h3">Очень длинный заголовк статьи</a>
                                    </header>
                                    <p class="descr">
                                        Разнообразный и богатый опыт дальнейшее развитие различных форм деятельности в значительной степени обуславливает создание направлений прогрессивного развития...
                                    </p>
                                </article>
                                <article class="announcement">
                                    <header>
                                        <a href="" class="h3">Очень длинный заголовк статьи</a>
                                    </header>
                                    <p class="descr">
                                        Разнообразный и богатый опыт дальнейшее развитие различных форм деятельности в значительной степени обуславливает создание направлений прогрессивного развития...
                                    </p>
                                </article>
                                <a href="" class="more-info">Перейти ко всем статьям <i class="fa fa-angle-double-right"></i></a>
                            </div>
                        </aside>

                    </div>
                </div>
            </section>
        </div>
        <!-- #content -->
    </div>
    <!-- #primary -->
</div>
HTML;
