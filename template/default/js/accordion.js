$(document).ready(function() {
	function close_accordion_section() {
		$('.accordion-section-title').removeClass('active');
		$('.accordion-section-content').slideUp(300).removeClass('open');
	}

	$('.accordion-section-title').on('click', function(e) {
		e.preventDefault();

		// Grab current anchor value
		var currentAttrValue = $(this).attr('href');

		if($(e.target).is('.active')) {
			close_accordion_section();
		}else {
			close_accordion_section();

			// Add active class to section title
			$(this).addClass('active');
			// Open up the hidden content panel
			$(currentAttrValue).slideDown(300).addClass('open');
		}

	});
});