/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: olegbogdanov86@gmail.com
 ------------------------------------------
 10.02.2016
 */

$('#loginform .has-account a').on('click', function(e){
    $('.login-modal-inner').css({display: 'none'});
    $('.forget-pass-inner').css({display: 'block'});
    e.preventDefault();
});

$('#lostpass .has-account a').on('click', function(e){
    $('.login-modal-inner').css({display: 'block'});
    $('.forget-pass-inner').css({display: 'none'});
    e.preventDefault();
});

$('.mobile-register-link').on('click', function(e){
    $('.login-modal-inner').css({display: 'none'});
    $('.forget-pass-inner').css({display: 'none'});
    $('.registrate-modal-inner').css({display: 'block'});
    e.preventDefault();
});

//todo чеаем регион, открываем модалку
//todo выход по API

$('#choose-city-in').on('click', function(){
    Geo.onLoad();
});

if($('.account')){
    $(document).ready(function() {
        $('#other-sum-form-open').click(function() {
            $(".other-sum-form").slideDown("slow");
            $('#other-sum-form-open').addClass('no-visible');
            $('.select-sum .sum-block').removeClass('active');
        });


        $('.select-sum .sum-block').on('click', function(){
            $('.select-sum .sum-block').removeClass('active');
            $(this).addClass('active');
        })

    });
}
