<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 09.02.2016
 * Time: 12:29
 */

return <<<HTML
<div id="choose-city-in-wrap" class="modal-login modal" rel="geo">

    <div class="modal-title">
        <div class=" row">
            <div class="col-sm-6 col-md-4">
                Выбор города
            </div>
            <div class="col-sm-6 col-md-8">
                <form name="searchcity" id="search-city-form" action="" method="post">
                    <input type="text" name="search" id="search-city" class="input" placeholder="Название города" v-model="filterCity">
                    <button id="remove-text"><i class="fa fa-times-circle"></i></button>
                </form>
            </div>
        </div>
    </div>

    <div class="geo-inner">
        <div class="spinner big" v-show="isLoading" style="display: none;"></div>


        <div class="row">
            <div class="col-md-4 federals-inner">
                <a href="#" class="all-russia" @click="allCountrySet(\$event)">
                    <img src="{$tpl['url']}/images/rus-flag.png"> Вся Россия
                </a>
                <div class="h3">
                    Федеральный округ:
                </div>
                <ul class="all-federals">

                    <li v-for="d in districts" :class="{active: d.isActive}" @click="onActiveDistrict(\$event, \$index, d.id)">
                        <a class="federal-title" href="#">
                            {{d.title}}
                            <i class="fa fa-check" v-if="d.isActive"></i>
                        </a>
                    </li>
                </ul>
            </div>

            <div class="col-md-4 regions-inner">
                <button class="modal-back">
                    <i class="fa fa-arrow-circle-o-left"></i>Назад
                </button>
                <div class="h3">
                    Регион:
                </div>

                <ul class="all-regions">

                    <li v-for="r in regions" :class="{active: r.isActive}" @click="onActiveRegion(\$event, \$index, r.id)">
                        <a class="region-title" href="#">
                            {{r.title}}
                            <i class="fa fa-check" v-if="r.isActive"></i>
                        </a>
                    </li>

                </ul>
            </div>

            <div class="col-md-4 cities-inner">
                <button class="modal-back">
                    <i class="fa fa-arrow-circle-o-left"></i>Назад
                </button>
                <div class="all-region-cities">
                    <a href="#" @click="allCityRegion(\$event)">
                        <i class="fa fa-map-pin"></i> Все города региона
                        </a>
                </div>

                <ul class="popular-cities">
                    <li class="strong active">
                        <a href="">Владивосток</a> <span class="amount-ads">7896</span><i class="fa fa-check"></i>
                    </li>
                    <li class="strong">
                        <a href="">Уссурийск</a> <span class="amount-ads">1996</span>
                    </li>
                    <li class="strong">
                        <a href="">Находка</a> <span class="amount-ads">896</span>
                    </li>
                </ul>

                <ul class="all-cities">

                    <li v-for="c in cityList | filterBy filterCity in 'title'" :class="{active: c.isActive}" @click="onActiveCity(\$event, \$index, c.id)">
                        <a class="city-title" href="#">{{c.title}}</a>
                        <span class="amount-ads">1234</span>
                        <i class="fa fa-check" v-if="c.isActive"></i>
                    </li>

                </ul>

            </div>
        </div>
    </div>
    <button title="Close (Esc)" type="button" class="mfp-close">×</button>
</div>
HTML;
