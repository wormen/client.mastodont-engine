<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 09.02.2016
 * Time: 12:26
 */

return [
    'login'=> include 'login.php',
    'city'=> include 'city.php',
    'advertisement'=> include 'advertisement.php',
];