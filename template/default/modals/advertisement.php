<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 09.02.2016
 * Time: 12:28
 */

return <<<HTML
<div id="add-advertisement-wrap" class="modal-login modal" rel="auth">

    <div class="h4"><span class="star">*</span> Для добавления объявления необходима регистрация. Если вы уже зарегистрированы, то войдите на сайт.</div>
    <div class="row">

        <div class="col-xs-12 col-sm-6 col-md-6 login-modal-inner">
            <div class="h2 modal-title">Вход</div>
            <div id="loginform" name="loginform" class="manager-form">
                <fieldset class="login-username">
                    <label for="user_login">Логин или email</label>
                    <input type="text" name="log" id="user_login" class="input" value="" size="20" />
                </fieldset>

                <fieldset class="login-password">
                    <label for="user_pass">Пароль</label>
                    <input type="password" name="pwd" id="user_pass" class="input" value="" size="20" />
                </fieldset>

                <fieldset class="has-account"><i class="icon-help-circled"></i> <a href="">Забыли пароль?</a>
                    <button type="submit" name="wp-submit" class="button-primary wp-submit">Войти</button>
                </fieldset>
                <p class="mobile-register-link">Впервые на сайте? <a href="">Зарегистрируйтесь!</a></p>
            </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-6 forget-pass-inner" style="display:none;">
            <div class="h2 modal-title">Восстановление пароля</div>
            <div id="lostpass" name="loginform" class="manager-form">
                <fieldset class="login-username">
                    <label for="user_login">Логин или email</label>
                    <input type="text" name="log" id="user_login" class="input" value="" size="20" />
                </fieldset>

                <fieldset class="has-account"><a href="#">Войти</a>
                    <button type="submit" name="wp-submit" class="button-primary wp-submit">Восстановить</button>
                </fieldset>
            </div>
        </div>

        <div class="col-xs-12 col-sm-6  col-md-6 registrate-modal-inner">
            <div class="h2 modal-title">Регистрация</div>
            <form action="" method="post" id="register-form" name="register-form" class="manager-form">
                <fieldset>
                    <label for="">Логин</label>
                    <input type="text" class="input-text" name="nicname" id="nicname" placeholder="" value="" maxlength="" required />
                </fieldset>
                <fieldset>
                    <label for="">Email</label>
                    <input type="text" class="input-text" name="email" id="email" placeholder="email@example.com" value="" maxlength="" required />
                </fieldset>
                <fieldset>
                    <label for="">Пароль</label>
                    <input type="password" class="input-text" name="password" id="password" placeholder="" value="" maxlength="" required />
                </fieldset>
                <fieldset>
                    <label for="">Повторите пароль</label>
                    <input type="password" class="input-text" name="repeat-password" id="repeat-password" placeholder="" value="" maxlength="" required />
                </fieldset>
                <fieldset class="conditions">
                    <input name="conditions" type="checkbox" id="conditions" value="forever" checked="checked">
                    <label>Я согласен с <a href="">условиями</a></label>
                </fieldset>
                <fieldset class="has-account">
                    <button type="submit" name="wp-submit" class="button-primary wp-submit">Зарегистрироваться</button>
                </fieldset>
            </form>
        </div>

    </div>
</div>
HTML;
