<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 09.02.2016
 * Time: 12:25
 */

$lang = $GLOBALS['lang'];

return <<<HTML
<div id="login-modal-wrap" class="modal-login modal" rel="auth">


    <div class="row">

        <div class="col-xs-12 col-sm-6 col-md-6 login-modal-inner">
            <div class="h2 modal-title">Вход</div>
            <div id="loginform" name="loginform" class="manager-form">
                <div class="spinner big" v-show="isAuthLoading" style="display: none;"></div>

                <fieldset class="login-username">
                    <label for="user_login">{$lang['account']['login']} или email</label>
                    <input type="text" id="user_login" class="input" size="20" v-model="user.login" />
                </fieldset>

                <fieldset class="login-password">
                    <label for="user_pass">{$lang['account']['password']}</label>
                    <input type="password" id="user_pass" class="input" v-model="user.password" />
                </fieldset>

                <fieldset class="has-account"><i class="icon-help-circled"></i> <a href="#">{$lang['account']['forgotPass']}</a>
                    <button class="button-primary wp-submit" @click="onAuth()">{$lang['btns']['logIn']}</button>
                </fieldset>
                <p class="mobile-register-link">Впервые на сайте? <a href="#">Зарегистрируйтесь!</a></p>
            </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-6 forget-pass-inner" style="display:none;">
            <div class="h2 modal-title">Восстановление пароля</div>
            <div id="lostpass" name="loginform" class="manager-form">
                <div class="spinner big" v-show="isRestoreLoading" style="display: none;"></div>

                <fieldset class="login-username">
                    <label for="user_login">{$lang['account']['login']} или email</label>
                    <input type="text" name="log" id="user_login" class="input" value="" size="20" />
                </fieldset>

                <fieldset class="has-account"><a href="#">{$lang['btns']['logIn']}</a>
                    <button class="button-primary wp-submit">{$lang['btns']['repair']}</button>
                </fieldset>
            </div>
        </div>


        <div class="col-xs-12 col-sm-6  col-md-6 registrate-modal-inner">
            <div class="h2 modal-title">Регистрация</div>
            <div id="register-form" name="register-form" class="manager-form">
                <div class="spinner big" v-show="isRegisterLoading" style="display: none;"></div>

                <fieldset>
                    <label for="nicname">{$lang['account']['login']}</label>
                    <input type="text" class="input-text" id="nicname" placeholder="" maxlength="20" required v-model="reg.login" @keyup="onCheckLogin | debounce 150" />
                    <div class="gfield_description validation_message" style="display:none;" v-show="isLogin">Этот логин уже занят</div>
                    <div class="gfield_description validation_message" style="display:none;" v-show="isLoginF">{{isLoginT}}</div>
                </fieldset>

                <fieldset>
                    <label for="email">Email</label>
                    <input type="email" class="input-text" id="email" placeholder="email@example.com" required v-model="reg.email" @keyup="onCheckEmail | debounce 150" />
                    <div class="gfield_description validation_message" style="display:none;" v-show="isEmail">Этот Email уже занят</div>
                    <div class="gfield_description validation_message" style="display:none;" v-show="isEmailF">{{isEmailT}}</div>
                </fieldset>

                <fieldset>
                    <label for="">{$lang['account']['password']}</label>
                    <input type="password" class="input-text" id="password" placeholder="" required v-model="reg.password" />
                    <div class="gfield_description validation_message" style="display:none;" v-show="noPass">{{noPassT}}</div>
                </fieldset>

                <fieldset>
                    <label for="repeat-password">{$lang['account']['repeat-password']}</label>
                    <input type="password" class="input-text" id="repeat-password" placeholder="" required v-model="reg.repass" />
                    <div class="gfield_description validation_message" style="display:none;" v-show="reg.repass.length>3 && reg.password != reg.repass">Пароли не совпадают</div>
                </fieldset>

                <fieldset class="conditions">
                    <input name="conditions" type="checkbox" id="conditions" v-model="reg.iAgree">
                    <label>Я согласен с <a href="/static/Pravila-sayta">условиями</a></label>
                    <div class="gfield_description validation_message" style="display:none;" v-show="isAgree">Подтвердите ваше согласие</div>
                </fieldset>

                <div class="up-layer-notify {{regRes.isErr ? 'error' : 'success'}}" display:none; v-show="regRes.text">
                   <p>{{regRes.text}}</p>
                </div>

                <fieldset class="has-account">
                    <button class="button-primary wp-submit" @click="onRegister">Зарегистрироваться</button>
                </fieldset>
            </div>
        </div>

    </div>
</div>
HTML;
