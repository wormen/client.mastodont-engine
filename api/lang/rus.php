<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 23.01.2016
 * Time: 22:14
 */

return [
    'invalidParams'=>'Неверно указаны параметры',
    'incorrectData'=>'Некорректные данные',
    'notFound'=>'Документ не найден',
];