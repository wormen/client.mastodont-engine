<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 23.01.2016
 * Time: 21:56
 */

//$start = microtime(true);

use \Symfony\Component\HttpFoundation\Request;
use \Symfony\Component\HttpFoundation\Response;

$DS = DIRECTORY_SEPARATOR;
require_once implode($DS, [__DIR__, '..', 'config', 'constants.php']);

//----------------------------------------------------------------------------------------------------------------------

$app = new \Silex\Application();


// выставляем заголовки
$Core->Headers('api');

// проверяем пользователя в черном списке
$Security->CheckBanList();

// todo проверка локализации (доделать)
//if(!file_exists($lang)){
//    $res = new Response('This localization is not', 400);
//    $res->send();
//    die;
//}else
    $lang = $GLOBALS['lang'];


//$this->getRequest()->getHost()
try{

    //todo проверяем, доступно ли нам API

    // проверка токена
    $isNext = true;
//    if(!isset($_REQUEST['token'])){
//
////        $T = \Core\Security::DecodeToken();
////        var_dump($T);
//
////        if($T['sid'] != session_id()) die;
////        else{
////            $res = new Response(json_encode([
////                'statusCode'=>401,
////                'error'=>'Unknown token',
////            ]), 401);
////            $res->send();
////            die;
////        }
//
//    }else{
//        //todo расшафровка токена / проверка токена
//
//
////        $T = \Core\Security::DecodeToken($_REQUEST['token'], $_REQUEST['key']);
////
////        switch($T['type']){
////            case 'site':
////                if($T['sid'] != session_id()) die;
////                break;
////
////            case 'soft':
////                break;
////        }
//    }

    if(!$isNext) die;

//    $app['debug'] = true;

//----------------------------------------------------------------------------------------------------------------------
    $methodReq = mb_strtolower($_SERVER['REQUEST_METHOD'], 'UTF-8');

    //todo оставить минимальный набор роутов с максимальной эффективностью

    $app->{$methodReq}('/{version}/{module}', function (Request $req, $version, $module) use ($app, $lang) {

        $file = implode(DIRECTORY_SEPARATOR, [ModuleDir, $version, $module, 'index.php']);
        if(file_exists($file))
            return include$file;
        else{
            $res = new Response(json_encode([
                'code'=>'invalidParams',
                'statusCode'=>400,
                'error'=>$lang['invalidParams']
            ]), 400);
            $res->send();
            die;
        }
    });

    $app->{$methodReq}('/{version}/{module}/{method}', function (Request $req, $version, $module, $method) use ($app, $lang) {

        $file = implode(DIRECTORY_SEPARATOR, [ModuleDir, $version, $module, 'index.php']);
        if(file_exists($file))
            return include$file;

        else{
            $res = new Response(json_encode([
                'code'=>'invalidParams',
                'statusCode'=>400,
                'error'=>$lang['invalidParams']
            ]), 400);
            $res->send();
            die;
        }

    });

    $app->{$methodReq}('/{version}/{module}/{method}/{id}', function (Request $req, $version, $module, $method, $id) use ($app, $lang) {

        $file = implode(DIRECTORY_SEPARATOR, [ModuleDir, $version, $module, 'index.php']);
        if(file_exists($file))
            return include$file;

        else{
            $res = new Response(json_encode([
                'code'=>'invalidParams',
                'statusCode'=>400,
                'error'=>$lang['invalidParams']
            ]), 400);
            $res->send();
            die;
        }

    });

    
    
    $app->post('/fm/upload', function (Request $req) use ($app, $lang) {

        $file = implode(DIRECTORY_SEPARATOR, [ModuleDir, 'fm', 'index.php']);
        if(file_exists($file))
            return json_encode(include$file);
        else{
            $res = new Response(json_encode([
                'code'=>'invalidParams',
                'statusCode'=>400,
                'error'=>$lang['invalidParams']
            ]), 400);
            $res->send();
            die;
        }
    });


    
    
    



    //todo претенденты на удаление

    $app->post('/{version}/{module}', function (Request $req, $version, $module) use ($app, $lang){

        $file = implode(DIRECTORY_SEPARATOR, [ModuleDir, $version, $module, 'index.php']);
        if(file_exists($file))
            return include$file;
        else{
            $res = new Response(json_encode([
                'code'=>'invalidParams',
                'statusCode'=>400,
                'error'=>$lang['invalidParams']
            ]), 400);
            $res->send();
            die;
        }
    });

    $app->post('/{version}/{module}/{section}', function (Request $req, $version, $module, $section) use ($app, $lang){

        $file = implode(DIRECTORY_SEPARATOR, [ModuleDir, $version, $module, $section, 'index.php']);
        if(file_exists($file))
            return include$file;
        else{
            $res = new Response(json_encode([
                'code'=>'invalidParams',
                'statusCode'=>400,
                'error'=>$lang['invalidParams']
            ]), 400);
            $res->send();
            die;
        }
    });


    $app->put('/{version}/{module}', function (Request $req, $version, $module) use ($app, $lang){

        $file = implode(DIRECTORY_SEPARATOR, [ModuleDir, $version, $module, 'index.php']);
        if(file_exists($file))
            return include$file;
        else{
            $res = new Response(json_encode([
                'code'=>'invalidParams',
                'statusCode'=>400,
                'error'=>$lang['invalidParams']
            ]), 400);
            $res->send();
            die;
        }
    });

    $app->put('/{version}/{module}/{section}', function (Request $req, $version, $module, $section) use ($app, $lang){

        $file = implode(DIRECTORY_SEPARATOR, [ModuleDir, $version, $module, $section, 'index.php']);
        if(file_exists($file))
            return include$file;
        else{
            $res = new Response(json_encode([
                'code'=>'invalidParams',
                'statusCode'=>400,
                'error'=>$lang['invalidParams']
            ]), 400);
            $res->send();
            die;
        }
    });

    $app->delete('/{version}/{module}', function (Request $req, $version, $module) use ($app, $lang){

        $file = implode(DIRECTORY_SEPARATOR, [ModuleDir, $version, $module, 'index.php']);
        if(file_exists($file))
            return include$file;
        else{
            $res = new Response(json_encode([
                'code'=>'invalidParams',
                'statusCode'=>400,
                'error'=>$lang['invalidParams']
            ]), 400);
            $res->send();
            die;
        }
    });

    $app->delete('/{version}/{module}/{section}', function (Request $req, $version, $module, $section) use ($app, $lang){

        $file = implode(DIRECTORY_SEPARATOR, [ModuleDir, $version, $module, $section, 'index.php']);
        if(file_exists($file))
            return include$file;
        else{
            $res = new Response(json_encode([
                'code'=>'invalidParams',
                'statusCode'=>400,
                'error'=>$lang['invalidParams']
            ]), 400);
            $res->send();
            die;
        }
    });






    $app->run();

}catch(Exception $e){
    if($app['debug']) var_dump($e);
    else{
        $Log->set($e, 'api');
        $res = new Response(json_encode([
            'statusCode'=>500,
            'error'=>'Fatal Error',
        ]), 401);
        $res->send();
        die;
    }

    //todo фиксируем IP для временного бана
}

//var_dump("Время выполнения скрипта в мин: ". date('i:s:u', (microtime(true) - $start)) );