
function Kladr(){
    this.token = '51dfe5d42fb2b43e3300006e';
    this.key = '86a2c2a06f1b2451a87d05512cc2c3edfdf41969';

    this.map = {
        type: 'yandex',
        position: {},
        zoom: 12
    };

    this.address = {
        region: '',
        district: '',
        city: '',
        street: '',
        dom: '',
        korp: '',
        kv: '',
        zip: '',
        companyName: ''
    };

    this.fullAddress = '';
}

Kladr.prototype.Init = function(config){
    var self = this;

    var opts = {
        // container: null,

        mapSelectorID: 'map', // ID селектор для вывода карты
        addressSelector: null,

        mapUpdate: false, // обновление карты

        region: null,
        district: null,
        city: null,
        street: null,
        building: null,
        buildingAdd: null,
        kv: null,
        zip: null,
        companyName: null,

        map: null,
        placemark: null,
        map_created: false
    };

    if(config.dom){
        opts.building = config.dom;
        delete config.dom;
    }

    if(config.korp){
        opts.buildingAdd = config.buildingAdd;
        delete config.buildingAdd;
    }

    this.config = $.extend(opts, config);

    this.tooltip  = $('.tooltip');

    // Подключение плагина для поля ввода региона
    if(this.config.region)
        this.config.region.kladr({
            token: self.config.token,
            key: self.config.key,
            type: $.kladr.type.region,
            labelFormat: self.LabelFormat,
            verify: true,
            select: function (obj) {
                self.config.region.val(obj.name + ' ' + obj.typeShort);

                self.AddressUpdate();
                self.MapUpdate();
            },
            check: function (obj) {
                if (obj) {
                    self.config.region.text(obj.name);
                    self.AddressUpdate();
                    self.MapUpdate();

                } else
                    self.ShowError(self.config.region, 'Неверно введено название региона');
            }
        });

    // Подключение плагина для поля ввода района
    if(this.config.district)
        this.config.district.kladr({
            token: self.config.token,
            key: self.config.key,
            type: $.kladr.type.district,
            labelFormat: self.LabelFormat,
            verify: true,
            select: function (obj) {
                self.config.city.kladr('parentType', $.kladr.type.district);
                self.config.city.kladr('parentId', obj.id);

                self.Log(obj);
                self.AddressUpdate();
                self.MapUpdate();
            },
            check: function (obj) {
                if (obj) {
                    self.config.district.text(obj.name);
                    self.config.district.parent().find('label').text(obj.type);
                    self.config.city.kladr('parentType', $.kladr.type.district);
                    self.config.city.kladr('parentId', obj.id);
                    self.tooltip.hide();

                    self.Log(obj);
                } else
                    self.ShowError(self.config.district, 'Неверно введено название района');
            }
        });

    // Подключение плагина для поля ввода города
    if(this.config.city)
        this.config.city.kladr({
            token: self.config.token,
            key: self.config.key,
            type: $.kladr.type.city,
            withParents: true,
            labelFormat: self.LabelFormat,
            verify: true,
            select: function (obj) {
                self.config.city.val(obj.typeShort+'. '+obj.name);

                self.config.street.kladr( 'parentType', $.kladr.type.city );
                self.config.street.kladr( 'parentId', obj.id );
                self.config.building.kladr( 'parentType', $.kladr.type.city );
                self.config.building.kladr( 'parentId', obj.id );
                if(obj.parents[0]) self.config.region.val(obj.parents[0].name+' '+obj.parents[0].typeShort);
                if(obj.parents[1]) self.config.district.val(obj.parents[1].name);

                self.config.city.parent().find('label').text(obj.type);
                self.config.street.kladr('parentType', $.kladr.type.city);
                self.config.street.kladr('parentId', obj.id);
                self.config.building.kladr('parentType', $.kladr.type.city);
                self.config.building.kladr('parentId', obj.id);

                self.AddressUpdate();
                self.MapUpdate();
                self.Log(obj);
            },
            check: function (obj) {
                if (obj) {
                    self.config.city.parent().find('label').text(obj.type);
                    self.config.street.kladr('parentType', $.kladr.type.city);
                    self.config.street.kladr('parentId', obj.id);
                    self.config.building.kladr('parentType', $.kladr.type.city);
                    self.config.building.kladr('parentId', obj.id);
                    self.Log(obj);
                }

                self.AddressUpdate();
                self.MapUpdate();
            }
        });

    // Подключение плагина для поля ввода улицы
    if(this.config.street)
        this.config.street.kladr({
            token: self.config.token,
            key: self.config.key,
            type: $.kladr.type.street,
            labelFormat: self.LabelFormat,
            verify: true,
            select: function (obj) {
                self.config.street.val(obj.typeShort+'. '+obj.name);

                self.config.street.parent().find('label').text(obj.type);
                self.config.building.kladr('parentType', $.kladr.type.street);
                self.config.building.kladr('parentId', obj.id);

                self.AddressUpdate();
                self.MapUpdate();
                self.Log(obj);
            },
            check: function (obj) {
                if (obj) {
                    self.config.street.parent().find('label').text(obj.type);
                    self.config.building.kladr('parentType', $.kladr.type.street);
                    self.config.building.kladr('parentId', obj.id);
                    self.Log(obj);
                }

                self.AddressUpdate();
                self.MapUpdate();
            }
        });

    // Подключение плагина для поля ввода номера дома
    if(this.config.building)
        this.config.building.kladr({
            token: self.config.token,
            key: self.config.key,
            type: $.kladr.type.building,
            labelFormat: self.LabelFormat,
            verify: true,
            select: function (obj) {
                self.AddressUpdate();
                self.MapUpdate();
                if(obj){
                    self.Log(obj);
                }
            },
            check: function (obj) {
                self.AddressUpdate();
                self.MapUpdate();
                if(obj)
                    self.Log(obj);
            }
        });

    // Проверка названия корпуса
    if(this.config.korp)
        this.config.korp.change(function () {
            self.AddressUpdate();
            self.MapUpdate();
        });

    this.MapUpdate();

    if(this.config.companyName)
        this.config.companyName.on('change', function(){
            self.setAddress('companyName', null, self.config.companyName.val());
        });
};

// Формирует подписи в autocomplete
Kladr.prototype.LabelFormat = function (obj, query) {
    var label = '';

    var name = obj.name.toLowerCase();
    query = query.toLowerCase();

    var start = name.indexOf(query);
    start = start > 0 ? start : 0;

    if (obj.typeShort) {
        label += '<span class="ac-s2">' + obj.typeShort + '. ' + '</span>';
    }

    if (query.length < obj.name.length) {
        label += '<span class="ac-s2">' + obj.name.substr(0, start) + '</span>';
        label += '<span class="ac-s">' + obj.name.substr(start, query.length) + '</span>';
        label += '<span class="ac-s2">' + obj.name.substr(start + query.length, obj.name.length - query.length - start) + '</span>';
    } else {
        label += '<span class="ac-s">' + obj.name + '</span>';
    }

    if (obj.parents) {
        for (var k = obj.parents.length - 1; k > -1; k--) {
            var parent = obj.parents[k];
            if (parent.name) {
                if (label) label += '<span class="ac-st">, </span>';
                label += '<span class="ac-st">' + parent.name + ' ' + parent.typeShort + '.</span>';
            }
        }
    }

    return label;
};

// Обновляет текстовое представление адреса
Kladr.prototype.AddressUpdate = function () {
    var self = this,
        obj='',
        value='',
        name = null,
        type = null;

    // Регион
    obj = this.config.region.kladr('current');
    value = $.trim(this.config.region.val());

    this.fullAddress = '';

    if(value)
        this.fullAddress = value;

    this.setAddress('region', null, value);


    // Район
    obj = this.config.district.kladr('current');
    value = $.trim(this.config.district.val());

    if(obj){
        name = obj.name;
        type = obj.typeShort + '.';

        if(obj.zip)
            this.setAddress('zip', null, obj.zip);

    } else if(value){
        name = value;
        type = 'р-он.';
    }

    if(name){
        if(this.fullAddress)
            this.fullAddress += ', ';

        this.fullAddress += type + ' ' + name;
        this.setAddress('district', type, value);
    }


    // Город
    obj = this.config.city.kladr('current');
    value = $.trim(this.config.city.val());

    if(obj){
        name = obj.name;
        type = obj.typeShort + '.';
        if(obj.zip)
            this.setAddress('zip', null, obj.zip);

        this.setAddress('city', type, name);
    } else if(value){
        name = value;
        type = 'г.';
    }

    if(name){
        if(this.fullAddress)
            this.fullAddress += ', ';

        this.fullAddress += type + ' ' + name;
    }


    // Улица
    name = null;
    type = null;
    obj = this.config.street.kladr('current');
    value = $.trim(this.config.street.val());

    if(obj){
        name = obj.name;
        type = obj.typeShort + '.';
        if(obj.zip)
            this.setAddress('zip', null, obj.zip);

        this.setAddress('street', type, name);
    } else if(value){
        name = value;
        type = 'ул.';
    }

    if(name){
        if(this.fullAddress)
            this.fullAddress += ', ';

        this.fullAddress += type + ' ' + name;
    }


    // Дом
    name = null;
    type = null;
    obj = this.config.building.kladr('current');
    value = $.trim(this.config.building.val());

    if(obj){
        name = obj.name;
        type = 'д.';
        if(obj.zip)
            this.setAddress('zip', null, obj.zip);

    } else if(value){
        name = value;
        type = 'д.';
    }

    if(name){
        if(this.fullAddress)
            this.fullAddress += ', ';

        this.fullAddress += type + ' ' + name;
        this.setAddress('dom', type, value);
    }



    // Корпус
    name = null;
    type = null;
    value = this.config.korp.val() ? $.trim(this.config.korp.val()) : '';

    if(value){
        name = value;
        type = 'корп.';
    }

    if(name){
        if(this.fullAddress)
            this.fullAddress += ', ';

        this.fullAddress += type + ' ' + name;
        this.setAddress('korp', type, value);
    }

    // Квартира
    name = null;
    type = null;
    value = this.address.kv ? $.trim(this.config.kv.val()) : '';

    if(value){
        name = value;
        type = 'кв.';
    }

    if(name){
        if(this.fullAddress)
            this.fullAddress += ', ';

        this.fullAddress += type + ' ' + name;
        this.setAddress('kv', type, value);
    }

    this.fullAddress = (this.address.zip ? this.address.zip + ', ' : '') + this.fullAddress;
    this.config.addressSelector.text(this.fullAddress);
};

// Обновляет карту
Kladr.prototype.MapUpdate = function (address) {
    var self = this;

    if(!this.config.mapUpdate) return;

    // Регион
    obj = this.config.region.kladr('current');
    value = $.trim(this.config.region.val());

    if (value)
        address = value;

    // Район
    obj = this.config.district.kladr('current');
    value = $.trim(this.config.district.val());

    if (obj) {
        name = obj.name;
        type = obj.typeShort + '.';
        if(obj.zip)
            this.setAddress('zip', null, obj.zip);

    } else if (value) {
        name = value;
        type = 'р-он.';
    }

    if (name) {
        if (address) address += ', ';
        address += type + ' ' + name;
    }

    // Город
    var name = null;
    var type = null;
    var obj = this.config.city.kladr('current');
    var value = $.trim(this.config.city.val());

    if (obj) {
        name = obj.name;
        type = obj.type + '.';

    } else if (value) {
        name = value;
        type = 'г.';
    }

    if (name) {
        if (address) address += ', ';
        address += type + ' ' + name;
        self.map.zoom = 12;
    }

    // Улица
    name = null;
    type = null;
    obj = this.config.street.kladr('current');
    value = $.trim(this.config.street.val());

    if (obj) {
        name = obj.name;
        type = obj.type + '.';

    } else if (value) {
        name = value;
        type = 'улица';
    }

    if (name) {
        if (address) address += ', ';
        address += type + ' ' + name;
        self.map.zoom = 14;
    }

    // Дом
    name = null;
    type = null;
    obj = this.config.building.kladr('current');
    value = $.trim(this.config.building.val());

    if (obj) {
        name = obj.name;
        type = 'дом';

    } else if (value) {
        name = value;
        type = 'дом';
    }

    if (name) {
        if (address) address += ', ';
        address += type + ' ' + name;
        self.map.zoom = 16;
    }

    // Корпус
    name = null;
    type = null;
    value = $.trim(this.config.korp.val());

    if (value) {
        name = value;
        type = 'корпус';
    }

    if (name) {
        if (address) address += ', ';
        address += type + ' ' + name;
        self.map.zoom = 16;
    }


    if (address && this.config.map_created) {
        try{
            var geocode = ymaps.geocode(address);
            geocode.then(function (res) {
                self.config.map.geoObjects.each(function (geoObject) {
                    self.config.map.geoObjects.remove(geoObject);
                });

                self.map.position = res.geoObjects.get(0).geometry.getCoordinates();

                self.config.placemark = new ymaps.Placemark(self.map.position, {}, {});

                self.config.map.geoObjects.add(self.config.placemark);
                self.config.map.setCenter(self.map.position, self.map.zoom);
            });
        }catch(e){
            console.error(e);
        }
    }

    this.MapGenerate();
};

Kladr.prototype.MapGenerate = function(){
    var self = this;

    try{
        ymaps.ready(function () {
            if (self.config.map_created) return;
            self.config.map_created = true;

            self.config.map = new ymaps.Map(self.config.mapSelectorID, {
                center: [55.76, 37.64],
                zoom: 12
            });

            self.config.map.controls.add('smallZoomControl', {top: 5, left: 5});
        });
    }catch(e){
        console.error(e);
    }
    
};

Kladr.prototype.setAddress = function(t, type, value){
    if(!t || !value) return;

    if(t == 'zip')
        value = parseInt(value);


    this.address[t] = (type ? type+' ' : '')+value;

    if(Account){
        Account.profile.map = this.map;
        Account.profile.address = this.address;
        Account.profile.fullAddress = this.fullAddress;
    }
};

// Обновляет лог текущего выбранного объекта
Kladr.prototype.Log = function(obj){
    // var logId = $('#id');
    if(obj && obj.id){
        // logId.find('.value').text(obj.id);
        // logId.show();
    } else {
        // logId.hide();
    }

    var okato = $('#okato');
    if(obj && obj.okato){
        // okato.find('.value').text(obj.okato);
        // okato.show();

        this.setAddress('okato', null, obj.okato);
    } else {
        // okato.hide();
    }

    // var logName = $('#name');
    // if(obj && obj.name){
    //     logName.find('.value').text(obj.name);
    //     logName.show();
    // } else {
    //     logName.hide();
    // }
    //
    // var logZip = $('#zip');
    // if(obj && obj.zip){
    //     logZip.find('.value').text(obj.zip);
    //     this.config.zip.val(obj.zip);
    //     logZip.show();
    // } else {
    //     logZip.hide();
    // }

    // var logType = $('#type');
    // if(obj && obj.type){
    //     logType.find('.value').text(obj.type);
    //     logType.show();
    // } else {
    //     logType.hide();
    // }
    //
    // var logTypeShort = $('#type_short');
    // if(obj && obj.typeShort){
    //     logTypeShort.find('.value').text(obj.typeShort);
    //     logTypeShort.show();
    // } else {
    //     logTypeShort.hide();
    // }
};

Kladr.prototype.ShowError = function(input, message){
    this.tooltip.find('span').text(message);

    console.log(message);

    var inputOffset = input.offset();
    var inputWidth  = input.outerWidth();
    var inputHeight = input.outerHeight();

    var tooltipHeight = tooltip.outerHeight();

    this.tooltip.css({
        left: (inputOffset.left + inputWidth + 10) + 'px',
        top: (inputOffset.top + (inputHeight - tooltipHeight)/2 - 1) + 'px'
    });

    this.tooltip.show();
};

// -----------------------------------
Kladr = new Kladr();