var Core =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.l = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// identity function for calling harmory imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 42);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports) {

	"use strict";
	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = assertString;
	function assertString(input) {
	  if (typeof input !== 'string') {
	    throw new TypeError('This library (validator.js) validates strings only');
	  }
	}
	module.exports = exports['default'];

/***/ },
/* 1 */
/***/ function(module, exports) {

	"use strict";
	'use strict';

	var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };

	/**
	 * Utility functions.
	 */

	var _ = exports,
	    array = [],
	    console = window.console;

	_.warn = function (msg) {
	    if (console && _.warning && (!_.config.silent || _.config.debug)) {
	        console.warn('[VueResource warn]: ' + msg);
	    }
	};

	_.error = function (msg) {
	    if (console) {
	        console.error(msg);
	    }
	};

	_.trim = function (str) {
	    return str.replace(/^\s*|\s*$/g, '');
	};

	_.toLower = function (str) {
	    return str ? str.toLowerCase() : '';
	};

	_.isArray = Array.isArray;

	_.isString = function (val) {
	    return typeof val === 'string';
	};

	_.isFunction = function (val) {
	    return typeof val === 'function';
	};

	_.isObject = function (obj) {
	    return obj !== null && (typeof obj === 'undefined' ? 'undefined' : _typeof(obj)) === 'object';
	};

	_.isPlainObject = function (obj) {
	    return _.isObject(obj) && Object.getPrototypeOf(obj) == Object.prototype;
	};

	_.options = function (fn, obj, options) {

	    options = options || {};

	    if (_.isFunction(options)) {
	        options = options.call(obj);
	    }

	    return _.merge(fn.bind({ $vm: obj, $options: options }), fn, { $options: options });
	};

	_.each = function (obj, iterator) {

	    var i, key;

	    if (typeof obj.length == 'number') {
	        for (i = 0; i < obj.length; i++) {
	            iterator.call(obj[i], obj[i], i);
	        }
	    } else if (_.isObject(obj)) {
	        for (key in obj) {
	            if (obj.hasOwnProperty(key)) {
	                iterator.call(obj[key], obj[key], key);
	            }
	        }
	    }

	    return obj;
	};

	_.defaults = function (target, source) {

	    for (var key in source) {
	        if (target[key] === undefined) {
	            target[key] = source[key];
	        }
	    }

	    return target;
	};

	_.extend = function (target) {

	    var args = array.slice.call(arguments, 1);

	    args.forEach(function (arg) {
	        merge(target, arg);
	    });

	    return target;
	};

	_.merge = function (target) {

	    var args = array.slice.call(arguments, 1);

	    args.forEach(function (arg) {
	        merge(target, arg, true);
	    });

	    return target;
	};

	function merge(target, source, deep) {
	    for (var key in source) {
	        if (deep && (_.isPlainObject(source[key]) || _.isArray(source[key]))) {
	            if (_.isPlainObject(source[key]) && !_.isPlainObject(target[key])) {
	                target[key] = {};
	            }
	            if (_.isArray(source[key]) && !_.isArray(target[key])) {
	                target[key] = [];
	            }
	            merge(target[key], source[key], deep);
	        } else if (source[key] !== undefined) {
	            target[key] = source[key];
	        }
	    }
	}

/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	/* WEBPACK VAR INJECTION */(function(global) {var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_RESULT__;"use strict";

	var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };

	/*!
	 * Vue.js v1.0.24
	 * (c) 2016 Evan You
	 * Released under the MIT License.
	 */
	!function (t, e) {
	  "object" == ( false ? "undefined" : _typeof(exports)) && "undefined" != typeof module ? module.exports = e() :  true ? !(__WEBPACK_AMD_DEFINE_FACTORY__ = (e), __WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ? (__WEBPACK_AMD_DEFINE_FACTORY__.call(exports, __webpack_require__, exports, module)) : __WEBPACK_AMD_DEFINE_FACTORY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)) : t.Vue = e();
	}(undefined, function () {
	  "use strict";
	  function t(e, n, r) {
	    if (i(e, n)) return void (e[n] = r);if (e._isVue) return void t(e._data, n, r);var s = e.__ob__;if (!s) return void (e[n] = r);if (s.convert(n, r), s.dep.notify(), s.vms) for (var o = s.vms.length; o--;) {
	      var a = s.vms[o];a._proxy(n), a._digest();
	    }return r;
	  }function e(t, e) {
	    if (i(t, e)) {
	      delete t[e];var n = t.__ob__;if (!n) return void (t._isVue && (delete t._data[e], t._digest()));if (n.dep.notify(), n.vms) for (var r = n.vms.length; r--;) {
	        var s = n.vms[r];s._unproxy(e), s._digest();
	      }
	    }
	  }function i(t, e) {
	    return Ai.call(t, e);
	  }function n(t) {
	    return Oi.test(t);
	  }function r(t) {
	    var e = (t + "").charCodeAt(0);return 36 === e || 95 === e;
	  }function s(t) {
	    return null == t ? "" : t.toString();
	  }function o(t) {
	    if ("string" != typeof t) return t;var e = Number(t);return isNaN(e) ? t : e;
	  }function a(t) {
	    return "true" === t ? !0 : "false" === t ? !1 : t;
	  }function h(t) {
	    var e = t.charCodeAt(0),
	        i = t.charCodeAt(t.length - 1);return e !== i || 34 !== e && 39 !== e ? t : t.slice(1, -1);
	  }function l(t) {
	    return t.replace(Ti, c);
	  }function c(t, e) {
	    return e ? e.toUpperCase() : "";
	  }function u(t) {
	    return t.replace(Ni, "$1-$2").toLowerCase();
	  }function f(t) {
	    return t.replace(ji, c);
	  }function p(t, e) {
	    return function (i) {
	      var n = arguments.length;return n ? n > 1 ? t.apply(e, arguments) : t.call(e, i) : t.call(e);
	    };
	  }function d(t, e) {
	    e = e || 0;for (var i = t.length - e, n = new Array(i); i--;) {
	      n[i] = t[i + e];
	    }return n;
	  }function v(t, e) {
	    for (var i = Object.keys(e), n = i.length; n--;) {
	      t[i[n]] = e[i[n]];
	    }return t;
	  }function m(t) {
	    return null !== t && "object" == (typeof t === "undefined" ? "undefined" : _typeof(t));
	  }function g(t) {
	    return Ei.call(t) === Si;
	  }function _(t, e, i, n) {
	    Object.defineProperty(t, e, { value: i, enumerable: !!n, writable: !0, configurable: !0 });
	  }function y(t, e) {
	    var i,
	        n,
	        r,
	        s,
	        o,
	        a = function h() {
	      var a = Date.now() - s;e > a && a >= 0 ? i = setTimeout(h, e - a) : (i = null, o = t.apply(r, n), i || (r = n = null));
	    };return function () {
	      return r = this, n = arguments, s = Date.now(), i || (i = setTimeout(a, e)), o;
	    };
	  }function b(t, e) {
	    for (var i = t.length; i--;) {
	      if (t[i] === e) return i;
	    }return -1;
	  }function w(t) {
	    var e = function i() {
	      return i.cancelled ? void 0 : t.apply(this, arguments);
	    };return e.cancel = function () {
	      e.cancelled = !0;
	    }, e;
	  }function C(t, e) {
	    return t == e || (m(t) && m(e) ? JSON.stringify(t) === JSON.stringify(e) : !1);
	  }function $(t) {
	    this.size = 0, this.limit = t, this.head = this.tail = void 0, this._keymap = Object.create(null);
	  }function k() {
	    var t,
	        e = Xi.slice(rn, en).trim();if (e) {
	      t = {};var i = e.match(un);t.name = i[0], i.length > 1 && (t.args = i.slice(1).map(x));
	    }t && (Yi.filters = Yi.filters || []).push(t), rn = en + 1;
	  }function x(t) {
	    if (fn.test(t)) return { value: o(t), dynamic: !1 };var e = h(t),
	        i = e === t;return { value: i ? t : e, dynamic: i };
	  }function A(t) {
	    var e = cn.get(t);if (e) return e;for (Xi = t, sn = on = !1, an = hn = ln = 0, rn = 0, Yi = {}, en = 0, nn = Xi.length; nn > en; en++) {
	      if (tn = Ki, Ki = Xi.charCodeAt(en), sn) 39 === Ki && 92 !== tn && (sn = !sn);else if (on) 34 === Ki && 92 !== tn && (on = !on);else if (124 === Ki && 124 !== Xi.charCodeAt(en + 1) && 124 !== Xi.charCodeAt(en - 1)) null == Yi.expression ? (rn = en + 1, Yi.expression = Xi.slice(0, en).trim()) : k();else switch (Ki) {case 34:
	          on = !0;break;case 39:
	          sn = !0;break;case 40:
	          ln++;break;case 41:
	          ln--;break;case 91:
	          hn++;break;case 93:
	          hn--;break;case 123:
	          an++;break;case 125:
	          an--;}
	    }return null == Yi.expression ? Yi.expression = Xi.slice(0, en).trim() : 0 !== rn && k(), cn.put(t, Yi), Yi;
	  }function O(t) {
	    return t.replace(dn, "\\$&");
	  }function T() {
	    var t = O(Cn.delimiters[0]),
	        e = O(Cn.delimiters[1]),
	        i = O(Cn.unsafeDelimiters[0]),
	        n = O(Cn.unsafeDelimiters[1]);mn = new RegExp(i + "((?:.|\\n)+?)" + n + "|" + t + "((?:.|\\n)+?)" + e, "g"), gn = new RegExp("^" + i + ".*" + n + "$"), vn = new $(1e3);
	  }function N(t) {
	    vn || T();var e = vn.get(t);if (e) return e;if (!mn.test(t)) return null;for (var i, n, r, s, o, a, h = [], l = mn.lastIndex = 0; i = mn.exec(t);) {
	      n = i.index, n > l && h.push({ value: t.slice(l, n) }), r = gn.test(i[0]), s = r ? i[1] : i[2], o = s.charCodeAt(0), a = 42 === o, s = a ? s.slice(1) : s, h.push({ tag: !0, value: s.trim(), html: r, oneTime: a }), l = n + i[0].length;
	    }return l < t.length && h.push({ value: t.slice(l) }), vn.put(t, h), h;
	  }function j(t, e) {
	    return t.length > 1 ? t.map(function (t) {
	      return E(t, e);
	    }).join("+") : E(t[0], e, !0);
	  }function E(t, e, i) {
	    return t.tag ? t.oneTime && e ? '"' + e.$eval(t.value) + '"' : S(t.value, i) : '"' + t.value + '"';
	  }function S(t, e) {
	    if (_n.test(t)) {
	      var i = A(t);return i.filters ? "this._applyFilters(" + i.expression + ",null," + JSON.stringify(i.filters) + ",false)" : "(" + t + ")";
	    }return e ? t : "(" + t + ")";
	  }function F(t, e, i, n) {
	    R(t, 1, function () {
	      e.appendChild(t);
	    }, i, n);
	  }function D(t, e, i, n) {
	    R(t, 1, function () {
	      V(t, e);
	    }, i, n);
	  }function P(t, e, i) {
	    R(t, -1, function () {
	      z(t);
	    }, e, i);
	  }function R(t, e, i, n, r) {
	    var s = t.__v_trans;if (!s || !s.hooks && !Bi || !n._isCompiled || n.$parent && !n.$parent._isCompiled) return i(), void (r && r());var o = e > 0 ? "enter" : "leave";s[o](i, r);
	  }function L(t) {
	    if ("string" == typeof t) {
	      t = document.querySelector(t);
	    }return t;
	  }function H(t) {
	    if (!t) return !1;var e = t.ownerDocument.documentElement,
	        i = t.parentNode;return e === t || e === i || !(!i || 1 !== i.nodeType || !e.contains(i));
	  }function I(t, e) {
	    var i = t.getAttribute(e);return null !== i && t.removeAttribute(e), i;
	  }function M(t, e) {
	    var i = I(t, ":" + e);return null === i && (i = I(t, "v-bind:" + e)), i;
	  }function W(t, e) {
	    return t.hasAttribute(e) || t.hasAttribute(":" + e) || t.hasAttribute("v-bind:" + e);
	  }function V(t, e) {
	    e.parentNode.insertBefore(t, e);
	  }function B(t, e) {
	    e.nextSibling ? V(t, e.nextSibling) : e.parentNode.appendChild(t);
	  }function z(t) {
	    t.parentNode.removeChild(t);
	  }function U(t, e) {
	    e.firstChild ? V(t, e.firstChild) : e.appendChild(t);
	  }function J(t, e) {
	    var i = t.parentNode;i && i.replaceChild(e, t);
	  }function q(t, e, i, n) {
	    t.addEventListener(e, i, n);
	  }function Q(t, e, i) {
	    t.removeEventListener(e, i);
	  }function G(t) {
	    var e = t.className;return "object" == (typeof e === "undefined" ? "undefined" : _typeof(e)) && (e = e.baseVal || ""), e;
	  }function Z(t, e) {
	    Hi && !/svg$/.test(t.namespaceURI) ? t.className = e : t.setAttribute("class", e);
	  }function X(t, e) {
	    if (t.classList) t.classList.add(e);else {
	      var i = " " + G(t) + " ";i.indexOf(" " + e + " ") < 0 && Z(t, (i + e).trim());
	    }
	  }function Y(t, e) {
	    if (t.classList) t.classList.remove(e);else {
	      for (var i = " " + G(t) + " ", n = " " + e + " "; i.indexOf(n) >= 0;) {
	        i = i.replace(n, " ");
	      }Z(t, i.trim());
	    }t.className || t.removeAttribute("class");
	  }function K(t, e) {
	    var i, n;if (it(t) && at(t.content) && (t = t.content), t.hasChildNodes()) for (tt(t), n = e ? document.createDocumentFragment() : document.createElement("div"); i = t.firstChild;) {
	      n.appendChild(i);
	    }return n;
	  }function tt(t) {
	    for (var e; e = t.firstChild, et(e);) {
	      t.removeChild(e);
	    }for (; e = t.lastChild, et(e);) {
	      t.removeChild(e);
	    }
	  }function et(t) {
	    return t && (3 === t.nodeType && !t.data.trim() || 8 === t.nodeType);
	  }function it(t) {
	    return t.tagName && "template" === t.tagName.toLowerCase();
	  }function nt(t, e) {
	    var i = Cn.debug ? document.createComment(t) : document.createTextNode(e ? " " : "");return i.__v_anchor = !0, i;
	  }function rt(t) {
	    if (t.hasAttributes()) for (var e = t.attributes, i = 0, n = e.length; n > i; i++) {
	      var r = e[i].name;if (xn.test(r)) return l(r.replace(xn, ""));
	    }
	  }function st(t, e, i) {
	    for (var n; t !== e;) {
	      n = t.nextSibling, i(t), t = n;
	    }i(e);
	  }function ot(t, e, i, n, r) {
	    function s() {
	      if (a++, o && a >= h.length) {
	        for (var t = 0; t < h.length; t++) {
	          n.appendChild(h[t]);
	        }r && r();
	      }
	    }var o = !1,
	        a = 0,
	        h = [];st(t, e, function (t) {
	      t === e && (o = !0), h.push(t), P(t, i, s);
	    });
	  }function at(t) {
	    return t && 11 === t.nodeType;
	  }function ht(t) {
	    if (t.outerHTML) return t.outerHTML;var e = document.createElement("div");return e.appendChild(t.cloneNode(!0)), e.innerHTML;
	  }function lt(t, e) {
	    var i = t.tagName.toLowerCase(),
	        n = t.hasAttributes();if (An.test(i) || On.test(i)) {
	      if (n) return ct(t, e);
	    } else {
	      if (gt(e, "components", i)) return { id: i };var r = n && ct(t, e);if (r) return r;
	    }
	  }function ct(t, e) {
	    var i = t.getAttribute("is");if (null != i) {
	      if (gt(e, "components", i)) return t.removeAttribute("is"), { id: i };
	    } else if (i = M(t, "is"), null != i) return { id: i, dynamic: !0 };
	  }function ut(e, n) {
	    var r, s, o;for (r in n) {
	      s = e[r], o = n[r], i(e, r) ? m(s) && m(o) && ut(s, o) : t(e, r, o);
	    }return e;
	  }function ft(t, e) {
	    var i = Object.create(t || null);return e ? v(i, vt(e)) : i;
	  }function pt(t) {
	    if (t.components) for (var e, i = t.components = vt(t.components), n = Object.keys(i), r = 0, s = n.length; s > r; r++) {
	      var o = n[r];An.test(o) || On.test(o) || (e = i[o], g(e) && (i[o] = bi.extend(e)));
	    }
	  }function dt(t) {
	    var e,
	        i,
	        n = t.props;if (Fi(n)) for (t.props = {}, e = n.length; e--;) {
	      i = n[e], "string" == typeof i ? t.props[i] = null : i.name && (t.props[i.name] = i);
	    } else if (g(n)) {
	      var r = Object.keys(n);for (e = r.length; e--;) {
	        i = n[r[e]], "function" == typeof i && (n[r[e]] = { type: i });
	      }
	    }
	  }function vt(t) {
	    if (Fi(t)) {
	      for (var e, i = {}, n = t.length; n--;) {
	        e = t[n];var r = "function" == typeof e ? e.options && e.options.name || e.id : e.name || e.id;r && (i[r] = e);
	      }return i;
	    }return t;
	  }function mt(t, e, n) {
	    function r(i) {
	      var r = Tn[i] || Nn;o[i] = r(t[i], e[i], n, i);
	    }pt(e), dt(e);var s,
	        o = {};if (e["extends"] && (t = "function" == typeof e["extends"] ? mt(t, e["extends"].options, n) : mt(t, e["extends"], n)), e.mixins) for (var a = 0, h = e.mixins.length; h > a; a++) {
	      t = mt(t, e.mixins[a], n);
	    }for (s in t) {
	      r(s);
	    }for (s in e) {
	      i(t, s) || r(s);
	    }return o;
	  }function gt(t, e, i, n) {
	    if ("string" == typeof i) {
	      var r,
	          s = t[e],
	          o = s[i] || s[r = l(i)] || s[r.charAt(0).toUpperCase() + r.slice(1)];return o;
	    }
	  }function _t() {
	    this.id = jn++, this.subs = [];
	  }function yt(t) {
	    Dn = !1, t(), Dn = !0;
	  }function bt(t) {
	    if (this.value = t, this.dep = new _t(), _(t, "__ob__", this), Fi(t)) {
	      var e = Di ? wt : Ct;e(t, Sn, Fn), this.observeArray(t);
	    } else this.walk(t);
	  }function wt(t, e) {
	    t.__proto__ = e;
	  }function Ct(t, e, i) {
	    for (var n = 0, r = i.length; r > n; n++) {
	      var s = i[n];_(t, s, e[s]);
	    }
	  }function $t(t, e) {
	    if (t && "object" == (typeof t === "undefined" ? "undefined" : _typeof(t))) {
	      var n;return i(t, "__ob__") && t.__ob__ instanceof bt ? n = t.__ob__ : Dn && (Fi(t) || g(t)) && Object.isExtensible(t) && !t._isVue && (n = new bt(t)), n && e && n.addVm(e), n;
	    }
	  }function kt(t, e, i) {
	    var n = new _t(),
	        r = Object.getOwnPropertyDescriptor(t, e);if (!r || r.configurable !== !1) {
	      var s = r && r.get,
	          o = r && r.set,
	          a = $t(i);Object.defineProperty(t, e, { enumerable: !0, configurable: !0, get: function get() {
	          var e = s ? s.call(t) : i;if (_t.target && (n.depend(), a && a.dep.depend(), Fi(e))) for (var r, o = 0, h = e.length; h > o; o++) {
	            r = e[o], r && r.__ob__ && r.__ob__.dep.depend();
	          }return e;
	        }, set: function set(e) {
	          var r = s ? s.call(t) : i;e !== r && (o ? o.call(t, e) : i = e, a = $t(e), n.notify());
	        } });
	    }
	  }function xt(t) {
	    t.prototype._init = function (t) {
	      t = t || {}, this.$el = null, this.$parent = t.parent, this.$root = this.$parent ? this.$parent.$root : this, this.$children = [], this.$refs = {}, this.$els = {}, this._watchers = [], this._directives = [], this._uid = Rn++, this._isVue = !0, this._events = {}, this._eventsCount = {}, this._isFragment = !1, this._fragment = this._fragmentStart = this._fragmentEnd = null, this._isCompiled = this._isDestroyed = this._isReady = this._isAttached = this._isBeingDestroyed = this._vForRemoving = !1, this._unlinkFn = null, this._context = t._context || this.$parent, this._scope = t._scope, this._frag = t._frag, this._frag && this._frag.children.push(this), this.$parent && this.$parent.$children.push(this), t = this.$options = mt(this.constructor.options, t, this), this._updateRef(), this._data = {}, this._callHook("init"), this._initState(), this._initEvents(), this._callHook("created"), t.el && this.$mount(t.el);
	    };
	  }function At(t) {
	    if (void 0 === t) return "eof";var e = t.charCodeAt(0);switch (e) {case 91:case 93:case 46:case 34:case 39:case 48:
	        return t;case 95:case 36:
	        return "ident";case 32:case 9:case 10:case 13:case 160:case 65279:case 8232:case 8233:
	        return "ws";}return e >= 97 && 122 >= e || e >= 65 && 90 >= e ? "ident" : e >= 49 && 57 >= e ? "number" : "else";
	  }function Ot(t) {
	    var e = t.trim();return "0" === t.charAt(0) && isNaN(t) ? !1 : n(e) ? h(e) : "*" + e;
	  }function Tt(t) {
	    function e() {
	      var e = t[c + 1];return u === qn && "'" === e || u === Qn && '"' === e ? (c++, n = "\\" + e, p[Hn](), !0) : void 0;
	    }var i,
	        n,
	        r,
	        s,
	        o,
	        a,
	        h,
	        l = [],
	        c = -1,
	        u = Vn,
	        f = 0,
	        p = [];for (p[In] = function () {
	      void 0 !== r && (l.push(r), r = void 0);
	    }, p[Hn] = function () {
	      void 0 === r ? r = n : r += n;
	    }, p[Mn] = function () {
	      p[Hn](), f++;
	    }, p[Wn] = function () {
	      if (f > 0) f--, u = Jn, p[Hn]();else {
	        if (f = 0, r = Ot(r), r === !1) return !1;p[In]();
	      }
	    }; null != u;) {
	      if (c++, i = t[c], "\\" !== i || !e()) {
	        if (s = At(i), h = Xn[u], o = h[s] || h["else"] || Zn, o === Zn) return;if (u = o[0], a = p[o[1]], a && (n = o[2], n = void 0 === n ? i : n, a() === !1)) return;if (u === Gn) return l.raw = t, l;
	      }
	    }
	  }function Nt(t) {
	    var e = Ln.get(t);return e || (e = Tt(t), e && Ln.put(t, e)), e;
	  }function jt(t, e) {
	    return Ht(e).get(t);
	  }function Et(e, i, n) {
	    var r = e;if ("string" == typeof i && (i = Tt(i)), !i || !m(e)) return !1;for (var s, o, a = 0, h = i.length; h > a; a++) {
	      s = e, o = i[a], "*" === o.charAt(0) && (o = Ht(o.slice(1)).get.call(r, r)), h - 1 > a ? (e = e[o], m(e) || (e = {}, t(s, o, e))) : Fi(e) ? e.$set(o, n) : o in e ? e[o] = n : t(e, o, n);
	    }return !0;
	  }function St(t, e) {
	    var i = ur.length;return ur[i] = e ? t.replace(sr, "\\n") : t, '"' + i + '"';
	  }function Ft(t) {
	    var e = t.charAt(0),
	        i = t.slice(1);return er.test(i) ? t : (i = i.indexOf('"') > -1 ? i.replace(ar, Dt) : i, e + "scope." + i);
	  }function Dt(t, e) {
	    return ur[e];
	  }function Pt(t) {
	    nr.test(t), ur.length = 0;var e = t.replace(or, St).replace(rr, "");return e = (" " + e).replace(lr, Ft).replace(ar, Dt), Rt(e);
	  }function Rt(t) {
	    try {
	      return new Function("scope", "return " + t + ";");
	    } catch (e) {}
	  }function Lt(t) {
	    var e = Nt(t);return e ? function (t, i) {
	      Et(t, e, i);
	    } : void 0;
	  }function Ht(t, e) {
	    t = t.trim();var i = Kn.get(t);if (i) return e && !i.set && (i.set = Lt(i.exp)), i;var n = { exp: t };return n.get = It(t) && t.indexOf("[") < 0 ? Rt("scope." + t) : Pt(t), e && (n.set = Lt(t)), Kn.put(t, n), n;
	  }function It(t) {
	    return hr.test(t) && !cr.test(t) && "Math." !== t.slice(0, 5);
	  }function Mt() {
	    pr.length = 0, dr.length = 0, vr = {}, mr = {}, gr = !1;
	  }function Wt() {
	    for (var t = !0; t;) {
	      t = !1, Vt(pr), Vt(dr), pr.length ? t = !0 : (Ri && Cn.devtools && Ri.emit("flush"), Mt());
	    }
	  }function Vt(t) {
	    for (var e = 0; e < t.length; e++) {
	      var i = t[e],
	          n = i.id;vr[n] = null, i.run();
	    }t.length = 0;
	  }function Bt(t) {
	    var e = t.id;if (null == vr[e]) {
	      var i = t.user ? dr : pr;vr[e] = i.length, i.push(t), gr || (gr = !0, Qi(Wt));
	    }
	  }function zt(t, e, i, n) {
	    n && v(this, n);var r = "function" == typeof e;if (this.vm = t, t._watchers.push(this), this.expression = e, this.cb = i, this.id = ++_r, this.active = !0, this.dirty = this.lazy, this.deps = [], this.newDeps = [], this.depIds = new Gi(), this.newDepIds = new Gi(), this.prevError = null, r) this.getter = e, this.setter = void 0;else {
	      var s = Ht(e, this.twoWay);this.getter = s.get, this.setter = s.set;
	    }this.value = this.lazy ? void 0 : this.get(), this.queued = this.shallow = !1;
	  }function Ut(t, e) {
	    var i = void 0,
	        n = void 0;e || (e = yr, e.clear());var r = Fi(t),
	        s = m(t);if (r || s) {
	      if (t.__ob__) {
	        var o = t.__ob__.dep.id;if (e.has(o)) return;e.add(o);
	      }if (r) for (i = t.length; i--;) {
	        Ut(t[i], e);
	      } else if (s) for (n = Object.keys(t), i = n.length; i--;) {
	        Ut(t[n[i]], e);
	      }
	    }
	  }function Jt(t) {
	    return it(t) && at(t.content);
	  }function qt(t, e) {
	    var i = e ? t : t.trim(),
	        n = wr.get(i);if (n) return n;var r = document.createDocumentFragment(),
	        s = t.match(kr),
	        o = xr.test(t);if (s || o) {
	      var a = s && s[1],
	          h = $r[a] || $r.efault,
	          l = h[0],
	          c = h[1],
	          u = h[2],
	          f = document.createElement("div");for (f.innerHTML = c + t + u; l--;) {
	        f = f.lastChild;
	      }for (var p; p = f.firstChild;) {
	        r.appendChild(p);
	      }
	    } else r.appendChild(document.createTextNode(t));return e || tt(r), wr.put(i, r), r;
	  }function Qt(t) {
	    if (Jt(t)) return qt(t.innerHTML);if ("SCRIPT" === t.tagName) return qt(t.textContent);for (var e, i = Gt(t), n = document.createDocumentFragment(); e = i.firstChild;) {
	      n.appendChild(e);
	    }return tt(n), n;
	  }function Gt(t) {
	    if (!t.querySelectorAll) return t.cloneNode();var e,
	        i,
	        n,
	        r = t.cloneNode(!0);if (Ar) {
	      var s = r;if (Jt(t) && (t = t.content, s = r.content), i = t.querySelectorAll("template"), i.length) for (n = s.querySelectorAll("template"), e = n.length; e--;) {
	        n[e].parentNode.replaceChild(Gt(i[e]), n[e]);
	      }
	    }if (Or) if ("TEXTAREA" === t.tagName) r.value = t.value;else if (i = t.querySelectorAll("textarea"), i.length) for (n = r.querySelectorAll("textarea"), e = n.length; e--;) {
	      n[e].value = i[e].value;
	    }return r;
	  }function Zt(t, e, i) {
	    var n, r;return at(t) ? (tt(t), e ? Gt(t) : t) : ("string" == typeof t ? i || "#" !== t.charAt(0) ? r = qt(t, i) : (r = Cr.get(t), r || (n = document.getElementById(t.slice(1)), n && (r = Qt(n), Cr.put(t, r)))) : t.nodeType && (r = Qt(t)), r && e ? Gt(r) : r);
	  }function Xt(t, e, i, n, r, s) {
	    this.children = [], this.childFrags = [], this.vm = e, this.scope = r, this.inserted = !1, this.parentFrag = s, s && s.childFrags.push(this), this.unlink = t(e, i, n, r, this);var o = this.single = 1 === i.childNodes.length && !i.childNodes[0].__v_anchor;o ? (this.node = i.childNodes[0], this.before = Yt, this.remove = Kt) : (this.node = nt("fragment-start"), this.end = nt("fragment-end"), this.frag = i, U(this.node, i), i.appendChild(this.end), this.before = te, this.remove = ee), this.node.__v_frag = this;
	  }function Yt(t, e) {
	    this.inserted = !0;var i = e !== !1 ? D : V;i(this.node, t, this.vm), H(this.node) && this.callHook(ie);
	  }function Kt() {
	    this.inserted = !1;var t = H(this.node),
	        e = this;this.beforeRemove(), P(this.node, this.vm, function () {
	      t && e.callHook(ne), e.destroy();
	    });
	  }function te(t, e) {
	    this.inserted = !0;var i = this.vm,
	        n = e !== !1 ? D : V;st(this.node, this.end, function (e) {
	      n(e, t, i);
	    }), H(this.node) && this.callHook(ie);
	  }function ee() {
	    this.inserted = !1;var t = this,
	        e = H(this.node);this.beforeRemove(), ot(this.node, this.end, this.vm, this.frag, function () {
	      e && t.callHook(ne), t.destroy();
	    });
	  }function ie(t) {
	    !t._isAttached && H(t.$el) && t._callHook("attached");
	  }function ne(t) {
	    t._isAttached && !H(t.$el) && t._callHook("detached");
	  }function re(t, e) {
	    this.vm = t;var i,
	        n = "string" == typeof e;n || it(e) && !e.hasAttribute("v-if") ? i = Zt(e, !0) : (i = document.createDocumentFragment(), i.appendChild(e)), this.template = i;var r,
	        s = t.constructor.cid;if (s > 0) {
	      var o = s + (n ? e : ht(e));r = jr.get(o), r || (r = Fe(i, t.$options, !0), jr.put(o, r));
	    } else r = Fe(i, t.$options, !0);this.linker = r;
	  }function se(t, e, i) {
	    var n = t.node.previousSibling;if (n) {
	      for (t = n.__v_frag; !(t && t.forId === i && t.inserted || n === e);) {
	        if (n = n.previousSibling, !n) return;t = n.__v_frag;
	      }return t;
	    }
	  }function oe(t) {
	    var e = t.node;if (t.end) for (; !e.__vue__ && e !== t.end && e.nextSibling;) {
	      e = e.nextSibling;
	    }return e.__vue__;
	  }function ae(t) {
	    for (var e = -1, i = new Array(Math.floor(t)); ++e < t;) {
	      i[e] = e;
	    }return i;
	  }function he(t, e, i, n) {
	    return n ? "$index" === n ? t : n.charAt(0).match(/\w/) ? jt(i, n) : i[n] : e || i;
	  }function le(t, e, i) {
	    for (var n, r, s, o = e ? [] : null, a = 0, h = t.options.length; h > a; a++) {
	      if (n = t.options[a], s = i ? n.hasAttribute("selected") : n.selected) {
	        if (r = n.hasOwnProperty("_value") ? n._value : n.value, !e) return r;o.push(r);
	      }
	    }return o;
	  }function ce(t, e) {
	    for (var i = t.length; i--;) {
	      if (C(t[i], e)) return i;
	    }return -1;
	  }function ue(t, e) {
	    var i = e.map(function (t) {
	      var e = t.charCodeAt(0);return e > 47 && 58 > e ? parseInt(t, 10) : 1 === t.length && (e = t.toUpperCase().charCodeAt(0), e > 64 && 91 > e) ? e : Xr[t];
	    });return i = [].concat.apply([], i), function (e) {
	      return i.indexOf(e.keyCode) > -1 ? t.call(this, e) : void 0;
	    };
	  }function fe(t) {
	    return function (e) {
	      return e.stopPropagation(), t.call(this, e);
	    };
	  }function pe(t) {
	    return function (e) {
	      return e.preventDefault(), t.call(this, e);
	    };
	  }function de(t) {
	    return function (e) {
	      return e.target === e.currentTarget ? t.call(this, e) : void 0;
	    };
	  }function ve(t) {
	    if (is[t]) return is[t];var e = me(t);return is[t] = is[e] = e, e;
	  }function me(t) {
	    t = u(t);var e = l(t),
	        i = e.charAt(0).toUpperCase() + e.slice(1);ns || (ns = document.createElement("div"));var n,
	        r = Kr.length;if ("filter" !== e && e in ns.style) return { kebab: t, camel: e };for (; r--;) {
	      if (n = ts[r] + i, n in ns.style) return { kebab: Kr[r] + t, camel: n };
	    }
	  }function ge(t) {
	    var e = [];if (Fi(t)) for (var i = 0, n = t.length; n > i; i++) {
	      var r = t[i];if (r) if ("string" == typeof r) e.push(r);else for (var s in r) {
	        r[s] && e.push(s);
	      }
	    } else if (m(t)) for (var o in t) {
	      t[o] && e.push(o);
	    }return e;
	  }function _e(t, e, i) {
	    if (e = e.trim(), -1 === e.indexOf(" ")) return void i(t, e);for (var n = e.split(/\s+/), r = 0, s = n.length; s > r; r++) {
	      i(t, n[r]);
	    }
	  }function ye(t, e, i) {
	    function n() {
	      ++s >= r ? i() : t[s].call(e, n);
	    }var r = t.length,
	        s = 0;t[0].call(e, n);
	  }function be(t, e, i) {
	    for (var r, s, o, a, h, c, f, p = [], d = Object.keys(e), v = d.length; v--;) {
	      s = d[v], r = e[s] || ys, h = l(s), bs.test(h) && (f = { name: s, path: h, options: r, mode: _s.ONE_WAY, raw: null }, o = u(s), null === (a = M(t, o)) && (null !== (a = M(t, o + ".sync")) ? f.mode = _s.TWO_WAY : null !== (a = M(t, o + ".once")) && (f.mode = _s.ONE_TIME)), null !== a ? (f.raw = a, c = A(a), a = c.expression, f.filters = c.filters, n(a) && !c.filters ? f.optimizedLiteral = !0 : f.dynamic = !0, f.parentPath = a) : null !== (a = I(t, o)) && (f.raw = a), p.push(f));
	    }return we(p);
	  }function we(t) {
	    return function (e, n) {
	      e._props = {};for (var r, s, l, c, f, p = e.$options.propsData, d = t.length; d--;) {
	        if (r = t[d], f = r.raw, s = r.path, l = r.options, e._props[s] = r, p && i(p, s) && $e(e, r, p[s]), null === f) $e(e, r, void 0);else if (r.dynamic) r.mode === _s.ONE_TIME ? (c = (n || e._context || e).$get(r.parentPath), $e(e, r, c)) : e._context ? e._bindDir({ name: "prop", def: Cs, prop: r }, null, null, n) : $e(e, r, e.$get(r.parentPath));else if (r.optimizedLiteral) {
	          var v = h(f);c = v === f ? a(o(f)) : v, $e(e, r, c);
	        } else c = l.type !== Boolean || "" !== f && f !== u(r.name) ? f : !0, $e(e, r, c);
	      }
	    };
	  }function Ce(t, e, i, n) {
	    var r = e.dynamic && It(e.parentPath),
	        s = i;void 0 === s && (s = xe(t, e)), s = Oe(e, s);var o = s !== i;Ae(e, s, t) || (s = void 0), r && !o ? yt(function () {
	      n(s);
	    }) : n(s);
	  }function $e(t, e, i) {
	    Ce(t, e, i, function (i) {
	      kt(t, e.path, i);
	    });
	  }function ke(t, e, i) {
	    Ce(t, e, i, function (i) {
	      t[e.path] = i;
	    });
	  }function xe(t, e) {
	    var n = e.options;if (!i(n, "default")) return n.type === Boolean ? !1 : void 0;var r = n["default"];return m(r), "function" == typeof r && n.type !== Function ? r.call(t) : r;
	  }function Ae(t, e, i) {
	    if (!t.options.required && (null === t.raw || null == e)) return !0;var n = t.options,
	        r = n.type,
	        s = !r,
	        o = [];if (r) {
	      Fi(r) || (r = [r]);for (var a = 0; a < r.length && !s; a++) {
	        var h = Te(e, r[a]);o.push(h.expectedType), s = h.valid;
	      }
	    }if (!s) return !1;var l = n.validator;return !l || l(e);
	  }function Oe(t, e) {
	    var i = t.options.coerce;return i ? i(e) : e;
	  }function Te(t, e) {
	    var i, n;return e === String ? (n = "string", i = (typeof t === "undefined" ? "undefined" : _typeof(t)) === n) : e === Number ? (n = "number", i = (typeof t === "undefined" ? "undefined" : _typeof(t)) === n) : e === Boolean ? (n = "boolean", i = (typeof t === "undefined" ? "undefined" : _typeof(t)) === n) : e === Function ? (n = "function", i = (typeof t === "undefined" ? "undefined" : _typeof(t)) === n) : e === Object ? (n = "object", i = g(t)) : e === Array ? (n = "array", i = Fi(t)) : i = t instanceof e, { valid: i, expectedType: n };
	  }function Ne(t) {
	    $s.push(t), ks || (ks = !0, Qi(je));
	  }function je() {
	    for (var t = document.documentElement.offsetHeight, e = 0; e < $s.length; e++) {
	      $s[e]();
	    }return $s = [], ks = !1, t;
	  }function Ee(t, e, i, n) {
	    this.id = e, this.el = t, this.enterClass = i && i.enterClass || e + "-enter", this.leaveClass = i && i.leaveClass || e + "-leave", this.hooks = i, this.vm = n, this.pendingCssEvent = this.pendingCssCb = this.cancel = this.pendingJsCb = this.op = this.cb = null, this.justEntered = !1, this.entered = this.left = !1, this.typeCache = {}, this.type = i && i.type;var r = this;["enterNextTick", "enterDone", "leaveNextTick", "leaveDone"].forEach(function (t) {
	      r[t] = p(r[t], r);
	    });
	  }function Se(t) {
	    if (/svg$/.test(t.namespaceURI)) {
	      var e = t.getBoundingClientRect();return !(e.width || e.height);
	    }return !(t.offsetWidth || t.offsetHeight || t.getClientRects().length);
	  }function Fe(t, e, i) {
	    var n = i || !e._asComponent ? Me(t, e) : null,
	        r = n && n.terminal || ni(t) || !t.hasChildNodes() ? null : Je(t.childNodes, e);return function (t, e, i, s, o) {
	      var a = d(e.childNodes),
	          h = De(function () {
	        n && n(t, e, i, s, o), r && r(t, a, i, s, o);
	      }, t);return Re(t, h);
	    };
	  }function De(t, e) {
	    e._directives = [];var i = e._directives.length;t();var n = e._directives.slice(i);n.sort(Pe);for (var r = 0, s = n.length; s > r; r++) {
	      n[r]._bind();
	    }return n;
	  }function Pe(t, e) {
	    return t = t.descriptor.def.priority || Is, e = e.descriptor.def.priority || Is, t > e ? -1 : t === e ? 0 : 1;
	  }function Re(t, e, i, n) {
	    function r(r) {
	      Le(t, e, r), i && n && Le(i, n);
	    }return r.dirs = e, r;
	  }function Le(t, e, i) {
	    for (var n = e.length; n--;) {
	      e[n]._teardown();
	    }
	  }function He(t, e, i, n) {
	    var r = be(e, i, t),
	        s = De(function () {
	      r(t, n);
	    }, t);return Re(t, s);
	  }function Ie(t, e, i) {
	    var n,
	        r,
	        s = e._containerAttrs,
	        o = e._replacerAttrs;return 11 !== t.nodeType && (e._asComponent ? (s && i && (n = Ke(s, i)), o && (r = Ke(o, e))) : r = Ke(t.attributes, e)), e._containerAttrs = e._replacerAttrs = null, function (t, e, i) {
	      var s,
	          o = t._context;o && n && (s = De(function () {
	        n(o, e, null, i);
	      }, o));var a = De(function () {
	        r && r(t, e);
	      }, t);return Re(t, a, o, s);
	    };
	  }function Me(t, e) {
	    var i = t.nodeType;return 1 !== i || ni(t) ? 3 === i && t.data.trim() ? Ve(t, e) : null : We(t, e);
	  }function We(t, e) {
	    if ("TEXTAREA" === t.tagName) {
	      var i = N(t.value);i && (t.setAttribute(":value", j(i)), t.value = "");
	    }var n,
	        r = t.hasAttributes(),
	        s = r && d(t.attributes);return r && (n = Ze(t, s, e)), n || (n = Qe(t, e)), n || (n = Ge(t, e)), !n && r && (n = Ke(s, e)), n;
	  }function Ve(t, e) {
	    if (t._skip) return Be;var i = N(t.wholeText);if (!i) return null;for (var n = t.nextSibling; n && 3 === n.nodeType;) {
	      n._skip = !0, n = n.nextSibling;
	    }for (var r, s, o = document.createDocumentFragment(), a = 0, h = i.length; h > a; a++) {
	      s = i[a], r = s.tag ? ze(s, e) : document.createTextNode(s.value), o.appendChild(r);
	    }return Ue(i, o, e);
	  }function Be(t, e) {
	    z(e);
	  }function ze(t, e) {
	    function i(e) {
	      if (!t.descriptor) {
	        var i = A(t.value);t.descriptor = { name: e, def: vs[e], expression: i.expression, filters: i.filters };
	      }
	    }var n;return t.oneTime ? n = document.createTextNode(t.value) : t.html ? (n = document.createComment("v-html"), i("html")) : (n = document.createTextNode(" "), i("text")), n;
	  }function Ue(t, e) {
	    return function (i, n, r, s) {
	      for (var o, a, h, l = e.cloneNode(!0), c = d(l.childNodes), u = 0, f = t.length; f > u; u++) {
	        o = t[u], a = o.value, o.tag && (h = c[u], o.oneTime ? (a = (s || i).$eval(a), o.html ? J(h, Zt(a, !0)) : h.data = a) : i._bindDir(o.descriptor, h, r, s));
	      }J(n, l);
	    };
	  }function Je(t, e) {
	    for (var i, n, r, s = [], o = 0, a = t.length; a > o; o++) {
	      r = t[o], i = Me(r, e), n = i && i.terminal || "SCRIPT" === r.tagName || !r.hasChildNodes() ? null : Je(r.childNodes, e), s.push(i, n);
	    }return s.length ? qe(s) : null;
	  }function qe(t) {
	    return function (e, i, n, r, s) {
	      for (var o, a, h, l = 0, c = 0, u = t.length; u > l; c++) {
	        o = i[c], a = t[l++], h = t[l++];var f = d(o.childNodes);a && a(e, o, n, r, s), h && h(e, f, n, r, s);
	      }
	    };
	  }function Qe(t, e) {
	    var i = t.tagName.toLowerCase();if (!An.test(i)) {
	      var n = gt(e, "elementDirectives", i);return n ? Ye(t, i, "", e, n) : void 0;
	    }
	  }function Ge(t, e) {
	    var i = lt(t, e);if (i) {
	      var n = rt(t),
	          r = { name: "component", ref: n, expression: i.id, def: Fs.component, modifiers: { literal: !i.dynamic } },
	          s = function s(t, e, i, _s2, o) {
	        n && kt((_s2 || t).$refs, n, null), t._bindDir(r, e, i, _s2, o);
	      };return s.terminal = !0, s;
	    }
	  }function Ze(t, e, i) {
	    if (null !== I(t, "v-pre")) return Xe;if (t.hasAttribute("v-else")) {
	      var n = t.previousElementSibling;if (n && n.hasAttribute("v-if")) return Xe;
	    }for (var r, s, o, a, h, l, c, u, f, p, d = 0, v = e.length; v > d; d++) {
	      r = e[d], s = r.name.replace(Ls, ""), (h = s.match(Rs)) && (f = gt(i, "directives", h[1]), f && f.terminal && (!p || (f.priority || Ms) > p.priority) && (p = f, c = r.name, a = ti(r.name), o = r.value, l = h[1], u = h[2]));
	    }return p ? Ye(t, l, o, i, p, c, u, a) : void 0;
	  }function Xe() {}function Ye(t, e, i, n, r, s, o, a) {
	    var h = A(i),
	        l = { name: e, arg: o, expression: h.expression, filters: h.filters, raw: i, attr: s, modifiers: a, def: r };"for" !== e && "router-view" !== e || (l.ref = rt(t));var c = function c(t, e, i, n, r) {
	      l.ref && kt((n || t).$refs, l.ref, null), t._bindDir(l, e, i, n, r);
	    };return c.terminal = !0, c;
	  }function Ke(t, e) {
	    function i(t, e, i) {
	      var n = i && ii(i),
	          r = !n && A(s);v.push({ name: t, attr: o, raw: a, def: e, arg: l, modifiers: c, expression: r && r.expression, filters: r && r.filters, interp: i, hasOneTime: n });
	    }for (var n, r, s, o, a, h, l, c, u, f, p, d = t.length, v = []; d--;) {
	      if (n = t[d], r = o = n.name, s = a = n.value, f = N(s), l = null, c = ti(r), r = r.replace(Ls, ""), f) s = j(f), l = r, i("bind", vs.bind, f);else if (Hs.test(r)) c.literal = !Ds.test(r), i("transition", Fs.transition);else if (Ps.test(r)) l = r.replace(Ps, ""), i("on", vs.on);else if (Ds.test(r)) h = r.replace(Ds, ""), "style" === h || "class" === h ? i(h, Fs[h]) : (l = h, i("bind", vs.bind));else if (p = r.match(Rs)) {
	        if (h = p[1], l = p[2], "else" === h) continue;u = gt(e, "directives", h, !0), u && i(h, u);
	      }
	    }return v.length ? ei(v) : void 0;
	  }function ti(t) {
	    var e = Object.create(null),
	        i = t.match(Ls);if (i) for (var n = i.length; n--;) {
	      e[i[n].slice(1)] = !0;
	    }return e;
	  }function ei(t) {
	    return function (e, i, n, r, s) {
	      for (var o = t.length; o--;) {
	        e._bindDir(t[o], i, n, r, s);
	      }
	    };
	  }function ii(t) {
	    for (var e = t.length; e--;) {
	      if (t[e].oneTime) return !0;
	    }
	  }function ni(t) {
	    return "SCRIPT" === t.tagName && (!t.hasAttribute("type") || "text/javascript" === t.getAttribute("type"));
	  }function ri(t, e) {
	    return e && (e._containerAttrs = oi(t)), it(t) && (t = Zt(t)), e && (e._asComponent && !e.template && (e.template = "<slot></slot>"), e.template && (e._content = K(t), t = si(t, e))), at(t) && (U(nt("v-start", !0), t), t.appendChild(nt("v-end", !0))), t;
	  }function si(t, e) {
	    var i = e.template,
	        n = Zt(i, !0);if (n) {
	      var r = n.firstChild,
	          s = r.tagName && r.tagName.toLowerCase();return e.replace ? (t === document.body, n.childNodes.length > 1 || 1 !== r.nodeType || "component" === s || gt(e, "components", s) || W(r, "is") || gt(e, "elementDirectives", s) || r.hasAttribute("v-for") || r.hasAttribute("v-if") ? n : (e._replacerAttrs = oi(r), ai(t, r), r)) : (t.appendChild(n), t);
	    }
	  }function oi(t) {
	    return 1 === t.nodeType && t.hasAttributes() ? d(t.attributes) : void 0;
	  }function ai(t, e) {
	    for (var i, n, r = t.attributes, s = r.length; s--;) {
	      i = r[s].name, n = r[s].value, e.hasAttribute(i) || Ws.test(i) ? "class" === i && !N(n) && (n = n.trim()) && n.split(/\s+/).forEach(function (t) {
	        X(e, t);
	      }) : e.setAttribute(i, n);
	    }
	  }function hi(t, e) {
	    if (e) {
	      for (var i, n, r = t._slotContents = Object.create(null), s = 0, o = e.children.length; o > s; s++) {
	        i = e.children[s], (n = i.getAttribute("slot")) && (r[n] || (r[n] = [])).push(i);
	      }for (n in r) {
	        r[n] = li(r[n], e);
	      }if (e.hasChildNodes()) {
	        var a = e.childNodes;if (1 === a.length && 3 === a[0].nodeType && !a[0].data.trim()) return;r["default"] = li(e.childNodes, e);
	      }
	    }
	  }function li(t, e) {
	    var i = document.createDocumentFragment();t = d(t);for (var n = 0, r = t.length; r > n; n++) {
	      var s = t[n];!it(s) || s.hasAttribute("v-if") || s.hasAttribute("v-for") || (e.removeChild(s), s = Zt(s, !0)), i.appendChild(s);
	    }return i;
	  }function ci(t) {
	    function e() {}function n(t, e) {
	      var i = new zt(e, t, null, { lazy: !0 });return function () {
	        return i.dirty && i.evaluate(), _t.target && i.depend(), i.value;
	      };
	    }Object.defineProperty(t.prototype, "$data", { get: function get() {
	        return this._data;
	      }, set: function set(t) {
	        t !== this._data && this._setData(t);
	      } }), t.prototype._initState = function () {
	      this._initProps(), this._initMeta(), this._initMethods(), this._initData(), this._initComputed();
	    }, t.prototype._initProps = function () {
	      var t = this.$options,
	          e = t.el,
	          i = t.props;e = t.el = L(e), this._propsUnlinkFn = e && 1 === e.nodeType && i ? He(this, e, i, this._scope) : null;
	    }, t.prototype._initData = function () {
	      var t = this.$options.data,
	          e = this._data = t ? t() : {};g(e) || (e = {});var n,
	          r,
	          s = this._props,
	          o = Object.keys(e);for (n = o.length; n--;) {
	        r = o[n], s && i(s, r) || this._proxy(r);
	      }$t(e, this);
	    }, t.prototype._setData = function (t) {
	      t = t || {};var e = this._data;this._data = t;var n, r, s;for (n = Object.keys(e), s = n.length; s--;) {
	        r = n[s], r in t || this._unproxy(r);
	      }for (n = Object.keys(t), s = n.length; s--;) {
	        r = n[s], i(this, r) || this._proxy(r);
	      }e.__ob__.removeVm(this), $t(t, this), this._digest();
	    }, t.prototype._proxy = function (t) {
	      if (!r(t)) {
	        var e = this;Object.defineProperty(e, t, { configurable: !0, enumerable: !0, get: function get() {
	            return e._data[t];
	          }, set: function set(i) {
	            e._data[t] = i;
	          } });
	      }
	    }, t.prototype._unproxy = function (t) {
	      r(t) || delete this[t];
	    }, t.prototype._digest = function () {
	      for (var t = 0, e = this._watchers.length; e > t; t++) {
	        this._watchers[t].update(!0);
	      }
	    }, t.prototype._initComputed = function () {
	      var t = this.$options.computed;if (t) for (var i in t) {
	        var r = t[i],
	            s = { enumerable: !0, configurable: !0 };"function" == typeof r ? (s.get = n(r, this), s.set = e) : (s.get = r.get ? r.cache !== !1 ? n(r.get, this) : p(r.get, this) : e, s.set = r.set ? p(r.set, this) : e), Object.defineProperty(this, i, s);
	      }
	    }, t.prototype._initMethods = function () {
	      var t = this.$options.methods;if (t) for (var e in t) {
	        this[e] = p(t[e], this);
	      }
	    }, t.prototype._initMeta = function () {
	      var t = this.$options._meta;if (t) for (var e in t) {
	        kt(this, e, t[e]);
	      }
	    };
	  }function ui(t) {
	    function e(t, e) {
	      for (var i, n, r, s = e.attributes, o = 0, a = s.length; a > o; o++) {
	        i = s[o].name, Bs.test(i) && (i = i.replace(Bs, ""), n = s[o].value, It(n) && (n += ".apply(this, $arguments)"), r = (t._scope || t._context).$eval(n, !0), r._fromParent = !0, t.$on(i.replace(Bs), r));
	      }
	    }function i(t, e, i) {
	      if (i) {
	        var r, s, o, a;for (s in i) {
	          if (r = i[s], Fi(r)) for (o = 0, a = r.length; a > o; o++) {
	            n(t, e, s, r[o]);
	          } else n(t, e, s, r);
	        }
	      }
	    }function n(t, e, i, r, s) {
	      var o = typeof r === "undefined" ? "undefined" : _typeof(r);if ("function" === o) t[e](i, r, s);else if ("string" === o) {
	        var a = t.$options.methods,
	            h = a && a[r];h && t[e](i, h, s);
	      } else r && "object" === o && n(t, e, i, r.handler, r);
	    }function r() {
	      this._isAttached || (this._isAttached = !0, this.$children.forEach(s));
	    }function s(t) {
	      !t._isAttached && H(t.$el) && t._callHook("attached");
	    }function o() {
	      this._isAttached && (this._isAttached = !1, this.$children.forEach(a));
	    }function a(t) {
	      t._isAttached && !H(t.$el) && t._callHook("detached");
	    }t.prototype._initEvents = function () {
	      var t = this.$options;t._asComponent && e(this, t.el), i(this, "$on", t.events), i(this, "$watch", t.watch);
	    }, t.prototype._initDOMHooks = function () {
	      this.$on("hook:attached", r), this.$on("hook:detached", o);
	    }, t.prototype._callHook = function (t) {
	      this.$emit("pre-hook:" + t);var e = this.$options[t];if (e) for (var i = 0, n = e.length; n > i; i++) {
	        e[i].call(this);
	      }this.$emit("hook:" + t);
	    };
	  }function fi() {}function pi(t, e, i, n, r, s) {
	    this.vm = e, this.el = i, this.descriptor = t, this.name = t.name, this.expression = t.expression, this.arg = t.arg, this.modifiers = t.modifiers, this.filters = t.filters, this.literal = this.modifiers && this.modifiers.literal, this._locked = !1, this._bound = !1, this._listeners = null, this._host = n, this._scope = r, this._frag = s;
	  }function di(t) {
	    t.prototype._updateRef = function (t) {
	      var e = this.$options._ref;if (e) {
	        var i = (this._scope || this._context).$refs;t ? i[e] === this && (i[e] = null) : i[e] = this;
	      }
	    }, t.prototype._compile = function (t) {
	      var e = this.$options,
	          i = t;if (t = ri(t, e), this._initElement(t), 1 !== t.nodeType || null === I(t, "v-pre")) {
	        var n = this._context && this._context.$options,
	            r = Ie(t, e, n);hi(this, e._content);var s,
	            o = this.constructor;e._linkerCachable && (s = o.linker, s || (s = o.linker = Fe(t, e)));var a = r(this, t, this._scope),
	            h = s ? s(this, t) : Fe(t, e)(this, t);this._unlinkFn = function () {
	          a(), h(!0);
	        }, e.replace && J(i, t), this._isCompiled = !0, this._callHook("compiled");
	      }
	    }, t.prototype._initElement = function (t) {
	      at(t) ? (this._isFragment = !0, this.$el = this._fragmentStart = t.firstChild, this._fragmentEnd = t.lastChild, 3 === this._fragmentStart.nodeType && (this._fragmentStart.data = this._fragmentEnd.data = ""), this._fragment = t) : this.$el = t, this.$el.__vue__ = this, this._callHook("beforeCompile");
	    }, t.prototype._bindDir = function (t, e, i, n, r) {
	      this._directives.push(new pi(t, this, e, i, n, r));
	    }, t.prototype._destroy = function (t, e) {
	      if (this._isBeingDestroyed) return void (e || this._cleanup());var i,
	          n,
	          r = this,
	          s = function s() {
	        !i || n || e || r._cleanup();
	      };t && this.$el && (n = !0, this.$remove(function () {
	        n = !1, s();
	      })), this._callHook("beforeDestroy"), this._isBeingDestroyed = !0;var o,
	          a = this.$parent;for (a && !a._isBeingDestroyed && (a.$children.$remove(this), this._updateRef(!0)), o = this.$children.length; o--;) {
	        this.$children[o].$destroy();
	      }for (this._propsUnlinkFn && this._propsUnlinkFn(), this._unlinkFn && this._unlinkFn(), o = this._watchers.length; o--;) {
	        this._watchers[o].teardown();
	      }this.$el && (this.$el.__vue__ = null), i = !0, s();
	    }, t.prototype._cleanup = function () {
	      this._isDestroyed || (this._frag && this._frag.children.$remove(this), this._data && this._data.__ob__ && this._data.__ob__.removeVm(this), this.$el = this.$parent = this.$root = this.$children = this._watchers = this._context = this._scope = this._directives = null, this._isDestroyed = !0, this._callHook("destroyed"), this.$off());
	    };
	  }function vi(t) {
	    t.prototype._applyFilters = function (t, e, i, n) {
	      var r, s, o, a, h, l, c, u, f;for (l = 0, c = i.length; c > l; l++) {
	        if (r = i[n ? c - l - 1 : l], s = gt(this.$options, "filters", r.name, !0), s && (s = n ? s.write : s.read || s, "function" == typeof s)) {
	          if (o = n ? [t, e] : [t], h = n ? 2 : 1, r.args) for (u = 0, f = r.args.length; f > u; u++) {
	            a = r.args[u], o[u + h] = a.dynamic ? this.$get(a.value) : a.value;
	          }t = s.apply(this, o);
	        }
	      }return t;
	    }, t.prototype._resolveComponent = function (e, i) {
	      var n;if (n = "function" == typeof e ? e : gt(this.$options, "components", e, !0)) if (n.options) i(n);else if (n.resolved) i(n.resolved);else if (n.requested) n.pendingCallbacks.push(i);else {
	        n.requested = !0;var r = n.pendingCallbacks = [i];n.call(this, function (e) {
	          g(e) && (e = t.extend(e)), n.resolved = e;for (var i = 0, s = r.length; s > i; i++) {
	            r[i](e);
	          }
	        }, function (t) {});
	      }
	    };
	  }function mi(t) {
	    function i(t) {
	      return JSON.parse(JSON.stringify(t));
	    }t.prototype.$get = function (t, e) {
	      var i = Ht(t);if (i) {
	        if (e) {
	          var n = this;return function () {
	            n.$arguments = d(arguments);var t = i.get.call(n, n);return n.$arguments = null, t;
	          };
	        }try {
	          return i.get.call(this, this);
	        } catch (r) {}
	      }
	    }, t.prototype.$set = function (t, e) {
	      var i = Ht(t, !0);i && i.set && i.set.call(this, this, e);
	    }, t.prototype.$delete = function (t) {
	      e(this._data, t);
	    }, t.prototype.$watch = function (t, e, i) {
	      var n,
	          r = this;"string" == typeof t && (n = A(t), t = n.expression);var s = new zt(r, t, e, { deep: i && i.deep, sync: i && i.sync, filters: n && n.filters, user: !i || i.user !== !1 });return i && i.immediate && e.call(r, s.value), function () {
	        s.teardown();
	      };
	    }, t.prototype.$eval = function (t, e) {
	      if (zs.test(t)) {
	        var i = A(t),
	            n = this.$get(i.expression, e);return i.filters ? this._applyFilters(n, null, i.filters) : n;
	      }return this.$get(t, e);
	    }, t.prototype.$interpolate = function (t) {
	      var e = N(t),
	          i = this;return e ? 1 === e.length ? i.$eval(e[0].value) + "" : e.map(function (t) {
	        return t.tag ? i.$eval(t.value) : t.value;
	      }).join("") : t;
	    }, t.prototype.$log = function (t) {
	      var e = t ? jt(this._data, t) : this._data;if (e && (e = i(e)), !t) {
	        var n;for (n in this.$options.computed) {
	          e[n] = i(this[n]);
	        }if (this._props) for (n in this._props) {
	          e[n] = i(this[n]);
	        }
	      }console.log(e);
	    };
	  }function gi(t) {
	    function e(t, e, n, r, s, o) {
	      e = i(e);var a = !H(e),
	          h = r === !1 || a ? s : o,
	          l = !a && !t._isAttached && !H(t.$el);return t._isFragment ? (st(t._fragmentStart, t._fragmentEnd, function (i) {
	        h(i, e, t);
	      }), n && n()) : h(t.$el, e, t, n), l && t._callHook("attached"), t;
	    }function i(t) {
	      return "string" == typeof t ? document.querySelector(t) : t;
	    }function n(t, e, i, n) {
	      e.appendChild(t), n && n();
	    }function r(t, e, i, n) {
	      V(t, e), n && n();
	    }function s(t, e, i) {
	      z(t), i && i();
	    }t.prototype.$nextTick = function (t) {
	      Qi(t, this);
	    }, t.prototype.$appendTo = function (t, i, r) {
	      return e(this, t, i, r, n, F);
	    }, t.prototype.$prependTo = function (t, e, n) {
	      return t = i(t), t.hasChildNodes() ? this.$before(t.firstChild, e, n) : this.$appendTo(t, e, n), this;
	    }, t.prototype.$before = function (t, i, n) {
	      return e(this, t, i, n, r, D);
	    }, t.prototype.$after = function (t, e, n) {
	      return t = i(t), t.nextSibling ? this.$before(t.nextSibling, e, n) : this.$appendTo(t.parentNode, e, n), this;
	    }, t.prototype.$remove = function (t, e) {
	      if (!this.$el.parentNode) return t && t();var i = this._isAttached && H(this.$el);i || (e = !1);var n = this,
	          r = function r() {
	        i && n._callHook("detached"), t && t();
	      };if (this._isFragment) ot(this._fragmentStart, this._fragmentEnd, this, this._fragment, r);else {
	        var o = e === !1 ? s : P;o(this.$el, this, r);
	      }return this;
	    };
	  }function _i(t) {
	    function e(t, e, n) {
	      var r = t.$parent;if (r && n && !i.test(e)) for (; r;) {
	        r._eventsCount[e] = (r._eventsCount[e] || 0) + n, r = r.$parent;
	      }
	    }t.prototype.$on = function (t, i) {
	      return (this._events[t] || (this._events[t] = [])).push(i), e(this, t, 1), this;
	    }, t.prototype.$once = function (t, e) {
	      function i() {
	        n.$off(t, i), e.apply(this, arguments);
	      }var n = this;return i.fn = e, this.$on(t, i), this;
	    }, t.prototype.$off = function (t, i) {
	      var n;if (!arguments.length) {
	        if (this.$parent) for (t in this._events) {
	          n = this._events[t], n && e(this, t, -n.length);
	        }return this._events = {}, this;
	      }if (n = this._events[t], !n) return this;if (1 === arguments.length) return e(this, t, -n.length), this._events[t] = null, this;for (var r, s = n.length; s--;) {
	        if (r = n[s], r === i || r.fn === i) {
	          e(this, t, -1), n.splice(s, 1);break;
	        }
	      }return this;
	    }, t.prototype.$emit = function (t) {
	      var e = "string" == typeof t;t = e ? t : t.name;var i = this._events[t],
	          n = e || !i;if (i) {
	        i = i.length > 1 ? d(i) : i;var r = e && i.some(function (t) {
	          return t._fromParent;
	        });r && (n = !1);for (var s = d(arguments, 1), o = 0, a = i.length; a > o; o++) {
	          var h = i[o],
	              l = h.apply(this, s);l !== !0 || r && !h._fromParent || (n = !0);
	        }
	      }return n;
	    }, t.prototype.$broadcast = function (t) {
	      var e = "string" == typeof t;if (t = e ? t : t.name, this._eventsCount[t]) {
	        var i = this.$children,
	            n = d(arguments);e && (n[0] = { name: t, source: this });for (var r = 0, s = i.length; s > r; r++) {
	          var o = i[r],
	              a = o.$emit.apply(o, n);a && o.$broadcast.apply(o, n);
	        }return this;
	      }
	    }, t.prototype.$dispatch = function (t) {
	      var e = this.$emit.apply(this, arguments);if (e) {
	        var i = this.$parent,
	            n = d(arguments);for (n[0] = { name: t, source: this }; i;) {
	          e = i.$emit.apply(i, n), i = e ? i.$parent : null;
	        }return this;
	      }
	    };var i = /^hook:/;
	  }function yi(t) {
	    function e() {
	      this._isAttached = !0, this._isReady = !0, this._callHook("ready");
	    }t.prototype.$mount = function (t) {
	      return this._isCompiled ? void 0 : (t = L(t), t || (t = document.createElement("div")), this._compile(t), this._initDOMHooks(), H(this.$el) ? (this._callHook("attached"), e.call(this)) : this.$once("hook:attached", e), this);
	    }, t.prototype.$destroy = function (t, e) {
	      this._destroy(t, e);
	    }, t.prototype.$compile = function (t, e, i, n) {
	      return Fe(t, this.$options, !0)(this, t, e, i, n);
	    };
	  }function bi(t) {
	    this._init(t);
	  }function wi(t, e, i) {
	    return i = i ? parseInt(i, 10) : 0, e = o(e), "number" == typeof e ? t.slice(i, i + e) : t;
	  }function Ci(t, e, i) {
	    if (t = Qs(t), null == e) return t;if ("function" == typeof e) return t.filter(e);e = ("" + e).toLowerCase();for (var n, r, s, o, a = "in" === i ? 3 : 2, h = Array.prototype.concat.apply([], d(arguments, a)), l = [], c = 0, u = t.length; u > c; c++) {
	      if (n = t[c], s = n && n.$value || n, o = h.length) {
	        for (; o--;) {
	          if (r = h[o], "$key" === r && ki(n.$key, e) || ki(jt(s, r), e)) {
	            l.push(n);break;
	          }
	        }
	      } else ki(n, e) && l.push(n);
	    }return l;
	  }function $i(t) {
	    function e(t, e, i) {
	      var r = n[i];return r && ("$key" !== r && (m(t) && "$value" in t && (t = t.$value), m(e) && "$value" in e && (e = e.$value)), t = m(t) ? jt(t, r) : t, e = m(e) ? jt(e, r) : e), t === e ? 0 : t > e ? s : -s;
	    }var _i2 = null,
	        n = void 0;t = Qs(t);var r = d(arguments, 1),
	        s = r[r.length - 1];"number" == typeof s ? (s = 0 > s ? -1 : 1, r = r.length > 1 ? r.slice(0, -1) : r) : s = 1;var o = r[0];return o ? ("function" == typeof o ? _i2 = function i(t, e) {
	      return o(t, e) * s;
	    } : (n = Array.prototype.concat.apply([], r), _i2 = function i(t, r, s) {
	      return s = s || 0, s >= n.length - 1 ? e(t, r, s) : e(t, r, s) || _i2(t, r, s + 1);
	    }), t.slice().sort(_i2)) : t;
	  }function ki(t, e) {
	    var i;if (g(t)) {
	      var n = Object.keys(t);for (i = n.length; i--;) {
	        if (ki(t[n[i]], e)) return !0;
	      }
	    } else if (Fi(t)) {
	      for (i = t.length; i--;) {
	        if (ki(t[i], e)) return !0;
	      }
	    } else if (null != t) return t.toString().toLowerCase().indexOf(e) > -1;
	  }function xi(i) {
	    function n(t) {
	      return new Function("return function " + f(t) + " (options) { this._init(options) }")();
	    }i.options = { directives: vs, elementDirectives: qs, filters: Zs, transitions: {}, components: {}, partials: {}, replace: !0 }, i.util = Pn, i.config = Cn, i.set = t, i["delete"] = e, i.nextTick = Qi, i.compiler = Vs, i.FragmentFactory = re, i.internalDirectives = Fs, i.parsers = { path: Yn, text: yn, template: Tr, directive: pn, expression: fr }, i.cid = 0;var r = 1;i.extend = function (t) {
	      t = t || {};var e = this,
	          i = 0 === e.cid;if (i && t._Ctor) return t._Ctor;var s = t.name || e.options.name,
	          o = n(s || "VueComponent");return o.prototype = Object.create(e.prototype), o.prototype.constructor = o, o.cid = r++, o.options = mt(e.options, t), o["super"] = e, o.extend = e.extend, Cn._assetTypes.forEach(function (t) {
	        o[t] = e[t];
	      }), s && (o.options.components[s] = o), i && (t._Ctor = o), o;
	    }, i.use = function (t) {
	      if (!t.installed) {
	        var e = d(arguments, 1);return e.unshift(this), "function" == typeof t.install ? t.install.apply(t, e) : t.apply(null, e), t.installed = !0, this;
	      }
	    }, i.mixin = function (t) {
	      i.options = mt(i.options, t);
	    }, Cn._assetTypes.forEach(function (t) {
	      i[t] = function (e, n) {
	        return n ? ("component" === t && g(n) && (n.name = e, n = i.extend(n)), this.options[t + "s"][e] = n, n) : this.options[t + "s"][e];
	      };
	    }), v(i.transition, kn);
	  }var Ai = Object.prototype.hasOwnProperty,
	      Oi = /^\s?(true|false|-?[\d\.]+|'[^']*'|"[^"]*")\s?$/,
	      Ti = /-(\w)/g,
	      Ni = /([a-z\d])([A-Z])/g,
	      ji = /(?:^|[-_\/])(\w)/g,
	      Ei = Object.prototype.toString,
	      Si = "[object Object]",
	      Fi = Array.isArray,
	      Di = "__proto__" in {},
	      Pi = "undefined" != typeof window && "[object Object]" !== Object.prototype.toString.call(window),
	      Ri = Pi && window.__VUE_DEVTOOLS_GLOBAL_HOOK__,
	      Li = Pi && window.navigator.userAgent.toLowerCase(),
	      Hi = Li && Li.indexOf("msie 9.0") > 0,
	      Ii = Li && Li.indexOf("android") > 0,
	      Mi = Li && /(iphone|ipad|ipod|ios)/i.test(Li),
	      Wi = Li && Li.indexOf("micromessenger") > 0,
	      Vi = void 0,
	      Bi = void 0,
	      zi = void 0,
	      Ui = void 0;if (Pi && !Hi) {
	    var Ji = void 0 === window.ontransitionend && void 0 !== window.onwebkittransitionend,
	        qi = void 0 === window.onanimationend && void 0 !== window.onwebkitanimationend;Vi = Ji ? "WebkitTransition" : "transition", Bi = Ji ? "webkitTransitionEnd" : "transitionend", zi = qi ? "WebkitAnimation" : "animation", Ui = qi ? "webkitAnimationEnd" : "animationend";
	  }var Qi = function () {
	    function t() {
	      n = !1;var t = i.slice(0);i = [];for (var e = 0; e < t.length; e++) {
	        t[e]();
	      }
	    }var e,
	        i = [],
	        n = !1;if ("undefined" == typeof MutationObserver || Wi && Mi) {
	      var r = Pi ? window : "undefined" != typeof global ? global : {};e = r.setImmediate || setTimeout;
	    } else {
	      var s = 1,
	          o = new MutationObserver(t),
	          a = document.createTextNode(s);o.observe(a, { characterData: !0 }), e = function e() {
	        s = (s + 1) % 2, a.data = s;
	      };
	    }return function (r, s) {
	      var o = s ? function () {
	        r.call(s);
	      } : r;i.push(o), n || (n = !0, e(t, 0));
	    };
	  }(),
	      Gi = void 0;"undefined" != typeof Set && Set.toString().match(/native code/) ? Gi = Set : (Gi = function Gi() {
	    this.set = Object.create(null);
	  }, Gi.prototype.has = function (t) {
	    return void 0 !== this.set[t];
	  }, Gi.prototype.add = function (t) {
	    this.set[t] = 1;
	  }, Gi.prototype.clear = function () {
	    this.set = Object.create(null);
	  });var Zi = $.prototype;Zi.put = function (t, e) {
	    var i;this.size === this.limit && (i = this.shift());var n = this.get(t, !0);return n || (n = { key: t }, this._keymap[t] = n, this.tail ? (this.tail.newer = n, n.older = this.tail) : this.head = n, this.tail = n, this.size++), n.value = e, i;
	  }, Zi.shift = function () {
	    var t = this.head;return t && (this.head = this.head.newer, this.head.older = void 0, t.newer = t.older = void 0, this._keymap[t.key] = void 0, this.size--), t;
	  }, Zi.get = function (t, e) {
	    var i = this._keymap[t];if (void 0 !== i) return i === this.tail ? e ? i : i.value : (i.newer && (i === this.head && (this.head = i.newer), i.newer.older = i.older), i.older && (i.older.newer = i.newer), i.newer = void 0, i.older = this.tail, this.tail && (this.tail.newer = i), this.tail = i, e ? i : i.value);
	  };var Xi,
	      Yi,
	      Ki,
	      tn,
	      en,
	      nn,
	      rn,
	      sn,
	      on,
	      an,
	      hn,
	      ln,
	      cn = new $(1e3),
	      un = /[^\s'"]+|'[^']*'|"[^"]*"/g,
	      fn = /^in$|^-?\d+/,
	      pn = Object.freeze({ parseDirective: A }),
	      dn = /[-.*+?^${}()|[\]\/\\]/g,
	      vn = void 0,
	      mn = void 0,
	      gn = void 0,
	      _n = /[^|]\|[^|]/,
	      yn = Object.freeze({ compileRegex: T, parseText: N, tokensToExp: j }),
	      bn = ["{{", "}}"],
	      wn = ["{{{", "}}}"],
	      Cn = Object.defineProperties({ debug: !1, silent: !1, async: !0, warnExpressionErrors: !0, devtools: !1, _delimitersChanged: !0, _assetTypes: ["component", "directive", "elementDirective", "filter", "transition", "partial"], _propBindingModes: { ONE_WAY: 0, TWO_WAY: 1, ONE_TIME: 2 }, _maxUpdateCount: 100 }, { delimiters: { get: function get() {
	        return bn;
	      }, set: function set(t) {
	        bn = t, T();
	      }, configurable: !0, enumerable: !0 }, unsafeDelimiters: { get: function get() {
	        return wn;
	      }, set: function set(t) {
	        wn = t, T();
	      }, configurable: !0, enumerable: !0 } }),
	      $n = void 0,
	      kn = Object.freeze({ appendWithTransition: F, beforeWithTransition: D, removeWithTransition: P, applyTransition: R }),
	      xn = /^v-ref:/,
	      An = /^(div|p|span|img|a|b|i|br|ul|ol|li|h1|h2|h3|h4|h5|h6|code|pre|table|th|td|tr|form|label|input|select|option|nav|article|section|header|footer)$/i,
	      On = /^(slot|partial|component)$/i,
	      Tn = Cn.optionMergeStrategies = Object.create(null);Tn.data = function (t, e, i) {
	    return i ? t || e ? function () {
	      var n = "function" == typeof e ? e.call(i) : e,
	          r = "function" == typeof t ? t.call(i) : void 0;return n ? ut(n, r) : r;
	    } : void 0 : e ? "function" != typeof e ? t : t ? function () {
	      return ut(e.call(this), t.call(this));
	    } : e : t;
	  }, Tn.el = function (t, e, i) {
	    if (i || !e || "function" == typeof e) {
	      var n = e || t;return i && "function" == typeof n ? n.call(i) : n;
	    }
	  }, Tn.init = Tn.created = Tn.ready = Tn.attached = Tn.detached = Tn.beforeCompile = Tn.compiled = Tn.beforeDestroy = Tn.destroyed = Tn.activate = function (t, e) {
	    return e ? t ? t.concat(e) : Fi(e) ? e : [e] : t;
	  }, Cn._assetTypes.forEach(function (t) {
	    Tn[t + "s"] = ft;
	  }), Tn.watch = Tn.events = function (t, e) {
	    if (!e) return t;if (!t) return e;var i = {};v(i, t);for (var n in e) {
	      var r = i[n],
	          s = e[n];r && !Fi(r) && (r = [r]), i[n] = r ? r.concat(s) : [s];
	    }return i;
	  }, Tn.props = Tn.methods = Tn.computed = function (t, e) {
	    if (!e) return t;if (!t) return e;var i = Object.create(null);return v(i, t), v(i, e), i;
	  };var Nn = function Nn(t, e) {
	    return void 0 === e ? t : e;
	  },
	      jn = 0;_t.target = null, _t.prototype.addSub = function (t) {
	    this.subs.push(t);
	  }, _t.prototype.removeSub = function (t) {
	    this.subs.$remove(t);
	  }, _t.prototype.depend = function () {
	    _t.target.addDep(this);
	  }, _t.prototype.notify = function () {
	    for (var t = d(this.subs), e = 0, i = t.length; i > e; e++) {
	      t[e].update();
	    }
	  };var En = Array.prototype,
	      Sn = Object.create(En);["push", "pop", "shift", "unshift", "splice", "sort", "reverse"].forEach(function (t) {
	    var e = En[t];_(Sn, t, function () {
	      for (var i = arguments.length, n = new Array(i); i--;) {
	        n[i] = arguments[i];
	      }var r,
	          s = e.apply(this, n),
	          o = this.__ob__;switch (t) {case "push":
	          r = n;break;case "unshift":
	          r = n;break;case "splice":
	          r = n.slice(2);}return r && o.observeArray(r), o.dep.notify(), s;
	    });
	  }), _(En, "$set", function (t, e) {
	    return t >= this.length && (this.length = Number(t) + 1), this.splice(t, 1, e)[0];
	  }), _(En, "$remove", function (t) {
	    if (this.length) {
	      var e = b(this, t);return e > -1 ? this.splice(e, 1) : void 0;
	    }
	  });var Fn = Object.getOwnPropertyNames(Sn),
	      Dn = !0;bt.prototype.walk = function (t) {
	    for (var e = Object.keys(t), i = 0, n = e.length; n > i; i++) {
	      this.convert(e[i], t[e[i]]);
	    }
	  }, bt.prototype.observeArray = function (t) {
	    for (var e = 0, i = t.length; i > e; e++) {
	      $t(t[e]);
	    }
	  }, bt.prototype.convert = function (t, e) {
	    kt(this.value, t, e);
	  }, bt.prototype.addVm = function (t) {
	    (this.vms || (this.vms = [])).push(t);
	  }, bt.prototype.removeVm = function (t) {
	    this.vms.$remove(t);
	  };var Pn = Object.freeze({ defineReactive: kt, set: t, del: e, hasOwn: i, isLiteral: n, isReserved: r, _toString: s, toNumber: o, toBoolean: a, stripQuotes: h, camelize: l, hyphenate: u, classify: f, bind: p, toArray: d, extend: v, isObject: m, isPlainObject: g, def: _, debounce: y, indexOf: b, cancellable: w, looseEqual: C, isArray: Fi, hasProto: Di, inBrowser: Pi, devtools: Ri, isIE9: Hi, isAndroid: Ii, isIos: Mi, isWechat: Wi, get transitionProp() {
	      return Vi;
	    }, get transitionEndEvent() {
	      return Bi;
	    }, get animationProp() {
	      return zi;
	    }, get animationEndEvent() {
	      return Ui;
	    }, nextTick: Qi, get _Set() {
	      return Gi;
	    }, query: L, inDoc: H, getAttr: I, getBindAttr: M, hasBindAttr: W, before: V, after: B, remove: z, prepend: U, replace: J, on: q, off: Q, setClass: Z, addClass: X, removeClass: Y, extractContent: K, trimNode: tt, isTemplate: it, createAnchor: nt, findRef: rt, mapNodeRange: st, removeNodeRange: ot, isFragment: at, getOuterHTML: ht, mergeOptions: mt, resolveAsset: gt, checkComponentAttr: lt, commonTagRE: An, reservedTagRE: On, warn: $n }),
	      Rn = 0,
	      Ln = new $(1e3),
	      Hn = 0,
	      In = 1,
	      Mn = 2,
	      Wn = 3,
	      Vn = 0,
	      Bn = 1,
	      zn = 2,
	      Un = 3,
	      Jn = 4,
	      qn = 5,
	      Qn = 6,
	      Gn = 7,
	      Zn = 8,
	      Xn = [];Xn[Vn] = { ws: [Vn], ident: [Un, Hn], "[": [Jn], eof: [Gn] }, Xn[Bn] = { ws: [Bn], ".": [zn], "[": [Jn], eof: [Gn] }, Xn[zn] = { ws: [zn], ident: [Un, Hn] }, Xn[Un] = { ident: [Un, Hn], 0: [Un, Hn], number: [Un, Hn], ws: [Bn, In], ".": [zn, In], "[": [Jn, In], eof: [Gn, In] }, Xn[Jn] = { "'": [qn, Hn], '"': [Qn, Hn], "[": [Jn, Mn], "]": [Bn, Wn], eof: Zn, "else": [Jn, Hn] }, Xn[qn] = { "'": [Jn, Hn], eof: Zn, "else": [qn, Hn] }, Xn[Qn] = { '"': [Jn, Hn], eof: Zn, "else": [Qn, Hn] };var Yn = Object.freeze({ parsePath: Nt, getPath: jt, setPath: Et }),
	      Kn = new $(1e3),
	      tr = "Math,Date,this,true,false,null,undefined,Infinity,NaN,isNaN,isFinite,decodeURI,decodeURIComponent,encodeURI,encodeURIComponent,parseInt,parseFloat",
	      er = new RegExp("^(" + tr.replace(/,/g, "\\b|") + "\\b)"),
	      ir = "break,case,class,catch,const,continue,debugger,default,delete,do,else,export,extends,finally,for,function,if,import,in,instanceof,let,return,super,switch,throw,try,var,while,with,yield,enum,await,implements,package,protected,static,interface,private,public",
	      nr = new RegExp("^(" + ir.replace(/,/g, "\\b|") + "\\b)"),
	      rr = /\s/g,
	      sr = /\n/g,
	      or = /[\{,]\s*[\w\$_]+\s*:|('(?:[^'\\]|\\.)*'|"(?:[^"\\]|\\.)*"|`(?:[^`\\]|\\.)*\$\{|\}(?:[^`\\]|\\.)*`|`(?:[^`\\]|\\.)*`)|new |typeof |void /g,
	      ar = /"(\d+)"/g,
	      hr = /^[A-Za-z_$][\w$]*(?:\.[A-Za-z_$][\w$]*|\['.*?'\]|\[".*?"\]|\[\d+\]|\[[A-Za-z_$][\w$]*\])*$/,
	      lr = /[^\w$\.](?:[A-Za-z_$][\w$]*)/g,
	      cr = /^(?:true|false)$/,
	      ur = [],
	      fr = Object.freeze({ parseExpression: Ht, isSimplePath: It }),
	      pr = [],
	      dr = [],
	      vr = {},
	      mr = {},
	      gr = !1,
	      _r = 0;zt.prototype.get = function () {
	    this.beforeGet();var t,
	        e = this.scope || this.vm;try {
	      t = this.getter.call(e, e);
	    } catch (i) {}return this.deep && Ut(t), this.preProcess && (t = this.preProcess(t)), this.filters && (t = e._applyFilters(t, null, this.filters, !1)), this.postProcess && (t = this.postProcess(t)), this.afterGet(), t;
	  }, zt.prototype.set = function (t) {
	    var e = this.scope || this.vm;this.filters && (t = e._applyFilters(t, this.value, this.filters, !0));try {
	      this.setter.call(e, e, t);
	    } catch (i) {}var n = e.$forContext;if (n && n.alias === this.expression) {
	      if (n.filters) return;n._withLock(function () {
	        e.$key ? n.rawValue[e.$key] = t : n.rawValue.$set(e.$index, t);
	      });
	    }
	  }, zt.prototype.beforeGet = function () {
	    _t.target = this;
	  }, zt.prototype.addDep = function (t) {
	    var e = t.id;this.newDepIds.has(e) || (this.newDepIds.add(e), this.newDeps.push(t), this.depIds.has(e) || t.addSub(this));
	  }, zt.prototype.afterGet = function () {
	    _t.target = null;for (var t = this.deps.length; t--;) {
	      var e = this.deps[t];this.newDepIds.has(e.id) || e.removeSub(this);
	    }var i = this.depIds;this.depIds = this.newDepIds, this.newDepIds = i, this.newDepIds.clear(), i = this.deps, this.deps = this.newDeps, this.newDeps = i, this.newDeps.length = 0;
	  }, zt.prototype.update = function (t) {
	    this.lazy ? this.dirty = !0 : this.sync || !Cn.async ? this.run() : (this.shallow = this.queued ? t ? this.shallow : !1 : !!t, this.queued = !0, Bt(this));
	  }, zt.prototype.run = function () {
	    if (this.active) {
	      var t = this.get();if (t !== this.value || (m(t) || this.deep) && !this.shallow) {
	        var e = this.value;this.value = t;this.prevError;this.cb.call(this.vm, t, e);
	      }this.queued = this.shallow = !1;
	    }
	  }, zt.prototype.evaluate = function () {
	    var t = _t.target;this.value = this.get(), this.dirty = !1, _t.target = t;
	  }, zt.prototype.depend = function () {
	    for (var t = this.deps.length; t--;) {
	      this.deps[t].depend();
	    }
	  }, zt.prototype.teardown = function () {
	    if (this.active) {
	      this.vm._isBeingDestroyed || this.vm._vForRemoving || this.vm._watchers.$remove(this);for (var t = this.deps.length; t--;) {
	        this.deps[t].removeSub(this);
	      }this.active = !1, this.vm = this.cb = this.value = null;
	    }
	  };var yr = new Gi(),
	      br = { bind: function bind() {
	      this.attr = 3 === this.el.nodeType ? "data" : "textContent";
	    }, update: function update(t) {
	      this.el[this.attr] = s(t);
	    } },
	      wr = new $(1e3),
	      Cr = new $(1e3),
	      $r = { efault: [0, "", ""], legend: [1, "<fieldset>", "</fieldset>"], tr: [2, "<table><tbody>", "</tbody></table>"], col: [2, "<table><tbody></tbody><colgroup>", "</colgroup></table>"] };$r.td = $r.th = [3, "<table><tbody><tr>", "</tr></tbody></table>"], $r.option = $r.optgroup = [1, '<select multiple="multiple">', "</select>"], $r.thead = $r.tbody = $r.colgroup = $r.caption = $r.tfoot = [1, "<table>", "</table>"], $r.g = $r.defs = $r.symbol = $r.use = $r.image = $r.text = $r.circle = $r.ellipse = $r.line = $r.path = $r.polygon = $r.polyline = $r.rect = [1, '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:ev="http://www.w3.org/2001/xml-events"version="1.1">', "</svg>"];var kr = /<([\w:-]+)/,
	      xr = /&#?\w+?;/,
	      Ar = function () {
	    if (Pi) {
	      var t = document.createElement("div");return t.innerHTML = "<template>1</template>", !t.cloneNode(!0).firstChild.innerHTML;
	    }return !1;
	  }(),
	      Or = function () {
	    if (Pi) {
	      var t = document.createElement("textarea");return t.placeholder = "t", "t" === t.cloneNode(!0).value;
	    }return !1;
	  }(),
	      Tr = Object.freeze({ cloneNode: Gt, parseTemplate: Zt }),
	      Nr = { bind: function bind() {
	      8 === this.el.nodeType && (this.nodes = [], this.anchor = nt("v-html"), J(this.el, this.anchor));
	    }, update: function update(t) {
	      t = s(t), this.nodes ? this.swap(t) : this.el.innerHTML = t;
	    }, swap: function swap(t) {
	      for (var e = this.nodes.length; e--;) {
	        z(this.nodes[e]);
	      }var i = Zt(t, !0, !0);this.nodes = d(i.childNodes), V(i, this.anchor);
	    } };Xt.prototype.callHook = function (t) {
	    var e, i;for (e = 0, i = this.childFrags.length; i > e; e++) {
	      this.childFrags[e].callHook(t);
	    }for (e = 0, i = this.children.length; i > e; e++) {
	      t(this.children[e]);
	    }
	  }, Xt.prototype.beforeRemove = function () {
	    var t, e;for (t = 0, e = this.childFrags.length; e > t; t++) {
	      this.childFrags[t].beforeRemove(!1);
	    }for (t = 0, e = this.children.length; e > t; t++) {
	      this.children[t].$destroy(!1, !0);
	    }var i = this.unlink.dirs;for (t = 0, e = i.length; e > t; t++) {
	      i[t]._watcher && i[t]._watcher.teardown();
	    }
	  }, Xt.prototype.destroy = function () {
	    this.parentFrag && this.parentFrag.childFrags.$remove(this), this.node.__v_frag = null, this.unlink();
	  };var jr = new $(5e3);re.prototype.create = function (t, e, i) {
	    var n = Gt(this.template);return new Xt(this.linker, this.vm, n, t, e, i);
	  };var Er = 700,
	      Sr = 800,
	      Fr = 850,
	      Dr = 1100,
	      Pr = 1500,
	      Rr = 1500,
	      Lr = 1750,
	      Hr = 2100,
	      Ir = 2200,
	      Mr = 2300,
	      Wr = 0,
	      Vr = { priority: Ir, terminal: !0, params: ["track-by", "stagger", "enter-stagger", "leave-stagger"], bind: function bind() {
	      var t = this.expression.match(/(.*) (?:in|of) (.*)/);if (t) {
	        var e = t[1].match(/\((.*),(.*)\)/);e ? (this.iterator = e[1].trim(), this.alias = e[2].trim()) : this.alias = t[1].trim(), this.expression = t[2];
	      }if (this.alias) {
	        this.id = "__v-for__" + ++Wr;var i = this.el.tagName;this.isOption = ("OPTION" === i || "OPTGROUP" === i) && "SELECT" === this.el.parentNode.tagName, this.start = nt("v-for-start"), this.end = nt("v-for-end"), J(this.el, this.end), V(this.start, this.end), this.cache = Object.create(null), this.factory = new re(this.vm, this.el);
	      }
	    }, update: function update(t) {
	      this.diff(t), this.updateRef(), this.updateModel();
	    }, diff: function diff(t) {
	      var e,
	          n,
	          r,
	          s,
	          o,
	          a,
	          h = t[0],
	          l = this.fromObject = m(h) && i(h, "$key") && i(h, "$value"),
	          c = this.params.trackBy,
	          u = this.frags,
	          f = this.frags = new Array(t.length),
	          p = this.alias,
	          d = this.iterator,
	          v = this.start,
	          g = this.end,
	          _ = H(v),
	          y = !u;for (e = 0, n = t.length; n > e; e++) {
	        h = t[e], s = l ? h.$key : null, o = l ? h.$value : h, a = !m(o), r = !y && this.getCachedFrag(o, e, s), r ? (r.reused = !0, r.scope.$index = e, s && (r.scope.$key = s), d && (r.scope[d] = null !== s ? s : e), (c || l || a) && yt(function () {
	          r.scope[p] = o;
	        })) : (r = this.create(o, p, e, s), r.fresh = !y), f[e] = r, y && r.before(g);
	      }if (!y) {
	        var b = 0,
	            w = u.length - f.length;for (this.vm._vForRemoving = !0, e = 0, n = u.length; n > e; e++) {
	          r = u[e], r.reused || (this.deleteCachedFrag(r), this.remove(r, b++, w, _));
	        }this.vm._vForRemoving = !1, b && (this.vm._watchers = this.vm._watchers.filter(function (t) {
	          return t.active;
	        }));var C,
	            $,
	            k,
	            x = 0;for (e = 0, n = f.length; n > e; e++) {
	          r = f[e], C = f[e - 1], $ = C ? C.staggerCb ? C.staggerAnchor : C.end || C.node : v, r.reused && !r.staggerCb ? (k = se(r, v, this.id), k === C || k && se(k, v, this.id) === C || this.move(r, $)) : this.insert(r, x++, $, _), r.reused = r.fresh = !1;
	        }
	      }
	    }, create: function create(t, e, i, n) {
	      var r = this._host,
	          s = this._scope || this.vm,
	          o = Object.create(s);o.$refs = Object.create(s.$refs), o.$els = Object.create(s.$els), o.$parent = s, o.$forContext = this, yt(function () {
	        kt(o, e, t);
	      }), kt(o, "$index", i), n ? kt(o, "$key", n) : o.$key && _(o, "$key", null), this.iterator && kt(o, this.iterator, null !== n ? n : i);var a = this.factory.create(r, o, this._frag);return a.forId = this.id, this.cacheFrag(t, a, i, n), a;
	    }, updateRef: function updateRef() {
	      var t = this.descriptor.ref;if (t) {
	        var e,
	            i = (this._scope || this.vm).$refs;this.fromObject ? (e = {}, this.frags.forEach(function (t) {
	          e[t.scope.$key] = oe(t);
	        })) : e = this.frags.map(oe), i[t] = e;
	      }
	    }, updateModel: function updateModel() {
	      if (this.isOption) {
	        var t = this.start.parentNode,
	            e = t && t.__v_model;e && e.forceUpdate();
	      }
	    }, insert: function insert(t, e, i, n) {
	      t.staggerCb && (t.staggerCb.cancel(), t.staggerCb = null);var r = this.getStagger(t, e, null, "enter");if (n && r) {
	        var s = t.staggerAnchor;s || (s = t.staggerAnchor = nt("stagger-anchor"), s.__v_frag = t), B(s, i);var o = t.staggerCb = w(function () {
	          t.staggerCb = null, t.before(s), z(s);
	        });setTimeout(o, r);
	      } else {
	        var a = i.nextSibling;a || (B(this.end, i), a = this.end), t.before(a);
	      }
	    }, remove: function remove(t, e, i, n) {
	      if (t.staggerCb) return t.staggerCb.cancel(), void (t.staggerCb = null);var r = this.getStagger(t, e, i, "leave");if (n && r) {
	        var s = t.staggerCb = w(function () {
	          t.staggerCb = null, t.remove();
	        });setTimeout(s, r);
	      } else t.remove();
	    }, move: function move(t, e) {
	      e.nextSibling || this.end.parentNode.appendChild(this.end), t.before(e.nextSibling, !1);
	    }, cacheFrag: function cacheFrag(t, e, n, r) {
	      var s,
	          o = this.params.trackBy,
	          a = this.cache,
	          h = !m(t);r || o || h ? (s = he(n, r, t, o), a[s] || (a[s] = e)) : (s = this.id, i(t, s) ? null === t[s] && (t[s] = e) : Object.isExtensible(t) && _(t, s, e)), e.raw = t;
	    }, getCachedFrag: function getCachedFrag(t, e, i) {
	      var n,
	          r = this.params.trackBy,
	          s = !m(t);if (i || r || s) {
	        var o = he(e, i, t, r);n = this.cache[o];
	      } else n = t[this.id];return n && (n.reused || n.fresh), n;
	    }, deleteCachedFrag: function deleteCachedFrag(t) {
	      var e = t.raw,
	          n = this.params.trackBy,
	          r = t.scope,
	          s = r.$index,
	          o = i(r, "$key") && r.$key,
	          a = !m(e);if (n || o || a) {
	        var h = he(s, o, e, n);this.cache[h] = null;
	      } else e[this.id] = null, t.raw = null;
	    }, getStagger: function getStagger(t, e, i, n) {
	      n += "Stagger";var r = t.node.__v_trans,
	          s = r && r.hooks,
	          o = s && (s[n] || s.stagger);return o ? o.call(t, e, i) : e * parseInt(this.params[n] || this.params.stagger, 10);
	    }, _preProcess: function _preProcess(t) {
	      return this.rawValue = t, t;
	    }, _postProcess: function _postProcess(t) {
	      if (Fi(t)) return t;if (g(t)) {
	        for (var e, i = Object.keys(t), n = i.length, r = new Array(n); n--;) {
	          e = i[n], r[n] = { $key: e, $value: t[e] };
	        }return r;
	      }return "number" != typeof t || isNaN(t) || (t = ae(t)), t || [];
	    }, unbind: function unbind() {
	      if (this.descriptor.ref && ((this._scope || this.vm).$refs[this.descriptor.ref] = null), this.frags) for (var t, e = this.frags.length; e--;) {
	        t = this.frags[e], this.deleteCachedFrag(t), t.destroy();
	      }
	    } },
	      Br = { priority: Hr, terminal: !0, bind: function bind() {
	      var t = this.el;if (t.__vue__) this.invalid = !0;else {
	        var e = t.nextElementSibling;e && null !== I(e, "v-else") && (z(e), this.elseEl = e), this.anchor = nt("v-if"), J(t, this.anchor);
	      }
	    }, update: function update(t) {
	      this.invalid || (t ? this.frag || this.insert() : this.remove());
	    }, insert: function insert() {
	      this.elseFrag && (this.elseFrag.remove(), this.elseFrag = null), this.factory || (this.factory = new re(this.vm, this.el)), this.frag = this.factory.create(this._host, this._scope, this._frag), this.frag.before(this.anchor);
	    }, remove: function remove() {
	      this.frag && (this.frag.remove(), this.frag = null), this.elseEl && !this.elseFrag && (this.elseFactory || (this.elseFactory = new re(this.elseEl._context || this.vm, this.elseEl)), this.elseFrag = this.elseFactory.create(this._host, this._scope, this._frag), this.elseFrag.before(this.anchor));
	    }, unbind: function unbind() {
	      this.frag && this.frag.destroy(), this.elseFrag && this.elseFrag.destroy();
	    } },
	      zr = { bind: function bind() {
	      var t = this.el.nextElementSibling;t && null !== I(t, "v-else") && (this.elseEl = t);
	    }, update: function update(t) {
	      this.apply(this.el, t), this.elseEl && this.apply(this.elseEl, !t);
	    }, apply: function apply(t, e) {
	      function i() {
	        t.style.display = e ? "" : "none";
	      }H(t) ? R(t, e ? 1 : -1, i, this.vm) : i();
	    } },
	      Ur = { bind: function bind() {
	      var t = this,
	          e = this.el,
	          i = "range" === e.type,
	          n = this.params.lazy,
	          r = this.params.number,
	          s = this.params.debounce,
	          a = !1;if (Ii || i || (this.on("compositionstart", function () {
	        a = !0;
	      }), this.on("compositionend", function () {
	        a = !1, n || t.listener();
	      })), this.focused = !1, i || n || (this.on("focus", function () {
	        t.focused = !0;
	      }), this.on("blur", function () {
	        t.focused = !1, t._frag && !t._frag.inserted || t.rawListener();
	      })), this.listener = this.rawListener = function () {
	        if (!a && t._bound) {
	          var n = r || i ? o(e.value) : e.value;t.set(n), Qi(function () {
	            t._bound && !t.focused && t.update(t._watcher.value);
	          });
	        }
	      }, s && (this.listener = y(this.listener, s)), this.hasjQuery = "function" == typeof jQuery, this.hasjQuery) {
	        var h = jQuery.fn.on ? "on" : "bind";jQuery(e)[h]("change", this.rawListener), n || jQuery(e)[h]("input", this.listener);
	      } else this.on("change", this.rawListener), n || this.on("input", this.listener);!n && Hi && (this.on("cut", function () {
	        Qi(t.listener);
	      }), this.on("keyup", function (e) {
	        46 !== e.keyCode && 8 !== e.keyCode || t.listener();
	      })), (e.hasAttribute("value") || "TEXTAREA" === e.tagName && e.value.trim()) && (this.afterBind = this.listener);
	    }, update: function update(t) {
	      this.el.value = s(t);
	    }, unbind: function unbind() {
	      var t = this.el;if (this.hasjQuery) {
	        var e = jQuery.fn.off ? "off" : "unbind";jQuery(t)[e]("change", this.listener), jQuery(t)[e]("input", this.listener);
	      }
	    } },
	      Jr = { bind: function bind() {
	      var t = this,
	          e = this.el;this.getValue = function () {
	        if (e.hasOwnProperty("_value")) return e._value;var i = e.value;return t.params.number && (i = o(i)), i;
	      }, this.listener = function () {
	        t.set(t.getValue());
	      }, this.on("change", this.listener), e.hasAttribute("checked") && (this.afterBind = this.listener);
	    }, update: function update(t) {
	      this.el.checked = C(t, this.getValue());
	    } },
	      qr = { bind: function bind() {
	      var t = this,
	          e = this.el;this.forceUpdate = function () {
	        t._watcher && t.update(t._watcher.get());
	      };var i = this.multiple = e.hasAttribute("multiple");this.listener = function () {
	        var n = le(e, i);n = t.params.number ? Fi(n) ? n.map(o) : o(n) : n, t.set(n);
	      }, this.on("change", this.listener);var n = le(e, i, !0);(i && n.length || !i && null !== n) && (this.afterBind = this.listener), this.vm.$on("hook:attached", this.forceUpdate);
	    }, update: function update(t) {
	      var e = this.el;e.selectedIndex = -1;for (var i, n, r = this.multiple && Fi(t), s = e.options, o = s.length; o--;) {
	        i = s[o], n = i.hasOwnProperty("_value") ? i._value : i.value, i.selected = r ? ce(t, n) > -1 : C(t, n);
	      }
	    }, unbind: function unbind() {
	      this.vm.$off("hook:attached", this.forceUpdate);
	    } },
	      Qr = { bind: function bind() {
	      function t() {
	        var t = i.checked;return t && i.hasOwnProperty("_trueValue") ? i._trueValue : !t && i.hasOwnProperty("_falseValue") ? i._falseValue : t;
	      }var e = this,
	          i = this.el;this.getValue = function () {
	        return i.hasOwnProperty("_value") ? i._value : e.params.number ? o(i.value) : i.value;
	      }, this.listener = function () {
	        var n = e._watcher.value;if (Fi(n)) {
	          var r = e.getValue();i.checked ? b(n, r) < 0 && n.push(r) : n.$remove(r);
	        } else e.set(t());
	      }, this.on("change", this.listener), i.hasAttribute("checked") && (this.afterBind = this.listener);
	    }, update: function update(t) {
	      var e = this.el;Fi(t) ? e.checked = b(t, this.getValue()) > -1 : e.hasOwnProperty("_trueValue") ? e.checked = C(t, e._trueValue) : e.checked = !!t;
	    } },
	      Gr = { text: Ur, radio: Jr, select: qr, checkbox: Qr },
	      Zr = { priority: Sr, twoWay: !0, handlers: Gr, params: ["lazy", "number", "debounce"], bind: function bind() {
	      this.checkFilters(), this.hasRead && !this.hasWrite;var t,
	          e = this.el,
	          i = e.tagName;if ("INPUT" === i) t = Gr[e.type] || Gr.text;else if ("SELECT" === i) t = Gr.select;else {
	        if ("TEXTAREA" !== i) return;t = Gr.text;
	      }e.__v_model = this, t.bind.call(this), this.update = t.update, this._unbind = t.unbind;
	    }, checkFilters: function checkFilters() {
	      var t = this.filters;if (t) for (var e = t.length; e--;) {
	        var i = gt(this.vm.$options, "filters", t[e].name);("function" == typeof i || i.read) && (this.hasRead = !0), i.write && (this.hasWrite = !0);
	      }
	    }, unbind: function unbind() {
	      this.el.__v_model = null, this._unbind && this._unbind();
	    } },
	      Xr = { esc: 27, tab: 9, enter: 13, space: 32, "delete": [8, 46], up: 38, left: 37, right: 39, down: 40 },
	      Yr = { priority: Er, acceptStatement: !0, keyCodes: Xr, bind: function bind() {
	      if ("IFRAME" === this.el.tagName && "load" !== this.arg) {
	        var t = this;this.iframeBind = function () {
	          q(t.el.contentWindow, t.arg, t.handler, t.modifiers.capture);
	        }, this.on("load", this.iframeBind);
	      }
	    }, update: function update(t) {
	      if (this.descriptor.raw || (t = function t() {}), "function" == typeof t) {
	        this.modifiers.stop && (t = fe(t)), this.modifiers.prevent && (t = pe(t)), this.modifiers.self && (t = de(t));var e = Object.keys(this.modifiers).filter(function (t) {
	          return "stop" !== t && "prevent" !== t && "self" !== t && "capture" !== t;
	        });e.length && (t = ue(t, e)), this.reset(), this.handler = t, this.iframeBind ? this.iframeBind() : q(this.el, this.arg, this.handler, this.modifiers.capture);
	      }
	    }, reset: function reset() {
	      var t = this.iframeBind ? this.el.contentWindow : this.el;this.handler && Q(t, this.arg, this.handler);
	    }, unbind: function unbind() {
	      this.reset();
	    } },
	      Kr = ["-webkit-", "-moz-", "-ms-"],
	      ts = ["Webkit", "Moz", "ms"],
	      es = /!important;?$/,
	      is = Object.create(null),
	      ns = null,
	      rs = { deep: !0, update: function update(t) {
	      "string" == typeof t ? this.el.style.cssText = t : Fi(t) ? this.handleObject(t.reduce(v, {})) : this.handleObject(t || {});
	    }, handleObject: function handleObject(t) {
	      var e,
	          i,
	          n = this.cache || (this.cache = {});for (e in n) {
	        e in t || (this.handleSingle(e, null), delete n[e]);
	      }for (e in t) {
	        i = t[e], i !== n[e] && (n[e] = i, this.handleSingle(e, i));
	      }
	    }, handleSingle: function handleSingle(t, e) {
	      if (t = ve(t)) if (null != e && (e += ""), e) {
	        var i = es.test(e) ? "important" : "";i ? (e = e.replace(es, "").trim(), this.el.style.setProperty(t.kebab, e, i)) : this.el.style[t.camel] = e;
	      } else this.el.style[t.camel] = "";
	    } },
	      ss = "http://www.w3.org/1999/xlink",
	      os = /^xlink:/,
	      as = /^v-|^:|^@|^(?:is|transition|transition-mode|debounce|track-by|stagger|enter-stagger|leave-stagger)$/,
	      hs = /^(?:value|checked|selected|muted)$/,
	      ls = /^(?:draggable|contenteditable|spellcheck)$/,
	      cs = { value: "_value", "true-value": "_trueValue", "false-value": "_falseValue" },
	      us = { priority: Fr, bind: function bind() {
	      var t = this.arg,
	          e = this.el.tagName;t || (this.deep = !0);var i = this.descriptor,
	          n = i.interp;n && (i.hasOneTime && (this.expression = j(n, this._scope || this.vm)), (as.test(t) || "name" === t && ("PARTIAL" === e || "SLOT" === e)) && (this.el.removeAttribute(t), this.invalid = !0));
	    }, update: function update(t) {
	      if (!this.invalid) {
	        var e = this.arg;this.arg ? this.handleSingle(e, t) : this.handleObject(t || {});
	      }
	    }, handleObject: rs.handleObject, handleSingle: function handleSingle(t, e) {
	      var i = this.el,
	          n = this.descriptor.interp;if (this.modifiers.camel && (t = l(t)), !n && hs.test(t) && t in i) {
	        var r = "value" === t && null == e ? "" : e;i[t] !== r && (i[t] = r);
	      }var s = cs[t];if (!n && s) {
	        i[s] = e;var o = i.__v_model;o && o.listener();
	      }return "value" === t && "TEXTAREA" === i.tagName ? void i.removeAttribute(t) : void (ls.test(t) ? i.setAttribute(t, e ? "true" : "false") : null != e && e !== !1 ? "class" === t ? (i.__v_trans && (e += " " + i.__v_trans.id + "-transition"), Z(i, e)) : os.test(t) ? i.setAttributeNS(ss, t, e === !0 ? "" : e) : i.setAttribute(t, e === !0 ? "" : e) : i.removeAttribute(t));
	    } },
	      fs = { priority: Pr, bind: function bind() {
	      if (this.arg) {
	        var t = this.id = l(this.arg),
	            e = (this._scope || this.vm).$els;i(e, t) ? e[t] = this.el : kt(e, t, this.el);
	      }
	    }, unbind: function unbind() {
	      var t = (this._scope || this.vm).$els;t[this.id] === this.el && (t[this.id] = null);
	    } },
	      ps = { bind: function bind() {} },
	      ds = { bind: function bind() {
	      var t = this.el;this.vm.$once("pre-hook:compiled", function () {
	        t.removeAttribute("v-cloak");
	      });
	    } },
	      vs = { text: br, html: Nr, "for": Vr, "if": Br, show: zr, model: Zr, on: Yr, bind: us, el: fs, ref: ps, cloak: ds },
	      ms = { deep: !0, update: function update(t) {
	      t ? "string" == typeof t ? this.setClass(t.trim().split(/\s+/)) : this.setClass(ge(t)) : this.cleanup();
	    }, setClass: function setClass(t) {
	      this.cleanup(t);for (var e = 0, i = t.length; i > e; e++) {
	        var n = t[e];n && _e(this.el, n, X);
	      }this.prevKeys = t;
	    }, cleanup: function cleanup(t) {
	      var e = this.prevKeys;if (e) for (var i = e.length; i--;) {
	        var n = e[i];(!t || t.indexOf(n) < 0) && _e(this.el, n, Y);
	      }
	    } },
	      gs = { priority: Rr, params: ["keep-alive", "transition-mode", "inline-template"], bind: function bind() {
	      this.el.__vue__ || (this.keepAlive = this.params.keepAlive, this.keepAlive && (this.cache = {}), this.params.inlineTemplate && (this.inlineTemplate = K(this.el, !0)), this.pendingComponentCb = this.Component = null, this.pendingRemovals = 0, this.pendingRemovalCb = null, this.anchor = nt("v-component"), J(this.el, this.anchor), this.el.removeAttribute("is"), this.el.removeAttribute(":is"), this.descriptor.ref && this.el.removeAttribute("v-ref:" + u(this.descriptor.ref)), this.literal && this.setComponent(this.expression));
	    }, update: function update(t) {
	      this.literal || this.setComponent(t);
	    }, setComponent: function setComponent(t, e) {
	      if (this.invalidatePending(), t) {
	        var i = this;this.resolveComponent(t, function () {
	          i.mountComponent(e);
	        });
	      } else this.unbuild(!0), this.remove(this.childVM, e), this.childVM = null;
	    }, resolveComponent: function resolveComponent(t, e) {
	      var i = this;this.pendingComponentCb = w(function (n) {
	        i.ComponentName = n.options.name || ("string" == typeof t ? t : null), i.Component = n, e();
	      }), this.vm._resolveComponent(t, this.pendingComponentCb);
	    }, mountComponent: function mountComponent(t) {
	      this.unbuild(!0);var e = this,
	          i = this.Component.options.activate,
	          n = this.getCached(),
	          r = this.build();i && !n ? (this.waitingFor = r, ye(i, r, function () {
	        e.waitingFor === r && (e.waitingFor = null, e.transition(r, t));
	      })) : (n && r._updateRef(), this.transition(r, t));
	    }, invalidatePending: function invalidatePending() {
	      this.pendingComponentCb && (this.pendingComponentCb.cancel(), this.pendingComponentCb = null);
	    }, build: function build(t) {
	      var e = this.getCached();if (e) return e;if (this.Component) {
	        var i = { name: this.ComponentName, el: Gt(this.el), template: this.inlineTemplate, parent: this._host || this.vm, _linkerCachable: !this.inlineTemplate, _ref: this.descriptor.ref, _asComponent: !0, _isRouterView: this._isRouterView, _context: this.vm, _scope: this._scope, _frag: this._frag };t && v(i, t);var n = new this.Component(i);return this.keepAlive && (this.cache[this.Component.cid] = n), n;
	      }
	    }, getCached: function getCached() {
	      return this.keepAlive && this.cache[this.Component.cid];
	    }, unbuild: function unbuild(t) {
	      this.waitingFor && (this.keepAlive || this.waitingFor.$destroy(), this.waitingFor = null);var e = this.childVM;return !e || this.keepAlive ? void (e && (e._inactive = !0, e._updateRef(!0))) : void e.$destroy(!1, t);
	    }, remove: function remove(t, e) {
	      var i = this.keepAlive;if (t) {
	        this.pendingRemovals++, this.pendingRemovalCb = e;var n = this;t.$remove(function () {
	          n.pendingRemovals--, i || t._cleanup(), !n.pendingRemovals && n.pendingRemovalCb && (n.pendingRemovalCb(), n.pendingRemovalCb = null);
	        });
	      } else e && e();
	    }, transition: function transition(t, e) {
	      var i = this,
	          n = this.childVM;switch (n && (n._inactive = !0), t._inactive = !1, this.childVM = t, i.params.transitionMode) {case "in-out":
	          t.$before(i.anchor, function () {
	            i.remove(n, e);
	          });break;case "out-in":
	          i.remove(n, function () {
	            t.$before(i.anchor, e);
	          });break;default:
	          i.remove(n), t.$before(i.anchor, e);}
	    }, unbind: function unbind() {
	      if (this.invalidatePending(), this.unbuild(), this.cache) {
	        for (var t in this.cache) {
	          this.cache[t].$destroy();
	        }this.cache = null;
	      }
	    } },
	      _s = Cn._propBindingModes,
	      ys = {},
	      bs = /^[$_a-zA-Z]+[\w$]*$/,
	      ws = Cn._propBindingModes,
	      Cs = { bind: function bind() {
	      var t = this.vm,
	          e = t._context,
	          i = this.descriptor.prop,
	          n = i.path,
	          r = i.parentPath,
	          s = i.mode === ws.TWO_WAY,
	          o = this.parentWatcher = new zt(e, r, function (e) {
	        ke(t, i, e);
	      }, { twoWay: s, filters: i.filters, scope: this._scope });if ($e(t, i, o.value), s) {
	        var a = this;t.$once("pre-hook:created", function () {
	          a.childWatcher = new zt(t, n, function (t) {
	            o.set(t);
	          }, { sync: !0 });
	        });
	      }
	    }, unbind: function unbind() {
	      this.parentWatcher.teardown(), this.childWatcher && this.childWatcher.teardown();
	    } },
	      $s = [],
	      ks = !1,
	      xs = "transition",
	      As = "animation",
	      Os = Vi + "Duration",
	      Ts = zi + "Duration",
	      Ns = Pi && window.requestAnimationFrame,
	      js = Ns ? function (t) {
	    Ns(function () {
	      Ns(t);
	    });
	  } : function (t) {
	    setTimeout(t, 50);
	  },
	      Es = Ee.prototype;Es.enter = function (t, e) {
	    this.cancelPending(), this.callHook("beforeEnter"), this.cb = e, X(this.el, this.enterClass), t(), this.entered = !1, this.callHookWithCb("enter"), this.entered || (this.cancel = this.hooks && this.hooks.enterCancelled, Ne(this.enterNextTick));
	  }, Es.enterNextTick = function () {
	    var t = this;this.justEntered = !0, js(function () {
	      t.justEntered = !1;
	    });var e = this.enterDone,
	        i = this.getCssTransitionType(this.enterClass);this.pendingJsCb ? i === xs && Y(this.el, this.enterClass) : i === xs ? (Y(this.el, this.enterClass), this.setupCssCb(Bi, e)) : i === As ? this.setupCssCb(Ui, e) : e();
	  }, Es.enterDone = function () {
	    this.entered = !0, this.cancel = this.pendingJsCb = null, Y(this.el, this.enterClass), this.callHook("afterEnter"), this.cb && this.cb();
	  }, Es.leave = function (t, e) {
	    this.cancelPending(), this.callHook("beforeLeave"), this.op = t, this.cb = e, X(this.el, this.leaveClass), this.left = !1, this.callHookWithCb("leave"), this.left || (this.cancel = this.hooks && this.hooks.leaveCancelled, this.op && !this.pendingJsCb && (this.justEntered ? this.leaveDone() : Ne(this.leaveNextTick)));
	  }, Es.leaveNextTick = function () {
	    var t = this.getCssTransitionType(this.leaveClass);if (t) {
	      var e = t === xs ? Bi : Ui;this.setupCssCb(e, this.leaveDone);
	    } else this.leaveDone();
	  }, Es.leaveDone = function () {
	    this.left = !0, this.cancel = this.pendingJsCb = null, this.op(), Y(this.el, this.leaveClass), this.callHook("afterLeave"), this.cb && this.cb(), this.op = null;
	  }, Es.cancelPending = function () {
	    this.op = this.cb = null;var t = !1;this.pendingCssCb && (t = !0, Q(this.el, this.pendingCssEvent, this.pendingCssCb), this.pendingCssEvent = this.pendingCssCb = null), this.pendingJsCb && (t = !0, this.pendingJsCb.cancel(), this.pendingJsCb = null), t && (Y(this.el, this.enterClass), Y(this.el, this.leaveClass)), this.cancel && (this.cancel.call(this.vm, this.el), this.cancel = null);
	  }, Es.callHook = function (t) {
	    this.hooks && this.hooks[t] && this.hooks[t].call(this.vm, this.el);
	  }, Es.callHookWithCb = function (t) {
	    var e = this.hooks && this.hooks[t];e && (e.length > 1 && (this.pendingJsCb = w(this[t + "Done"])), e.call(this.vm, this.el, this.pendingJsCb));
	  }, Es.getCssTransitionType = function (t) {
	    if (!(!Bi || document.hidden || this.hooks && this.hooks.css === !1 || Se(this.el))) {
	      var e = this.type || this.typeCache[t];if (e) return e;var i = this.el.style,
	          n = window.getComputedStyle(this.el),
	          r = i[Os] || n[Os];if (r && "0s" !== r) e = xs;else {
	        var s = i[Ts] || n[Ts];s && "0s" !== s && (e = As);
	      }return e && (this.typeCache[t] = e), e;
	    }
	  }, Es.setupCssCb = function (t, e) {
	    this.pendingCssEvent = t;var i = this,
	        n = this.el,
	        r = this.pendingCssCb = function (s) {
	      s.target === n && (Q(n, t, r), i.pendingCssEvent = i.pendingCssCb = null, !i.pendingJsCb && e && e());
	    };q(n, t, r);
	  };var Ss = { priority: Dr, update: function update(t, e) {
	      var i = this.el,
	          n = gt(this.vm.$options, "transitions", t);t = t || "v", i.__v_trans = new Ee(i, t, n, this.vm), e && Y(i, e + "-transition"), X(i, t + "-transition");
	    } },
	      Fs = { style: rs, "class": ms, component: gs, prop: Cs, transition: Ss },
	      Ds = /^v-bind:|^:/,
	      Ps = /^v-on:|^@/,
	      Rs = /^v-([^:]+)(?:$|:(.*)$)/,
	      Ls = /\.[^\.]+/g,
	      Hs = /^(v-bind:|:)?transition$/,
	      Is = 1e3,
	      Ms = 2e3;Xe.terminal = !0;var Ws = /[^\w\-:\.]/,
	      Vs = Object.freeze({ compile: Fe, compileAndLinkProps: He, compileRoot: Ie, transclude: ri, resolveSlots: hi }),
	      Bs = /^v-on:|^@/;pi.prototype._bind = function () {
	    var t = this.name,
	        e = this.descriptor;if (("cloak" !== t || this.vm._isCompiled) && this.el && this.el.removeAttribute) {
	      var i = e.attr || "v-" + t;this.el.removeAttribute(i);
	    }var n = e.def;if ("function" == typeof n ? this.update = n : v(this, n), this._setupParams(), this.bind && this.bind(), this._bound = !0, this.literal) this.update && this.update(e.raw);else if ((this.expression || this.modifiers) && (this.update || this.twoWay) && !this._checkStatement()) {
	      var r = this;this.update ? this._update = function (t, e) {
	        r._locked || r.update(t, e);
	      } : this._update = fi;var s = this._preProcess ? p(this._preProcess, this) : null,
	          o = this._postProcess ? p(this._postProcess, this) : null,
	          a = this._watcher = new zt(this.vm, this.expression, this._update, { filters: this.filters, twoWay: this.twoWay, deep: this.deep, preProcess: s, postProcess: o, scope: this._scope });this.afterBind ? this.afterBind() : this.update && this.update(a.value);
	    }
	  }, pi.prototype._setupParams = function () {
	    if (this.params) {
	      var t = this.params;this.params = Object.create(null);for (var e, i, n, r = t.length; r--;) {
	        e = u(t[r]), n = l(e), i = M(this.el, e), null != i ? this._setupParamWatcher(n, i) : (i = I(this.el, e), null != i && (this.params[n] = "" === i ? !0 : i));
	      }
	    }
	  }, pi.prototype._setupParamWatcher = function (t, e) {
	    var i = this,
	        n = !1,
	        r = (this._scope || this.vm).$watch(e, function (e, r) {
	      if (i.params[t] = e, n) {
	        var s = i.paramWatchers && i.paramWatchers[t];s && s.call(i, e, r);
	      } else n = !0;
	    }, { immediate: !0, user: !1 });(this._paramUnwatchFns || (this._paramUnwatchFns = [])).push(r);
	  }, pi.prototype._checkStatement = function () {
	    var t = this.expression;if (t && this.acceptStatement && !It(t)) {
	      var e = Ht(t).get,
	          i = this._scope || this.vm,
	          n = function n(t) {
	        i.$event = t, e.call(i, i), i.$event = null;
	      };return this.filters && (n = i._applyFilters(n, null, this.filters)), this.update(n), !0;
	    }
	  }, pi.prototype.set = function (t) {
	    this.twoWay && this._withLock(function () {
	      this._watcher.set(t);
	    });
	  }, pi.prototype._withLock = function (t) {
	    var e = this;e._locked = !0, t.call(e), Qi(function () {
	      e._locked = !1;
	    });
	  }, pi.prototype.on = function (t, e, i) {
	    q(this.el, t, e, i), (this._listeners || (this._listeners = [])).push([t, e]);
	  }, pi.prototype._teardown = function () {
	    if (this._bound) {
	      this._bound = !1, this.unbind && this.unbind(), this._watcher && this._watcher.teardown();var t,
	          e = this._listeners;if (e) for (t = e.length; t--;) {
	        Q(this.el, e[t][0], e[t][1]);
	      }var i = this._paramUnwatchFns;if (i) for (t = i.length; t--;) {
	        i[t]();
	      }this.vm = this.el = this._watcher = this._listeners = null;
	    }
	  };var zs = /[^|]\|[^|]/;xt(bi), ci(bi), ui(bi), di(bi), vi(bi), mi(bi), gi(bi), _i(bi), yi(bi);var Us = { priority: Mr, params: ["name"], bind: function bind() {
	      var t = this.params.name || "default",
	          e = this.vm._slotContents && this.vm._slotContents[t];e && e.hasChildNodes() ? this.compile(e.cloneNode(!0), this.vm._context, this.vm) : this.fallback();
	    }, compile: function compile(t, e, i) {
	      if (t && e) {
	        if (this.el.hasChildNodes() && 1 === t.childNodes.length && 1 === t.childNodes[0].nodeType && t.childNodes[0].hasAttribute("v-if")) {
	          var n = document.createElement("template");n.setAttribute("v-else", ""), n.innerHTML = this.el.innerHTML, n._context = this.vm, t.appendChild(n);
	        }var r = i ? i._scope : this._scope;this.unlink = e.$compile(t, i, r, this._frag);
	      }t ? J(this.el, t) : z(this.el);
	    }, fallback: function fallback() {
	      this.compile(K(this.el, !0), this.vm);
	    }, unbind: function unbind() {
	      this.unlink && this.unlink();
	    } },
	      Js = { priority: Lr, params: ["name"], paramWatchers: { name: function name(t) {
	        Br.remove.call(this), t && this.insert(t);
	      } }, bind: function bind() {
	      this.anchor = nt("v-partial"), J(this.el, this.anchor), this.insert(this.params.name);
	    }, insert: function insert(t) {
	      var e = gt(this.vm.$options, "partials", t, !0);e && (this.factory = new re(this.vm, e), Br.insert.call(this));
	    }, unbind: function unbind() {
	      this.frag && this.frag.destroy();
	    } },
	      qs = { slot: Us, partial: Js },
	      Qs = Vr._postProcess,
	      Gs = /(\d{3})(?=\d)/g,
	      Zs = { orderBy: $i, filterBy: Ci, limitBy: wi, json: { read: function read(t, e) {
	        return "string" == typeof t ? t : JSON.stringify(t, null, Number(e) || 2);
	      }, write: function write(t) {
	        try {
	          return JSON.parse(t);
	        } catch (e) {
	          return t;
	        }
	      } }, capitalize: function capitalize(t) {
	      return t || 0 === t ? (t = t.toString(), t.charAt(0).toUpperCase() + t.slice(1)) : "";
	    }, uppercase: function uppercase(t) {
	      return t || 0 === t ? t.toString().toUpperCase() : "";
	    }, lowercase: function lowercase(t) {
	      return t || 0 === t ? t.toString().toLowerCase() : "";
	    }, currency: function currency(t, e, i) {
	      if (t = parseFloat(t), !isFinite(t) || !t && 0 !== t) return "";e = null != e ? e : "$", i = null != i ? i : 2;var n = Math.abs(t).toFixed(i),
	          r = i ? n.slice(0, -1 - i) : n,
	          s = r.length % 3,
	          o = s > 0 ? r.slice(0, s) + (r.length > 3 ? "," : "") : "",
	          a = i ? n.slice(-1 - i) : "",
	          h = 0 > t ? "-" : "";return h + e + o + r.slice(s).replace(Gs, "$1,") + a;
	    }, pluralize: function pluralize(t) {
	      var e = d(arguments, 1);return e.length > 1 ? e[t % 10 - 1] || e[e.length - 1] : e[0] + (1 === t ? "" : "s");
	    }, debounce: function debounce(t, e) {
	      return t ? (e || (e = 300), y(t, e)) : void 0;
	    } };return xi(bi), bi.version = "1.0.24", setTimeout(function () {
	    Cn.devtools && Ri && Ri.emit("init", bi);
	  }, 0), bi;
	});
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(3)))

/***/ },
/* 3 */
/***/ function(module, exports) {

	"use strict";
	"use strict";

	var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };

	var g;

	// This works in non-strict mode
	g = function () {
		return this;
	}();

	try {
		// This works if eval is allowed (see CSP)
		g = g || Function("return this")() || (1, eval)("this");
	} catch (e) {
		// This works if the window reference is available
		if ((typeof window === "undefined" ? "undefined" : _typeof(window)) === "object") g = window;
	}

	// g can still be undefined, but nothing to do about it...
	// We return undefined, instead of nothing here, so it's
	// easier to handle this case. if(!global) { ...}

	module.exports = g;

/***/ },
/* 4 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	/**
	 * Promise adapter.
	 */

	var _ = __webpack_require__(1);
	var PromiseObj = window.Promise || __webpack_require__(56);

	function Promise(executor, context) {

	    if (executor instanceof PromiseObj) {
	        this.promise = executor;
	    } else {
	        this.promise = new PromiseObj(executor.bind(context));
	    }

	    this.context = context;
	}

	Promise.all = function (iterable, context) {
	    return new Promise(PromiseObj.all(iterable), context);
	};

	Promise.resolve = function (value, context) {
	    return new Promise(PromiseObj.resolve(value), context);
	};

	Promise.reject = function (reason, context) {
	    return new Promise(PromiseObj.reject(reason), context);
	};

	Promise.race = function (iterable, context) {
	    return new Promise(PromiseObj.race(iterable), context);
	};

	var p = Promise.prototype;

	p.bind = function (context) {
	    this.context = context;
	    return this;
	};

	p.then = function (fulfilled, rejected) {

	    if (fulfilled && fulfilled.bind && this.context) {
	        fulfilled = fulfilled.bind(this.context);
	    }

	    if (rejected && rejected.bind && this.context) {
	        rejected = rejected.bind(this.context);
	    }

	    this.promise = this.promise.then(fulfilled, rejected);

	    return this;
	};

	p.catch = function (rejected) {

	    if (rejected && rejected.bind && this.context) {
	        rejected = rejected.bind(this.context);
	    }

	    this.promise = this.promise.catch(rejected);

	    return this;
	};

	p.finally = function (callback) {

	    return this.then(function (value) {
	        callback.call(this);
	        return value;
	    }, function (reason) {
	        callback.call(this);
	        return PromiseObj.reject(reason);
	    });
	};

	p.success = function (callback) {

	    _.warn('The `success` method has been deprecated. Use the `then` method instead.');

	    return this.then(function (response) {
	        return callback.call(this, response.data, response.status, response) || response;
	    });
	};

	p.error = function (callback) {

	    _.warn('The `error` method has been deprecated. Use the `catch` method instead.');

	    return this.catch(function (response) {
	        return callback.call(this, response.data, response.status, response) || response;
	    });
	};

	p.always = function (callback) {

	    _.warn('The `always` method has been deprecated. Use the `finally` method instead.');

	    var cb = function cb(response) {
	        return callback.call(this, response.data, response.status, response) || response;
	    };

	    return this.then(cb, cb);
	};

	module.exports = Promise;

/***/ },
/* 5 */
/***/ function(module, exports) {

	"use strict";
	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = merge;
	function merge() {
	  var obj = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];
	  var defaults = arguments[1];

	  for (var key in defaults) {
	    if (typeof obj[key] === 'undefined') {
	      obj[key] = defaults[key];
	    }
	  }
	  return obj;
	}
	module.exports = exports['default'];

/***/ },
/* 6 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = isFDQN;

	var _assertString = __webpack_require__(0);

	var _assertString2 = _interopRequireDefault(_assertString);

	var _merge = __webpack_require__(5);

	var _merge2 = _interopRequireDefault(_merge);

	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { default: obj };
	}

	var default_fqdn_options = {
	  require_tld: true,
	  allow_underscores: false,
	  allow_trailing_dot: false
	};

	function isFDQN(str, options) {
	  (0, _assertString2.default)(str);
	  options = (0, _merge2.default)(options, default_fqdn_options);

	  /* Remove the optional trailing dot before checking validity */
	  if (options.allow_trailing_dot && str[str.length - 1] === '.') {
	    str = str.substring(0, str.length - 1);
	  }
	  var parts = str.split('.');
	  if (options.require_tld) {
	    var tld = parts.pop();
	    if (!parts.length || !/^([a-z\u00a1-\uffff]{2,}|xn[a-z0-9-]{2,})$/i.test(tld)) {
	      return false;
	    }
	  }
	  for (var part, i = 0; i < parts.length; i++) {
	    part = parts[i];
	    if (options.allow_underscores) {
	      part = part.replace(/_/g, '');
	    }
	    if (!/^[a-z\u00a1-\uffff0-9-]+$/i.test(part)) {
	      return false;
	    }
	    if (/[\uff01-\uff5e]/.test(part)) {
	      // disallow full-width chars
	      return false;
	    }
	    if (part[0] === '-' || part[part.length - 1] === '-') {
	      return false;
	    }
	  }
	  return true;
	}
	module.exports = exports['default'];

/***/ },
/* 7 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = toDate;

	var _assertString = __webpack_require__(0);

	var _assertString2 = _interopRequireDefault(_assertString);

	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { default: obj };
	}

	function toDate(date) {
	  (0, _assertString2.default)(date);
	  date = Date.parse(date);
	  return !isNaN(date) ? new Date(date) : null;
	}
	module.exports = exports['default'];

/***/ },
/* 8 */
/***/ function(module, exports) {

	"use strict";
	'use strict';

	var _typeof2 = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _typeof = typeof Symbol === "function" && _typeof2(Symbol.iterator) === "symbol" ? function (obj) {
	  return typeof obj === "undefined" ? "undefined" : _typeof2(obj);
	} : function (obj) {
	  return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj === "undefined" ? "undefined" : _typeof2(obj);
	};

	exports.default = toString;
	function toString(input) {
	  if ((typeof input === 'undefined' ? 'undefined' : _typeof(input)) === 'object' && input !== null) {
	    if (typeof input.toString === 'function') {
	      input = input.toString();
	    } else {
	      input = '[object Object]';
	    }
	  } else if (input === null || typeof input === 'undefined' || isNaN(input) && !input.length) {
	    input = '';
	  }
	  return String(input);
	}
	module.exports = exports['default'];

/***/ },
/* 9 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	/* WEBPACK VAR INJECTION */(function(global, process) {'use strict';

	var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };

	// Copyright Joyent, Inc. and other Node contributors.
	//
	// Permission is hereby granted, free of charge, to any person obtaining a
	// copy of this software and associated documentation files (the
	// "Software"), to deal in the Software without restriction, including
	// without limitation the rights to use, copy, modify, merge, publish,
	// distribute, sublicense, and/or sell copies of the Software, and to permit
	// persons to whom the Software is furnished to do so, subject to the
	// following conditions:
	//
	// The above copyright notice and this permission notice shall be included
	// in all copies or substantial portions of the Software.
	//
	// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
	// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
	// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
	// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
	// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
	// USE OR OTHER DEALINGS IN THE SOFTWARE.

	var formatRegExp = /%[sdj%]/g;
	exports.format = function (f) {
	  if (!isString(f)) {
	    var objects = [];
	    for (var i = 0; i < arguments.length; i++) {
	      objects.push(inspect(arguments[i]));
	    }
	    return objects.join(' ');
	  }

	  var i = 1;
	  var args = arguments;
	  var len = args.length;
	  var str = String(f).replace(formatRegExp, function (x) {
	    if (x === '%%') return '%';
	    if (i >= len) return x;
	    switch (x) {
	      case '%s':
	        return String(args[i++]);
	      case '%d':
	        return Number(args[i++]);
	      case '%j':
	        try {
	          return JSON.stringify(args[i++]);
	        } catch (_) {
	          return '[Circular]';
	        }
	      default:
	        return x;
	    }
	  });
	  for (var x = args[i]; i < len; x = args[++i]) {
	    if (isNull(x) || !isObject(x)) {
	      str += ' ' + x;
	    } else {
	      str += ' ' + inspect(x);
	    }
	  }
	  return str;
	};

	// Mark that a method should not be used.
	// Returns a modified function which warns once by default.
	// If --no-deprecation is set, then it is a no-op.
	exports.deprecate = function (fn, msg) {
	  // Allow for deprecating things in the process of starting up.
	  if (isUndefined(global.process)) {
	    return function () {
	      return exports.deprecate(fn, msg).apply(this, arguments);
	    };
	  }

	  if (process.noDeprecation === true) {
	    return fn;
	  }

	  var warned = false;
	  function deprecated() {
	    if (!warned) {
	      if (process.throwDeprecation) {
	        throw new Error(msg);
	      } else if (process.traceDeprecation) {
	        console.trace(msg);
	      } else {
	        console.error(msg);
	      }
	      warned = true;
	    }
	    return fn.apply(this, arguments);
	  }

	  return deprecated;
	};

	var debugs = {};
	var debugEnviron;
	exports.debuglog = function (set) {
	  if (isUndefined(debugEnviron)) debugEnviron = process.env.NODE_DEBUG || '';
	  set = set.toUpperCase();
	  if (!debugs[set]) {
	    if (new RegExp('\\b' + set + '\\b', 'i').test(debugEnviron)) {
	      var pid = process.pid;
	      debugs[set] = function () {
	        var msg = exports.format.apply(exports, arguments);
	        console.error('%s %d: %s', set, pid, msg);
	      };
	    } else {
	      debugs[set] = function () {};
	    }
	  }
	  return debugs[set];
	};

	/**
	 * Echos the value of a value. Trys to print the value out
	 * in the best way possible given the different types.
	 *
	 * @param {Object} obj The object to print out.
	 * @param {Object} opts Optional options object that alters the output.
	 */
	/* legacy: obj, showHidden, depth, colors*/
	function inspect(obj, opts) {
	  // default options
	  var ctx = {
	    seen: [],
	    stylize: stylizeNoColor
	  };
	  // legacy...
	  if (arguments.length >= 3) ctx.depth = arguments[2];
	  if (arguments.length >= 4) ctx.colors = arguments[3];
	  if (isBoolean(opts)) {
	    // legacy...
	    ctx.showHidden = opts;
	  } else if (opts) {
	    // got an "options" object
	    exports._extend(ctx, opts);
	  }
	  // set default options
	  if (isUndefined(ctx.showHidden)) ctx.showHidden = false;
	  if (isUndefined(ctx.depth)) ctx.depth = 2;
	  if (isUndefined(ctx.colors)) ctx.colors = false;
	  if (isUndefined(ctx.customInspect)) ctx.customInspect = true;
	  if (ctx.colors) ctx.stylize = stylizeWithColor;
	  return formatValue(ctx, obj, ctx.depth);
	}
	exports.inspect = inspect;

	// http://en.wikipedia.org/wiki/ANSI_escape_code#graphics
	inspect.colors = {
	  'bold': [1, 22],
	  'italic': [3, 23],
	  'underline': [4, 24],
	  'inverse': [7, 27],
	  'white': [37, 39],
	  'grey': [90, 39],
	  'black': [30, 39],
	  'blue': [34, 39],
	  'cyan': [36, 39],
	  'green': [32, 39],
	  'magenta': [35, 39],
	  'red': [31, 39],
	  'yellow': [33, 39]
	};

	// Don't use 'blue' not visible on cmd.exe
	inspect.styles = {
	  'special': 'cyan',
	  'number': 'yellow',
	  'boolean': 'yellow',
	  'undefined': 'grey',
	  'null': 'bold',
	  'string': 'green',
	  'date': 'magenta',
	  // "name": intentionally not styling
	  'regexp': 'red'
	};

	function stylizeWithColor(str, styleType) {
	  var style = inspect.styles[styleType];

	  if (style) {
	    return '\u001b[' + inspect.colors[style][0] + 'm' + str + '\u001b[' + inspect.colors[style][1] + 'm';
	  } else {
	    return str;
	  }
	}

	function stylizeNoColor(str, styleType) {
	  return str;
	}

	function arrayToHash(array) {
	  var hash = {};

	  array.forEach(function (val, idx) {
	    hash[val] = true;
	  });

	  return hash;
	}

	function formatValue(ctx, value, recurseTimes) {
	  // Provide a hook for user-specified inspect functions.
	  // Check that value is an object with an inspect function on it
	  if (ctx.customInspect && value && isFunction(value.inspect) &&
	  // Filter out the util module, it's inspect function is special
	  value.inspect !== exports.inspect &&
	  // Also filter out any prototype objects using the circular check.
	  !(value.constructor && value.constructor.prototype === value)) {
	    var ret = value.inspect(recurseTimes, ctx);
	    if (!isString(ret)) {
	      ret = formatValue(ctx, ret, recurseTimes);
	    }
	    return ret;
	  }

	  // Primitive types cannot have properties
	  var primitive = formatPrimitive(ctx, value);
	  if (primitive) {
	    return primitive;
	  }

	  // Look up the keys of the object.
	  var keys = Object.keys(value);
	  var visibleKeys = arrayToHash(keys);

	  if (ctx.showHidden) {
	    keys = Object.getOwnPropertyNames(value);
	  }

	  // IE doesn't make error fields non-enumerable
	  // http://msdn.microsoft.com/en-us/library/ie/dww52sbt(v=vs.94).aspx
	  if (isError(value) && (keys.indexOf('message') >= 0 || keys.indexOf('description') >= 0)) {
	    return formatError(value);
	  }

	  // Some type of object without properties can be shortcutted.
	  if (keys.length === 0) {
	    if (isFunction(value)) {
	      var name = value.name ? ': ' + value.name : '';
	      return ctx.stylize('[Function' + name + ']', 'special');
	    }
	    if (isRegExp(value)) {
	      return ctx.stylize(RegExp.prototype.toString.call(value), 'regexp');
	    }
	    if (isDate(value)) {
	      return ctx.stylize(Date.prototype.toString.call(value), 'date');
	    }
	    if (isError(value)) {
	      return formatError(value);
	    }
	  }

	  var base = '',
	      array = false,
	      braces = ['{', '}'];

	  // Make Array say that they are Array
	  if (isArray(value)) {
	    array = true;
	    braces = ['[', ']'];
	  }

	  // Make functions say that they are functions
	  if (isFunction(value)) {
	    var n = value.name ? ': ' + value.name : '';
	    base = ' [Function' + n + ']';
	  }

	  // Make RegExps say that they are RegExps
	  if (isRegExp(value)) {
	    base = ' ' + RegExp.prototype.toString.call(value);
	  }

	  // Make dates with properties first say the date
	  if (isDate(value)) {
	    base = ' ' + Date.prototype.toUTCString.call(value);
	  }

	  // Make error with message first say the error
	  if (isError(value)) {
	    base = ' ' + formatError(value);
	  }

	  if (keys.length === 0 && (!array || value.length == 0)) {
	    return braces[0] + base + braces[1];
	  }

	  if (recurseTimes < 0) {
	    if (isRegExp(value)) {
	      return ctx.stylize(RegExp.prototype.toString.call(value), 'regexp');
	    } else {
	      return ctx.stylize('[Object]', 'special');
	    }
	  }

	  ctx.seen.push(value);

	  var output;
	  if (array) {
	    output = formatArray(ctx, value, recurseTimes, visibleKeys, keys);
	  } else {
	    output = keys.map(function (key) {
	      return formatProperty(ctx, value, recurseTimes, visibleKeys, key, array);
	    });
	  }

	  ctx.seen.pop();

	  return reduceToSingleString(output, base, braces);
	}

	function formatPrimitive(ctx, value) {
	  if (isUndefined(value)) return ctx.stylize('undefined', 'undefined');
	  if (isString(value)) {
	    var simple = '\'' + JSON.stringify(value).replace(/^"|"$/g, '').replace(/'/g, "\\'").replace(/\\"/g, '"') + '\'';
	    return ctx.stylize(simple, 'string');
	  }
	  if (isNumber(value)) return ctx.stylize('' + value, 'number');
	  if (isBoolean(value)) return ctx.stylize('' + value, 'boolean');
	  // For some reason typeof null is "object", so special case here.
	  if (isNull(value)) return ctx.stylize('null', 'null');
	}

	function formatError(value) {
	  return '[' + Error.prototype.toString.call(value) + ']';
	}

	function formatArray(ctx, value, recurseTimes, visibleKeys, keys) {
	  var output = [];
	  for (var i = 0, l = value.length; i < l; ++i) {
	    if (hasOwnProperty(value, String(i))) {
	      output.push(formatProperty(ctx, value, recurseTimes, visibleKeys, String(i), true));
	    } else {
	      output.push('');
	    }
	  }
	  keys.forEach(function (key) {
	    if (!key.match(/^\d+$/)) {
	      output.push(formatProperty(ctx, value, recurseTimes, visibleKeys, key, true));
	    }
	  });
	  return output;
	}

	function formatProperty(ctx, value, recurseTimes, visibleKeys, key, array) {
	  var name, str, desc;
	  desc = Object.getOwnPropertyDescriptor(value, key) || { value: value[key] };
	  if (desc.get) {
	    if (desc.set) {
	      str = ctx.stylize('[Getter/Setter]', 'special');
	    } else {
	      str = ctx.stylize('[Getter]', 'special');
	    }
	  } else {
	    if (desc.set) {
	      str = ctx.stylize('[Setter]', 'special');
	    }
	  }
	  if (!hasOwnProperty(visibleKeys, key)) {
	    name = '[' + key + ']';
	  }
	  if (!str) {
	    if (ctx.seen.indexOf(desc.value) < 0) {
	      if (isNull(recurseTimes)) {
	        str = formatValue(ctx, desc.value, null);
	      } else {
	        str = formatValue(ctx, desc.value, recurseTimes - 1);
	      }
	      if (str.indexOf('\n') > -1) {
	        if (array) {
	          str = str.split('\n').map(function (line) {
	            return '  ' + line;
	          }).join('\n').substr(2);
	        } else {
	          str = '\n' + str.split('\n').map(function (line) {
	            return '   ' + line;
	          }).join('\n');
	        }
	      }
	    } else {
	      str = ctx.stylize('[Circular]', 'special');
	    }
	  }
	  if (isUndefined(name)) {
	    if (array && key.match(/^\d+$/)) {
	      return str;
	    }
	    name = JSON.stringify('' + key);
	    if (name.match(/^"([a-zA-Z_][a-zA-Z_0-9]*)"$/)) {
	      name = name.substr(1, name.length - 2);
	      name = ctx.stylize(name, 'name');
	    } else {
	      name = name.replace(/'/g, "\\'").replace(/\\"/g, '"').replace(/(^"|"$)/g, "'");
	      name = ctx.stylize(name, 'string');
	    }
	  }

	  return name + ': ' + str;
	}

	function reduceToSingleString(output, base, braces) {
	  var numLinesEst = 0;
	  var length = output.reduce(function (prev, cur) {
	    numLinesEst++;
	    if (cur.indexOf('\n') >= 0) numLinesEst++;
	    return prev + cur.replace(/\u001b\[\d\d?m/g, '').length + 1;
	  }, 0);

	  if (length > 60) {
	    return braces[0] + (base === '' ? '' : base + '\n ') + ' ' + output.join(',\n  ') + ' ' + braces[1];
	  }

	  return braces[0] + base + ' ' + output.join(', ') + ' ' + braces[1];
	}

	// NOTE: These type checking functions intentionally don't use `instanceof`
	// because it is fragile and can be easily faked with `Object.create()`.
	function isArray(ar) {
	  return Array.isArray(ar);
	}
	exports.isArray = isArray;

	function isBoolean(arg) {
	  return typeof arg === 'boolean';
	}
	exports.isBoolean = isBoolean;

	function isNull(arg) {
	  return arg === null;
	}
	exports.isNull = isNull;

	function isNullOrUndefined(arg) {
	  return arg == null;
	}
	exports.isNullOrUndefined = isNullOrUndefined;

	function isNumber(arg) {
	  return typeof arg === 'number';
	}
	exports.isNumber = isNumber;

	function isString(arg) {
	  return typeof arg === 'string';
	}
	exports.isString = isString;

	function isSymbol(arg) {
	  return (typeof arg === 'undefined' ? 'undefined' : _typeof(arg)) === 'symbol';
	}
	exports.isSymbol = isSymbol;

	function isUndefined(arg) {
	  return arg === void 0;
	}
	exports.isUndefined = isUndefined;

	function isRegExp(re) {
	  return isObject(re) && objectToString(re) === '[object RegExp]';
	}
	exports.isRegExp = isRegExp;

	function isObject(arg) {
	  return (typeof arg === 'undefined' ? 'undefined' : _typeof(arg)) === 'object' && arg !== null;
	}
	exports.isObject = isObject;

	function isDate(d) {
	  return isObject(d) && objectToString(d) === '[object Date]';
	}
	exports.isDate = isDate;

	function isError(e) {
	  return isObject(e) && (objectToString(e) === '[object Error]' || e instanceof Error);
	}
	exports.isError = isError;

	function isFunction(arg) {
	  return typeof arg === 'function';
	}
	exports.isFunction = isFunction;

	function isPrimitive(arg) {
	  return arg === null || typeof arg === 'boolean' || typeof arg === 'number' || typeof arg === 'string' || (typeof arg === 'undefined' ? 'undefined' : _typeof(arg)) === 'symbol' || // ES6 symbol
	  typeof arg === 'undefined';
	}
	exports.isPrimitive = isPrimitive;

	exports.isBuffer = __webpack_require__(74);

	function objectToString(o) {
	  return Object.prototype.toString.call(o);
	}

	function pad(n) {
	  return n < 10 ? '0' + n.toString(10) : n.toString(10);
	}

	var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

	// 26 Feb 16:19:34
	function timestamp() {
	  var d = new Date();
	  var time = [pad(d.getHours()), pad(d.getMinutes()), pad(d.getSeconds())].join(':');
	  return [d.getDate(), months[d.getMonth()], time].join(' ');
	}

	// log is just a thin wrapper to console.log that prepends a timestamp
	exports.log = function () {
	  console.log('%s - %s', timestamp(), exports.format.apply(exports, arguments));
	};

	/**
	 * Inherit the prototype methods from one constructor into another.
	 *
	 * The Function.prototype.inherits from lang.js rewritten as a standalone
	 * function (not on Function.prototype). NOTE: If this file is to be loaded
	 * during bootstrapping this function needs to be rewritten using some native
	 * functions as prototype setup using normal JavaScript does not work as
	 * expected during bootstrapping (see mirror.js in r114903).
	 *
	 * @param {function} ctor Constructor function which needs to inherit the
	 *     prototype.
	 * @param {function} superCtor Constructor function to inherit prototype from.
	 */
	exports.inherits = __webpack_require__(67);

	exports._extend = function (origin, add) {
	  // Don't do anything if add isn't an object
	  if (!add || !isObject(add)) return origin;

	  var keys = Object.keys(add);
	  var i = keys.length;
	  while (i--) {
	    origin[keys[i]] = add[keys[i]];
	  }
	  return origin;
	};

	function hasOwnProperty(obj, prop) {
	  return Object.prototype.hasOwnProperty.call(obj, prop);
	}
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(3), __webpack_require__(68)))

/***/ },
/* 10 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	/* WEBPACK VAR INJECTION */(function(Buffer, global) {/*!
	 * The buffer module from node.js, for the browser.
	 *
	 * @author   Feross Aboukhadijeh <feross@feross.org> <http://feross.org>
	 * @license  MIT
	 */
	/* eslint-disable no-proto */

	'use strict';

	var base64 = __webpack_require__(63);
	var ieee754 = __webpack_require__(66);
	var isArray = __webpack_require__(64);

	exports.Buffer = Buffer;
	exports.SlowBuffer = SlowBuffer;
	exports.INSPECT_MAX_BYTES = 50;

	/**
	 * If `Buffer.TYPED_ARRAY_SUPPORT`:
	 *   === true    Use Uint8Array implementation (fastest)
	 *   === false   Use Object implementation (most compatible, even IE6)
	 *
	 * Browsers that support typed arrays are IE 10+, Firefox 4+, Chrome 7+, Safari 5.1+,
	 * Opera 11.6+, iOS 4.2+.
	 *
	 * Due to various browser bugs, sometimes the Object implementation will be used even
	 * when the browser supports typed arrays.
	 *
	 * Note:
	 *
	 *   - Firefox 4-29 lacks support for adding new properties to `Uint8Array` instances,
	 *     See: https://bugzilla.mozilla.org/show_bug.cgi?id=695438.
	 *
	 *   - Chrome 9-10 is missing the `TypedArray.prototype.subarray` function.
	 *
	 *   - IE10 has a broken `TypedArray.prototype.subarray` function which returns arrays of
	 *     incorrect length in some situations.

	 * We detect these buggy browsers and set `Buffer.TYPED_ARRAY_SUPPORT` to `false` so they
	 * get the Object implementation, which is slower but behaves correctly.
	 */
	Buffer.TYPED_ARRAY_SUPPORT = global.TYPED_ARRAY_SUPPORT !== undefined ? global.TYPED_ARRAY_SUPPORT : typedArraySupport();

	/*
	 * Export kMaxLength after typed array support is determined.
	 */
	exports.kMaxLength = kMaxLength();

	function typedArraySupport() {
	  try {
	    var arr = new Uint8Array(1);
	    arr.foo = function () {
	      return 42;
	    };
	    return arr.foo() === 42 && // typed array instances can be augmented
	    typeof arr.subarray === 'function' && // chrome 9-10 lack `subarray`
	    arr.subarray(1, 1).byteLength === 0; // ie10 has broken `subarray`
	  } catch (e) {
	    return false;
	  }
	}

	function kMaxLength() {
	  return Buffer.TYPED_ARRAY_SUPPORT ? 0x7fffffff : 0x3fffffff;
	}

	function createBuffer(that, length) {
	  if (kMaxLength() < length) {
	    throw new RangeError('Invalid typed array length');
	  }
	  if (Buffer.TYPED_ARRAY_SUPPORT) {
	    // Return an augmented `Uint8Array` instance, for best performance
	    that = new Uint8Array(length);
	    that.__proto__ = Buffer.prototype;
	  } else {
	    // Fallback: Return an object instance of the Buffer class
	    if (that === null) {
	      that = new Buffer(length);
	    }
	    that.length = length;
	  }

	  return that;
	}

	/**
	 * The Buffer constructor returns instances of `Uint8Array` that have their
	 * prototype changed to `Buffer.prototype`. Furthermore, `Buffer` is a subclass of
	 * `Uint8Array`, so the returned instances will have all the node `Buffer` methods
	 * and the `Uint8Array` methods. Square bracket notation works as expected -- it
	 * returns a single octet.
	 *
	 * The `Uint8Array` prototype remains unmodified.
	 */

	function Buffer(arg, encodingOrOffset, length) {
	  if (!Buffer.TYPED_ARRAY_SUPPORT && !(this instanceof Buffer)) {
	    return new Buffer(arg, encodingOrOffset, length);
	  }

	  // Common case.
	  if (typeof arg === 'number') {
	    if (typeof encodingOrOffset === 'string') {
	      throw new Error('If encoding is specified then the first argument must be a string');
	    }
	    return allocUnsafe(this, arg);
	  }
	  return from(this, arg, encodingOrOffset, length);
	}

	Buffer.poolSize = 8192; // not used by this implementation

	// TODO: Legacy, not needed anymore. Remove in next major version.
	Buffer._augment = function (arr) {
	  arr.__proto__ = Buffer.prototype;
	  return arr;
	};

	function from(that, value, encodingOrOffset, length) {
	  if (typeof value === 'number') {
	    throw new TypeError('"value" argument must not be a number');
	  }

	  if (typeof ArrayBuffer !== 'undefined' && value instanceof ArrayBuffer) {
	    return fromArrayBuffer(that, value, encodingOrOffset, length);
	  }

	  if (typeof value === 'string') {
	    return fromString(that, value, encodingOrOffset);
	  }

	  return fromObject(that, value);
	}

	/**
	 * Functionally equivalent to Buffer(arg, encoding) but throws a TypeError
	 * if value is a number.
	 * Buffer.from(str[, encoding])
	 * Buffer.from(array)
	 * Buffer.from(buffer)
	 * Buffer.from(arrayBuffer[, byteOffset[, length]])
	 **/
	Buffer.from = function (value, encodingOrOffset, length) {
	  return from(null, value, encodingOrOffset, length);
	};

	if (Buffer.TYPED_ARRAY_SUPPORT) {
	  Buffer.prototype.__proto__ = Uint8Array.prototype;
	  Buffer.__proto__ = Uint8Array;
	  if (typeof Symbol !== 'undefined' && Symbol.species && Buffer[Symbol.species] === Buffer) {
	    // Fix subarray() in ES2016. See: https://github.com/feross/buffer/pull/97
	    Object.defineProperty(Buffer, Symbol.species, {
	      value: null,
	      configurable: true
	    });
	  }
	}

	function assertSize(size) {
	  if (typeof size !== 'number') {
	    throw new TypeError('"size" argument must be a number');
	  }
	}

	function alloc(that, size, fill, encoding) {
	  assertSize(size);
	  if (size <= 0) {
	    return createBuffer(that, size);
	  }
	  if (fill !== undefined) {
	    // Only pay attention to encoding if it's a string. This
	    // prevents accidentally sending in a number that would
	    // be interpretted as a start offset.
	    return typeof encoding === 'string' ? createBuffer(that, size).fill(fill, encoding) : createBuffer(that, size).fill(fill);
	  }
	  return createBuffer(that, size);
	}

	/**
	 * Creates a new filled Buffer instance.
	 * alloc(size[, fill[, encoding]])
	 **/
	Buffer.alloc = function (size, fill, encoding) {
	  return alloc(null, size, fill, encoding);
	};

	function allocUnsafe(that, size) {
	  assertSize(size);
	  that = createBuffer(that, size < 0 ? 0 : checked(size) | 0);
	  if (!Buffer.TYPED_ARRAY_SUPPORT) {
	    for (var i = 0; i < size; i++) {
	      that[i] = 0;
	    }
	  }
	  return that;
	}

	/**
	 * Equivalent to Buffer(num), by default creates a non-zero-filled Buffer instance.
	 * */
	Buffer.allocUnsafe = function (size) {
	  return allocUnsafe(null, size);
	};
	/**
	 * Equivalent to SlowBuffer(num), by default creates a non-zero-filled Buffer instance.
	 */
	Buffer.allocUnsafeSlow = function (size) {
	  return allocUnsafe(null, size);
	};

	function fromString(that, string, encoding) {
	  if (typeof encoding !== 'string' || encoding === '') {
	    encoding = 'utf8';
	  }

	  if (!Buffer.isEncoding(encoding)) {
	    throw new TypeError('"encoding" must be a valid string encoding');
	  }

	  var length = byteLength(string, encoding) | 0;
	  that = createBuffer(that, length);

	  that.write(string, encoding);
	  return that;
	}

	function fromArrayLike(that, array) {
	  var length = checked(array.length) | 0;
	  that = createBuffer(that, length);
	  for (var i = 0; i < length; i += 1) {
	    that[i] = array[i] & 255;
	  }
	  return that;
	}

	function fromArrayBuffer(that, array, byteOffset, length) {
	  array.byteLength; // this throws if `array` is not a valid ArrayBuffer

	  if (byteOffset < 0 || array.byteLength < byteOffset) {
	    throw new RangeError('\'offset\' is out of bounds');
	  }

	  if (array.byteLength < byteOffset + (length || 0)) {
	    throw new RangeError('\'length\' is out of bounds');
	  }

	  if (length === undefined) {
	    array = new Uint8Array(array, byteOffset);
	  } else {
	    array = new Uint8Array(array, byteOffset, length);
	  }

	  if (Buffer.TYPED_ARRAY_SUPPORT) {
	    // Return an augmented `Uint8Array` instance, for best performance
	    that = array;
	    that.__proto__ = Buffer.prototype;
	  } else {
	    // Fallback: Return an object instance of the Buffer class
	    that = fromArrayLike(that, array);
	  }
	  return that;
	}

	function fromObject(that, obj) {
	  if (Buffer.isBuffer(obj)) {
	    var len = checked(obj.length) | 0;
	    that = createBuffer(that, len);

	    if (that.length === 0) {
	      return that;
	    }

	    obj.copy(that, 0, 0, len);
	    return that;
	  }

	  if (obj) {
	    if (typeof ArrayBuffer !== 'undefined' && obj.buffer instanceof ArrayBuffer || 'length' in obj) {
	      if (typeof obj.length !== 'number' || isnan(obj.length)) {
	        return createBuffer(that, 0);
	      }
	      return fromArrayLike(that, obj);
	    }

	    if (obj.type === 'Buffer' && isArray(obj.data)) {
	      return fromArrayLike(that, obj.data);
	    }
	  }

	  throw new TypeError('First argument must be a string, Buffer, ArrayBuffer, Array, or array-like object.');
	}

	function checked(length) {
	  // Note: cannot use `length < kMaxLength` here because that fails when
	  // length is NaN (which is otherwise coerced to zero.)
	  if (length >= kMaxLength()) {
	    throw new RangeError('Attempt to allocate Buffer larger than maximum ' + 'size: 0x' + kMaxLength().toString(16) + ' bytes');
	  }
	  return length | 0;
	}

	function SlowBuffer(length) {
	  if (+length != length) {
	    // eslint-disable-line eqeqeq
	    length = 0;
	  }
	  return Buffer.alloc(+length);
	}

	Buffer.isBuffer = function isBuffer(b) {
	  return !!(b != null && b._isBuffer);
	};

	Buffer.compare = function compare(a, b) {
	  if (!Buffer.isBuffer(a) || !Buffer.isBuffer(b)) {
	    throw new TypeError('Arguments must be Buffers');
	  }

	  if (a === b) return 0;

	  var x = a.length;
	  var y = b.length;

	  for (var i = 0, len = Math.min(x, y); i < len; ++i) {
	    if (a[i] !== b[i]) {
	      x = a[i];
	      y = b[i];
	      break;
	    }
	  }

	  if (x < y) return -1;
	  if (y < x) return 1;
	  return 0;
	};

	Buffer.isEncoding = function isEncoding(encoding) {
	  switch (String(encoding).toLowerCase()) {
	    case 'hex':
	    case 'utf8':
	    case 'utf-8':
	    case 'ascii':
	    case 'binary':
	    case 'base64':
	    case 'raw':
	    case 'ucs2':
	    case 'ucs-2':
	    case 'utf16le':
	    case 'utf-16le':
	      return true;
	    default:
	      return false;
	  }
	};

	Buffer.concat = function concat(list, length) {
	  if (!isArray(list)) {
	    throw new TypeError('"list" argument must be an Array of Buffers');
	  }

	  if (list.length === 0) {
	    return Buffer.alloc(0);
	  }

	  var i;
	  if (length === undefined) {
	    length = 0;
	    for (i = 0; i < list.length; i++) {
	      length += list[i].length;
	    }
	  }

	  var buffer = Buffer.allocUnsafe(length);
	  var pos = 0;
	  for (i = 0; i < list.length; i++) {
	    var buf = list[i];
	    if (!Buffer.isBuffer(buf)) {
	      throw new TypeError('"list" argument must be an Array of Buffers');
	    }
	    buf.copy(buffer, pos);
	    pos += buf.length;
	  }
	  return buffer;
	};

	function byteLength(string, encoding) {
	  if (Buffer.isBuffer(string)) {
	    return string.length;
	  }
	  if (typeof ArrayBuffer !== 'undefined' && typeof ArrayBuffer.isView === 'function' && (ArrayBuffer.isView(string) || string instanceof ArrayBuffer)) {
	    return string.byteLength;
	  }
	  if (typeof string !== 'string') {
	    string = '' + string;
	  }

	  var len = string.length;
	  if (len === 0) return 0;

	  // Use a for loop to avoid recursion
	  var loweredCase = false;
	  for (;;) {
	    switch (encoding) {
	      case 'ascii':
	      case 'binary':
	      // Deprecated
	      case 'raw':
	      case 'raws':
	        return len;
	      case 'utf8':
	      case 'utf-8':
	      case undefined:
	        return utf8ToBytes(string).length;
	      case 'ucs2':
	      case 'ucs-2':
	      case 'utf16le':
	      case 'utf-16le':
	        return len * 2;
	      case 'hex':
	        return len >>> 1;
	      case 'base64':
	        return base64ToBytes(string).length;
	      default:
	        if (loweredCase) return utf8ToBytes(string).length; // assume utf8
	        encoding = ('' + encoding).toLowerCase();
	        loweredCase = true;
	    }
	  }
	}
	Buffer.byteLength = byteLength;

	function slowToString(encoding, start, end) {
	  var loweredCase = false;

	  // No need to verify that "this.length <= MAX_UINT32" since it's a read-only
	  // property of a typed array.

	  // This behaves neither like String nor Uint8Array in that we set start/end
	  // to their upper/lower bounds if the value passed is out of range.
	  // undefined is handled specially as per ECMA-262 6th Edition,
	  // Section 13.3.3.7 Runtime Semantics: KeyedBindingInitialization.
	  if (start === undefined || start < 0) {
	    start = 0;
	  }
	  // Return early if start > this.length. Done here to prevent potential uint32
	  // coercion fail below.
	  if (start > this.length) {
	    return '';
	  }

	  if (end === undefined || end > this.length) {
	    end = this.length;
	  }

	  if (end <= 0) {
	    return '';
	  }

	  // Force coersion to uint32. This will also coerce falsey/NaN values to 0.
	  end >>>= 0;
	  start >>>= 0;

	  if (end <= start) {
	    return '';
	  }

	  if (!encoding) encoding = 'utf8';

	  while (true) {
	    switch (encoding) {
	      case 'hex':
	        return hexSlice(this, start, end);

	      case 'utf8':
	      case 'utf-8':
	        return utf8Slice(this, start, end);

	      case 'ascii':
	        return asciiSlice(this, start, end);

	      case 'binary':
	        return binarySlice(this, start, end);

	      case 'base64':
	        return base64Slice(this, start, end);

	      case 'ucs2':
	      case 'ucs-2':
	      case 'utf16le':
	      case 'utf-16le':
	        return utf16leSlice(this, start, end);

	      default:
	        if (loweredCase) throw new TypeError('Unknown encoding: ' + encoding);
	        encoding = (encoding + '').toLowerCase();
	        loweredCase = true;
	    }
	  }
	}

	// The property is used by `Buffer.isBuffer` and `is-buffer` (in Safari 5-7) to detect
	// Buffer instances.
	Buffer.prototype._isBuffer = true;

	function swap(b, n, m) {
	  var i = b[n];
	  b[n] = b[m];
	  b[m] = i;
	}

	Buffer.prototype.swap16 = function swap16() {
	  var len = this.length;
	  if (len % 2 !== 0) {
	    throw new RangeError('Buffer size must be a multiple of 16-bits');
	  }
	  for (var i = 0; i < len; i += 2) {
	    swap(this, i, i + 1);
	  }
	  return this;
	};

	Buffer.prototype.swap32 = function swap32() {
	  var len = this.length;
	  if (len % 4 !== 0) {
	    throw new RangeError('Buffer size must be a multiple of 32-bits');
	  }
	  for (var i = 0; i < len; i += 4) {
	    swap(this, i, i + 3);
	    swap(this, i + 1, i + 2);
	  }
	  return this;
	};

	Buffer.prototype.toString = function toString() {
	  var length = this.length | 0;
	  if (length === 0) return '';
	  if (arguments.length === 0) return utf8Slice(this, 0, length);
	  return slowToString.apply(this, arguments);
	};

	Buffer.prototype.equals = function equals(b) {
	  if (!Buffer.isBuffer(b)) throw new TypeError('Argument must be a Buffer');
	  if (this === b) return true;
	  return Buffer.compare(this, b) === 0;
	};

	Buffer.prototype.inspect = function inspect() {
	  var str = '';
	  var max = exports.INSPECT_MAX_BYTES;
	  if (this.length > 0) {
	    str = this.toString('hex', 0, max).match(/.{2}/g).join(' ');
	    if (this.length > max) str += ' ... ';
	  }
	  return '<Buffer ' + str + '>';
	};

	Buffer.prototype.compare = function compare(target, start, end, thisStart, thisEnd) {
	  if (!Buffer.isBuffer(target)) {
	    throw new TypeError('Argument must be a Buffer');
	  }

	  if (start === undefined) {
	    start = 0;
	  }
	  if (end === undefined) {
	    end = target ? target.length : 0;
	  }
	  if (thisStart === undefined) {
	    thisStart = 0;
	  }
	  if (thisEnd === undefined) {
	    thisEnd = this.length;
	  }

	  if (start < 0 || end > target.length || thisStart < 0 || thisEnd > this.length) {
	    throw new RangeError('out of range index');
	  }

	  if (thisStart >= thisEnd && start >= end) {
	    return 0;
	  }
	  if (thisStart >= thisEnd) {
	    return -1;
	  }
	  if (start >= end) {
	    return 1;
	  }

	  start >>>= 0;
	  end >>>= 0;
	  thisStart >>>= 0;
	  thisEnd >>>= 0;

	  if (this === target) return 0;

	  var x = thisEnd - thisStart;
	  var y = end - start;
	  var len = Math.min(x, y);

	  var thisCopy = this.slice(thisStart, thisEnd);
	  var targetCopy = target.slice(start, end);

	  for (var i = 0; i < len; ++i) {
	    if (thisCopy[i] !== targetCopy[i]) {
	      x = thisCopy[i];
	      y = targetCopy[i];
	      break;
	    }
	  }

	  if (x < y) return -1;
	  if (y < x) return 1;
	  return 0;
	};

	function arrayIndexOf(arr, val, byteOffset, encoding) {
	  var indexSize = 1;
	  var arrLength = arr.length;
	  var valLength = val.length;

	  if (encoding !== undefined) {
	    encoding = String(encoding).toLowerCase();
	    if (encoding === 'ucs2' || encoding === 'ucs-2' || encoding === 'utf16le' || encoding === 'utf-16le') {
	      if (arr.length < 2 || val.length < 2) {
	        return -1;
	      }
	      indexSize = 2;
	      arrLength /= 2;
	      valLength /= 2;
	      byteOffset /= 2;
	    }
	  }

	  function read(buf, i) {
	    if (indexSize === 1) {
	      return buf[i];
	    } else {
	      return buf.readUInt16BE(i * indexSize);
	    }
	  }

	  var foundIndex = -1;
	  for (var i = 0; byteOffset + i < arrLength; i++) {
	    if (read(arr, byteOffset + i) === read(val, foundIndex === -1 ? 0 : i - foundIndex)) {
	      if (foundIndex === -1) foundIndex = i;
	      if (i - foundIndex + 1 === valLength) return (byteOffset + foundIndex) * indexSize;
	    } else {
	      if (foundIndex !== -1) i -= i - foundIndex;
	      foundIndex = -1;
	    }
	  }
	  return -1;
	}

	Buffer.prototype.indexOf = function indexOf(val, byteOffset, encoding) {
	  if (typeof byteOffset === 'string') {
	    encoding = byteOffset;
	    byteOffset = 0;
	  } else if (byteOffset > 0x7fffffff) {
	    byteOffset = 0x7fffffff;
	  } else if (byteOffset < -0x80000000) {
	    byteOffset = -0x80000000;
	  }
	  byteOffset >>= 0;

	  if (this.length === 0) return -1;
	  if (byteOffset >= this.length) return -1;

	  // Negative offsets start from the end of the buffer
	  if (byteOffset < 0) byteOffset = Math.max(this.length + byteOffset, 0);

	  if (typeof val === 'string') {
	    val = Buffer.from(val, encoding);
	  }

	  if (Buffer.isBuffer(val)) {
	    // special case: looking for empty string/buffer always fails
	    if (val.length === 0) {
	      return -1;
	    }
	    return arrayIndexOf(this, val, byteOffset, encoding);
	  }
	  if (typeof val === 'number') {
	    if (Buffer.TYPED_ARRAY_SUPPORT && Uint8Array.prototype.indexOf === 'function') {
	      return Uint8Array.prototype.indexOf.call(this, val, byteOffset);
	    }
	    return arrayIndexOf(this, [val], byteOffset, encoding);
	  }

	  throw new TypeError('val must be string, number or Buffer');
	};

	Buffer.prototype.includes = function includes(val, byteOffset, encoding) {
	  return this.indexOf(val, byteOffset, encoding) !== -1;
	};

	function hexWrite(buf, string, offset, length) {
	  offset = Number(offset) || 0;
	  var remaining = buf.length - offset;
	  if (!length) {
	    length = remaining;
	  } else {
	    length = Number(length);
	    if (length > remaining) {
	      length = remaining;
	    }
	  }

	  // must be an even number of digits
	  var strLen = string.length;
	  if (strLen % 2 !== 0) throw new Error('Invalid hex string');

	  if (length > strLen / 2) {
	    length = strLen / 2;
	  }
	  for (var i = 0; i < length; i++) {
	    var parsed = parseInt(string.substr(i * 2, 2), 16);
	    if (isNaN(parsed)) return i;
	    buf[offset + i] = parsed;
	  }
	  return i;
	}

	function utf8Write(buf, string, offset, length) {
	  return blitBuffer(utf8ToBytes(string, buf.length - offset), buf, offset, length);
	}

	function asciiWrite(buf, string, offset, length) {
	  return blitBuffer(asciiToBytes(string), buf, offset, length);
	}

	function binaryWrite(buf, string, offset, length) {
	  return asciiWrite(buf, string, offset, length);
	}

	function base64Write(buf, string, offset, length) {
	  return blitBuffer(base64ToBytes(string), buf, offset, length);
	}

	function ucs2Write(buf, string, offset, length) {
	  return blitBuffer(utf16leToBytes(string, buf.length - offset), buf, offset, length);
	}

	Buffer.prototype.write = function write(string, offset, length, encoding) {
	  // Buffer#write(string)
	  if (offset === undefined) {
	    encoding = 'utf8';
	    length = this.length;
	    offset = 0;
	    // Buffer#write(string, encoding)
	  } else if (length === undefined && typeof offset === 'string') {
	      encoding = offset;
	      length = this.length;
	      offset = 0;
	      // Buffer#write(string, offset[, length][, encoding])
	    } else if (isFinite(offset)) {
	        offset = offset | 0;
	        if (isFinite(length)) {
	          length = length | 0;
	          if (encoding === undefined) encoding = 'utf8';
	        } else {
	          encoding = length;
	          length = undefined;
	        }
	        // legacy write(string, encoding, offset, length) - remove in v0.13
	      } else {
	          throw new Error('Buffer.write(string, encoding, offset[, length]) is no longer supported');
	        }

	  var remaining = this.length - offset;
	  if (length === undefined || length > remaining) length = remaining;

	  if (string.length > 0 && (length < 0 || offset < 0) || offset > this.length) {
	    throw new RangeError('Attempt to write outside buffer bounds');
	  }

	  if (!encoding) encoding = 'utf8';

	  var loweredCase = false;
	  for (;;) {
	    switch (encoding) {
	      case 'hex':
	        return hexWrite(this, string, offset, length);

	      case 'utf8':
	      case 'utf-8':
	        return utf8Write(this, string, offset, length);

	      case 'ascii':
	        return asciiWrite(this, string, offset, length);

	      case 'binary':
	        return binaryWrite(this, string, offset, length);

	      case 'base64':
	        // Warning: maxLength not taken into account in base64Write
	        return base64Write(this, string, offset, length);

	      case 'ucs2':
	      case 'ucs-2':
	      case 'utf16le':
	      case 'utf-16le':
	        return ucs2Write(this, string, offset, length);

	      default:
	        if (loweredCase) throw new TypeError('Unknown encoding: ' + encoding);
	        encoding = ('' + encoding).toLowerCase();
	        loweredCase = true;
	    }
	  }
	};

	Buffer.prototype.toJSON = function toJSON() {
	  return {
	    type: 'Buffer',
	    data: Array.prototype.slice.call(this._arr || this, 0)
	  };
	};

	function base64Slice(buf, start, end) {
	  if (start === 0 && end === buf.length) {
	    return base64.fromByteArray(buf);
	  } else {
	    return base64.fromByteArray(buf.slice(start, end));
	  }
	}

	function utf8Slice(buf, start, end) {
	  end = Math.min(buf.length, end);
	  var res = [];

	  var i = start;
	  while (i < end) {
	    var firstByte = buf[i];
	    var codePoint = null;
	    var bytesPerSequence = firstByte > 0xEF ? 4 : firstByte > 0xDF ? 3 : firstByte > 0xBF ? 2 : 1;

	    if (i + bytesPerSequence <= end) {
	      var secondByte, thirdByte, fourthByte, tempCodePoint;

	      switch (bytesPerSequence) {
	        case 1:
	          if (firstByte < 0x80) {
	            codePoint = firstByte;
	          }
	          break;
	        case 2:
	          secondByte = buf[i + 1];
	          if ((secondByte & 0xC0) === 0x80) {
	            tempCodePoint = (firstByte & 0x1F) << 0x6 | secondByte & 0x3F;
	            if (tempCodePoint > 0x7F) {
	              codePoint = tempCodePoint;
	            }
	          }
	          break;
	        case 3:
	          secondByte = buf[i + 1];
	          thirdByte = buf[i + 2];
	          if ((secondByte & 0xC0) === 0x80 && (thirdByte & 0xC0) === 0x80) {
	            tempCodePoint = (firstByte & 0xF) << 0xC | (secondByte & 0x3F) << 0x6 | thirdByte & 0x3F;
	            if (tempCodePoint > 0x7FF && (tempCodePoint < 0xD800 || tempCodePoint > 0xDFFF)) {
	              codePoint = tempCodePoint;
	            }
	          }
	          break;
	        case 4:
	          secondByte = buf[i + 1];
	          thirdByte = buf[i + 2];
	          fourthByte = buf[i + 3];
	          if ((secondByte & 0xC0) === 0x80 && (thirdByte & 0xC0) === 0x80 && (fourthByte & 0xC0) === 0x80) {
	            tempCodePoint = (firstByte & 0xF) << 0x12 | (secondByte & 0x3F) << 0xC | (thirdByte & 0x3F) << 0x6 | fourthByte & 0x3F;
	            if (tempCodePoint > 0xFFFF && tempCodePoint < 0x110000) {
	              codePoint = tempCodePoint;
	            }
	          }
	      }
	    }

	    if (codePoint === null) {
	      // we did not generate a valid codePoint so insert a
	      // replacement char (U+FFFD) and advance only 1 byte
	      codePoint = 0xFFFD;
	      bytesPerSequence = 1;
	    } else if (codePoint > 0xFFFF) {
	      // encode to utf16 (surrogate pair dance)
	      codePoint -= 0x10000;
	      res.push(codePoint >>> 10 & 0x3FF | 0xD800);
	      codePoint = 0xDC00 | codePoint & 0x3FF;
	    }

	    res.push(codePoint);
	    i += bytesPerSequence;
	  }

	  return decodeCodePointsArray(res);
	}

	// Based on http://stackoverflow.com/a/22747272/680742, the browser with
	// the lowest limit is Chrome, with 0x10000 args.
	// We go 1 magnitude less, for safety
	var MAX_ARGUMENTS_LENGTH = 0x1000;

	function decodeCodePointsArray(codePoints) {
	  var len = codePoints.length;
	  if (len <= MAX_ARGUMENTS_LENGTH) {
	    return String.fromCharCode.apply(String, codePoints); // avoid extra slice()
	  }

	  // Decode in chunks to avoid "call stack size exceeded".
	  var res = '';
	  var i = 0;
	  while (i < len) {
	    res += String.fromCharCode.apply(String, codePoints.slice(i, i += MAX_ARGUMENTS_LENGTH));
	  }
	  return res;
	}

	function asciiSlice(buf, start, end) {
	  var ret = '';
	  end = Math.min(buf.length, end);

	  for (var i = start; i < end; i++) {
	    ret += String.fromCharCode(buf[i] & 0x7F);
	  }
	  return ret;
	}

	function binarySlice(buf, start, end) {
	  var ret = '';
	  end = Math.min(buf.length, end);

	  for (var i = start; i < end; i++) {
	    ret += String.fromCharCode(buf[i]);
	  }
	  return ret;
	}

	function hexSlice(buf, start, end) {
	  var len = buf.length;

	  if (!start || start < 0) start = 0;
	  if (!end || end < 0 || end > len) end = len;

	  var out = '';
	  for (var i = start; i < end; i++) {
	    out += toHex(buf[i]);
	  }
	  return out;
	}

	function utf16leSlice(buf, start, end) {
	  var bytes = buf.slice(start, end);
	  var res = '';
	  for (var i = 0; i < bytes.length; i += 2) {
	    res += String.fromCharCode(bytes[i] + bytes[i + 1] * 256);
	  }
	  return res;
	}

	Buffer.prototype.slice = function slice(start, end) {
	  var len = this.length;
	  start = ~ ~start;
	  end = end === undefined ? len : ~ ~end;

	  if (start < 0) {
	    start += len;
	    if (start < 0) start = 0;
	  } else if (start > len) {
	    start = len;
	  }

	  if (end < 0) {
	    end += len;
	    if (end < 0) end = 0;
	  } else if (end > len) {
	    end = len;
	  }

	  if (end < start) end = start;

	  var newBuf;
	  if (Buffer.TYPED_ARRAY_SUPPORT) {
	    newBuf = this.subarray(start, end);
	    newBuf.__proto__ = Buffer.prototype;
	  } else {
	    var sliceLen = end - start;
	    newBuf = new Buffer(sliceLen, undefined);
	    for (var i = 0; i < sliceLen; i++) {
	      newBuf[i] = this[i + start];
	    }
	  }

	  return newBuf;
	};

	/*
	 * Need to make sure that buffer isn't trying to write out of bounds.
	 */
	function checkOffset(offset, ext, length) {
	  if (offset % 1 !== 0 || offset < 0) throw new RangeError('offset is not uint');
	  if (offset + ext > length) throw new RangeError('Trying to access beyond buffer length');
	}

	Buffer.prototype.readUIntLE = function readUIntLE(offset, byteLength, noAssert) {
	  offset = offset | 0;
	  byteLength = byteLength | 0;
	  if (!noAssert) checkOffset(offset, byteLength, this.length);

	  var val = this[offset];
	  var mul = 1;
	  var i = 0;
	  while (++i < byteLength && (mul *= 0x100)) {
	    val += this[offset + i] * mul;
	  }

	  return val;
	};

	Buffer.prototype.readUIntBE = function readUIntBE(offset, byteLength, noAssert) {
	  offset = offset | 0;
	  byteLength = byteLength | 0;
	  if (!noAssert) {
	    checkOffset(offset, byteLength, this.length);
	  }

	  var val = this[offset + --byteLength];
	  var mul = 1;
	  while (byteLength > 0 && (mul *= 0x100)) {
	    val += this[offset + --byteLength] * mul;
	  }

	  return val;
	};

	Buffer.prototype.readUInt8 = function readUInt8(offset, noAssert) {
	  if (!noAssert) checkOffset(offset, 1, this.length);
	  return this[offset];
	};

	Buffer.prototype.readUInt16LE = function readUInt16LE(offset, noAssert) {
	  if (!noAssert) checkOffset(offset, 2, this.length);
	  return this[offset] | this[offset + 1] << 8;
	};

	Buffer.prototype.readUInt16BE = function readUInt16BE(offset, noAssert) {
	  if (!noAssert) checkOffset(offset, 2, this.length);
	  return this[offset] << 8 | this[offset + 1];
	};

	Buffer.prototype.readUInt32LE = function readUInt32LE(offset, noAssert) {
	  if (!noAssert) checkOffset(offset, 4, this.length);

	  return (this[offset] | this[offset + 1] << 8 | this[offset + 2] << 16) + this[offset + 3] * 0x1000000;
	};

	Buffer.prototype.readUInt32BE = function readUInt32BE(offset, noAssert) {
	  if (!noAssert) checkOffset(offset, 4, this.length);

	  return this[offset] * 0x1000000 + (this[offset + 1] << 16 | this[offset + 2] << 8 | this[offset + 3]);
	};

	Buffer.prototype.readIntLE = function readIntLE(offset, byteLength, noAssert) {
	  offset = offset | 0;
	  byteLength = byteLength | 0;
	  if (!noAssert) checkOffset(offset, byteLength, this.length);

	  var val = this[offset];
	  var mul = 1;
	  var i = 0;
	  while (++i < byteLength && (mul *= 0x100)) {
	    val += this[offset + i] * mul;
	  }
	  mul *= 0x80;

	  if (val >= mul) val -= Math.pow(2, 8 * byteLength);

	  return val;
	};

	Buffer.prototype.readIntBE = function readIntBE(offset, byteLength, noAssert) {
	  offset = offset | 0;
	  byteLength = byteLength | 0;
	  if (!noAssert) checkOffset(offset, byteLength, this.length);

	  var i = byteLength;
	  var mul = 1;
	  var val = this[offset + --i];
	  while (i > 0 && (mul *= 0x100)) {
	    val += this[offset + --i] * mul;
	  }
	  mul *= 0x80;

	  if (val >= mul) val -= Math.pow(2, 8 * byteLength);

	  return val;
	};

	Buffer.prototype.readInt8 = function readInt8(offset, noAssert) {
	  if (!noAssert) checkOffset(offset, 1, this.length);
	  if (!(this[offset] & 0x80)) return this[offset];
	  return (0xff - this[offset] + 1) * -1;
	};

	Buffer.prototype.readInt16LE = function readInt16LE(offset, noAssert) {
	  if (!noAssert) checkOffset(offset, 2, this.length);
	  var val = this[offset] | this[offset + 1] << 8;
	  return val & 0x8000 ? val | 0xFFFF0000 : val;
	};

	Buffer.prototype.readInt16BE = function readInt16BE(offset, noAssert) {
	  if (!noAssert) checkOffset(offset, 2, this.length);
	  var val = this[offset + 1] | this[offset] << 8;
	  return val & 0x8000 ? val | 0xFFFF0000 : val;
	};

	Buffer.prototype.readInt32LE = function readInt32LE(offset, noAssert) {
	  if (!noAssert) checkOffset(offset, 4, this.length);

	  return this[offset] | this[offset + 1] << 8 | this[offset + 2] << 16 | this[offset + 3] << 24;
	};

	Buffer.prototype.readInt32BE = function readInt32BE(offset, noAssert) {
	  if (!noAssert) checkOffset(offset, 4, this.length);

	  return this[offset] << 24 | this[offset + 1] << 16 | this[offset + 2] << 8 | this[offset + 3];
	};

	Buffer.prototype.readFloatLE = function readFloatLE(offset, noAssert) {
	  if (!noAssert) checkOffset(offset, 4, this.length);
	  return ieee754.read(this, offset, true, 23, 4);
	};

	Buffer.prototype.readFloatBE = function readFloatBE(offset, noAssert) {
	  if (!noAssert) checkOffset(offset, 4, this.length);
	  return ieee754.read(this, offset, false, 23, 4);
	};

	Buffer.prototype.readDoubleLE = function readDoubleLE(offset, noAssert) {
	  if (!noAssert) checkOffset(offset, 8, this.length);
	  return ieee754.read(this, offset, true, 52, 8);
	};

	Buffer.prototype.readDoubleBE = function readDoubleBE(offset, noAssert) {
	  if (!noAssert) checkOffset(offset, 8, this.length);
	  return ieee754.read(this, offset, false, 52, 8);
	};

	function checkInt(buf, value, offset, ext, max, min) {
	  if (!Buffer.isBuffer(buf)) throw new TypeError('"buffer" argument must be a Buffer instance');
	  if (value > max || value < min) throw new RangeError('"value" argument is out of bounds');
	  if (offset + ext > buf.length) throw new RangeError('Index out of range');
	}

	Buffer.prototype.writeUIntLE = function writeUIntLE(value, offset, byteLength, noAssert) {
	  value = +value;
	  offset = offset | 0;
	  byteLength = byteLength | 0;
	  if (!noAssert) {
	    var maxBytes = Math.pow(2, 8 * byteLength) - 1;
	    checkInt(this, value, offset, byteLength, maxBytes, 0);
	  }

	  var mul = 1;
	  var i = 0;
	  this[offset] = value & 0xFF;
	  while (++i < byteLength && (mul *= 0x100)) {
	    this[offset + i] = value / mul & 0xFF;
	  }

	  return offset + byteLength;
	};

	Buffer.prototype.writeUIntBE = function writeUIntBE(value, offset, byteLength, noAssert) {
	  value = +value;
	  offset = offset | 0;
	  byteLength = byteLength | 0;
	  if (!noAssert) {
	    var maxBytes = Math.pow(2, 8 * byteLength) - 1;
	    checkInt(this, value, offset, byteLength, maxBytes, 0);
	  }

	  var i = byteLength - 1;
	  var mul = 1;
	  this[offset + i] = value & 0xFF;
	  while (--i >= 0 && (mul *= 0x100)) {
	    this[offset + i] = value / mul & 0xFF;
	  }

	  return offset + byteLength;
	};

	Buffer.prototype.writeUInt8 = function writeUInt8(value, offset, noAssert) {
	  value = +value;
	  offset = offset | 0;
	  if (!noAssert) checkInt(this, value, offset, 1, 0xff, 0);
	  if (!Buffer.TYPED_ARRAY_SUPPORT) value = Math.floor(value);
	  this[offset] = value & 0xff;
	  return offset + 1;
	};

	function objectWriteUInt16(buf, value, offset, littleEndian) {
	  if (value < 0) value = 0xffff + value + 1;
	  for (var i = 0, j = Math.min(buf.length - offset, 2); i < j; i++) {
	    buf[offset + i] = (value & 0xff << 8 * (littleEndian ? i : 1 - i)) >>> (littleEndian ? i : 1 - i) * 8;
	  }
	}

	Buffer.prototype.writeUInt16LE = function writeUInt16LE(value, offset, noAssert) {
	  value = +value;
	  offset = offset | 0;
	  if (!noAssert) checkInt(this, value, offset, 2, 0xffff, 0);
	  if (Buffer.TYPED_ARRAY_SUPPORT) {
	    this[offset] = value & 0xff;
	    this[offset + 1] = value >>> 8;
	  } else {
	    objectWriteUInt16(this, value, offset, true);
	  }
	  return offset + 2;
	};

	Buffer.prototype.writeUInt16BE = function writeUInt16BE(value, offset, noAssert) {
	  value = +value;
	  offset = offset | 0;
	  if (!noAssert) checkInt(this, value, offset, 2, 0xffff, 0);
	  if (Buffer.TYPED_ARRAY_SUPPORT) {
	    this[offset] = value >>> 8;
	    this[offset + 1] = value & 0xff;
	  } else {
	    objectWriteUInt16(this, value, offset, false);
	  }
	  return offset + 2;
	};

	function objectWriteUInt32(buf, value, offset, littleEndian) {
	  if (value < 0) value = 0xffffffff + value + 1;
	  for (var i = 0, j = Math.min(buf.length - offset, 4); i < j; i++) {
	    buf[offset + i] = value >>> (littleEndian ? i : 3 - i) * 8 & 0xff;
	  }
	}

	Buffer.prototype.writeUInt32LE = function writeUInt32LE(value, offset, noAssert) {
	  value = +value;
	  offset = offset | 0;
	  if (!noAssert) checkInt(this, value, offset, 4, 0xffffffff, 0);
	  if (Buffer.TYPED_ARRAY_SUPPORT) {
	    this[offset + 3] = value >>> 24;
	    this[offset + 2] = value >>> 16;
	    this[offset + 1] = value >>> 8;
	    this[offset] = value & 0xff;
	  } else {
	    objectWriteUInt32(this, value, offset, true);
	  }
	  return offset + 4;
	};

	Buffer.prototype.writeUInt32BE = function writeUInt32BE(value, offset, noAssert) {
	  value = +value;
	  offset = offset | 0;
	  if (!noAssert) checkInt(this, value, offset, 4, 0xffffffff, 0);
	  if (Buffer.TYPED_ARRAY_SUPPORT) {
	    this[offset] = value >>> 24;
	    this[offset + 1] = value >>> 16;
	    this[offset + 2] = value >>> 8;
	    this[offset + 3] = value & 0xff;
	  } else {
	    objectWriteUInt32(this, value, offset, false);
	  }
	  return offset + 4;
	};

	Buffer.prototype.writeIntLE = function writeIntLE(value, offset, byteLength, noAssert) {
	  value = +value;
	  offset = offset | 0;
	  if (!noAssert) {
	    var limit = Math.pow(2, 8 * byteLength - 1);

	    checkInt(this, value, offset, byteLength, limit - 1, -limit);
	  }

	  var i = 0;
	  var mul = 1;
	  var sub = 0;
	  this[offset] = value & 0xFF;
	  while (++i < byteLength && (mul *= 0x100)) {
	    if (value < 0 && sub === 0 && this[offset + i - 1] !== 0) {
	      sub = 1;
	    }
	    this[offset + i] = (value / mul >> 0) - sub & 0xFF;
	  }

	  return offset + byteLength;
	};

	Buffer.prototype.writeIntBE = function writeIntBE(value, offset, byteLength, noAssert) {
	  value = +value;
	  offset = offset | 0;
	  if (!noAssert) {
	    var limit = Math.pow(2, 8 * byteLength - 1);

	    checkInt(this, value, offset, byteLength, limit - 1, -limit);
	  }

	  var i = byteLength - 1;
	  var mul = 1;
	  var sub = 0;
	  this[offset + i] = value & 0xFF;
	  while (--i >= 0 && (mul *= 0x100)) {
	    if (value < 0 && sub === 0 && this[offset + i + 1] !== 0) {
	      sub = 1;
	    }
	    this[offset + i] = (value / mul >> 0) - sub & 0xFF;
	  }

	  return offset + byteLength;
	};

	Buffer.prototype.writeInt8 = function writeInt8(value, offset, noAssert) {
	  value = +value;
	  offset = offset | 0;
	  if (!noAssert) checkInt(this, value, offset, 1, 0x7f, -0x80);
	  if (!Buffer.TYPED_ARRAY_SUPPORT) value = Math.floor(value);
	  if (value < 0) value = 0xff + value + 1;
	  this[offset] = value & 0xff;
	  return offset + 1;
	};

	Buffer.prototype.writeInt16LE = function writeInt16LE(value, offset, noAssert) {
	  value = +value;
	  offset = offset | 0;
	  if (!noAssert) checkInt(this, value, offset, 2, 0x7fff, -0x8000);
	  if (Buffer.TYPED_ARRAY_SUPPORT) {
	    this[offset] = value & 0xff;
	    this[offset + 1] = value >>> 8;
	  } else {
	    objectWriteUInt16(this, value, offset, true);
	  }
	  return offset + 2;
	};

	Buffer.prototype.writeInt16BE = function writeInt16BE(value, offset, noAssert) {
	  value = +value;
	  offset = offset | 0;
	  if (!noAssert) checkInt(this, value, offset, 2, 0x7fff, -0x8000);
	  if (Buffer.TYPED_ARRAY_SUPPORT) {
	    this[offset] = value >>> 8;
	    this[offset + 1] = value & 0xff;
	  } else {
	    objectWriteUInt16(this, value, offset, false);
	  }
	  return offset + 2;
	};

	Buffer.prototype.writeInt32LE = function writeInt32LE(value, offset, noAssert) {
	  value = +value;
	  offset = offset | 0;
	  if (!noAssert) checkInt(this, value, offset, 4, 0x7fffffff, -0x80000000);
	  if (Buffer.TYPED_ARRAY_SUPPORT) {
	    this[offset] = value & 0xff;
	    this[offset + 1] = value >>> 8;
	    this[offset + 2] = value >>> 16;
	    this[offset + 3] = value >>> 24;
	  } else {
	    objectWriteUInt32(this, value, offset, true);
	  }
	  return offset + 4;
	};

	Buffer.prototype.writeInt32BE = function writeInt32BE(value, offset, noAssert) {
	  value = +value;
	  offset = offset | 0;
	  if (!noAssert) checkInt(this, value, offset, 4, 0x7fffffff, -0x80000000);
	  if (value < 0) value = 0xffffffff + value + 1;
	  if (Buffer.TYPED_ARRAY_SUPPORT) {
	    this[offset] = value >>> 24;
	    this[offset + 1] = value >>> 16;
	    this[offset + 2] = value >>> 8;
	    this[offset + 3] = value & 0xff;
	  } else {
	    objectWriteUInt32(this, value, offset, false);
	  }
	  return offset + 4;
	};

	function checkIEEE754(buf, value, offset, ext, max, min) {
	  if (offset + ext > buf.length) throw new RangeError('Index out of range');
	  if (offset < 0) throw new RangeError('Index out of range');
	}

	function writeFloat(buf, value, offset, littleEndian, noAssert) {
	  if (!noAssert) {
	    checkIEEE754(buf, value, offset, 4, 3.4028234663852886e+38, -3.4028234663852886e+38);
	  }
	  ieee754.write(buf, value, offset, littleEndian, 23, 4);
	  return offset + 4;
	}

	Buffer.prototype.writeFloatLE = function writeFloatLE(value, offset, noAssert) {
	  return writeFloat(this, value, offset, true, noAssert);
	};

	Buffer.prototype.writeFloatBE = function writeFloatBE(value, offset, noAssert) {
	  return writeFloat(this, value, offset, false, noAssert);
	};

	function writeDouble(buf, value, offset, littleEndian, noAssert) {
	  if (!noAssert) {
	    checkIEEE754(buf, value, offset, 8, 1.7976931348623157E+308, -1.7976931348623157E+308);
	  }
	  ieee754.write(buf, value, offset, littleEndian, 52, 8);
	  return offset + 8;
	}

	Buffer.prototype.writeDoubleLE = function writeDoubleLE(value, offset, noAssert) {
	  return writeDouble(this, value, offset, true, noAssert);
	};

	Buffer.prototype.writeDoubleBE = function writeDoubleBE(value, offset, noAssert) {
	  return writeDouble(this, value, offset, false, noAssert);
	};

	// copy(targetBuffer, targetStart=0, sourceStart=0, sourceEnd=buffer.length)
	Buffer.prototype.copy = function copy(target, targetStart, start, end) {
	  if (!start) start = 0;
	  if (!end && end !== 0) end = this.length;
	  if (targetStart >= target.length) targetStart = target.length;
	  if (!targetStart) targetStart = 0;
	  if (end > 0 && end < start) end = start;

	  // Copy 0 bytes; we're done
	  if (end === start) return 0;
	  if (target.length === 0 || this.length === 0) return 0;

	  // Fatal error conditions
	  if (targetStart < 0) {
	    throw new RangeError('targetStart out of bounds');
	  }
	  if (start < 0 || start >= this.length) throw new RangeError('sourceStart out of bounds');
	  if (end < 0) throw new RangeError('sourceEnd out of bounds');

	  // Are we oob?
	  if (end > this.length) end = this.length;
	  if (target.length - targetStart < end - start) {
	    end = target.length - targetStart + start;
	  }

	  var len = end - start;
	  var i;

	  if (this === target && start < targetStart && targetStart < end) {
	    // descending copy from end
	    for (i = len - 1; i >= 0; i--) {
	      target[i + targetStart] = this[i + start];
	    }
	  } else if (len < 1000 || !Buffer.TYPED_ARRAY_SUPPORT) {
	    // ascending copy from start
	    for (i = 0; i < len; i++) {
	      target[i + targetStart] = this[i + start];
	    }
	  } else {
	    Uint8Array.prototype.set.call(target, this.subarray(start, start + len), targetStart);
	  }

	  return len;
	};

	// Usage:
	//    buffer.fill(number[, offset[, end]])
	//    buffer.fill(buffer[, offset[, end]])
	//    buffer.fill(string[, offset[, end]][, encoding])
	Buffer.prototype.fill = function fill(val, start, end, encoding) {
	  // Handle string cases:
	  if (typeof val === 'string') {
	    if (typeof start === 'string') {
	      encoding = start;
	      start = 0;
	      end = this.length;
	    } else if (typeof end === 'string') {
	      encoding = end;
	      end = this.length;
	    }
	    if (val.length === 1) {
	      var code = val.charCodeAt(0);
	      if (code < 256) {
	        val = code;
	      }
	    }
	    if (encoding !== undefined && typeof encoding !== 'string') {
	      throw new TypeError('encoding must be a string');
	    }
	    if (typeof encoding === 'string' && !Buffer.isEncoding(encoding)) {
	      throw new TypeError('Unknown encoding: ' + encoding);
	    }
	  } else if (typeof val === 'number') {
	    val = val & 255;
	  }

	  // Invalid ranges are not set to a default, so can range check early.
	  if (start < 0 || this.length < start || this.length < end) {
	    throw new RangeError('Out of range index');
	  }

	  if (end <= start) {
	    return this;
	  }

	  start = start >>> 0;
	  end = end === undefined ? this.length : end >>> 0;

	  if (!val) val = 0;

	  var i;
	  if (typeof val === 'number') {
	    for (i = start; i < end; i++) {
	      this[i] = val;
	    }
	  } else {
	    var bytes = Buffer.isBuffer(val) ? val : utf8ToBytes(new Buffer(val, encoding).toString());
	    var len = bytes.length;
	    for (i = 0; i < end - start; i++) {
	      this[i + start] = bytes[i % len];
	    }
	  }

	  return this;
	};

	// HELPER FUNCTIONS
	// ================

	var INVALID_BASE64_RE = /[^+\/0-9A-Za-z-_]/g;

	function base64clean(str) {
	  // Node strips out invalid characters like \n and \t from the string, base64-js does not
	  str = stringtrim(str).replace(INVALID_BASE64_RE, '');
	  // Node converts strings with length < 2 to ''
	  if (str.length < 2) return '';
	  // Node allows for non-padded base64 strings (missing trailing ===), base64-js does not
	  while (str.length % 4 !== 0) {
	    str = str + '=';
	  }
	  return str;
	}

	function stringtrim(str) {
	  if (str.trim) return str.trim();
	  return str.replace(/^\s+|\s+$/g, '');
	}

	function toHex(n) {
	  if (n < 16) return '0' + n.toString(16);
	  return n.toString(16);
	}

	function utf8ToBytes(string, units) {
	  units = units || Infinity;
	  var codePoint;
	  var length = string.length;
	  var leadSurrogate = null;
	  var bytes = [];

	  for (var i = 0; i < length; i++) {
	    codePoint = string.charCodeAt(i);

	    // is surrogate component
	    if (codePoint > 0xD7FF && codePoint < 0xE000) {
	      // last char was a lead
	      if (!leadSurrogate) {
	        // no lead yet
	        if (codePoint > 0xDBFF) {
	          // unexpected trail
	          if ((units -= 3) > -1) bytes.push(0xEF, 0xBF, 0xBD);
	          continue;
	        } else if (i + 1 === length) {
	          // unpaired lead
	          if ((units -= 3) > -1) bytes.push(0xEF, 0xBF, 0xBD);
	          continue;
	        }

	        // valid lead
	        leadSurrogate = codePoint;

	        continue;
	      }

	      // 2 leads in a row
	      if (codePoint < 0xDC00) {
	        if ((units -= 3) > -1) bytes.push(0xEF, 0xBF, 0xBD);
	        leadSurrogate = codePoint;
	        continue;
	      }

	      // valid surrogate pair
	      codePoint = (leadSurrogate - 0xD800 << 10 | codePoint - 0xDC00) + 0x10000;
	    } else if (leadSurrogate) {
	      // valid bmp char, but last char was a lead
	      if ((units -= 3) > -1) bytes.push(0xEF, 0xBF, 0xBD);
	    }

	    leadSurrogate = null;

	    // encode utf8
	    if (codePoint < 0x80) {
	      if ((units -= 1) < 0) break;
	      bytes.push(codePoint);
	    } else if (codePoint < 0x800) {
	      if ((units -= 2) < 0) break;
	      bytes.push(codePoint >> 0x6 | 0xC0, codePoint & 0x3F | 0x80);
	    } else if (codePoint < 0x10000) {
	      if ((units -= 3) < 0) break;
	      bytes.push(codePoint >> 0xC | 0xE0, codePoint >> 0x6 & 0x3F | 0x80, codePoint & 0x3F | 0x80);
	    } else if (codePoint < 0x110000) {
	      if ((units -= 4) < 0) break;
	      bytes.push(codePoint >> 0x12 | 0xF0, codePoint >> 0xC & 0x3F | 0x80, codePoint >> 0x6 & 0x3F | 0x80, codePoint & 0x3F | 0x80);
	    } else {
	      throw new Error('Invalid code point');
	    }
	  }

	  return bytes;
	}

	function asciiToBytes(str) {
	  var byteArray = [];
	  for (var i = 0; i < str.length; i++) {
	    // Node's code seems to be doing this and not & 0x7F..
	    byteArray.push(str.charCodeAt(i) & 0xFF);
	  }
	  return byteArray;
	}

	function utf16leToBytes(str, units) {
	  var c, hi, lo;
	  var byteArray = [];
	  for (var i = 0; i < str.length; i++) {
	    if ((units -= 2) < 0) break;

	    c = str.charCodeAt(i);
	    hi = c >> 8;
	    lo = c % 256;
	    byteArray.push(lo);
	    byteArray.push(hi);
	  }

	  return byteArray;
	}

	function base64ToBytes(str) {
	  return base64.toByteArray(base64clean(str));
	}

	function blitBuffer(src, dst, offset, length) {
	  for (var i = 0; i < length; i++) {
	    if (i + offset >= dst.length || i >= src.length) break;
	    dst[i + offset] = src[i];
	  }
	  return i;
	}

	function isnan(val) {
	  return val !== val; // eslint-disable-line no-self-compare
	}
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(10).Buffer, __webpack_require__(3)))

/***/ },
/* 11 */
/***/ function(module, exports) {

	"use strict";
	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	var alpha = exports.alpha = {
	  'en-US': /^[A-Z]+$/i,
	  'cs-CZ': /^[A-ZÁČĎÉĚÍŇÓŘŠŤÚŮÝŽ]+$/i,
	  'de-DE': /^[A-ZÄÖÜß]+$/i,
	  'es-ES': /^[A-ZÁÉÍÑÓÚÜ]+$/i,
	  'fr-FR': /^[A-ZÀÂÆÇÉÈÊËÏÎÔŒÙÛÜŸ]+$/i,
	  'nl-NL': /^[A-ZÉËÏÓÖÜ]+$/i,
	  'hu-HU': /^[A-ZÁÉÓÖŐÚÜŰ]+$/i,
	  'pl-PL': /^[A-ZĄĆĘŚŁŃÓŻŹ]+$/i,
	  'pt-PT': /^[A-ZÃÁÀÂÇÉÊÍÕÓÔÚÜ]+$/i,
	  'ru-RU': /^[А-ЯЁа-яё]+$/i,
	  'tr-TR': /^[A-ZÇĞİıÖŞÜ]+$/i,
	  ar: /^[ءآأؤإئابةتثجحخدذرزسشصضطظعغفقكلمنهوىيًٌٍَُِّْٰ]+$/
	};

	var alphanumeric = exports.alphanumeric = {
	  'en-US': /^[0-9A-Z]+$/i,
	  'cs-CZ': /^[0-9A-ZÁČĎÉĚÍŇÓŘŠŤÚŮÝŽ]+$/i,
	  'de-DE': /^[0-9A-ZÄÖÜß]+$/i,
	  'es-ES': /^[0-9A-ZÁÉÍÑÓÚÜ]+$/i,
	  'fr-FR': /^[0-9A-ZÀÂÆÇÉÈÊËÏÎÔŒÙÛÜŸ]+$/i,
	  'hu-HU': /^[0-9A-ZÁÉÓÖŐÚÜŰ]+$/i,
	  'nl-NL': /^[0-9A-ZÉËÏÓÖÜ]+$/i,
	  'pl-PL': /^[0-9A-ZĄĆĘŚŁŃÓŻŹ]+$/i,
	  'pt-PT': /^[0-9A-ZÃÁÀÂÇÉÊÍÕÓÔÚÜ]+$/i,
	  'ru-RU': /^[0-9А-ЯЁа-яё]+$/i,
	  'tr-TR': /^[0-9A-ZÇĞİıÖŞÜ]+$/i,
	  ar: /^[٠١٢٣٤٥٦٧٨٩0-9ءآأؤإئابةتثجحخدذرزسشصضطظعغفقكلمنهوىيًٌٍَُِّْٰ]+$/
	};

	var englishLocales = exports.englishLocales = ['AU', 'GB', 'HK', 'IN', 'NZ', 'ZA', 'ZM'];

	for (var locale, i = 0; i < englishLocales.length; i++) {
	  locale = 'en-' + englishLocales[i];
	  alpha[locale] = alpha['en-US'];
	  alphanumeric[locale] = alphanumeric['en-US'];
	}

	// Source: http://www.localeplanet.com/java/
	var arabicLocales = exports.arabicLocales = ['AE', 'BH', 'DZ', 'EG', 'IQ', 'JO', 'KW', 'LB', 'LY', 'MA', 'QM', 'QA', 'SA', 'SD', 'SY', 'TN', 'YE'];

	for (var _locale, _i = 0; _i < arabicLocales.length; _i++) {
	  _locale = 'ar-' + arabicLocales[_i];
	  alpha[_locale] = alpha.ar;
	  alphanumeric[_locale] = alphanumeric.ar;
	}

/***/ },
/* 12 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = blacklist;

	var _assertString = __webpack_require__(0);

	var _assertString2 = _interopRequireDefault(_assertString);

	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { default: obj };
	}

	function blacklist(str, chars) {
	  (0, _assertString2.default)(str);
	  return str.replace(new RegExp('[' + chars + ']+', 'g'), '');
	}
	module.exports = exports['default'];

/***/ },
/* 13 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	var _typeof2 = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _typeof = typeof Symbol === "function" && _typeof2(Symbol.iterator) === "symbol" ? function (obj) {
	  return typeof obj === "undefined" ? "undefined" : _typeof2(obj);
	} : function (obj) {
	  return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj === "undefined" ? "undefined" : _typeof2(obj);
	};

	exports.default = isByteLength;

	var _assertString = __webpack_require__(0);

	var _assertString2 = _interopRequireDefault(_assertString);

	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { default: obj };
	}

	/* eslint-disable prefer-rest-params */
	function isByteLength(str, options) {
	  (0, _assertString2.default)(str);
	  var min = void 0;
	  var max = void 0;
	  if ((typeof options === 'undefined' ? 'undefined' : _typeof(options)) === 'object') {
	    min = options.min || 0;
	    max = options.max;
	  } else {
	    // backwards compatibility: isByteLength(str, min [, max])
	    min = arguments[1];
	    max = arguments[2];
	  }
	  var len = encodeURI(str).split(/%..|./).length - 1;
	  return len >= min && (typeof max === 'undefined' || len <= max);
	}
	module.exports = exports['default'];

/***/ },
/* 14 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = isEmail;

	var _assertString = __webpack_require__(0);

	var _assertString2 = _interopRequireDefault(_assertString);

	var _merge = __webpack_require__(5);

	var _merge2 = _interopRequireDefault(_merge);

	var _isByteLength = __webpack_require__(13);

	var _isByteLength2 = _interopRequireDefault(_isByteLength);

	var _isFQDN = __webpack_require__(6);

	var _isFQDN2 = _interopRequireDefault(_isFQDN);

	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { default: obj };
	}

	var default_email_options = {
	  allow_display_name: false,
	  allow_utf8_local_part: true,
	  require_tld: true
	};

	/* eslint-disable max-len */
	/* eslint-disable no-control-regex */
	var displayName = /^[a-z\d!#\$%&'\*\+\-\/=\?\^_`{\|}~\.\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+[a-z\d!#\$%&'\*\+\-\/=\?\^_`{\|}~\.\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF\s]*<(.+)>$/i;
	var emailUserPart = /^[a-z\d!#\$%&'\*\+\-\/=\?\^_`{\|}~]+$/i;
	var quotedEmailUser = /^([\s\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e]|(\\[\x01-\x09\x0b\x0c\x0d-\x7f]))*$/i;
	var emailUserUtf8Part = /^[a-z\d!#\$%&'\*\+\-\/=\?\^_`{\|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+$/i;
	var quotedEmailUserUtf8 = /^([\s\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|(\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*$/i;
	/* eslint-enable max-len */
	/* eslint-enable no-control-regex */

	function isEmail(str, options) {
	  (0, _assertString2.default)(str);
	  options = (0, _merge2.default)(options, default_email_options);

	  if (options.allow_display_name) {
	    var display_email = str.match(displayName);
	    if (display_email) {
	      str = display_email[1];
	    }
	  }

	  var parts = str.split('@');
	  var domain = parts.pop();
	  var user = parts.join('@');

	  var lower_domain = domain.toLowerCase();
	  if (lower_domain === 'gmail.com' || lower_domain === 'googlemail.com') {
	    user = user.replace(/\./g, '').toLowerCase();
	  }

	  if (!(0, _isByteLength2.default)(user, { max: 64 }) || !(0, _isByteLength2.default)(domain, { max: 256 })) {
	    return false;
	  }

	  if (!(0, _isFQDN2.default)(domain, { require_tld: options.require_tld })) {
	    return false;
	  }

	  if (user[0] === '"') {
	    user = user.slice(1, user.length - 1);
	    return options.allow_utf8_local_part ? quotedEmailUserUtf8.test(user) : quotedEmailUser.test(user);
	  }

	  var pattern = options.allow_utf8_local_part ? emailUserUtf8Part : emailUserPart;

	  var user_parts = user.split('.');
	  for (var i = 0; i < user_parts.length; i++) {
	    if (!pattern.test(user_parts[i])) {
	      return false;
	    }
	  }

	  return true;
	}
	module.exports = exports['default'];

/***/ },
/* 15 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.fullWidth = undefined;
	exports.default = isFullWidth;

	var _assertString = __webpack_require__(0);

	var _assertString2 = _interopRequireDefault(_assertString);

	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { default: obj };
	}

	var fullWidth = exports.fullWidth = /[^\u0020-\u007E\uFF61-\uFF9F\uFFA0-\uFFDC\uFFE8-\uFFEE0-9a-zA-Z]/;

	function isFullWidth(str) {
	  (0, _assertString2.default)(str);
	  return fullWidth.test(str);
	}

/***/ },
/* 16 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.halfWidth = undefined;
	exports.default = isHalfWidth;

	var _assertString = __webpack_require__(0);

	var _assertString2 = _interopRequireDefault(_assertString);

	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { default: obj };
	}

	var halfWidth = exports.halfWidth = /[\u0020-\u007E\uFF61-\uFF9F\uFFA0-\uFFDC\uFFE8-\uFFEE0-9a-zA-Z]/;

	function isHalfWidth(str) {
	  (0, _assertString2.default)(str);
	  return halfWidth.test(str);
	}

/***/ },
/* 17 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = isHexadecimal;

	var _assertString = __webpack_require__(0);

	var _assertString2 = _interopRequireDefault(_assertString);

	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { default: obj };
	}

	var hexadecimal = /^[0-9A-F]+$/i;

	function isHexadecimal(str) {
	  (0, _assertString2.default)(str);
	  return hexadecimal.test(str);
	}
	module.exports = exports['default'];

/***/ },
/* 18 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = isIP;

	var _assertString = __webpack_require__(0);

	var _assertString2 = _interopRequireDefault(_assertString);

	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { default: obj };
	}

	var ipv4Maybe = /^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})$/;
	var ipv6Block = /^[0-9A-F]{1,4}$/i;

	function isIP(str) {
	  var version = arguments.length <= 1 || arguments[1] === undefined ? '' : arguments[1];

	  (0, _assertString2.default)(str);
	  version = String(version);
	  if (!version) {
	    return isIP(str, 4) || isIP(str, 6);
	  } else if (version === '4') {
	    if (!ipv4Maybe.test(str)) {
	      return false;
	    }
	    var parts = str.split('.').sort(function (a, b) {
	      return a - b;
	    });
	    return parts[3] <= 255;
	  } else if (version === '6') {
	    var blocks = str.split(':');
	    var foundOmissionBlock = false; // marker to indicate ::

	    // At least some OS accept the last 32 bits of an IPv6 address
	    // (i.e. 2 of the blocks) in IPv4 notation, and RFC 3493 says
	    // that '::ffff:a.b.c.d' is valid for IPv4-mapped IPv6 addresses,
	    // and '::a.b.c.d' is deprecated, but also valid.
	    var foundIPv4TransitionBlock = isIP(blocks[blocks.length - 1], 4);
	    var expectedNumberOfBlocks = foundIPv4TransitionBlock ? 7 : 8;

	    if (blocks.length > expectedNumberOfBlocks) {
	      return false;
	    }
	    // initial or final ::
	    if (str === '::') {
	      return true;
	    } else if (str.substr(0, 2) === '::') {
	      blocks.shift();
	      blocks.shift();
	      foundOmissionBlock = true;
	    } else if (str.substr(str.length - 2) === '::') {
	      blocks.pop();
	      blocks.pop();
	      foundOmissionBlock = true;
	    }

	    for (var i = 0; i < blocks.length; ++i) {
	      // test for a :: which can not be at the string start/end
	      // since those cases have been handled above
	      if (blocks[i] === '' && i > 0 && i < blocks.length - 1) {
	        if (foundOmissionBlock) {
	          return false; // multiple :: in address
	        }
	        foundOmissionBlock = true;
	      } else if (foundIPv4TransitionBlock && i === blocks.length - 1) {
	        // it has been checked before that the last
	        // block is a valid IPv4 address
	      } else if (!ipv6Block.test(blocks[i])) {
	          return false;
	        }
	    }
	    if (foundOmissionBlock) {
	      return blocks.length >= 1;
	    }
	    return blocks.length === expectedNumberOfBlocks;
	  }
	  return false;
	}
	module.exports = exports['default'];

/***/ },
/* 19 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.iso8601 = undefined;

	exports.default = function (str) {
	  (0, _assertString2.default)(str);
	  return iso8601.test(str);
	};

	var _assertString = __webpack_require__(0);

	var _assertString2 = _interopRequireDefault(_assertString);

	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { default: obj };
	}

	/* eslint-disable max-len */
	// from http://goo.gl/0ejHHW
	var iso8601 = exports.iso8601 = /^([\+-]?\d{4}(?!\d{2}\b))((-?)((0[1-9]|1[0-2])(\3([12]\d|0[1-9]|3[01]))?|W([0-4]\d|5[0-2])(-?[1-7])?|(00[1-9]|0[1-9]\d|[12]\d{2}|3([0-5]\d|6[1-6])))([T\s]((([01]\d|2[0-3])((:?)[0-5]\d)?|24\:?00)([\.,]\d+(?!:))?)?(\17[0-5]\d([\.,]\d+)?)?([zZ]|([\+-])([01]\d|2[0-3]):?([0-5]\d)?)?)?)?$/;
	/* eslint-enable max-len */

/***/ },
/* 20 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = toFloat;

	var _assertString = __webpack_require__(0);

	var _assertString2 = _interopRequireDefault(_assertString);

	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { default: obj };
	}

	function toFloat(str) {
	  (0, _assertString2.default)(str);
	  return parseFloat(str);
	}
	module.exports = exports['default'];

/***/ },
/* 21 */
/***/ function(module, exports) {

	"use strict";
	"use strict";

	module.exports = function (module) {
		if (!module.webpackPolyfill) {
			module.deprecate = function () {};
			module.paths = [];
			// module.parent = undefined by default
			if (!module.children) module.children = [];
			Object.defineProperty(module, "loaded", {
				enumerable: true,
				configurable: false,
				get: function get() {
					return module.l;
				}
			});
			Object.defineProperty(module, "id", {
				enumerable: true,
				configurable: false,
				get: function get() {
					return module.i;
				}
			});
			module.webpackPolyfill = 1;
		}
		return module;
	};

/***/ },
/* 22 */
/***/ function(module, exports) {

	/* WEBPACK VAR INJECTION */(function(__webpack_amd_options__) {module.exports = __webpack_amd_options__;

	/* WEBPACK VAR INJECTION */}.call(exports, {}))

/***/ },
/* 23 */,
/* 24 */,
/* 25 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	/* WEBPACK VAR INJECTION */(function(localforage) {"use strict";

	localforage.config({
	    driver: [localforage.INDEXEDDB, localforage.WEBSQL, localforage.LOCALSTORAGE],
	    name: 'mastodont'
	});

	var Store = {
	    Get: localStorage.getItem,
	    Set: localStorage.setItem,
	    Remove: localStorage.removeItem,
	    Clear: localStorage.clear,
	    key: localStorage.key,
	    keys: localStorage.keys,
	    length: localStorage.length
	};

	module.exports = Store;
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(62)))

/***/ },
/* 26 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	/**
	 Copyright © Oleg Bogdanov
	 Developer: Oleg Bogdanov
	 Contacts: olegbogdanov86@gmail.com
	 ------------------------------------------
	 */
	"use strict";

	var $util = __webpack_require__(9),
	    deepExtend = __webpack_require__(65);

	$util.clearStr = function (str) {
	    var patern = arguments.length <= 1 || arguments[1] === undefined ? /[^A-Za-zА-Яа-яЁё!?.,0-9\+\"()\- \s]/gim : arguments[1];

	    return str.replace(patern, "").replace('  ', ' ');
	};

	$util.getNumber = function (str) {
	    var num = 0;

	    if (str && $util.isString(str)) {
	        num = parseInt(str.replace(/[^0-9]/gim, ''));
	        num = $util.isNumber(num) ? num : 0;

	        if (isNaN(num)) num = 0;
	    } else num = str;

	    return num;
	};

	/**
	 * 
	 * @param origin
	 * @param add
	 * @param deep
	 * @returns {*}
	 */
	$util.extend = function (origin, add) {
	    var deep = arguments.length <= 2 || arguments[2] === undefined ? false : arguments[2];

	    if (!add) return;

	    var keys = Object.keys(add),
	        i = keys.length;

	    while (i--) {
	        origin[keys[i]] = add[keys[i]];
	    }

	    if (deep) return JSON.parse(JSON.stringify(origin));else return origin;
	};

	/**
	 * Рекурсивное слияние объектов
	 * @param origin
	 * @param add
	 */
	$util.extendRecurs = function (origin, add) {
	    return deepExtend(origin, add);
	};

	module.exports = $util;

/***/ },
/* 27 */,
/* 28 */
/***/ function(module, exports) {

	"use strict";
	"use strict";

	var _arguments = arguments;
	var Base64 = {

	    // private property
	    _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",

	    // public method for encoding
	    encode: function encode(input) {
	        var output = "";
	        var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
	        var i = 0;

	        input = Base64._utf8_encode(input);

	        while (i < input.length) {

	            chr1 = input.charCodeAt(i++);
	            chr2 = input.charCodeAt(i++);
	            chr3 = input.charCodeAt(i++);

	            enc1 = chr1 >> 2;
	            enc2 = (chr1 & 3) << 4 | chr2 >> 4;
	            enc3 = (chr2 & 15) << 2 | chr3 >> 6;
	            enc4 = chr3 & 63;

	            if (isNaN(chr2)) {
	                enc3 = enc4 = 64;
	            } else if (isNaN(chr3)) {
	                enc4 = 64;
	            }

	            output = output + this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) + this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);
	        }

	        return output;
	    },


	    // public method for decoding
	    decode: function decode(input) {
	        if (!input) return;

	        var output = "";
	        var chr1, chr2, chr3;
	        var enc1, enc2, enc3, enc4;
	        var i = 0;

	        input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

	        while (i < input.length) {

	            enc1 = this._keyStr.indexOf(input.charAt(i++));
	            enc2 = this._keyStr.indexOf(input.charAt(i++));
	            enc3 = this._keyStr.indexOf(input.charAt(i++));
	            enc4 = this._keyStr.indexOf(input.charAt(i++));

	            chr1 = enc1 << 2 | enc2 >> 4;
	            chr2 = (enc2 & 15) << 4 | enc3 >> 2;
	            chr3 = (enc3 & 3) << 6 | enc4;

	            output = output + String.fromCharCode(chr1);

	            if (enc3 != 64) {
	                output = output + String.fromCharCode(chr2);
	            }
	            if (enc4 != 64) {
	                output = output + String.fromCharCode(chr3);
	            }
	        }

	        output = Base64._utf8_decode(output);

	        return output;
	    },


	    // private method for UTF-8 encoding
	    _utf8_encode: function _utf8_encode(string) {
	        string = string.replace(/\r\n/g, "\n");
	        var utftext = "";

	        for (var n = 0; n < string.length; n++) {

	            var c = string.charCodeAt(n);

	            if (c < 128) {
	                utftext += String.fromCharCode(c);
	            } else if (c > 127 && c < 2048) {
	                utftext += String.fromCharCode(c >> 6 | 192);
	                utftext += String.fromCharCode(c & 63 | 128);
	            } else {
	                utftext += String.fromCharCode(c >> 12 | 224);
	                utftext += String.fromCharCode(c >> 6 & 63 | 128);
	                utftext += String.fromCharCode(c & 63 | 128);
	            }
	        }

	        return utftext;
	    },


	    // private method for UTF-8 decoding
	    _utf8_decode: function _utf8_decode(utftext) {
	        var c1,
	            c2,
	            c3,
	            string = "";
	        var i = 0;
	        var c = c1 = c2 = 0;

	        while (i < utftext.length) {

	            c = utftext.charCodeAt(i);

	            if (c < 128) {
	                string += String.fromCharCode(c);
	                i++;
	            } else if (c > 191 && c < 224) {
	                c2 = utftext.charCodeAt(i + 1);
	                string += String.fromCharCode((c & 31) << 6 | c2 & 63);
	                i += 2;
	            } else {
	                c2 = utftext.charCodeAt(i + 1);
	                c3 = utftext.charCodeAt(i + 2);
	                string += String.fromCharCode((c & 15) << 12 | (c2 & 63) << 6 | c3 & 63);
	                i += 3;
	            }
	        }

	        return string;
	    }
	};

	var base64 = {};
	base64.PADCHAR = '=';
	base64.ALPHA = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';

	base64.makeDOMException = function () {
	    // sadly in FF,Safari,Chrome you can't make a DOMException
	    var e, tmp;

	    try {
	        return new DOMException(DOMException.INVALID_CHARACTER_ERR);
	    } catch (tmp) {
	        // not available, just passback a duck-typed equiv
	        // https://developer.mozilla.org/en/Core_JavaScript_1.5_Reference/Global_Objects/Error
	        // https://developer.mozilla.org/en/Core_JavaScript_1.5_Reference/Global_Objects/Error/prototype
	        var ex = new Error("DOM Exception 5");

	        // ex.number and ex.description is IE-specific.
	        ex.code = ex.number = 5;
	        ex.name = ex.description = "INVALID_CHARACTER_ERR";

	        // Safari/Chrome output format
	        ex.toString = function () {
	            return 'Error: ' + ex.name + ': ' + ex.message;
	        };
	        return ex;
	    }
	};

	base64.getbyte64 = function (s, i) {
	    // This is oddly fast, except on Chrome/V8.
	    //  Minimal or no improvement in performance by using a
	    //   object with properties mapping chars to value (eg. 'A': 0)
	    var idx = base64.ALPHA.indexOf(s.charAt(i));
	    if (idx === -1) {
	        throw base64.makeDOMException();
	    }
	    return idx;
	};

	base64.decode = function (s) {
	    // convert to string
	    s = '' + s;
	    var getbyte64 = base64.getbyte64;
	    var pads, i, b10;
	    var imax = s.length;
	    if (imax === 0) {
	        return s;
	    }

	    if (imax % 4 !== 0) {
	        throw base64.makeDOMException();
	    }

	    pads = 0;
	    if (s.charAt(imax - 1) === base64.PADCHAR) {
	        pads = 1;
	        if (s.charAt(imax - 2) === base64.PADCHAR) {
	            pads = 2;
	        }
	        // either way, we want to ignore this last block
	        imax -= 4;
	    }

	    var x = [];
	    for (i = 0; i < imax; i += 4) {
	        b10 = getbyte64(s, i) << 18 | getbyte64(s, i + 1) << 12 | getbyte64(s, i + 2) << 6 | getbyte64(s, i + 3);
	        x.push(String.fromCharCode(b10 >> 16, b10 >> 8 & 0xff, b10 & 0xff));
	    }

	    switch (pads) {
	        case 1:
	            b10 = getbyte64(s, i) << 18 | getbyte64(s, i + 1) << 12 | getbyte64(s, i + 2) << 6;
	            x.push(String.fromCharCode(b10 >> 16, b10 >> 8 & 0xff));
	            break;
	        case 2:
	            b10 = getbyte64(s, i) << 18 | getbyte64(s, i + 1) << 12;
	            x.push(String.fromCharCode(b10 >> 16));
	            break;
	    }
	    return x.join('');
	};

	base64.getbyte = function (s, i) {
	    var x = s.charCodeAt(i);
	    if (x > 255) {
	        throw base64.makeDOMException();
	    }
	    return x;
	};

	base64.encode = function (s) {
	    if (_arguments.length !== 1) {
	        throw new SyntaxError("Not enough arguments");
	    }
	    var padchar = base64.PADCHAR;
	    var alpha = base64.ALPHA;
	    var getbyte = base64.getbyte;

	    var i, b10;
	    var x = [];

	    // convert to string
	    s = '' + s;

	    var imax = s.length - s.length % 3;

	    if (s.length === 0) {
	        return s;
	    }
	    for (i = 0; i < imax; i += 3) {
	        b10 = getbyte(s, i) << 16 | getbyte(s, i + 1) << 8 | getbyte(s, i + 2);
	        x.push(alpha.charAt(b10 >> 18));
	        x.push(alpha.charAt(b10 >> 12 & 0x3F));
	        x.push(alpha.charAt(b10 >> 6 & 0x3f));
	        x.push(alpha.charAt(b10 & 0x3f));
	    }
	    switch (s.length - imax) {
	        case 1:
	            b10 = getbyte(s, i) << 16;
	            x.push(alpha.charAt(b10 >> 18) + alpha.charAt(b10 >> 12 & 0x3F) + padchar + padchar);
	            break;
	        case 2:
	            b10 = getbyte(s, i) << 16 | getbyte(s, i + 1) << 8;
	            x.push(alpha.charAt(b10 >> 18) + alpha.charAt(b10 >> 12 & 0x3F) + alpha.charAt(b10 >> 6 & 0x3f) + padchar);
	            break;
	    }
	    return x.join('');
	};

	module.exports = Base64;

/***/ },
/* 29 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	var validator = __webpack_require__(75);

	validator.checkPass = function (str) {
	    return (/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$/.test(str)
	    );
	};

	validator.clearStr = function (str) {
	    return (/^[а-яА-ЯёЁa-zA-Z0-9 -_\.]+$/.test(str)
	    );
	};

	validator.Email = validator.isEmail;

	module.exports = validator;

/***/ },
/* 30 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	/**
	 * Service for sending network requests.
	 */

	var _ = __webpack_require__(1);
	var Client = __webpack_require__(45);
	var Promise = __webpack_require__(4);
	var interceptor = __webpack_require__(51);
	var jsonType = { 'Content-Type': 'application/json' };

	function Http(url, options) {

	    var client = Client,
	        request,
	        promise;

	    Http.interceptors.forEach(function (handler) {
	        client = interceptor(handler, this.$vm)(client);
	    }, this);

	    options = _.isObject(url) ? url : _.extend({ url: url }, options);
	    request = _.merge({}, Http.options, this.$options, options);
	    promise = client(request).bind(this.$vm).then(function (response) {

	        return response.ok ? response : Promise.reject(response);
	    }, function (response) {

	        if (response instanceof Error) {
	            _.error(response);
	        }

	        return Promise.reject(response);
	    });

	    if (request.success) {
	        promise.success(request.success);
	    }

	    if (request.error) {
	        promise.error(request.error);
	    }

	    return promise;
	}

	Http.options = {
	    method: 'get',
	    data: '',
	    params: {},
	    headers: {},
	    xhr: null,
	    upload: null,
	    jsonp: 'callback',
	    beforeSend: null,
	    crossOrigin: null,
	    emulateHTTP: false,
	    emulateJSON: false,
	    timeout: 0
	};

	Http.interceptors = [__webpack_require__(44), __webpack_require__(55), __webpack_require__(52), __webpack_require__(53), __webpack_require__(54), __webpack_require__(50), __webpack_require__(49)];

	Http.headers = {
	    put: jsonType,
	    post: jsonType,
	    patch: jsonType,
	    delete: jsonType,
	    common: { 'Accept': 'application/json, text/plain, */*' },
	    custom: { 'X-Requested-With': 'XMLHttpRequest' }
	};

	['get', 'put', 'post', 'patch', 'delete', 'jsonp'].forEach(function (method) {

	    Http[method] = function (url, data, success, options) {

	        if (_.isFunction(data)) {
	            options = success;
	            success = data;
	            data = undefined;
	        }

	        if (_.isObject(success)) {
	            options = success;
	            success = undefined;
	        }

	        return this(url, _.extend({ method: method, data: data, success: success }, options));
	    };
	});

	module.exports = _.http = Http;

/***/ },
/* 31 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	/**
	 * Service for interacting with RESTful services.
	 */

	var _ = __webpack_require__(1);

	function Resource(url, params, actions, options) {

	    var self = this,
	        resource = {};

	    actions = _.extend({}, Resource.actions, actions);

	    _.each(actions, function (action, name) {

	        action = _.merge({ url: url, params: params || {} }, options, action);

	        resource[name] = function () {
	            return (self.$http || _.http)(opts(action, arguments));
	        };
	    });

	    return resource;
	}

	function opts(action, args) {

	    var options = _.extend({}, action),
	        params = {},
	        data,
	        success,
	        error;

	    switch (args.length) {

	        case 4:

	            error = args[3];
	            success = args[2];

	        case 3:
	        case 2:

	            if (_.isFunction(args[1])) {

	                if (_.isFunction(args[0])) {

	                    success = args[0];
	                    error = args[1];

	                    break;
	                }

	                success = args[1];
	                error = args[2];
	            } else {

	                params = args[0];
	                data = args[1];
	                success = args[2];

	                break;
	            }

	        case 1:

	            if (_.isFunction(args[0])) {
	                success = args[0];
	            } else if (/^(POST|PUT|PATCH)$/i.test(options.method)) {
	                data = args[0];
	            } else {
	                params = args[0];
	            }

	            break;

	        case 0:

	            break;

	        default:

	            throw 'Expected up to 4 arguments [params, data, success, error], got ' + args.length + ' arguments';
	    }

	    options.data = data;
	    options.params = _.extend({}, options.params, params);

	    if (success) {
	        options.success = success;
	    }

	    if (error) {
	        options.error = error;
	    }

	    return options;
	}

	Resource.actions = {

	    get: { method: 'GET' },
	    save: { method: 'POST' },
	    query: { method: 'GET' },
	    update: { method: 'PUT' },
	    remove: { method: 'DELETE' },
	    delete: { method: 'DELETE' }

	};

	module.exports = _.resource = Resource;

/***/ },
/* 32 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	/**
	 * Service for URL templating.
	 */

	var _ = __webpack_require__(1);
	var ie = document.documentMode;
	var el = document.createElement('a');

	function Url(url, params) {

	    var options = url,
	        transform;

	    if (_.isString(url)) {
	        options = { url: url, params: params };
	    }

	    options = _.merge({}, Url.options, this.$options, options);

	    Url.transforms.forEach(function (handler) {
	        transform = factory(handler, transform, this.$vm);
	    }, this);

	    return transform(options);
	};

	/**
	 * Url options.
	 */

	Url.options = {
	    url: '',
	    root: null,
	    params: {}
	};

	/**
	 * Url transforms.
	 */

	Url.transforms = [__webpack_require__(61), __webpack_require__(58), __webpack_require__(59), __webpack_require__(60)];

	/**
	 * Encodes a Url parameter string.
	 *
	 * @param {Object} obj
	 */

	Url.params = function (obj) {

	    var params = [],
	        escape = encodeURIComponent;

	    params.add = function (key, value) {

	        if (_.isFunction(value)) {
	            value = value();
	        }

	        if (value === null) {
	            value = '';
	        }

	        this.push(escape(key) + '=' + escape(value));
	    };

	    serialize(params, obj);

	    return params.join('&').replace(/%20/g, '+');
	};

	/**
	 * Parse a URL and return its components.
	 *
	 * @param {String} url
	 */

	Url.parse = function (url) {

	    if (ie) {
	        el.href = url;
	        url = el.href;
	    }

	    el.href = url;

	    return {
	        href: el.href,
	        protocol: el.protocol ? el.protocol.replace(/:$/, '') : '',
	        port: el.port,
	        host: el.host,
	        hostname: el.hostname,
	        pathname: el.pathname.charAt(0) === '/' ? el.pathname : '/' + el.pathname,
	        search: el.search ? el.search.replace(/^\?/, '') : '',
	        hash: el.hash ? el.hash.replace(/^#/, '') : ''
	    };
	};

	function factory(handler, next, vm) {
	    return function (options) {
	        return handler.call(vm, options, next);
	    };
	}

	function serialize(params, obj, scope) {

	    var array = _.isArray(obj),
	        plain = _.isPlainObject(obj),
	        hash;

	    _.each(obj, function (value, key) {

	        hash = _.isObject(value) || _.isArray(value);

	        if (scope) {
	            key = scope + '[' + (plain || hash ? key : '') + ']';
	        }

	        if (!scope && array) {
	            params.add(value.name, value.value);
	        } else if (hash) {
	            serialize(params, value, key);
	        } else {
	            params.add(key, value);
	        }
	    });
	}

	module.exports = _.url = Url;

/***/ },
/* 33 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_RESULT__;'use strict';

	var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };

	/*!
	 * JavaScript Cookie v2.1.0
	 * https://github.com/js-cookie/js-cookie
	 *
	 * Copyright 2006, 2015 Klaus Hartl & Fagner Brack
	 * Released under the MIT license
	 */
	(function (factory) {
		if (true) {
			!(__WEBPACK_AMD_DEFINE_FACTORY__ = (factory), __WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ? (__WEBPACK_AMD_DEFINE_FACTORY__.call(exports, __webpack_require__, exports, module)) : __WEBPACK_AMD_DEFINE_FACTORY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
		} else if ((typeof exports === 'undefined' ? 'undefined' : _typeof(exports)) === 'object') {
			module.exports = factory();
		} else {
			var _OldCookies = window.Cookies;
			var api = window.Cookies = factory();
			api.noConflict = function () {
				window.Cookies = _OldCookies;
				return api;
			};
		}
	})(function () {
		function extend() {
			var i = 0;
			var result = {};
			for (; i < arguments.length; i++) {
				var attributes = arguments[i];
				for (var key in attributes) {
					result[key] = attributes[key];
				}
			}
			return result;
		}

		function init(converter) {
			function api(key, value, attributes) {
				var result;

				// Write

				if (arguments.length > 1) {
					attributes = extend({
						path: '/'
					}, api.defaults, attributes);

					if (typeof attributes.expires === 'number') {
						var expires = new Date();
						expires.setMilliseconds(expires.getMilliseconds() + attributes.expires * 864e+5);
						attributes.expires = expires;
					}

					try {
						result = JSON.stringify(value);
						if (/^[\{\[]/.test(result)) {
							value = result;
						}
					} catch (e) {}

					if (!converter.write) {
						value = encodeURIComponent(String(value)).replace(/%(23|24|26|2B|3A|3C|3E|3D|2F|3F|40|5B|5D|5E|60|7B|7D|7C)/g, decodeURIComponent);
					} else {
						value = converter.write(value, key);
					}

					key = encodeURIComponent(String(key));
					key = key.replace(/%(23|24|26|2B|5E|60|7C)/g, decodeURIComponent);
					key = key.replace(/[\(\)]/g, escape);

					return document.cookie = [key, '=', value, attributes.expires && '; expires=' + attributes.expires.toUTCString(), // use expires attribute, max-age is not supported by IE
					attributes.path && '; path=' + attributes.path, attributes.domain && '; domain=' + attributes.domain, attributes.secure ? '; secure' : ''].join('');
				}

				// Read

				if (!key) {
					result = {};
				}

				// To prevent the for loop in the first place assign an empty array
				// in case there are no cookies at all. Also prevents odd result when
				// calling "get()"
				var cookies = document.cookie ? document.cookie.split('; ') : [];
				var rdecode = /(%[0-9A-Z]{2})+/g;
				var i = 0;

				for (; i < cookies.length; i++) {
					var parts = cookies[i].split('=');
					var name = parts[0].replace(rdecode, decodeURIComponent);
					var cookie = parts.slice(1).join('=');

					if (cookie.charAt(0) === '"') {
						cookie = cookie.slice(1, -1);
					}

					try {
						cookie = converter.read ? converter.read(cookie, name) : converter(cookie, name) || cookie.replace(rdecode, decodeURIComponent);

						if (this.json) {
							try {
								cookie = JSON.parse(cookie);
							} catch (e) {}
						}

						if (key === name) {
							result = cookie;
							break;
						}

						if (!key) {
							result[name] = cookie;
						}
					} catch (e) {}
				}

				return result;
			}

			api.get = api.set = api;
			api.getJSON = function () {
				return api.apply({
					json: true
				}, [].slice.call(arguments));
			};
			api.defaults = {};

			api.remove = function (key, attributes) {
				api(key, '', extend(attributes, {
					expires: -1
				}));
			};

			api.withConverter = init;

			return api;
		}

		return init(function () {});
	});

/***/ },
/* 34 */
/***/ function(module, exports) {

	"use strict";
	'use strict';

	var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };

	// Copyright Joyent, Inc. and other Node contributors.
	//
	// Permission is hereby granted, free of charge, to any person obtaining a
	// copy of this software and associated documentation files (the
	// "Software"), to deal in the Software without restriction, including
	// without limitation the rights to use, copy, modify, merge, publish,
	// distribute, sublicense, and/or sell copies of the Software, and to permit
	// persons to whom the Software is furnished to do so, subject to the
	// following conditions:
	//
	// The above copyright notice and this permission notice shall be included
	// in all copies or substantial portions of the Software.
	//
	// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
	// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
	// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
	// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
	// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
	// USE OR OTHER DEALINGS IN THE SOFTWARE.

	function EventEmitter() {
	  this._events = this._events || {};
	  this._maxListeners = this._maxListeners || undefined;
	}
	module.exports = EventEmitter;

	// Backwards-compat with node 0.10.x
	EventEmitter.EventEmitter = EventEmitter;

	EventEmitter.prototype._events = undefined;
	EventEmitter.prototype._maxListeners = undefined;

	// By default EventEmitters will print a warning if more than 10 listeners are
	// added to it. This is a useful default which helps finding memory leaks.
	EventEmitter.defaultMaxListeners = 10;

	// Obviously not all Emitters should be limited to 10. This function allows
	// that to be increased. Set to zero for unlimited.
	EventEmitter.prototype.setMaxListeners = function (n) {
	  if (!isNumber(n) || n < 0 || isNaN(n)) throw TypeError('n must be a positive number');
	  this._maxListeners = n;
	  return this;
	};

	EventEmitter.prototype.emit = function (type) {
	  var er, handler, len, args, i, listeners;

	  if (!this._events) this._events = {};

	  // If there is no 'error' event listener then throw.
	  if (type === 'error') {
	    if (!this._events.error || isObject(this._events.error) && !this._events.error.length) {
	      er = arguments[1];
	      if (er instanceof Error) {
	        throw er; // Unhandled 'error' event
	      }
	      throw TypeError('Uncaught, unspecified "error" event.');
	    }
	  }

	  handler = this._events[type];

	  if (isUndefined(handler)) return false;

	  if (isFunction(handler)) {
	    switch (arguments.length) {
	      // fast cases
	      case 1:
	        handler.call(this);
	        break;
	      case 2:
	        handler.call(this, arguments[1]);
	        break;
	      case 3:
	        handler.call(this, arguments[1], arguments[2]);
	        break;
	      // slower
	      default:
	        args = Array.prototype.slice.call(arguments, 1);
	        handler.apply(this, args);
	    }
	  } else if (isObject(handler)) {
	    args = Array.prototype.slice.call(arguments, 1);
	    listeners = handler.slice();
	    len = listeners.length;
	    for (i = 0; i < len; i++) {
	      listeners[i].apply(this, args);
	    }
	  }

	  return true;
	};

	EventEmitter.prototype.addListener = function (type, listener) {
	  var m;

	  if (!isFunction(listener)) throw TypeError('listener must be a function');

	  if (!this._events) this._events = {};

	  // To avoid recursion in the case that type === "newListener"! Before
	  // adding it to the listeners, first emit "newListener".
	  if (this._events.newListener) this.emit('newListener', type, isFunction(listener.listener) ? listener.listener : listener);

	  if (!this._events[type])
	    // Optimize the case of one listener. Don't need the extra array object.
	    this._events[type] = listener;else if (isObject(this._events[type]))
	    // If we've already got an array, just append.
	    this._events[type].push(listener);else
	    // Adding the second element, need to change to array.
	    this._events[type] = [this._events[type], listener];

	  // Check for listener leak
	  if (isObject(this._events[type]) && !this._events[type].warned) {
	    if (!isUndefined(this._maxListeners)) {
	      m = this._maxListeners;
	    } else {
	      m = EventEmitter.defaultMaxListeners;
	    }

	    if (m && m > 0 && this._events[type].length > m) {
	      this._events[type].warned = true;
	      console.error('(node) warning: possible EventEmitter memory ' + 'leak detected. %d listeners added. ' + 'Use emitter.setMaxListeners() to increase limit.', this._events[type].length);
	      if (typeof console.trace === 'function') {
	        // not supported in IE 10
	        console.trace();
	      }
	    }
	  }

	  return this;
	};

	EventEmitter.prototype.on = EventEmitter.prototype.addListener;

	EventEmitter.prototype.once = function (type, listener) {
	  if (!isFunction(listener)) throw TypeError('listener must be a function');

	  var fired = false;

	  function g() {
	    this.removeListener(type, g);

	    if (!fired) {
	      fired = true;
	      listener.apply(this, arguments);
	    }
	  }

	  g.listener = listener;
	  this.on(type, g);

	  return this;
	};

	// emits a 'removeListener' event iff the listener was removed
	EventEmitter.prototype.removeListener = function (type, listener) {
	  var list, position, length, i;

	  if (!isFunction(listener)) throw TypeError('listener must be a function');

	  if (!this._events || !this._events[type]) return this;

	  list = this._events[type];
	  length = list.length;
	  position = -1;

	  if (list === listener || isFunction(list.listener) && list.listener === listener) {
	    delete this._events[type];
	    if (this._events.removeListener) this.emit('removeListener', type, listener);
	  } else if (isObject(list)) {
	    for (i = length; i-- > 0;) {
	      if (list[i] === listener || list[i].listener && list[i].listener === listener) {
	        position = i;
	        break;
	      }
	    }

	    if (position < 0) return this;

	    if (list.length === 1) {
	      list.length = 0;
	      delete this._events[type];
	    } else {
	      list.splice(position, 1);
	    }

	    if (this._events.removeListener) this.emit('removeListener', type, listener);
	  }

	  return this;
	};

	EventEmitter.prototype.removeAllListeners = function (type) {
	  var key, listeners;

	  if (!this._events) return this;

	  // not listening for removeListener, no need to emit
	  if (!this._events.removeListener) {
	    if (arguments.length === 0) this._events = {};else if (this._events[type]) delete this._events[type];
	    return this;
	  }

	  // emit removeListener for all listeners on all events
	  if (arguments.length === 0) {
	    for (key in this._events) {
	      if (key === 'removeListener') continue;
	      this.removeAllListeners(key);
	    }
	    this.removeAllListeners('removeListener');
	    this._events = {};
	    return this;
	  }

	  listeners = this._events[type];

	  if (isFunction(listeners)) {
	    this.removeListener(type, listeners);
	  } else if (listeners) {
	    // LIFO order
	    while (listeners.length) {
	      this.removeListener(type, listeners[listeners.length - 1]);
	    }
	  }
	  delete this._events[type];

	  return this;
	};

	EventEmitter.prototype.listeners = function (type) {
	  var ret;
	  if (!this._events || !this._events[type]) ret = [];else if (isFunction(this._events[type])) ret = [this._events[type]];else ret = this._events[type].slice();
	  return ret;
	};

	EventEmitter.prototype.listenerCount = function (type) {
	  if (this._events) {
	    var evlistener = this._events[type];

	    if (isFunction(evlistener)) return 1;else if (evlistener) return evlistener.length;
	  }
	  return 0;
	};

	EventEmitter.listenerCount = function (emitter, type) {
	  return emitter.listenerCount(type);
	};

	function isFunction(arg) {
	  return typeof arg === 'function';
	}

	function isNumber(arg) {
	  return typeof arg === 'number';
	}

	function isObject(arg) {
	  return (typeof arg === 'undefined' ? 'undefined' : _typeof(arg)) === 'object' && arg !== null;
	}

	function isUndefined(arg) {
	  return arg === void 0;
	}

/***/ },
/* 35 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	"use strict";

	// THIS FILE IS GENERATED - DO NOT EDIT!
	/*global module:false, define:false*/

	(function (define, undefined) {
	    define(function () {
	        'use strict';

	        var impl = {};

	        impl.mobileDetectRules = {
	            "phones": {
	                "iPhone": "\\biPhone\\b|\\biPod\\b",
	                "BlackBerry": "BlackBerry|\\bBB10\\b|rim[0-9]+",
	                "HTC": "HTC|HTC.*(Sensation|Evo|Vision|Explorer|6800|8100|8900|A7272|S510e|C110e|Legend|Desire|T8282)|APX515CKT|Qtek9090|APA9292KT|HD_mini|Sensation.*Z710e|PG86100|Z715e|Desire.*(A8181|HD)|ADR6200|ADR6400L|ADR6425|001HT|Inspire 4G|Android.*\\bEVO\\b|T-Mobile G1|Z520m",
	                "Nexus": "Nexus One|Nexus S|Galaxy.*Nexus|Android.*Nexus.*Mobile|Nexus 4|Nexus 5|Nexus 6",
	                "Dell": "Dell.*Streak|Dell.*Aero|Dell.*Venue|DELL.*Venue Pro|Dell Flash|Dell Smoke|Dell Mini 3iX|XCD28|XCD35|\\b001DL\\b|\\b101DL\\b|\\bGS01\\b",
	                "Motorola": "Motorola|DROIDX|DROID BIONIC|\\bDroid\\b.*Build|Android.*Xoom|HRI39|MOT-|A1260|A1680|A555|A853|A855|A953|A955|A956|Motorola.*ELECTRIFY|Motorola.*i1|i867|i940|MB200|MB300|MB501|MB502|MB508|MB511|MB520|MB525|MB526|MB611|MB612|MB632|MB810|MB855|MB860|MB861|MB865|MB870|ME501|ME502|ME511|ME525|ME600|ME632|ME722|ME811|ME860|ME863|ME865|MT620|MT710|MT716|MT720|MT810|MT870|MT917|Motorola.*TITANIUM|WX435|WX445|XT300|XT301|XT311|XT316|XT317|XT319|XT320|XT390|XT502|XT530|XT531|XT532|XT535|XT603|XT610|XT611|XT615|XT681|XT701|XT702|XT711|XT720|XT800|XT806|XT860|XT862|XT875|XT882|XT883|XT894|XT901|XT907|XT909|XT910|XT912|XT928|XT926|XT915|XT919|XT925|XT1021|\\bMoto E\\b",
	                "Samsung": "Samsung|SM-G9250|GT-19300|SGH-I337|BGT-S5230|GT-B2100|GT-B2700|GT-B2710|GT-B3210|GT-B3310|GT-B3410|GT-B3730|GT-B3740|GT-B5510|GT-B5512|GT-B5722|GT-B6520|GT-B7300|GT-B7320|GT-B7330|GT-B7350|GT-B7510|GT-B7722|GT-B7800|GT-C3010|GT-C3011|GT-C3060|GT-C3200|GT-C3212|GT-C3212I|GT-C3262|GT-C3222|GT-C3300|GT-C3300K|GT-C3303|GT-C3303K|GT-C3310|GT-C3322|GT-C3330|GT-C3350|GT-C3500|GT-C3510|GT-C3530|GT-C3630|GT-C3780|GT-C5010|GT-C5212|GT-C6620|GT-C6625|GT-C6712|GT-E1050|GT-E1070|GT-E1075|GT-E1080|GT-E1081|GT-E1085|GT-E1087|GT-E1100|GT-E1107|GT-E1110|GT-E1120|GT-E1125|GT-E1130|GT-E1160|GT-E1170|GT-E1175|GT-E1180|GT-E1182|GT-E1200|GT-E1210|GT-E1225|GT-E1230|GT-E1390|GT-E2100|GT-E2120|GT-E2121|GT-E2152|GT-E2220|GT-E2222|GT-E2230|GT-E2232|GT-E2250|GT-E2370|GT-E2550|GT-E2652|GT-E3210|GT-E3213|GT-I5500|GT-I5503|GT-I5700|GT-I5800|GT-I5801|GT-I6410|GT-I6420|GT-I7110|GT-I7410|GT-I7500|GT-I8000|GT-I8150|GT-I8160|GT-I8190|GT-I8320|GT-I8330|GT-I8350|GT-I8530|GT-I8700|GT-I8703|GT-I8910|GT-I9000|GT-I9001|GT-I9003|GT-I9010|GT-I9020|GT-I9023|GT-I9070|GT-I9082|GT-I9100|GT-I9103|GT-I9220|GT-I9250|GT-I9300|GT-I9305|GT-I9500|GT-I9505|GT-M3510|GT-M5650|GT-M7500|GT-M7600|GT-M7603|GT-M8800|GT-M8910|GT-N7000|GT-S3110|GT-S3310|GT-S3350|GT-S3353|GT-S3370|GT-S3650|GT-S3653|GT-S3770|GT-S3850|GT-S5210|GT-S5220|GT-S5229|GT-S5230|GT-S5233|GT-S5250|GT-S5253|GT-S5260|GT-S5263|GT-S5270|GT-S5300|GT-S5330|GT-S5350|GT-S5360|GT-S5363|GT-S5369|GT-S5380|GT-S5380D|GT-S5560|GT-S5570|GT-S5600|GT-S5603|GT-S5610|GT-S5620|GT-S5660|GT-S5670|GT-S5690|GT-S5750|GT-S5780|GT-S5830|GT-S5839|GT-S6102|GT-S6500|GT-S7070|GT-S7200|GT-S7220|GT-S7230|GT-S7233|GT-S7250|GT-S7500|GT-S7530|GT-S7550|GT-S7562|GT-S7710|GT-S8000|GT-S8003|GT-S8500|GT-S8530|GT-S8600|SCH-A310|SCH-A530|SCH-A570|SCH-A610|SCH-A630|SCH-A650|SCH-A790|SCH-A795|SCH-A850|SCH-A870|SCH-A890|SCH-A930|SCH-A950|SCH-A970|SCH-A990|SCH-I100|SCH-I110|SCH-I400|SCH-I405|SCH-I500|SCH-I510|SCH-I515|SCH-I600|SCH-I730|SCH-I760|SCH-I770|SCH-I830|SCH-I910|SCH-I920|SCH-I959|SCH-LC11|SCH-N150|SCH-N300|SCH-R100|SCH-R300|SCH-R351|SCH-R400|SCH-R410|SCH-T300|SCH-U310|SCH-U320|SCH-U350|SCH-U360|SCH-U365|SCH-U370|SCH-U380|SCH-U410|SCH-U430|SCH-U450|SCH-U460|SCH-U470|SCH-U490|SCH-U540|SCH-U550|SCH-U620|SCH-U640|SCH-U650|SCH-U660|SCH-U700|SCH-U740|SCH-U750|SCH-U810|SCH-U820|SCH-U900|SCH-U940|SCH-U960|SCS-26UC|SGH-A107|SGH-A117|SGH-A127|SGH-A137|SGH-A157|SGH-A167|SGH-A177|SGH-A187|SGH-A197|SGH-A227|SGH-A237|SGH-A257|SGH-A437|SGH-A517|SGH-A597|SGH-A637|SGH-A657|SGH-A667|SGH-A687|SGH-A697|SGH-A707|SGH-A717|SGH-A727|SGH-A737|SGH-A747|SGH-A767|SGH-A777|SGH-A797|SGH-A817|SGH-A827|SGH-A837|SGH-A847|SGH-A867|SGH-A877|SGH-A887|SGH-A897|SGH-A927|SGH-B100|SGH-B130|SGH-B200|SGH-B220|SGH-C100|SGH-C110|SGH-C120|SGH-C130|SGH-C140|SGH-C160|SGH-C170|SGH-C180|SGH-C200|SGH-C207|SGH-C210|SGH-C225|SGH-C230|SGH-C417|SGH-C450|SGH-D307|SGH-D347|SGH-D357|SGH-D407|SGH-D415|SGH-D780|SGH-D807|SGH-D980|SGH-E105|SGH-E200|SGH-E315|SGH-E316|SGH-E317|SGH-E335|SGH-E590|SGH-E635|SGH-E715|SGH-E890|SGH-F300|SGH-F480|SGH-I200|SGH-I300|SGH-I320|SGH-I550|SGH-I577|SGH-I600|SGH-I607|SGH-I617|SGH-I627|SGH-I637|SGH-I677|SGH-I700|SGH-I717|SGH-I727|SGH-i747M|SGH-I777|SGH-I780|SGH-I827|SGH-I847|SGH-I857|SGH-I896|SGH-I897|SGH-I900|SGH-I907|SGH-I917|SGH-I927|SGH-I937|SGH-I997|SGH-J150|SGH-J200|SGH-L170|SGH-L700|SGH-M110|SGH-M150|SGH-M200|SGH-N105|SGH-N500|SGH-N600|SGH-N620|SGH-N625|SGH-N700|SGH-N710|SGH-P107|SGH-P207|SGH-P300|SGH-P310|SGH-P520|SGH-P735|SGH-P777|SGH-Q105|SGH-R210|SGH-R220|SGH-R225|SGH-S105|SGH-S307|SGH-T109|SGH-T119|SGH-T139|SGH-T209|SGH-T219|SGH-T229|SGH-T239|SGH-T249|SGH-T259|SGH-T309|SGH-T319|SGH-T329|SGH-T339|SGH-T349|SGH-T359|SGH-T369|SGH-T379|SGH-T409|SGH-T429|SGH-T439|SGH-T459|SGH-T469|SGH-T479|SGH-T499|SGH-T509|SGH-T519|SGH-T539|SGH-T559|SGH-T589|SGH-T609|SGH-T619|SGH-T629|SGH-T639|SGH-T659|SGH-T669|SGH-T679|SGH-T709|SGH-T719|SGH-T729|SGH-T739|SGH-T746|SGH-T749|SGH-T759|SGH-T769|SGH-T809|SGH-T819|SGH-T839|SGH-T919|SGH-T929|SGH-T939|SGH-T959|SGH-T989|SGH-U100|SGH-U200|SGH-U800|SGH-V205|SGH-V206|SGH-X100|SGH-X105|SGH-X120|SGH-X140|SGH-X426|SGH-X427|SGH-X475|SGH-X495|SGH-X497|SGH-X507|SGH-X600|SGH-X610|SGH-X620|SGH-X630|SGH-X700|SGH-X820|SGH-X890|SGH-Z130|SGH-Z150|SGH-Z170|SGH-ZX10|SGH-ZX20|SHW-M110|SPH-A120|SPH-A400|SPH-A420|SPH-A460|SPH-A500|SPH-A560|SPH-A600|SPH-A620|SPH-A660|SPH-A700|SPH-A740|SPH-A760|SPH-A790|SPH-A800|SPH-A820|SPH-A840|SPH-A880|SPH-A900|SPH-A940|SPH-A960|SPH-D600|SPH-D700|SPH-D710|SPH-D720|SPH-I300|SPH-I325|SPH-I330|SPH-I350|SPH-I500|SPH-I600|SPH-I700|SPH-L700|SPH-M100|SPH-M220|SPH-M240|SPH-M300|SPH-M305|SPH-M320|SPH-M330|SPH-M350|SPH-M360|SPH-M370|SPH-M380|SPH-M510|SPH-M540|SPH-M550|SPH-M560|SPH-M570|SPH-M580|SPH-M610|SPH-M620|SPH-M630|SPH-M800|SPH-M810|SPH-M850|SPH-M900|SPH-M910|SPH-M920|SPH-M930|SPH-N100|SPH-N200|SPH-N240|SPH-N300|SPH-N400|SPH-Z400|SWC-E100|SCH-i909|GT-N7100|GT-N7105|SCH-I535|SM-N900A|SGH-I317|SGH-T999L|GT-S5360B|GT-I8262|GT-S6802|GT-S6312|GT-S6310|GT-S5312|GT-S5310|GT-I9105|GT-I8510|GT-S6790N|SM-G7105|SM-N9005|GT-S5301|GT-I9295|GT-I9195|SM-C101|GT-S7392|GT-S7560|GT-B7610|GT-I5510|GT-S7582|GT-S7530E|GT-I8750|SM-G9006V|SM-G9008V|SM-G9009D|SM-G900A|SM-G900D|SM-G900F|SM-G900H|SM-G900I|SM-G900J|SM-G900K|SM-G900L|SM-G900M|SM-G900P|SM-G900R4|SM-G900S|SM-G900T|SM-G900V|SM-G900W8|SHV-E160K|SCH-P709|SCH-P729|SM-T2558|GT-I9205|SM-G9350",
	                "LG": "\\bLG\\b;|LG[- ]?(C800|C900|E400|E610|E900|E-900|F160|F180K|F180L|F180S|730|855|L160|LS740|LS840|LS970|LU6200|MS690|MS695|MS770|MS840|MS870|MS910|P500|P700|P705|VM696|AS680|AS695|AX840|C729|E970|GS505|272|C395|E739BK|E960|L55C|L75C|LS696|LS860|P769BK|P350|P500|P509|P870|UN272|US730|VS840|VS950|LN272|LN510|LS670|LS855|LW690|MN270|MN510|P509|P769|P930|UN200|UN270|UN510|UN610|US670|US740|US760|UX265|UX840|VN271|VN530|VS660|VS700|VS740|VS750|VS910|VS920|VS930|VX9200|VX11000|AX840A|LW770|P506|P925|P999|E612|D955|D802|MS323)",
	                "Sony": "SonyST|SonyLT|SonyEricsson|SonyEricssonLT15iv|LT18i|E10i|LT28h|LT26w|SonyEricssonMT27i|C5303|C6902|C6903|C6906|C6943|D2533",
	                "Asus": "Asus.*Galaxy|PadFone.*Mobile",
	                "Micromax": "Micromax.*\\b(A210|A92|A88|A72|A111|A110Q|A115|A116|A110|A90S|A26|A51|A35|A54|A25|A27|A89|A68|A65|A57|A90)\\b",
	                "Palm": "PalmSource|Palm",
	                "Vertu": "Vertu|Vertu.*Ltd|Vertu.*Ascent|Vertu.*Ayxta|Vertu.*Constellation(F|Quest)?|Vertu.*Monika|Vertu.*Signature",
	                "Pantech": "PANTECH|IM-A850S|IM-A840S|IM-A830L|IM-A830K|IM-A830S|IM-A820L|IM-A810K|IM-A810S|IM-A800S|IM-T100K|IM-A725L|IM-A780L|IM-A775C|IM-A770K|IM-A760S|IM-A750K|IM-A740S|IM-A730S|IM-A720L|IM-A710K|IM-A690L|IM-A690S|IM-A650S|IM-A630K|IM-A600S|VEGA PTL21|PT003|P8010|ADR910L|P6030|P6020|P9070|P4100|P9060|P5000|CDM8992|TXT8045|ADR8995|IS11PT|P2030|P6010|P8000|PT002|IS06|CDM8999|P9050|PT001|TXT8040|P2020|P9020|P2000|P7040|P7000|C790",
	                "Fly": "IQ230|IQ444|IQ450|IQ440|IQ442|IQ441|IQ245|IQ256|IQ236|IQ255|IQ235|IQ245|IQ275|IQ240|IQ285|IQ280|IQ270|IQ260|IQ250",
	                "Wiko": "KITE 4G|HIGHWAY|GETAWAY|STAIRWAY|DARKSIDE|DARKFULL|DARKNIGHT|DARKMOON|SLIDE|WAX 4G|RAINBOW|BLOOM|SUNSET|GOA|LENNY|BARRY|IGGY|OZZY|CINK FIVE|CINK PEAX|CINK PEAX 2|CINK SLIM|CINK SLIM 2|CINK +|CINK KING|CINK PEAX|CINK SLIM|SUBLIM",
	                "iMobile": "i-mobile (IQ|i-STYLE|idea|ZAA|Hitz)",
	                "SimValley": "\\b(SP-80|XT-930|SX-340|XT-930|SX-310|SP-360|SP60|SPT-800|SP-120|SPT-800|SP-140|SPX-5|SPX-8|SP-100|SPX-8|SPX-12)\\b",
	                "Wolfgang": "AT-B24D|AT-AS50HD|AT-AS40W|AT-AS55HD|AT-AS45q2|AT-B26D|AT-AS50Q",
	                "Alcatel": "Alcatel",
	                "Nintendo": "Nintendo 3DS",
	                "Amoi": "Amoi",
	                "INQ": "INQ",
	                "GenericPhone": "Tapatalk|PDA;|SAGEM|\\bmmp\\b|pocket|\\bpsp\\b|symbian|Smartphone|smartfon|treo|up.browser|up.link|vodafone|\\bwap\\b|nokia|Series40|Series60|S60|SonyEricsson|N900|MAUI.*WAP.*Browser"
	            },
	            "tablets": {
	                "iPad": "iPad|iPad.*Mobile",
	                "NexusTablet": "Android.*Nexus[\\s]+(7|9|10)",
	                "SamsungTablet": "SAMSUNG.*Tablet|Galaxy.*Tab|SC-01C|GT-P1000|GT-P1003|GT-P1010|GT-P3105|GT-P6210|GT-P6800|GT-P6810|GT-P7100|GT-P7300|GT-P7310|GT-P7500|GT-P7510|SCH-I800|SCH-I815|SCH-I905|SGH-I957|SGH-I987|SGH-T849|SGH-T859|SGH-T869|SPH-P100|GT-P3100|GT-P3108|GT-P3110|GT-P5100|GT-P5110|GT-P6200|GT-P7320|GT-P7511|GT-N8000|GT-P8510|SGH-I497|SPH-P500|SGH-T779|SCH-I705|SCH-I915|GT-N8013|GT-P3113|GT-P5113|GT-P8110|GT-N8010|GT-N8005|GT-N8020|GT-P1013|GT-P6201|GT-P7501|GT-N5100|GT-N5105|GT-N5110|SHV-E140K|SHV-E140L|SHV-E140S|SHV-E150S|SHV-E230K|SHV-E230L|SHV-E230S|SHW-M180K|SHW-M180L|SHW-M180S|SHW-M180W|SHW-M300W|SHW-M305W|SHW-M380K|SHW-M380S|SHW-M380W|SHW-M430W|SHW-M480K|SHW-M480S|SHW-M480W|SHW-M485W|SHW-M486W|SHW-M500W|GT-I9228|SCH-P739|SCH-I925|GT-I9200|GT-P5200|GT-P5210|GT-P5210X|SM-T311|SM-T310|SM-T310X|SM-T210|SM-T210R|SM-T211|SM-P600|SM-P601|SM-P605|SM-P900|SM-P901|SM-T217|SM-T217A|SM-T217S|SM-P6000|SM-T3100|SGH-I467|XE500|SM-T110|GT-P5220|GT-I9200X|GT-N5110X|GT-N5120|SM-P905|SM-T111|SM-T2105|SM-T315|SM-T320|SM-T320X|SM-T321|SM-T520|SM-T525|SM-T530NU|SM-T230NU|SM-T330NU|SM-T900|XE500T1C|SM-P605V|SM-P905V|SM-T337V|SM-T537V|SM-T707V|SM-T807V|SM-P600X|SM-P900X|SM-T210X|SM-T230|SM-T230X|SM-T325|GT-P7503|SM-T531|SM-T330|SM-T530|SM-T705|SM-T705C|SM-T535|SM-T331|SM-T800|SM-T700|SM-T537|SM-T807|SM-P907A|SM-T337A|SM-T537A|SM-T707A|SM-T807A|SM-T237|SM-T807P|SM-P607T|SM-T217T|SM-T337T|SM-T807T|SM-T116NQ|SM-P550|SM-T350|SM-T550|SM-T9000|SM-P9000|SM-T705Y|SM-T805|GT-P3113|SM-T710|SM-T810|SM-T815|SM-T360|SM-T533|SM-T113|SM-T335|SM-T715|SM-T560|SM-T670|SM-T677|SM-T377|SM-T567|SM-T357T|SM-T555|SM-T561",
	                "Kindle": "Kindle|Silk.*Accelerated|Android.*\\b(KFOT|KFTT|KFJWI|KFJWA|KFOTE|KFSOWI|KFTHWI|KFTHWA|KFAPWI|KFAPWA|WFJWAE|KFSAWA|KFSAWI|KFASWI)\\b",
	                "SurfaceTablet": "Windows NT [0-9.]+; ARM;.*(Tablet|ARMBJS)",
	                "HPTablet": "HP Slate (7|8|10)|HP ElitePad 900|hp-tablet|EliteBook.*Touch|HP 8|Slate 21|HP SlateBook 10",
	                "AsusTablet": "^.*PadFone((?!Mobile).)*$|Transformer|TF101|TF101G|TF300T|TF300TG|TF300TL|TF700T|TF700KL|TF701T|TF810C|ME171|ME301T|ME302C|ME371MG|ME370T|ME372MG|ME172V|ME173X|ME400C|Slider SL101|\\bK00F\\b|\\bK00C\\b|\\bK00E\\b|\\bK00L\\b|TX201LA|ME176C|ME102A|\\bM80TA\\b|ME372CL|ME560CG|ME372CG|ME302KL| K010 | K017 |ME572C|ME103K|ME170C|ME171C|\\bME70C\\b|ME581C|ME581CL|ME8510C|ME181C",
	                "BlackBerryTablet": "PlayBook|RIM Tablet",
	                "HTCtablet": "HTC_Flyer_P512|HTC Flyer|HTC Jetstream|HTC-P715a|HTC EVO View 4G|PG41200|PG09410",
	                "MotorolaTablet": "xoom|sholest|MZ615|MZ605|MZ505|MZ601|MZ602|MZ603|MZ604|MZ606|MZ607|MZ608|MZ609|MZ615|MZ616|MZ617",
	                "NookTablet": "Android.*Nook|NookColor|nook browser|BNRV200|BNRV200A|BNTV250|BNTV250A|BNTV400|BNTV600|LogicPD Zoom2",
	                "AcerTablet": "Android.*; \\b(A100|A101|A110|A200|A210|A211|A500|A501|A510|A511|A700|A701|W500|W500P|W501|W501P|W510|W511|W700|G100|G100W|B1-A71|B1-710|B1-711|A1-810|A1-811|A1-830)\\b|W3-810|\\bA3-A10\\b|\\bA3-A11\\b",
	                "ToshibaTablet": "Android.*(AT100|AT105|AT200|AT205|AT270|AT275|AT300|AT305|AT1S5|AT500|AT570|AT700|AT830)|TOSHIBA.*FOLIO",
	                "LGTablet": "\\bL-06C|LG-V909|LG-V900|LG-V700|LG-V510|LG-V500|LG-V410|LG-V400|LG-VK810\\b",
	                "FujitsuTablet": "Android.*\\b(F-01D|F-02F|F-05E|F-10D|M532|Q572)\\b",
	                "PrestigioTablet": "PMP3170B|PMP3270B|PMP3470B|PMP7170B|PMP3370B|PMP3570C|PMP5870C|PMP3670B|PMP5570C|PMP5770D|PMP3970B|PMP3870C|PMP5580C|PMP5880D|PMP5780D|PMP5588C|PMP7280C|PMP7280C3G|PMP7280|PMP7880D|PMP5597D|PMP5597|PMP7100D|PER3464|PER3274|PER3574|PER3884|PER5274|PER5474|PMP5097CPRO|PMP5097|PMP7380D|PMP5297C|PMP5297C_QUAD|PMP812E|PMP812E3G|PMP812F|PMP810E|PMP880TD|PMT3017|PMT3037|PMT3047|PMT3057|PMT7008|PMT5887|PMT5001|PMT5002",
	                "LenovoTablet": "Lenovo TAB|Idea(Tab|Pad)( A1|A10| K1|)|ThinkPad([ ]+)?Tablet|Lenovo.*(S2109|S2110|S5000|S6000|K3011|A3000|A3500|A1000|A2107|A2109|A1107|A5500|A7600|B6000|B8000|B8080)(-|)(FL|F|HV|H|)",
	                "DellTablet": "Venue 11|Venue 8|Venue 7|Dell Streak 10|Dell Streak 7",
	                "YarvikTablet": "Android.*\\b(TAB210|TAB211|TAB224|TAB250|TAB260|TAB264|TAB310|TAB360|TAB364|TAB410|TAB411|TAB420|TAB424|TAB450|TAB460|TAB461|TAB464|TAB465|TAB467|TAB468|TAB07-100|TAB07-101|TAB07-150|TAB07-151|TAB07-152|TAB07-200|TAB07-201-3G|TAB07-210|TAB07-211|TAB07-212|TAB07-214|TAB07-220|TAB07-400|TAB07-485|TAB08-150|TAB08-200|TAB08-201-3G|TAB08-201-30|TAB09-100|TAB09-211|TAB09-410|TAB10-150|TAB10-201|TAB10-211|TAB10-400|TAB10-410|TAB13-201|TAB274EUK|TAB275EUK|TAB374EUK|TAB462EUK|TAB474EUK|TAB9-200)\\b",
	                "MedionTablet": "Android.*\\bOYO\\b|LIFE.*(P9212|P9514|P9516|S9512)|LIFETAB",
	                "ArnovaTablet": "AN10G2|AN7bG3|AN7fG3|AN8G3|AN8cG3|AN7G3|AN9G3|AN7dG3|AN7dG3ST|AN7dG3ChildPad|AN10bG3|AN10bG3DT|AN9G2",
	                "IntensoTablet": "INM8002KP|INM1010FP|INM805ND|Intenso Tab|TAB1004",
	                "IRUTablet": "M702pro",
	                "MegafonTablet": "MegaFon V9|\\bZTE V9\\b|Android.*\\bMT7A\\b",
	                "EbodaTablet": "E-Boda (Supreme|Impresspeed|Izzycomm|Essential)",
	                "AllViewTablet": "Allview.*(Viva|Alldro|City|Speed|All TV|Frenzy|Quasar|Shine|TX1|AX1|AX2)",
	                "ArchosTablet": "\\b(101G9|80G9|A101IT)\\b|Qilive 97R|Archos5|\\bARCHOS (70|79|80|90|97|101|FAMILYPAD|)(b|)(G10| Cobalt| TITANIUM(HD|)| Xenon| Neon|XSK| 2| XS 2| PLATINUM| CARBON|GAMEPAD)\\b",
	                "AinolTablet": "NOVO7|NOVO8|NOVO10|Novo7Aurora|Novo7Basic|NOVO7PALADIN|novo9-Spark",
	                "SonyTablet": "Sony.*Tablet|Xperia Tablet|Sony Tablet S|SO-03E|SGPT12|SGPT13|SGPT114|SGPT121|SGPT122|SGPT123|SGPT111|SGPT112|SGPT113|SGPT131|SGPT132|SGPT133|SGPT211|SGPT212|SGPT213|SGP311|SGP312|SGP321|EBRD1101|EBRD1102|EBRD1201|SGP351|SGP341|SGP511|SGP512|SGP521|SGP541|SGP551|SGP621|SGP612|SOT31",
	                "PhilipsTablet": "\\b(PI2010|PI3000|PI3100|PI3105|PI3110|PI3205|PI3210|PI3900|PI4010|PI7000|PI7100)\\b",
	                "CubeTablet": "Android.*(K8GT|U9GT|U10GT|U16GT|U17GT|U18GT|U19GT|U20GT|U23GT|U30GT)|CUBE U8GT",
	                "CobyTablet": "MID1042|MID1045|MID1125|MID1126|MID7012|MID7014|MID7015|MID7034|MID7035|MID7036|MID7042|MID7048|MID7127|MID8042|MID8048|MID8127|MID9042|MID9740|MID9742|MID7022|MID7010",
	                "MIDTablet": "M9701|M9000|M9100|M806|M1052|M806|T703|MID701|MID713|MID710|MID727|MID760|MID830|MID728|MID933|MID125|MID810|MID732|MID120|MID930|MID800|MID731|MID900|MID100|MID820|MID735|MID980|MID130|MID833|MID737|MID960|MID135|MID860|MID736|MID140|MID930|MID835|MID733|MID4X10",
	                "MSITablet": "MSI \\b(Primo 73K|Primo 73L|Primo 81L|Primo 77|Primo 93|Primo 75|Primo 76|Primo 73|Primo 81|Primo 91|Primo 90|Enjoy 71|Enjoy 7|Enjoy 10)\\b",
	                "SMiTTablet": "Android.*(\\bMID\\b|MID-560|MTV-T1200|MTV-PND531|MTV-P1101|MTV-PND530)",
	                "RockChipTablet": "Android.*(RK2818|RK2808A|RK2918|RK3066)|RK2738|RK2808A",
	                "FlyTablet": "IQ310|Fly Vision",
	                "bqTablet": "Android.*(bq)?.*(Elcano|Curie|Edison|Maxwell|Kepler|Pascal|Tesla|Hypatia|Platon|Newton|Livingstone|Cervantes|Avant|Aquaris E10)|Maxwell.*Lite|Maxwell.*Plus",
	                "HuaweiTablet": "MediaPad|MediaPad 7 Youth|IDEOS S7|S7-201c|S7-202u|S7-101|S7-103|S7-104|S7-105|S7-106|S7-201|S7-Slim",
	                "NecTablet": "\\bN-06D|\\bN-08D",
	                "PantechTablet": "Pantech.*P4100",
	                "BronchoTablet": "Broncho.*(N701|N708|N802|a710)",
	                "VersusTablet": "TOUCHPAD.*[78910]|\\bTOUCHTAB\\b",
	                "ZyncTablet": "z1000|Z99 2G|z99|z930|z999|z990|z909|Z919|z900",
	                "PositivoTablet": "TB07STA|TB10STA|TB07FTA|TB10FTA",
	                "NabiTablet": "Android.*\\bNabi",
	                "KoboTablet": "Kobo Touch|\\bK080\\b|\\bVox\\b Build|\\bArc\\b Build",
	                "DanewTablet": "DSlide.*\\b(700|701R|702|703R|704|802|970|971|972|973|974|1010|1012)\\b",
	                "TexetTablet": "NaviPad|TB-772A|TM-7045|TM-7055|TM-9750|TM-7016|TM-7024|TM-7026|TM-7041|TM-7043|TM-7047|TM-8041|TM-9741|TM-9747|TM-9748|TM-9751|TM-7022|TM-7021|TM-7020|TM-7011|TM-7010|TM-7023|TM-7025|TM-7037W|TM-7038W|TM-7027W|TM-9720|TM-9725|TM-9737W|TM-1020|TM-9738W|TM-9740|TM-9743W|TB-807A|TB-771A|TB-727A|TB-725A|TB-719A|TB-823A|TB-805A|TB-723A|TB-715A|TB-707A|TB-705A|TB-709A|TB-711A|TB-890HD|TB-880HD|TB-790HD|TB-780HD|TB-770HD|TB-721HD|TB-710HD|TB-434HD|TB-860HD|TB-840HD|TB-760HD|TB-750HD|TB-740HD|TB-730HD|TB-722HD|TB-720HD|TB-700HD|TB-500HD|TB-470HD|TB-431HD|TB-430HD|TB-506|TB-504|TB-446|TB-436|TB-416|TB-146SE|TB-126SE",
	                "PlaystationTablet": "Playstation.*(Portable|Vita)",
	                "TrekstorTablet": "ST10416-1|VT10416-1|ST70408-1|ST702xx-1|ST702xx-2|ST80208|ST97216|ST70104-2|VT10416-2|ST10216-2A|SurfTab",
	                "PyleAudioTablet": "\\b(PTBL10CEU|PTBL10C|PTBL72BC|PTBL72BCEU|PTBL7CEU|PTBL7C|PTBL92BC|PTBL92BCEU|PTBL9CEU|PTBL9CUK|PTBL9C)\\b",
	                "AdvanTablet": "Android.* \\b(E3A|T3X|T5C|T5B|T3E|T3C|T3B|T1J|T1F|T2A|T1H|T1i|E1C|T1-E|T5-A|T4|E1-B|T2Ci|T1-B|T1-D|O1-A|E1-A|T1-A|T3A|T4i)\\b ",
	                "DanyTechTablet": "Genius Tab G3|Genius Tab S2|Genius Tab Q3|Genius Tab G4|Genius Tab Q4|Genius Tab G-II|Genius TAB GII|Genius TAB GIII|Genius Tab S1",
	                "GalapadTablet": "Android.*\\bG1\\b",
	                "MicromaxTablet": "Funbook|Micromax.*\\b(P250|P560|P360|P362|P600|P300|P350|P500|P275)\\b",
	                "KarbonnTablet": "Android.*\\b(A39|A37|A34|ST8|ST10|ST7|Smart Tab3|Smart Tab2)\\b",
	                "AllFineTablet": "Fine7 Genius|Fine7 Shine|Fine7 Air|Fine8 Style|Fine9 More|Fine10 Joy|Fine11 Wide",
	                "PROSCANTablet": "\\b(PEM63|PLT1023G|PLT1041|PLT1044|PLT1044G|PLT1091|PLT4311|PLT4311PL|PLT4315|PLT7030|PLT7033|PLT7033D|PLT7035|PLT7035D|PLT7044K|PLT7045K|PLT7045KB|PLT7071KG|PLT7072|PLT7223G|PLT7225G|PLT7777G|PLT7810K|PLT7849G|PLT7851G|PLT7852G|PLT8015|PLT8031|PLT8034|PLT8036|PLT8080K|PLT8082|PLT8088|PLT8223G|PLT8234G|PLT8235G|PLT8816K|PLT9011|PLT9045K|PLT9233G|PLT9735|PLT9760G|PLT9770G)\\b",
	                "YONESTablet": "BQ1078|BC1003|BC1077|RK9702|BC9730|BC9001|IT9001|BC7008|BC7010|BC708|BC728|BC7012|BC7030|BC7027|BC7026",
	                "ChangJiaTablet": "TPC7102|TPC7103|TPC7105|TPC7106|TPC7107|TPC7201|TPC7203|TPC7205|TPC7210|TPC7708|TPC7709|TPC7712|TPC7110|TPC8101|TPC8103|TPC8105|TPC8106|TPC8203|TPC8205|TPC8503|TPC9106|TPC9701|TPC97101|TPC97103|TPC97105|TPC97106|TPC97111|TPC97113|TPC97203|TPC97603|TPC97809|TPC97205|TPC10101|TPC10103|TPC10106|TPC10111|TPC10203|TPC10205|TPC10503",
	                "GUTablet": "TX-A1301|TX-M9002|Q702|kf026",
	                "PointOfViewTablet": "TAB-P506|TAB-navi-7-3G-M|TAB-P517|TAB-P-527|TAB-P701|TAB-P703|TAB-P721|TAB-P731N|TAB-P741|TAB-P825|TAB-P905|TAB-P925|TAB-PR945|TAB-PL1015|TAB-P1025|TAB-PI1045|TAB-P1325|TAB-PROTAB[0-9]+|TAB-PROTAB25|TAB-PROTAB26|TAB-PROTAB27|TAB-PROTAB26XL|TAB-PROTAB2-IPS9|TAB-PROTAB30-IPS9|TAB-PROTAB25XXL|TAB-PROTAB26-IPS10|TAB-PROTAB30-IPS10",
	                "OvermaxTablet": "OV-(SteelCore|NewBase|Basecore|Baseone|Exellen|Quattor|EduTab|Solution|ACTION|BasicTab|TeddyTab|MagicTab|Stream|TB-08|TB-09)",
	                "HCLTablet": "HCL.*Tablet|Connect-3G-2.0|Connect-2G-2.0|ME Tablet U1|ME Tablet U2|ME Tablet G1|ME Tablet X1|ME Tablet Y2|ME Tablet Sync",
	                "DPSTablet": "DPS Dream 9|DPS Dual 7",
	                "VistureTablet": "V97 HD|i75 3G|Visture V4( HD)?|Visture V5( HD)?|Visture V10",
	                "CrestaTablet": "CTP(-)?810|CTP(-)?818|CTP(-)?828|CTP(-)?838|CTP(-)?888|CTP(-)?978|CTP(-)?980|CTP(-)?987|CTP(-)?988|CTP(-)?989",
	                "MediatekTablet": "\\bMT8125|MT8389|MT8135|MT8377\\b",
	                "ConcordeTablet": "Concorde([ ]+)?Tab|ConCorde ReadMan",
	                "GoCleverTablet": "GOCLEVER TAB|A7GOCLEVER|M1042|M7841|M742|R1042BK|R1041|TAB A975|TAB A7842|TAB A741|TAB A741L|TAB M723G|TAB M721|TAB A1021|TAB I921|TAB R721|TAB I720|TAB T76|TAB R70|TAB R76.2|TAB R106|TAB R83.2|TAB M813G|TAB I721|GCTA722|TAB I70|TAB I71|TAB S73|TAB R73|TAB R74|TAB R93|TAB R75|TAB R76.1|TAB A73|TAB A93|TAB A93.2|TAB T72|TAB R83|TAB R974|TAB R973|TAB A101|TAB A103|TAB A104|TAB A104.2|R105BK|M713G|A972BK|TAB A971|TAB R974.2|TAB R104|TAB R83.3|TAB A1042",
	                "ModecomTablet": "FreeTAB 9000|FreeTAB 7.4|FreeTAB 7004|FreeTAB 7800|FreeTAB 2096|FreeTAB 7.5|FreeTAB 1014|FreeTAB 1001 |FreeTAB 8001|FreeTAB 9706|FreeTAB 9702|FreeTAB 7003|FreeTAB 7002|FreeTAB 1002|FreeTAB 7801|FreeTAB 1331|FreeTAB 1004|FreeTAB 8002|FreeTAB 8014|FreeTAB 9704|FreeTAB 1003",
	                "VoninoTablet": "\\b(Argus[ _]?S|Diamond[ _]?79HD|Emerald[ _]?78E|Luna[ _]?70C|Onyx[ _]?S|Onyx[ _]?Z|Orin[ _]?HD|Orin[ _]?S|Otis[ _]?S|SpeedStar[ _]?S|Magnet[ _]?M9|Primus[ _]?94[ _]?3G|Primus[ _]?94HD|Primus[ _]?QS|Android.*\\bQ8\\b|Sirius[ _]?EVO[ _]?QS|Sirius[ _]?QS|Spirit[ _]?S)\\b",
	                "ECSTablet": "V07OT2|TM105A|S10OT1|TR10CS1",
	                "StorexTablet": "eZee[_']?(Tab|Go)[0-9]+|TabLC7|Looney Tunes Tab",
	                "VodafoneTablet": "SmartTab([ ]+)?[0-9]+|SmartTabII10|SmartTabII7",
	                "EssentielBTablet": "Smart[ ']?TAB[ ]+?[0-9]+|Family[ ']?TAB2",
	                "RossMoorTablet": "RM-790|RM-997|RMD-878G|RMD-974R|RMT-705A|RMT-701|RME-601|RMT-501|RMT-711",
	                "iMobileTablet": "i-mobile i-note",
	                "TolinoTablet": "tolino tab [0-9.]+|tolino shine",
	                "AudioSonicTablet": "\\bC-22Q|T7-QC|T-17B|T-17P\\b",
	                "AMPETablet": "Android.* A78 ",
	                "SkkTablet": "Android.* (SKYPAD|PHOENIX|CYCLOPS)",
	                "TecnoTablet": "TECNO P9",
	                "JXDTablet": "Android.* \\b(F3000|A3300|JXD5000|JXD3000|JXD2000|JXD300B|JXD300|S5800|S7800|S602b|S5110b|S7300|S5300|S602|S603|S5100|S5110|S601|S7100a|P3000F|P3000s|P101|P200s|P1000m|P200m|P9100|P1000s|S6600b|S908|P1000|P300|S18|S6600|S9100)\\b",
	                "iJoyTablet": "Tablet (Spirit 7|Essentia|Galatea|Fusion|Onix 7|Landa|Titan|Scooby|Deox|Stella|Themis|Argon|Unique 7|Sygnus|Hexen|Finity 7|Cream|Cream X2|Jade|Neon 7|Neron 7|Kandy|Scape|Saphyr 7|Rebel|Biox|Rebel|Rebel 8GB|Myst|Draco 7|Myst|Tab7-004|Myst|Tadeo Jones|Tablet Boing|Arrow|Draco Dual Cam|Aurix|Mint|Amity|Revolution|Finity 9|Neon 9|T9w|Amity 4GB Dual Cam|Stone 4GB|Stone 8GB|Andromeda|Silken|X2|Andromeda II|Halley|Flame|Saphyr 9,7|Touch 8|Planet|Triton|Unique 10|Hexen 10|Memphis 4GB|Memphis 8GB|Onix 10)",
	                "FX2Tablet": "FX2 PAD7|FX2 PAD10",
	                "XoroTablet": "KidsPAD 701|PAD[ ]?712|PAD[ ]?714|PAD[ ]?716|PAD[ ]?717|PAD[ ]?718|PAD[ ]?720|PAD[ ]?721|PAD[ ]?722|PAD[ ]?790|PAD[ ]?792|PAD[ ]?900|PAD[ ]?9715D|PAD[ ]?9716DR|PAD[ ]?9718DR|PAD[ ]?9719QR|PAD[ ]?9720QR|TelePAD1030|Telepad1032|TelePAD730|TelePAD731|TelePAD732|TelePAD735Q|TelePAD830|TelePAD9730|TelePAD795|MegaPAD 1331|MegaPAD 1851|MegaPAD 2151",
	                "ViewsonicTablet": "ViewPad 10pi|ViewPad 10e|ViewPad 10s|ViewPad E72|ViewPad7|ViewPad E100|ViewPad 7e|ViewSonic VB733|VB100a",
	                "OdysTablet": "LOOX|XENO10|ODYS[ -](Space|EVO|Xpress|NOON)|\\bXELIO\\b|Xelio10Pro|XELIO7PHONETAB|XELIO10EXTREME|XELIOPT2|NEO_QUAD10",
	                "CaptivaTablet": "CAPTIVA PAD",
	                "IconbitTablet": "NetTAB|NT-3702|NT-3702S|NT-3702S|NT-3603P|NT-3603P|NT-0704S|NT-0704S|NT-3805C|NT-3805C|NT-0806C|NT-0806C|NT-0909T|NT-0909T|NT-0907S|NT-0907S|NT-0902S|NT-0902S",
	                "TeclastTablet": "T98 4G|\\bP80\\b|\\bX90HD\\b|X98 Air|X98 Air 3G|\\bX89\\b|P80 3G|\\bX80h\\b|P98 Air|\\bX89HD\\b|P98 3G|\\bP90HD\\b|P89 3G|X98 3G|\\bP70h\\b|P79HD 3G|G18d 3G|\\bP79HD\\b|\\bP89s\\b|\\bA88\\b|\\bP10HD\\b|\\bP19HD\\b|G18 3G|\\bP78HD\\b|\\bA78\\b|\\bP75\\b|G17s 3G|G17h 3G|\\bP85t\\b|\\bP90\\b|\\bP11\\b|\\bP98t\\b|\\bP98HD\\b|\\bG18d\\b|\\bP85s\\b|\\bP11HD\\b|\\bP88s\\b|\\bA80HD\\b|\\bA80se\\b|\\bA10h\\b|\\bP89\\b|\\bP78s\\b|\\bG18\\b|\\bP85\\b|\\bA70h\\b|\\bA70\\b|\\bG17\\b|\\bP18\\b|\\bA80s\\b|\\bA11s\\b|\\bP88HD\\b|\\bA80h\\b|\\bP76s\\b|\\bP76h\\b|\\bP98\\b|\\bA10HD\\b|\\bP78\\b|\\bP88\\b|\\bA11\\b|\\bA10t\\b|\\bP76a\\b|\\bP76t\\b|\\bP76e\\b|\\bP85HD\\b|\\bP85a\\b|\\bP86\\b|\\bP75HD\\b|\\bP76v\\b|\\bA12\\b|\\bP75a\\b|\\bA15\\b|\\bP76Ti\\b|\\bP81HD\\b|\\bA10\\b|\\bT760VE\\b|\\bT720HD\\b|\\bP76\\b|\\bP73\\b|\\bP71\\b|\\bP72\\b|\\bT720SE\\b|\\bC520Ti\\b|\\bT760\\b|\\bT720VE\\b|T720-3GE|T720-WiFi",
	                "OndaTablet": "\\b(V975i|Vi30|VX530|V701|Vi60|V701s|Vi50|V801s|V719|Vx610w|VX610W|V819i|Vi10|VX580W|Vi10|V711s|V813|V811|V820w|V820|Vi20|V711|VI30W|V712|V891w|V972|V819w|V820w|Vi60|V820w|V711|V813s|V801|V819|V975s|V801|V819|V819|V818|V811|V712|V975m|V101w|V961w|V812|V818|V971|V971s|V919|V989|V116w|V102w|V973|Vi40)\\b[\\s]+",
	                "JaytechTablet": "TPC-PA762",
	                "BlaupunktTablet": "Endeavour 800NG|Endeavour 1010",
	                "DigmaTablet": "\\b(iDx10|iDx9|iDx8|iDx7|iDxD7|iDxD8|iDsQ8|iDsQ7|iDsQ8|iDsD10|iDnD7|3TS804H|iDsQ11|iDj7|iDs10)\\b",
	                "EvolioTablet": "ARIA_Mini_wifi|Aria[ _]Mini|Evolio X10|Evolio X7|Evolio X8|\\bEvotab\\b|\\bNeura\\b",
	                "LavaTablet": "QPAD E704|\\bIvoryS\\b|E-TAB IVORY|\\bE-TAB\\b",
	                "AocTablet": "MW0811|MW0812|MW0922|MTK8382",
	                "MpmanTablet": "MP11 OCTA|MP10 OCTA|MPQC1114|MPQC1004|MPQC994|MPQC974|MPQC973|MPQC804|MPQC784|MPQC780|\\bMPG7\\b|MPDCG75|MPDCG71|MPDC1006|MP101DC|MPDC9000|MPDC905|MPDC706HD|MPDC706|MPDC705|MPDC110|MPDC100|MPDC99|MPDC97|MPDC88|MPDC8|MPDC77|MP709|MID701|MID711|MID170|MPDC703|MPQC1010",
	                "CelkonTablet": "CT695|CT888|CT[\\s]?910|CT7 Tab|CT9 Tab|CT3 Tab|CT2 Tab|CT1 Tab|C820|C720|\\bCT-1\\b",
	                "WolderTablet": "miTab \\b(DIAMOND|SPACE|BROOKLYN|NEO|FLY|MANHATTAN|FUNK|EVOLUTION|SKY|GOCAR|IRON|GENIUS|POP|MINT|EPSILON|BROADWAY|JUMP|HOP|LEGEND|NEW AGE|LINE|ADVANCE|FEEL|FOLLOW|LIKE|LINK|LIVE|THINK|FREEDOM|CHICAGO|CLEVELAND|BALTIMORE-GH|IOWA|BOSTON|SEATTLE|PHOENIX|DALLAS|IN 101|MasterChef)\\b",
	                "MiTablet": "\\bMI PAD\\b|\\bHM NOTE 1W\\b",
	                "NibiruTablet": "Nibiru M1|Nibiru Jupiter One",
	                "NexoTablet": "NEXO NOVA|NEXO 10|NEXO AVIO|NEXO FREE|NEXO GO|NEXO EVO|NEXO 3G|NEXO SMART|NEXO KIDDO|NEXO MOBI",
	                "LeaderTablet": "TBLT10Q|TBLT10I|TBL-10WDKB|TBL-10WDKBO2013|TBL-W230V2|TBL-W450|TBL-W500|SV572|TBLT7I|TBA-AC7-8G|TBLT79|TBL-8W16|TBL-10W32|TBL-10WKB|TBL-W100",
	                "UbislateTablet": "UbiSlate[\\s]?7C",
	                "PocketBookTablet": "Pocketbook",
	                "Hudl": "Hudl HT7S3|Hudl 2",
	                "TelstraTablet": "T-Hub2",
	                "GenericTablet": "Android.*\\b97D\\b|Tablet(?!.*PC)|BNTV250A|MID-WCDMA|LogicPD Zoom2|\\bA7EB\\b|CatNova8|A1_07|CT704|CT1002|\\bM721\\b|rk30sdk|\\bEVOTAB\\b|M758A|ET904|ALUMIUM10|Smartfren Tab|Endeavour 1010|Tablet-PC-4|Tagi Tab|\\bM6pro\\b|CT1020W|arc 10HD|\\bJolla\\b|\\bTP750\\b"
	            },
	            "oss": {
	                "AndroidOS": "Android",
	                "BlackBerryOS": "blackberry|\\bBB10\\b|rim tablet os",
	                "PalmOS": "PalmOS|avantgo|blazer|elaine|hiptop|palm|plucker|xiino",
	                "SymbianOS": "Symbian|SymbOS|Series60|Series40|SYB-[0-9]+|\\bS60\\b",
	                "WindowsMobileOS": "Windows CE.*(PPC|Smartphone|Mobile|[0-9]{3}x[0-9]{3})|Window Mobile|Windows Phone [0-9.]+|WCE;",
	                "WindowsPhoneOS": "Windows Phone 10.0|Windows Phone 8.1|Windows Phone 8.0|Windows Phone OS|XBLWP7|ZuneWP7|Windows NT 6.[23]; ARM;",
	                "iOS": "\\biPhone.*Mobile|\\biPod|\\biPad",
	                "MeeGoOS": "MeeGo",
	                "MaemoOS": "Maemo",
	                "JavaOS": "J2ME\/|\\bMIDP\\b|\\bCLDC\\b",
	                "webOS": "webOS|hpwOS",
	                "badaOS": "\\bBada\\b",
	                "BREWOS": "BREW"
	            },
	            "uas": {
	                "Chrome": "\\bCrMo\\b|CriOS|Android.*Chrome\/[.0-9]* (Mobile)?",
	                "Dolfin": "\\bDolfin\\b",
	                "Opera": "Opera.*Mini|Opera.*Mobi|Android.*Opera|Mobile.*OPR\/[0-9.]+|Coast\/[0-9.]+",
	                "Skyfire": "Skyfire",
	                "IE": "IEMobile|MSIEMobile",
	                "Firefox": "fennec|firefox.*maemo|(Mobile|Tablet).*Firefox|Firefox.*Mobile",
	                "Bolt": "bolt",
	                "TeaShark": "teashark",
	                "Blazer": "Blazer",
	                "Safari": "Version.*Mobile.*Safari|Safari.*Mobile|MobileSafari",
	                "Tizen": "Tizen",
	                "UCBrowser": "UC.*Browser|UCWEB",
	                "baiduboxapp": "baiduboxapp",
	                "baidubrowser": "baidubrowser",
	                "DiigoBrowser": "DiigoBrowser",
	                "Puffin": "Puffin",
	                "Mercury": "\\bMercury\\b",
	                "ObigoBrowser": "Obigo",
	                "NetFront": "NF-Browser",
	                "GenericBrowser": "NokiaBrowser|OviBrowser|OneBrowser|TwonkyBeamBrowser|SEMC.*Browser|FlyFlow|Minimo|NetFront|Novarra-Vision|MQQBrowser|MicroMessenger"
	            },
	            "props": {
	                "Mobile": "Mobile\/[VER]",
	                "Build": "Build\/[VER]",
	                "Version": "Version\/[VER]",
	                "VendorID": "VendorID\/[VER]",
	                "iPad": "iPad.*CPU[a-z ]+[VER]",
	                "iPhone": "iPhone.*CPU[a-z ]+[VER]",
	                "iPod": "iPod.*CPU[a-z ]+[VER]",
	                "Kindle": "Kindle\/[VER]",
	                "Chrome": ["Chrome\/[VER]", "CriOS\/[VER]", "CrMo\/[VER]"],
	                "Coast": ["Coast\/[VER]"],
	                "Dolfin": "Dolfin\/[VER]",
	                "Firefox": "Firefox\/[VER]",
	                "Fennec": "Fennec\/[VER]",
	                "IE": ["IEMobile\/[VER];", "IEMobile [VER]", "MSIE [VER];", "Trident\/[0-9.]+;.*rv:[VER]"],
	                "NetFront": "NetFront\/[VER]",
	                "NokiaBrowser": "NokiaBrowser\/[VER]",
	                "Opera": [" OPR\/[VER]", "Opera Mini\/[VER]", "Version\/[VER]"],
	                "Opera Mini": "Opera Mini\/[VER]",
	                "Opera Mobi": "Version\/[VER]",
	                "UC Browser": "UC Browser[VER]",
	                "MQQBrowser": "MQQBrowser\/[VER]",
	                "MicroMessenger": "MicroMessenger\/[VER]",
	                "baiduboxapp": "baiduboxapp\/[VER]",
	                "baidubrowser": "baidubrowser\/[VER]",
	                "Iron": "Iron\/[VER]",
	                "Safari": ["Version\/[VER]", "Safari\/[VER]"],
	                "Skyfire": "Skyfire\/[VER]",
	                "Tizen": "Tizen\/[VER]",
	                "Webkit": "webkit[ \/][VER]",
	                "Gecko": "Gecko\/[VER]",
	                "Trident": "Trident\/[VER]",
	                "Presto": "Presto\/[VER]",
	                "iOS": " \\bi?OS\\b [VER][ ;]{1}",
	                "Android": "Android [VER]",
	                "BlackBerry": ["BlackBerry[\\w]+\/[VER]", "BlackBerry.*Version\/[VER]", "Version\/[VER]"],
	                "BREW": "BREW [VER]",
	                "Java": "Java\/[VER]",
	                "Windows Phone OS": ["Windows Phone OS [VER]", "Windows Phone [VER]"],
	                "Windows Phone": "Windows Phone [VER]",
	                "Windows CE": "Windows CE\/[VER]",
	                "Windows NT": "Windows NT [VER]",
	                "Symbian": ["SymbianOS\/[VER]", "Symbian\/[VER]"],
	                "webOS": ["webOS\/[VER]", "hpwOS\/[VER];"]
	            },
	            "utils": {
	                "Bot": "Googlebot|facebookexternalhit|AdsBot-Google|Google Keyword Suggestion|Facebot|YandexBot|bingbot|ia_archiver|AhrefsBot|Ezooms|GSLFbot|WBSearchBot|Twitterbot|TweetmemeBot|Twikle|PaperLiBot|Wotbox|UnwindFetchor|Exabot|MJ12bot|YandexImages|TurnitinBot|Pingdom",
	                "MobileBot": "Googlebot-Mobile|AdsBot-Google-Mobile|YahooSeeker\/M1A1-R2D2",
	                "DesktopMode": "WPDesktop",
	                "TV": "SonyDTV|HbbTV",
	                "WebKit": "(webkit)[ \/]([\\w.]+)",
	                "Console": "\\b(Nintendo|Nintendo WiiU|Nintendo 3DS|PLAYSTATION|Xbox)\\b",
	                "Watch": "SM-V700"
	            }
	        };

	        // following patterns come from http://detectmobilebrowsers.com/
	        impl.detectMobileBrowsers = {
	            fullPattern: /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i,
	            shortPattern: /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i,
	            tabletPattern: /android|ipad|playbook|silk/i
	        };

	        var hasOwnProp = Object.prototype.hasOwnProperty,
	            isArray;

	        impl.FALLBACK_PHONE = 'UnknownPhone';
	        impl.FALLBACK_TABLET = 'UnknownTablet';
	        impl.FALLBACK_MOBILE = 'UnknownMobile';

	        isArray = 'isArray' in Array ? Array.isArray : function (value) {
	            return Object.prototype.toString.call(value) === '[object Array]';
	        };

	        function equalIC(a, b) {
	            return a != null && b != null && a.toLowerCase() === b.toLowerCase();
	        }

	        function containsIC(array, value) {
	            var valueLC,
	                i,
	                len = array.length;
	            if (!len || !value) {
	                return false;
	            }
	            valueLC = value.toLowerCase();
	            for (i = 0; i < len; ++i) {
	                if (valueLC === array[i].toLowerCase()) {
	                    return true;
	                }
	            }
	            return false;
	        }

	        function convertPropsToRegExp(object) {
	            for (var key in object) {
	                if (hasOwnProp.call(object, key)) {
	                    object[key] = new RegExp(object[key], 'i');
	                }
	            }
	        }

	        (function init() {
	            var key,
	                values,
	                value,
	                i,
	                len,
	                verPos,
	                mobileDetectRules = impl.mobileDetectRules;
	            for (key in mobileDetectRules.props) {
	                if (hasOwnProp.call(mobileDetectRules.props, key)) {
	                    values = mobileDetectRules.props[key];
	                    if (!isArray(values)) {
	                        values = [values];
	                    }
	                    len = values.length;
	                    for (i = 0; i < len; ++i) {
	                        value = values[i];
	                        verPos = value.indexOf('[VER]');
	                        if (verPos >= 0) {
	                            value = value.substring(0, verPos) + '([\\w._\\+]+)' + value.substring(verPos + 5);
	                        }
	                        values[i] = new RegExp(value, 'i');
	                    }
	                    mobileDetectRules.props[key] = values;
	                }
	            }
	            convertPropsToRegExp(mobileDetectRules.oss);
	            convertPropsToRegExp(mobileDetectRules.phones);
	            convertPropsToRegExp(mobileDetectRules.tablets);
	            convertPropsToRegExp(mobileDetectRules.uas);
	            convertPropsToRegExp(mobileDetectRules.utils);

	            // copy some patterns to oss0 which are tested first (see issue#15)
	            mobileDetectRules.oss0 = {
	                WindowsPhoneOS: mobileDetectRules.oss.WindowsPhoneOS,
	                WindowsMobileOS: mobileDetectRules.oss.WindowsMobileOS
	            };
	        })();

	        /**
	         * Test userAgent string against a set of rules and find the first matched key.
	         * @param {Object} rules (key is String, value is RegExp)
	         * @param {String} userAgent the navigator.userAgent (or HTTP-Header 'User-Agent').
	         * @returns {String|null} the matched key if found, otherwise <tt>null</tt>
	         * @private
	         */
	        impl.findMatch = function (rules, userAgent) {
	            for (var key in rules) {
	                if (hasOwnProp.call(rules, key)) {
	                    if (rules[key].test(userAgent)) {
	                        return key;
	                    }
	                }
	            }
	            return null;
	        };

	        /**
	         * Test userAgent string against a set of rules and return an array of matched keys.
	         * @param {Object} rules (key is String, value is RegExp)
	         * @param {String} userAgent the navigator.userAgent (or HTTP-Header 'User-Agent').
	         * @returns {Array} an array of matched keys, may be empty when there is no match, but not <tt>null</tt>
	         * @private
	         */
	        impl.findMatches = function (rules, userAgent) {
	            var result = [];
	            for (var key in rules) {
	                if (hasOwnProp.call(rules, key)) {
	                    if (rules[key].test(userAgent)) {
	                        result.push(key);
	                    }
	                }
	            }
	            return result;
	        };

	        /**
	         * Check the version of the given property in the User-Agent.
	         *
	         * @param {String} propertyName
	         * @param {String} userAgent
	         * @return {String} version or <tt>null</tt> if version not found
	         * @private
	         */
	        impl.getVersionStr = function (propertyName, userAgent) {
	            var props = impl.mobileDetectRules.props,
	                patterns,
	                i,
	                len,
	                match;
	            if (hasOwnProp.call(props, propertyName)) {
	                patterns = props[propertyName];
	                len = patterns.length;
	                for (i = 0; i < len; ++i) {
	                    match = patterns[i].exec(userAgent);
	                    if (match !== null) {
	                        return match[1];
	                    }
	                }
	            }
	            return null;
	        };

	        /**
	         * Check the version of the given property in the User-Agent.
	         * Will return a float number. (eg. 2_0 will return 2.0, 4.3.1 will return 4.31)
	         *
	         * @param {String} propertyName
	         * @param {String} userAgent
	         * @return {Number} version or <tt>NaN</tt> if version not found
	         * @private
	         */
	        impl.getVersion = function (propertyName, userAgent) {
	            var version = impl.getVersionStr(propertyName, userAgent);
	            return version ? impl.prepareVersionNo(version) : NaN;
	        };

	        /**
	         * Prepare the version number.
	         *
	         * @param {String} version
	         * @return {Number} the version number as a floating number
	         * @private
	         */
	        impl.prepareVersionNo = function (version) {
	            var numbers;

	            numbers = version.split(/[a-z._ \/\-]/i);
	            if (numbers.length === 1) {
	                version = numbers[0];
	            }
	            if (numbers.length > 1) {
	                version = numbers[0] + '.';
	                numbers.shift();
	                version += numbers.join('');
	            }
	            return Number(version);
	        };

	        impl.isMobileFallback = function (userAgent) {
	            return impl.detectMobileBrowsers.fullPattern.test(userAgent) || impl.detectMobileBrowsers.shortPattern.test(userAgent.substr(0, 4));
	        };

	        impl.isTabletFallback = function (userAgent) {
	            return impl.detectMobileBrowsers.tabletPattern.test(userAgent);
	        };

	        impl.prepareDetectionCache = function (cache, userAgent, maxPhoneWidth) {
	            if (cache.mobile !== undefined) {
	                return;
	            }
	            var phone, tablet, phoneSized;

	            // first check for stronger tablet rules, then phone (see issue#5)
	            tablet = impl.findMatch(impl.mobileDetectRules.tablets, userAgent);
	            if (tablet) {
	                cache.mobile = cache.tablet = tablet;
	                cache.phone = null;
	                return; // unambiguously identified as tablet
	            }

	            phone = impl.findMatch(impl.mobileDetectRules.phones, userAgent);
	            if (phone) {
	                cache.mobile = cache.phone = phone;
	                cache.tablet = null;
	                return; // unambiguously identified as phone
	            }

	            // our rules haven't found a match -> try more general fallback rules
	            if (impl.isMobileFallback(userAgent)) {
	                phoneSized = MobileDetect.isPhoneSized(maxPhoneWidth);
	                if (phoneSized === undefined) {
	                    cache.mobile = impl.FALLBACK_MOBILE;
	                    cache.tablet = cache.phone = null;
	                } else if (phoneSized) {
	                    cache.mobile = cache.phone = impl.FALLBACK_PHONE;
	                    cache.tablet = null;
	                } else {
	                    cache.mobile = cache.tablet = impl.FALLBACK_TABLET;
	                    cache.phone = null;
	                }
	            } else if (impl.isTabletFallback(userAgent)) {
	                cache.mobile = cache.tablet = impl.FALLBACK_TABLET;
	                cache.phone = null;
	            } else {
	                // not mobile at all!
	                cache.mobile = cache.tablet = cache.phone = null;
	            }
	        };

	        // t is a reference to a MobileDetect instance
	        impl.mobileGrade = function (t) {
	            // impl note:
	            // To keep in sync w/ Mobile_Detect.php easily, the following code is tightly aligned to the PHP version.
	            // When changes are made in Mobile_Detect.php, copy this method and replace:
	            //     $this-> / t.
	            //     self::MOBILE_GRADE_(.) / '$1'
	            //     , self::VERSION_TYPE_FLOAT / (nothing)
	            //     isIOS() / os('iOS')
	            //     [reg] / (nothing)   <-- jsdelivr complaining about unescaped unicode character U+00AE
	            var $isMobile = t.mobile() !== null;

	            if (
	            // Apple iOS 3.2-5.1 - Tested on the original iPad (4.3 / 5.0), iPad 2 (4.3), iPad 3 (5.1), original iPhone (3.1), iPhone 3 (3.2), 3GS (4.3), 4 (4.3 / 5.0), and 4S (5.1)
	            t.os('iOS') && t.version('iPad') >= 4.3 || t.os('iOS') && t.version('iPhone') >= 3.1 || t.os('iOS') && t.version('iPod') >= 3.1 ||

	            // Android 2.1-2.3 - Tested on the HTC Incredible (2.2), original Droid (2.2), HTC Aria (2.1), Google Nexus S (2.3). Functional on 1.5 & 1.6 but performance may be sluggish, tested on Google G1 (1.5)
	            // Android 3.1 (Honeycomb)  - Tested on the Samsung Galaxy Tab 10.1 and Motorola XOOM
	            // Android 4.0 (ICS)  - Tested on a Galaxy Nexus. Note: transition performance can be poor on upgraded devices
	            // Android 4.1 (Jelly Bean)  - Tested on a Galaxy Nexus and Galaxy 7
	            t.version('Android') > 2.1 && t.is('Webkit') ||

	            // Windows Phone 7-7.5 - Tested on the HTC Surround (7.0) HTC Trophy (7.5), LG-E900 (7.5), Nokia Lumia 800
	            t.version('Windows Phone OS') >= 7.0 ||

	            // Blackberry 7 - Tested on BlackBerry Torch 9810
	            // Blackberry 6.0 - Tested on the Torch 9800 and Style 9670
	            t.is('BlackBerry') && t.version('BlackBerry') >= 6.0 ||
	            // Blackberry Playbook (1.0-2.0) - Tested on PlayBook
	            t.match('Playbook.*Tablet') ||

	            // Palm WebOS (1.4-2.0) - Tested on the Palm Pixi (1.4), Pre (1.4), Pre 2 (2.0)
	            t.version('webOS') >= 1.4 && t.match('Palm|Pre|Pixi') ||
	            // Palm WebOS 3.0  - Tested on HP TouchPad
	            t.match('hp.*TouchPad') ||

	            // Firefox Mobile (12 Beta) - Tested on Android 2.3 device
	            t.is('Firefox') && t.version('Firefox') >= 12 ||

	            // Chrome for Android - Tested on Android 4.0, 4.1 device
	            t.is('Chrome') && t.is('AndroidOS') && t.version('Android') >= 4.0 ||

	            // Skyfire 4.1 - Tested on Android 2.3 device
	            t.is('Skyfire') && t.version('Skyfire') >= 4.1 && t.is('AndroidOS') && t.version('Android') >= 2.3 ||

	            // Opera Mobile 11.5-12: Tested on Android 2.3
	            t.is('Opera') && t.version('Opera Mobi') > 11 && t.is('AndroidOS') ||

	            // Meego 1.2 - Tested on Nokia 950 and N9
	            t.is('MeeGoOS') ||

	            // Tizen (pre-release) - Tested on early hardware
	            t.is('Tizen') ||

	            // Samsung Bada 2.0 - Tested on a Samsung Wave 3, Dolphin browser
	            // @todo: more tests here!
	            t.is('Dolfin') && t.version('Bada') >= 2.0 ||

	            // UC Browser - Tested on Android 2.3 device
	            (t.is('UC Browser') || t.is('Dolfin')) && t.version('Android') >= 2.3 ||

	            // Kindle 3 and Fire  - Tested on the built-in WebKit browser for each
	            t.match('Kindle Fire') || t.is('Kindle') && t.version('Kindle') >= 3.0 ||

	            // Nook Color 1.4.1 - Tested on original Nook Color, not Nook Tablet
	            t.is('AndroidOS') && t.is('NookTablet') ||

	            // Chrome Desktop 11-21 - Tested on OS X 10.7 and Windows 7
	            t.version('Chrome') >= 11 && !$isMobile ||

	            // Safari Desktop 4-5 - Tested on OS X 10.7 and Windows 7
	            t.version('Safari') >= 5.0 && !$isMobile ||

	            // Firefox Desktop 4-13 - Tested on OS X 10.7 and Windows 7
	            t.version('Firefox') >= 4.0 && !$isMobile ||

	            // Internet Explorer 7-9 - Tested on Windows XP, Vista and 7
	            t.version('MSIE') >= 7.0 && !$isMobile ||

	            // Opera Desktop 10-12 - Tested on OS X 10.7 and Windows 7
	            // @reference: http://my.opera.com/community/openweb/idopera/
	            t.version('Opera') >= 10 && !$isMobile) {
	                return 'A';
	            }

	            if (t.os('iOS') && t.version('iPad') < 4.3 || t.os('iOS') && t.version('iPhone') < 3.1 || t.os('iOS') && t.version('iPod') < 3.1 ||

	            // Blackberry 5.0: Tested on the Storm 2 9550, Bold 9770
	            t.is('Blackberry') && t.version('BlackBerry') >= 5 && t.version('BlackBerry') < 6 ||

	            //Opera Mini (5.0-6.5) - Tested on iOS 3.2/4.3 and Android 2.3
	            t.version('Opera Mini') >= 5.0 && t.version('Opera Mini') <= 6.5 && (t.version('Android') >= 2.3 || t.is('iOS')) ||

	            // Nokia Symbian^3 - Tested on Nokia N8 (Symbian^3), C7 (Symbian^3), also works on N97 (Symbian^1)
	            t.match('NokiaN8|NokiaC7|N97.*Series60|Symbian/3') ||

	            // @todo: report this (tested on Nokia N71)
	            t.version('Opera Mobi') >= 11 && t.is('SymbianOS')) {
	                return 'B';
	            }

	            if (
	            // Blackberry 4.x - Tested on the Curve 8330
	            t.version('BlackBerry') < 5.0 ||
	            // Windows Mobile - Tested on the HTC Leo (WinMo 5.2)
	            t.match('MSIEMobile|Windows CE.*Mobile') || t.version('Windows Mobile') <= 5.2) {
	                return 'C';
	            }

	            //All older smartphone platforms and featurephones - Any device that doesn't support media queries
	            //will receive the basic, C grade experience.
	            return 'C';
	        };

	        impl.detectOS = function (ua) {
	            return impl.findMatch(impl.mobileDetectRules.oss0, ua) || impl.findMatch(impl.mobileDetectRules.oss, ua);
	        };

	        impl.getDeviceSmallerSide = function () {
	            return window.screen.width < window.screen.height ? window.screen.width : window.screen.height;
	        };

	        /**
	         * Constructor for MobileDetect object.
	         * <br>
	         * Such an object will keep a reference to the given user-agent string and cache most of the detect queries.<br>
	         * <div style="background-color: #d9edf7; border: 1px solid #bce8f1; color: #3a87ad; padding: 14px; border-radius: 2px; margin-top: 20px">
	         *     <strong>Find information how to download and install:</strong>
	         *     <a href="https://github.com/hgoebl/mobile-detect.js/">github.com/hgoebl/mobile-detect.js/</a>
	         * </div>
	         *
	         * @example <pre>
	         *     var md = new MobileDetect(window.navigator.userAgent);
	         *     if (md.mobile()) {
	         *         location.href = (md.mobileGrade() === 'A') ? '/mobile/' : '/lynx/';
	         *     }
	         * </pre>
	         *
	         * @param {string} userAgent typically taken from window.navigator.userAgent or http_header['User-Agent']
	         * @param {number} [maxPhoneWidth=600] <strong>only for browsers</strong> specify a value for the maximum
	         *        width of smallest device side (in logical "CSS" pixels) until a device detected as mobile will be handled
	         *        as phone.
	         *        This is only used in cases where the device cannot be classified as phone or tablet.<br>
	         *        See <a href="http://developer.android.com/guide/practices/screens_support.html">Declaring Tablet Layouts
	         *        for Android</a>.<br>
	         *        If you provide a value < 0, then this "fuzzy" check is disabled.
	         * @constructor
	         * @global
	         */
	        function MobileDetect(userAgent, maxPhoneWidth) {
	            this.ua = userAgent || '';
	            this._cache = {};
	            //600dp is typical 7" tablet minimum width
	            this.maxPhoneWidth = maxPhoneWidth || 600;
	        }

	        MobileDetect.prototype = {
	            constructor: MobileDetect,

	            /**
	             * Returns the detected phone or tablet type or <tt>null</tt> if it is not a mobile device.
	             * <br>
	             * For a list of possible return values see {@link MobileDetect#phone} and {@link MobileDetect#tablet}.<br>
	             * <br>
	             * If the device is not detected by the regular expressions from Mobile-Detect, a test is made against
	             * the patterns of <a href="http://detectmobilebrowsers.com/">detectmobilebrowsers.com</a>. If this test
	             * is positive, a value of <code>UnknownPhone</code>, <code>UnknownTablet</code> or
	             * <code>UnknownMobile</code> is returned.<br>
	             * When used in browser, the decision whether phone or tablet is made based on <code>screen.width/height</code>.<br>
	             * <br>
	             * When used server-side (node.js), there is no way to tell the difference between <code>UnknownTablet</code>
	             * and <code>UnknownMobile</code>, so you will get <code>UnknownMobile</code> here.<br>
	             * Be aware that since v1.0.0 in this special case you will get <code>UnknownMobile</code> only for:
	             * {@link MobileDetect#mobile}, not for {@link MobileDetect#phone} and {@link MobileDetect#tablet}.
	             * In versions before v1.0.0 all 3 methods returned <code>UnknownMobile</code> which was tedious to use.
	             * <br>
	             * In most cases you will use the return value just as a boolean.
	             *
	             * @returns {String} the key for the phone family or tablet family, e.g. "Nexus".
	             * @function MobileDetect#mobile
	             */
	            mobile: function mobile() {
	                impl.prepareDetectionCache(this._cache, this.ua, this.maxPhoneWidth);
	                return this._cache.mobile;
	            },

	            /**
	             * Returns the detected phone type/family string or <tt>null</tt>.
	             * <br>
	             * The returned tablet (family or producer) is one of following keys:<br>
	             * <br><tt>iPhone, BlackBerry, HTC, Nexus, Dell, Motorola, Samsung, LG, Sony, Asus,
	             * Micromax, Palm, Vertu, Pantech, Fly, Wiko, iMobile, SimValley, Wolfgang,
	             * Alcatel, Nintendo, Amoi, INQ, GenericPhone</tt><br>
	             * <br>
	             * If the device is not detected by the regular expressions from Mobile-Detect, a test is made against
	             * the patterns of <a href="http://detectmobilebrowsers.com/">detectmobilebrowsers.com</a>. If this test
	             * is positive, a value of <code>UnknownPhone</code> or <code>UnknownMobile</code> is returned.<br>
	             * When used in browser, the decision whether phone or tablet is made based on <code>screen.width/height</code>.<br>
	             * <br>
	             * When used server-side (node.js), there is no way to tell the difference between <code>UnknownTablet</code>
	             * and <code>UnknownMobile</code>, so you will get <code>null</code> here, while {@link MobileDetect#mobile}
	             * will return <code>UnknownMobile</code>.<br>
	             * Be aware that since v1.0.0 in this special case you will get <code>UnknownMobile</code> only for:
	             * {@link MobileDetect#mobile}, not for {@link MobileDetect#phone} and {@link MobileDetect#tablet}.
	             * In versions before v1.0.0 all 3 methods returned <code>UnknownMobile</code> which was tedious to use.
	             * <br>
	             * In most cases you will use the return value just as a boolean.
	             *
	             * @returns {String} the key of the phone family or producer, e.g. "iPhone"
	             * @function MobileDetect#phone
	             */
	            phone: function phone() {
	                impl.prepareDetectionCache(this._cache, this.ua, this.maxPhoneWidth);
	                return this._cache.phone;
	            },

	            /**
	             * Returns the detected tablet type/family string or <tt>null</tt>.
	             * <br>
	             * The returned tablet (family or producer) is one of following keys:<br>
	             * <br><tt>iPad, NexusTablet, SamsungTablet, Kindle, SurfaceTablet, HPTablet, AsusTablet,
	             * BlackBerryTablet, HTCtablet, MotorolaTablet, NookTablet, AcerTablet,
	             * ToshibaTablet, LGTablet, FujitsuTablet, PrestigioTablet, LenovoTablet,
	             * DellTablet, YarvikTablet, MedionTablet, ArnovaTablet, IntensoTablet, IRUTablet,
	             * MegafonTablet, EbodaTablet, AllViewTablet, ArchosTablet, AinolTablet,
	             * SonyTablet, PhilipsTablet, CubeTablet, CobyTablet, MIDTablet, MSITablet,
	             * SMiTTablet, RockChipTablet, FlyTablet, bqTablet, HuaweiTablet, NecTablet,
	             * PantechTablet, BronchoTablet, VersusTablet, ZyncTablet, PositivoTablet,
	             * NabiTablet, KoboTablet, DanewTablet, TexetTablet, PlaystationTablet,
	             * TrekstorTablet, PyleAudioTablet, AdvanTablet, DanyTechTablet, GalapadTablet,
	             * MicromaxTablet, KarbonnTablet, AllFineTablet, PROSCANTablet, YONESTablet,
	             * ChangJiaTablet, GUTablet, PointOfViewTablet, OvermaxTablet, HCLTablet,
	             * DPSTablet, VistureTablet, CrestaTablet, MediatekTablet, ConcordeTablet,
	             * GoCleverTablet, ModecomTablet, VoninoTablet, ECSTablet, StorexTablet,
	             * VodafoneTablet, EssentielBTablet, RossMoorTablet, iMobileTablet, TolinoTablet,
	             * AudioSonicTablet, AMPETablet, SkkTablet, TecnoTablet, JXDTablet, iJoyTablet,
	             * FX2Tablet, XoroTablet, ViewsonicTablet, OdysTablet, CaptivaTablet,
	             * IconbitTablet, TeclastTablet, OndaTablet, JaytechTablet, BlaupunktTablet,
	             * DigmaTablet, EvolioTablet, LavaTablet, AocTablet, MpmanTablet, CelkonTablet,
	             * WolderTablet, MiTablet, NibiruTablet, NexoTablet, LeaderTablet, UbislateTablet,
	             * PocketBookTablet, Hudl, TelstraTablet, GenericTablet</tt><br>
	             * <br>
	             * If the device is not detected by the regular expressions from Mobile-Detect, a test is made against
	             * the patterns of <a href="http://detectmobilebrowsers.com/">detectmobilebrowsers.com</a>. If this test
	             * is positive, a value of <code>UnknownTablet</code> or <code>UnknownMobile</code> is returned.<br>
	             * When used in browser, the decision whether phone or tablet is made based on <code>screen.width/height</code>.<br>
	             * <br>
	             * When used server-side (node.js), there is no way to tell the difference between <code>UnknownTablet</code>
	             * and <code>UnknownMobile</code>, so you will get <code>null</code> here, while {@link MobileDetect#mobile}
	             * will return <code>UnknownMobile</code>.<br>
	             * Be aware that since v1.0.0 in this special case you will get <code>UnknownMobile</code> only for:
	             * {@link MobileDetect#mobile}, not for {@link MobileDetect#phone} and {@link MobileDetect#tablet}.
	             * In versions before v1.0.0 all 3 methods returned <code>UnknownMobile</code> which was tedious to use.
	             * <br>
	             * In most cases you will use the return value just as a boolean.
	             *
	             * @returns {String} the key of the tablet family or producer, e.g. "SamsungTablet"
	             * @function MobileDetect#tablet
	             */
	            tablet: function tablet() {
	                impl.prepareDetectionCache(this._cache, this.ua, this.maxPhoneWidth);
	                return this._cache.tablet;
	            },

	            /**
	             * Returns the (first) detected user-agent string or <tt>null</tt>.
	             * <br>
	             * The returned user-agent is one of following keys:<br>
	             * <br><tt>Chrome, Dolfin, Opera, Skyfire, IE, Firefox, Bolt, TeaShark, Blazer, Safari,
	             * Tizen, UCBrowser, baiduboxapp, baidubrowser, DiigoBrowser, Puffin, Mercury,
	             * ObigoBrowser, NetFront, GenericBrowser</tt><br>
	             * <br>
	             * In most cases calling {@link MobileDetect#userAgent} will be sufficient. But there are rare
	             * cases where a mobile device pretends to be more than one particular browser. You can get the
	             * list of all matches with {@link MobileDetect#userAgents} or check for a particular value by
	             * providing one of the defined keys as first argument to {@link MobileDetect#is}.
	             *
	             * @returns {String} the key for the detected user-agent or <tt>null</tt>
	             * @function MobileDetect#userAgent
	             */
	            userAgent: function userAgent() {
	                if (this._cache.userAgent === undefined) {
	                    this._cache.userAgent = impl.findMatch(impl.mobileDetectRules.uas, this.ua);
	                }
	                return this._cache.userAgent;
	            },

	            /**
	             * Returns all detected user-agent strings.
	             * <br>
	             * The array is empty or contains one or more of following keys:<br>
	             * <br><tt>Chrome, Dolfin, Opera, Skyfire, IE, Firefox, Bolt, TeaShark, Blazer, Safari,
	             * Tizen, UCBrowser, baiduboxapp, baidubrowser, DiigoBrowser, Puffin, Mercury,
	             * ObigoBrowser, NetFront, GenericBrowser</tt><br>
	             * <br>
	             * In most cases calling {@link MobileDetect#userAgent} will be sufficient. But there are rare
	             * cases where a mobile device pretends to be more than one particular browser. You can get the
	             * list of all matches with {@link MobileDetect#userAgents} or check for a particular value by
	             * providing one of the defined keys as first argument to {@link MobileDetect#is}.
	             *
	             * @returns {Array} the array of detected user-agent keys or <tt>[]</tt>
	             * @function MobileDetect#userAgents
	             */
	            userAgents: function userAgents() {
	                if (this._cache.userAgents === undefined) {
	                    this._cache.userAgents = impl.findMatches(impl.mobileDetectRules.uas, this.ua);
	                }
	                return this._cache.userAgents;
	            },

	            /**
	             * Returns the detected operating system string or <tt>null</tt>.
	             * <br>
	             * The operating system is one of following keys:<br>
	             * <br><tt>AndroidOS, BlackBerryOS, PalmOS, SymbianOS, WindowsMobileOS, WindowsPhoneOS,
	             * iOS, MeeGoOS, MaemoOS, JavaOS, webOS, badaOS, BREWOS</tt><br>
	             *
	             * @returns {String} the key for the detected operating system.
	             * @function MobileDetect#os
	             */
	            os: function os() {
	                if (this._cache.os === undefined) {
	                    this._cache.os = impl.detectOS(this.ua);
	                }
	                return this._cache.os;
	            },

	            /**
	             * Get the version (as Number) of the given property in the User-Agent.
	             * <br>
	             * Will return a float number. (eg. 2_0 will return 2.0, 4.3.1 will return 4.31)
	             *
	             * @param {String} key a key defining a thing which has a version.<br>
	             *        You can use one of following keys:<br>
	             * <br><tt>Mobile, Build, Version, VendorID, iPad, iPhone, iPod, Kindle, Chrome, Coast,
	             * Dolfin, Firefox, Fennec, IE, NetFront, NokiaBrowser, Opera, Opera Mini, Opera
	             * Mobi, UC Browser, MQQBrowser, MicroMessenger, baiduboxapp, baidubrowser, Iron,
	             * Safari, Skyfire, Tizen, Webkit, Gecko, Trident, Presto, iOS, Android,
	             * BlackBerry, BREW, Java, Windows Phone OS, Windows Phone, Windows CE, Windows
	             * NT, Symbian, webOS</tt><br>
	             *
	             * @returns {Number} the version as float or <tt>NaN</tt> if User-Agent doesn't contain this version.
	             *          Be careful when comparing this value with '==' operator!
	             * @function MobileDetect#version
	             */
	            version: function version(key) {
	                return impl.getVersion(key, this.ua);
	            },

	            /**
	             * Get the version (as String) of the given property in the User-Agent.
	             * <br>
	             *
	             * @param {String} key a key defining a thing which has a version.<br>
	             *        You can use one of following keys:<br>
	             * <br><tt>Mobile, Build, Version, VendorID, iPad, iPhone, iPod, Kindle, Chrome, Coast,
	             * Dolfin, Firefox, Fennec, IE, NetFront, NokiaBrowser, Opera, Opera Mini, Opera
	             * Mobi, UC Browser, MQQBrowser, MicroMessenger, baiduboxapp, baidubrowser, Iron,
	             * Safari, Skyfire, Tizen, Webkit, Gecko, Trident, Presto, iOS, Android,
	             * BlackBerry, BREW, Java, Windows Phone OS, Windows Phone, Windows CE, Windows
	             * NT, Symbian, webOS</tt><br>
	             *
	             * @returns {String} the "raw" version as String or <tt>null</tt> if User-Agent doesn't contain this version.
	             *
	             * @function MobileDetect#versionStr
	             */
	            versionStr: function versionStr(key) {
	                return impl.getVersionStr(key, this.ua);
	            },

	            /**
	             * Global test key against userAgent, os, phone, tablet and some other properties of userAgent string.
	             *
	             * @param {String} key the key (case-insensitive) of a userAgent, an operating system, phone or
	             *        tablet family.<br>
	             *        For a complete list of possible values, see {@link MobileDetect#userAgent},
	             *        {@link MobileDetect#os}, {@link MobileDetect#phone}, {@link MobileDetect#tablet}.<br>
	             *        Additionally you have following keys:<br>
	             * <br><tt>Bot, MobileBot, DesktopMode, TV, WebKit, Console, Watch</tt><br>
	             *
	             * @returns {boolean} <tt>true</tt> when the given key is one of the defined keys of userAgent, os, phone,
	             *                    tablet or one of the listed additional keys, otherwise <tt>false</tt>
	             * @function MobileDetect#is
	             */
	            is: function is(key) {
	                return containsIC(this.userAgents(), key) || equalIC(key, this.os()) || equalIC(key, this.phone()) || equalIC(key, this.tablet()) || containsIC(impl.findMatches(impl.mobileDetectRules.utils, this.ua), key);
	            },

	            /**
	             * Do a quick test against navigator::userAgent.
	             *
	             * @param {String|RegExp} pattern the pattern, either as String or RegExp
	             *                        (a string will be converted to a case-insensitive RegExp).
	             * @returns {boolean} <tt>true</tt> when the pattern matches, otherwise <tt>false</tt>
	             * @function MobileDetect#match
	             */
	            match: function match(pattern) {
	                if (!(pattern instanceof RegExp)) {
	                    pattern = new RegExp(pattern, 'i');
	                }
	                return pattern.test(this.ua);
	            },

	            /**
	             * Checks whether the mobile device can be considered as phone regarding <code>screen.width</code>.
	             * <br>
	             * Obviously this method makes sense in browser environments only (not for Node.js)!
	             * @param {number} [maxPhoneWidth] the maximum logical pixels (aka. CSS-pixels) to be considered as phone.<br>
	             *        The argument is optional and if not present or falsy, the value of the constructor is taken.
	             * @returns {boolean|undefined} <code>undefined</code> if screen size wasn't detectable, else <code>true</code>
	             *          when screen.width is less or equal to maxPhoneWidth, otherwise <code>false</code>.<br>
	             *          Will always return <code>undefined</code> server-side.
	             */
	            isPhoneSized: function isPhoneSized(maxPhoneWidth) {
	                return MobileDetect.isPhoneSized(maxPhoneWidth || this.maxPhoneWidth);
	            },

	            /**
	             * Returns the mobile grade ('A', 'B', 'C').
	             *
	             * @returns {String} one of the mobile grades ('A', 'B', 'C').
	             * @function MobileDetect#mobileGrade
	             */
	            mobileGrade: function mobileGrade() {
	                if (this._cache.grade === undefined) {
	                    this._cache.grade = impl.mobileGrade(this);
	                }
	                return this._cache.grade;
	            }
	        };

	        // environment-dependent
	        if (typeof window !== 'undefined' && window.screen) {
	            MobileDetect.isPhoneSized = function (maxPhoneWidth) {
	                return maxPhoneWidth < 0 ? undefined : impl.getDeviceSmallerSide() <= maxPhoneWidth;
	            };
	        } else {
	            MobileDetect.isPhoneSized = function () {};
	        }

	        // should not be replaced by a completely new object - just overwrite existing methods
	        MobileDetect._impl = impl;

	        return MobileDetect;
	    }); // end of call of define()
	})(function (undefined) {
	    if (typeof module !== 'undefined' && module.exports) {
	        return function (factory) {
	            module.exports = factory();
	        };
	    } else if (true) {
	        return __webpack_require__(123);
	    } else if (typeof window !== 'undefined') {
	        return function (factory) {
	            window.MobileDetect = factory();
	        };
	    } else {
	        // please file a bug if you get this error!
	        throw new Error('unknown environment');
	    }
	}());

/***/ },
/* 36 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	/* WEBPACK VAR INJECTION */(function(module) {var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_RESULT__;"use strict";var _typeof=typeof Symbol==="function"&&typeof Symbol.iterator==="symbol"?function(obj){return typeof obj;}:function(obj){return obj&&typeof Symbol==="function"&&obj.constructor===Symbol?"symbol":typeof obj;};!function(a,b){"object"==( false?"undefined":_typeof(exports))&&"undefined"!=typeof module?module.exports=b(): true?!(__WEBPACK_AMD_DEFINE_FACTORY__ = (b), __WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ? (__WEBPACK_AMD_DEFINE_FACTORY__.call(exports, __webpack_require__, exports, module)) : __WEBPACK_AMD_DEFINE_FACTORY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)):a.moment=b();}(undefined,function(){"use strict";function a(){return ce.apply(null,arguments);}function b(a){ce=a;}function c(a){return a instanceof Array||"[object Array]"===Object.prototype.toString.call(a);}function d(a){return a instanceof Date||"[object Date]"===Object.prototype.toString.call(a);}function e(a,b){var c,d=[];for(c=0;c<a.length;++c){d.push(b(a[c],c));}return d;}function f(a,b){return Object.prototype.hasOwnProperty.call(a,b);}function g(a,b){for(var c in b){f(b,c)&&(a[c]=b[c]);}return f(b,"toString")&&(a.toString=b.toString),f(b,"valueOf")&&(a.valueOf=b.valueOf),a;}function h(a,b,c,d){return Ja(a,b,c,d,!0).utc();}function i(){return {empty:!1,unusedTokens:[],unusedInput:[],overflow:-2,charsLeftOver:0,nullInput:!1,invalidMonth:null,invalidFormat:!1,userInvalidated:!1,iso:!1,parsedDateParts:[],meridiem:null};}function j(a){return null==a._pf&&(a._pf=i()),a._pf;}function k(a){if(null==a._isValid){var b=j(a),c=de.call(b.parsedDateParts,function(a){return null!=a;});a._isValid=!isNaN(a._d.getTime())&&b.overflow<0&&!b.empty&&!b.invalidMonth&&!b.invalidWeekday&&!b.nullInput&&!b.invalidFormat&&!b.userInvalidated&&(!b.meridiem||b.meridiem&&c),a._strict&&(a._isValid=a._isValid&&0===b.charsLeftOver&&0===b.unusedTokens.length&&void 0===b.bigHour);}return a._isValid;}function l(a){var b=h(NaN);return null!=a?g(j(b),a):j(b).userInvalidated=!0,b;}function m(a){return void 0===a;}function n(a,b){var c,d,e;if(m(b._isAMomentObject)||(a._isAMomentObject=b._isAMomentObject),m(b._i)||(a._i=b._i),m(b._f)||(a._f=b._f),m(b._l)||(a._l=b._l),m(b._strict)||(a._strict=b._strict),m(b._tzm)||(a._tzm=b._tzm),m(b._isUTC)||(a._isUTC=b._isUTC),m(b._offset)||(a._offset=b._offset),m(b._pf)||(a._pf=j(b)),m(b._locale)||(a._locale=b._locale),ee.length>0)for(c in ee){d=ee[c],e=b[d],m(e)||(a[d]=e);}return a;}function o(b){n(this,b),this._d=new Date(null!=b._d?b._d.getTime():NaN),fe===!1&&(fe=!0,a.updateOffset(this),fe=!1);}function p(a){return a instanceof o||null!=a&&null!=a._isAMomentObject;}function q(a){return 0>a?Math.ceil(a):Math.floor(a);}function r(a){var b=+a,c=0;return 0!==b&&isFinite(b)&&(c=q(b)),c;}function s(a,b,c){var d,e=Math.min(a.length,b.length),f=Math.abs(a.length-b.length),g=0;for(d=0;e>d;d++){(c&&a[d]!==b[d]||!c&&r(a[d])!==r(b[d]))&&g++;}return g+f;}function t(b){a.suppressDeprecationWarnings===!1&&"undefined"!=typeof console&&console.warn&&console.warn("Deprecation warning: "+b);}function u(b,c){var d=!0;return g(function(){return null!=a.deprecationHandler&&a.deprecationHandler(null,b),d&&(t(b+"\nArguments: "+Array.prototype.slice.call(arguments).join(", ")+"\n"+new Error().stack),d=!1),c.apply(this,arguments);},c);}function v(b,c){null!=a.deprecationHandler&&a.deprecationHandler(b,c),ge[b]||(t(c),ge[b]=!0);}function w(a){return a instanceof Function||"[object Function]"===Object.prototype.toString.call(a);}function x(a){return "[object Object]"===Object.prototype.toString.call(a);}function y(a){var b,c;for(c in a){b=a[c],w(b)?this[c]=b:this["_"+c]=b;}this._config=a,this._ordinalParseLenient=new RegExp(this._ordinalParse.source+"|"+/\d{1,2}/.source);}function z(a,b){var c,d=g({},a);for(c in b){f(b,c)&&(x(a[c])&&x(b[c])?(d[c]={},g(d[c],a[c]),g(d[c],b[c])):null!=b[c]?d[c]=b[c]:delete d[c]);}return d;}function A(a){null!=a&&this.set(a);}function B(a){return a?a.toLowerCase().replace("_","-"):a;}function C(a){for(var b,c,d,e,f=0;f<a.length;){for(e=B(a[f]).split("-"),b=e.length,c=B(a[f+1]),c=c?c.split("-"):null;b>0;){if(d=D(e.slice(0,b).join("-")))return d;if(c&&c.length>=b&&s(e,c,!0)>=b-1)break;b--;}f++;}return null;}function D(a){var b=null;if(!ke[a]&&"undefined"!=typeof module&&module&&module.exports)try{b=ie._abbr,!(function webpackMissingModule() { var e = new Error("Cannot find module \"./locale\""); e.code = 'MODULE_NOT_FOUND'; throw e; }()),E(b);}catch(c){}return ke[a];}function E(a,b){var c;return a&&(c=m(b)?H(a):F(a,b),c&&(ie=c)),ie._abbr;}function F(a,b){return null!==b?(b.abbr=a,null!=ke[a]?(v("defineLocaleOverride","use moment.updateLocale(localeName, config) to change an existing locale. moment.defineLocale(localeName, config) should only be used for creating a new locale"),b=z(ke[a]._config,b)):null!=b.parentLocale&&(null!=ke[b.parentLocale]?b=z(ke[b.parentLocale]._config,b):v("parentLocaleUndefined","specified parentLocale is not defined yet")),ke[a]=new A(b),E(a),ke[a]):(delete ke[a],null);}function G(a,b){if(null!=b){var c;null!=ke[a]&&(b=z(ke[a]._config,b)),c=new A(b),c.parentLocale=ke[a],ke[a]=c,E(a);}else null!=ke[a]&&(null!=ke[a].parentLocale?ke[a]=ke[a].parentLocale:null!=ke[a]&&delete ke[a]);return ke[a];}function H(a){var b;if(a&&a._locale&&a._locale._abbr&&(a=a._locale._abbr),!a)return ie;if(!c(a)){if(b=D(a))return b;a=[a];}return C(a);}function I(){return he(ke);}function J(a,b){var c=a.toLowerCase();le[c]=le[c+"s"]=le[b]=a;}function K(a){return "string"==typeof a?le[a]||le[a.toLowerCase()]:void 0;}function L(a){var b,c,d={};for(c in a){f(a,c)&&(b=K(c),b&&(d[b]=a[c]));}return d;}function M(b,c){return function(d){return null!=d?(O(this,b,d),a.updateOffset(this,c),this):N(this,b);};}function N(a,b){return a.isValid()?a._d["get"+(a._isUTC?"UTC":"")+b]():NaN;}function O(a,b,c){a.isValid()&&a._d["set"+(a._isUTC?"UTC":"")+b](c);}function P(a,b){var c;if("object"==(typeof a==="undefined"?"undefined":_typeof(a)))for(c in a){this.set(c,a[c]);}else if(a=K(a),w(this[a]))return this[a](b);return this;}function Q(a,b,c){var d=""+Math.abs(a),e=b-d.length,f=a>=0;return (f?c?"+":"":"-")+Math.pow(10,Math.max(0,e)).toString().substr(1)+d;}function R(a,b,c,d){var e=d;"string"==typeof d&&(e=function e(){return this[d]();}),a&&(pe[a]=e),b&&(pe[b[0]]=function(){return Q(e.apply(this,arguments),b[1],b[2]);}),c&&(pe[c]=function(){return this.localeData().ordinal(e.apply(this,arguments),a);});}function S(a){return a.match(/\[[\s\S]/)?a.replace(/^\[|\]$/g,""):a.replace(/\\/g,"");}function T(a){var b,c,d=a.match(me);for(b=0,c=d.length;c>b;b++){pe[d[b]]?d[b]=pe[d[b]]:d[b]=S(d[b]);}return function(b){var e,f="";for(e=0;c>e;e++){f+=d[e] instanceof Function?d[e].call(b,a):d[e];}return f;};}function U(a,b){return a.isValid()?(b=V(b,a.localeData()),oe[b]=oe[b]||T(b),oe[b](a)):a.localeData().invalidDate();}function V(a,b){function c(a){return b.longDateFormat(a)||a;}var d=5;for(ne.lastIndex=0;d>=0&&ne.test(a);){a=a.replace(ne,c),ne.lastIndex=0,d-=1;}return a;}function W(a,b,c){He[a]=w(b)?b:function(a,d){return a&&c?c:b;};}function X(a,b){return f(He,a)?He[a](b._strict,b._locale):new RegExp(Y(a));}function Y(a){return Z(a.replace("\\","").replace(/\\(\[)|\\(\])|\[([^\]\[]*)\]|\\(.)/g,function(a,b,c,d,e){return b||c||d||e;}));}function Z(a){return a.replace(/[-\/\\^$*+?.()|[\]{}]/g,"\\$&");}function $(a,b){var c,d=b;for("string"==typeof a&&(a=[a]),"number"==typeof b&&(d=function d(a,c){c[b]=r(a);}),c=0;c<a.length;c++){Ie[a[c]]=d;}}function _(a,b){$(a,function(a,c,d,e){d._w=d._w||{},b(a,d._w,d,e);});}function aa(a,b,c){null!=b&&f(Ie,a)&&Ie[a](b,c._a,c,a);}function ba(a,b){return new Date(Date.UTC(a,b+1,0)).getUTCDate();}function ca(a,b){return c(this._months)?this._months[a.month()]:this._months[Se.test(b)?"format":"standalone"][a.month()];}function da(a,b){return c(this._monthsShort)?this._monthsShort[a.month()]:this._monthsShort[Se.test(b)?"format":"standalone"][a.month()];}function ea(a,b,c){var d,e,f,g=a.toLocaleLowerCase();if(!this._monthsParse)for(this._monthsParse=[],this._longMonthsParse=[],this._shortMonthsParse=[],d=0;12>d;++d){f=h([2e3,d]),this._shortMonthsParse[d]=this.monthsShort(f,"").toLocaleLowerCase(),this._longMonthsParse[d]=this.months(f,"").toLocaleLowerCase();}return c?"MMM"===b?(e=je.call(this._shortMonthsParse,g),-1!==e?e:null):(e=je.call(this._longMonthsParse,g),-1!==e?e:null):"MMM"===b?(e=je.call(this._shortMonthsParse,g),-1!==e?e:(e=je.call(this._longMonthsParse,g),-1!==e?e:null)):(e=je.call(this._longMonthsParse,g),-1!==e?e:(e=je.call(this._shortMonthsParse,g),-1!==e?e:null));}function fa(a,b,c){var d,e,f;if(this._monthsParseExact)return ea.call(this,a,b,c);for(this._monthsParse||(this._monthsParse=[],this._longMonthsParse=[],this._shortMonthsParse=[]),d=0;12>d;d++){if(e=h([2e3,d]),c&&!this._longMonthsParse[d]&&(this._longMonthsParse[d]=new RegExp("^"+this.months(e,"").replace(".","")+"$","i"),this._shortMonthsParse[d]=new RegExp("^"+this.monthsShort(e,"").replace(".","")+"$","i")),c||this._monthsParse[d]||(f="^"+this.months(e,"")+"|^"+this.monthsShort(e,""),this._monthsParse[d]=new RegExp(f.replace(".",""),"i")),c&&"MMMM"===b&&this._longMonthsParse[d].test(a))return d;if(c&&"MMM"===b&&this._shortMonthsParse[d].test(a))return d;if(!c&&this._monthsParse[d].test(a))return d;}}function ga(a,b){var c;if(!a.isValid())return a;if("string"==typeof b)if(/^\d+$/.test(b))b=r(b);else if(b=a.localeData().monthsParse(b),"number"!=typeof b)return a;return c=Math.min(a.date(),ba(a.year(),b)),a._d["set"+(a._isUTC?"UTC":"")+"Month"](b,c),a;}function ha(b){return null!=b?(ga(this,b),a.updateOffset(this,!0),this):N(this,"Month");}function ia(){return ba(this.year(),this.month());}function ja(a){return this._monthsParseExact?(f(this,"_monthsRegex")||la.call(this),a?this._monthsShortStrictRegex:this._monthsShortRegex):this._monthsShortStrictRegex&&a?this._monthsShortStrictRegex:this._monthsShortRegex;}function ka(a){return this._monthsParseExact?(f(this,"_monthsRegex")||la.call(this),a?this._monthsStrictRegex:this._monthsRegex):this._monthsStrictRegex&&a?this._monthsStrictRegex:this._monthsRegex;}function la(){function a(a,b){return b.length-a.length;}var b,c,d=[],e=[],f=[];for(b=0;12>b;b++){c=h([2e3,b]),d.push(this.monthsShort(c,"")),e.push(this.months(c,"")),f.push(this.months(c,"")),f.push(this.monthsShort(c,""));}for(d.sort(a),e.sort(a),f.sort(a),b=0;12>b;b++){d[b]=Z(d[b]),e[b]=Z(e[b]),f[b]=Z(f[b]);}this._monthsRegex=new RegExp("^("+f.join("|")+")","i"),this._monthsShortRegex=this._monthsRegex,this._monthsStrictRegex=new RegExp("^("+e.join("|")+")","i"),this._monthsShortStrictRegex=new RegExp("^("+d.join("|")+")","i");}function ma(a){var b,c=a._a;return c&&-2===j(a).overflow&&(b=c[Ke]<0||c[Ke]>11?Ke:c[Le]<1||c[Le]>ba(c[Je],c[Ke])?Le:c[Me]<0||c[Me]>24||24===c[Me]&&(0!==c[Ne]||0!==c[Oe]||0!==c[Pe])?Me:c[Ne]<0||c[Ne]>59?Ne:c[Oe]<0||c[Oe]>59?Oe:c[Pe]<0||c[Pe]>999?Pe:-1,j(a)._overflowDayOfYear&&(Je>b||b>Le)&&(b=Le),j(a)._overflowWeeks&&-1===b&&(b=Qe),j(a)._overflowWeekday&&-1===b&&(b=Re),j(a).overflow=b),a;}function na(a){var b,c,d,e,f,g,h=a._i,i=Xe.exec(h)||Ye.exec(h);if(i){for(j(a).iso=!0,b=0,c=$e.length;c>b;b++){if($e[b][1].exec(i[1])){e=$e[b][0],d=$e[b][2]!==!1;break;}}if(null==e)return void (a._isValid=!1);if(i[3]){for(b=0,c=_e.length;c>b;b++){if(_e[b][1].exec(i[3])){f=(i[2]||" ")+_e[b][0];break;}}if(null==f)return void (a._isValid=!1);}if(!d&&null!=f)return void (a._isValid=!1);if(i[4]){if(!Ze.exec(i[4]))return void (a._isValid=!1);g="Z";}a._f=e+(f||"")+(g||""),Ca(a);}else a._isValid=!1;}function oa(b){var c=af.exec(b._i);return null!==c?void (b._d=new Date(+c[1])):(na(b),void (b._isValid===!1&&(delete b._isValid,a.createFromInputFallback(b))));}function pa(a,b,c,d,e,f,g){var h=new Date(a,b,c,d,e,f,g);return 100>a&&a>=0&&isFinite(h.getFullYear())&&h.setFullYear(a),h;}function qa(a){var b=new Date(Date.UTC.apply(null,arguments));return 100>a&&a>=0&&isFinite(b.getUTCFullYear())&&b.setUTCFullYear(a),b;}function ra(a){return sa(a)?366:365;}function sa(a){return a%4===0&&a%100!==0||a%400===0;}function ta(){return sa(this.year());}function ua(a,b,c){var d=7+b-c,e=(7+qa(a,0,d).getUTCDay()-b)%7;return -e+d-1;}function va(a,b,c,d,e){var f,g,h=(7+c-d)%7,i=ua(a,d,e),j=1+7*(b-1)+h+i;return 0>=j?(f=a-1,g=ra(f)+j):j>ra(a)?(f=a+1,g=j-ra(a)):(f=a,g=j),{year:f,dayOfYear:g};}function wa(a,b,c){var d,e,f=ua(a.year(),b,c),g=Math.floor((a.dayOfYear()-f-1)/7)+1;return 1>g?(e=a.year()-1,d=g+xa(e,b,c)):g>xa(a.year(),b,c)?(d=g-xa(a.year(),b,c),e=a.year()+1):(e=a.year(),d=g),{week:d,year:e};}function xa(a,b,c){var d=ua(a,b,c),e=ua(a+1,b,c);return (ra(a)-d+e)/7;}function ya(a,b,c){return null!=a?a:null!=b?b:c;}function za(b){var c=new Date(a.now());return b._useUTC?[c.getUTCFullYear(),c.getUTCMonth(),c.getUTCDate()]:[c.getFullYear(),c.getMonth(),c.getDate()];}function Aa(a){var b,c,d,e,f=[];if(!a._d){for(d=za(a),a._w&&null==a._a[Le]&&null==a._a[Ke]&&Ba(a),a._dayOfYear&&(e=ya(a._a[Je],d[Je]),a._dayOfYear>ra(e)&&(j(a)._overflowDayOfYear=!0),c=qa(e,0,a._dayOfYear),a._a[Ke]=c.getUTCMonth(),a._a[Le]=c.getUTCDate()),b=0;3>b&&null==a._a[b];++b){a._a[b]=f[b]=d[b];}for(;7>b;b++){a._a[b]=f[b]=null==a._a[b]?2===b?1:0:a._a[b];}24===a._a[Me]&&0===a._a[Ne]&&0===a._a[Oe]&&0===a._a[Pe]&&(a._nextDay=!0,a._a[Me]=0),a._d=(a._useUTC?qa:pa).apply(null,f),null!=a._tzm&&a._d.setUTCMinutes(a._d.getUTCMinutes()-a._tzm),a._nextDay&&(a._a[Me]=24);}}function Ba(a){var b,c,d,e,f,g,h,i;b=a._w,null!=b.GG||null!=b.W||null!=b.E?(f=1,g=4,c=ya(b.GG,a._a[Je],wa(Ka(),1,4).year),d=ya(b.W,1),e=ya(b.E,1),(1>e||e>7)&&(i=!0)):(f=a._locale._week.dow,g=a._locale._week.doy,c=ya(b.gg,a._a[Je],wa(Ka(),f,g).year),d=ya(b.w,1),null!=b.d?(e=b.d,(0>e||e>6)&&(i=!0)):null!=b.e?(e=b.e+f,(b.e<0||b.e>6)&&(i=!0)):e=f),1>d||d>xa(c,f,g)?j(a)._overflowWeeks=!0:null!=i?j(a)._overflowWeekday=!0:(h=va(c,d,e,f,g),a._a[Je]=h.year,a._dayOfYear=h.dayOfYear);}function Ca(b){if(b._f===a.ISO_8601)return void na(b);b._a=[],j(b).empty=!0;var c,d,e,f,g,h=""+b._i,i=h.length,k=0;for(e=V(b._f,b._locale).match(me)||[],c=0;c<e.length;c++){f=e[c],d=(h.match(X(f,b))||[])[0],d&&(g=h.substr(0,h.indexOf(d)),g.length>0&&j(b).unusedInput.push(g),h=h.slice(h.indexOf(d)+d.length),k+=d.length),pe[f]?(d?j(b).empty=!1:j(b).unusedTokens.push(f),aa(f,d,b)):b._strict&&!d&&j(b).unusedTokens.push(f);}j(b).charsLeftOver=i-k,h.length>0&&j(b).unusedInput.push(h),j(b).bigHour===!0&&b._a[Me]<=12&&b._a[Me]>0&&(j(b).bigHour=void 0),j(b).parsedDateParts=b._a.slice(0),j(b).meridiem=b._meridiem,b._a[Me]=Da(b._locale,b._a[Me],b._meridiem),Aa(b),ma(b);}function Da(a,b,c){var d;return null==c?b:null!=a.meridiemHour?a.meridiemHour(b,c):null!=a.isPM?(d=a.isPM(c),d&&12>b&&(b+=12),d||12!==b||(b=0),b):b;}function Ea(a){var b,c,d,e,f;if(0===a._f.length)return j(a).invalidFormat=!0,void (a._d=new Date(NaN));for(e=0;e<a._f.length;e++){f=0,b=n({},a),null!=a._useUTC&&(b._useUTC=a._useUTC),b._f=a._f[e],Ca(b),k(b)&&(f+=j(b).charsLeftOver,f+=10*j(b).unusedTokens.length,j(b).score=f,(null==d||d>f)&&(d=f,c=b));}g(a,c||b);}function Fa(a){if(!a._d){var b=L(a._i);a._a=e([b.year,b.month,b.day||b.date,b.hour,b.minute,b.second,b.millisecond],function(a){return a&&parseInt(a,10);}),Aa(a);}}function Ga(a){var b=new o(ma(Ha(a)));return b._nextDay&&(b.add(1,"d"),b._nextDay=void 0),b;}function Ha(a){var b=a._i,e=a._f;return a._locale=a._locale||H(a._l),null===b||void 0===e&&""===b?l({nullInput:!0}):("string"==typeof b&&(a._i=b=a._locale.preparse(b)),p(b)?new o(ma(b)):(c(e)?Ea(a):e?Ca(a):d(b)?a._d=b:Ia(a),k(a)||(a._d=null),a));}function Ia(b){var f=b._i;void 0===f?b._d=new Date(a.now()):d(f)?b._d=new Date(f.valueOf()):"string"==typeof f?oa(b):c(f)?(b._a=e(f.slice(0),function(a){return parseInt(a,10);}),Aa(b)):"object"==(typeof f==="undefined"?"undefined":_typeof(f))?Fa(b):"number"==typeof f?b._d=new Date(f):a.createFromInputFallback(b);}function Ja(a,b,c,d,e){var f={};return "boolean"==typeof c&&(d=c,c=void 0),f._isAMomentObject=!0,f._useUTC=f._isUTC=e,f._l=c,f._i=a,f._f=b,f._strict=d,Ga(f);}function Ka(a,b,c,d){return Ja(a,b,c,d,!1);}function La(a,b){var d,e;if(1===b.length&&c(b[0])&&(b=b[0]),!b.length)return Ka();for(d=b[0],e=1;e<b.length;++e){(!b[e].isValid()||b[e][a](d))&&(d=b[e]);}return d;}function Ma(){var a=[].slice.call(arguments,0);return La("isBefore",a);}function Na(){var a=[].slice.call(arguments,0);return La("isAfter",a);}function Oa(a){var b=L(a),c=b.year||0,d=b.quarter||0,e=b.month||0,f=b.week||0,g=b.day||0,h=b.hour||0,i=b.minute||0,j=b.second||0,k=b.millisecond||0;this._milliseconds=+k+1e3*j+6e4*i+1e3*h*60*60,this._days=+g+7*f,this._months=+e+3*d+12*c,this._data={},this._locale=H(),this._bubble();}function Pa(a){return a instanceof Oa;}function Qa(a,b){R(a,0,0,function(){var a=this.utcOffset(),c="+";return 0>a&&(a=-a,c="-"),c+Q(~ ~(a/60),2)+b+Q(~ ~a%60,2);});}function Ra(a,b){var c=(b||"").match(a)||[],d=c[c.length-1]||[],e=(d+"").match(ff)||["-",0,0],f=+(60*e[1])+r(e[2]);return "+"===e[0]?f:-f;}function Sa(b,c){var e,f;return c._isUTC?(e=c.clone(),f=(p(b)||d(b)?b.valueOf():Ka(b).valueOf())-e.valueOf(),e._d.setTime(e._d.valueOf()+f),a.updateOffset(e,!1),e):Ka(b).local();}function Ta(a){return 15*-Math.round(a._d.getTimezoneOffset()/15);}function Ua(b,c){var d,e=this._offset||0;return this.isValid()?null!=b?("string"==typeof b?b=Ra(Ee,b):Math.abs(b)<16&&(b=60*b),!this._isUTC&&c&&(d=Ta(this)),this._offset=b,this._isUTC=!0,null!=d&&this.add(d,"m"),e!==b&&(!c||this._changeInProgress?jb(this,db(b-e,"m"),1,!1):this._changeInProgress||(this._changeInProgress=!0,a.updateOffset(this,!0),this._changeInProgress=null)),this):this._isUTC?e:Ta(this):null!=b?this:NaN;}function Va(a,b){return null!=a?("string"!=typeof a&&(a=-a),this.utcOffset(a,b),this):-this.utcOffset();}function Wa(a){return this.utcOffset(0,a);}function Xa(a){return this._isUTC&&(this.utcOffset(0,a),this._isUTC=!1,a&&this.subtract(Ta(this),"m")),this;}function Ya(){return this._tzm?this.utcOffset(this._tzm):"string"==typeof this._i&&this.utcOffset(Ra(De,this._i)),this;}function Za(a){return this.isValid()?(a=a?Ka(a).utcOffset():0,(this.utcOffset()-a)%60===0):!1;}function $a(){return this.utcOffset()>this.clone().month(0).utcOffset()||this.utcOffset()>this.clone().month(5).utcOffset();}function _a(){if(!m(this._isDSTShifted))return this._isDSTShifted;var a={};if(n(a,this),a=Ha(a),a._a){var b=a._isUTC?h(a._a):Ka(a._a);this._isDSTShifted=this.isValid()&&s(a._a,b.toArray())>0;}else this._isDSTShifted=!1;return this._isDSTShifted;}function ab(){return this.isValid()?!this._isUTC:!1;}function bb(){return this.isValid()?this._isUTC:!1;}function cb(){return this.isValid()?this._isUTC&&0===this._offset:!1;}function db(a,b){var c,d,e,g=a,h=null;return Pa(a)?g={ms:a._milliseconds,d:a._days,M:a._months}:"number"==typeof a?(g={},b?g[b]=a:g.milliseconds=a):(h=gf.exec(a))?(c="-"===h[1]?-1:1,g={y:0,d:r(h[Le])*c,h:r(h[Me])*c,m:r(h[Ne])*c,s:r(h[Oe])*c,ms:r(h[Pe])*c}):(h=hf.exec(a))?(c="-"===h[1]?-1:1,g={y:eb(h[2],c),M:eb(h[3],c),w:eb(h[4],c),d:eb(h[5],c),h:eb(h[6],c),m:eb(h[7],c),s:eb(h[8],c)}):null==g?g={}:"object"==(typeof g==="undefined"?"undefined":_typeof(g))&&("from" in g||"to" in g)&&(e=gb(Ka(g.from),Ka(g.to)),g={},g.ms=e.milliseconds,g.M=e.months),d=new Oa(g),Pa(a)&&f(a,"_locale")&&(d._locale=a._locale),d;}function eb(a,b){var c=a&&parseFloat(a.replace(",","."));return (isNaN(c)?0:c)*b;}function fb(a,b){var c={milliseconds:0,months:0};return c.months=b.month()-a.month()+12*(b.year()-a.year()),a.clone().add(c.months,"M").isAfter(b)&&--c.months,c.milliseconds=+b-+a.clone().add(c.months,"M"),c;}function gb(a,b){var c;return a.isValid()&&b.isValid()?(b=Sa(b,a),a.isBefore(b)?c=fb(a,b):(c=fb(b,a),c.milliseconds=-c.milliseconds,c.months=-c.months),c):{milliseconds:0,months:0};}function hb(a){return 0>a?-1*Math.round(-1*a):Math.round(a);}function ib(a,b){return function(c,d){var e,f;return null===d||isNaN(+d)||(v(b,"moment()."+b+"(period, number) is deprecated. Please use moment()."+b+"(number, period)."),f=c,c=d,d=f),c="string"==typeof c?+c:c,e=db(c,d),jb(this,e,a),this;};}function jb(b,c,d,e){var f=c._milliseconds,g=hb(c._days),h=hb(c._months);b.isValid()&&(e=null==e?!0:e,f&&b._d.setTime(b._d.valueOf()+f*d),g&&O(b,"Date",N(b,"Date")+g*d),h&&ga(b,N(b,"Month")+h*d),e&&a.updateOffset(b,g||h));}function kb(a,b){var c=a||Ka(),d=Sa(c,this).startOf("day"),e=this.diff(d,"days",!0),f=-6>e?"sameElse":-1>e?"lastWeek":0>e?"lastDay":1>e?"sameDay":2>e?"nextDay":7>e?"nextWeek":"sameElse",g=b&&(w(b[f])?b[f]():b[f]);return this.format(g||this.localeData().calendar(f,this,Ka(c)));}function lb(){return new o(this);}function mb(a,b){var c=p(a)?a:Ka(a);return this.isValid()&&c.isValid()?(b=K(m(b)?"millisecond":b),"millisecond"===b?this.valueOf()>c.valueOf():c.valueOf()<this.clone().startOf(b).valueOf()):!1;}function nb(a,b){var c=p(a)?a:Ka(a);return this.isValid()&&c.isValid()?(b=K(m(b)?"millisecond":b),"millisecond"===b?this.valueOf()<c.valueOf():this.clone().endOf(b).valueOf()<c.valueOf()):!1;}function ob(a,b,c,d){return d=d||"()",("("===d[0]?this.isAfter(a,c):!this.isBefore(a,c))&&(")"===d[1]?this.isBefore(b,c):!this.isAfter(b,c));}function pb(a,b){var c,d=p(a)?a:Ka(a);return this.isValid()&&d.isValid()?(b=K(b||"millisecond"),"millisecond"===b?this.valueOf()===d.valueOf():(c=d.valueOf(),this.clone().startOf(b).valueOf()<=c&&c<=this.clone().endOf(b).valueOf())):!1;}function qb(a,b){return this.isSame(a,b)||this.isAfter(a,b);}function rb(a,b){return this.isSame(a,b)||this.isBefore(a,b);}function sb(a,b,c){var d,e,f,g;return this.isValid()?(d=Sa(a,this),d.isValid()?(e=6e4*(d.utcOffset()-this.utcOffset()),b=K(b),"year"===b||"month"===b||"quarter"===b?(g=tb(this,d),"quarter"===b?g/=3:"year"===b&&(g/=12)):(f=this-d,g="second"===b?f/1e3:"minute"===b?f/6e4:"hour"===b?f/36e5:"day"===b?(f-e)/864e5:"week"===b?(f-e)/6048e5:f),c?g:q(g)):NaN):NaN;}function tb(a,b){var c,d,e=12*(b.year()-a.year())+(b.month()-a.month()),f=a.clone().add(e,"months");return 0>b-f?(c=a.clone().add(e-1,"months"),d=(b-f)/(f-c)):(c=a.clone().add(e+1,"months"),d=(b-f)/(c-f)),-(e+d)||0;}function ub(){return this.clone().locale("en").format("ddd MMM DD YYYY HH:mm:ss [GMT]ZZ");}function vb(){var a=this.clone().utc();return 0<a.year()&&a.year()<=9999?w(Date.prototype.toISOString)?this.toDate().toISOString():U(a,"YYYY-MM-DD[T]HH:mm:ss.SSS[Z]"):U(a,"YYYYYY-MM-DD[T]HH:mm:ss.SSS[Z]");}function wb(b){b||(b=this.isUtc()?a.defaultFormatUtc:a.defaultFormat);var c=U(this,b);return this.localeData().postformat(c);}function xb(a,b){return this.isValid()&&(p(a)&&a.isValid()||Ka(a).isValid())?db({to:this,from:a}).locale(this.locale()).humanize(!b):this.localeData().invalidDate();}function yb(a){return this.from(Ka(),a);}function zb(a,b){return this.isValid()&&(p(a)&&a.isValid()||Ka(a).isValid())?db({from:this,to:a}).locale(this.locale()).humanize(!b):this.localeData().invalidDate();}function Ab(a){return this.to(Ka(),a);}function Bb(a){var b;return void 0===a?this._locale._abbr:(b=H(a),null!=b&&(this._locale=b),this);}function Cb(){return this._locale;}function Db(a){switch(a=K(a)){case "year":this.month(0);case "quarter":case "month":this.date(1);case "week":case "isoWeek":case "day":case "date":this.hours(0);case "hour":this.minutes(0);case "minute":this.seconds(0);case "second":this.milliseconds(0);}return "week"===a&&this.weekday(0),"isoWeek"===a&&this.isoWeekday(1),"quarter"===a&&this.month(3*Math.floor(this.month()/3)),this;}function Eb(a){return a=K(a),void 0===a||"millisecond"===a?this:("date"===a&&(a="day"),this.startOf(a).add(1,"isoWeek"===a?"week":a).subtract(1,"ms"));}function Fb(){return this._d.valueOf()-6e4*(this._offset||0);}function Gb(){return Math.floor(this.valueOf()/1e3);}function Hb(){return this._offset?new Date(this.valueOf()):this._d;}function Ib(){var a=this;return [a.year(),a.month(),a.date(),a.hour(),a.minute(),a.second(),a.millisecond()];}function Jb(){var a=this;return {years:a.year(),months:a.month(),date:a.date(),hours:a.hours(),minutes:a.minutes(),seconds:a.seconds(),milliseconds:a.milliseconds()};}function Kb(){return this.isValid()?this.toISOString():null;}function Lb(){return k(this);}function Mb(){return g({},j(this));}function Nb(){return j(this).overflow;}function Ob(){return {input:this._i,format:this._f,locale:this._locale,isUTC:this._isUTC,strict:this._strict};}function Pb(a,b){R(0,[a,a.length],0,b);}function Qb(a){return Ub.call(this,a,this.week(),this.weekday(),this.localeData()._week.dow,this.localeData()._week.doy);}function Rb(a){return Ub.call(this,a,this.isoWeek(),this.isoWeekday(),1,4);}function Sb(){return xa(this.year(),1,4);}function Tb(){var a=this.localeData()._week;return xa(this.year(),a.dow,a.doy);}function Ub(a,b,c,d,e){var f;return null==a?wa(this,d,e).year:(f=xa(a,d,e),b>f&&(b=f),Vb.call(this,a,b,c,d,e));}function Vb(a,b,c,d,e){var f=va(a,b,c,d,e),g=qa(f.year,0,f.dayOfYear);return this.year(g.getUTCFullYear()),this.month(g.getUTCMonth()),this.date(g.getUTCDate()),this;}function Wb(a){return null==a?Math.ceil((this.month()+1)/3):this.month(3*(a-1)+this.month()%3);}function Xb(a){return wa(a,this._week.dow,this._week.doy).week;}function Yb(){return this._week.dow;}function Zb(){return this._week.doy;}function $b(a){var b=this.localeData().week(this);return null==a?b:this.add(7*(a-b),"d");}function _b(a){var b=wa(this,1,4).week;return null==a?b:this.add(7*(a-b),"d");}function ac(a,b){return "string"!=typeof a?a:isNaN(a)?(a=b.weekdaysParse(a),"number"==typeof a?a:null):parseInt(a,10);}function bc(a,b){return c(this._weekdays)?this._weekdays[a.day()]:this._weekdays[this._weekdays.isFormat.test(b)?"format":"standalone"][a.day()];}function cc(a){return this._weekdaysShort[a.day()];}function dc(a){return this._weekdaysMin[a.day()];}function ec(a,b,c){var d,e,f,g=a.toLocaleLowerCase();if(!this._weekdaysParse)for(this._weekdaysParse=[],this._shortWeekdaysParse=[],this._minWeekdaysParse=[],d=0;7>d;++d){f=h([2e3,1]).day(d),this._minWeekdaysParse[d]=this.weekdaysMin(f,"").toLocaleLowerCase(),this._shortWeekdaysParse[d]=this.weekdaysShort(f,"").toLocaleLowerCase(),this._weekdaysParse[d]=this.weekdays(f,"").toLocaleLowerCase();}return c?"dddd"===b?(e=je.call(this._weekdaysParse,g),-1!==e?e:null):"ddd"===b?(e=je.call(this._shortWeekdaysParse,g),-1!==e?e:null):(e=je.call(this._minWeekdaysParse,g),-1!==e?e:null):"dddd"===b?(e=je.call(this._weekdaysParse,g),-1!==e?e:(e=je.call(this._shortWeekdaysParse,g),-1!==e?e:(e=je.call(this._minWeekdaysParse,g),-1!==e?e:null))):"ddd"===b?(e=je.call(this._shortWeekdaysParse,g),-1!==e?e:(e=je.call(this._weekdaysParse,g),-1!==e?e:(e=je.call(this._minWeekdaysParse,g),-1!==e?e:null))):(e=je.call(this._minWeekdaysParse,g),-1!==e?e:(e=je.call(this._weekdaysParse,g),-1!==e?e:(e=je.call(this._shortWeekdaysParse,g),-1!==e?e:null)));}function fc(a,b,c){var d,e,f;if(this._weekdaysParseExact)return ec.call(this,a,b,c);for(this._weekdaysParse||(this._weekdaysParse=[],this._minWeekdaysParse=[],this._shortWeekdaysParse=[],this._fullWeekdaysParse=[]),d=0;7>d;d++){if(e=h([2e3,1]).day(d),c&&!this._fullWeekdaysParse[d]&&(this._fullWeekdaysParse[d]=new RegExp("^"+this.weekdays(e,"").replace(".",".?")+"$","i"),this._shortWeekdaysParse[d]=new RegExp("^"+this.weekdaysShort(e,"").replace(".",".?")+"$","i"),this._minWeekdaysParse[d]=new RegExp("^"+this.weekdaysMin(e,"").replace(".",".?")+"$","i")),this._weekdaysParse[d]||(f="^"+this.weekdays(e,"")+"|^"+this.weekdaysShort(e,"")+"|^"+this.weekdaysMin(e,""),this._weekdaysParse[d]=new RegExp(f.replace(".",""),"i")),c&&"dddd"===b&&this._fullWeekdaysParse[d].test(a))return d;if(c&&"ddd"===b&&this._shortWeekdaysParse[d].test(a))return d;if(c&&"dd"===b&&this._minWeekdaysParse[d].test(a))return d;if(!c&&this._weekdaysParse[d].test(a))return d;}}function gc(a){if(!this.isValid())return null!=a?this:NaN;var b=this._isUTC?this._d.getUTCDay():this._d.getDay();return null!=a?(a=ac(a,this.localeData()),this.add(a-b,"d")):b;}function hc(a){if(!this.isValid())return null!=a?this:NaN;var b=(this.day()+7-this.localeData()._week.dow)%7;return null==a?b:this.add(a-b,"d");}function ic(a){return this.isValid()?null==a?this.day()||7:this.day(this.day()%7?a:a-7):null!=a?this:NaN;}function jc(a){return this._weekdaysParseExact?(f(this,"_weekdaysRegex")||mc.call(this),a?this._weekdaysStrictRegex:this._weekdaysRegex):this._weekdaysStrictRegex&&a?this._weekdaysStrictRegex:this._weekdaysRegex;}function kc(a){return this._weekdaysParseExact?(f(this,"_weekdaysRegex")||mc.call(this),a?this._weekdaysShortStrictRegex:this._weekdaysShortRegex):this._weekdaysShortStrictRegex&&a?this._weekdaysShortStrictRegex:this._weekdaysShortRegex;}function lc(a){return this._weekdaysParseExact?(f(this,"_weekdaysRegex")||mc.call(this),a?this._weekdaysMinStrictRegex:this._weekdaysMinRegex):this._weekdaysMinStrictRegex&&a?this._weekdaysMinStrictRegex:this._weekdaysMinRegex;}function mc(){function a(a,b){return b.length-a.length;}var b,c,d,e,f,g=[],i=[],j=[],k=[];for(b=0;7>b;b++){c=h([2e3,1]).day(b),d=this.weekdaysMin(c,""),e=this.weekdaysShort(c,""),f=this.weekdays(c,""),g.push(d),i.push(e),j.push(f),k.push(d),k.push(e),k.push(f);}for(g.sort(a),i.sort(a),j.sort(a),k.sort(a),b=0;7>b;b++){i[b]=Z(i[b]),j[b]=Z(j[b]),k[b]=Z(k[b]);}this._weekdaysRegex=new RegExp("^("+k.join("|")+")","i"),this._weekdaysShortRegex=this._weekdaysRegex,this._weekdaysMinRegex=this._weekdaysRegex,this._weekdaysStrictRegex=new RegExp("^("+j.join("|")+")","i"),this._weekdaysShortStrictRegex=new RegExp("^("+i.join("|")+")","i"),this._weekdaysMinStrictRegex=new RegExp("^("+g.join("|")+")","i");}function nc(a){var b=Math.round((this.clone().startOf("day")-this.clone().startOf("year"))/864e5)+1;return null==a?b:this.add(a-b,"d");}function oc(){return this.hours()%12||12;}function pc(){return this.hours()||24;}function qc(a,b){R(a,0,0,function(){return this.localeData().meridiem(this.hours(),this.minutes(),b);});}function rc(a,b){return b._meridiemParse;}function sc(a){return "p"===(a+"").toLowerCase().charAt(0);}function tc(a,b,c){return a>11?c?"pm":"PM":c?"am":"AM";}function uc(a,b){b[Pe]=r(1e3*("0."+a));}function vc(){return this._isUTC?"UTC":"";}function wc(){return this._isUTC?"Coordinated Universal Time":"";}function xc(a){return Ka(1e3*a);}function yc(){return Ka.apply(null,arguments).parseZone();}function zc(a,b,c){var d=this._calendar[a];return w(d)?d.call(b,c):d;}function Ac(a){var b=this._longDateFormat[a],c=this._longDateFormat[a.toUpperCase()];return b||!c?b:(this._longDateFormat[a]=c.replace(/MMMM|MM|DD|dddd/g,function(a){return a.slice(1);}),this._longDateFormat[a]);}function Bc(){return this._invalidDate;}function Cc(a){return this._ordinal.replace("%d",a);}function Dc(a){return a;}function Ec(a,b,c,d){var e=this._relativeTime[c];return w(e)?e(a,b,c,d):e.replace(/%d/i,a);}function Fc(a,b){var c=this._relativeTime[a>0?"future":"past"];return w(c)?c(b):c.replace(/%s/i,b);}function Gc(a,b,c,d){var e=H(),f=h().set(d,b);return e[c](f,a);}function Hc(a,b,c){if("number"==typeof a&&(b=a,a=void 0),a=a||"",null!=b)return Gc(a,b,c,"month");var d,e=[];for(d=0;12>d;d++){e[d]=Gc(a,d,c,"month");}return e;}function Ic(a,b,c,d){"boolean"==typeof a?("number"==typeof b&&(c=b,b=void 0),b=b||""):(b=a,c=b,a=!1,"number"==typeof b&&(c=b,b=void 0),b=b||"");var e=H(),f=a?e._week.dow:0;if(null!=c)return Gc(b,(c+f)%7,d,"day");var g,h=[];for(g=0;7>g;g++){h[g]=Gc(b,(g+f)%7,d,"day");}return h;}function Jc(a,b){return Hc(a,b,"months");}function Kc(a,b){return Hc(a,b,"monthsShort");}function Lc(a,b,c){return Ic(a,b,c,"weekdays");}function Mc(a,b,c){return Ic(a,b,c,"weekdaysShort");}function Nc(a,b,c){return Ic(a,b,c,"weekdaysMin");}function Oc(){var a=this._data;return this._milliseconds=Jf(this._milliseconds),this._days=Jf(this._days),this._months=Jf(this._months),a.milliseconds=Jf(a.milliseconds),a.seconds=Jf(a.seconds),a.minutes=Jf(a.minutes),a.hours=Jf(a.hours),a.months=Jf(a.months),a.years=Jf(a.years),this;}function Pc(a,b,c,d){var e=db(b,c);return a._milliseconds+=d*e._milliseconds,a._days+=d*e._days,a._months+=d*e._months,a._bubble();}function Qc(a,b){return Pc(this,a,b,1);}function Rc(a,b){return Pc(this,a,b,-1);}function Sc(a){return 0>a?Math.floor(a):Math.ceil(a);}function Tc(){var a,b,c,d,e,f=this._milliseconds,g=this._days,h=this._months,i=this._data;return f>=0&&g>=0&&h>=0||0>=f&&0>=g&&0>=h||(f+=864e5*Sc(Vc(h)+g),g=0,h=0),i.milliseconds=f%1e3,a=q(f/1e3),i.seconds=a%60,b=q(a/60),i.minutes=b%60,c=q(b/60),i.hours=c%24,g+=q(c/24),e=q(Uc(g)),h+=e,g-=Sc(Vc(e)),d=q(h/12),h%=12,i.days=g,i.months=h,i.years=d,this;}function Uc(a){return 4800*a/146097;}function Vc(a){return 146097*a/4800;}function Wc(a){var b,c,d=this._milliseconds;if(a=K(a),"month"===a||"year"===a)return b=this._days+d/864e5,c=this._months+Uc(b),"month"===a?c:c/12;switch(b=this._days+Math.round(Vc(this._months)),a){case "week":return b/7+d/6048e5;case "day":return b+d/864e5;case "hour":return 24*b+d/36e5;case "minute":return 1440*b+d/6e4;case "second":return 86400*b+d/1e3;case "millisecond":return Math.floor(864e5*b)+d;default:throw new Error("Unknown unit "+a);}}function Xc(){return this._milliseconds+864e5*this._days+this._months%12*2592e6+31536e6*r(this._months/12);}function Yc(a){return function(){return this.as(a);};}function Zc(a){return a=K(a),this[a+"s"]();}function $c(a){return function(){return this._data[a];};}function _c(){return q(this.days()/7);}function ad(a,b,c,d,e){return e.relativeTime(b||1,!!c,a,d);}function bd(a,b,c){var d=db(a).abs(),e=Zf(d.as("s")),f=Zf(d.as("m")),g=Zf(d.as("h")),h=Zf(d.as("d")),i=Zf(d.as("M")),j=Zf(d.as("y")),k=e<$f.s&&["s",e]||1>=f&&["m"]||f<$f.m&&["mm",f]||1>=g&&["h"]||g<$f.h&&["hh",g]||1>=h&&["d"]||h<$f.d&&["dd",h]||1>=i&&["M"]||i<$f.M&&["MM",i]||1>=j&&["y"]||["yy",j];return k[2]=b,k[3]=+a>0,k[4]=c,ad.apply(null,k);}function cd(a,b){return void 0===$f[a]?!1:void 0===b?$f[a]:($f[a]=b,!0);}function dd(a){var b=this.localeData(),c=bd(this,!a,b);return a&&(c=b.pastFuture(+this,c)),b.postformat(c);}function ed(){var a,b,c,d=_f(this._milliseconds)/1e3,e=_f(this._days),f=_f(this._months);a=q(d/60),b=q(a/60),d%=60,a%=60,c=q(f/12),f%=12;var g=c,h=f,i=e,j=b,k=a,l=d,m=this.asSeconds();return m?(0>m?"-":"")+"P"+(g?g+"Y":"")+(h?h+"M":"")+(i?i+"D":"")+(j||k||l?"T":"")+(j?j+"H":"")+(k?k+"M":"")+(l?l+"S":""):"P0D";} //! moment.js locale configuration
	//! locale : belarusian (be)
	//! author : Dmitry Demidov : https://github.com/demidov91
	//! author: Praleska: http://praleska.pro/
	//! Author : Menelion Elensúle : https://github.com/Oire
	function fd(a,b){var c=a.split("_");return b%10===1&&b%100!==11?c[0]:b%10>=2&&4>=b%10&&(10>b%100||b%100>=20)?c[1]:c[2];}function gd(a,b,c){var d={mm:b?"хвіліна_хвіліны_хвілін":"хвіліну_хвіліны_хвілін",hh:b?"гадзіна_гадзіны_гадзін":"гадзіну_гадзіны_гадзін",dd:"дзень_дні_дзён",MM:"месяц_месяцы_месяцаў",yy:"год_гады_гадоў"};return "m"===c?b?"хвіліна":"хвіліну":"h"===c?b?"гадзіна":"гадзіну":a+" "+fd(d[c],+a);} //! moment.js locale configuration
	//! locale : breton (br)
	//! author : Jean-Baptiste Le Duigou : https://github.com/jbleduigou
	function hd(a,b,c){var d={mm:"munutenn",MM:"miz",dd:"devezh"};return a+" "+kd(d[c],a);}function id(a){switch(jd(a)){case 1:case 3:case 4:case 5:case 9:return a+" bloaz";default:return a+" vloaz";}}function jd(a){return a>9?jd(a%10):a;}function kd(a,b){return 2===b?ld(a):a;}function ld(a){var b={m:"v",b:"v",d:"z"};return void 0===b[a.charAt(0)]?a:b[a.charAt(0)]+a.substring(1);} //! moment.js locale configuration
	//! locale : bosnian (bs)
	//! author : Nedim Cholich : https://github.com/frontyard
	//! based on (hr) translation by Bojan Marković
	function md(a,b,c){var d=a+" ";switch(c){case "m":return b?"jedna minuta":"jedne minute";case "mm":return d+=1===a?"minuta":2===a||3===a||4===a?"minute":"minuta";case "h":return b?"jedan sat":"jednog sata";case "hh":return d+=1===a?"sat":2===a||3===a||4===a?"sata":"sati";case "dd":return d+=1===a?"dan":"dana";case "MM":return d+=1===a?"mjesec":2===a||3===a||4===a?"mjeseca":"mjeseci";case "yy":return d+=1===a?"godina":2===a||3===a||4===a?"godine":"godina";}}function nd(a){return a>1&&5>a&&1!==~ ~(a/10);}function od(a,b,c,d){var e=a+" ";switch(c){case "s":return b||d?"pár sekund":"pár sekundami";case "m":return b?"minuta":d?"minutu":"minutou";case "mm":return b||d?e+(nd(a)?"minuty":"minut"):e+"minutami";break;case "h":return b?"hodina":d?"hodinu":"hodinou";case "hh":return b||d?e+(nd(a)?"hodiny":"hodin"):e+"hodinami";break;case "d":return b||d?"den":"dnem";case "dd":return b||d?e+(nd(a)?"dny":"dní"):e+"dny";break;case "M":return b||d?"měsíc":"měsícem";case "MM":return b||d?e+(nd(a)?"měsíce":"měsíců"):e+"měsíci";break;case "y":return b||d?"rok":"rokem";case "yy":return b||d?e+(nd(a)?"roky":"let"):e+"lety";}} //! moment.js locale configuration
	//! locale : austrian german (de-at)
	//! author : lluchs : https://github.com/lluchs
	//! author: Menelion Elensúle: https://github.com/Oire
	//! author : Martin Groller : https://github.com/MadMG
	//! author : Mikolaj Dadela : https://github.com/mik01aj
	function pd(a,b,c,d){var e={m:["eine Minute","einer Minute"],h:["eine Stunde","einer Stunde"],d:["ein Tag","einem Tag"],dd:[a+" Tage",a+" Tagen"],M:["ein Monat","einem Monat"],MM:[a+" Monate",a+" Monaten"],y:["ein Jahr","einem Jahr"],yy:[a+" Jahre",a+" Jahren"]};return b?e[c][0]:e[c][1];} //! moment.js locale configuration
	//! locale : german (de)
	//! author : lluchs : https://github.com/lluchs
	//! author: Menelion Elensúle: https://github.com/Oire
	//! author : Mikolaj Dadela : https://github.com/mik01aj
	function qd(a,b,c,d){var e={m:["eine Minute","einer Minute"],h:["eine Stunde","einer Stunde"],d:["ein Tag","einem Tag"],dd:[a+" Tage",a+" Tagen"],M:["ein Monat","einem Monat"],MM:[a+" Monate",a+" Monaten"],y:["ein Jahr","einem Jahr"],yy:[a+" Jahre",a+" Jahren"]};return b?e[c][0]:e[c][1];} //! moment.js locale configuration
	//! locale : estonian (et)
	//! author : Henry Kehlmann : https://github.com/madhenry
	//! improvements : Illimar Tambek : https://github.com/ragulka
	function rd(a,b,c,d){var e={s:["mõne sekundi","mõni sekund","paar sekundit"],m:["ühe minuti","üks minut"],mm:[a+" minuti",a+" minutit"],h:["ühe tunni","tund aega","üks tund"],hh:[a+" tunni",a+" tundi"],d:["ühe päeva","üks päev"],M:["kuu aja","kuu aega","üks kuu"],MM:[a+" kuu",a+" kuud"],y:["ühe aasta","aasta","üks aasta"],yy:[a+" aasta",a+" aastat"]};return b?e[c][2]?e[c][2]:e[c][1]:d?e[c][0]:e[c][1];}function sd(a,b,c,d){var e="";switch(c){case "s":return d?"muutaman sekunnin":"muutama sekunti";case "m":return d?"minuutin":"minuutti";case "mm":e=d?"minuutin":"minuuttia";break;case "h":return d?"tunnin":"tunti";case "hh":e=d?"tunnin":"tuntia";break;case "d":return d?"päivän":"päivä";case "dd":e=d?"päivän":"päivää";break;case "M":return d?"kuukauden":"kuukausi";case "MM":e=d?"kuukauden":"kuukautta";break;case "y":return d?"vuoden":"vuosi";case "yy":e=d?"vuoden":"vuotta";}return e=td(a,d)+" "+e;}function td(a,b){return 10>a?b?yg[a]:xg[a]:a;} //! moment.js locale configuration
	//! locale : hrvatski (hr)
	//! author : Bojan Marković : https://github.com/bmarkovic
	function ud(a,b,c){var d=a+" ";switch(c){case "m":return b?"jedna minuta":"jedne minute";case "mm":return d+=1===a?"minuta":2===a||3===a||4===a?"minute":"minuta";case "h":return b?"jedan sat":"jednog sata";case "hh":return d+=1===a?"sat":2===a||3===a||4===a?"sata":"sati";case "dd":return d+=1===a?"dan":"dana";case "MM":return d+=1===a?"mjesec":2===a||3===a||4===a?"mjeseca":"mjeseci";case "yy":return d+=1===a?"godina":2===a||3===a||4===a?"godine":"godina";}}function vd(a,b,c,d){var e=a;switch(c){case "s":return d||b?"néhány másodperc":"néhány másodperce";case "m":return "egy"+(d||b?" perc":" perce");case "mm":return e+(d||b?" perc":" perce");case "h":return "egy"+(d||b?" óra":" órája");case "hh":return e+(d||b?" óra":" órája");case "d":return "egy"+(d||b?" nap":" napja");case "dd":return e+(d||b?" nap":" napja");case "M":return "egy"+(d||b?" hónap":" hónapja");case "MM":return e+(d||b?" hónap":" hónapja");case "y":return "egy"+(d||b?" év":" éve");case "yy":return e+(d||b?" év":" éve");}return "";}function wd(a){return (a?"":"[múlt] ")+"["+Ig[this.day()]+"] LT[-kor]";} //! moment.js locale configuration
	//! locale : icelandic (is)
	//! author : Hinrik Örn Sigurðsson : https://github.com/hinrik
	function xd(a){return a%100===11?!0:a%10===1?!1:!0;}function yd(a,b,c,d){var e=a+" ";switch(c){case "s":return b||d?"nokkrar sekúndur":"nokkrum sekúndum";case "m":return b?"mínúta":"mínútu";case "mm":return xd(a)?e+(b||d?"mínútur":"mínútum"):b?e+"mínúta":e+"mínútu";case "hh":return xd(a)?e+(b||d?"klukkustundir":"klukkustundum"):e+"klukkustund";case "d":return b?"dagur":d?"dag":"degi";case "dd":return xd(a)?b?e+"dagar":e+(d?"daga":"dögum"):b?e+"dagur":e+(d?"dag":"degi");case "M":return b?"mánuður":d?"mánuð":"mánuði";case "MM":return xd(a)?b?e+"mánuðir":e+(d?"mánuði":"mánuðum"):b?e+"mánuður":e+(d?"mánuð":"mánuði");case "y":return b||d?"ár":"ári";case "yy":return xd(a)?e+(b||d?"ár":"árum"):e+(b||d?"ár":"ári");}} //! moment.js locale configuration
	//! locale : Luxembourgish (lb)
	//! author : mweimerskirch : https://github.com/mweimerskirch, David Raison : https://github.com/kwisatz
	function zd(a,b,c,d){var e={m:["eng Minutt","enger Minutt"],h:["eng Stonn","enger Stonn"],d:["een Dag","engem Dag"],M:["ee Mount","engem Mount"],y:["ee Joer","engem Joer"]};return b?e[c][0]:e[c][1];}function Ad(a){var b=a.substr(0,a.indexOf(" "));return Cd(b)?"a "+a:"an "+a;}function Bd(a){var b=a.substr(0,a.indexOf(" "));return Cd(b)?"viru "+a:"virun "+a;}function Cd(a){if(a=parseInt(a,10),isNaN(a))return !1;if(0>a)return !0;if(10>a)return a>=4&&7>=a?!0:!1;if(100>a){var b=a%10,c=a/10;return Cd(0===b?c:b);}if(1e4>a){for(;a>=10;){a/=10;}return Cd(a);}return a/=1e3,Cd(a);}function Dd(a,b,c,d){return b?"kelios sekundės":d?"kelių sekundžių":"kelias sekundes";}function Ed(a,b,c,d){return b?Gd(c)[0]:d?Gd(c)[1]:Gd(c)[2];}function Fd(a){return a%10===0||a>10&&20>a;}function Gd(a){return Lg[a].split("_");}function Hd(a,b,c,d){var e=a+" ";return 1===a?e+Ed(a,b,c[0],d):b?e+(Fd(a)?Gd(c)[1]:Gd(c)[0]):d?e+Gd(c)[1]:e+(Fd(a)?Gd(c)[1]:Gd(c)[2]);}function Id(a,b,c){return c?b%10===1&&11!==b?a[2]:a[3]:b%10===1&&11!==b?a[0]:a[1];}function Jd(a,b,c){return a+" "+Id(Mg[c],a,b);}function Kd(a,b,c){return Id(Mg[c],a,b);}function Ld(a,b){return b?"dažas sekundes":"dažām sekundēm";}function Md(a,b,c,d){var e="";if(b)switch(c){case "s":e="काही सेकंद";break;case "m":e="एक मिनिट";break;case "mm":e="%d मिनिटे";break;case "h":e="एक तास";break;case "hh":e="%d तास";break;case "d":e="एक दिवस";break;case "dd":e="%d दिवस";break;case "M":e="एक महिना";break;case "MM":e="%d महिने";break;case "y":e="एक वर्ष";break;case "yy":e="%d वर्षे";}else switch(c){case "s":e="काही सेकंदां";break;case "m":e="एका मिनिटा";break;case "mm":e="%d मिनिटां";break;case "h":e="एका तासा";break;case "hh":e="%d तासां";break;case "d":e="एका दिवसा";break;case "dd":e="%d दिवसां";break;case "M":e="एका महिन्या";break;case "MM":e="%d महिन्यां";break;case "y":e="एका वर्षा";break;case "yy":e="%d वर्षां";}return e.replace(/%d/i,a);}function Nd(a){return 5>a%10&&a%10>1&&~ ~(a/10)%10!==1;}function Od(a,b,c){var d=a+" ";switch(c){case "m":return b?"minuta":"minutę";case "mm":return d+(Nd(a)?"minuty":"minut");case "h":return b?"godzina":"godzinę";case "hh":return d+(Nd(a)?"godziny":"godzin");case "MM":return d+(Nd(a)?"miesiące":"miesięcy");case "yy":return d+(Nd(a)?"lata":"lat");}} //! moment.js locale configuration
	//! locale : romanian (ro)
	//! author : Vlad Gurdiga : https://github.com/gurdiga
	//! author : Valentin Agachi : https://github.com/avaly
	function Pd(a,b,c){var d={mm:"minute",hh:"ore",dd:"zile",MM:"luni",yy:"ani"},e=" ";return (a%100>=20||a>=100&&a%100===0)&&(e=" de "),a+e+d[c];} //! moment.js locale configuration
	//! locale : russian (ru)
	//! author : Viktorminator : https://github.com/Viktorminator
	//! Author : Menelion Elensúle : https://github.com/Oire
	//! author : Коренберг Марк : https://github.com/socketpair
	function Qd(a,b){var c=a.split("_");return b%10===1&&b%100!==11?c[0]:b%10>=2&&4>=b%10&&(10>b%100||b%100>=20)?c[1]:c[2];}function Rd(a,b,c){var d={mm:b?"минута_минуты_минут":"минуту_минуты_минут",hh:"час_часа_часов",dd:"день_дня_дней",MM:"месяц_месяца_месяцев",yy:"год_года_лет"};return "m"===c?b?"минута":"минуту":a+" "+Qd(d[c],+a);}function Sd(a){return a>1&&5>a;}function Td(a,b,c,d){var e=a+" ";switch(c){case "s":return b||d?"pár sekúnd":"pár sekundami";case "m":return b?"minúta":d?"minútu":"minútou";case "mm":return b||d?e+(Sd(a)?"minúty":"minút"):e+"minútami";break;case "h":return b?"hodina":d?"hodinu":"hodinou";case "hh":return b||d?e+(Sd(a)?"hodiny":"hodín"):e+"hodinami";break;case "d":return b||d?"deň":"dňom";case "dd":return b||d?e+(Sd(a)?"dni":"dní"):e+"dňami";break;case "M":return b||d?"mesiac":"mesiacom";case "MM":return b||d?e+(Sd(a)?"mesiace":"mesiacov"):e+"mesiacmi";break;case "y":return b||d?"rok":"rokom";case "yy":return b||d?e+(Sd(a)?"roky":"rokov"):e+"rokmi";}} //! moment.js locale configuration
	//! locale : slovenian (sl)
	//! author : Robert Sedovšek : https://github.com/sedovsek
	function Ud(a,b,c,d){var e=a+" ";switch(c){case "s":return b||d?"nekaj sekund":"nekaj sekundami";case "m":return b?"ena minuta":"eno minuto";case "mm":return e+=1===a?b?"minuta":"minuto":2===a?b||d?"minuti":"minutama":5>a?b||d?"minute":"minutami":b||d?"minut":"minutami";case "h":return b?"ena ura":"eno uro";case "hh":return e+=1===a?b?"ura":"uro":2===a?b||d?"uri":"urama":5>a?b||d?"ure":"urami":b||d?"ur":"urami";case "d":return b||d?"en dan":"enim dnem";case "dd":return e+=1===a?b||d?"dan":"dnem":2===a?b||d?"dni":"dnevoma":b||d?"dni":"dnevi";case "M":return b||d?"en mesec":"enim mesecem";case "MM":return e+=1===a?b||d?"mesec":"mesecem":2===a?b||d?"meseca":"mesecema":5>a?b||d?"mesece":"meseci":b||d?"mesecev":"meseci";case "y":return b||d?"eno leto":"enim letom";case "yy":return e+=1===a?b||d?"leto":"letom":2===a?b||d?"leti":"letoma":5>a?b||d?"leta":"leti":b||d?"let":"leti";}}function Vd(a){var b=a;return b=-1!==a.indexOf("jaj")?b.slice(0,-3)+"leS":-1!==a.indexOf("jar")?b.slice(0,-3)+"waQ":-1!==a.indexOf("DIS")?b.slice(0,-3)+"nem":b+" pIq";}function Wd(a){var b=a;return b=-1!==a.indexOf("jaj")?b.slice(0,-3)+"Hu’":-1!==a.indexOf("jar")?b.slice(0,-3)+"wen":-1!==a.indexOf("DIS")?b.slice(0,-3)+"ben":b+" ret";}function Xd(a,b,c,d){var e=Yd(a);switch(c){case "mm":return e+" tup";case "hh":return e+" rep";case "dd":return e+" jaj";case "MM":return e+" jar";case "yy":return e+" DIS";}}function Yd(a){var b=Math.floor(a%1e3/100),c=Math.floor(a%100/10),d=a%10,e="";return b>0&&(e+=fh[b]+"vatlh"),c>0&&(e+=(""!==e?" ":"")+fh[c]+"maH"),d>0&&(e+=(""!==e?" ":"")+fh[d]),""===e?"pagh":e;}function Zd(a,b,c,d){var e={s:["viensas secunds","'iensas secunds"],m:["'n míut","'iens míut"],mm:[a+" míuts",""+a+" míuts"],h:["'n þora","'iensa þora"],hh:[a+" þoras",""+a+" þoras"],d:["'n ziua","'iensa ziua"],dd:[a+" ziuas",""+a+" ziuas"],M:["'n mes","'iens mes"],MM:[a+" mesen",""+a+" mesen"],y:["'n ar","'iens ar"],yy:[a+" ars",""+a+" ars"]};return d?e[c][0]:b?e[c][0]:e[c][1];} //! moment.js locale configuration
	//! locale : ukrainian (uk)
	//! author : zemlanin : https://github.com/zemlanin
	//! Author : Menelion Elensúle : https://github.com/Oire
	function $d(a,b){var c=a.split("_");return b%10===1&&b%100!==11?c[0]:b%10>=2&&4>=b%10&&(10>b%100||b%100>=20)?c[1]:c[2];}function _d(a,b,c){var d={mm:b?"хвилина_хвилини_хвилин":"хвилину_хвилини_хвилин",hh:b?"година_години_годин":"годину_години_годин",dd:"день_дні_днів",MM:"місяць_місяці_місяців",yy:"рік_роки_років"};return "m"===c?b?"хвилина":"хвилину":"h"===c?b?"година":"годину":a+" "+$d(d[c],+a);}function ae(a,b){var c={nominative:"неділя_понеділок_вівторок_середа_четвер_п’ятниця_субота".split("_"),accusative:"неділю_понеділок_вівторок_середу_четвер_п’ятницю_суботу".split("_"),genitive:"неділі_понеділка_вівторка_середи_четверга_п’ятниці_суботи".split("_")},d=/(\[[ВвУу]\]) ?dddd/.test(b)?"accusative":/\[?(?:минулої|наступної)? ?\] ?dddd/.test(b)?"genitive":"nominative";return c[d][a.day()];}function be(a){return function(){return a+"о"+(11===this.hours()?"б":"")+"] LT";};}var ce,de;de=Array.prototype.some?Array.prototype.some:function(a){for(var b=Object(this),c=b.length>>>0,d=0;c>d;d++){if(d in b&&a.call(this,b[d],d,b))return !0;}return !1;};var ee=a.momentProperties=[],fe=!1,ge={};a.suppressDeprecationWarnings=!1,a.deprecationHandler=null;var he;he=Object.keys?Object.keys:function(a){var b,c=[];for(b in a){f(a,b)&&c.push(b);}return c;};var ie,je,ke={},le={},me=/(\[[^\[]*\])|(\\)?([Hh]mm(ss)?|Mo|MM?M?M?|Do|DDDo|DD?D?D?|ddd?d?|do?|w[o|w]?|W[o|W]?|Qo?|YYYYYY|YYYYY|YYYY|YY|gg(ggg?)?|GG(GGG?)?|e|E|a|A|hh?|HH?|kk?|mm?|ss?|S{1,9}|x|X|zz?|ZZ?|.)/g,ne=/(\[[^\[]*\])|(\\)?(LTS|LT|LL?L?L?|l{1,4})/g,oe={},pe={},qe=/\d/,re=/\d\d/,se=/\d{3}/,te=/\d{4}/,ue=/[+-]?\d{6}/,ve=/\d\d?/,we=/\d\d\d\d?/,xe=/\d\d\d\d\d\d?/,ye=/\d{1,3}/,ze=/\d{1,4}/,Ae=/[+-]?\d{1,6}/,Be=/\d+/,Ce=/[+-]?\d+/,De=/Z|[+-]\d\d:?\d\d/gi,Ee=/Z|[+-]\d\d(?::?\d\d)?/gi,Fe=/[+-]?\d+(\.\d{1,3})?/,Ge=/[0-9]*['a-z\u00A0-\u05FF\u0700-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+|[\u0600-\u06FF\/]+(\s*?[\u0600-\u06FF]+){1,2}/i,He={},Ie={},Je=0,Ke=1,Le=2,Me=3,Ne=4,Oe=5,Pe=6,Qe=7,Re=8;je=Array.prototype.indexOf?Array.prototype.indexOf:function(a){var b;for(b=0;b<this.length;++b){if(this[b]===a)return b;}return -1;},R("M",["MM",2],"Mo",function(){return this.month()+1;}),R("MMM",0,0,function(a){return this.localeData().monthsShort(this,a);}),R("MMMM",0,0,function(a){return this.localeData().months(this,a);}),J("month","M"),W("M",ve),W("MM",ve,re),W("MMM",function(a,b){return b.monthsShortRegex(a);}),W("MMMM",function(a,b){return b.monthsRegex(a);}),$(["M","MM"],function(a,b){b[Ke]=r(a)-1;}),$(["MMM","MMMM"],function(a,b,c,d){var e=c._locale.monthsParse(a,d,c._strict);null!=e?b[Ke]=e:j(c).invalidMonth=a;});var Se=/D[oD]?(\[[^\[\]]*\]|\s+)+MMMM?/,Te="January_February_March_April_May_June_July_August_September_October_November_December".split("_"),Ue="Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec".split("_"),Ve=Ge,We=Ge,Xe=/^\s*((?:[+-]\d{6}|\d{4})-(?:\d\d-\d\d|W\d\d-\d|W\d\d|\d\d\d|\d\d))(?:(T| )(\d\d(?::\d\d(?::\d\d(?:[.,]\d+)?)?)?)([\+\-]\d\d(?::?\d\d)?|\s*Z)?)?/,Ye=/^\s*((?:[+-]\d{6}|\d{4})(?:\d\d\d\d|W\d\d\d|W\d\d|\d\d\d|\d\d))(?:(T| )(\d\d(?:\d\d(?:\d\d(?:[.,]\d+)?)?)?)([\+\-]\d\d(?::?\d\d)?|\s*Z)?)?/,Ze=/Z|[+-]\d\d(?::?\d\d)?/,$e=[["YYYYYY-MM-DD",/[+-]\d{6}-\d\d-\d\d/],["YYYY-MM-DD",/\d{4}-\d\d-\d\d/],["GGGG-[W]WW-E",/\d{4}-W\d\d-\d/],["GGGG-[W]WW",/\d{4}-W\d\d/,!1],["YYYY-DDD",/\d{4}-\d{3}/],["YYYY-MM",/\d{4}-\d\d/,!1],["YYYYYYMMDD",/[+-]\d{10}/],["YYYYMMDD",/\d{8}/],["GGGG[W]WWE",/\d{4}W\d{3}/],["GGGG[W]WW",/\d{4}W\d{2}/,!1],["YYYYDDD",/\d{7}/]],_e=[["HH:mm:ss.SSSS",/\d\d:\d\d:\d\d\.\d+/],["HH:mm:ss,SSSS",/\d\d:\d\d:\d\d,\d+/],["HH:mm:ss",/\d\d:\d\d:\d\d/],["HH:mm",/\d\d:\d\d/],["HHmmss.SSSS",/\d\d\d\d\d\d\.\d+/],["HHmmss,SSSS",/\d\d\d\d\d\d,\d+/],["HHmmss",/\d\d\d\d\d\d/],["HHmm",/\d\d\d\d/],["HH",/\d\d/]],af=/^\/?Date\((\-?\d+)/i;a.createFromInputFallback=u("moment construction falls back to js Date. This is discouraged and will be removed in upcoming major release. Please refer to https://github.com/moment/moment/issues/1407 for more info.",function(a){a._d=new Date(a._i+(a._useUTC?" UTC":""));}),R("Y",0,0,function(){var a=this.year();return 9999>=a?""+a:"+"+a;}),R(0,["YY",2],0,function(){return this.year()%100;}),R(0,["YYYY",4],0,"year"),R(0,["YYYYY",5],0,"year"),R(0,["YYYYYY",6,!0],0,"year"),J("year","y"),W("Y",Ce),W("YY",ve,re),W("YYYY",ze,te),W("YYYYY",Ae,ue),W("YYYYYY",Ae,ue),$(["YYYYY","YYYYYY"],Je),$("YYYY",function(b,c){c[Je]=2===b.length?a.parseTwoDigitYear(b):r(b);}),$("YY",function(b,c){c[Je]=a.parseTwoDigitYear(b);}),$("Y",function(a,b){b[Je]=parseInt(a,10);}),a.parseTwoDigitYear=function(a){return r(a)+(r(a)>68?1900:2e3);};var bf=M("FullYear",!0);a.ISO_8601=function(){};var cf=u("moment().min is deprecated, use moment.max instead. https://github.com/moment/moment/issues/1548",function(){var a=Ka.apply(null,arguments);return this.isValid()&&a.isValid()?this>a?this:a:l();}),df=u("moment().max is deprecated, use moment.min instead. https://github.com/moment/moment/issues/1548",function(){var a=Ka.apply(null,arguments);return this.isValid()&&a.isValid()?a>this?this:a:l();}),ef=function ef(){return Date.now?Date.now():+new Date();};Qa("Z",":"),Qa("ZZ",""),W("Z",Ee),W("ZZ",Ee),$(["Z","ZZ"],function(a,b,c){c._useUTC=!0,c._tzm=Ra(Ee,a);});var ff=/([\+\-]|\d\d)/gi;a.updateOffset=function(){};var gf=/^(\-)?(?:(\d*)[. ])?(\d+)\:(\d+)(?:\:(\d+)\.?(\d{3})?\d*)?$/,hf=/^(-)?P(?:(-?[0-9,.]*)Y)?(?:(-?[0-9,.]*)M)?(?:(-?[0-9,.]*)W)?(?:(-?[0-9,.]*)D)?(?:T(?:(-?[0-9,.]*)H)?(?:(-?[0-9,.]*)M)?(?:(-?[0-9,.]*)S)?)?$/;db.fn=Oa.prototype;var jf=ib(1,"add"),kf=ib(-1,"subtract");a.defaultFormat="YYYY-MM-DDTHH:mm:ssZ",a.defaultFormatUtc="YYYY-MM-DDTHH:mm:ss[Z]";var lf=u("moment().lang() is deprecated. Instead, use moment().localeData() to get the language configuration. Use moment().locale() to change languages.",function(a){return void 0===a?this.localeData():this.locale(a);});R(0,["gg",2],0,function(){return this.weekYear()%100;}),R(0,["GG",2],0,function(){return this.isoWeekYear()%100;}),Pb("gggg","weekYear"),Pb("ggggg","weekYear"),Pb("GGGG","isoWeekYear"),Pb("GGGGG","isoWeekYear"),J("weekYear","gg"),J("isoWeekYear","GG"),W("G",Ce),W("g",Ce),W("GG",ve,re),W("gg",ve,re),W("GGGG",ze,te),W("gggg",ze,te),W("GGGGG",Ae,ue),W("ggggg",Ae,ue),_(["gggg","ggggg","GGGG","GGGGG"],function(a,b,c,d){b[d.substr(0,2)]=r(a);}),_(["gg","GG"],function(b,c,d,e){c[e]=a.parseTwoDigitYear(b);}),R("Q",0,"Qo","quarter"),J("quarter","Q"),W("Q",qe),$("Q",function(a,b){b[Ke]=3*(r(a)-1);}),R("w",["ww",2],"wo","week"),R("W",["WW",2],"Wo","isoWeek"),J("week","w"),J("isoWeek","W"),W("w",ve),W("ww",ve,re),W("W",ve),W("WW",ve,re),_(["w","ww","W","WW"],function(a,b,c,d){b[d.substr(0,1)]=r(a);});var mf={dow:0,doy:6};R("D",["DD",2],"Do","date"),J("date","D"),W("D",ve),W("DD",ve,re),W("Do",function(a,b){return a?b._ordinalParse:b._ordinalParseLenient;}),$(["D","DD"],Le),$("Do",function(a,b){b[Le]=r(a.match(ve)[0],10);});var nf=M("Date",!0);R("d",0,"do","day"),R("dd",0,0,function(a){return this.localeData().weekdaysMin(this,a);}),R("ddd",0,0,function(a){return this.localeData().weekdaysShort(this,a);}),R("dddd",0,0,function(a){return this.localeData().weekdays(this,a);}),R("e",0,0,"weekday"),R("E",0,0,"isoWeekday"),J("day","d"),J("weekday","e"),J("isoWeekday","E"),W("d",ve),W("e",ve),W("E",ve),W("dd",function(a,b){return b.weekdaysMinRegex(a);}),W("ddd",function(a,b){return b.weekdaysShortRegex(a);}),W("dddd",function(a,b){return b.weekdaysRegex(a);}),_(["dd","ddd","dddd"],function(a,b,c,d){var e=c._locale.weekdaysParse(a,d,c._strict);null!=e?b.d=e:j(c).invalidWeekday=a;}),_(["d","e","E"],function(a,b,c,d){b[d]=r(a);});var of="Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday".split("_"),pf="Sun_Mon_Tue_Wed_Thu_Fri_Sat".split("_"),qf="Su_Mo_Tu_We_Th_Fr_Sa".split("_"),rf=Ge,sf=Ge,tf=Ge;R("DDD",["DDDD",3],"DDDo","dayOfYear"),J("dayOfYear","DDD"),W("DDD",ye),W("DDDD",se),$(["DDD","DDDD"],function(a,b,c){c._dayOfYear=r(a);}),R("H",["HH",2],0,"hour"),R("h",["hh",2],0,oc),R("k",["kk",2],0,pc),R("hmm",0,0,function(){return ""+oc.apply(this)+Q(this.minutes(),2);}),R("hmmss",0,0,function(){return ""+oc.apply(this)+Q(this.minutes(),2)+Q(this.seconds(),2);}),R("Hmm",0,0,function(){return ""+this.hours()+Q(this.minutes(),2);}),R("Hmmss",0,0,function(){return ""+this.hours()+Q(this.minutes(),2)+Q(this.seconds(),2);}),qc("a",!0),qc("A",!1),J("hour","h"),W("a",rc),W("A",rc),W("H",ve),W("h",ve),W("HH",ve,re),W("hh",ve,re),W("hmm",we),W("hmmss",xe),W("Hmm",we),W("Hmmss",xe),$(["H","HH"],Me),$(["a","A"],function(a,b,c){c._isPm=c._locale.isPM(a),c._meridiem=a;}),$(["h","hh"],function(a,b,c){b[Me]=r(a),j(c).bigHour=!0;}),$("hmm",function(a,b,c){var d=a.length-2;b[Me]=r(a.substr(0,d)),b[Ne]=r(a.substr(d)),j(c).bigHour=!0;}),$("hmmss",function(a,b,c){var d=a.length-4,e=a.length-2;b[Me]=r(a.substr(0,d)),b[Ne]=r(a.substr(d,2)),b[Oe]=r(a.substr(e)),j(c).bigHour=!0;}),$("Hmm",function(a,b,c){var d=a.length-2;b[Me]=r(a.substr(0,d)),b[Ne]=r(a.substr(d));}),$("Hmmss",function(a,b,c){var d=a.length-4,e=a.length-2;b[Me]=r(a.substr(0,d)),b[Ne]=r(a.substr(d,2)),b[Oe]=r(a.substr(e));});var uf=/[ap]\.?m?\.?/i,vf=M("Hours",!0);R("m",["mm",2],0,"minute"),J("minute","m"),W("m",ve),W("mm",ve,re),$(["m","mm"],Ne);var wf=M("Minutes",!1);R("s",["ss",2],0,"second"),J("second","s"),W("s",ve),W("ss",ve,re),$(["s","ss"],Oe);var xf=M("Seconds",!1);R("S",0,0,function(){return ~ ~(this.millisecond()/100);}),R(0,["SS",2],0,function(){return ~ ~(this.millisecond()/10);}),R(0,["SSS",3],0,"millisecond"),R(0,["SSSS",4],0,function(){return 10*this.millisecond();}),R(0,["SSSSS",5],0,function(){return 100*this.millisecond();}),R(0,["SSSSSS",6],0,function(){return 1e3*this.millisecond();}),R(0,["SSSSSSS",7],0,function(){return 1e4*this.millisecond();}),R(0,["SSSSSSSS",8],0,function(){return 1e5*this.millisecond();}),R(0,["SSSSSSSSS",9],0,function(){return 1e6*this.millisecond();}),J("millisecond","ms"),W("S",ye,qe),W("SS",ye,re),W("SSS",ye,se);var yf;for(yf="SSSS";yf.length<=9;yf+="S"){W(yf,Be);}for(yf="S";yf.length<=9;yf+="S"){$(yf,uc);}var zf=M("Milliseconds",!1);R("z",0,0,"zoneAbbr"),R("zz",0,0,"zoneName");var Af=o.prototype;Af.add=jf,Af.calendar=kb,Af.clone=lb,Af.diff=sb,Af.endOf=Eb,Af.format=wb,Af.from=xb,Af.fromNow=yb,Af.to=zb,Af.toNow=Ab,Af.get=P,Af.invalidAt=Nb,Af.isAfter=mb,Af.isBefore=nb,Af.isBetween=ob,Af.isSame=pb,Af.isSameOrAfter=qb,Af.isSameOrBefore=rb,Af.isValid=Lb,Af.lang=lf,Af.locale=Bb,Af.localeData=Cb,Af.max=df,Af.min=cf,Af.parsingFlags=Mb,Af.set=P,Af.startOf=Db,Af.subtract=kf,Af.toArray=Ib,Af.toObject=Jb,Af.toDate=Hb,Af.toISOString=vb,Af.toJSON=Kb,Af.toString=ub,Af.unix=Gb,Af.valueOf=Fb,Af.creationData=Ob,Af.year=bf,Af.isLeapYear=ta,Af.weekYear=Qb,Af.isoWeekYear=Rb,Af.quarter=Af.quarters=Wb,Af.month=ha,Af.daysInMonth=ia,Af.week=Af.weeks=$b,Af.isoWeek=Af.isoWeeks=_b,Af.weeksInYear=Tb,Af.isoWeeksInYear=Sb,Af.date=nf,Af.day=Af.days=gc,Af.weekday=hc,Af.isoWeekday=ic,Af.dayOfYear=nc,Af.hour=Af.hours=vf,Af.minute=Af.minutes=wf,Af.second=Af.seconds=xf,Af.millisecond=Af.milliseconds=zf,Af.utcOffset=Ua,Af.utc=Wa,Af.local=Xa,Af.parseZone=Ya,Af.hasAlignedHourOffset=Za,Af.isDST=$a,Af.isDSTShifted=_a,Af.isLocal=ab,Af.isUtcOffset=bb,Af.isUtc=cb,Af.isUTC=cb,Af.zoneAbbr=vc,Af.zoneName=wc,Af.dates=u("dates accessor is deprecated. Use date instead.",nf),Af.months=u("months accessor is deprecated. Use month instead",ha),Af.years=u("years accessor is deprecated. Use year instead",bf),Af.zone=u("moment().zone is deprecated, use moment().utcOffset instead. https://github.com/moment/moment/issues/1779",Va);var Bf=Af,Cf={sameDay:"[Today at] LT",nextDay:"[Tomorrow at] LT",nextWeek:"dddd [at] LT",lastDay:"[Yesterday at] LT",lastWeek:"[Last] dddd [at] LT",sameElse:"L"},Df={LTS:"h:mm:ss A",LT:"h:mm A",L:"MM/DD/YYYY",LL:"MMMM D, YYYY",LLL:"MMMM D, YYYY h:mm A",LLLL:"dddd, MMMM D, YYYY h:mm A"},Ef="Invalid date",Ff="%d",Gf=/\d{1,2}/,Hf={future:"in %s",past:"%s ago",s:"a few seconds",m:"a minute",mm:"%d minutes",h:"an hour",hh:"%d hours",d:"a day",dd:"%d days",M:"a month",MM:"%d months",y:"a year",yy:"%d years"},If=A.prototype;If._calendar=Cf,If.calendar=zc,If._longDateFormat=Df,If.longDateFormat=Ac,If._invalidDate=Ef,If.invalidDate=Bc,If._ordinal=Ff,If.ordinal=Cc,If._ordinalParse=Gf,If.preparse=Dc,If.postformat=Dc,If._relativeTime=Hf,If.relativeTime=Ec,If.pastFuture=Fc,If.set=y,If.months=ca,If._months=Te,If.monthsShort=da,If._monthsShort=Ue,If.monthsParse=fa,If._monthsRegex=We,If.monthsRegex=ka,If._monthsShortRegex=Ve,If.monthsShortRegex=ja,If.week=Xb,If._week=mf,If.firstDayOfYear=Zb,If.firstDayOfWeek=Yb,If.weekdays=bc,If._weekdays=of,If.weekdaysMin=dc,If._weekdaysMin=qf,If.weekdaysShort=cc,If._weekdaysShort=pf,If.weekdaysParse=fc,If._weekdaysRegex=rf,If.weekdaysRegex=jc,If._weekdaysShortRegex=sf,If.weekdaysShortRegex=kc,If._weekdaysMinRegex=tf,If.weekdaysMinRegex=lc,If.isPM=sc,If._meridiemParse=uf,If.meridiem=tc,E("en",{ordinalParse:/\d{1,2}(th|st|nd|rd)/,ordinal:function ordinal(a){var b=a%10,c=1===r(a%100/10)?"th":1===b?"st":2===b?"nd":3===b?"rd":"th";return a+c;}}),a.lang=u("moment.lang is deprecated. Use moment.locale instead.",E),a.langData=u("moment.langData is deprecated. Use moment.localeData instead.",H);var Jf=Math.abs,Kf=Yc("ms"),Lf=Yc("s"),Mf=Yc("m"),Nf=Yc("h"),Of=Yc("d"),Pf=Yc("w"),Qf=Yc("M"),Rf=Yc("y"),Sf=$c("milliseconds"),Tf=$c("seconds"),Uf=$c("minutes"),Vf=$c("hours"),Wf=$c("days"),Xf=$c("months"),Yf=$c("years"),Zf=Math.round,$f={s:45,m:45,h:22,d:26,M:11},_f=Math.abs,ag=Oa.prototype;ag.abs=Oc,ag.add=Qc,ag.subtract=Rc,ag.as=Wc,ag.asMilliseconds=Kf,ag.asSeconds=Lf,ag.asMinutes=Mf,ag.asHours=Nf,ag.asDays=Of,ag.asWeeks=Pf,ag.asMonths=Qf,ag.asYears=Rf,ag.valueOf=Xc,ag._bubble=Tc,ag.get=Zc,ag.milliseconds=Sf,ag.seconds=Tf,ag.minutes=Uf,ag.hours=Vf,ag.days=Wf,ag.weeks=_c,ag.months=Xf,ag.years=Yf,ag.humanize=dd,ag.toISOString=ed,ag.toString=ed,ag.toJSON=ed,ag.locale=Bb,ag.localeData=Cb,ag.toIsoString=u("toIsoString() is deprecated. Please use toISOString() instead (notice the capitals)",ed),ag.lang=lf,R("X",0,0,"unix"),R("x",0,0,"valueOf"),W("x",Ce),W("X",Fe),$("X",function(a,b,c){c._d=new Date(1e3*parseFloat(a,10));}),$("x",function(a,b,c){c._d=new Date(r(a));}), //! moment.js
	//! version : 2.13.0
	//! authors : Tim Wood, Iskren Chernev, Moment.js contributors
	//! license : MIT
	//! momentjs.com
	a.version="2.13.0",b(Ka),a.fn=Bf,a.min=Ma,a.max=Na,a.now=ef,a.utc=h,a.unix=xc,a.months=Jc,a.isDate=d,a.locale=E,a.invalid=l,a.duration=db,a.isMoment=p,a.weekdays=Lc,a.parseZone=yc,a.localeData=H,a.isDuration=Pa,a.monthsShort=Kc,a.weekdaysMin=Nc,a.defineLocale=F,a.updateLocale=G,a.locales=I,a.weekdaysShort=Mc,a.normalizeUnits=K,a.relativeTimeThreshold=cd,a.prototype=Bf;var bg=a,cg=(bg.defineLocale("af",{months:"Januarie_Februarie_Maart_April_Mei_Junie_Julie_Augustus_September_Oktober_November_Desember".split("_"),monthsShort:"Jan_Feb_Mar_Apr_Mei_Jun_Jul_Aug_Sep_Okt_Nov_Des".split("_"),weekdays:"Sondag_Maandag_Dinsdag_Woensdag_Donderdag_Vrydag_Saterdag".split("_"),weekdaysShort:"Son_Maa_Din_Woe_Don_Vry_Sat".split("_"),weekdaysMin:"So_Ma_Di_Wo_Do_Vr_Sa".split("_"),meridiemParse:/vm|nm/i,isPM:function isPM(a){return (/^nm$/i.test(a));},meridiem:function meridiem(a,b,c){return 12>a?c?"vm":"VM":c?"nm":"NM";},longDateFormat:{LT:"HH:mm",LTS:"HH:mm:ss",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY HH:mm",LLLL:"dddd, D MMMM YYYY HH:mm"},calendar:{sameDay:"[Vandag om] LT",nextDay:"[Môre om] LT",nextWeek:"dddd [om] LT",lastDay:"[Gister om] LT",lastWeek:"[Laas] dddd [om] LT",sameElse:"L"},relativeTime:{future:"oor %s",past:"%s gelede",s:"'n paar sekondes",m:"'n minuut",mm:"%d minute",h:"'n uur",hh:"%d ure",d:"'n dag",dd:"%d dae",M:"'n maand",MM:"%d maande",y:"'n jaar",yy:"%d jaar"},ordinalParse:/\d{1,2}(ste|de)/,ordinal:function ordinal(a){return a+(1===a||8===a||a>=20?"ste":"de");},week:{dow:1,doy:4}}),bg.defineLocale("ar-ma",{months:"يناير_فبراير_مارس_أبريل_ماي_يونيو_يوليوز_غشت_شتنبر_أكتوبر_نونبر_دجنبر".split("_"),monthsShort:"يناير_فبراير_مارس_أبريل_ماي_يونيو_يوليوز_غشت_شتنبر_أكتوبر_نونبر_دجنبر".split("_"),weekdays:"الأحد_الإتنين_الثلاثاء_الأربعاء_الخميس_الجمعة_السبت".split("_"),weekdaysShort:"احد_اتنين_ثلاثاء_اربعاء_خميس_جمعة_سبت".split("_"),weekdaysMin:"ح_ن_ث_ر_خ_ج_س".split("_"),weekdaysParseExact:!0,longDateFormat:{LT:"HH:mm",LTS:"HH:mm:ss",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY HH:mm",LLLL:"dddd D MMMM YYYY HH:mm"},calendar:{sameDay:"[اليوم على الساعة] LT",nextDay:"[غدا على الساعة] LT",nextWeek:"dddd [على الساعة] LT",lastDay:"[أمس على الساعة] LT",lastWeek:"dddd [على الساعة] LT",sameElse:"L"},relativeTime:{future:"في %s",past:"منذ %s",s:"ثوان",m:"دقيقة",mm:"%d دقائق",h:"ساعة",hh:"%d ساعات",d:"يوم",dd:"%d أيام",M:"شهر",MM:"%d أشهر",y:"سنة",yy:"%d سنوات"},week:{dow:6,doy:12}}),{1:"١",2:"٢",3:"٣",4:"٤",5:"٥",6:"٦",7:"٧",8:"٨",9:"٩",0:"٠"}),dg={"١":"1","٢":"2","٣":"3","٤":"4","٥":"5","٦":"6","٧":"7","٨":"8","٩":"9","٠":"0"},eg=(bg.defineLocale("ar-sa",{months:"يناير_فبراير_مارس_أبريل_مايو_يونيو_يوليو_أغسطس_سبتمبر_أكتوبر_نوفمبر_ديسمبر".split("_"),monthsShort:"يناير_فبراير_مارس_أبريل_مايو_يونيو_يوليو_أغسطس_سبتمبر_أكتوبر_نوفمبر_ديسمبر".split("_"),weekdays:"الأحد_الإثنين_الثلاثاء_الأربعاء_الخميس_الجمعة_السبت".split("_"),weekdaysShort:"أحد_إثنين_ثلاثاء_أربعاء_خميس_جمعة_سبت".split("_"),weekdaysMin:"ح_ن_ث_ر_خ_ج_س".split("_"),weekdaysParseExact:!0,longDateFormat:{LT:"HH:mm",LTS:"HH:mm:ss",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY HH:mm",LLLL:"dddd D MMMM YYYY HH:mm"},meridiemParse:/ص|م/,isPM:function isPM(a){return "م"===a;},meridiem:function meridiem(a,b,c){return 12>a?"ص":"م";},calendar:{sameDay:"[اليوم على الساعة] LT",nextDay:"[غدا على الساعة] LT",nextWeek:"dddd [على الساعة] LT",lastDay:"[أمس على الساعة] LT",lastWeek:"dddd [على الساعة] LT",sameElse:"L"},relativeTime:{future:"في %s",past:"منذ %s",s:"ثوان",m:"دقيقة",mm:"%d دقائق",h:"ساعة",hh:"%d ساعات",d:"يوم",dd:"%d أيام",M:"شهر",MM:"%d أشهر",y:"سنة",yy:"%d سنوات"},preparse:function preparse(a){return a.replace(/[١٢٣٤٥٦٧٨٩٠]/g,function(a){return dg[a];}).replace(/،/g,",");},postformat:function postformat(a){return a.replace(/\d/g,function(a){return cg[a];}).replace(/,/g,"،");},week:{dow:6,doy:12}}),bg.defineLocale("ar-tn",{months:"جانفي_فيفري_مارس_أفريل_ماي_جوان_جويلية_أوت_سبتمبر_أكتوبر_نوفمبر_ديسمبر".split("_"),monthsShort:"جانفي_فيفري_مارس_أفريل_ماي_جوان_جويلية_أوت_سبتمبر_أكتوبر_نوفمبر_ديسمبر".split("_"),weekdays:"الأحد_الإثنين_الثلاثاء_الأربعاء_الخميس_الجمعة_السبت".split("_"),weekdaysShort:"أحد_إثنين_ثلاثاء_أربعاء_خميس_جمعة_سبت".split("_"),weekdaysMin:"ح_ن_ث_ر_خ_ج_س".split("_"),weekdaysParseExact:!0,longDateFormat:{LT:"HH:mm",LTS:"HH:mm:ss",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY HH:mm",LLLL:"dddd D MMMM YYYY HH:mm"},calendar:{sameDay:"[اليوم على الساعة] LT",nextDay:"[غدا على الساعة] LT",nextWeek:"dddd [على الساعة] LT",lastDay:"[أمس على الساعة] LT",lastWeek:"dddd [على الساعة] LT",sameElse:"L"},relativeTime:{future:"في %s",past:"منذ %s",s:"ثوان",m:"دقيقة",mm:"%d دقائق",h:"ساعة",hh:"%d ساعات",d:"يوم",dd:"%d أيام",M:"شهر",MM:"%d أشهر",y:"سنة",yy:"%d سنوات"},week:{dow:1,doy:4}}),{1:"١",2:"٢",3:"٣",4:"٤",5:"٥",6:"٦",7:"٧",8:"٨",9:"٩",0:"٠"}),fg={"١":"1","٢":"2","٣":"3","٤":"4","٥":"5","٦":"6","٧":"7","٨":"8","٩":"9","٠":"0"},gg=function gg(a){return 0===a?0:1===a?1:2===a?2:a%100>=3&&10>=a%100?3:a%100>=11?4:5;},hg={s:["أقل من ثانية","ثانية واحدة",["ثانيتان","ثانيتين"],"%d ثوان","%d ثانية","%d ثانية"],m:["أقل من دقيقة","دقيقة واحدة",["دقيقتان","دقيقتين"],"%d دقائق","%d دقيقة","%d دقيقة"],h:["أقل من ساعة","ساعة واحدة",["ساعتان","ساعتين"],"%d ساعات","%d ساعة","%d ساعة"],d:["أقل من يوم","يوم واحد",["يومان","يومين"],"%d أيام","%d يومًا","%d يوم"],M:["أقل من شهر","شهر واحد",["شهران","شهرين"],"%d أشهر","%d شهرا","%d شهر"],y:["أقل من عام","عام واحد",["عامان","عامين"],"%d أعوام","%d عامًا","%d عام"]},ig=function ig(a){return function(b,c,d,e){var f=gg(b),g=hg[a][gg(b)];return 2===f&&(g=g[c?0:1]),g.replace(/%d/i,b);};},jg=["كانون الثاني يناير","شباط فبراير","آذار مارس","نيسان أبريل","أيار مايو","حزيران يونيو","تموز يوليو","آب أغسطس","أيلول سبتمبر","تشرين الأول أكتوبر","تشرين الثاني نوفمبر","كانون الأول ديسمبر"],kg=(bg.defineLocale("ar",{months:jg,monthsShort:jg,weekdays:"الأحد_الإثنين_الثلاثاء_الأربعاء_الخميس_الجمعة_السبت".split("_"),weekdaysShort:"أحد_إثنين_ثلاثاء_أربعاء_خميس_جمعة_سبت".split("_"),weekdaysMin:"ح_ن_ث_ر_خ_ج_س".split("_"),weekdaysParseExact:!0,longDateFormat:{LT:"HH:mm",LTS:"HH:mm:ss",L:"D/‏M/‏YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY HH:mm",LLLL:"dddd D MMMM YYYY HH:mm"},meridiemParse:/ص|م/,isPM:function isPM(a){return "م"===a;},meridiem:function meridiem(a,b,c){return 12>a?"ص":"م";},calendar:{sameDay:"[اليوم عند الساعة] LT",nextDay:"[غدًا عند الساعة] LT",nextWeek:"dddd [عند الساعة] LT",lastDay:"[أمس عند الساعة] LT",lastWeek:"dddd [عند الساعة] LT",sameElse:"L"},relativeTime:{future:"بعد %s",past:"منذ %s",s:ig("s"),m:ig("m"),mm:ig("m"),h:ig("h"),hh:ig("h"),d:ig("d"),dd:ig("d"),M:ig("M"),MM:ig("M"),y:ig("y"),yy:ig("y")},preparse:function preparse(a){return a.replace(/\u200f/g,"").replace(/[١٢٣٤٥٦٧٨٩٠]/g,function(a){return fg[a];}).replace(/،/g,",");},postformat:function postformat(a){return a.replace(/\d/g,function(a){return eg[a];}).replace(/,/g,"،");},week:{dow:6,doy:12}}),{1:"-inci",5:"-inci",8:"-inci",70:"-inci",80:"-inci",2:"-nci",7:"-nci",20:"-nci",50:"-nci",3:"-üncü",4:"-üncü",100:"-üncü",6:"-ncı",9:"-uncu",10:"-uncu",30:"-uncu",60:"-ıncı",90:"-ıncı"}),lg=(bg.defineLocale("az",{months:"yanvar_fevral_mart_aprel_may_iyun_iyul_avqust_sentyabr_oktyabr_noyabr_dekabr".split("_"),monthsShort:"yan_fev_mar_apr_may_iyn_iyl_avq_sen_okt_noy_dek".split("_"),weekdays:"Bazar_Bazar ertəsi_Çərşənbə axşamı_Çərşənbə_Cümə axşamı_Cümə_Şənbə".split("_"),weekdaysShort:"Baz_BzE_ÇAx_Çər_CAx_Cüm_Şən".split("_"),weekdaysMin:"Bz_BE_ÇA_Çə_CA_Cü_Şə".split("_"),weekdaysParseExact:!0,longDateFormat:{LT:"HH:mm",LTS:"HH:mm:ss",L:"DD.MM.YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY HH:mm",LLLL:"dddd, D MMMM YYYY HH:mm"},calendar:{sameDay:"[bugün saat] LT",nextDay:"[sabah saat] LT",nextWeek:"[gələn həftə] dddd [saat] LT",lastDay:"[dünən] LT",lastWeek:"[keçən həftə] dddd [saat] LT",sameElse:"L"},relativeTime:{future:"%s sonra",past:"%s əvvəl",s:"birneçə saniyyə",m:"bir dəqiqə",mm:"%d dəqiqə",h:"bir saat",hh:"%d saat",d:"bir gün",dd:"%d gün",M:"bir ay",MM:"%d ay",y:"bir il",yy:"%d il"},meridiemParse:/gecə|səhər|gündüz|axşam/,isPM:function isPM(a){return (/^(gündüz|axşam)$/.test(a));},meridiem:function meridiem(a,b,c){return 4>a?"gecə":12>a?"səhər":17>a?"gündüz":"axşam";},ordinalParse:/\d{1,2}-(ıncı|inci|nci|üncü|ncı|uncu)/,ordinal:function ordinal(a){if(0===a)return a+"-ıncı";var b=a%10,c=a%100-b,d=a>=100?100:null;return a+(kg[b]||kg[c]||kg[d]);},week:{dow:1,doy:7}}),bg.defineLocale("be",{months:{format:"студзеня_лютага_сакавіка_красавіка_траўня_чэрвеня_ліпеня_жніўня_верасня_кастрычніка_лістапада_снежня".split("_"),standalone:"студзень_люты_сакавік_красавік_травень_чэрвень_ліпень_жнівень_верасень_кастрычнік_лістапад_снежань".split("_")},monthsShort:"студ_лют_сак_крас_трав_чэрв_ліп_жнів_вер_каст_ліст_снеж".split("_"),weekdays:{format:"нядзелю_панядзелак_аўторак_сераду_чацвер_пятніцу_суботу".split("_"),standalone:"нядзеля_панядзелак_аўторак_серада_чацвер_пятніца_субота".split("_"),isFormat:/\[ ?[Вв] ?(?:мінулую|наступную)? ?\] ?dddd/},weekdaysShort:"нд_пн_ат_ср_чц_пт_сб".split("_"),weekdaysMin:"нд_пн_ат_ср_чц_пт_сб".split("_"),longDateFormat:{LT:"HH:mm",LTS:"HH:mm:ss",L:"DD.MM.YYYY",LL:"D MMMM YYYY г.",LLL:"D MMMM YYYY г., HH:mm",LLLL:"dddd, D MMMM YYYY г., HH:mm"},calendar:{sameDay:"[Сёння ў] LT",nextDay:"[Заўтра ў] LT",lastDay:"[Учора ў] LT",nextWeek:function nextWeek(){return "[У] dddd [ў] LT";},lastWeek:function lastWeek(){switch(this.day()){case 0:case 3:case 5:case 6:return "[У мінулую] dddd [ў] LT";case 1:case 2:case 4:return "[У мінулы] dddd [ў] LT";}},sameElse:"L"},relativeTime:{future:"праз %s",past:"%s таму",s:"некалькі секунд",m:gd,mm:gd,h:gd,hh:gd,d:"дзень",dd:gd,M:"месяц",MM:gd,y:"год",yy:gd},meridiemParse:/ночы|раніцы|дня|вечара/,isPM:function isPM(a){return (/^(дня|вечара)$/.test(a));},meridiem:function meridiem(a,b,c){return 4>a?"ночы":12>a?"раніцы":17>a?"дня":"вечара";},ordinalParse:/\d{1,2}-(і|ы|га)/,ordinal:function ordinal(a,b){switch(b){case "M":case "d":case "DDD":case "w":case "W":return a%10!==2&&a%10!==3||a%100===12||a%100===13?a+"-ы":a+"-і";case "D":return a+"-га";default:return a;}},week:{dow:1,doy:7}}),bg.defineLocale("bg",{months:"януари_февруари_март_април_май_юни_юли_август_септември_октомври_ноември_декември".split("_"),monthsShort:"янр_фев_мар_апр_май_юни_юли_авг_сеп_окт_ное_дек".split("_"),weekdays:"неделя_понеделник_вторник_сряда_четвъртък_петък_събота".split("_"),weekdaysShort:"нед_пон_вто_сря_чет_пет_съб".split("_"),weekdaysMin:"нд_пн_вт_ср_чт_пт_сб".split("_"),longDateFormat:{LT:"H:mm",LTS:"H:mm:ss",L:"D.MM.YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY H:mm",LLLL:"dddd, D MMMM YYYY H:mm"},calendar:{sameDay:"[Днес в] LT",nextDay:"[Утре в] LT",nextWeek:"dddd [в] LT",lastDay:"[Вчера в] LT",lastWeek:function lastWeek(){switch(this.day()){case 0:case 3:case 6:return "[В изминалата] dddd [в] LT";case 1:case 2:case 4:case 5:return "[В изминалия] dddd [в] LT";}},sameElse:"L"},relativeTime:{future:"след %s",past:"преди %s",s:"няколко секунди",m:"минута",mm:"%d минути",h:"час",hh:"%d часа",d:"ден",dd:"%d дни",M:"месец",MM:"%d месеца",y:"година",yy:"%d години"},ordinalParse:/\d{1,2}-(ев|ен|ти|ви|ри|ми)/,ordinal:function ordinal(a){var b=a%10,c=a%100;return 0===a?a+"-ев":0===c?a+"-ен":c>10&&20>c?a+"-ти":1===b?a+"-ви":2===b?a+"-ри":7===b||8===b?a+"-ми":a+"-ти";},week:{dow:1,doy:7}}),{1:"১",2:"২",3:"৩",4:"৪",5:"৫",6:"৬",7:"৭",8:"৮",9:"৯",0:"০"}),mg={"১":"1","২":"2","৩":"3","৪":"4","৫":"5","৬":"6","৭":"7","৮":"8","৯":"9","০":"0"},ng=(bg.defineLocale("bn",{months:"জানুয়ারী_ফেবুয়ারী_মার্চ_এপ্রিল_মে_জুন_জুলাই_অগাস্ট_সেপ্টেম্বর_অক্টোবর_নভেম্বর_ডিসেম্বর".split("_"),monthsShort:"জানু_ফেব_মার্চ_এপর_মে_জুন_জুল_অগ_সেপ্ট_অক্টো_নভ_ডিসেম্".split("_"),weekdays:"রবিবার_সোমবার_মঙ্গলবার_বুধবার_বৃহস্পত্তিবার_শুক্রবার_শনিবার".split("_"),weekdaysShort:"রবি_সোম_মঙ্গল_বুধ_বৃহস্পত্তি_শুক্র_শনি".split("_"),weekdaysMin:"রব_সম_মঙ্গ_বু_ব্রিহ_শু_শনি".split("_"),longDateFormat:{LT:"A h:mm সময়",LTS:"A h:mm:ss সময়",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY, A h:mm সময়",LLLL:"dddd, D MMMM YYYY, A h:mm সময়"},calendar:{sameDay:"[আজ] LT",nextDay:"[আগামীকাল] LT",nextWeek:"dddd, LT",lastDay:"[গতকাল] LT",lastWeek:"[গত] dddd, LT",sameElse:"L"},relativeTime:{future:"%s পরে",past:"%s আগে",s:"কয়েক সেকেন্ড",m:"এক মিনিট",mm:"%d মিনিট",h:"এক ঘন্টা",hh:"%d ঘন্টা",d:"এক দিন",dd:"%d দিন",M:"এক মাস",MM:"%d মাস",y:"এক বছর",yy:"%d বছর"},preparse:function preparse(a){return a.replace(/[১২৩৪৫৬৭৮৯০]/g,function(a){return mg[a];});},postformat:function postformat(a){return a.replace(/\d/g,function(a){return lg[a];});},meridiemParse:/রাত|সকাল|দুপুর|বিকাল|রাত/,meridiemHour:function meridiemHour(a,b){return 12===a&&(a=0),"রাত"===b&&a>=4||"দুপুর"===b&&5>a||"বিকাল"===b?a+12:a;},meridiem:function meridiem(a,b,c){return 4>a?"রাত":10>a?"সকাল":17>a?"দুপুর":20>a?"বিকাল":"রাত";},week:{dow:0,doy:6}}),{1:"༡",2:"༢",3:"༣",4:"༤",5:"༥",6:"༦",7:"༧",8:"༨",9:"༩",0:"༠"}),og={"༡":"1","༢":"2","༣":"3","༤":"4","༥":"5","༦":"6","༧":"7","༨":"8","༩":"9","༠":"0"},pg=(bg.defineLocale("bo",{months:"ཟླ་བ་དང་པོ_ཟླ་བ་གཉིས་པ_ཟླ་བ་གསུམ་པ_ཟླ་བ་བཞི་པ_ཟླ་བ་ལྔ་པ_ཟླ་བ་དྲུག་པ_ཟླ་བ་བདུན་པ_ཟླ་བ་བརྒྱད་པ_ཟླ་བ་དགུ་པ_ཟླ་བ་བཅུ་པ_ཟླ་བ་བཅུ་གཅིག་པ_ཟླ་བ་བཅུ་གཉིས་པ".split("_"),monthsShort:"ཟླ་བ་དང་པོ_ཟླ་བ་གཉིས་པ_ཟླ་བ་གསུམ་པ_ཟླ་བ་བཞི་པ_ཟླ་བ་ལྔ་པ_ཟླ་བ་དྲུག་པ_ཟླ་བ་བདུན་པ_ཟླ་བ་བརྒྱད་པ_ཟླ་བ་དགུ་པ_ཟླ་བ་བཅུ་པ_ཟླ་བ་བཅུ་གཅིག་པ_ཟླ་བ་བཅུ་གཉིས་པ".split("_"),weekdays:"གཟའ་ཉི་མ་_གཟའ་ཟླ་བ་_གཟའ་མིག་དམར་_གཟའ་ལྷག་པ་_གཟའ་ཕུར་བུ_གཟའ་པ་སངས་_གཟའ་སྤེན་པ་".split("_"),weekdaysShort:"ཉི་མ་_ཟླ་བ་_མིག་དམར་_ལྷག་པ་_ཕུར་བུ_པ་སངས་_སྤེན་པ་".split("_"),weekdaysMin:"ཉི་མ་_ཟླ་བ་_མིག་དམར་_ལྷག་པ་_ཕུར་བུ_པ་སངས་_སྤེན་པ་".split("_"),longDateFormat:{LT:"A h:mm",LTS:"A h:mm:ss",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY, A h:mm",LLLL:"dddd, D MMMM YYYY, A h:mm"},calendar:{sameDay:"[དི་རིང] LT",nextDay:"[སང་ཉིན] LT",nextWeek:"[བདུན་ཕྲག་རྗེས་མ], LT",lastDay:"[ཁ་སང] LT",lastWeek:"[བདུན་ཕྲག་མཐའ་མ] dddd, LT",sameElse:"L"},relativeTime:{future:"%s ལ་",past:"%s སྔན་ལ",s:"ལམ་སང",m:"སྐར་མ་གཅིག",mm:"%d སྐར་མ",h:"ཆུ་ཚོད་གཅིག",hh:"%d ཆུ་ཚོད",d:"ཉིན་གཅིག",dd:"%d ཉིན་",M:"ཟླ་བ་གཅིག",MM:"%d ཟླ་བ",y:"ལོ་གཅིག",yy:"%d ལོ"},preparse:function preparse(a){return a.replace(/[༡༢༣༤༥༦༧༨༩༠]/g,function(a){return og[a];});},postformat:function postformat(a){return a.replace(/\d/g,function(a){return ng[a];});},meridiemParse:/མཚན་མོ|ཞོགས་ཀས|ཉིན་གུང|དགོང་དག|མཚན་མོ/,meridiemHour:function meridiemHour(a,b){return 12===a&&(a=0),"མཚན་མོ"===b&&a>=4||"ཉིན་གུང"===b&&5>a||"དགོང་དག"===b?a+12:a;},meridiem:function meridiem(a,b,c){return 4>a?"མཚན་མོ":10>a?"ཞོགས་ཀས":17>a?"ཉིན་གུང":20>a?"དགོང་དག":"མཚན་མོ";},week:{dow:0,doy:6}}),bg.defineLocale("br",{months:"Genver_C'hwevrer_Meurzh_Ebrel_Mae_Mezheven_Gouere_Eost_Gwengolo_Here_Du_Kerzu".split("_"),monthsShort:"Gen_C'hwe_Meu_Ebr_Mae_Eve_Gou_Eos_Gwe_Her_Du_Ker".split("_"),weekdays:"Sul_Lun_Meurzh_Merc'her_Yaou_Gwener_Sadorn".split("_"),weekdaysShort:"Sul_Lun_Meu_Mer_Yao_Gwe_Sad".split("_"),weekdaysMin:"Su_Lu_Me_Mer_Ya_Gw_Sa".split("_"),weekdaysParseExact:!0,longDateFormat:{LT:"h[e]mm A",LTS:"h[e]mm:ss A",L:"DD/MM/YYYY",LL:"D [a viz] MMMM YYYY",LLL:"D [a viz] MMMM YYYY h[e]mm A",LLLL:"dddd, D [a viz] MMMM YYYY h[e]mm A"},calendar:{sameDay:"[Hiziv da] LT",nextDay:"[Warc'hoazh da] LT",nextWeek:"dddd [da] LT",lastDay:"[Dec'h da] LT",lastWeek:"dddd [paset da] LT",sameElse:"L"},relativeTime:{future:"a-benn %s",past:"%s 'zo",s:"un nebeud segondennoù",m:"ur vunutenn",mm:hd,h:"un eur",hh:"%d eur",d:"un devezh",dd:hd,M:"ur miz",MM:hd,y:"ur bloaz",yy:id},ordinalParse:/\d{1,2}(añ|vet)/,ordinal:function ordinal(a){var b=1===a?"añ":"vet";return a+b;},week:{dow:1,doy:4}}),bg.defineLocale("bs",{months:"januar_februar_mart_april_maj_juni_juli_august_septembar_oktobar_novembar_decembar".split("_"),monthsShort:"jan._feb._mar._apr._maj._jun._jul._aug._sep._okt._nov._dec.".split("_"),monthsParseExact:!0,weekdays:"nedjelja_ponedjeljak_utorak_srijeda_četvrtak_petak_subota".split("_"),weekdaysShort:"ned._pon._uto._sri._čet._pet._sub.".split("_"),weekdaysMin:"ne_po_ut_sr_če_pe_su".split("_"),weekdaysParseExact:!0,longDateFormat:{LT:"H:mm",LTS:"H:mm:ss",L:"DD. MM. YYYY",LL:"D. MMMM YYYY",LLL:"D. MMMM YYYY H:mm",LLLL:"dddd, D. MMMM YYYY H:mm"},calendar:{sameDay:"[danas u] LT",nextDay:"[sutra u] LT",nextWeek:function nextWeek(){switch(this.day()){case 0:return "[u] [nedjelju] [u] LT";case 3:return "[u] [srijedu] [u] LT";case 6:return "[u] [subotu] [u] LT";case 1:case 2:case 4:case 5:return "[u] dddd [u] LT";}},lastDay:"[jučer u] LT",lastWeek:function lastWeek(){switch(this.day()){case 0:case 3:return "[prošlu] dddd [u] LT";case 6:return "[prošle] [subote] [u] LT";case 1:case 2:case 4:case 5:return "[prošli] dddd [u] LT";}},sameElse:"L"},relativeTime:{future:"za %s",past:"prije %s",s:"par sekundi",m:md,mm:md,h:md,hh:md,d:"dan",dd:md,M:"mjesec",MM:md,y:"godinu",yy:md},ordinalParse:/\d{1,2}\./,ordinal:"%d.",week:{dow:1,doy:7}}),bg.defineLocale("ca",{months:"gener_febrer_març_abril_maig_juny_juliol_agost_setembre_octubre_novembre_desembre".split("_"),monthsShort:"gen._febr._mar._abr._mai._jun._jul._ag._set._oct._nov._des.".split("_"),monthsParseExact:!0,weekdays:"diumenge_dilluns_dimarts_dimecres_dijous_divendres_dissabte".split("_"),weekdaysShort:"dg._dl._dt._dc._dj._dv._ds.".split("_"),weekdaysMin:"Dg_Dl_Dt_Dc_Dj_Dv_Ds".split("_"),weekdaysParseExact:!0,longDateFormat:{LT:"H:mm",LTS:"H:mm:ss",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY H:mm",LLLL:"dddd D MMMM YYYY H:mm"},calendar:{sameDay:function sameDay(){return "[avui a "+(1!==this.hours()?"les":"la")+"] LT";},nextDay:function nextDay(){return "[demà a "+(1!==this.hours()?"les":"la")+"] LT";},nextWeek:function nextWeek(){return "dddd [a "+(1!==this.hours()?"les":"la")+"] LT";},lastDay:function lastDay(){return "[ahir a "+(1!==this.hours()?"les":"la")+"] LT";},lastWeek:function lastWeek(){return "[el] dddd [passat a "+(1!==this.hours()?"les":"la")+"] LT";},sameElse:"L"},relativeTime:{future:"en %s",past:"fa %s",s:"uns segons",m:"un minut",mm:"%d minuts",h:"una hora",hh:"%d hores",d:"un dia",dd:"%d dies",M:"un mes",MM:"%d mesos",y:"un any",yy:"%d anys"},ordinalParse:/\d{1,2}(r|n|t|è|a)/,ordinal:function ordinal(a,b){var c=1===a?"r":2===a?"n":3===a?"r":4===a?"t":"è";return ("w"===b||"W"===b)&&(c="a"),a+c;},week:{dow:1,doy:4}}),"leden_únor_březen_duben_květen_červen_červenec_srpen_září_říjen_listopad_prosinec".split("_")),qg="led_úno_bře_dub_kvě_čvn_čvc_srp_zář_říj_lis_pro".split("_"),rg=(bg.defineLocale("cs",{months:pg,monthsShort:qg,monthsParse:function(a,b){var c,d=[];for(c=0;12>c;c++){d[c]=new RegExp("^"+a[c]+"$|^"+b[c]+"$","i");}return d;}(pg,qg),shortMonthsParse:function(a){var b,c=[];for(b=0;12>b;b++){c[b]=new RegExp("^"+a[b]+"$","i");}return c;}(qg),longMonthsParse:function(a){var b,c=[];for(b=0;12>b;b++){c[b]=new RegExp("^"+a[b]+"$","i");}return c;}(pg),weekdays:"neděle_pondělí_úterý_středa_čtvrtek_pátek_sobota".split("_"),weekdaysShort:"ne_po_út_st_čt_pá_so".split("_"),weekdaysMin:"ne_po_út_st_čt_pá_so".split("_"),longDateFormat:{LT:"H:mm",LTS:"H:mm:ss",L:"DD.MM.YYYY",LL:"D. MMMM YYYY",LLL:"D. MMMM YYYY H:mm",LLLL:"dddd D. MMMM YYYY H:mm"},calendar:{sameDay:"[dnes v] LT",nextDay:"[zítra v] LT",nextWeek:function nextWeek(){switch(this.day()){case 0:return "[v neděli v] LT";case 1:case 2:return "[v] dddd [v] LT";case 3:return "[ve středu v] LT";case 4:return "[ve čtvrtek v] LT";case 5:return "[v pátek v] LT";case 6:return "[v sobotu v] LT";}},lastDay:"[včera v] LT",lastWeek:function lastWeek(){switch(this.day()){case 0:return "[minulou neděli v] LT";case 1:case 2:return "[minulé] dddd [v] LT";case 3:return "[minulou středu v] LT";case 4:case 5:return "[minulý] dddd [v] LT";case 6:return "[minulou sobotu v] LT";}},sameElse:"L"},relativeTime:{future:"za %s",past:"před %s",s:od,m:od,mm:od,h:od,hh:od,d:od,dd:od,M:od,MM:od,y:od,yy:od},ordinalParse:/\d{1,2}\./,ordinal:"%d.",week:{dow:1,doy:4}}),bg.defineLocale("cv",{months:"кӑрлач_нарӑс_пуш_ака_май_ҫӗртме_утӑ_ҫурла_авӑн_юпа_чӳк_раштав".split("_"),monthsShort:"кӑр_нар_пуш_ака_май_ҫӗр_утӑ_ҫур_авн_юпа_чӳк_раш".split("_"),weekdays:"вырсарникун_тунтикун_ытларикун_юнкун_кӗҫнерникун_эрнекун_шӑматкун".split("_"),weekdaysShort:"выр_тун_ытл_юн_кӗҫ_эрн_шӑм".split("_"),weekdaysMin:"вр_тн_ыт_юн_кҫ_эр_шм".split("_"),longDateFormat:{LT:"HH:mm",LTS:"HH:mm:ss",L:"DD-MM-YYYY",LL:"YYYY [ҫулхи] MMMM [уйӑхӗн] D[-мӗшӗ]",LLL:"YYYY [ҫулхи] MMMM [уйӑхӗн] D[-мӗшӗ], HH:mm",LLLL:"dddd, YYYY [ҫулхи] MMMM [уйӑхӗн] D[-мӗшӗ], HH:mm"},calendar:{sameDay:"[Паян] LT [сехетре]",nextDay:"[Ыран] LT [сехетре]",lastDay:"[Ӗнер] LT [сехетре]",nextWeek:"[Ҫитес] dddd LT [сехетре]",lastWeek:"[Иртнӗ] dddd LT [сехетре]",sameElse:"L"},relativeTime:{future:function future(a){var b=/сехет$/i.exec(a)?"рен":/ҫул$/i.exec(a)?"тан":"ран";return a+b;},past:"%s каялла",s:"пӗр-ик ҫеккунт",m:"пӗр минут",mm:"%d минут",h:"пӗр сехет",hh:"%d сехет",d:"пӗр кун",dd:"%d кун",M:"пӗр уйӑх",MM:"%d уйӑх",y:"пӗр ҫул",yy:"%d ҫул"},ordinalParse:/\d{1,2}-мӗш/,ordinal:"%d-мӗш",week:{dow:1,doy:7}}),bg.defineLocale("cy",{months:"Ionawr_Chwefror_Mawrth_Ebrill_Mai_Mehefin_Gorffennaf_Awst_Medi_Hydref_Tachwedd_Rhagfyr".split("_"),monthsShort:"Ion_Chwe_Maw_Ebr_Mai_Meh_Gor_Aws_Med_Hyd_Tach_Rhag".split("_"),weekdays:"Dydd Sul_Dydd Llun_Dydd Mawrth_Dydd Mercher_Dydd Iau_Dydd Gwener_Dydd Sadwrn".split("_"),weekdaysShort:"Sul_Llun_Maw_Mer_Iau_Gwe_Sad".split("_"),weekdaysMin:"Su_Ll_Ma_Me_Ia_Gw_Sa".split("_"),weekdaysParseExact:!0,longDateFormat:{LT:"HH:mm",LTS:"HH:mm:ss",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY HH:mm",LLLL:"dddd, D MMMM YYYY HH:mm"},calendar:{sameDay:"[Heddiw am] LT",nextDay:"[Yfory am] LT",nextWeek:"dddd [am] LT",lastDay:"[Ddoe am] LT",lastWeek:"dddd [diwethaf am] LT",sameElse:"L"},relativeTime:{future:"mewn %s",past:"%s yn ôl",s:"ychydig eiliadau",m:"munud",mm:"%d munud",h:"awr",hh:"%d awr",d:"diwrnod",dd:"%d diwrnod",M:"mis",MM:"%d mis",y:"blwyddyn",yy:"%d flynedd"},ordinalParse:/\d{1,2}(fed|ain|af|il|ydd|ed|eg)/,ordinal:function ordinal(a){var b=a,c="",d=["","af","il","ydd","ydd","ed","ed","ed","fed","fed","fed","eg","fed","eg","eg","fed","eg","eg","fed","eg","fed"];return b>20?c=40===b||50===b||60===b||80===b||100===b?"fed":"ain":b>0&&(c=d[b]),a+c;},week:{dow:1,doy:4}}),bg.defineLocale("da",{months:"januar_februar_marts_april_maj_juni_juli_august_september_oktober_november_december".split("_"),monthsShort:"jan_feb_mar_apr_maj_jun_jul_aug_sep_okt_nov_dec".split("_"),weekdays:"søndag_mandag_tirsdag_onsdag_torsdag_fredag_lørdag".split("_"),weekdaysShort:"søn_man_tir_ons_tor_fre_lør".split("_"),weekdaysMin:"sø_ma_ti_on_to_fr_lø".split("_"),longDateFormat:{LT:"HH:mm",LTS:"HH:mm:ss",L:"DD/MM/YYYY",LL:"D. MMMM YYYY",LLL:"D. MMMM YYYY HH:mm",LLLL:"dddd [d.] D. MMMM YYYY HH:mm"},calendar:{sameDay:"[I dag kl.] LT",nextDay:"[I morgen kl.] LT",nextWeek:"dddd [kl.] LT",lastDay:"[I går kl.] LT",lastWeek:"[sidste] dddd [kl] LT",sameElse:"L"},relativeTime:{future:"om %s",past:"%s siden",s:"få sekunder",m:"et minut",mm:"%d minutter",h:"en time",hh:"%d timer",d:"en dag",dd:"%d dage",M:"en måned",MM:"%d måneder",y:"et år",yy:"%d år"},ordinalParse:/\d{1,2}\./,ordinal:"%d.",week:{dow:1,doy:4}}),bg.defineLocale("de-at",{months:"Jänner_Februar_März_April_Mai_Juni_Juli_August_September_Oktober_November_Dezember".split("_"),monthsShort:"Jän._Febr._Mrz._Apr._Mai_Jun._Jul._Aug._Sept._Okt._Nov._Dez.".split("_"),monthsParseExact:!0,weekdays:"Sonntag_Montag_Dienstag_Mittwoch_Donnerstag_Freitag_Samstag".split("_"),weekdaysShort:"So._Mo._Di._Mi._Do._Fr._Sa.".split("_"),weekdaysMin:"So_Mo_Di_Mi_Do_Fr_Sa".split("_"),weekdaysParseExact:!0,longDateFormat:{LT:"HH:mm",LTS:"HH:mm:ss",L:"DD.MM.YYYY",LL:"D. MMMM YYYY",LLL:"D. MMMM YYYY HH:mm",LLLL:"dddd, D. MMMM YYYY HH:mm"},calendar:{sameDay:"[heute um] LT [Uhr]",sameElse:"L",nextDay:"[morgen um] LT [Uhr]",nextWeek:"dddd [um] LT [Uhr]",lastDay:"[gestern um] LT [Uhr]",lastWeek:"[letzten] dddd [um] LT [Uhr]"},relativeTime:{future:"in %s",past:"vor %s",s:"ein paar Sekunden",m:pd,mm:"%d Minuten",h:pd,hh:"%d Stunden",d:pd,dd:pd,M:pd,MM:pd,y:pd,yy:pd},ordinalParse:/\d{1,2}\./,ordinal:"%d.",week:{dow:1,doy:4}}),bg.defineLocale("de",{months:"Januar_Februar_März_April_Mai_Juni_Juli_August_September_Oktober_November_Dezember".split("_"),monthsShort:"Jan._Febr._Mrz._Apr._Mai_Jun._Jul._Aug._Sept._Okt._Nov._Dez.".split("_"),monthsParseExact:!0,weekdays:"Sonntag_Montag_Dienstag_Mittwoch_Donnerstag_Freitag_Samstag".split("_"),weekdaysShort:"So._Mo._Di._Mi._Do._Fr._Sa.".split("_"),weekdaysMin:"So_Mo_Di_Mi_Do_Fr_Sa".split("_"),weekdaysParseExact:!0,longDateFormat:{LT:"HH:mm",LTS:"HH:mm:ss",L:"DD.MM.YYYY",LL:"D. MMMM YYYY",LLL:"D. MMMM YYYY HH:mm",LLLL:"dddd, D. MMMM YYYY HH:mm"},calendar:{sameDay:"[heute um] LT [Uhr]",sameElse:"L",nextDay:"[morgen um] LT [Uhr]",nextWeek:"dddd [um] LT [Uhr]",lastDay:"[gestern um] LT [Uhr]",lastWeek:"[letzten] dddd [um] LT [Uhr]"},relativeTime:{future:"in %s",past:"vor %s",s:"ein paar Sekunden",m:qd,mm:"%d Minuten",h:qd,hh:"%d Stunden",d:qd,dd:qd,M:qd,MM:qd,y:qd,yy:qd},ordinalParse:/\d{1,2}\./,ordinal:"%d.",week:{dow:1,doy:4}}),["ޖެނުއަރީ","ފެބްރުއަރީ","މާރިޗު","އޭޕްރީލު","މޭ","ޖޫން","ޖުލައި","އޯގަސްޓު","ސެޕްޓެމްބަރު","އޮކްޓޯބަރު","ނޮވެމްބަރު","ޑިސެމްބަރު"]),sg=["އާދިއްތަ","ހޯމަ","އަންގާރަ","ބުދަ","ބުރާސްފަތި","ހުކުރު","ހޮނިހިރު"],tg=(bg.defineLocale("dv",{months:rg,monthsShort:rg,weekdays:sg,weekdaysShort:sg,weekdaysMin:"އާދި_ހޯމަ_އަން_ބުދަ_ބުރާ_ހުކު_ހޮނި".split("_"),longDateFormat:{LT:"HH:mm",LTS:"HH:mm:ss",L:"D/M/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY HH:mm",LLLL:"dddd D MMMM YYYY HH:mm"},meridiemParse:/މކ|މފ/,isPM:function isPM(a){return "މފ"===a;},meridiem:function meridiem(a,b,c){return 12>a?"މކ":"މފ";},calendar:{sameDay:"[މިއަދު] LT",nextDay:"[މާދަމާ] LT",nextWeek:"dddd LT",lastDay:"[އިއްޔެ] LT",lastWeek:"[ފާއިތުވި] dddd LT",sameElse:"L"},relativeTime:{future:"ތެރޭގައި %s",past:"ކުރިން %s",s:"ސިކުންތުކޮޅެއް",m:"މިނިޓެއް",mm:"މިނިޓު %d",h:"ގަޑިއިރެއް",hh:"ގަޑިއިރު %d",d:"ދުވަހެއް",dd:"ދުވަސް %d",M:"މަހެއް",MM:"މަސް %d",y:"އަހަރެއް",yy:"އަހަރު %d"},preparse:function preparse(a){return a.replace(/،/g,",");},postformat:function postformat(a){return a.replace(/,/g,"،");},week:{dow:7,doy:12}}),bg.defineLocale("el",{monthsNominativeEl:"Ιανουάριος_Φεβρουάριος_Μάρτιος_Απρίλιος_Μάιος_Ιούνιος_Ιούλιος_Αύγουστος_Σεπτέμβριος_Οκτώβριος_Νοέμβριος_Δεκέμβριος".split("_"),monthsGenitiveEl:"Ιανουαρίου_Φεβρουαρίου_Μαρτίου_Απριλίου_Μαΐου_Ιουνίου_Ιουλίου_Αυγούστου_Σεπτεμβρίου_Οκτωβρίου_Νοεμβρίου_Δεκεμβρίου".split("_"),months:function months(a,b){return (/D/.test(b.substring(0,b.indexOf("MMMM")))?this._monthsGenitiveEl[a.month()]:this._monthsNominativeEl[a.month()]);},monthsShort:"Ιαν_Φεβ_Μαρ_Απρ_Μαϊ_Ιουν_Ιουλ_Αυγ_Σεπ_Οκτ_Νοε_Δεκ".split("_"),weekdays:"Κυριακή_Δευτέρα_Τρίτη_Τετάρτη_Πέμπτη_Παρασκευή_Σάββατο".split("_"),weekdaysShort:"Κυρ_Δευ_Τρι_Τετ_Πεμ_Παρ_Σαβ".split("_"),weekdaysMin:"Κυ_Δε_Τρ_Τε_Πε_Πα_Σα".split("_"),meridiem:function meridiem(a,b,c){return a>11?c?"μμ":"ΜΜ":c?"πμ":"ΠΜ";},isPM:function isPM(a){return "μ"===(a+"").toLowerCase()[0];},meridiemParse:/[ΠΜ]\.?Μ?\.?/i,longDateFormat:{LT:"h:mm A",LTS:"h:mm:ss A",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY h:mm A",LLLL:"dddd, D MMMM YYYY h:mm A"},calendarEl:{sameDay:"[Σήμερα {}] LT",nextDay:"[Αύριο {}] LT",nextWeek:"dddd [{}] LT",lastDay:"[Χθες {}] LT",lastWeek:function lastWeek(){switch(this.day()){case 6:return "[το προηγούμενο] dddd [{}] LT";default:return "[την προηγούμενη] dddd [{}] LT";}},sameElse:"L"},calendar:function calendar(a,b){var c=this._calendarEl[a],d=b&&b.hours();return w(c)&&(c=c.apply(b)),c.replace("{}",d%12===1?"στη":"στις");},relativeTime:{future:"σε %s",past:"%s πριν",s:"λίγα δευτερόλεπτα",m:"ένα λεπτό",mm:"%d λεπτά",h:"μία ώρα",hh:"%d ώρες",d:"μία μέρα",dd:"%d μέρες",M:"ένας μήνας",MM:"%d μήνες",y:"ένας χρόνος",yy:"%d χρόνια"},ordinalParse:/\d{1,2}η/,ordinal:"%dη",week:{dow:1,doy:4}}),bg.defineLocale("en-au",{months:"January_February_March_April_May_June_July_August_September_October_November_December".split("_"),monthsShort:"Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec".split("_"),weekdays:"Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday".split("_"),weekdaysShort:"Sun_Mon_Tue_Wed_Thu_Fri_Sat".split("_"),weekdaysMin:"Su_Mo_Tu_We_Th_Fr_Sa".split("_"),longDateFormat:{LT:"h:mm A",LTS:"h:mm:ss A",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY h:mm A",LLLL:"dddd, D MMMM YYYY h:mm A"},calendar:{sameDay:"[Today at] LT",nextDay:"[Tomorrow at] LT",nextWeek:"dddd [at] LT",lastDay:"[Yesterday at] LT",lastWeek:"[Last] dddd [at] LT",sameElse:"L"},relativeTime:{future:"in %s",past:"%s ago",s:"a few seconds",m:"a minute",mm:"%d minutes",h:"an hour",hh:"%d hours",d:"a day",dd:"%d days",M:"a month",MM:"%d months",y:"a year",yy:"%d years"},ordinalParse:/\d{1,2}(st|nd|rd|th)/,ordinal:function ordinal(a){var b=a%10,c=1===~ ~(a%100/10)?"th":1===b?"st":2===b?"nd":3===b?"rd":"th";return a+c;},week:{dow:1,doy:4}}),bg.defineLocale("en-ca",{months:"January_February_March_April_May_June_July_August_September_October_November_December".split("_"),monthsShort:"Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec".split("_"),weekdays:"Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday".split("_"),weekdaysShort:"Sun_Mon_Tue_Wed_Thu_Fri_Sat".split("_"),weekdaysMin:"Su_Mo_Tu_We_Th_Fr_Sa".split("_"),longDateFormat:{LT:"h:mm A",LTS:"h:mm:ss A",L:"YYYY-MM-DD",LL:"MMMM D, YYYY",LLL:"MMMM D, YYYY h:mm A",LLLL:"dddd, MMMM D, YYYY h:mm A"},calendar:{sameDay:"[Today at] LT",nextDay:"[Tomorrow at] LT",nextWeek:"dddd [at] LT",lastDay:"[Yesterday at] LT",lastWeek:"[Last] dddd [at] LT",sameElse:"L"},relativeTime:{future:"in %s",past:"%s ago",s:"a few seconds",m:"a minute",mm:"%d minutes",h:"an hour",hh:"%d hours",d:"a day",dd:"%d days",M:"a month",MM:"%d months",y:"a year",yy:"%d years"},ordinalParse:/\d{1,2}(st|nd|rd|th)/,ordinal:function ordinal(a){var b=a%10,c=1===~ ~(a%100/10)?"th":1===b?"st":2===b?"nd":3===b?"rd":"th";return a+c;}}),bg.defineLocale("en-gb",{months:"January_February_March_April_May_June_July_August_September_October_November_December".split("_"),monthsShort:"Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec".split("_"),weekdays:"Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday".split("_"),weekdaysShort:"Sun_Mon_Tue_Wed_Thu_Fri_Sat".split("_"),weekdaysMin:"Su_Mo_Tu_We_Th_Fr_Sa".split("_"),longDateFormat:{LT:"HH:mm",LTS:"HH:mm:ss",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY HH:mm",LLLL:"dddd, D MMMM YYYY HH:mm"},calendar:{sameDay:"[Today at] LT",nextDay:"[Tomorrow at] LT",nextWeek:"dddd [at] LT",lastDay:"[Yesterday at] LT",lastWeek:"[Last] dddd [at] LT",sameElse:"L"},relativeTime:{future:"in %s",past:"%s ago",s:"a few seconds",m:"a minute",mm:"%d minutes",h:"an hour",hh:"%d hours",d:"a day",dd:"%d days",M:"a month",MM:"%d months",y:"a year",yy:"%d years"},ordinalParse:/\d{1,2}(st|nd|rd|th)/,ordinal:function ordinal(a){var b=a%10,c=1===~ ~(a%100/10)?"th":1===b?"st":2===b?"nd":3===b?"rd":"th";return a+c;},week:{dow:1,doy:4}}),bg.defineLocale("en-ie",{months:"January_February_March_April_May_June_July_August_September_October_November_December".split("_"),monthsShort:"Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec".split("_"),weekdays:"Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday".split("_"),weekdaysShort:"Sun_Mon_Tue_Wed_Thu_Fri_Sat".split("_"),weekdaysMin:"Su_Mo_Tu_We_Th_Fr_Sa".split("_"),longDateFormat:{LT:"HH:mm",LTS:"HH:mm:ss",L:"DD-MM-YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY HH:mm",LLLL:"dddd D MMMM YYYY HH:mm"},calendar:{sameDay:"[Today at] LT",nextDay:"[Tomorrow at] LT",nextWeek:"dddd [at] LT",lastDay:"[Yesterday at] LT",lastWeek:"[Last] dddd [at] LT",sameElse:"L"},relativeTime:{future:"in %s",past:"%s ago",s:"a few seconds",m:"a minute",mm:"%d minutes",h:"an hour",hh:"%d hours",d:"a day",dd:"%d days",M:"a month",MM:"%d months",y:"a year",yy:"%d years"},ordinalParse:/\d{1,2}(st|nd|rd|th)/,ordinal:function ordinal(a){var b=a%10,c=1===~ ~(a%100/10)?"th":1===b?"st":2===b?"nd":3===b?"rd":"th";return a+c;},week:{dow:1,doy:4}}),bg.defineLocale("en-nz",{months:"January_February_March_April_May_June_July_August_September_October_November_December".split("_"),monthsShort:"Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec".split("_"),weekdays:"Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday".split("_"),weekdaysShort:"Sun_Mon_Tue_Wed_Thu_Fri_Sat".split("_"),weekdaysMin:"Su_Mo_Tu_We_Th_Fr_Sa".split("_"),longDateFormat:{LT:"h:mm A",LTS:"h:mm:ss A",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY h:mm A",LLLL:"dddd, D MMMM YYYY h:mm A"},calendar:{sameDay:"[Today at] LT",nextDay:"[Tomorrow at] LT",nextWeek:"dddd [at] LT",lastDay:"[Yesterday at] LT",lastWeek:"[Last] dddd [at] LT",sameElse:"L"},relativeTime:{future:"in %s",past:"%s ago",s:"a few seconds",m:"a minute",mm:"%d minutes",h:"an hour",hh:"%d hours",d:"a day",dd:"%d days",M:"a month",MM:"%d months",y:"a year",yy:"%d years"},ordinalParse:/\d{1,2}(st|nd|rd|th)/,ordinal:function ordinal(a){var b=a%10,c=1===~ ~(a%100/10)?"th":1===b?"st":2===b?"nd":3===b?"rd":"th";return a+c;},week:{dow:1,doy:4}}),bg.defineLocale("eo",{months:"januaro_februaro_marto_aprilo_majo_junio_julio_aŭgusto_septembro_oktobro_novembro_decembro".split("_"),monthsShort:"jan_feb_mar_apr_maj_jun_jul_aŭg_sep_okt_nov_dec".split("_"),weekdays:"Dimanĉo_Lundo_Mardo_Merkredo_Ĵaŭdo_Vendredo_Sabato".split("_"),weekdaysShort:"Dim_Lun_Mard_Merk_Ĵaŭ_Ven_Sab".split("_"),weekdaysMin:"Di_Lu_Ma_Me_Ĵa_Ve_Sa".split("_"),longDateFormat:{LT:"HH:mm",LTS:"HH:mm:ss",L:"YYYY-MM-DD",LL:"D[-an de] MMMM, YYYY",LLL:"D[-an de] MMMM, YYYY HH:mm",LLLL:"dddd, [la] D[-an de] MMMM, YYYY HH:mm"},meridiemParse:/[ap]\.t\.m/i,isPM:function isPM(a){return "p"===a.charAt(0).toLowerCase();},meridiem:function meridiem(a,b,c){return a>11?c?"p.t.m.":"P.T.M.":c?"a.t.m.":"A.T.M.";},calendar:{sameDay:"[Hodiaŭ je] LT",nextDay:"[Morgaŭ je] LT",nextWeek:"dddd [je] LT",lastDay:"[Hieraŭ je] LT",lastWeek:"[pasinta] dddd [je] LT",sameElse:"L"},relativeTime:{future:"je %s",past:"antaŭ %s",s:"sekundoj",m:"minuto",mm:"%d minutoj",h:"horo",hh:"%d horoj",d:"tago",dd:"%d tagoj",M:"monato",MM:"%d monatoj",y:"jaro",yy:"%d jaroj"},ordinalParse:/\d{1,2}a/,ordinal:"%da",week:{dow:1,doy:7}}),"ene._feb._mar._abr._may._jun._jul._ago._sep._oct._nov._dic.".split("_")),ug="ene_feb_mar_abr_may_jun_jul_ago_sep_oct_nov_dic".split("_"),vg=(bg.defineLocale("es",{months:"enero_febrero_marzo_abril_mayo_junio_julio_agosto_septiembre_octubre_noviembre_diciembre".split("_"),monthsShort:function monthsShort(a,b){return (/-MMM-/.test(b)?ug[a.month()]:tg[a.month()]);},monthsParseExact:!0,weekdays:"domingo_lunes_martes_miércoles_jueves_viernes_sábado".split("_"),weekdaysShort:"dom._lun._mar._mié._jue._vie._sáb.".split("_"),weekdaysMin:"do_lu_ma_mi_ju_vi_sá".split("_"),weekdaysParseExact:!0,longDateFormat:{LT:"H:mm",LTS:"H:mm:ss",L:"DD/MM/YYYY",LL:"D [de] MMMM [de] YYYY",LLL:"D [de] MMMM [de] YYYY H:mm",LLLL:"dddd, D [de] MMMM [de] YYYY H:mm"},calendar:{sameDay:function sameDay(){return "[hoy a la"+(1!==this.hours()?"s":"")+"] LT";},nextDay:function nextDay(){return "[mañana a la"+(1!==this.hours()?"s":"")+"] LT";},nextWeek:function nextWeek(){return "dddd [a la"+(1!==this.hours()?"s":"")+"] LT";},lastDay:function lastDay(){return "[ayer a la"+(1!==this.hours()?"s":"")+"] LT";},lastWeek:function lastWeek(){return "[el] dddd [pasado a la"+(1!==this.hours()?"s":"")+"] LT";},sameElse:"L"},relativeTime:{future:"en %s",past:"hace %s",s:"unos segundos",m:"un minuto",mm:"%d minutos",h:"una hora",hh:"%d horas",d:"un día",dd:"%d días",M:"un mes",MM:"%d meses",y:"un año",yy:"%d años"},ordinalParse:/\d{1,2}º/,ordinal:"%dº",week:{dow:1,doy:4}}),bg.defineLocale("et",{months:"jaanuar_veebruar_märts_aprill_mai_juuni_juuli_august_september_oktoober_november_detsember".split("_"),monthsShort:"jaan_veebr_märts_apr_mai_juuni_juuli_aug_sept_okt_nov_dets".split("_"),weekdays:"pühapäev_esmaspäev_teisipäev_kolmapäev_neljapäev_reede_laupäev".split("_"),weekdaysShort:"P_E_T_K_N_R_L".split("_"),weekdaysMin:"P_E_T_K_N_R_L".split("_"),longDateFormat:{LT:"H:mm",LTS:"H:mm:ss",L:"DD.MM.YYYY",LL:"D. MMMM YYYY",LLL:"D. MMMM YYYY H:mm",LLLL:"dddd, D. MMMM YYYY H:mm"},calendar:{sameDay:"[Täna,] LT",nextDay:"[Homme,] LT",nextWeek:"[Järgmine] dddd LT",lastDay:"[Eile,] LT",lastWeek:"[Eelmine] dddd LT",sameElse:"L"},relativeTime:{future:"%s pärast",past:"%s tagasi",s:rd,m:rd,mm:rd,h:rd,hh:rd,d:rd,dd:"%d päeva",M:rd,MM:rd,y:rd,yy:rd},ordinalParse:/\d{1,2}\./,ordinal:"%d.",week:{dow:1,doy:4}}),bg.defineLocale("eu",{months:"urtarrila_otsaila_martxoa_apirila_maiatza_ekaina_uztaila_abuztua_iraila_urria_azaroa_abendua".split("_"),monthsShort:"urt._ots._mar._api._mai._eka._uzt._abu._ira._urr._aza._abe.".split("_"),monthsParseExact:!0,weekdays:"igandea_astelehena_asteartea_asteazkena_osteguna_ostirala_larunbata".split("_"),weekdaysShort:"ig._al._ar._az._og._ol._lr.".split("_"),weekdaysMin:"ig_al_ar_az_og_ol_lr".split("_"),weekdaysParseExact:!0,longDateFormat:{LT:"HH:mm",LTS:"HH:mm:ss",L:"YYYY-MM-DD",LL:"YYYY[ko] MMMM[ren] D[a]",LLL:"YYYY[ko] MMMM[ren] D[a] HH:mm",LLLL:"dddd, YYYY[ko] MMMM[ren] D[a] HH:mm",l:"YYYY-M-D",ll:"YYYY[ko] MMM D[a]",lll:"YYYY[ko] MMM D[a] HH:mm",llll:"ddd, YYYY[ko] MMM D[a] HH:mm"},calendar:{sameDay:"[gaur] LT[etan]",nextDay:"[bihar] LT[etan]",nextWeek:"dddd LT[etan]",lastDay:"[atzo] LT[etan]",lastWeek:"[aurreko] dddd LT[etan]",sameElse:"L"},relativeTime:{future:"%s barru",past:"duela %s",s:"segundo batzuk",m:"minutu bat",mm:"%d minutu",h:"ordu bat",hh:"%d ordu",d:"egun bat",dd:"%d egun",M:"hilabete bat",MM:"%d hilabete",y:"urte bat",yy:"%d urte"},ordinalParse:/\d{1,2}\./,ordinal:"%d.",week:{dow:1,doy:7}}),{1:"۱",2:"۲",3:"۳",4:"۴",5:"۵",6:"۶",7:"۷",8:"۸",9:"۹",0:"۰"}),wg={"۱":"1","۲":"2","۳":"3","۴":"4","۵":"5","۶":"6","۷":"7","۸":"8","۹":"9","۰":"0"},xg=(bg.defineLocale("fa",{months:"ژانویه_فوریه_مارس_آوریل_مه_ژوئن_ژوئیه_اوت_سپتامبر_اکتبر_نوامبر_دسامبر".split("_"),monthsShort:"ژانویه_فوریه_مارس_آوریل_مه_ژوئن_ژوئیه_اوت_سپتامبر_اکتبر_نوامبر_دسامبر".split("_"),weekdays:"یک‌شنبه_دوشنبه_سه‌شنبه_چهارشنبه_پنج‌شنبه_جمعه_شنبه".split("_"),weekdaysShort:"یک‌شنبه_دوشنبه_سه‌شنبه_چهارشنبه_پنج‌شنبه_جمعه_شنبه".split("_"),weekdaysMin:"ی_د_س_چ_پ_ج_ش".split("_"),weekdaysParseExact:!0,longDateFormat:{LT:"HH:mm",LTS:"HH:mm:ss",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY HH:mm",LLLL:"dddd, D MMMM YYYY HH:mm"},meridiemParse:/قبل از ظهر|بعد از ظهر/,isPM:function isPM(a){return (/بعد از ظهر/.test(a));},meridiem:function meridiem(a,b,c){return 12>a?"قبل از ظهر":"بعد از ظهر";},calendar:{sameDay:"[امروز ساعت] LT",nextDay:"[فردا ساعت] LT",nextWeek:"dddd [ساعت] LT",lastDay:"[دیروز ساعت] LT",lastWeek:"dddd [پیش] [ساعت] LT",sameElse:"L"},relativeTime:{future:"در %s",past:"%s پیش",s:"چندین ثانیه",m:"یک دقیقه",mm:"%d دقیقه",h:"یک ساعت",hh:"%d ساعت",d:"یک روز",dd:"%d روز",M:"یک ماه",MM:"%d ماه",y:"یک سال",yy:"%d سال"},preparse:function preparse(a){return a.replace(/[۰-۹]/g,function(a){return wg[a];}).replace(/،/g,",");},postformat:function postformat(a){return a.replace(/\d/g,function(a){return vg[a];}).replace(/,/g,"،");},ordinalParse:/\d{1,2}م/,ordinal:"%dم",week:{dow:6,doy:12}}),"nolla yksi kaksi kolme neljä viisi kuusi seitsemän kahdeksan yhdeksän".split(" ")),yg=["nolla","yhden","kahden","kolmen","neljän","viiden","kuuden",xg[7],xg[8],xg[9]],zg=(bg.defineLocale("fi",{months:"tammikuu_helmikuu_maaliskuu_huhtikuu_toukokuu_kesäkuu_heinäkuu_elokuu_syyskuu_lokakuu_marraskuu_joulukuu".split("_"),monthsShort:"tammi_helmi_maalis_huhti_touko_kesä_heinä_elo_syys_loka_marras_joulu".split("_"),weekdays:"sunnuntai_maanantai_tiistai_keskiviikko_torstai_perjantai_lauantai".split("_"),weekdaysShort:"su_ma_ti_ke_to_pe_la".split("_"),weekdaysMin:"su_ma_ti_ke_to_pe_la".split("_"),longDateFormat:{LT:"HH.mm",LTS:"HH.mm.ss",L:"DD.MM.YYYY",LL:"Do MMMM[ta] YYYY",LLL:"Do MMMM[ta] YYYY, [klo] HH.mm",LLLL:"dddd, Do MMMM[ta] YYYY, [klo] HH.mm",l:"D.M.YYYY",ll:"Do MMM YYYY",lll:"Do MMM YYYY, [klo] HH.mm",llll:"ddd, Do MMM YYYY, [klo] HH.mm"},calendar:{sameDay:"[tänään] [klo] LT",nextDay:"[huomenna] [klo] LT",nextWeek:"dddd [klo] LT",lastDay:"[eilen] [klo] LT",lastWeek:"[viime] dddd[na] [klo] LT",sameElse:"L"},relativeTime:{future:"%s päästä",past:"%s sitten",s:sd,m:sd,mm:sd,h:sd,hh:sd,d:sd,dd:sd,M:sd,MM:sd,y:sd,yy:sd},ordinalParse:/\d{1,2}\./,ordinal:"%d.",week:{dow:1,doy:4}}),bg.defineLocale("fo",{months:"januar_februar_mars_apríl_mai_juni_juli_august_september_oktober_november_desember".split("_"),monthsShort:"jan_feb_mar_apr_mai_jun_jul_aug_sep_okt_nov_des".split("_"),weekdays:"sunnudagur_mánadagur_týsdagur_mikudagur_hósdagur_fríggjadagur_leygardagur".split("_"),weekdaysShort:"sun_mán_týs_mik_hós_frí_ley".split("_"),weekdaysMin:"su_má_tý_mi_hó_fr_le".split("_"),longDateFormat:{LT:"HH:mm",LTS:"HH:mm:ss",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY HH:mm",LLLL:"dddd D. MMMM, YYYY HH:mm"},calendar:{sameDay:"[Í dag kl.] LT",nextDay:"[Í morgin kl.] LT",nextWeek:"dddd [kl.] LT",lastDay:"[Í gjár kl.] LT",lastWeek:"[síðstu] dddd [kl] LT",sameElse:"L"},relativeTime:{future:"um %s",past:"%s síðani",s:"fá sekund",m:"ein minutt",mm:"%d minuttir",h:"ein tími",hh:"%d tímar",d:"ein dagur",dd:"%d dagar",M:"ein mánaði",MM:"%d mánaðir",y:"eitt ár",yy:"%d ár"},ordinalParse:/\d{1,2}\./,ordinal:"%d.",week:{dow:1,doy:4}}),bg.defineLocale("fr-ca",{months:"janvier_février_mars_avril_mai_juin_juillet_août_septembre_octobre_novembre_décembre".split("_"),monthsShort:"janv._févr._mars_avr._mai_juin_juil._août_sept._oct._nov._déc.".split("_"),monthsParseExact:!0,weekdays:"dimanche_lundi_mardi_mercredi_jeudi_vendredi_samedi".split("_"),weekdaysShort:"dim._lun._mar._mer._jeu._ven._sam.".split("_"),weekdaysMin:"Di_Lu_Ma_Me_Je_Ve_Sa".split("_"),weekdaysParseExact:!0,longDateFormat:{LT:"HH:mm",LTS:"HH:mm:ss",L:"YYYY-MM-DD",LL:"D MMMM YYYY",LLL:"D MMMM YYYY HH:mm",LLLL:"dddd D MMMM YYYY HH:mm"},calendar:{sameDay:"[Aujourd'hui à] LT",nextDay:"[Demain à] LT",nextWeek:"dddd [à] LT",lastDay:"[Hier à] LT",lastWeek:"dddd [dernier à] LT",sameElse:"L"},relativeTime:{future:"dans %s",past:"il y a %s",s:"quelques secondes",m:"une minute",mm:"%d minutes",h:"une heure",hh:"%d heures",d:"un jour",dd:"%d jours",M:"un mois",MM:"%d mois",y:"un an",yy:"%d ans"},ordinalParse:/\d{1,2}(er|e)/,ordinal:function ordinal(a){return a+(1===a?"er":"e");}}),bg.defineLocale("fr-ch",{months:"janvier_février_mars_avril_mai_juin_juillet_août_septembre_octobre_novembre_décembre".split("_"),monthsShort:"janv._févr._mars_avr._mai_juin_juil._août_sept._oct._nov._déc.".split("_"),monthsParseExact:!0,weekdays:"dimanche_lundi_mardi_mercredi_jeudi_vendredi_samedi".split("_"),weekdaysShort:"dim._lun._mar._mer._jeu._ven._sam.".split("_"),weekdaysMin:"Di_Lu_Ma_Me_Je_Ve_Sa".split("_"),weekdaysParseExact:!0,longDateFormat:{LT:"HH:mm",LTS:"HH:mm:ss",L:"DD.MM.YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY HH:mm",LLLL:"dddd D MMMM YYYY HH:mm"},calendar:{sameDay:"[Aujourd'hui à] LT",nextDay:"[Demain à] LT",nextWeek:"dddd [à] LT",lastDay:"[Hier à] LT",lastWeek:"dddd [dernier à] LT",sameElse:"L"},relativeTime:{future:"dans %s",past:"il y a %s",s:"quelques secondes",m:"une minute",mm:"%d minutes",h:"une heure",hh:"%d heures",d:"un jour",dd:"%d jours",M:"un mois",MM:"%d mois",y:"un an",yy:"%d ans"},ordinalParse:/\d{1,2}(er|e)/,ordinal:function ordinal(a){return a+(1===a?"er":"e");},week:{dow:1,doy:4}}),bg.defineLocale("fr",{months:"janvier_février_mars_avril_mai_juin_juillet_août_septembre_octobre_novembre_décembre".split("_"),monthsShort:"janv._févr._mars_avr._mai_juin_juil._août_sept._oct._nov._déc.".split("_"),monthsParseExact:!0,weekdays:"dimanche_lundi_mardi_mercredi_jeudi_vendredi_samedi".split("_"),weekdaysShort:"dim._lun._mar._mer._jeu._ven._sam.".split("_"),weekdaysMin:"Di_Lu_Ma_Me_Je_Ve_Sa".split("_"),weekdaysParseExact:!0,longDateFormat:{LT:"HH:mm",LTS:"HH:mm:ss",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY HH:mm",LLLL:"dddd D MMMM YYYY HH:mm"},calendar:{sameDay:"[Aujourd'hui à] LT",nextDay:"[Demain à] LT",nextWeek:"dddd [à] LT",lastDay:"[Hier à] LT",lastWeek:"dddd [dernier à] LT",sameElse:"L"},relativeTime:{future:"dans %s",past:"il y a %s",s:"quelques secondes",m:"une minute",mm:"%d minutes",h:"une heure",hh:"%d heures",d:"un jour",dd:"%d jours",M:"un mois",MM:"%d mois",y:"un an",yy:"%d ans"},ordinalParse:/\d{1,2}(er|)/,ordinal:function ordinal(a){return a+(1===a?"er":"");},week:{dow:1,doy:4}}),"jan._feb._mrt._apr._mai_jun._jul._aug._sep._okt._nov._des.".split("_")),Ag="jan_feb_mrt_apr_mai_jun_jul_aug_sep_okt_nov_des".split("_"),Bg=(bg.defineLocale("fy",{months:"jannewaris_febrewaris_maart_april_maaie_juny_july_augustus_septimber_oktober_novimber_desimber".split("_"),monthsShort:function monthsShort(a,b){return (/-MMM-/.test(b)?Ag[a.month()]:zg[a.month()]);},monthsParseExact:!0,weekdays:"snein_moandei_tiisdei_woansdei_tongersdei_freed_sneon".split("_"),weekdaysShort:"si._mo._ti._wo._to._fr._so.".split("_"),weekdaysMin:"Si_Mo_Ti_Wo_To_Fr_So".split("_"),weekdaysParseExact:!0,longDateFormat:{LT:"HH:mm",LTS:"HH:mm:ss",L:"DD-MM-YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY HH:mm",LLLL:"dddd D MMMM YYYY HH:mm"},calendar:{sameDay:"[hjoed om] LT",nextDay:"[moarn om] LT",nextWeek:"dddd [om] LT",lastDay:"[juster om] LT",lastWeek:"[ôfrûne] dddd [om] LT",sameElse:"L"},relativeTime:{future:"oer %s",past:"%s lyn",s:"in pear sekonden",m:"ien minút",mm:"%d minuten",h:"ien oere",hh:"%d oeren",d:"ien dei",dd:"%d dagen",M:"ien moanne",MM:"%d moannen",y:"ien jier",yy:"%d jierren"},ordinalParse:/\d{1,2}(ste|de)/,ordinal:function ordinal(a){return a+(1===a||8===a||a>=20?"ste":"de");},week:{dow:1,doy:4}}),["Am Faoilleach","An Gearran","Am Màrt","An Giblean","An Cèitean","An t-Ògmhios","An t-Iuchar","An Lùnastal","An t-Sultain","An Dàmhair","An t-Samhain","An Dùbhlachd"]),Cg=["Faoi","Gear","Màrt","Gibl","Cèit","Ògmh","Iuch","Lùn","Sult","Dàmh","Samh","Dùbh"],Dg=["Didòmhnaich","Diluain","Dimàirt","Diciadain","Diardaoin","Dihaoine","Disathairne"],Eg=["Did","Dil","Dim","Dic","Dia","Dih","Dis"],Fg=["Dò","Lu","Mà","Ci","Ar","Ha","Sa"],Gg=(bg.defineLocale("gd",{months:Bg,monthsShort:Cg,monthsParseExact:!0,weekdays:Dg,weekdaysShort:Eg,weekdaysMin:Fg,longDateFormat:{LT:"HH:mm",LTS:"HH:mm:ss",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY HH:mm",LLLL:"dddd, D MMMM YYYY HH:mm"},calendar:{sameDay:"[An-diugh aig] LT",nextDay:"[A-màireach aig] LT",nextWeek:"dddd [aig] LT",lastDay:"[An-dè aig] LT",lastWeek:"dddd [seo chaidh] [aig] LT",sameElse:"L"},relativeTime:{future:"ann an %s",past:"bho chionn %s",s:"beagan diogan",m:"mionaid",mm:"%d mionaidean",h:"uair",hh:"%d uairean",d:"latha",dd:"%d latha",M:"mìos",MM:"%d mìosan",y:"bliadhna",yy:"%d bliadhna"},ordinalParse:/\d{1,2}(d|na|mh)/,ordinal:function ordinal(a){var b=1===a?"d":a%10===2?"na":"mh";return a+b;},week:{dow:1,doy:4}}),bg.defineLocale("gl",{months:"Xaneiro_Febreiro_Marzo_Abril_Maio_Xuño_Xullo_Agosto_Setembro_Outubro_Novembro_Decembro".split("_"),monthsShort:"Xan._Feb._Mar._Abr._Mai._Xuñ._Xul._Ago._Set._Out._Nov._Dec.".split("_"),monthsParseExact:!0,weekdays:"Domingo_Luns_Martes_Mércores_Xoves_Venres_Sábado".split("_"),weekdaysShort:"Dom._Lun._Mar._Mér._Xov._Ven._Sáb.".split("_"),weekdaysMin:"Do_Lu_Ma_Mé_Xo_Ve_Sá".split("_"),weekdaysParseExact:!0,longDateFormat:{LT:"H:mm",LTS:"H:mm:ss",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY H:mm",LLLL:"dddd D MMMM YYYY H:mm"},calendar:{sameDay:function sameDay(){return "[hoxe "+(1!==this.hours()?"ás":"á")+"] LT";},nextDay:function nextDay(){return "[mañá "+(1!==this.hours()?"ás":"á")+"] LT";},nextWeek:function nextWeek(){return "dddd ["+(1!==this.hours()?"ás":"a")+"] LT";},lastDay:function lastDay(){return "[onte "+(1!==this.hours()?"á":"a")+"] LT";},lastWeek:function lastWeek(){return "[o] dddd [pasado "+(1!==this.hours()?"ás":"a")+"] LT";},sameElse:"L"},relativeTime:{future:function future(a){return "uns segundos"===a?"nuns segundos":"en "+a;},past:"hai %s",s:"uns segundos",m:"un minuto",mm:"%d minutos",h:"unha hora",hh:"%d horas",d:"un día",dd:"%d días",M:"un mes",MM:"%d meses",y:"un ano",yy:"%d anos"},ordinalParse:/\d{1,2}º/,ordinal:"%dº",week:{dow:1,doy:7}}),bg.defineLocale("he",{months:"ינואר_פברואר_מרץ_אפריל_מאי_יוני_יולי_אוגוסט_ספטמבר_אוקטובר_נובמבר_דצמבר".split("_"),monthsShort:"ינו׳_פבר׳_מרץ_אפר׳_מאי_יוני_יולי_אוג׳_ספט׳_אוק׳_נוב׳_דצמ׳".split("_"),weekdays:"ראשון_שני_שלישי_רביעי_חמישי_שישי_שבת".split("_"),weekdaysShort:"א׳_ב׳_ג׳_ד׳_ה׳_ו׳_ש׳".split("_"),weekdaysMin:"א_ב_ג_ד_ה_ו_ש".split("_"),longDateFormat:{LT:"HH:mm",LTS:"HH:mm:ss",L:"DD/MM/YYYY",LL:"D [ב]MMMM YYYY",LLL:"D [ב]MMMM YYYY HH:mm",LLLL:"dddd, D [ב]MMMM YYYY HH:mm",l:"D/M/YYYY",ll:"D MMM YYYY",lll:"D MMM YYYY HH:mm",llll:"ddd, D MMM YYYY HH:mm"},calendar:{sameDay:"[היום ב־]LT",nextDay:"[מחר ב־]LT",nextWeek:"dddd [בשעה] LT",lastDay:"[אתמול ב־]LT",lastWeek:"[ביום] dddd [האחרון בשעה] LT",sameElse:"L"},relativeTime:{future:"בעוד %s",past:"לפני %s",s:"מספר שניות",m:"דקה",mm:"%d דקות",h:"שעה",hh:function hh(a){return 2===a?"שעתיים":a+" שעות";},d:"יום",dd:function dd(a){return 2===a?"יומיים":a+" ימים";},M:"חודש",MM:function MM(a){return 2===a?"חודשיים":a+" חודשים";},y:"שנה",yy:function yy(a){return 2===a?"שנתיים":a%10===0&&10!==a?a+" שנה":a+" שנים";}},meridiemParse:/אחה"צ|לפנה"צ|אחרי הצהריים|לפני הצהריים|לפנות בוקר|בבוקר|בערב/i,isPM:function isPM(a){return (/^(אחה"צ|אחרי הצהריים|בערב)$/.test(a));},meridiem:function meridiem(a,b,c){return 5>a?"לפנות בוקר":10>a?"בבוקר":12>a?c?'לפנה"צ':"לפני הצהריים":18>a?c?'אחה"צ':"אחרי הצהריים":"בערב";}}),{1:"१",2:"२",3:"३",4:"४",5:"५",6:"६",7:"७",8:"८",9:"९",0:"०"}),Hg={"१":"1","२":"2","३":"3","४":"4","५":"5","६":"6","७":"7","८":"8","९":"9","०":"0"},Ig=(bg.defineLocale("hi",{months:"जनवरी_फ़रवरी_मार्च_अप्रैल_मई_जून_जुलाई_अगस्त_सितम्बर_अक्टूबर_नवम्बर_दिसम्बर".split("_"),monthsShort:"जन._फ़र._मार्च_अप्रै._मई_जून_जुल._अग._सित._अक्टू._नव._दिस.".split("_"),monthsParseExact:!0,weekdays:"रविवार_सोमवार_मंगलवार_बुधवार_गुरूवार_शुक्रवार_शनिवार".split("_"),weekdaysShort:"रवि_सोम_मंगल_बुध_गुरू_शुक्र_शनि".split("_"),weekdaysMin:"र_सो_मं_बु_गु_शु_श".split("_"),longDateFormat:{LT:"A h:mm बजे",LTS:"A h:mm:ss बजे",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY, A h:mm बजे",LLLL:"dddd, D MMMM YYYY, A h:mm बजे"},calendar:{sameDay:"[आज] LT",nextDay:"[कल] LT",nextWeek:"dddd, LT",lastDay:"[कल] LT",lastWeek:"[पिछले] dddd, LT",sameElse:"L"},relativeTime:{future:"%s में",past:"%s पहले",s:"कुछ ही क्षण",m:"एक मिनट",mm:"%d मिनट",h:"एक घंटा",hh:"%d घंटे",d:"एक दिन",dd:"%d दिन",M:"एक महीने",MM:"%d महीने",y:"एक वर्ष",yy:"%d वर्ष"},preparse:function preparse(a){return a.replace(/[१२३४५६७८९०]/g,function(a){return Hg[a];});},postformat:function postformat(a){return a.replace(/\d/g,function(a){return Gg[a];});},meridiemParse:/रात|सुबह|दोपहर|शाम/,meridiemHour:function meridiemHour(a,b){return 12===a&&(a=0),"रात"===b?4>a?a:a+12:"सुबह"===b?a:"दोपहर"===b?a>=10?a:a+12:"शाम"===b?a+12:void 0;},meridiem:function meridiem(a,b,c){return 4>a?"रात":10>a?"सुबह":17>a?"दोपहर":20>a?"शाम":"रात";},week:{dow:0,doy:6}}),bg.defineLocale("hr",{months:{format:"siječnja_veljače_ožujka_travnja_svibnja_lipnja_srpnja_kolovoza_rujna_listopada_studenoga_prosinca".split("_"),standalone:"siječanj_veljača_ožujak_travanj_svibanj_lipanj_srpanj_kolovoz_rujan_listopad_studeni_prosinac".split("_")},monthsShort:"sij._velj._ožu._tra._svi._lip._srp._kol._ruj._lis._stu._pro.".split("_"),monthsParseExact:!0,weekdays:"nedjelja_ponedjeljak_utorak_srijeda_četvrtak_petak_subota".split("_"),weekdaysShort:"ned._pon._uto._sri._čet._pet._sub.".split("_"),weekdaysMin:"ne_po_ut_sr_če_pe_su".split("_"),weekdaysParseExact:!0,longDateFormat:{LT:"H:mm",LTS:"H:mm:ss",L:"DD. MM. YYYY",LL:"D. MMMM YYYY",LLL:"D. MMMM YYYY H:mm",LLLL:"dddd, D. MMMM YYYY H:mm"},calendar:{sameDay:"[danas u] LT",nextDay:"[sutra u] LT",nextWeek:function nextWeek(){switch(this.day()){case 0:return "[u] [nedjelju] [u] LT";case 3:return "[u] [srijedu] [u] LT";case 6:return "[u] [subotu] [u] LT";case 1:case 2:case 4:case 5:return "[u] dddd [u] LT";}},lastDay:"[jučer u] LT",lastWeek:function lastWeek(){switch(this.day()){case 0:case 3:return "[prošlu] dddd [u] LT";case 6:return "[prošle] [subote] [u] LT";case 1:case 2:case 4:case 5:return "[prošli] dddd [u] LT";}},sameElse:"L"},relativeTime:{future:"za %s",past:"prije %s",s:"par sekundi",m:ud,mm:ud,h:ud,hh:ud,d:"dan",dd:ud,M:"mjesec",MM:ud,y:"godinu",yy:ud},ordinalParse:/\d{1,2}\./,ordinal:"%d.",week:{dow:1,doy:7}}),"vasárnap hétfőn kedden szerdán csütörtökön pénteken szombaton".split(" ")),Jg=(bg.defineLocale("hu",{months:"január_február_március_április_május_június_július_augusztus_szeptember_október_november_december".split("_"),monthsShort:"jan_feb_márc_ápr_máj_jún_júl_aug_szept_okt_nov_dec".split("_"),weekdays:"vasárnap_hétfő_kedd_szerda_csütörtök_péntek_szombat".split("_"),weekdaysShort:"vas_hét_kedd_sze_csüt_pén_szo".split("_"),weekdaysMin:"v_h_k_sze_cs_p_szo".split("_"),longDateFormat:{LT:"H:mm",LTS:"H:mm:ss",L:"YYYY.MM.DD.",LL:"YYYY. MMMM D.",LLL:"YYYY. MMMM D. H:mm",LLLL:"YYYY. MMMM D., dddd H:mm"},meridiemParse:/de|du/i,isPM:function isPM(a){return "u"===a.charAt(1).toLowerCase();},meridiem:function meridiem(a,b,c){return 12>a?c===!0?"de":"DE":c===!0?"du":"DU";},calendar:{sameDay:"[ma] LT[-kor]",nextDay:"[holnap] LT[-kor]",nextWeek:function nextWeek(){return wd.call(this,!0);},lastDay:"[tegnap] LT[-kor]",lastWeek:function lastWeek(){return wd.call(this,!1);},sameElse:"L"},relativeTime:{future:"%s múlva",past:"%s",s:vd,m:vd,mm:vd,h:vd,hh:vd,d:vd,dd:vd,M:vd,MM:vd,y:vd,yy:vd},ordinalParse:/\d{1,2}\./,ordinal:"%d.",week:{dow:1,doy:7}}),bg.defineLocale("hy-am",{months:{format:"հունվարի_փետրվարի_մարտի_ապրիլի_մայիսի_հունիսի_հուլիսի_օգոստոսի_սեպտեմբերի_հոկտեմբերի_նոյեմբերի_դեկտեմբերի".split("_"),standalone:"հունվար_փետրվար_մարտ_ապրիլ_մայիս_հունիս_հուլիս_օգոստոս_սեպտեմբեր_հոկտեմբեր_նոյեմբեր_դեկտեմբեր".split("_")},monthsShort:"հնվ_փտր_մրտ_ապր_մյս_հնս_հլս_օգս_սպտ_հկտ_նմբ_դկտ".split("_"),weekdays:"կիրակի_երկուշաբթի_երեքշաբթի_չորեքշաբթի_հինգշաբթի_ուրբաթ_շաբաթ".split("_"),weekdaysShort:"կրկ_երկ_երք_չրք_հնգ_ուրբ_շբթ".split("_"),weekdaysMin:"կրկ_երկ_երք_չրք_հնգ_ուրբ_շբթ".split("_"),longDateFormat:{LT:"HH:mm",LTS:"HH:mm:ss",L:"DD.MM.YYYY",LL:"D MMMM YYYY թ.",LLL:"D MMMM YYYY թ., HH:mm",LLLL:"dddd, D MMMM YYYY թ., HH:mm"},calendar:{sameDay:"[այսօր] LT",nextDay:"[վաղը] LT",lastDay:"[երեկ] LT",nextWeek:function nextWeek(){return "dddd [օրը ժամը] LT";},lastWeek:function lastWeek(){return "[անցած] dddd [օրը ժամը] LT";},sameElse:"L"},relativeTime:{future:"%s հետո",past:"%s առաջ",s:"մի քանի վայրկյան",m:"րոպե",mm:"%d րոպե",h:"ժամ",hh:"%d ժամ",d:"օր",dd:"%d օր",M:"ամիս",MM:"%d ամիս",y:"տարի",yy:"%d տարի"},meridiemParse:/գիշերվա|առավոտվա|ցերեկվա|երեկոյան/,isPM:function isPM(a){return (/^(ցերեկվա|երեկոյան)$/.test(a));},meridiem:function meridiem(a){return 4>a?"գիշերվա":12>a?"առավոտվա":17>a?"ցերեկվա":"երեկոյան";},ordinalParse:/\d{1,2}|\d{1,2}-(ին|րդ)/,ordinal:function ordinal(a,b){switch(b){case "DDD":case "w":case "W":case "DDDo":return 1===a?a+"-ին":a+"-րդ";default:return a;}},week:{dow:1,doy:7}}),bg.defineLocale("id",{months:"Januari_Februari_Maret_April_Mei_Juni_Juli_Agustus_September_Oktober_November_Desember".split("_"),monthsShort:"Jan_Feb_Mar_Apr_Mei_Jun_Jul_Ags_Sep_Okt_Nov_Des".split("_"),weekdays:"Minggu_Senin_Selasa_Rabu_Kamis_Jumat_Sabtu".split("_"),weekdaysShort:"Min_Sen_Sel_Rab_Kam_Jum_Sab".split("_"),weekdaysMin:"Mg_Sn_Sl_Rb_Km_Jm_Sb".split("_"),longDateFormat:{LT:"HH.mm",LTS:"HH.mm.ss",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY [pukul] HH.mm",LLLL:"dddd, D MMMM YYYY [pukul] HH.mm"},meridiemParse:/pagi|siang|sore|malam/,meridiemHour:function meridiemHour(a,b){return 12===a&&(a=0),"pagi"===b?a:"siang"===b?a>=11?a:a+12:"sore"===b||"malam"===b?a+12:void 0;},meridiem:function meridiem(a,b,c){return 11>a?"pagi":15>a?"siang":19>a?"sore":"malam";},calendar:{sameDay:"[Hari ini pukul] LT",nextDay:"[Besok pukul] LT",nextWeek:"dddd [pukul] LT",lastDay:"[Kemarin pukul] LT",lastWeek:"dddd [lalu pukul] LT",sameElse:"L"},relativeTime:{future:"dalam %s",past:"%s yang lalu",s:"beberapa detik",m:"semenit",mm:"%d menit",h:"sejam",hh:"%d jam",d:"sehari",dd:"%d hari",M:"sebulan",MM:"%d bulan",y:"setahun",yy:"%d tahun"},week:{dow:1,doy:7}}),bg.defineLocale("is",{months:"janúar_febrúar_mars_apríl_maí_júní_júlí_ágúst_september_október_nóvember_desember".split("_"),monthsShort:"jan_feb_mar_apr_maí_jún_júl_ágú_sep_okt_nóv_des".split("_"),weekdays:"sunnudagur_mánudagur_þriðjudagur_miðvikudagur_fimmtudagur_föstudagur_laugardagur".split("_"),weekdaysShort:"sun_mán_þri_mið_fim_fös_lau".split("_"),weekdaysMin:"Su_Má_Þr_Mi_Fi_Fö_La".split("_"),longDateFormat:{LT:"H:mm",LTS:"H:mm:ss",L:"DD.MM.YYYY",LL:"D. MMMM YYYY",LLL:"D. MMMM YYYY [kl.] H:mm",LLLL:"dddd, D. MMMM YYYY [kl.] H:mm"},calendar:{sameDay:"[í dag kl.] LT",nextDay:"[á morgun kl.] LT",nextWeek:"dddd [kl.] LT",lastDay:"[í gær kl.] LT",lastWeek:"[síðasta] dddd [kl.] LT",sameElse:"L"},relativeTime:{future:"eftir %s",past:"fyrir %s síðan",s:yd,m:yd,mm:yd,h:"klukkustund",hh:yd,d:yd,dd:yd,M:yd,MM:yd,y:yd,yy:yd},ordinalParse:/\d{1,2}\./,ordinal:"%d.",week:{dow:1,doy:4}}),bg.defineLocale("it",{months:"gennaio_febbraio_marzo_aprile_maggio_giugno_luglio_agosto_settembre_ottobre_novembre_dicembre".split("_"),monthsShort:"gen_feb_mar_apr_mag_giu_lug_ago_set_ott_nov_dic".split("_"),weekdays:"Domenica_Lunedì_Martedì_Mercoledì_Giovedì_Venerdì_Sabato".split("_"),weekdaysShort:"Dom_Lun_Mar_Mer_Gio_Ven_Sab".split("_"),weekdaysMin:"Do_Lu_Ma_Me_Gi_Ve_Sa".split("_"),longDateFormat:{LT:"HH:mm",LTS:"HH:mm:ss",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY HH:mm",LLLL:"dddd, D MMMM YYYY HH:mm"},calendar:{sameDay:"[Oggi alle] LT",nextDay:"[Domani alle] LT",nextWeek:"dddd [alle] LT",lastDay:"[Ieri alle] LT",lastWeek:function lastWeek(){switch(this.day()){case 0:return "[la scorsa] dddd [alle] LT";default:return "[lo scorso] dddd [alle] LT";}},sameElse:"L"},relativeTime:{future:function future(a){return (/^[0-9].+$/.test(a)?"tra":"in")+" "+a;},past:"%s fa",s:"alcuni secondi",m:"un minuto",mm:"%d minuti",h:"un'ora",hh:"%d ore",d:"un giorno",dd:"%d giorni",M:"un mese",MM:"%d mesi",y:"un anno",yy:"%d anni"},ordinalParse:/\d{1,2}º/,ordinal:"%dº",week:{dow:1,doy:4}}),bg.defineLocale("ja",{months:"1月_2月_3月_4月_5月_6月_7月_8月_9月_10月_11月_12月".split("_"),monthsShort:"1月_2月_3月_4月_5月_6月_7月_8月_9月_10月_11月_12月".split("_"),weekdays:"日曜日_月曜日_火曜日_水曜日_木曜日_金曜日_土曜日".split("_"),weekdaysShort:"日_月_火_水_木_金_土".split("_"),weekdaysMin:"日_月_火_水_木_金_土".split("_"),longDateFormat:{LT:"Ah時m分",LTS:"Ah時m分s秒",L:"YYYY/MM/DD",LL:"YYYY年M月D日",LLL:"YYYY年M月D日Ah時m分",LLLL:"YYYY年M月D日Ah時m分 dddd"},meridiemParse:/午前|午後/i,isPM:function isPM(a){return "午後"===a;},meridiem:function meridiem(a,b,c){return 12>a?"午前":"午後";},calendar:{sameDay:"[今日] LT",nextDay:"[明日] LT",nextWeek:"[来週]dddd LT",lastDay:"[昨日] LT",lastWeek:"[前週]dddd LT",sameElse:"L"},ordinalParse:/\d{1,2}日/,ordinal:function ordinal(a,b){switch(b){case "d":case "D":case "DDD":return a+"日";default:return a;}},relativeTime:{future:"%s後",past:"%s前",s:"数秒",m:"1分",mm:"%d分",h:"1時間",hh:"%d時間",d:"1日",dd:"%d日",M:"1ヶ月",MM:"%dヶ月",y:"1年",yy:"%d年"}}),bg.defineLocale("jv",{months:"Januari_Februari_Maret_April_Mei_Juni_Juli_Agustus_September_Oktober_Nopember_Desember".split("_"),monthsShort:"Jan_Feb_Mar_Apr_Mei_Jun_Jul_Ags_Sep_Okt_Nop_Des".split("_"),weekdays:"Minggu_Senen_Seloso_Rebu_Kemis_Jemuwah_Septu".split("_"),weekdaysShort:"Min_Sen_Sel_Reb_Kem_Jem_Sep".split("_"),weekdaysMin:"Mg_Sn_Sl_Rb_Km_Jm_Sp".split("_"),longDateFormat:{LT:"HH.mm",LTS:"HH.mm.ss",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY [pukul] HH.mm",LLLL:"dddd, D MMMM YYYY [pukul] HH.mm"},meridiemParse:/enjing|siyang|sonten|ndalu/,meridiemHour:function meridiemHour(a,b){return 12===a&&(a=0),"enjing"===b?a:"siyang"===b?a>=11?a:a+12:"sonten"===b||"ndalu"===b?a+12:void 0;},meridiem:function meridiem(a,b,c){return 11>a?"enjing":15>a?"siyang":19>a?"sonten":"ndalu";},calendar:{sameDay:"[Dinten puniko pukul] LT",nextDay:"[Mbenjang pukul] LT",nextWeek:"dddd [pukul] LT",lastDay:"[Kala wingi pukul] LT",lastWeek:"dddd [kepengker pukul] LT",sameElse:"L"},relativeTime:{future:"wonten ing %s",past:"%s ingkang kepengker",s:"sawetawis detik",m:"setunggal menit",mm:"%d menit",h:"setunggal jam",hh:"%d jam",d:"sedinten",dd:"%d dinten",M:"sewulan",MM:"%d wulan",y:"setaun",yy:"%d taun"},week:{dow:1,doy:7}}),bg.defineLocale("ka",{months:{standalone:"იანვარი_თებერვალი_მარტი_აპრილი_მაისი_ივნისი_ივლისი_აგვისტო_სექტემბერი_ოქტომბერი_ნოემბერი_დეკემბერი".split("_"),format:"იანვარს_თებერვალს_მარტს_აპრილის_მაისს_ივნისს_ივლისს_აგვისტს_სექტემბერს_ოქტომბერს_ნოემბერს_დეკემბერს".split("_")},monthsShort:"იან_თებ_მარ_აპრ_მაი_ივნ_ივლ_აგვ_სექ_ოქტ_ნოე_დეკ".split("_"),weekdays:{standalone:"კვირა_ორშაბათი_სამშაბათი_ოთხშაბათი_ხუთშაბათი_პარასკევი_შაბათი".split("_"),format:"კვირას_ორშაბათს_სამშაბათს_ოთხშაბათს_ხუთშაბათს_პარასკევს_შაბათს".split("_"),isFormat:/(წინა|შემდეგ)/},weekdaysShort:"კვი_ორშ_სამ_ოთხ_ხუთ_პარ_შაბ".split("_"),weekdaysMin:"კვ_ორ_სა_ოთ_ხუ_პა_შა".split("_"),longDateFormat:{LT:"h:mm A",LTS:"h:mm:ss A",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY h:mm A",LLLL:"dddd, D MMMM YYYY h:mm A"},calendar:{sameDay:"[დღეს] LT[-ზე]",nextDay:"[ხვალ] LT[-ზე]",lastDay:"[გუშინ] LT[-ზე]",nextWeek:"[შემდეგ] dddd LT[-ზე]",lastWeek:"[წინა] dddd LT-ზე",sameElse:"L"},relativeTime:{future:function future(a){return (/(წამი|წუთი|საათი|წელი)/.test(a)?a.replace(/ი$/,"ში"):a+"ში");},past:function past(a){return (/(წამი|წუთი|საათი|დღე|თვე)/.test(a)?a.replace(/(ი|ე)$/,"ის წინ"):/წელი/.test(a)?a.replace(/წელი$/,"წლის წინ"):void 0);},s:"რამდენიმე წამი",m:"წუთი",mm:"%d წუთი",h:"საათი",hh:"%d საათი",d:"დღე",dd:"%d დღე",M:"თვე",MM:"%d თვე",y:"წელი",yy:"%d წელი"},ordinalParse:/0|1-ლი|მე-\d{1,2}|\d{1,2}-ე/,ordinal:function ordinal(a){return 0===a?a:1===a?a+"-ლი":20>a||100>=a&&a%20===0||a%100===0?"მე-"+a:a+"-ე";},week:{dow:1,doy:7}}),{0:"-ші",1:"-ші",2:"-ші",3:"-ші",4:"-ші",5:"-ші",6:"-шы",7:"-ші",8:"-ші",9:"-шы",10:"-шы",20:"-шы",30:"-шы",40:"-шы",50:"-ші",60:"-шы",70:"-ші",80:"-ші",90:"-шы",100:"-ші"}),Kg=(bg.defineLocale("kk",{months:"қаңтар_ақпан_наурыз_сәуір_мамыр_маусым_шілде_тамыз_қыркүйек_қазан_қараша_желтоқсан".split("_"),monthsShort:"қаң_ақп_нау_сәу_мам_мау_шіл_там_қыр_қаз_қар_жел".split("_"),weekdays:"жексенбі_дүйсенбі_сейсенбі_сәрсенбі_бейсенбі_жұма_сенбі".split("_"),weekdaysShort:"жек_дүй_сей_сәр_бей_жұм_сен".split("_"),weekdaysMin:"жк_дй_сй_ср_бй_жм_сн".split("_"),longDateFormat:{LT:"HH:mm",LTS:"HH:mm:ss",L:"DD.MM.YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY HH:mm",LLLL:"dddd, D MMMM YYYY HH:mm"},calendar:{sameDay:"[Бүгін сағат] LT",nextDay:"[Ертең сағат] LT",nextWeek:"dddd [сағат] LT",lastDay:"[Кеше сағат] LT",lastWeek:"[Өткен аптаның] dddd [сағат] LT",sameElse:"L"},relativeTime:{future:"%s ішінде",past:"%s бұрын",s:"бірнеше секунд",m:"бір минут",mm:"%d минут",h:"бір сағат",hh:"%d сағат",d:"бір күн",dd:"%d күн",M:"бір ай",MM:"%d ай",y:"бір жыл",yy:"%d жыл"},ordinalParse:/\d{1,2}-(ші|шы)/,ordinal:function ordinal(a){var b=a%10,c=a>=100?100:null;return a+(Jg[a]||Jg[b]||Jg[c]);},week:{dow:1,doy:7}}),bg.defineLocale("km",{months:"មករា_កុម្ភៈ_មីនា_មេសា_ឧសភា_មិថុនា_កក្កដា_សីហា_កញ្ញា_តុលា_វិច្ឆិកា_ធ្នូ".split("_"),monthsShort:"មករា_កុម្ភៈ_មីនា_មេសា_ឧសភា_មិថុនា_កក្កដា_សីហា_កញ្ញា_តុលា_វិច្ឆិកា_ធ្នូ".split("_"),weekdays:"អាទិត្យ_ច័ន្ទ_អង្គារ_ពុធ_ព្រហស្បតិ៍_សុក្រ_សៅរ៍".split("_"),weekdaysShort:"អាទិត្យ_ច័ន្ទ_អង្គារ_ពុធ_ព្រហស្បតិ៍_សុក្រ_សៅរ៍".split("_"),weekdaysMin:"អាទិត្យ_ច័ន្ទ_អង្គារ_ពុធ_ព្រហស្បតិ៍_សុក្រ_សៅរ៍".split("_"),longDateFormat:{LT:"HH:mm",LTS:"HH:mm:ss",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY HH:mm",LLLL:"dddd, D MMMM YYYY HH:mm"},calendar:{sameDay:"[ថ្ងៃនេះ ម៉ោង] LT",nextDay:"[ស្អែក ម៉ោង] LT",nextWeek:"dddd [ម៉ោង] LT",lastDay:"[ម្សិលមិញ ម៉ោង] LT",lastWeek:"dddd [សប្តាហ៍មុន] [ម៉ោង] LT",sameElse:"L"},relativeTime:{future:"%sទៀត",past:"%sមុន",s:"ប៉ុន្មានវិនាទី",m:"មួយនាទី",mm:"%d នាទី",h:"មួយម៉ោង",hh:"%d ម៉ោង",d:"មួយថ្ងៃ",dd:"%d ថ្ងៃ",M:"មួយខែ",MM:"%d ខែ",y:"មួយឆ្នាំ",yy:"%d ឆ្នាំ"},week:{dow:1,doy:4}}),bg.defineLocale("ko",{months:"1월_2월_3월_4월_5월_6월_7월_8월_9월_10월_11월_12월".split("_"),monthsShort:"1월_2월_3월_4월_5월_6월_7월_8월_9월_10월_11월_12월".split("_"),weekdays:"일요일_월요일_화요일_수요일_목요일_금요일_토요일".split("_"),weekdaysShort:"일_월_화_수_목_금_토".split("_"),weekdaysMin:"일_월_화_수_목_금_토".split("_"),longDateFormat:{LT:"A h시 m분",LTS:"A h시 m분 s초",L:"YYYY.MM.DD",LL:"YYYY년 MMMM D일",LLL:"YYYY년 MMMM D일 A h시 m분",LLLL:"YYYY년 MMMM D일 dddd A h시 m분"},calendar:{sameDay:"오늘 LT",nextDay:"내일 LT",nextWeek:"dddd LT",lastDay:"어제 LT",lastWeek:"지난주 dddd LT",sameElse:"L"},relativeTime:{future:"%s 후",past:"%s 전",s:"몇 초",ss:"%d초",m:"일분",mm:"%d분",h:"한 시간",hh:"%d시간",d:"하루",dd:"%d일",M:"한 달",MM:"%d달",y:"일 년",yy:"%d년"},ordinalParse:/\d{1,2}일/,ordinal:"%d일",meridiemParse:/오전|오후/,isPM:function isPM(a){return "오후"===a;},meridiem:function meridiem(a,b,c){return 12>a?"오전":"오후";}}),{0:"-чү",1:"-чи",2:"-чи",3:"-чү",4:"-чү",5:"-чи",6:"-чы",7:"-чи",8:"-чи",9:"-чу",10:"-чу",20:"-чы",30:"-чу",40:"-чы",50:"-чү",60:"-чы",70:"-чи",80:"-чи",90:"-чу",100:"-чү"}),Lg=(bg.defineLocale("ky",{months:"январь_февраль_март_апрель_май_июнь_июль_август_сентябрь_октябрь_ноябрь_декабрь".split("_"),monthsShort:"янв_фев_март_апр_май_июнь_июль_авг_сен_окт_ноя_дек".split("_"),weekdays:"Жекшемби_Дүйшөмбү_Шейшемби_Шаршемби_Бейшемби_Жума_Ишемби".split("_"),weekdaysShort:"Жек_Дүй_Шей_Шар_Бей_Жум_Ише".split("_"),weekdaysMin:"Жк_Дй_Шй_Шр_Бй_Жм_Иш".split("_"),longDateFormat:{LT:"HH:mm",LTS:"HH:mm:ss",L:"DD.MM.YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY HH:mm",LLLL:"dddd, D MMMM YYYY HH:mm"},calendar:{sameDay:"[Бүгүн саат] LT",nextDay:"[Эртең саат] LT",nextWeek:"dddd [саат] LT",lastDay:"[Кече саат] LT",lastWeek:"[Өткен аптанын] dddd [күнү] [саат] LT",sameElse:"L"},relativeTime:{future:"%s ичинде",past:"%s мурун",s:"бирнече секунд",m:"бир мүнөт",mm:"%d мүнөт",h:"бир саат",hh:"%d саат",d:"бир күн",dd:"%d күн",M:"бир ай",MM:"%d ай",y:"бир жыл",yy:"%d жыл"},ordinalParse:/\d{1,2}-(чи|чы|чү|чу)/,ordinal:function ordinal(a){var b=a%10,c=a>=100?100:null;return a+(Kg[a]||Kg[b]||Kg[c]);},week:{dow:1,doy:7}}),bg.defineLocale("lb",{months:"Januar_Februar_Mäerz_Abrëll_Mee_Juni_Juli_August_September_Oktober_November_Dezember".split("_"),monthsShort:"Jan._Febr._Mrz._Abr._Mee_Jun._Jul._Aug._Sept._Okt._Nov._Dez.".split("_"),monthsParseExact:!0,weekdays:"Sonndeg_Méindeg_Dënschdeg_Mëttwoch_Donneschdeg_Freideg_Samschdeg".split("_"),weekdaysShort:"So._Mé._Dë._Më._Do._Fr._Sa.".split("_"),weekdaysMin:"So_Mé_Dë_Më_Do_Fr_Sa".split("_"),weekdaysParseExact:!0,longDateFormat:{LT:"H:mm [Auer]",LTS:"H:mm:ss [Auer]",L:"DD.MM.YYYY",LL:"D. MMMM YYYY",LLL:"D. MMMM YYYY H:mm [Auer]",LLLL:"dddd, D. MMMM YYYY H:mm [Auer]"},calendar:{sameDay:"[Haut um] LT",sameElse:"L",nextDay:"[Muer um] LT",nextWeek:"dddd [um] LT",lastDay:"[Gëschter um] LT",lastWeek:function lastWeek(){switch(this.day()){case 2:case 4:return "[Leschten] dddd [um] LT";default:return "[Leschte] dddd [um] LT";}}},relativeTime:{future:Ad,past:Bd,s:"e puer Sekonnen",m:zd,mm:"%d Minutten",h:zd,hh:"%d Stonnen",d:zd,dd:"%d Deeg",M:zd,MM:"%d Méint",y:zd,yy:"%d Joer"},ordinalParse:/\d{1,2}\./,ordinal:"%d.",week:{dow:1,doy:4}}),bg.defineLocale("lo",{months:"ມັງກອນ_ກຸມພາ_ມີນາ_ເມສາ_ພຶດສະພາ_ມິຖຸນາ_ກໍລະກົດ_ສິງຫາ_ກັນຍາ_ຕຸລາ_ພະຈິກ_ທັນວາ".split("_"),monthsShort:"ມັງກອນ_ກຸມພາ_ມີນາ_ເມສາ_ພຶດສະພາ_ມິຖຸນາ_ກໍລະກົດ_ສິງຫາ_ກັນຍາ_ຕຸລາ_ພະຈິກ_ທັນວາ".split("_"),weekdays:"ອາທິດ_ຈັນ_ອັງຄານ_ພຸດ_ພະຫັດ_ສຸກ_ເສົາ".split("_"),weekdaysShort:"ທິດ_ຈັນ_ອັງຄານ_ພຸດ_ພະຫັດ_ສຸກ_ເສົາ".split("_"),weekdaysMin:"ທ_ຈ_ອຄ_ພ_ພຫ_ສກ_ສ".split("_"),weekdaysParseExact:!0,longDateFormat:{LT:"HH:mm",LTS:"HH:mm:ss",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY HH:mm",LLLL:"ວັນdddd D MMMM YYYY HH:mm"},meridiemParse:/ຕອນເຊົ້າ|ຕອນແລງ/,isPM:function isPM(a){return "ຕອນແລງ"===a;},meridiem:function meridiem(a,b,c){return 12>a?"ຕອນເຊົ້າ":"ຕອນແລງ";},calendar:{sameDay:"[ມື້ນີ້ເວລາ] LT",nextDay:"[ມື້ອື່ນເວລາ] LT",nextWeek:"[ວັນ]dddd[ໜ້າເວລາ] LT",lastDay:"[ມື້ວານນີ້ເວລາ] LT",lastWeek:"[ວັນ]dddd[ແລ້ວນີ້ເວລາ] LT",sameElse:"L"},relativeTime:{future:"ອີກ %s",past:"%sຜ່ານມາ",s:"ບໍ່ເທົ່າໃດວິນາທີ",m:"1 ນາທີ",mm:"%d ນາທີ",h:"1 ຊົ່ວໂມງ",hh:"%d ຊົ່ວໂມງ",d:"1 ມື້",dd:"%d ມື້",M:"1 ເດືອນ",MM:"%d ເດືອນ",y:"1 ປີ",yy:"%d ປີ"},ordinalParse:/(ທີ່)\d{1,2}/,ordinal:function ordinal(a){return "ທີ່"+a;}}),{m:"minutė_minutės_minutę",mm:"minutės_minučių_minutes",h:"valanda_valandos_valandą",hh:"valandos_valandų_valandas",d:"diena_dienos_dieną",dd:"dienos_dienų_dienas",M:"mėnuo_mėnesio_mėnesį",MM:"mėnesiai_mėnesių_mėnesius",y:"metai_metų_metus",yy:"metai_metų_metus"}),Mg=(bg.defineLocale("lt",{months:{format:"sausio_vasario_kovo_balandžio_gegužės_birželio_liepos_rugpjūčio_rugsėjo_spalio_lapkričio_gruodžio".split("_"),standalone:"sausis_vasaris_kovas_balandis_gegužė_birželis_liepa_rugpjūtis_rugsėjis_spalis_lapkritis_gruodis".split("_")},monthsShort:"sau_vas_kov_bal_geg_bir_lie_rgp_rgs_spa_lap_grd".split("_"),weekdays:{format:"sekmadienį_pirmadienį_antradienį_trečiadienį_ketvirtadienį_penktadienį_šeštadienį".split("_"),standalone:"sekmadienis_pirmadienis_antradienis_trečiadienis_ketvirtadienis_penktadienis_šeštadienis".split("_"),isFormat:/dddd HH:mm/},weekdaysShort:"Sek_Pir_Ant_Tre_Ket_Pen_Šeš".split("_"),weekdaysMin:"S_P_A_T_K_Pn_Š".split("_"),weekdaysParseExact:!0,longDateFormat:{LT:"HH:mm",LTS:"HH:mm:ss",L:"YYYY-MM-DD",LL:"YYYY [m.] MMMM D [d.]",LLL:"YYYY [m.] MMMM D [d.], HH:mm [val.]",LLLL:"YYYY [m.] MMMM D [d.], dddd, HH:mm [val.]",l:"YYYY-MM-DD",ll:"YYYY [m.] MMMM D [d.]",lll:"YYYY [m.] MMMM D [d.], HH:mm [val.]",llll:"YYYY [m.] MMMM D [d.], ddd, HH:mm [val.]"},calendar:{sameDay:"[Šiandien] LT",nextDay:"[Rytoj] LT",nextWeek:"dddd LT",lastDay:"[Vakar] LT",lastWeek:"[Praėjusį] dddd LT",sameElse:"L"},relativeTime:{future:"po %s",past:"prieš %s",s:Dd,m:Ed,mm:Hd,h:Ed,hh:Hd,d:Ed,dd:Hd,M:Ed,MM:Hd,y:Ed,yy:Hd},ordinalParse:/\d{1,2}-oji/,ordinal:function ordinal(a){return a+"-oji";},week:{dow:1,doy:4}}),{m:"minūtes_minūtēm_minūte_minūtes".split("_"),mm:"minūtes_minūtēm_minūte_minūtes".split("_"),h:"stundas_stundām_stunda_stundas".split("_"),hh:"stundas_stundām_stunda_stundas".split("_"),d:"dienas_dienām_diena_dienas".split("_"),dd:"dienas_dienām_diena_dienas".split("_"),M:"mēneša_mēnešiem_mēnesis_mēneši".split("_"),MM:"mēneša_mēnešiem_mēnesis_mēneši".split("_"),y:"gada_gadiem_gads_gadi".split("_"),yy:"gada_gadiem_gads_gadi".split("_")}),Ng=(bg.defineLocale("lv",{months:"janvāris_februāris_marts_aprīlis_maijs_jūnijs_jūlijs_augusts_septembris_oktobris_novembris_decembris".split("_"),monthsShort:"jan_feb_mar_apr_mai_jūn_jūl_aug_sep_okt_nov_dec".split("_"),weekdays:"svētdiena_pirmdiena_otrdiena_trešdiena_ceturtdiena_piektdiena_sestdiena".split("_"),weekdaysShort:"Sv_P_O_T_C_Pk_S".split("_"),weekdaysMin:"Sv_P_O_T_C_Pk_S".split("_"),weekdaysParseExact:!0,longDateFormat:{LT:"HH:mm",LTS:"HH:mm:ss",L:"DD.MM.YYYY.",LL:"YYYY. [gada] D. MMMM",LLL:"YYYY. [gada] D. MMMM, HH:mm",LLLL:"YYYY. [gada] D. MMMM, dddd, HH:mm"},calendar:{sameDay:"[Šodien pulksten] LT",nextDay:"[Rīt pulksten] LT",nextWeek:"dddd [pulksten] LT",lastDay:"[Vakar pulksten] LT",lastWeek:"[Pagājušā] dddd [pulksten] LT",sameElse:"L"},relativeTime:{future:"pēc %s",past:"pirms %s",s:Ld,m:Kd,mm:Jd,h:Kd,hh:Jd,d:Kd,dd:Jd,M:Kd,MM:Jd,y:Kd,yy:Jd},ordinalParse:/\d{1,2}\./,ordinal:"%d.",week:{dow:1,doy:4}}),{words:{m:["jedan minut","jednog minuta"],mm:["minut","minuta","minuta"],h:["jedan sat","jednog sata"],hh:["sat","sata","sati"],dd:["dan","dana","dana"],MM:["mjesec","mjeseca","mjeseci"],yy:["godina","godine","godina"]},correctGrammaticalCase:function correctGrammaticalCase(a,b){return 1===a?b[0]:a>=2&&4>=a?b[1]:b[2];},translate:function translate(a,b,c){var d=Ng.words[c];return 1===c.length?b?d[0]:d[1]:a+" "+Ng.correctGrammaticalCase(a,d);}}),Og=(bg.defineLocale("me",{months:"januar_februar_mart_april_maj_jun_jul_avgust_septembar_oktobar_novembar_decembar".split("_"),monthsShort:"jan._feb._mar._apr._maj_jun_jul_avg._sep._okt._nov._dec.".split("_"),monthsParseExact:!0,weekdays:"nedjelja_ponedjeljak_utorak_srijeda_četvrtak_petak_subota".split("_"),weekdaysShort:"ned._pon._uto._sri._čet._pet._sub.".split("_"),weekdaysMin:"ne_po_ut_sr_če_pe_su".split("_"),weekdaysParseExact:!0,longDateFormat:{LT:"H:mm",LTS:"H:mm:ss",L:"DD. MM. YYYY",LL:"D. MMMM YYYY",LLL:"D. MMMM YYYY H:mm",LLLL:"dddd, D. MMMM YYYY H:mm"},calendar:{sameDay:"[danas u] LT",nextDay:"[sjutra u] LT",nextWeek:function nextWeek(){switch(this.day()){case 0:return "[u] [nedjelju] [u] LT";case 3:return "[u] [srijedu] [u] LT";case 6:return "[u] [subotu] [u] LT";case 1:case 2:case 4:case 5:return "[u] dddd [u] LT";}},lastDay:"[juče u] LT",lastWeek:function lastWeek(){var a=["[prošle] [nedjelje] [u] LT","[prošlog] [ponedjeljka] [u] LT","[prošlog] [utorka] [u] LT","[prošle] [srijede] [u] LT","[prošlog] [četvrtka] [u] LT","[prošlog] [petka] [u] LT","[prošle] [subote] [u] LT"];return a[this.day()];},sameElse:"L"},relativeTime:{future:"za %s",past:"prije %s",s:"nekoliko sekundi",m:Ng.translate,mm:Ng.translate,h:Ng.translate,hh:Ng.translate,d:"dan",dd:Ng.translate,M:"mjesec",MM:Ng.translate,y:"godinu",yy:Ng.translate},ordinalParse:/\d{1,2}\./,ordinal:"%d.",week:{dow:1,doy:7}}),bg.defineLocale("mk",{months:"јануари_февруари_март_април_мај_јуни_јули_август_септември_октомври_ноември_декември".split("_"),monthsShort:"јан_фев_мар_апр_мај_јун_јул_авг_сеп_окт_ное_дек".split("_"),weekdays:"недела_понеделник_вторник_среда_четврток_петок_сабота".split("_"),weekdaysShort:"нед_пон_вто_сре_чет_пет_саб".split("_"),weekdaysMin:"нe_пo_вт_ср_че_пе_сa".split("_"),longDateFormat:{LT:"H:mm",LTS:"H:mm:ss",L:"D.MM.YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY H:mm",LLLL:"dddd, D MMMM YYYY H:mm"},calendar:{sameDay:"[Денес во] LT",nextDay:"[Утре во] LT",nextWeek:"[Во] dddd [во] LT",lastDay:"[Вчера во] LT",lastWeek:function lastWeek(){switch(this.day()){case 0:case 3:case 6:return "[Изминатата] dddd [во] LT";case 1:case 2:case 4:case 5:return "[Изминатиот] dddd [во] LT";}},sameElse:"L"},relativeTime:{future:"после %s",past:"пред %s",s:"неколку секунди",m:"минута",mm:"%d минути",h:"час",hh:"%d часа",d:"ден",dd:"%d дена",M:"месец",MM:"%d месеци",y:"година",yy:"%d години"},ordinalParse:/\d{1,2}-(ев|ен|ти|ви|ри|ми)/,ordinal:function ordinal(a){var b=a%10,c=a%100;return 0===a?a+"-ев":0===c?a+"-ен":c>10&&20>c?a+"-ти":1===b?a+"-ви":2===b?a+"-ри":7===b||8===b?a+"-ми":a+"-ти";},week:{dow:1,doy:7}}),bg.defineLocale("ml",{months:"ജനുവരി_ഫെബ്രുവരി_മാർച്ച്_ഏപ്രിൽ_മേയ്_ജൂൺ_ജൂലൈ_ഓഗസ്റ്റ്_സെപ്റ്റംബർ_ഒക്ടോബർ_നവംബർ_ഡിസംബർ".split("_"),monthsShort:"ജനു._ഫെബ്രു._മാർ._ഏപ്രി._മേയ്_ജൂൺ_ജൂലൈ._ഓഗ._സെപ്റ്റ._ഒക്ടോ._നവം._ഡിസം.".split("_"),monthsParseExact:!0,weekdays:"ഞായറാഴ്ച_തിങ്കളാഴ്ച_ചൊവ്വാഴ്ച_ബുധനാഴ്ച_വ്യാഴാഴ്ച_വെള്ളിയാഴ്ച_ശനിയാഴ്ച".split("_"),weekdaysShort:"ഞായർ_തിങ്കൾ_ചൊവ്വ_ബുധൻ_വ്യാഴം_വെള്ളി_ശനി".split("_"),weekdaysMin:"ഞാ_തി_ചൊ_ബു_വ്യാ_വെ_ശ".split("_"),longDateFormat:{LT:"A h:mm -നു",LTS:"A h:mm:ss -നു",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY, A h:mm -നു",LLLL:"dddd, D MMMM YYYY, A h:mm -നു"},calendar:{sameDay:"[ഇന്ന്] LT",nextDay:"[നാളെ] LT",nextWeek:"dddd, LT",lastDay:"[ഇന്നലെ] LT",lastWeek:"[കഴിഞ്ഞ] dddd, LT",sameElse:"L"},relativeTime:{future:"%s കഴിഞ്ഞ്",past:"%s മുൻപ്",s:"അൽപ നിമിഷങ്ങൾ",m:"ഒരു മിനിറ്റ്",mm:"%d മിനിറ്റ്",h:"ഒരു മണിക്കൂർ",hh:"%d മണിക്കൂർ",d:"ഒരു ദിവസം",dd:"%d ദിവസം",M:"ഒരു മാസം",MM:"%d മാസം",y:"ഒരു വർഷം",yy:"%d വർഷം"},meridiemParse:/രാത്രി|രാവിലെ|ഉച്ച കഴിഞ്ഞ്|വൈകുന്നേരം|രാത്രി/i,meridiemHour:function meridiemHour(a,b){return 12===a&&(a=0),"രാത്രി"===b&&a>=4||"ഉച്ച കഴിഞ്ഞ്"===b||"വൈകുന്നേരം"===b?a+12:a;},meridiem:function meridiem(a,b,c){return 4>a?"രാത്രി":12>a?"രാവിലെ":17>a?"ഉച്ച കഴിഞ്ഞ്":20>a?"വൈകുന്നേരം":"രാത്രി";}}),{1:"१",2:"२",3:"३",4:"४",5:"५",6:"६",7:"७",8:"८",9:"९",0:"०"}),Pg={"१":"1","२":"2","३":"3","४":"4","५":"5","६":"6","७":"7","८":"8","९":"9","०":"0"},Qg=(bg.defineLocale("mr",{months:"जानेवारी_फेब्रुवारी_मार्च_एप्रिल_मे_जून_जुलै_ऑगस्ट_सप्टेंबर_ऑक्टोबर_नोव्हेंबर_डिसेंबर".split("_"),monthsShort:"जाने._फेब्रु._मार्च._एप्रि._मे._जून._जुलै._ऑग._सप्टें._ऑक्टो._नोव्हें._डिसें.".split("_"),monthsParseExact:!0,weekdays:"रविवार_सोमवार_मंगळवार_बुधवार_गुरूवार_शुक्रवार_शनिवार".split("_"),weekdaysShort:"रवि_सोम_मंगळ_बुध_गुरू_शुक्र_शनि".split("_"),weekdaysMin:"र_सो_मं_बु_गु_शु_श".split("_"),longDateFormat:{LT:"A h:mm वाजता",LTS:"A h:mm:ss वाजता",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY, A h:mm वाजता",LLLL:"dddd, D MMMM YYYY, A h:mm वाजता"},calendar:{sameDay:"[आज] LT",nextDay:"[उद्या] LT",nextWeek:"dddd, LT",lastDay:"[काल] LT",lastWeek:"[मागील] dddd, LT",sameElse:"L"},relativeTime:{future:"%sमध्ये",past:"%sपूर्वी",s:Md,m:Md,mm:Md,h:Md,hh:Md,d:Md,dd:Md,M:Md,MM:Md,y:Md,yy:Md},preparse:function preparse(a){return a.replace(/[१२३४५६७८९०]/g,function(a){return Pg[a];});},postformat:function postformat(a){return a.replace(/\d/g,function(a){return Og[a];});},meridiemParse:/रात्री|सकाळी|दुपारी|सायंकाळी/,meridiemHour:function meridiemHour(a,b){return 12===a&&(a=0),"रात्री"===b?4>a?a:a+12:"सकाळी"===b?a:"दुपारी"===b?a>=10?a:a+12:"सायंकाळी"===b?a+12:void 0;},meridiem:function meridiem(a,b,c){return 4>a?"रात्री":10>a?"सकाळी":17>a?"दुपारी":20>a?"सायंकाळी":"रात्री";},week:{dow:0,doy:6}}),bg.defineLocale("ms-my",{months:"Januari_Februari_Mac_April_Mei_Jun_Julai_Ogos_September_Oktober_November_Disember".split("_"),monthsShort:"Jan_Feb_Mac_Apr_Mei_Jun_Jul_Ogs_Sep_Okt_Nov_Dis".split("_"),weekdays:"Ahad_Isnin_Selasa_Rabu_Khamis_Jumaat_Sabtu".split("_"),weekdaysShort:"Ahd_Isn_Sel_Rab_Kha_Jum_Sab".split("_"),weekdaysMin:"Ah_Is_Sl_Rb_Km_Jm_Sb".split("_"),longDateFormat:{LT:"HH.mm",LTS:"HH.mm.ss",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY [pukul] HH.mm",LLLL:"dddd, D MMMM YYYY [pukul] HH.mm"},meridiemParse:/pagi|tengahari|petang|malam/,meridiemHour:function meridiemHour(a,b){return 12===a&&(a=0),"pagi"===b?a:"tengahari"===b?a>=11?a:a+12:"petang"===b||"malam"===b?a+12:void 0;},meridiem:function meridiem(a,b,c){return 11>a?"pagi":15>a?"tengahari":19>a?"petang":"malam";},calendar:{sameDay:"[Hari ini pukul] LT",nextDay:"[Esok pukul] LT",nextWeek:"dddd [pukul] LT",lastDay:"[Kelmarin pukul] LT",lastWeek:"dddd [lepas pukul] LT",sameElse:"L"},relativeTime:{future:"dalam %s",past:"%s yang lepas",s:"beberapa saat",m:"seminit",mm:"%d minit",h:"sejam",hh:"%d jam",d:"sehari",dd:"%d hari",M:"sebulan",MM:"%d bulan",y:"setahun",yy:"%d tahun"},week:{dow:1,doy:7}}),bg.defineLocale("ms",{months:"Januari_Februari_Mac_April_Mei_Jun_Julai_Ogos_September_Oktober_November_Disember".split("_"),monthsShort:"Jan_Feb_Mac_Apr_Mei_Jun_Jul_Ogs_Sep_Okt_Nov_Dis".split("_"),weekdays:"Ahad_Isnin_Selasa_Rabu_Khamis_Jumaat_Sabtu".split("_"),weekdaysShort:"Ahd_Isn_Sel_Rab_Kha_Jum_Sab".split("_"),weekdaysMin:"Ah_Is_Sl_Rb_Km_Jm_Sb".split("_"),longDateFormat:{LT:"HH.mm",LTS:"HH.mm.ss",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY [pukul] HH.mm",LLLL:"dddd, D MMMM YYYY [pukul] HH.mm"},meridiemParse:/pagi|tengahari|petang|malam/,meridiemHour:function meridiemHour(a,b){return 12===a&&(a=0),"pagi"===b?a:"tengahari"===b?a>=11?a:a+12:"petang"===b||"malam"===b?a+12:void 0;},meridiem:function meridiem(a,b,c){return 11>a?"pagi":15>a?"tengahari":19>a?"petang":"malam";},calendar:{sameDay:"[Hari ini pukul] LT",nextDay:"[Esok pukul] LT",nextWeek:"dddd [pukul] LT",lastDay:"[Kelmarin pukul] LT",lastWeek:"dddd [lepas pukul] LT",sameElse:"L"},relativeTime:{future:"dalam %s",past:"%s yang lepas",s:"beberapa saat",m:"seminit",mm:"%d minit",h:"sejam",hh:"%d jam",d:"sehari",dd:"%d hari",M:"sebulan",MM:"%d bulan",y:"setahun",yy:"%d tahun"},week:{dow:1,doy:7}}),{1:"၁",2:"၂",3:"၃",4:"၄",5:"၅",6:"၆",7:"၇",8:"၈",9:"၉",0:"၀"}),Rg={"၁":"1","၂":"2","၃":"3","၄":"4","၅":"5","၆":"6","၇":"7","၈":"8","၉":"9","၀":"0"},Sg=(bg.defineLocale("my",{months:"ဇန်နဝါရီ_ဖေဖော်ဝါရီ_မတ်_ဧပြီ_မေ_ဇွန်_ဇူလိုင်_သြဂုတ်_စက်တင်ဘာ_အောက်တိုဘာ_နိုဝင်ဘာ_ဒီဇင်ဘာ".split("_"),monthsShort:"ဇန်_ဖေ_မတ်_ပြီ_မေ_ဇွန်_လိုင်_သြ_စက်_အောက်_နို_ဒီ".split("_"),weekdays:"တနင်္ဂနွေ_တနင်္လာ_အင်္ဂါ_ဗုဒ္ဓဟူး_ကြာသပတေး_သောကြာ_စနေ".split("_"),weekdaysShort:"နွေ_လာ_ဂါ_ဟူး_ကြာ_သော_နေ".split("_"),weekdaysMin:"နွေ_လာ_ဂါ_ဟူး_ကြာ_သော_နေ".split("_"),longDateFormat:{LT:"HH:mm",LTS:"HH:mm:ss",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY HH:mm",LLLL:"dddd D MMMM YYYY HH:mm"},calendar:{sameDay:"[ယနေ.] LT [မှာ]",nextDay:"[မနက်ဖြန်] LT [မှာ]",nextWeek:"dddd LT [မှာ]",lastDay:"[မနေ.က] LT [မှာ]",lastWeek:"[ပြီးခဲ့သော] dddd LT [မှာ]",sameElse:"L"},relativeTime:{future:"လာမည့် %s မှာ",past:"လွန်ခဲ့သော %s က",s:"စက္ကန်.အနည်းငယ်",m:"တစ်မိနစ်",mm:"%d မိနစ်",h:"တစ်နာရီ",hh:"%d နာရီ",d:"တစ်ရက်",dd:"%d ရက်",M:"တစ်လ",MM:"%d လ",y:"တစ်နှစ်",yy:"%d နှစ်"},preparse:function preparse(a){return a.replace(/[၁၂၃၄၅၆၇၈၉၀]/g,function(a){return Rg[a];});},postformat:function postformat(a){return a.replace(/\d/g,function(a){return Qg[a];});},week:{dow:1,doy:4}}),bg.defineLocale("nb",{months:"januar_februar_mars_april_mai_juni_juli_august_september_oktober_november_desember".split("_"),monthsShort:"jan._feb._mars_april_mai_juni_juli_aug._sep._okt._nov._des.".split("_"),monthsParseExact:!0,weekdays:"søndag_mandag_tirsdag_onsdag_torsdag_fredag_lørdag".split("_"),weekdaysShort:"sø._ma._ti._on._to._fr._lø.".split("_"),weekdaysMin:"sø_ma_ti_on_to_fr_lø".split("_"),weekdaysParseExact:!0,longDateFormat:{LT:"HH:mm",LTS:"HH:mm:ss",L:"DD.MM.YYYY",LL:"D. MMMM YYYY",LLL:"D. MMMM YYYY [kl.] HH:mm",LLLL:"dddd D. MMMM YYYY [kl.] HH:mm"},calendar:{sameDay:"[i dag kl.] LT",nextDay:"[i morgen kl.] LT",nextWeek:"dddd [kl.] LT",lastDay:"[i går kl.] LT",lastWeek:"[forrige] dddd [kl.] LT",sameElse:"L"},relativeTime:{future:"om %s",past:"%s siden",s:"noen sekunder",m:"ett minutt",mm:"%d minutter",h:"en time",hh:"%d timer",d:"en dag",dd:"%d dager",M:"en måned",MM:"%d måneder",y:"ett år",yy:"%d år"},ordinalParse:/\d{1,2}\./,ordinal:"%d.",week:{dow:1,doy:4}}),{1:"१",2:"२",3:"३",4:"४",5:"५",6:"६",7:"७",8:"८",9:"९",0:"०"}),Tg={"१":"1","२":"2","३":"3","४":"4","५":"5","६":"6","७":"7","८":"8","९":"9","०":"0"},Ug=(bg.defineLocale("ne",{months:"जनवरी_फेब्रुवरी_मार्च_अप्रिल_मई_जुन_जुलाई_अगष्ट_सेप्टेम्बर_अक्टोबर_नोभेम्बर_डिसेम्बर".split("_"),monthsShort:"जन._फेब्रु._मार्च_अप्रि._मई_जुन_जुलाई._अग._सेप्ट._अक्टो._नोभे._डिसे.".split("_"),monthsParseExact:!0,weekdays:"आइतबार_सोमबार_मङ्गलबार_बुधबार_बिहिबार_शुक्रबार_शनिबार".split("_"),weekdaysShort:"आइत._सोम._मङ्गल._बुध._बिहि._शुक्र._शनि.".split("_"),weekdaysMin:"आ._सो._मं._बु._बि._शु._श.".split("_"),weekdaysParseExact:!0,longDateFormat:{LT:"Aको h:mm बजे",LTS:"Aको h:mm:ss बजे",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY, Aको h:mm बजे",LLLL:"dddd, D MMMM YYYY, Aको h:mm बजे"},preparse:function preparse(a){return a.replace(/[१२३४५६७८९०]/g,function(a){return Tg[a];});},postformat:function postformat(a){return a.replace(/\d/g,function(a){return Sg[a];});},meridiemParse:/राति|बिहान|दिउँसो|साँझ/,meridiemHour:function meridiemHour(a,b){return 12===a&&(a=0),"राति"===b?4>a?a:a+12:"बिहान"===b?a:"दिउँसो"===b?a>=10?a:a+12:"साँझ"===b?a+12:void 0;},meridiem:function meridiem(a,b,c){return 3>a?"राति":12>a?"बिहान":16>a?"दिउँसो":20>a?"साँझ":"राति";},calendar:{sameDay:"[आज] LT",nextDay:"[भोलि] LT",nextWeek:"[आउँदो] dddd[,] LT",lastDay:"[हिजो] LT",lastWeek:"[गएको] dddd[,] LT",sameElse:"L"},relativeTime:{future:"%sमा",past:"%s अगाडि",s:"केही क्षण",m:"एक मिनेट",mm:"%d मिनेट",h:"एक घण्टा",hh:"%d घण्टा",d:"एक दिन",dd:"%d दिन",M:"एक महिना",MM:"%d महिना",y:"एक बर्ष",yy:"%d बर्ष"},week:{dow:0,doy:6}}),"jan._feb._mrt._apr._mei_jun._jul._aug._sep._okt._nov._dec.".split("_")),Vg="jan_feb_mrt_apr_mei_jun_jul_aug_sep_okt_nov_dec".split("_"),Wg=(bg.defineLocale("nl",{months:"januari_februari_maart_april_mei_juni_juli_augustus_september_oktober_november_december".split("_"),monthsShort:function monthsShort(a,b){return (/-MMM-/.test(b)?Vg[a.month()]:Ug[a.month()]);},monthsParseExact:!0,weekdays:"zondag_maandag_dinsdag_woensdag_donderdag_vrijdag_zaterdag".split("_"),weekdaysShort:"zo._ma._di._wo._do._vr._za.".split("_"),weekdaysMin:"Zo_Ma_Di_Wo_Do_Vr_Za".split("_"),weekdaysParseExact:!0,longDateFormat:{LT:"HH:mm",LTS:"HH:mm:ss",L:"DD-MM-YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY HH:mm",LLLL:"dddd D MMMM YYYY HH:mm"},calendar:{sameDay:"[vandaag om] LT",nextDay:"[morgen om] LT",nextWeek:"dddd [om] LT",lastDay:"[gisteren om] LT",lastWeek:"[afgelopen] dddd [om] LT",sameElse:"L"},relativeTime:{future:"over %s",past:"%s geleden",s:"een paar seconden",m:"één minuut",mm:"%d minuten",h:"één uur",hh:"%d uur",d:"één dag",dd:"%d dagen",M:"één maand",MM:"%d maanden",y:"één jaar",yy:"%d jaar"},ordinalParse:/\d{1,2}(ste|de)/,ordinal:function ordinal(a){return a+(1===a||8===a||a>=20?"ste":"de");},week:{dow:1,doy:4}}),bg.defineLocale("nn",{months:"januar_februar_mars_april_mai_juni_juli_august_september_oktober_november_desember".split("_"),monthsShort:"jan_feb_mar_apr_mai_jun_jul_aug_sep_okt_nov_des".split("_"),weekdays:"sundag_måndag_tysdag_onsdag_torsdag_fredag_laurdag".split("_"),weekdaysShort:"sun_mån_tys_ons_tor_fre_lau".split("_"),weekdaysMin:"su_må_ty_on_to_fr_lø".split("_"),longDateFormat:{LT:"HH:mm",LTS:"HH:mm:ss",L:"DD.MM.YYYY",LL:"D. MMMM YYYY",LLL:"D. MMMM YYYY [kl.] H:mm",LLLL:"dddd D. MMMM YYYY [kl.] HH:mm"},calendar:{sameDay:"[I dag klokka] LT",nextDay:"[I morgon klokka] LT",nextWeek:"dddd [klokka] LT",lastDay:"[I går klokka] LT",lastWeek:"[Føregåande] dddd [klokka] LT",sameElse:"L"},relativeTime:{future:"om %s",past:"%s sidan",s:"nokre sekund",m:"eit minutt",mm:"%d minutt",h:"ein time",hh:"%d timar",d:"ein dag",dd:"%d dagar",M:"ein månad",MM:"%d månader",y:"eit år",yy:"%d år"},ordinalParse:/\d{1,2}\./,ordinal:"%d.",week:{dow:1,doy:4}}),{1:"੧",2:"੨",3:"੩",4:"੪",5:"੫",6:"੬",7:"੭",8:"੮",9:"੯",0:"੦"}),Xg={"੧":"1","੨":"2","੩":"3","੪":"4","੫":"5","੬":"6","੭":"7","੮":"8","੯":"9","੦":"0"},Yg=(bg.defineLocale("pa-in",{months:"ਜਨਵਰੀ_ਫ਼ਰਵਰੀ_ਮਾਰਚ_ਅਪ੍ਰੈਲ_ਮਈ_ਜੂਨ_ਜੁਲਾਈ_ਅਗਸਤ_ਸਤੰਬਰ_ਅਕਤੂਬਰ_ਨਵੰਬਰ_ਦਸੰਬਰ".split("_"),monthsShort:"ਜਨਵਰੀ_ਫ਼ਰਵਰੀ_ਮਾਰਚ_ਅਪ੍ਰੈਲ_ਮਈ_ਜੂਨ_ਜੁਲਾਈ_ਅਗਸਤ_ਸਤੰਬਰ_ਅਕਤੂਬਰ_ਨਵੰਬਰ_ਦਸੰਬਰ".split("_"),weekdays:"ਐਤਵਾਰ_ਸੋਮਵਾਰ_ਮੰਗਲਵਾਰ_ਬੁਧਵਾਰ_ਵੀਰਵਾਰ_ਸ਼ੁੱਕਰਵਾਰ_ਸ਼ਨੀਚਰਵਾਰ".split("_"),weekdaysShort:"ਐਤ_ਸੋਮ_ਮੰਗਲ_ਬੁਧ_ਵੀਰ_ਸ਼ੁਕਰ_ਸ਼ਨੀ".split("_"),weekdaysMin:"ਐਤ_ਸੋਮ_ਮੰਗਲ_ਬੁਧ_ਵੀਰ_ਸ਼ੁਕਰ_ਸ਼ਨੀ".split("_"),longDateFormat:{LT:"A h:mm ਵਜੇ",LTS:"A h:mm:ss ਵਜੇ",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY, A h:mm ਵਜੇ",LLLL:"dddd, D MMMM YYYY, A h:mm ਵਜੇ"},calendar:{sameDay:"[ਅਜ] LT",nextDay:"[ਕਲ] LT",nextWeek:"dddd, LT",lastDay:"[ਕਲ] LT",lastWeek:"[ਪਿਛਲੇ] dddd, LT",sameElse:"L"},relativeTime:{future:"%s ਵਿੱਚ",past:"%s ਪਿਛਲੇ",s:"ਕੁਝ ਸਕਿੰਟ",m:"ਇਕ ਮਿੰਟ",mm:"%d ਮਿੰਟ",h:"ਇੱਕ ਘੰਟਾ",hh:"%d ਘੰਟੇ",d:"ਇੱਕ ਦਿਨ",dd:"%d ਦਿਨ",M:"ਇੱਕ ਮਹੀਨਾ",MM:"%d ਮਹੀਨੇ",y:"ਇੱਕ ਸਾਲ",yy:"%d ਸਾਲ"},preparse:function preparse(a){return a.replace(/[੧੨੩੪੫੬੭੮੯੦]/g,function(a){return Xg[a];});},postformat:function postformat(a){return a.replace(/\d/g,function(a){return Wg[a];});},meridiemParse:/ਰਾਤ|ਸਵੇਰ|ਦੁਪਹਿਰ|ਸ਼ਾਮ/,meridiemHour:function meridiemHour(a,b){return 12===a&&(a=0),"ਰਾਤ"===b?4>a?a:a+12:"ਸਵੇਰ"===b?a:"ਦੁਪਹਿਰ"===b?a>=10?a:a+12:"ਸ਼ਾਮ"===b?a+12:void 0;},meridiem:function meridiem(a,b,c){return 4>a?"ਰਾਤ":10>a?"ਸਵੇਰ":17>a?"ਦੁਪਹਿਰ":20>a?"ਸ਼ਾਮ":"ਰਾਤ";},week:{dow:0,doy:6}}),"styczeń_luty_marzec_kwiecień_maj_czerwiec_lipiec_sierpień_wrzesień_październik_listopad_grudzień".split("_")),Zg="stycznia_lutego_marca_kwietnia_maja_czerwca_lipca_sierpnia_września_października_listopada_grudnia".split("_"),$g=(bg.defineLocale("pl",{months:function months(a,b){return ""===b?"("+Zg[a.month()]+"|"+Yg[a.month()]+")":/D MMMM/.test(b)?Zg[a.month()]:Yg[a.month()];},monthsShort:"sty_lut_mar_kwi_maj_cze_lip_sie_wrz_paź_lis_gru".split("_"),weekdays:"niedziela_poniedziałek_wtorek_środa_czwartek_piątek_sobota".split("_"),weekdaysShort:"nie_pon_wt_śr_czw_pt_sb".split("_"),weekdaysMin:"Nd_Pn_Wt_Śr_Cz_Pt_So".split("_"),longDateFormat:{LT:"HH:mm",LTS:"HH:mm:ss",L:"DD.MM.YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY HH:mm",LLLL:"dddd, D MMMM YYYY HH:mm"},calendar:{sameDay:"[Dziś o] LT",nextDay:"[Jutro o] LT",nextWeek:"[W] dddd [o] LT",lastDay:"[Wczoraj o] LT",lastWeek:function lastWeek(){switch(this.day()){case 0:return "[W zeszłą niedzielę o] LT";case 3:return "[W zeszłą środę o] LT";case 6:return "[W zeszłą sobotę o] LT";default:return "[W zeszły] dddd [o] LT";}},sameElse:"L"},relativeTime:{future:"za %s",past:"%s temu",s:"kilka sekund",m:Od,mm:Od,h:Od,hh:Od,d:"1 dzień",dd:"%d dni",M:"miesiąc",MM:Od,y:"rok",yy:Od},ordinalParse:/\d{1,2}\./,ordinal:"%d.",week:{dow:1,doy:4}}),bg.defineLocale("pt-br",{months:"Janeiro_Fevereiro_Março_Abril_Maio_Junho_Julho_Agosto_Setembro_Outubro_Novembro_Dezembro".split("_"),monthsShort:"Jan_Fev_Mar_Abr_Mai_Jun_Jul_Ago_Set_Out_Nov_Dez".split("_"),weekdays:"Domingo_Segunda-feira_Terça-feira_Quarta-feira_Quinta-feira_Sexta-feira_Sábado".split("_"),weekdaysShort:"Dom_Seg_Ter_Qua_Qui_Sex_Sáb".split("_"),weekdaysMin:"Dom_2ª_3ª_4ª_5ª_6ª_Sáb".split("_"),weekdaysParseExact:!0,longDateFormat:{LT:"HH:mm",LTS:"HH:mm:ss",L:"DD/MM/YYYY",LL:"D [de] MMMM [de] YYYY",LLL:"D [de] MMMM [de] YYYY [às] HH:mm",LLLL:"dddd, D [de] MMMM [de] YYYY [às] HH:mm"},calendar:{sameDay:"[Hoje às] LT",nextDay:"[Amanhã às] LT",nextWeek:"dddd [às] LT",lastDay:"[Ontem às] LT",lastWeek:function lastWeek(){return 0===this.day()||6===this.day()?"[Último] dddd [às] LT":"[Última] dddd [às] LT";},sameElse:"L"},relativeTime:{future:"em %s",past:"%s atrás",s:"poucos segundos",m:"um minuto",mm:"%d minutos",h:"uma hora",hh:"%d horas",d:"um dia",dd:"%d dias",M:"um mês",MM:"%d meses",y:"um ano",yy:"%d anos"},ordinalParse:/\d{1,2}º/,ordinal:"%dº"}),bg.defineLocale("pt",{months:"Janeiro_Fevereiro_Março_Abril_Maio_Junho_Julho_Agosto_Setembro_Outubro_Novembro_Dezembro".split("_"),monthsShort:"Jan_Fev_Mar_Abr_Mai_Jun_Jul_Ago_Set_Out_Nov_Dez".split("_"),weekdays:"Domingo_Segunda-Feira_Terça-Feira_Quarta-Feira_Quinta-Feira_Sexta-Feira_Sábado".split("_"),weekdaysShort:"Dom_Seg_Ter_Qua_Qui_Sex_Sáb".split("_"),weekdaysMin:"Dom_2ª_3ª_4ª_5ª_6ª_Sáb".split("_"),weekdaysParseExact:!0,longDateFormat:{LT:"HH:mm",LTS:"HH:mm:ss",L:"DD/MM/YYYY",LL:"D [de] MMMM [de] YYYY",LLL:"D [de] MMMM [de] YYYY HH:mm",LLLL:"dddd, D [de] MMMM [de] YYYY HH:mm"},calendar:{sameDay:"[Hoje às] LT",nextDay:"[Amanhã às] LT",nextWeek:"dddd [às] LT",lastDay:"[Ontem às] LT",lastWeek:function lastWeek(){return 0===this.day()||6===this.day()?"[Último] dddd [às] LT":"[Última] dddd [às] LT";},sameElse:"L"},relativeTime:{future:"em %s",past:"há %s",s:"segundos",m:"um minuto",mm:"%d minutos",h:"uma hora",hh:"%d horas",d:"um dia",dd:"%d dias",M:"um mês",MM:"%d meses",y:"um ano",yy:"%d anos"},ordinalParse:/\d{1,2}º/,ordinal:"%dº",week:{dow:1,doy:4}}),bg.defineLocale("ro",{months:"ianuarie_februarie_martie_aprilie_mai_iunie_iulie_august_septembrie_octombrie_noiembrie_decembrie".split("_"),monthsShort:"ian._febr._mart._apr._mai_iun._iul._aug._sept._oct._nov._dec.".split("_"),monthsParseExact:!0,weekdays:"duminică_luni_marți_miercuri_joi_vineri_sâmbătă".split("_"),weekdaysShort:"Dum_Lun_Mar_Mie_Joi_Vin_Sâm".split("_"),weekdaysMin:"Du_Lu_Ma_Mi_Jo_Vi_Sâ".split("_"),longDateFormat:{LT:"H:mm",LTS:"H:mm:ss",L:"DD.MM.YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY H:mm",LLLL:"dddd, D MMMM YYYY H:mm"},calendar:{sameDay:"[azi la] LT",nextDay:"[mâine la] LT",nextWeek:"dddd [la] LT",lastDay:"[ieri la] LT",lastWeek:"[fosta] dddd [la] LT",sameElse:"L"},relativeTime:{future:"peste %s",past:"%s în urmă",s:"câteva secunde",m:"un minut",mm:Pd,h:"o oră",hh:Pd,d:"o zi",dd:Pd,M:"o lună",MM:Pd,y:"un an",yy:Pd},week:{dow:1,doy:7}}),[/^янв/i,/^фев/i,/^мар/i,/^апр/i,/^ма[йя]/i,/^июн/i,/^июл/i,/^авг/i,/^сен/i,/^окт/i,/^ноя/i,/^дек/i]),_g=(bg.defineLocale("ru",{months:{format:"января_февраля_марта_апреля_мая_июня_июля_августа_сентября_октября_ноября_декабря".split("_"),standalone:"январь_февраль_март_апрель_май_июнь_июль_август_сентябрь_октябрь_ноябрь_декабрь".split("_")},monthsShort:{format:"янв._февр._мар._апр._мая_июня_июля_авг._сент._окт._нояб._дек.".split("_"),standalone:"янв._февр._март_апр._май_июнь_июль_авг._сент._окт._нояб._дек.".split("_")},weekdays:{standalone:"воскресенье_понедельник_вторник_среда_четверг_пятница_суббота".split("_"),format:"воскресенье_понедельник_вторник_среду_четверг_пятницу_субботу".split("_"),isFormat:/\[ ?[Вв] ?(?:прошлую|следующую|эту)? ?\] ?dddd/},weekdaysShort:"вс_пн_вт_ср_чт_пт_сб".split("_"),weekdaysMin:"вс_пн_вт_ср_чт_пт_сб".split("_"),monthsParse:$g,longMonthsParse:$g,shortMonthsParse:$g,monthsRegex:/^(сентябр[яь]|октябр[яь]|декабр[яь]|феврал[яь]|январ[яь]|апрел[яь]|августа?|ноябр[яь]|сент\.|февр\.|нояб\.|июнь|янв.|июль|дек.|авг.|апр.|марта|мар[.т]|окт.|июн[яь]|июл[яь]|ма[яй])/i,monthsShortRegex:/^(сентябр[яь]|октябр[яь]|декабр[яь]|феврал[яь]|январ[яь]|апрел[яь]|августа?|ноябр[яь]|сент\.|февр\.|нояб\.|июнь|янв.|июль|дек.|авг.|апр.|марта|мар[.т]|окт.|июн[яь]|июл[яь]|ма[яй])/i,monthsStrictRegex:/^(сентябр[яь]|октябр[яь]|декабр[яь]|феврал[яь]|январ[яь]|апрел[яь]|августа?|ноябр[яь]|марта?|июн[яь]|июл[яь]|ма[яй])/i,monthsShortStrictRegex:/^(нояб\.|февр\.|сент\.|июль|янв\.|июн[яь]|мар[.т]|авг\.|апр\.|окт\.|дек\.|ма[яй])/i,longDateFormat:{LT:"HH:mm",LTS:"HH:mm:ss",L:"DD.MM.YYYY",LL:"D MMMM YYYY г.",LLL:"D MMMM YYYY г., HH:mm",LLLL:"dddd, D MMMM YYYY г., HH:mm"},calendar:{sameDay:"[Сегодня в] LT",nextDay:"[Завтра в] LT",lastDay:"[Вчера в] LT",nextWeek:function nextWeek(a){if(a.week()===this.week())return 2===this.day()?"[Во] dddd [в] LT":"[В] dddd [в] LT";switch(this.day()){case 0:return "[В следующее] dddd [в] LT";case 1:case 2:case 4:return "[В следующий] dddd [в] LT";case 3:case 5:case 6:return "[В следующую] dddd [в] LT";}},lastWeek:function lastWeek(a){if(a.week()===this.week())return 2===this.day()?"[Во] dddd [в] LT":"[В] dddd [в] LT";switch(this.day()){case 0:return "[В прошлое] dddd [в] LT";case 1:case 2:case 4:return "[В прошлый] dddd [в] LT";case 3:case 5:case 6:return "[В прошлую] dddd [в] LT";}},sameElse:"L"},relativeTime:{future:"через %s",past:"%s назад",s:"несколько секунд",m:Rd,mm:Rd,h:"час",hh:Rd,d:"день",dd:Rd,M:"месяц",MM:Rd,y:"год",yy:Rd},meridiemParse:/ночи|утра|дня|вечера/i,isPM:function isPM(a){return (/^(дня|вечера)$/.test(a));},meridiem:function meridiem(a,b,c){return 4>a?"ночи":12>a?"утра":17>a?"дня":"вечера";},ordinalParse:/\d{1,2}-(й|го|я)/,ordinal:function ordinal(a,b){switch(b){case "M":case "d":case "DDD":return a+"-й";case "D":return a+"-го";case "w":case "W":return a+"-я";default:return a;}},week:{dow:1,doy:7}}),bg.defineLocale("se",{months:"ođđajagemánnu_guovvamánnu_njukčamánnu_cuoŋománnu_miessemánnu_geassemánnu_suoidnemánnu_borgemánnu_čakčamánnu_golggotmánnu_skábmamánnu_juovlamánnu".split("_"),monthsShort:"ođđj_guov_njuk_cuo_mies_geas_suoi_borg_čakč_golg_skáb_juov".split("_"),weekdays:"sotnabeaivi_vuossárga_maŋŋebárga_gaskavahkku_duorastat_bearjadat_lávvardat".split("_"),weekdaysShort:"sotn_vuos_maŋ_gask_duor_bear_láv".split("_"),weekdaysMin:"s_v_m_g_d_b_L".split("_"),longDateFormat:{LT:"HH:mm",LTS:"HH:mm:ss",L:"DD.MM.YYYY",LL:"MMMM D. [b.] YYYY",LLL:"MMMM D. [b.] YYYY [ti.] HH:mm",LLLL:"dddd, MMMM D. [b.] YYYY [ti.] HH:mm"},calendar:{sameDay:"[otne ti] LT",nextDay:"[ihttin ti] LT",nextWeek:"dddd [ti] LT",lastDay:"[ikte ti] LT",lastWeek:"[ovddit] dddd [ti] LT",sameElse:"L"},relativeTime:{future:"%s geažes",past:"maŋit %s",s:"moadde sekunddat",m:"okta minuhta",mm:"%d minuhtat",h:"okta diimmu",hh:"%d diimmut",d:"okta beaivi",dd:"%d beaivvit",M:"okta mánnu",MM:"%d mánut",y:"okta jahki",yy:"%d jagit"},ordinalParse:/\d{1,2}\./,ordinal:"%d.",week:{dow:1,doy:4}}),bg.defineLocale("si",{months:"ජනවාරි_පෙබරවාරි_මාර්තු_අප්‍රේල්_මැයි_ජූනි_ජූලි_අගෝස්තු_සැප්තැම්බර්_ඔක්තෝබර්_නොවැම්බර්_දෙසැම්බර්".split("_"),monthsShort:"ජන_පෙබ_මාර්_අප්_මැයි_ජූනි_ජූලි_අගෝ_සැප්_ඔක්_නොවැ_දෙසැ".split("_"),weekdays:"ඉරිදා_සඳුදා_අඟහරුවාදා_බදාදා_බ්‍රහස්පතින්දා_සිකුරාදා_සෙනසුරාදා".split("_"),weekdaysShort:"ඉරි_සඳු_අඟ_බදා_බ්‍රහ_සිකු_සෙන".split("_"),weekdaysMin:"ඉ_ස_අ_බ_බ්‍ර_සි_සෙ".split("_"),weekdaysParseExact:!0,longDateFormat:{LT:"a h:mm",LTS:"a h:mm:ss",L:"YYYY/MM/DD",LL:"YYYY MMMM D",LLL:"YYYY MMMM D, a h:mm",LLLL:"YYYY MMMM D [වැනි] dddd, a h:mm:ss"},calendar:{sameDay:"[අද] LT[ට]",nextDay:"[හෙට] LT[ට]",nextWeek:"dddd LT[ට]",lastDay:"[ඊයේ] LT[ට]",lastWeek:"[පසුගිය] dddd LT[ට]",sameElse:"L"},relativeTime:{future:"%sකින්",past:"%sකට පෙර",s:"තත්පර කිහිපය",m:"මිනිත්තුව",mm:"මිනිත්තු %d",h:"පැය",hh:"පැය %d",d:"දිනය",dd:"දින %d",M:"මාසය",MM:"මාස %d",y:"වසර",yy:"වසර %d"},ordinalParse:/\d{1,2} වැනි/,ordinal:function ordinal(a){return a+" වැනි";},meridiemParse:/පෙර වරු|පස් වරු|පෙ.ව|ප.ව./,isPM:function isPM(a){return "ප.ව."===a||"පස් වරු"===a;},meridiem:function meridiem(a,b,c){return a>11?c?"ප.ව.":"පස් වරු":c?"පෙ.ව.":"පෙර වරු";}}),"január_február_marec_apríl_máj_jún_júl_august_september_október_november_december".split("_")),ah="jan_feb_mar_apr_máj_jún_júl_aug_sep_okt_nov_dec".split("_"),bh=(bg.defineLocale("sk",{months:_g,monthsShort:ah,weekdays:"nedeľa_pondelok_utorok_streda_štvrtok_piatok_sobota".split("_"),weekdaysShort:"ne_po_ut_st_št_pi_so".split("_"),weekdaysMin:"ne_po_ut_st_št_pi_so".split("_"),longDateFormat:{LT:"H:mm",LTS:"H:mm:ss",L:"DD.MM.YYYY",LL:"D. MMMM YYYY",LLL:"D. MMMM YYYY H:mm",LLLL:"dddd D. MMMM YYYY H:mm"},calendar:{sameDay:"[dnes o] LT",nextDay:"[zajtra o] LT",nextWeek:function nextWeek(){switch(this.day()){case 0:return "[v nedeľu o] LT";case 1:case 2:return "[v] dddd [o] LT";case 3:return "[v stredu o] LT";case 4:return "[vo štvrtok o] LT";case 5:return "[v piatok o] LT";case 6:return "[v sobotu o] LT";}},lastDay:"[včera o] LT",lastWeek:function lastWeek(){switch(this.day()){case 0:return "[minulú nedeľu o] LT";case 1:case 2:return "[minulý] dddd [o] LT";case 3:return "[minulú stredu o] LT";case 4:case 5:return "[minulý] dddd [o] LT";case 6:return "[minulú sobotu o] LT";}},sameElse:"L"},relativeTime:{future:"za %s",past:"pred %s",s:Td,m:Td,mm:Td,h:Td,hh:Td,d:Td,dd:Td,M:Td,MM:Td,y:Td,yy:Td},ordinalParse:/\d{1,2}\./,ordinal:"%d.",week:{dow:1,doy:4}}),bg.defineLocale("sl",{months:"januar_februar_marec_april_maj_junij_julij_avgust_september_oktober_november_december".split("_"),monthsShort:"jan._feb._mar._apr._maj._jun._jul._avg._sep._okt._nov._dec.".split("_"),monthsParseExact:!0,weekdays:"nedelja_ponedeljek_torek_sreda_četrtek_petek_sobota".split("_"),weekdaysShort:"ned._pon._tor._sre._čet._pet._sob.".split("_"),weekdaysMin:"ne_po_to_sr_če_pe_so".split("_"),weekdaysParseExact:!0,longDateFormat:{LT:"H:mm",LTS:"H:mm:ss",L:"DD. MM. YYYY",LL:"D. MMMM YYYY",LLL:"D. MMMM YYYY H:mm",LLLL:"dddd, D. MMMM YYYY H:mm"},calendar:{sameDay:"[danes ob] LT",nextDay:"[jutri ob] LT",nextWeek:function nextWeek(){switch(this.day()){case 0:return "[v] [nedeljo] [ob] LT";case 3:return "[v] [sredo] [ob] LT";case 6:return "[v] [soboto] [ob] LT";case 1:case 2:case 4:case 5:return "[v] dddd [ob] LT";}},lastDay:"[včeraj ob] LT",lastWeek:function lastWeek(){switch(this.day()){case 0:return "[prejšnjo] [nedeljo] [ob] LT";case 3:return "[prejšnjo] [sredo] [ob] LT";case 6:return "[prejšnjo] [soboto] [ob] LT";case 1:case 2:case 4:case 5:return "[prejšnji] dddd [ob] LT";}},sameElse:"L"},relativeTime:{future:"čez %s",past:"pred %s",s:Ud,m:Ud,mm:Ud,h:Ud,hh:Ud,d:Ud,dd:Ud,M:Ud,MM:Ud,y:Ud,yy:Ud},ordinalParse:/\d{1,2}\./,ordinal:"%d.",week:{dow:1,doy:7}}),bg.defineLocale("sq",{months:"Janar_Shkurt_Mars_Prill_Maj_Qershor_Korrik_Gusht_Shtator_Tetor_Nëntor_Dhjetor".split("_"),monthsShort:"Jan_Shk_Mar_Pri_Maj_Qer_Kor_Gus_Sht_Tet_Nën_Dhj".split("_"),weekdays:"E Diel_E Hënë_E Martë_E Mërkurë_E Enjte_E Premte_E Shtunë".split("_"),weekdaysShort:"Die_Hën_Mar_Mër_Enj_Pre_Sht".split("_"),weekdaysMin:"D_H_Ma_Më_E_P_Sh".split("_"),weekdaysParseExact:!0,meridiemParse:/PD|MD/,isPM:function isPM(a){return "M"===a.charAt(0);},meridiem:function meridiem(a,b,c){return 12>a?"PD":"MD";},longDateFormat:{LT:"HH:mm",LTS:"HH:mm:ss",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY HH:mm",LLLL:"dddd, D MMMM YYYY HH:mm"},calendar:{sameDay:"[Sot në] LT",nextDay:"[Nesër në] LT",nextWeek:"dddd [në] LT",lastDay:"[Dje në] LT",lastWeek:"dddd [e kaluar në] LT",sameElse:"L"},relativeTime:{future:"në %s",past:"%s më parë",s:"disa sekonda",m:"një minutë",mm:"%d minuta",h:"një orë",hh:"%d orë",d:"një ditë",dd:"%d ditë",M:"një muaj",MM:"%d muaj",y:"një vit",yy:"%d vite"},ordinalParse:/\d{1,2}\./,ordinal:"%d.",week:{dow:1,doy:4}}),{words:{m:["један минут","једне минуте"],mm:["минут","минуте","минута"],h:["један сат","једног сата"],hh:["сат","сата","сати"],dd:["дан","дана","дана"],MM:["месец","месеца","месеци"],yy:["година","године","година"]},correctGrammaticalCase:function correctGrammaticalCase(a,b){return 1===a?b[0]:a>=2&&4>=a?b[1]:b[2];},translate:function translate(a,b,c){var d=bh.words[c];return 1===c.length?b?d[0]:d[1]:a+" "+bh.correctGrammaticalCase(a,d);}}),ch=(bg.defineLocale("sr-cyrl",{months:"јануар_фебруар_март_април_мај_јун_јул_август_септембар_октобар_новембар_децембар".split("_"),monthsShort:"јан._феб._мар._апр._мај_јун_јул_авг._сеп._окт._нов._дец.".split("_"),monthsParseExact:!0,weekdays:"недеља_понедељак_уторак_среда_четвртак_петак_субота".split("_"),weekdaysShort:"нед._пон._уто._сре._чет._пет._суб.".split("_"),weekdaysMin:"не_по_ут_ср_че_пе_су".split("_"),weekdaysParseExact:!0,longDateFormat:{LT:"H:mm",LTS:"H:mm:ss",L:"DD. MM. YYYY",LL:"D. MMMM YYYY",LLL:"D. MMMM YYYY H:mm",LLLL:"dddd, D. MMMM YYYY H:mm"},calendar:{sameDay:"[данас у] LT",nextDay:"[сутра у] LT",nextWeek:function nextWeek(){switch(this.day()){case 0:return "[у] [недељу] [у] LT";case 3:return "[у] [среду] [у] LT";case 6:return "[у] [суботу] [у] LT";case 1:case 2:case 4:case 5:return "[у] dddd [у] LT";}},lastDay:"[јуче у] LT",lastWeek:function lastWeek(){var a=["[прошле] [недеље] [у] LT","[прошлог] [понедељка] [у] LT","[прошлог] [уторка] [у] LT","[прошле] [среде] [у] LT","[прошлог] [четвртка] [у] LT","[прошлог] [петка] [у] LT","[прошле] [суботе] [у] LT"];return a[this.day()];},sameElse:"L"},relativeTime:{future:"за %s",past:"пре %s",s:"неколико секунди",m:bh.translate,mm:bh.translate,h:bh.translate,hh:bh.translate,d:"дан",dd:bh.translate,M:"месец",MM:bh.translate,y:"годину",yy:bh.translate},ordinalParse:/\d{1,2}\./,ordinal:"%d.",week:{dow:1,doy:7}}),{words:{m:["jedan minut","jedne minute"],mm:["minut","minute","minuta"],h:["jedan sat","jednog sata"],hh:["sat","sata","sati"],dd:["dan","dana","dana"],MM:["mesec","meseca","meseci"],yy:["godina","godine","godina"]},correctGrammaticalCase:function correctGrammaticalCase(a,b){return 1===a?b[0]:a>=2&&4>=a?b[1]:b[2];},translate:function translate(a,b,c){var d=ch.words[c];return 1===c.length?b?d[0]:d[1]:a+" "+ch.correctGrammaticalCase(a,d);}}),dh=(bg.defineLocale("sr",{months:"januar_februar_mart_april_maj_jun_jul_avgust_septembar_oktobar_novembar_decembar".split("_"),monthsShort:"jan._feb._mar._apr._maj_jun_jul_avg._sep._okt._nov._dec.".split("_"),monthsParseExact:!0,weekdays:"nedelja_ponedeljak_utorak_sreda_četvrtak_petak_subota".split("_"),weekdaysShort:"ned._pon._uto._sre._čet._pet._sub.".split("_"),weekdaysMin:"ne_po_ut_sr_če_pe_su".split("_"),weekdaysParseExact:!0,longDateFormat:{LT:"H:mm",LTS:"H:mm:ss",L:"DD. MM. YYYY",LL:"D. MMMM YYYY",LLL:"D. MMMM YYYY H:mm",LLLL:"dddd, D. MMMM YYYY H:mm"},calendar:{sameDay:"[danas u] LT",nextDay:"[sutra u] LT",nextWeek:function nextWeek(){switch(this.day()){case 0:return "[u] [nedelju] [u] LT";case 3:return "[u] [sredu] [u] LT";case 6:return "[u] [subotu] [u] LT";case 1:case 2:case 4:case 5:return "[u] dddd [u] LT";}},lastDay:"[juče u] LT",lastWeek:function lastWeek(){var a=["[prošle] [nedelje] [u] LT","[prošlog] [ponedeljka] [u] LT","[prošlog] [utorka] [u] LT","[prošle] [srede] [u] LT","[prošlog] [četvrtka] [u] LT","[prošlog] [petka] [u] LT","[prošle] [subote] [u] LT"];return a[this.day()];},sameElse:"L"},relativeTime:{future:"za %s",past:"pre %s",s:"nekoliko sekundi",m:ch.translate,mm:ch.translate,h:ch.translate,hh:ch.translate,d:"dan",dd:ch.translate,M:"mesec",MM:ch.translate,y:"godinu",yy:ch.translate},ordinalParse:/\d{1,2}\./,ordinal:"%d.",week:{dow:1,doy:7}}),bg.defineLocale("ss",{months:"Bhimbidvwane_Indlovana_Indlov'lenkhulu_Mabasa_Inkhwekhweti_Inhlaba_Kholwane_Ingci_Inyoni_Imphala_Lweti_Ingongoni".split("_"),monthsShort:"Bhi_Ina_Inu_Mab_Ink_Inh_Kho_Igc_Iny_Imp_Lwe_Igo".split("_"),weekdays:"Lisontfo_Umsombuluko_Lesibili_Lesitsatfu_Lesine_Lesihlanu_Umgcibelo".split("_"),weekdaysShort:"Lis_Umb_Lsb_Les_Lsi_Lsh_Umg".split("_"),weekdaysMin:"Li_Us_Lb_Lt_Ls_Lh_Ug".split("_"),weekdaysParseExact:!0,longDateFormat:{LT:"h:mm A",LTS:"h:mm:ss A",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY h:mm A",LLLL:"dddd, D MMMM YYYY h:mm A"},calendar:{sameDay:"[Namuhla nga] LT",nextDay:"[Kusasa nga] LT",nextWeek:"dddd [nga] LT",lastDay:"[Itolo nga] LT",lastWeek:"dddd [leliphelile] [nga] LT",sameElse:"L"},relativeTime:{future:"nga %s",past:"wenteka nga %s",s:"emizuzwana lomcane",m:"umzuzu",mm:"%d emizuzu",h:"lihora",hh:"%d emahora",d:"lilanga",dd:"%d emalanga",M:"inyanga",MM:"%d tinyanga",y:"umnyaka",yy:"%d iminyaka"},meridiemParse:/ekuseni|emini|entsambama|ebusuku/,meridiem:function meridiem(a,b,c){return 11>a?"ekuseni":15>a?"emini":19>a?"entsambama":"ebusuku";},meridiemHour:function meridiemHour(a,b){return 12===a&&(a=0),"ekuseni"===b?a:"emini"===b?a>=11?a:a+12:"entsambama"===b||"ebusuku"===b?0===a?0:a+12:void 0;},ordinalParse:/\d{1,2}/,ordinal:"%d",week:{dow:1,doy:4}}),bg.defineLocale("sv",{months:"januari_februari_mars_april_maj_juni_juli_augusti_september_oktober_november_december".split("_"),monthsShort:"jan_feb_mar_apr_maj_jun_jul_aug_sep_okt_nov_dec".split("_"),weekdays:"söndag_måndag_tisdag_onsdag_torsdag_fredag_lördag".split("_"),weekdaysShort:"sön_mån_tis_ons_tor_fre_lör".split("_"),weekdaysMin:"sö_må_ti_on_to_fr_lö".split("_"),longDateFormat:{LT:"HH:mm",LTS:"HH:mm:ss",L:"YYYY-MM-DD",LL:"D MMMM YYYY",LLL:"D MMMM YYYY [kl.] HH:mm",LLLL:"dddd D MMMM YYYY [kl.] HH:mm",lll:"D MMM YYYY HH:mm",llll:"ddd D MMM YYYY HH:mm"},calendar:{sameDay:"[Idag] LT",nextDay:"[Imorgon] LT",lastDay:"[Igår] LT",nextWeek:"[På] dddd LT",lastWeek:"[I] dddd[s] LT",sameElse:"L"},relativeTime:{future:"om %s",past:"för %s sedan",s:"några sekunder",m:"en minut",mm:"%d minuter",h:"en timme",hh:"%d timmar",d:"en dag",dd:"%d dagar",M:"en månad",MM:"%d månader",y:"ett år",yy:"%d år"},ordinalParse:/\d{1,2}(e|a)/,ordinal:function ordinal(a){var b=a%10,c=1===~ ~(a%100/10)?"e":1===b?"a":2===b?"a":"e";return a+c;},week:{dow:1,doy:4}}),bg.defineLocale("sw",{months:"Januari_Februari_Machi_Aprili_Mei_Juni_Julai_Agosti_Septemba_Oktoba_Novemba_Desemba".split("_"),monthsShort:"Jan_Feb_Mac_Apr_Mei_Jun_Jul_Ago_Sep_Okt_Nov_Des".split("_"),weekdays:"Jumapili_Jumatatu_Jumanne_Jumatano_Alhamisi_Ijumaa_Jumamosi".split("_"),weekdaysShort:"Jpl_Jtat_Jnne_Jtan_Alh_Ijm_Jmos".split("_"),weekdaysMin:"J2_J3_J4_J5_Al_Ij_J1".split("_"),weekdaysParseExact:!0,longDateFormat:{LT:"HH:mm",LTS:"HH:mm:ss",L:"DD.MM.YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY HH:mm",LLLL:"dddd, D MMMM YYYY HH:mm"},calendar:{sameDay:"[leo saa] LT",nextDay:"[kesho saa] LT",nextWeek:"[wiki ijayo] dddd [saat] LT",lastDay:"[jana] LT",lastWeek:"[wiki iliyopita] dddd [saat] LT",sameElse:"L"},relativeTime:{future:"%s baadaye",past:"tokea %s",s:"hivi punde",m:"dakika moja",mm:"dakika %d",h:"saa limoja",hh:"masaa %d",d:"siku moja",dd:"masiku %d",M:"mwezi mmoja",MM:"miezi %d",y:"mwaka mmoja",yy:"miaka %d"},week:{dow:1,doy:7}}),{1:"௧",2:"௨",3:"௩",4:"௪",5:"௫",6:"௬",7:"௭",8:"௮",9:"௯",0:"௦"}),eh={"௧":"1","௨":"2","௩":"3","௪":"4","௫":"5","௬":"6","௭":"7","௮":"8","௯":"9","௦":"0"},fh=(bg.defineLocale("ta",{months:"ஜனவரி_பிப்ரவரி_மார்ச்_ஏப்ரல்_மே_ஜூன்_ஜூலை_ஆகஸ்ட்_செப்டெம்பர்_அக்டோபர்_நவம்பர்_டிசம்பர்".split("_"),monthsShort:"ஜனவரி_பிப்ரவரி_மார்ச்_ஏப்ரல்_மே_ஜூன்_ஜூலை_ஆகஸ்ட்_செப்டெம்பர்_அக்டோபர்_நவம்பர்_டிசம்பர்".split("_"),weekdays:"ஞாயிற்றுக்கிழமை_திங்கட்கிழமை_செவ்வாய்கிழமை_புதன்கிழமை_வியாழக்கிழமை_வெள்ளிக்கிழமை_சனிக்கிழமை".split("_"),weekdaysShort:"ஞாயிறு_திங்கள்_செவ்வாய்_புதன்_வியாழன்_வெள்ளி_சனி".split("_"),weekdaysMin:"ஞா_தி_செ_பு_வி_வெ_ச".split("_"),longDateFormat:{LT:"HH:mm",LTS:"HH:mm:ss",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY, HH:mm",LLLL:"dddd, D MMMM YYYY, HH:mm"},calendar:{sameDay:"[இன்று] LT",nextDay:"[நாளை] LT",nextWeek:"dddd, LT",lastDay:"[நேற்று] LT",lastWeek:"[கடந்த வாரம்] dddd, LT",sameElse:"L"},relativeTime:{future:"%s இல்",past:"%s முன்",s:"ஒரு சில விநாடிகள்",m:"ஒரு நிமிடம்",mm:"%d நிமிடங்கள்",h:"ஒரு மணி நேரம்",hh:"%d மணி நேரம்",d:"ஒரு நாள்",dd:"%d நாட்கள்",M:"ஒரு மாதம்",MM:"%d மாதங்கள்",y:"ஒரு வருடம்",yy:"%d ஆண்டுகள்"},ordinalParse:/\d{1,2}வது/,ordinal:function ordinal(a){return a+"வது";},preparse:function preparse(a){return a.replace(/[௧௨௩௪௫௬௭௮௯௦]/g,function(a){return eh[a];});},postformat:function postformat(a){return a.replace(/\d/g,function(a){return dh[a];});},meridiemParse:/யாமம்|வைகறை|காலை|நண்பகல்|எற்பாடு|மாலை/,meridiem:function meridiem(a,b,c){return 2>a?" யாமம்":6>a?" வைகறை":10>a?" காலை":14>a?" நண்பகல்":18>a?" எற்பாடு":22>a?" மாலை":" யாமம்";},meridiemHour:function meridiemHour(a,b){return 12===a&&(a=0),"யாமம்"===b?2>a?a:a+12:"வைகறை"===b||"காலை"===b?a:"நண்பகல்"===b&&a>=10?a:a+12;},week:{dow:0,doy:6}}),bg.defineLocale("te",{months:"జనవరి_ఫిబ్రవరి_మార్చి_ఏప్రిల్_మే_జూన్_జూలై_ఆగస్టు_సెప్టెంబర్_అక్టోబర్_నవంబర్_డిసెంబర్".split("_"),monthsShort:"జన._ఫిబ్ర._మార్చి_ఏప్రి._మే_జూన్_జూలై_ఆగ._సెప్._అక్టో._నవ._డిసె.".split("_"),monthsParseExact:!0,weekdays:"ఆదివారం_సోమవారం_మంగళవారం_బుధవారం_గురువారం_శుక్రవారం_శనివారం".split("_"),weekdaysShort:"ఆది_సోమ_మంగళ_బుధ_గురు_శుక్ర_శని".split("_"),weekdaysMin:"ఆ_సో_మం_బు_గు_శు_శ".split("_"),longDateFormat:{LT:"A h:mm",LTS:"A h:mm:ss",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY, A h:mm",LLLL:"dddd, D MMMM YYYY, A h:mm"},calendar:{sameDay:"[నేడు] LT",nextDay:"[రేపు] LT",nextWeek:"dddd, LT",lastDay:"[నిన్న] LT",lastWeek:"[గత] dddd, LT",sameElse:"L"},relativeTime:{future:"%s లో",past:"%s క్రితం",s:"కొన్ని క్షణాలు",m:"ఒక నిమిషం",mm:"%d నిమిషాలు",h:"ఒక గంట",hh:"%d గంటలు",d:"ఒక రోజు",dd:"%d రోజులు",M:"ఒక నెల",MM:"%d నెలలు",y:"ఒక సంవత్సరం",yy:"%d సంవత్సరాలు"},ordinalParse:/\d{1,2}వ/,ordinal:"%dవ",meridiemParse:/రాత్రి|ఉదయం|మధ్యాహ్నం|సాయంత్రం/,meridiemHour:function meridiemHour(a,b){return 12===a&&(a=0),"రాత్రి"===b?4>a?a:a+12:"ఉదయం"===b?a:"మధ్యాహ్నం"===b?a>=10?a:a+12:"సాయంత్రం"===b?a+12:void 0;},meridiem:function meridiem(a,b,c){return 4>a?"రాత్రి":10>a?"ఉదయం":17>a?"మధ్యాహ్నం":20>a?"సాయంత్రం":"రాత్రి";},week:{dow:0,doy:6}}),bg.defineLocale("th",{months:"มกราคม_กุมภาพันธ์_มีนาคม_เมษายน_พฤษภาคม_มิถุนายน_กรกฎาคม_สิงหาคม_กันยายน_ตุลาคม_พฤศจิกายน_ธันวาคม".split("_"),monthsShort:"มกรา_กุมภา_มีนา_เมษา_พฤษภา_มิถุนา_กรกฎา_สิงหา_กันยา_ตุลา_พฤศจิกา_ธันวา".split("_"),monthsParseExact:!0,weekdays:"อาทิตย์_จันทร์_อังคาร_พุธ_พฤหัสบดี_ศุกร์_เสาร์".split("_"),weekdaysShort:"อาทิตย์_จันทร์_อังคาร_พุธ_พฤหัส_ศุกร์_เสาร์".split("_"),weekdaysMin:"อา._จ._อ._พ._พฤ._ศ._ส.".split("_"),weekdaysParseExact:!0,longDateFormat:{LT:"H นาฬิกา m นาที",LTS:"H นาฬิกา m นาที s วินาที",L:"YYYY/MM/DD",LL:"D MMMM YYYY",LLL:"D MMMM YYYY เวลา H นาฬิกา m นาที",LLLL:"วันddddที่ D MMMM YYYY เวลา H นาฬิกา m นาที"},meridiemParse:/ก่อนเที่ยง|หลังเที่ยง/,isPM:function isPM(a){return "หลังเที่ยง"===a;},meridiem:function meridiem(a,b,c){return 12>a?"ก่อนเที่ยง":"หลังเที่ยง";},calendar:{sameDay:"[วันนี้ เวลา] LT",nextDay:"[พรุ่งนี้ เวลา] LT",nextWeek:"dddd[หน้า เวลา] LT",lastDay:"[เมื่อวานนี้ เวลา] LT",lastWeek:"[วัน]dddd[ที่แล้ว เวลา] LT",sameElse:"L"},relativeTime:{future:"อีก %s",past:"%sที่แล้ว",s:"ไม่กี่วินาที",m:"1 นาที",mm:"%d นาที",h:"1 ชั่วโมง",hh:"%d ชั่วโมง",d:"1 วัน",dd:"%d วัน",M:"1 เดือน",MM:"%d เดือน",y:"1 ปี",yy:"%d ปี"}}),bg.defineLocale("tl-ph",{months:"Enero_Pebrero_Marso_Abril_Mayo_Hunyo_Hulyo_Agosto_Setyembre_Oktubre_Nobyembre_Disyembre".split("_"),monthsShort:"Ene_Peb_Mar_Abr_May_Hun_Hul_Ago_Set_Okt_Nob_Dis".split("_"),weekdays:"Linggo_Lunes_Martes_Miyerkules_Huwebes_Biyernes_Sabado".split("_"),weekdaysShort:"Lin_Lun_Mar_Miy_Huw_Biy_Sab".split("_"),weekdaysMin:"Li_Lu_Ma_Mi_Hu_Bi_Sab".split("_"),longDateFormat:{LT:"HH:mm",LTS:"HH:mm:ss",L:"MM/D/YYYY",LL:"MMMM D, YYYY",LLL:"MMMM D, YYYY HH:mm",LLLL:"dddd, MMMM DD, YYYY HH:mm"},calendar:{sameDay:"[Ngayon sa] LT",nextDay:"[Bukas sa] LT",nextWeek:"dddd [sa] LT",lastDay:"[Kahapon sa] LT",lastWeek:"dddd [huling linggo] LT",sameElse:"L"},relativeTime:{future:"sa loob ng %s",past:"%s ang nakalipas",s:"ilang segundo",m:"isang minuto",mm:"%d minuto",h:"isang oras",hh:"%d oras",d:"isang araw",dd:"%d araw",M:"isang buwan",MM:"%d buwan",y:"isang taon",yy:"%d taon"},ordinalParse:/\d{1,2}/,ordinal:function ordinal(a){return a;},week:{dow:1,doy:4}}),"pagh_wa’_cha’_wej_loS_vagh_jav_Soch_chorgh_Hut".split("_")),gh=(bg.defineLocale("tlh",{months:"tera’ jar wa’_tera’ jar cha’_tera’ jar wej_tera’ jar loS_tera’ jar vagh_tera’ jar jav_tera’ jar Soch_tera’ jar chorgh_tera’ jar Hut_tera’ jar wa’maH_tera’ jar wa’maH wa’_tera’ jar wa’maH cha’".split("_"),monthsShort:"jar wa’_jar cha’_jar wej_jar loS_jar vagh_jar jav_jar Soch_jar chorgh_jar Hut_jar wa’maH_jar wa’maH wa’_jar wa’maH cha’".split("_"),monthsParseExact:!0,weekdays:"lojmItjaj_DaSjaj_povjaj_ghItlhjaj_loghjaj_buqjaj_ghInjaj".split("_"),weekdaysShort:"lojmItjaj_DaSjaj_povjaj_ghItlhjaj_loghjaj_buqjaj_ghInjaj".split("_"),weekdaysMin:"lojmItjaj_DaSjaj_povjaj_ghItlhjaj_loghjaj_buqjaj_ghInjaj".split("_"),longDateFormat:{LT:"HH:mm",LTS:"HH:mm:ss",L:"DD.MM.YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY HH:mm",LLLL:"dddd, D MMMM YYYY HH:mm"},calendar:{sameDay:"[DaHjaj] LT",nextDay:"[wa’leS] LT",nextWeek:"LLL",lastDay:"[wa’Hu’] LT",lastWeek:"LLL",sameElse:"L"},relativeTime:{future:Vd,past:Wd,s:"puS lup",m:"wa’ tup",mm:Xd,h:"wa’ rep",hh:Xd,d:"wa’ jaj",dd:Xd,M:"wa’ jar",MM:Xd,y:"wa’ DIS",yy:Xd},ordinalParse:/\d{1,2}\./,ordinal:"%d.",week:{dow:1,doy:4}}),{1:"'inci",5:"'inci",8:"'inci",70:"'inci",80:"'inci",2:"'nci",7:"'nci",20:"'nci",50:"'nci",3:"'üncü",4:"'üncü",100:"'üncü",6:"'ncı",9:"'uncu",10:"'uncu",30:"'uncu",60:"'ıncı",90:"'ıncı"}),hh=(bg.defineLocale("tr",{months:"Ocak_Şubat_Mart_Nisan_Mayıs_Haziran_Temmuz_Ağustos_Eylül_Ekim_Kasım_Aralık".split("_"),monthsShort:"Oca_Şub_Mar_Nis_May_Haz_Tem_Ağu_Eyl_Eki_Kas_Ara".split("_"),weekdays:"Pazar_Pazartesi_Salı_Çarşamba_Perşembe_Cuma_Cumartesi".split("_"),weekdaysShort:"Paz_Pts_Sal_Çar_Per_Cum_Cts".split("_"),weekdaysMin:"Pz_Pt_Sa_Ça_Pe_Cu_Ct".split("_"),longDateFormat:{LT:"HH:mm",LTS:"HH:mm:ss",L:"DD.MM.YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY HH:mm",LLLL:"dddd, D MMMM YYYY HH:mm"},calendar:{sameDay:"[bugün saat] LT",nextDay:"[yarın saat] LT",nextWeek:"[haftaya] dddd [saat] LT",lastDay:"[dün] LT",lastWeek:"[geçen hafta] dddd [saat] LT",sameElse:"L"},relativeTime:{future:"%s sonra",past:"%s önce",s:"birkaç saniye",m:"bir dakika",mm:"%d dakika",h:"bir saat",hh:"%d saat",d:"bir gün",dd:"%d gün",M:"bir ay",MM:"%d ay",y:"bir yıl",yy:"%d yıl"},ordinalParse:/\d{1,2}'(inci|nci|üncü|ncı|uncu|ıncı)/,ordinal:function ordinal(a){if(0===a)return a+"'ıncı";var b=a%10,c=a%100-b,d=a>=100?100:null;return a+(gh[b]||gh[c]||gh[d]);},week:{dow:1,doy:7}}),bg.defineLocale("tzl",{months:"Januar_Fevraglh_Març_Avrïu_Mai_Gün_Julia_Guscht_Setemvar_Listopäts_Noemvar_Zecemvar".split("_"),monthsShort:"Jan_Fev_Mar_Avr_Mai_Gün_Jul_Gus_Set_Lis_Noe_Zec".split("_"),weekdays:"Súladi_Lúneçi_Maitzi_Márcuri_Xhúadi_Viénerçi_Sáturi".split("_"),weekdaysShort:"Súl_Lún_Mai_Már_Xhú_Vié_Sát".split("_"),weekdaysMin:"Sú_Lú_Ma_Má_Xh_Vi_Sá".split("_"),longDateFormat:{LT:"HH.mm",LTS:"HH.mm.ss",L:"DD.MM.YYYY",LL:"D. MMMM [dallas] YYYY",LLL:"D. MMMM [dallas] YYYY HH.mm",LLLL:"dddd, [li] D. MMMM [dallas] YYYY HH.mm"},meridiemParse:/d\'o|d\'a/i,isPM:function isPM(a){return "d'o"===a.toLowerCase();},meridiem:function meridiem(a,b,c){return a>11?c?"d'o":"D'O":c?"d'a":"D'A";},calendar:{sameDay:"[oxhi à] LT",nextDay:"[demà à] LT",nextWeek:"dddd [à] LT",lastDay:"[ieiri à] LT",lastWeek:"[sür el] dddd [lasteu à] LT",sameElse:"L"},relativeTime:{future:"osprei %s",past:"ja%s",s:Zd,m:Zd,mm:Zd,h:Zd,hh:Zd,d:Zd,dd:Zd,M:Zd,MM:Zd,y:Zd,yy:Zd},ordinalParse:/\d{1,2}\./,ordinal:"%d.",week:{dow:1,doy:4}}),bg.defineLocale("tzm-latn",{months:"innayr_brˤayrˤ_marˤsˤ_ibrir_mayyw_ywnyw_ywlywz_ɣwšt_šwtanbir_ktˤwbrˤ_nwwanbir_dwjnbir".split("_"),monthsShort:"innayr_brˤayrˤ_marˤsˤ_ibrir_mayyw_ywnyw_ywlywz_ɣwšt_šwtanbir_ktˤwbrˤ_nwwanbir_dwjnbir".split("_"),weekdays:"asamas_aynas_asinas_akras_akwas_asimwas_asiḍyas".split("_"),weekdaysShort:"asamas_aynas_asinas_akras_akwas_asimwas_asiḍyas".split("_"),weekdaysMin:"asamas_aynas_asinas_akras_akwas_asimwas_asiḍyas".split("_"),longDateFormat:{LT:"HH:mm",LTS:"HH:mm:ss",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY HH:mm",LLLL:"dddd D MMMM YYYY HH:mm"},calendar:{sameDay:"[asdkh g] LT",nextDay:"[aska g] LT",nextWeek:"dddd [g] LT",lastDay:"[assant g] LT",lastWeek:"dddd [g] LT",sameElse:"L"},relativeTime:{future:"dadkh s yan %s",past:"yan %s",s:"imik",m:"minuḍ",mm:"%d minuḍ",h:"saɛa",hh:"%d tassaɛin",d:"ass",dd:"%d ossan",M:"ayowr",MM:"%d iyyirn",y:"asgas",yy:"%d isgasn"},week:{dow:6,doy:12}}),bg.defineLocale("tzm",{months:"ⵉⵏⵏⴰⵢⵔ_ⴱⵕⴰⵢⵕ_ⵎⴰⵕⵚ_ⵉⴱⵔⵉⵔ_ⵎⴰⵢⵢⵓ_ⵢⵓⵏⵢⵓ_ⵢⵓⵍⵢⵓⵣ_ⵖⵓⵛⵜ_ⵛⵓⵜⴰⵏⴱⵉⵔ_ⴽⵟⵓⴱⵕ_ⵏⵓⵡⴰⵏⴱⵉⵔ_ⴷⵓⵊⵏⴱⵉⵔ".split("_"),monthsShort:"ⵉⵏⵏⴰⵢⵔ_ⴱⵕⴰⵢⵕ_ⵎⴰⵕⵚ_ⵉⴱⵔⵉⵔ_ⵎⴰⵢⵢⵓ_ⵢⵓⵏⵢⵓ_ⵢⵓⵍⵢⵓⵣ_ⵖⵓⵛⵜ_ⵛⵓⵜⴰⵏⴱⵉⵔ_ⴽⵟⵓⴱⵕ_ⵏⵓⵡⴰⵏⴱⵉⵔ_ⴷⵓⵊⵏⴱⵉⵔ".split("_"),weekdays:"ⴰⵙⴰⵎⴰⵙ_ⴰⵢⵏⴰⵙ_ⴰⵙⵉⵏⴰⵙ_ⴰⴽⵔⴰⵙ_ⴰⴽⵡⴰⵙ_ⴰⵙⵉⵎⵡⴰⵙ_ⴰⵙⵉⴹⵢⴰⵙ".split("_"),weekdaysShort:"ⴰⵙⴰⵎⴰⵙ_ⴰⵢⵏⴰⵙ_ⴰⵙⵉⵏⴰⵙ_ⴰⴽⵔⴰⵙ_ⴰⴽⵡⴰⵙ_ⴰⵙⵉⵎⵡⴰⵙ_ⴰⵙⵉⴹⵢⴰⵙ".split("_"),weekdaysMin:"ⴰⵙⴰⵎⴰⵙ_ⴰⵢⵏⴰⵙ_ⴰⵙⵉⵏⴰⵙ_ⴰⴽⵔⴰⵙ_ⴰⴽⵡⴰⵙ_ⴰⵙⵉⵎⵡⴰⵙ_ⴰⵙⵉⴹⵢⴰⵙ".split("_"),longDateFormat:{LT:"HH:mm",LTS:"HH:mm:ss",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY HH:mm",LLLL:"dddd D MMMM YYYY HH:mm"},calendar:{sameDay:"[ⴰⵙⴷⵅ ⴴ] LT",nextDay:"[ⴰⵙⴽⴰ ⴴ] LT",nextWeek:"dddd [ⴴ] LT",lastDay:"[ⴰⵚⴰⵏⵜ ⴴ] LT",lastWeek:"dddd [ⴴ] LT",sameElse:"L"},relativeTime:{future:"ⴷⴰⴷⵅ ⵙ ⵢⴰⵏ %s",past:"ⵢⴰⵏ %s",s:"ⵉⵎⵉⴽ",m:"ⵎⵉⵏⵓⴺ",mm:"%d ⵎⵉⵏⵓⴺ",h:"ⵙⴰⵄⴰ",hh:"%d ⵜⴰⵙⵙⴰⵄⵉⵏ",d:"ⴰⵙⵙ",dd:"%d oⵙⵙⴰⵏ",M:"ⴰⵢoⵓⵔ",MM:"%d ⵉⵢⵢⵉⵔⵏ",y:"ⴰⵙⴳⴰⵙ",yy:"%d ⵉⵙⴳⴰⵙⵏ"},week:{dow:6,doy:12}}),bg.defineLocale("uk",{months:{format:"січня_лютого_березня_квітня_травня_червня_липня_серпня_вересня_жовтня_листопада_грудня".split("_"),standalone:"січень_лютий_березень_квітень_травень_червень_липень_серпень_вересень_жовтень_листопад_грудень".split("_")},monthsShort:"січ_лют_бер_квіт_трав_черв_лип_серп_вер_жовт_лист_груд".split("_"),weekdays:ae,weekdaysShort:"нд_пн_вт_ср_чт_пт_сб".split("_"),weekdaysMin:"нд_пн_вт_ср_чт_пт_сб".split("_"),longDateFormat:{LT:"HH:mm",LTS:"HH:mm:ss",L:"DD.MM.YYYY",LL:"D MMMM YYYY р.",LLL:"D MMMM YYYY р., HH:mm",LLLL:"dddd, D MMMM YYYY р., HH:mm"},calendar:{sameDay:be("[Сьогодні "),nextDay:be("[Завтра "),lastDay:be("[Вчора "),nextWeek:be("[У] dddd ["),lastWeek:function lastWeek(){switch(this.day()){case 0:case 3:case 5:case 6:return be("[Минулої] dddd [").call(this);case 1:case 2:case 4:return be("[Минулого] dddd [").call(this);}},sameElse:"L"},relativeTime:{future:"за %s",past:"%s тому",s:"декілька секунд",m:_d,mm:_d,h:"годину",hh:_d,d:"день",dd:_d,M:"місяць",MM:_d,y:"рік",yy:_d},meridiemParse:/ночі|ранку|дня|вечора/,isPM:function isPM(a){return (/^(дня|вечора)$/.test(a));},meridiem:function meridiem(a,b,c){return 4>a?"ночі":12>a?"ранку":17>a?"дня":"вечора";},ordinalParse:/\d{1,2}-(й|го)/,ordinal:function ordinal(a,b){switch(b){case "M":case "d":case "DDD":case "w":case "W":return a+"-й";case "D":return a+"-го";default:return a;}},week:{dow:1,doy:7}}),bg.defineLocale("uz",{months:"январ_феврал_март_апрел_май_июн_июл_август_сентябр_октябр_ноябр_декабр".split("_"),monthsShort:"янв_фев_мар_апр_май_июн_июл_авг_сен_окт_ноя_дек".split("_"),weekdays:"Якшанба_Душанба_Сешанба_Чоршанба_Пайшанба_Жума_Шанба".split("_"),weekdaysShort:"Якш_Душ_Сеш_Чор_Пай_Жум_Шан".split("_"),weekdaysMin:"Як_Ду_Се_Чо_Па_Жу_Ша".split("_"),longDateFormat:{LT:"HH:mm",LTS:"HH:mm:ss",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY HH:mm",LLLL:"D MMMM YYYY, dddd HH:mm"},calendar:{sameDay:"[Бугун соат] LT [да]",nextDay:"[Эртага] LT [да]",nextWeek:"dddd [куни соат] LT [да]",lastDay:"[Кеча соат] LT [да]",lastWeek:"[Утган] dddd [куни соат] LT [да]",sameElse:"L"},relativeTime:{future:"Якин %s ичида",past:"Бир неча %s олдин",s:"фурсат",m:"бир дакика",mm:"%d дакика",h:"бир соат",hh:"%d соат",d:"бир кун",dd:"%d кун",M:"бир ой",MM:"%d ой",y:"бир йил",yy:"%d йил"},week:{dow:1,doy:7}}),bg.defineLocale("vi",{months:"tháng 1_tháng 2_tháng 3_tháng 4_tháng 5_tháng 6_tháng 7_tháng 8_tháng 9_tháng 10_tháng 11_tháng 12".split("_"),monthsShort:"Th01_Th02_Th03_Th04_Th05_Th06_Th07_Th08_Th09_Th10_Th11_Th12".split("_"),monthsParseExact:!0,weekdays:"chủ nhật_thứ hai_thứ ba_thứ tư_thứ năm_thứ sáu_thứ bảy".split("_"),weekdaysShort:"CN_T2_T3_T4_T5_T6_T7".split("_"),weekdaysMin:"CN_T2_T3_T4_T5_T6_T7".split("_"),weekdaysParseExact:!0,meridiemParse:/sa|ch/i,isPM:function isPM(a){return (/^ch$/i.test(a));},meridiem:function meridiem(a,b,c){return 12>a?c?"sa":"SA":c?"ch":"CH";},longDateFormat:{LT:"HH:mm",LTS:"HH:mm:ss",L:"DD/MM/YYYY",LL:"D MMMM [năm] YYYY",LLL:"D MMMM [năm] YYYY HH:mm",LLLL:"dddd, D MMMM [năm] YYYY HH:mm",l:"DD/M/YYYY",ll:"D MMM YYYY",lll:"D MMM YYYY HH:mm",llll:"ddd, D MMM YYYY HH:mm"},calendar:{sameDay:"[Hôm nay lúc] LT",nextDay:"[Ngày mai lúc] LT",nextWeek:"dddd [tuần tới lúc] LT",lastDay:"[Hôm qua lúc] LT",lastWeek:"dddd [tuần rồi lúc] LT",sameElse:"L"},relativeTime:{future:"%s tới",past:"%s trước",s:"vài giây",m:"một phút",mm:"%d phút",h:"một giờ",hh:"%d giờ",d:"một ngày",dd:"%d ngày",M:"một tháng",MM:"%d tháng",y:"một năm",yy:"%d năm"},ordinalParse:/\d{1,2}/,ordinal:function ordinal(a){return a;},week:{dow:1,doy:4}}),bg.defineLocale("x-pseudo",{months:"J~áñúá~rý_F~ébrú~árý_~Márc~h_Áp~ríl_~Máý_~Júñé~_Júl~ý_Áú~gúst~_Sép~témb~ér_Ó~ctób~ér_Ñ~óvém~bér_~Décé~mbér".split("_"),monthsShort:"J~áñ_~Féb_~Már_~Ápr_~Máý_~Júñ_~Júl_~Áúg_~Sép_~Óct_~Ñóv_~Déc".split("_"),monthsParseExact:!0,weekdays:"S~úñdá~ý_Mó~ñdáý~_Túé~sdáý~_Wéd~ñésd~áý_T~húrs~dáý_~Fríd~áý_S~átúr~dáý".split("_"),weekdaysShort:"S~úñ_~Móñ_~Túé_~Wéd_~Thú_~Frí_~Sát".split("_"),weekdaysMin:"S~ú_Mó~_Tú_~Wé_T~h_Fr~_Sá".split("_"),weekdaysParseExact:!0,longDateFormat:{LT:"HH:mm",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY HH:mm",LLLL:"dddd, D MMMM YYYY HH:mm"},calendar:{sameDay:"[T~ódá~ý át] LT",nextDay:"[T~ómó~rró~w át] LT",nextWeek:"dddd [át] LT",lastDay:"[Ý~ést~érdá~ý át] LT",lastWeek:"[L~ást] dddd [át] LT",sameElse:"L"},relativeTime:{future:"í~ñ %s",past:"%s á~gó",s:"á ~féw ~sécó~ñds",m:"á ~míñ~úté",mm:"%d m~íñú~tés",h:"á~ñ hó~úr",hh:"%d h~óúrs",d:"á ~dáý",dd:"%d d~áýs",M:"á ~móñ~th",MM:"%d m~óñt~hs",y:"á ~ýéár",yy:"%d ý~éárs"},ordinalParse:/\d{1,2}(th|st|nd|rd)/,ordinal:function ordinal(a){var b=a%10,c=1===~ ~(a%100/10)?"th":1===b?"st":2===b?"nd":3===b?"rd":"th";return a+c;},week:{dow:1,doy:4}}),bg.defineLocale("zh-cn",{months:"一月_二月_三月_四月_五月_六月_七月_八月_九月_十月_十一月_十二月".split("_"),monthsShort:"1月_2月_3月_4月_5月_6月_7月_8月_9月_10月_11月_12月".split("_"),weekdays:"星期日_星期一_星期二_星期三_星期四_星期五_星期六".split("_"),weekdaysShort:"周日_周一_周二_周三_周四_周五_周六".split("_"),weekdaysMin:"日_一_二_三_四_五_六".split("_"),longDateFormat:{LT:"Ah点mm分",LTS:"Ah点m分s秒",L:"YYYY-MM-DD",LL:"YYYY年MMMD日",LLL:"YYYY年MMMD日Ah点mm分",LLLL:"YYYY年MMMD日ddddAh点mm分",l:"YYYY-MM-DD",ll:"YYYY年MMMD日",lll:"YYYY年MMMD日Ah点mm分",llll:"YYYY年MMMD日ddddAh点mm分"},meridiemParse:/凌晨|早上|上午|中午|下午|晚上/,meridiemHour:function meridiemHour(a,b){return 12===a&&(a=0),"凌晨"===b||"早上"===b||"上午"===b?a:"下午"===b||"晚上"===b?a+12:a>=11?a:a+12;},meridiem:function meridiem(a,b,c){var d=100*a+b;return 600>d?"凌晨":900>d?"早上":1130>d?"上午":1230>d?"中午":1800>d?"下午":"晚上";},calendar:{sameDay:function sameDay(){return 0===this.minutes()?"[今天]Ah[点整]":"[今天]LT";},nextDay:function nextDay(){return 0===this.minutes()?"[明天]Ah[点整]":"[明天]LT";},lastDay:function lastDay(){return 0===this.minutes()?"[昨天]Ah[点整]":"[昨天]LT";},nextWeek:function nextWeek(){var a,b;return a=bg().startOf("week"),b=this.diff(a,"days")>=7?"[下]":"[本]",0===this.minutes()?b+"dddAh点整":b+"dddAh点mm";},lastWeek:function lastWeek(){var a,b;return a=bg().startOf("week"),b=this.unix()<a.unix()?"[上]":"[本]",0===this.minutes()?b+"dddAh点整":b+"dddAh点mm";},sameElse:"LL"},ordinalParse:/\d{1,2}(日|月|周)/,ordinal:function ordinal(a,b){switch(b){case "d":case "D":case "DDD":return a+"日";case "M":return a+"月";case "w":case "W":return a+"周";default:return a;}},relativeTime:{future:"%s内",past:"%s前",s:"几秒",m:"1 分钟",mm:"%d 分钟",h:"1 小时",hh:"%d 小时",d:"1 天",dd:"%d 天",M:"1 个月",MM:"%d 个月",y:"1 年",yy:"%d 年"},week:{dow:1,doy:4}}),bg.defineLocale("zh-tw",{months:"一月_二月_三月_四月_五月_六月_七月_八月_九月_十月_十一月_十二月".split("_"),monthsShort:"1月_2月_3月_4月_5月_6月_7月_8月_9月_10月_11月_12月".split("_"),weekdays:"星期日_星期一_星期二_星期三_星期四_星期五_星期六".split("_"),weekdaysShort:"週日_週一_週二_週三_週四_週五_週六".split("_"),weekdaysMin:"日_一_二_三_四_五_六".split("_"),longDateFormat:{LT:"Ah點mm分",LTS:"Ah點m分s秒",L:"YYYY年MMMD日",LL:"YYYY年MMMD日",LLL:"YYYY年MMMD日Ah點mm分",LLLL:"YYYY年MMMD日ddddAh點mm分",l:"YYYY年MMMD日",ll:"YYYY年MMMD日",lll:"YYYY年MMMD日Ah點mm分",llll:"YYYY年MMMD日ddddAh點mm分"},meridiemParse:/早上|上午|中午|下午|晚上/,meridiemHour:function meridiemHour(a,b){return 12===a&&(a=0),"早上"===b||"上午"===b?a:"中午"===b?a>=11?a:a+12:"下午"===b||"晚上"===b?a+12:void 0;},meridiem:function meridiem(a,b,c){var d=100*a+b;return 900>d?"早上":1130>d?"上午":1230>d?"中午":1800>d?"下午":"晚上";},calendar:{sameDay:"[今天]LT",nextDay:"[明天]LT",nextWeek:"[下]ddddLT",lastDay:"[昨天]LT",lastWeek:"[上]ddddLT",sameElse:"L"},ordinalParse:/\d{1,2}(日|月|週)/,ordinal:function ordinal(a,b){switch(b){case "d":case "D":case "DDD":return a+"日";case "M":return a+"月";case "w":case "W":return a+"週";default:return a;}},relativeTime:{future:"%s內",past:"%s前",s:"幾秒",m:"1分鐘",mm:"%d分鐘",h:"1小時",hh:"%d小時",d:"1天",dd:"%d天",M:"1個月",MM:"%d個月",y:"1年",yy:"%d年"}}),bg);return hh.locale("en"),hh;});
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(21)(module)))

/***/ },
/* 37 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	// Copyright Joyent, Inc. and other Node contributors.
	//
	// Permission is hereby granted, free of charge, to any person obtaining a
	// copy of this software and associated documentation files (the
	// "Software"), to deal in the Software without restriction, including
	// without limitation the rights to use, copy, modify, merge, publish,
	// distribute, sublicense, and/or sell copies of the Software, and to permit
	// persons to whom the Software is furnished to do so, subject to the
	// following conditions:
	//
	// The above copyright notice and this permission notice shall be included
	// in all copies or substantial portions of the Software.
	//
	// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
	// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
	// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
	// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
	// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
	// USE OR OTHER DEALINGS IN THE SOFTWARE.

	'use strict';

	var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };

	var punycode = __webpack_require__(69);
	var util = __webpack_require__(73);

	exports.parse = urlParse;
	exports.resolve = urlResolve;
	exports.resolveObject = urlResolveObject;
	exports.format = urlFormat;

	exports.Url = Url;

	function Url() {
	  this.protocol = null;
	  this.slashes = null;
	  this.auth = null;
	  this.host = null;
	  this.port = null;
	  this.hostname = null;
	  this.hash = null;
	  this.search = null;
	  this.query = null;
	  this.pathname = null;
	  this.path = null;
	  this.href = null;
	}

	// Reference: RFC 3986, RFC 1808, RFC 2396

	// define these here so at least they only have to be
	// compiled once on the first module load.
	var protocolPattern = /^([a-z0-9.+-]+:)/i,
	    portPattern = /:[0-9]*$/,


	// Special case for a simple path URL
	simplePathPattern = /^(\/\/?(?!\/)[^\?\s]*)(\?[^\s]*)?$/,


	// RFC 2396: characters reserved for delimiting URLs.
	// We actually just auto-escape these.
	delims = ['<', '>', '"', '`', ' ', '\r', '\n', '\t'],


	// RFC 2396: characters not allowed for various reasons.
	unwise = ['{', '}', '|', '\\', '^', '`'].concat(delims),


	// Allowed by RFCs, but cause of XSS attacks.  Always escape these.
	autoEscape = ['\''].concat(unwise),

	// Characters that are never ever allowed in a hostname.
	// Note that any invalid chars are also handled, but these
	// are the ones that are *expected* to be seen, so we fast-path
	// them.
	nonHostChars = ['%', '/', '?', ';', '#'].concat(autoEscape),
	    hostEndingChars = ['/', '?', '#'],
	    hostnameMaxLen = 255,
	    hostnamePartPattern = /^[+a-z0-9A-Z_-]{0,63}$/,
	    hostnamePartStart = /^([+a-z0-9A-Z_-]{0,63})(.*)$/,

	// protocols that can allow "unsafe" and "unwise" chars.
	unsafeProtocol = {
	  'javascript': true,
	  'javascript:': true
	},

	// protocols that never have a hostname.
	hostlessProtocol = {
	  'javascript': true,
	  'javascript:': true
	},

	// protocols that always contain a // bit.
	slashedProtocol = {
	  'http': true,
	  'https': true,
	  'ftp': true,
	  'gopher': true,
	  'file': true,
	  'http:': true,
	  'https:': true,
	  'ftp:': true,
	  'gopher:': true,
	  'file:': true
	},
	    querystring = __webpack_require__(72);

	function urlParse(url, parseQueryString, slashesDenoteHost) {
	  if (url && util.isObject(url) && url instanceof Url) return url;

	  var u = new Url();
	  u.parse(url, parseQueryString, slashesDenoteHost);
	  return u;
	}

	Url.prototype.parse = function (url, parseQueryString, slashesDenoteHost) {
	  if (!util.isString(url)) {
	    throw new TypeError("Parameter 'url' must be a string, not " + (typeof url === 'undefined' ? 'undefined' : _typeof(url)));
	  }

	  // Copy chrome, IE, opera backslash-handling behavior.
	  // Back slashes before the query string get converted to forward slashes
	  // See: https://code.google.com/p/chromium/issues/detail?id=25916
	  var queryIndex = url.indexOf('?'),
	      splitter = queryIndex !== -1 && queryIndex < url.indexOf('#') ? '?' : '#',
	      uSplit = url.split(splitter),
	      slashRegex = /\\/g;
	  uSplit[0] = uSplit[0].replace(slashRegex, '/');
	  url = uSplit.join(splitter);

	  var rest = url;

	  // trim before proceeding.
	  // This is to support parse stuff like "  http://foo.com  \n"
	  rest = rest.trim();

	  if (!slashesDenoteHost && url.split('#').length === 1) {
	    // Try fast path regexp
	    var simplePath = simplePathPattern.exec(rest);
	    if (simplePath) {
	      this.path = rest;
	      this.href = rest;
	      this.pathname = simplePath[1];
	      if (simplePath[2]) {
	        this.search = simplePath[2];
	        if (parseQueryString) {
	          this.query = querystring.parse(this.search.substr(1));
	        } else {
	          this.query = this.search.substr(1);
	        }
	      } else if (parseQueryString) {
	        this.search = '';
	        this.query = {};
	      }
	      return this;
	    }
	  }

	  var proto = protocolPattern.exec(rest);
	  if (proto) {
	    proto = proto[0];
	    var lowerProto = proto.toLowerCase();
	    this.protocol = lowerProto;
	    rest = rest.substr(proto.length);
	  }

	  // figure out if it's got a host
	  // user@server is *always* interpreted as a hostname, and url
	  // resolution will treat //foo/bar as host=foo,path=bar because that's
	  // how the browser resolves relative URLs.
	  if (slashesDenoteHost || proto || rest.match(/^\/\/[^@\/]+@[^@\/]+/)) {
	    var slashes = rest.substr(0, 2) === '//';
	    if (slashes && !(proto && hostlessProtocol[proto])) {
	      rest = rest.substr(2);
	      this.slashes = true;
	    }
	  }

	  if (!hostlessProtocol[proto] && (slashes || proto && !slashedProtocol[proto])) {

	    // there's a hostname.
	    // the first instance of /, ?, ;, or # ends the host.
	    //
	    // If there is an @ in the hostname, then non-host chars *are* allowed
	    // to the left of the last @ sign, unless some host-ending character
	    // comes *before* the @-sign.
	    // URLs are obnoxious.
	    //
	    // ex:
	    // http://a@b@c/ => user:a@b host:c
	    // http://a@b?@c => user:a host:c path:/?@c

	    // v0.12 TODO(isaacs): This is not quite how Chrome does things.
	    // Review our test case against browsers more comprehensively.

	    // find the first instance of any hostEndingChars
	    var hostEnd = -1;
	    for (var i = 0; i < hostEndingChars.length; i++) {
	      var hec = rest.indexOf(hostEndingChars[i]);
	      if (hec !== -1 && (hostEnd === -1 || hec < hostEnd)) hostEnd = hec;
	    }

	    // at this point, either we have an explicit point where the
	    // auth portion cannot go past, or the last @ char is the decider.
	    var auth, atSign;
	    if (hostEnd === -1) {
	      // atSign can be anywhere.
	      atSign = rest.lastIndexOf('@');
	    } else {
	      // atSign must be in auth portion.
	      // http://a@b/c@d => host:b auth:a path:/c@d
	      atSign = rest.lastIndexOf('@', hostEnd);
	    }

	    // Now we have a portion which is definitely the auth.
	    // Pull that off.
	    if (atSign !== -1) {
	      auth = rest.slice(0, atSign);
	      rest = rest.slice(atSign + 1);
	      this.auth = decodeURIComponent(auth);
	    }

	    // the host is the remaining to the left of the first non-host char
	    hostEnd = -1;
	    for (var i = 0; i < nonHostChars.length; i++) {
	      var hec = rest.indexOf(nonHostChars[i]);
	      if (hec !== -1 && (hostEnd === -1 || hec < hostEnd)) hostEnd = hec;
	    }
	    // if we still have not hit it, then the entire thing is a host.
	    if (hostEnd === -1) hostEnd = rest.length;

	    this.host = rest.slice(0, hostEnd);
	    rest = rest.slice(hostEnd);

	    // pull out port.
	    this.parseHost();

	    // we've indicated that there is a hostname,
	    // so even if it's empty, it has to be present.
	    this.hostname = this.hostname || '';

	    // if hostname begins with [ and ends with ]
	    // assume that it's an IPv6 address.
	    var ipv6Hostname = this.hostname[0] === '[' && this.hostname[this.hostname.length - 1] === ']';

	    // validate a little.
	    if (!ipv6Hostname) {
	      var hostparts = this.hostname.split(/\./);
	      for (var i = 0, l = hostparts.length; i < l; i++) {
	        var part = hostparts[i];
	        if (!part) continue;
	        if (!part.match(hostnamePartPattern)) {
	          var newpart = '';
	          for (var j = 0, k = part.length; j < k; j++) {
	            if (part.charCodeAt(j) > 127) {
	              // we replace non-ASCII char with a temporary placeholder
	              // we need this to make sure size of hostname is not
	              // broken by replacing non-ASCII by nothing
	              newpart += 'x';
	            } else {
	              newpart += part[j];
	            }
	          }
	          // we test again with ASCII char only
	          if (!newpart.match(hostnamePartPattern)) {
	            var validParts = hostparts.slice(0, i);
	            var notHost = hostparts.slice(i + 1);
	            var bit = part.match(hostnamePartStart);
	            if (bit) {
	              validParts.push(bit[1]);
	              notHost.unshift(bit[2]);
	            }
	            if (notHost.length) {
	              rest = '/' + notHost.join('.') + rest;
	            }
	            this.hostname = validParts.join('.');
	            break;
	          }
	        }
	      }
	    }

	    if (this.hostname.length > hostnameMaxLen) {
	      this.hostname = '';
	    } else {
	      // hostnames are always lower case.
	      this.hostname = this.hostname.toLowerCase();
	    }

	    if (!ipv6Hostname) {
	      // IDNA Support: Returns a punycoded representation of "domain".
	      // It only converts parts of the domain name that
	      // have non-ASCII characters, i.e. it doesn't matter if
	      // you call it with a domain that already is ASCII-only.
	      this.hostname = punycode.toASCII(this.hostname);
	    }

	    var p = this.port ? ':' + this.port : '';
	    var h = this.hostname || '';
	    this.host = h + p;
	    this.href += this.host;

	    // strip [ and ] from the hostname
	    // the host field still retains them, though
	    if (ipv6Hostname) {
	      this.hostname = this.hostname.substr(1, this.hostname.length - 2);
	      if (rest[0] !== '/') {
	        rest = '/' + rest;
	      }
	    }
	  }

	  // now rest is set to the post-host stuff.
	  // chop off any delim chars.
	  if (!unsafeProtocol[lowerProto]) {

	    // First, make 100% sure that any "autoEscape" chars get
	    // escaped, even if encodeURIComponent doesn't think they
	    // need to be.
	    for (var i = 0, l = autoEscape.length; i < l; i++) {
	      var ae = autoEscape[i];
	      if (rest.indexOf(ae) === -1) continue;
	      var esc = encodeURIComponent(ae);
	      if (esc === ae) {
	        esc = escape(ae);
	      }
	      rest = rest.split(ae).join(esc);
	    }
	  }

	  // chop off from the tail first.
	  var hash = rest.indexOf('#');
	  if (hash !== -1) {
	    // got a fragment string.
	    this.hash = rest.substr(hash);
	    rest = rest.slice(0, hash);
	  }
	  var qm = rest.indexOf('?');
	  if (qm !== -1) {
	    this.search = rest.substr(qm);
	    this.query = rest.substr(qm + 1);
	    if (parseQueryString) {
	      this.query = querystring.parse(this.query);
	    }
	    rest = rest.slice(0, qm);
	  } else if (parseQueryString) {
	    // no query string, but parseQueryString still requested
	    this.search = '';
	    this.query = {};
	  }
	  if (rest) this.pathname = rest;
	  if (slashedProtocol[lowerProto] && this.hostname && !this.pathname) {
	    this.pathname = '/';
	  }

	  //to support http.request
	  if (this.pathname || this.search) {
	    var p = this.pathname || '';
	    var s = this.search || '';
	    this.path = p + s;
	  }

	  // finally, reconstruct the href based on what has been validated.
	  this.href = this.format();
	  return this;
	};

	// format a parsed object into a url string
	function urlFormat(obj) {
	  // ensure it's an object, and not a string url.
	  // If it's an obj, this is a no-op.
	  // this way, you can call url_format() on strings
	  // to clean up potentially wonky urls.
	  if (util.isString(obj)) obj = urlParse(obj);
	  if (!(obj instanceof Url)) return Url.prototype.format.call(obj);
	  return obj.format();
	}

	Url.prototype.format = function () {
	  var auth = this.auth || '';
	  if (auth) {
	    auth = encodeURIComponent(auth);
	    auth = auth.replace(/%3A/i, ':');
	    auth += '@';
	  }

	  var protocol = this.protocol || '',
	      pathname = this.pathname || '',
	      hash = this.hash || '',
	      host = false,
	      query = '';

	  if (this.host) {
	    host = auth + this.host;
	  } else if (this.hostname) {
	    host = auth + (this.hostname.indexOf(':') === -1 ? this.hostname : '[' + this.hostname + ']');
	    if (this.port) {
	      host += ':' + this.port;
	    }
	  }

	  if (this.query && util.isObject(this.query) && Object.keys(this.query).length) {
	    query = querystring.stringify(this.query);
	  }

	  var search = this.search || query && '?' + query || '';

	  if (protocol && protocol.substr(-1) !== ':') protocol += ':';

	  // only the slashedProtocols get the //.  Not mailto:, xmpp:, etc.
	  // unless they had them to begin with.
	  if (this.slashes || (!protocol || slashedProtocol[protocol]) && host !== false) {
	    host = '//' + (host || '');
	    if (pathname && pathname.charAt(0) !== '/') pathname = '/' + pathname;
	  } else if (!host) {
	    host = '';
	  }

	  if (hash && hash.charAt(0) !== '#') hash = '#' + hash;
	  if (search && search.charAt(0) !== '?') search = '?' + search;

	  pathname = pathname.replace(/[?#]/g, function (match) {
	    return encodeURIComponent(match);
	  });
	  search = search.replace('#', '%23');

	  return protocol + host + pathname + search + hash;
	};

	function urlResolve(source, relative) {
	  return urlParse(source, false, true).resolve(relative);
	}

	Url.prototype.resolve = function (relative) {
	  return this.resolveObject(urlParse(relative, false, true)).format();
	};

	function urlResolveObject(source, relative) {
	  if (!source) return relative;
	  return urlParse(source, false, true).resolveObject(relative);
	}

	Url.prototype.resolveObject = function (relative) {
	  if (util.isString(relative)) {
	    var rel = new Url();
	    rel.parse(relative, false, true);
	    relative = rel;
	  }

	  var result = new Url();
	  var tkeys = Object.keys(this);
	  for (var tk = 0; tk < tkeys.length; tk++) {
	    var tkey = tkeys[tk];
	    result[tkey] = this[tkey];
	  }

	  // hash is always overridden, no matter what.
	  // even href="" will remove it.
	  result.hash = relative.hash;

	  // if the relative url is empty, then there's nothing left to do here.
	  if (relative.href === '') {
	    result.href = result.format();
	    return result;
	  }

	  // hrefs like //foo/bar always cut to the protocol.
	  if (relative.slashes && !relative.protocol) {
	    // take everything except the protocol from relative
	    var rkeys = Object.keys(relative);
	    for (var rk = 0; rk < rkeys.length; rk++) {
	      var rkey = rkeys[rk];
	      if (rkey !== 'protocol') result[rkey] = relative[rkey];
	    }

	    //urlParse appends trailing / to urls like http://www.example.com
	    if (slashedProtocol[result.protocol] && result.hostname && !result.pathname) {
	      result.path = result.pathname = '/';
	    }

	    result.href = result.format();
	    return result;
	  }

	  if (relative.protocol && relative.protocol !== result.protocol) {
	    // if it's a known url protocol, then changing
	    // the protocol does weird things
	    // first, if it's not file:, then we MUST have a host,
	    // and if there was a path
	    // to begin with, then we MUST have a path.
	    // if it is file:, then the host is dropped,
	    // because that's known to be hostless.
	    // anything else is assumed to be absolute.
	    if (!slashedProtocol[relative.protocol]) {
	      var keys = Object.keys(relative);
	      for (var v = 0; v < keys.length; v++) {
	        var k = keys[v];
	        result[k] = relative[k];
	      }
	      result.href = result.format();
	      return result;
	    }

	    result.protocol = relative.protocol;
	    if (!relative.host && !hostlessProtocol[relative.protocol]) {
	      var relPath = (relative.pathname || '').split('/');
	      while (relPath.length && !(relative.host = relPath.shift())) {}
	      if (!relative.host) relative.host = '';
	      if (!relative.hostname) relative.hostname = '';
	      if (relPath[0] !== '') relPath.unshift('');
	      if (relPath.length < 2) relPath.unshift('');
	      result.pathname = relPath.join('/');
	    } else {
	      result.pathname = relative.pathname;
	    }
	    result.search = relative.search;
	    result.query = relative.query;
	    result.host = relative.host || '';
	    result.auth = relative.auth;
	    result.hostname = relative.hostname || relative.host;
	    result.port = relative.port;
	    // to support http.request
	    if (result.pathname || result.search) {
	      var p = result.pathname || '';
	      var s = result.search || '';
	      result.path = p + s;
	    }
	    result.slashes = result.slashes || relative.slashes;
	    result.href = result.format();
	    return result;
	  }

	  var isSourceAbs = result.pathname && result.pathname.charAt(0) === '/',
	      isRelAbs = relative.host || relative.pathname && relative.pathname.charAt(0) === '/',
	      mustEndAbs = isRelAbs || isSourceAbs || result.host && relative.pathname,
	      removeAllDots = mustEndAbs,
	      srcPath = result.pathname && result.pathname.split('/') || [],
	      relPath = relative.pathname && relative.pathname.split('/') || [],
	      psychotic = result.protocol && !slashedProtocol[result.protocol];

	  // if the url is a non-slashed url, then relative
	  // links like ../.. should be able
	  // to crawl up to the hostname, as well.  This is strange.
	  // result.protocol has already been set by now.
	  // Later on, put the first path part into the host field.
	  if (psychotic) {
	    result.hostname = '';
	    result.port = null;
	    if (result.host) {
	      if (srcPath[0] === '') srcPath[0] = result.host;else srcPath.unshift(result.host);
	    }
	    result.host = '';
	    if (relative.protocol) {
	      relative.hostname = null;
	      relative.port = null;
	      if (relative.host) {
	        if (relPath[0] === '') relPath[0] = relative.host;else relPath.unshift(relative.host);
	      }
	      relative.host = null;
	    }
	    mustEndAbs = mustEndAbs && (relPath[0] === '' || srcPath[0] === '');
	  }

	  if (isRelAbs) {
	    // it's absolute.
	    result.host = relative.host || relative.host === '' ? relative.host : result.host;
	    result.hostname = relative.hostname || relative.hostname === '' ? relative.hostname : result.hostname;
	    result.search = relative.search;
	    result.query = relative.query;
	    srcPath = relPath;
	    // fall through to the dot-handling below.
	  } else if (relPath.length) {
	      // it's relative
	      // throw away the existing file, and take the new path instead.
	      if (!srcPath) srcPath = [];
	      srcPath.pop();
	      srcPath = srcPath.concat(relPath);
	      result.search = relative.search;
	      result.query = relative.query;
	    } else if (!util.isNullOrUndefined(relative.search)) {
	      // just pull out the search.
	      // like href='?foo'.
	      // Put this after the other two cases because it simplifies the booleans
	      if (psychotic) {
	        result.hostname = result.host = srcPath.shift();
	        //occationaly the auth can get stuck only in host
	        //this especially happens in cases like
	        //url.resolveObject('mailto:local1@domain1', 'local2@domain2')
	        var authInHost = result.host && result.host.indexOf('@') > 0 ? result.host.split('@') : false;
	        if (authInHost) {
	          result.auth = authInHost.shift();
	          result.host = result.hostname = authInHost.shift();
	        }
	      }
	      result.search = relative.search;
	      result.query = relative.query;
	      //to support http.request
	      if (!util.isNull(result.pathname) || !util.isNull(result.search)) {
	        result.path = (result.pathname ? result.pathname : '') + (result.search ? result.search : '');
	      }
	      result.href = result.format();
	      return result;
	    }

	  if (!srcPath.length) {
	    // no path at all.  easy.
	    // we've already handled the other stuff above.
	    result.pathname = null;
	    //to support http.request
	    if (result.search) {
	      result.path = '/' + result.search;
	    } else {
	      result.path = null;
	    }
	    result.href = result.format();
	    return result;
	  }

	  // if a url ENDs in . or .., then it must get a trailing slash.
	  // however, if it ends in anything else non-slashy,
	  // then it must NOT get a trailing slash.
	  var last = srcPath.slice(-1)[0];
	  var hasTrailingSlash = (result.host || relative.host || srcPath.length > 1) && (last === '.' || last === '..') || last === '';

	  // strip single dots, resolve double dots to parent dir
	  // if the path tries to go above the root, `up` ends up > 0
	  var up = 0;
	  for (var i = srcPath.length; i >= 0; i--) {
	    last = srcPath[i];
	    if (last === '.') {
	      srcPath.splice(i, 1);
	    } else if (last === '..') {
	      srcPath.splice(i, 1);
	      up++;
	    } else if (up) {
	      srcPath.splice(i, 1);
	      up--;
	    }
	  }

	  // if the path is allowed to go above the root, restore leading ..s
	  if (!mustEndAbs && !removeAllDots) {
	    for (; up--; up) {
	      srcPath.unshift('..');
	    }
	  }

	  if (mustEndAbs && srcPath[0] !== '' && (!srcPath[0] || srcPath[0].charAt(0) !== '/')) {
	    srcPath.unshift('');
	  }

	  if (hasTrailingSlash && srcPath.join('/').substr(-1) !== '/') {
	    srcPath.push('');
	  }

	  var isAbsolute = srcPath[0] === '' || srcPath[0] && srcPath[0].charAt(0) === '/';

	  // put the host back
	  if (psychotic) {
	    result.hostname = result.host = isAbsolute ? '' : srcPath.length ? srcPath.shift() : '';
	    //occationaly the auth can get stuck only in host
	    //this especially happens in cases like
	    //url.resolveObject('mailto:local1@domain1', 'local2@domain2')
	    var authInHost = result.host && result.host.indexOf('@') > 0 ? result.host.split('@') : false;
	    if (authInHost) {
	      result.auth = authInHost.shift();
	      result.host = result.hostname = authInHost.shift();
	    }
	  }

	  mustEndAbs = mustEndAbs || result.host && srcPath.length;

	  if (mustEndAbs && !isAbsolute) {
	    srcPath.unshift('');
	  }

	  if (!srcPath.length) {
	    result.pathname = null;
	    result.path = null;
	  } else {
	    result.pathname = srcPath.join('/');
	  }

	  //to support request.http
	  if (!util.isNull(result.pathname) || !util.isNull(result.search)) {
	    result.path = (result.pathname ? result.pathname : '') + (result.search ? result.search : '');
	  }
	  result.auth = relative.auth || result.auth;
	  result.slashes = result.slashes || relative.slashes;
	  result.href = result.format();
	  return result;
	};

	Url.prototype.parseHost = function () {
	  var host = this.host;
	  var port = portPattern.exec(host);
	  if (port) {
	    port = port[0];
	    if (port !== ':') {
	      this.port = port.substr(1);
	    }
	    host = host.substr(0, host.length - port.length);
	  }
	  if (host) this.hostname = host;
	};

/***/ },
/* 38 */,
/* 39 */,
/* 40 */,
/* 41 */,
/* 42 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	"use strict";

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	var _mobileDetect = __webpack_require__(35);

	var _mobileDetect2 = _interopRequireDefault(_mobileDetect);

	var _vueMin = __webpack_require__(2);

	var _vueMin2 = _interopRequireDefault(_vueMin);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	var resource = null,
	    events = __webpack_require__(34).EventEmitter;

	var className = [],
	    md = new _mobileDetect2.default(navigator.userAgent),
	    $url = __webpack_require__(37),
	    Base64 = __webpack_require__(28);

	_vueMin2.default.config.debug = false; //(NODE_ENV == 'dev');
	_vueMin2.default.config.devtools = false;

	// чекаем платформу
	if (md.phone()) className.push('mobile');
	if (md.tablet()) className.push('tablet');

	if (className.length > 0) document.getElementsByTagName('html')[0].className += ' ' + className.join(' ');

	var get_html_translation_table = function get_html_translation_table(table, quote_style) {
	    var entities = {},
	        hash_map = {},
	        decimal;
	    var constMappingTable = {},
	        constMappingQuoteStyle = {};
	    var useTable = {},
	        useQuoteStyle = {};

	    // Translate arguments
	    constMappingTable[0] = 'HTML_SPECIALCHARS';
	    constMappingTable[1] = 'HTML_ENTITIES';
	    constMappingQuoteStyle[0] = 'ENT_NOQUOTES';
	    constMappingQuoteStyle[2] = 'ENT_COMPAT';
	    constMappingQuoteStyle[3] = 'ENT_QUOTES';

	    useTable = !isNaN(table) ? constMappingTable[table] : table ? table.toUpperCase() : 'HTML_SPECIALCHARS';
	    useQuoteStyle = !isNaN(quote_style) ? constMappingQuoteStyle[quote_style] : quote_style ? quote_style.toUpperCase() : 'ENT_COMPAT';

	    if (useTable !== 'HTML_SPECIALCHARS' && useTable !== 'HTML_ENTITIES') {
	        throw new Error("Table: " + useTable + ' not supported');
	        // return false;
	    }

	    entities['38'] = '&amp;';
	    if (useTable === 'HTML_ENTITIES') {
	        entities['160'] = '&nbsp;';
	        entities['161'] = '&iexcl;';
	        entities['162'] = '&cent;';
	        entities['163'] = '&pound;';
	        entities['164'] = '&curren;';
	        entities['165'] = '&yen;';
	        entities['166'] = '&brvbar;';
	        entities['167'] = '&sect;';
	        entities['168'] = '&uml;';
	        entities['169'] = '&copy;';
	        entities['170'] = '&ordf;';
	        entities['171'] = '&laquo;';
	        entities['172'] = '&not;';
	        entities['173'] = '&shy;';
	        entities['174'] = '&reg;';
	        entities['175'] = '&macr;';
	        entities['176'] = '&deg;';
	        entities['177'] = '&plusmn;';
	        entities['178'] = '&sup2;';
	        entities['179'] = '&sup3;';
	        entities['180'] = '&acute;';
	        entities['181'] = '&micro;';
	        entities['182'] = '&para;';
	        entities['183'] = '&middot;';
	        entities['184'] = '&cedil;';
	        entities['185'] = '&sup1;';
	        entities['186'] = '&ordm;';
	        entities['187'] = '&raquo;';
	        entities['188'] = '&frac14;';
	        entities['189'] = '&frac12;';
	        entities['190'] = '&frac34;';
	        entities['191'] = '&iquest;';
	        entities['192'] = '&Agrave;';
	        entities['193'] = '&Aacute;';
	        entities['194'] = '&Acirc;';
	        entities['195'] = '&Atilde;';
	        entities['196'] = '&Auml;';
	        entities['197'] = '&Aring;';
	        entities['198'] = '&AElig;';
	        entities['199'] = '&Ccedil;';
	        entities['200'] = '&Egrave;';
	        entities['201'] = '&Eacute;';
	        entities['202'] = '&Ecirc;';
	        entities['203'] = '&Euml;';
	        entities['204'] = '&Igrave;';
	        entities['205'] = '&Iacute;';
	        entities['206'] = '&Icirc;';
	        entities['207'] = '&Iuml;';
	        entities['208'] = '&ETH;';
	        entities['209'] = '&Ntilde;';
	        entities['210'] = '&Ograve;';
	        entities['211'] = '&Oacute;';
	        entities['212'] = '&Ocirc;';
	        entities['213'] = '&Otilde;';
	        entities['214'] = '&Ouml;';
	        entities['215'] = '&times;';
	        entities['216'] = '&Oslash;';
	        entities['217'] = '&Ugrave;';
	        entities['218'] = '&Uacute;';
	        entities['219'] = '&Ucirc;';
	        entities['220'] = '&Uuml;';
	        entities['221'] = '&Yacute;';
	        entities['222'] = '&THORN;';
	        entities['223'] = '&szlig;';
	        entities['224'] = '&agrave;';
	        entities['225'] = '&aacute;';
	        entities['226'] = '&acirc;';
	        entities['227'] = '&atilde;';
	        entities['228'] = '&auml;';
	        entities['229'] = '&aring;';
	        entities['230'] = '&aelig;';
	        entities['231'] = '&ccedil;';
	        entities['232'] = '&egrave;';
	        entities['233'] = '&eacute;';
	        entities['234'] = '&ecirc;';
	        entities['235'] = '&euml;';
	        entities['236'] = '&igrave;';
	        entities['237'] = '&iacute;';
	        entities['238'] = '&icirc;';
	        entities['239'] = '&iuml;';
	        entities['240'] = '&eth;';
	        entities['241'] = '&ntilde;';
	        entities['242'] = '&ograve;';
	        entities['243'] = '&oacute;';
	        entities['244'] = '&ocirc;';
	        entities['245'] = '&otilde;';
	        entities['246'] = '&ouml;';
	        entities['247'] = '&divide;';
	        entities['248'] = '&oslash;';
	        entities['249'] = '&ugrave;';
	        entities['250'] = '&uacute;';
	        entities['251'] = '&ucirc;';
	        entities['252'] = '&uuml;';
	        entities['253'] = '&yacute;';
	        entities['254'] = '&thorn;';
	        entities['255'] = '&yuml;';
	    }

	    if (useQuoteStyle !== 'ENT_NOQUOTES') {
	        entities['34'] = '&quot;';
	    }
	    if (useQuoteStyle === 'ENT_QUOTES') {
	        entities['39'] = '&#39;';
	    }
	    entities['60'] = '&lt;';
	    entities['62'] = '&gt;';

	    // ascii decimals to real symbols
	    for (decimal in entities) {
	        if (entities.hasOwnProperty(decimal)) {
	            hash_map[String.fromCharCode(decimal)] = entities[decimal];
	        }
	    }

	    return hash_map;
	};

	var html_entity_decode = function html_entity_decode(string, quote_style) {

	    var hash_map = {},
	        symbol = '',
	        tmp_str = '',
	        entity = '';
	    tmp_str = string.toString();

	    if (false === (hash_map = this.get_html_translation_table('HTML_ENTITIES', quote_style))) {
	        return false;
	    }

	    // fix &amp; problem
	    // http://phpjs.org/functions/get_html_translation_table:416#comment_97660
	    delete hash_map['&'];
	    hash_map['&'] = '&amp;';

	    for (symbol in hash_map) {
	        entity = hash_map[symbol];
	        tmp_str = tmp_str.split(entity).join(symbol);
	    }
	    tmp_str = tmp_str.split('&#039;').join("'");

	    return tmp_str;
	};

	var Cookies = __webpack_require__(33); //https://github.com/js-cookie/js-cookie
	var Validation = __webpack_require__(29);
	var moment = __webpack_require__(36);

	var _util = __webpack_require__(26);

	var Core = function () {
	    function Core(config) {
	        _classCallCheck(this, Core);

	        this.detect = md;
	        this.Base64 = Base64;

	        this.moment = moment;

	        this.Store = __webpack_require__(25);
	        // this.SyncStore = require('./libs/syncStore');

	        this.Validation = Validation;
	        this.Cookies = Cookies;

	        this.events = new events.EventEmitter();

	        this.html_entity_decode = html_entity_decode;
	        this.get_html_translation_table = get_html_translation_table;

	        this.yandexCounterID = null;

	        if (config) {
	            this.yandexCounterID = config.yandexCounterID;
	        }

	        this.util = _util;
	    }

	    _createClass(Core, [{
	        key: 'get',
	        value: function get(property) {
	            var inStorage = arguments.length <= 1 || arguments[1] === undefined ? false : arguments[1];


	            if (inStorage) {

	                var data = sessionStorage.getItem(property);
	                if (data) return JSON.parse(data);else return this[property];
	            } else return this[property];
	        }
	    }, {
	        key: 'has',
	        value: function has(obj, name) {
	            return name in obj;
	        }
	    }, {
	        key: 'set',
	        value: function set(property, value) {
	            var inStorage = arguments.length <= 2 || arguments[2] === undefined ? false : arguments[2];


	            if (value) {
	                this[property] = value;

	                if (inStorage) {
	                    value = JSON.stringify(value);
	                    sessionStorage.setItem(property, value);
	                }
	            }
	        }
	    }, {
	        key: 'init',
	        value: function init() {}
	    }, {
	        key: 'parseUrl',
	        value: function parseUrl(url) {
	            url = url ? url : window.location.href;
	            return $url.parse(url, true);
	        }
	    }, {
	        key: 'GetUserCookie',
	        value: function GetUserCookie() {
	            var C = Cookies.get('mst_user');
	            if (C) {
	                try {
	                    return JSON.parse(Base64.decode(C));
	                } catch (e) {
	                    return {};
	                }
	            } else return {};
	        }
	    }, {
	        key: 'GetTplData',
	        value: function GetTplData() {
	            var C = Cookies.get('mst_tpl');
	            if (C) {
	                try {
	                    return JSON.parse(Base64.decode(C));
	                } catch (e) {
	                    return {};
	                }
	            } else return {};
	        }
	    }, {
	        key: 'GenerateHash',
	        value: function GenerateHash() {
	            var length = arguments.length <= 0 || arguments[0] === undefined ? 32 : arguments[0];

	            var n,
	                S = 'x',
	                self = this;

	            var hash = function hash(s) {

	                if (typeof s == 'number' && s === parseInt(s, 10)) s = Array(s + 1).join('x');

	                return s.replace(/x/g, function () {
	                    n = Math.round(Math.random() * 61) + 48;
	                    n = n > 57 ? n + 7 > 90 ? n + 13 : n + 7 : n;
	                    return String.fromCharCode(n);
	                });
	            };

	            for (var i = 0; i < length; i++) {
	                S = S + 'x';
	            }return hash(S);
	        }
	    }, {
	        key: 'getFormData',
	        value: function getFormData(form) {
	            var rv = {},
	                obj,
	                elements,
	                element,
	                index,
	                names,
	                nameIndex,
	                name,
	                value;

	            elements = form.elements;

	            if (elements) {
	                for (index = 0; index < elements.length; ++index) {
	                    element = elements[index];
	                    name = element.name;

	                    if (name) {

	                        value = $(element).val();

	                        if ($(element).is('[type="checkbox"]')) value = $(element).prop('checked');

	                        names = name.split(".");
	                        obj = rv;

	                        for (nameIndex = 0; nameIndex < names.length; ++nameIndex) {
	                            name = names[nameIndex];

	                            if (nameIndex == names.length - 1) obj[name] = value;else obj = obj[name] = obj[name] || {};
	                        }
	                    }
	                }
	            }

	            return rv;
	        }
	    }, {
	        key: 'param',
	        value: function param(json) {
	            return '?' + Object.keys(json).map(function (key) {
	                return encodeURIComponent(key) + '=' + encodeURIComponent(json[key]);
	            }).join('&');
	        }
	    }, {
	        key: 'CountSymbols',
	        value: function CountSymbols(str, count) {
	            return Math.round(count - str.length);
	        }
	    }, {
	        key: 'yandexEvent',
	        value: function yandexEvent() {
	            var nameEvent = arguments.length <= 0 || arguments[0] === undefined ? null : arguments[0];
	            var CounterID = arguments.length <= 1 || arguments[1] === undefined ? null : arguments[1];

	            var id = CounterID ? CounterID : this.yandexCounterID;
	            if (id && nameEvent) {}
	        }

	        /**
	         * Загрузка скриптов
	         * @param src - ссылка на файл
	         * @param async - ассинхронная загрузка
	         * @param callback
	         */

	    }, {
	        key: 'loadScripts',
	        value: function loadScripts(src) {
	            var async = arguments.length <= 1 || arguments[1] === undefined ? false : arguments[1];
	            var callback = arguments[2];

	            var el = document.createElement("script");
	            el.type = "text/javascript";
	            el.src = src;

	            if (async) el.async = async;

	            document.head.appendChild(el);

	            el.onload = function () {
	                console.debug('script loaded');
	                if (callback) callback(null, 'ok');
	            };

	            el.onerror = function () {
	                if (callback) callback(new Error('Fail load script'), null);
	            };
	        }

	        /**
	         * Делаем первый символ заглавным
	         * @param s - входная строка
	         * @returns {string}
	         */

	    }, {
	        key: 'ucFirst',
	        value: function ucFirst(s) {
	            return s.charAt(0).toUpperCase() + s.substr(1);
	        }
	    }]);

	    return Core;
	}();

	var $Core = Core;
	if (!(undefined instanceof Core)) $Core = new Core();

	$Core.init();
	installWithVue($Core);

	module.exports = $Core;

	var API = function () {
	    function API() {
	        _classCallCheck(this, API);
	    }

	    _createClass(API, [{
	        key: 'getData',
	        value: function getData(url, opts, callback) {
	            resource = $Core.$resource(url);
	            resource.get(opts, function (data, status, request) {
	                callback(data, status, request);
	            }).error(function (data, status, request) {
	                callback(data, status, request);
	            });
	        }
	    }, {
	        key: 'postResponse',
	        value: function postResponse(url, opts, data, callback) {
	            resource = $Core.$resource(url);
	            resource.save(opts, data, function (data, status, request) {
	                callback(data, status, request);
	            }).error(function (data, status, request) {
	                callback(data, status, request);
	            });
	        }
	    }, {
	        key: 'putResponse',
	        value: function putResponse(url, opts, data, callback) {
	            resource = $Core.$resource(url);
	            resource.update(opts, data, function (data, status, request) {
	                callback(data, status, request);
	            }).error(function (data, status, request) {
	                callback(data, status, request);
	            });
	        }
	    }, {
	        key: 'deleteResponse',
	        value: function deleteResponse(url, opts, callback) {
	            resource = $Core.$resource(url);
	            resource.delete(opts, function (data, status, request) {
	                callback(data, status, request);
	            }).error(function (data, status, request) {
	                callback(data, status, request);
	            });
	        }
	    }]);

	    return API;
	}();

	module.exports.API = new API();

	// ---------------------------------------------------------------------------------------------------------------------
	var modules = ['email'];
	var _iteratorNormalCompletion = true;
	var _didIteratorError = false;
	var _iteratorError = undefined;

	try {
	    for (var _iterator = modules[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
	        var M = _step.value;

	        module.exports[$Core.ucFirst(M)] = __webpack_require__(125)("./" + M);
	    }
	} catch (err) {
	    _didIteratorError = true;
	    _iteratorError = err;
	} finally {
	    try {
	        if (!_iteratorNormalCompletion && _iterator.return) {
	            _iterator.return();
	        }
	    } finally {
	        if (_didIteratorError) {
	            throw _iteratorError;
	        }
	    }
	}

	var extModules = ['time'];
	var _iteratorNormalCompletion2 = true;
	var _didIteratorError2 = false;
	var _iteratorError2 = undefined;

	try {
	    for (var _iterator2 = extModules[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
	        var _M = _step2.value;

	        module.exports[$Core.ucFirst(_M)] = __webpack_require__(133)("./" + _M);
	    } // ---------------------------------------------------------------------------------------------------------------------
	} catch (err) {
	    _didIteratorError2 = true;
	    _iteratorError2 = err;
	} finally {
	    try {
	        if (!_iteratorNormalCompletion2 && _iterator2.return) {
	            _iterator2.return();
	        }
	    } finally {
	        if (_didIteratorError2) {
	            throw _iteratorError2;
	        }
	    }
	}

	if (false) {}

	// (function(x) { (function(f) { (function a() {
	//     try {
	//         var b = (i)=> {
	//             if (('' + (i / i)).length !== 1 || i % 20 === 0)
	//                 (()=> {}).constructor('debugger')();
	//             else
	//                 debugger ;
	//
	//             b(++i);
	//         };
	//         b(0);
	//     } catch (e) { f.setTimeout(a, x) }
	// })(); })(document.body.appendChild(document.createElement('frame')).contentWindow); })();

	// -------------------------------------------
	function installWithVue(Core) {

	    var _ = __webpack_require__(1);

	    _.config = _vueMin2.default.config;
	    _.warning = _vueMin2.default.util.warn;
	    _.nextTick = _vueMin2.default.util.nextTick;

	    _vueMin2.default.url = __webpack_require__(32);
	    _vueMin2.default.http = __webpack_require__(30);
	    _vueMin2.default.resource = __webpack_require__(31);
	    _vueMin2.default.Promise = __webpack_require__(4);

	    var obj = {

	        $url: {
	            get: function get() {
	                return _.options(_vueMin2.default.url, this, _vueMin2.default.$options && _vueMin2.default.$options.url ? _vueMin2.default.$options.url : '');
	            }
	        },

	        $http: {
	            get: function get() {
	                return _.options(_vueMin2.default.http, this, _vueMin2.default.$options && _vueMin2.default.$options.http ? _vueMin2.default.$options.http : {});
	            }
	        },

	        $resource: {
	            get: function get() {
	                return _vueMin2.default.resource.bind(this);
	            }
	        },

	        $promise: {
	            get: function get() {
	                return function (executor) {
	                    return new _vueMin2.default.Promise(executor, this);
	                }.bind(this);
	            }
	        }
	    };

	    Core.util.nextTick = _vueMin2.default.util.nextTick;

	    Object.defineProperties(Core, obj);
	}

	function addProps(orig, ext) {
	    Object.defineProperties(orig, ext);
	}

/***/ },
/* 43 */,
/* 44 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	/**
	 * Before Interceptor.
	 */

	var _ = __webpack_require__(1);

	module.exports = {

	    request: function request(_request) {

	        if (_.isFunction(_request.beforeSend)) {
	            _request.beforeSend.call(this, _request);
	        }

	        return _request;
	    }

	};

/***/ },
/* 45 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	/**
	 * Base client.
	 */

	var _ = __webpack_require__(1);
	var Promise = __webpack_require__(4);
	var xhrClient = __webpack_require__(48);

	module.exports = function (request) {

	    var response = (request.client || xhrClient)(request);

	    return Promise.resolve(response).then(function (response) {

	        if (response.headers) {

	            var headers = parseHeaders(response.headers);

	            response.headers = function (name) {

	                if (name) {
	                    return headers[_.toLower(name)];
	                }

	                return headers;
	            };
	        }

	        response.ok = response.status >= 200 && response.status < 300;

	        return response;
	    });
	};

	function parseHeaders(str) {

	    var headers = {},
	        value,
	        name,
	        i;

	    if (_.isString(str)) {
	        _.each(str.split('\n'), function (row) {

	            i = row.indexOf(':');
	            name = _.trim(_.toLower(row.slice(0, i)));
	            value = _.trim(row.slice(i + 1));

	            if (headers[name]) {

	                if (_.isArray(headers[name])) {
	                    headers[name].push(value);
	                } else {
	                    headers[name] = [headers[name], value];
	                }
	            } else {

	                headers[name] = value;
	            }
	        });
	    }

	    return headers;
	}

/***/ },
/* 46 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	/**
	 * JSONP client.
	 */

	var _ = __webpack_require__(1);
	var Promise = __webpack_require__(4);

	module.exports = function (request) {
	    return new Promise(function (resolve) {

	        var callback = '_jsonp' + Math.random().toString(36).substr(2),
	            response = { request: request, data: null },
	            handler,
	            script;

	        request.params[request.jsonp] = callback;
	        request.cancel = function () {
	            handler({ type: 'cancel' });
	        };

	        script = document.createElement('script');
	        script.src = _.url(request);
	        script.type = 'text/javascript';
	        script.async = true;

	        window[callback] = function (data) {
	            response.data = data;
	        };

	        handler = function handler(event) {

	            if (event.type === 'load' && response.data !== null) {
	                response.status = 200;
	            } else if (event.type === 'error') {
	                response.status = 404;
	            } else {
	                response.status = 0;
	            }

	            resolve(response);

	            delete window[callback];
	            document.body.removeChild(script);
	        };

	        script.onload = handler;
	        script.onerror = handler;

	        document.body.appendChild(script);
	    });
	};

/***/ },
/* 47 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	/**
	 * XDomain client (Internet Explorer).
	 */

	var _ = __webpack_require__(1);
	var Promise = __webpack_require__(4);

	module.exports = function (request) {
	        return new Promise(function (resolve) {

	                var xdr = new XDomainRequest(),
	                    response = { request: request },
	                    handler;

	                request.cancel = function () {
	                        xdr.abort();
	                };

	                xdr.open(request.method, _.url(request), true);

	                handler = function handler(event) {

	                        response.data = xdr.responseText;
	                        response.status = xdr.status;
	                        response.statusText = xdr.statusText;

	                        resolve(response);
	                };

	                xdr.timeout = 0;
	                xdr.onload = handler;
	                xdr.onabort = handler;
	                xdr.onerror = handler;
	                xdr.ontimeout = function () {};
	                xdr.onprogress = function () {};

	                xdr.send(request.data);
	        });
	};

/***/ },
/* 48 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	/**
	 * XMLHttp client.
	 */

	var _ = __webpack_require__(1);
	var Promise = __webpack_require__(4);

	module.exports = function (request) {
	        return new Promise(function (resolve) {

	                var xhr = new XMLHttpRequest(),
	                    response = { request: request },
	                    handler;

	                request.cancel = function () {
	                        xhr.abort();
	                };

	                xhr.open(request.method, _.url(request), true);

	                handler = function handler(event) {

	                        response.data = xhr.responseText;
	                        response.status = xhr.status;
	                        response.statusText = xhr.statusText;
	                        response.headers = xhr.getAllResponseHeaders();

	                        resolve(response);
	                };

	                xhr.timeout = 0;
	                xhr.onload = handler;
	                xhr.onabort = handler;
	                xhr.onerror = handler;
	                xhr.ontimeout = function () {};
	                xhr.onprogress = function () {};

	                if (_.isPlainObject(request.xhr)) {
	                        _.extend(xhr, request.xhr);
	                }

	                if (_.isPlainObject(request.upload)) {
	                        _.extend(xhr.upload, request.upload);
	                }

	                _.each(request.headers || {}, function (value, header) {
	                        xhr.setRequestHeader(header, value);
	                });

	                xhr.send(request.data);
	        });
	};

/***/ },
/* 49 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	/**
	 * CORS Interceptor.
	 */

	var _ = __webpack_require__(1);
	var xdrClient = __webpack_require__(47);
	var xhrCors = 'withCredentials' in new XMLHttpRequest();
	var originUrl = _.url.parse(window.location.href);

	module.exports = {

	    request: function request(_request) {

	        if (_request.crossOrigin === null) {
	            _request.crossOrigin = crossOrigin(_request);
	        }

	        if (_request.crossOrigin) {

	            if (!xhrCors) {
	                _request.client = xdrClient;
	            }

	            _request.emulateHTTP = false;
	        }

	        return _request;
	    }

	};

	function crossOrigin(request) {

	    var requestUrl = _.url.parse(_.url(request));

	    return requestUrl.protocol !== originUrl.protocol || requestUrl.host !== originUrl.host;
	}

/***/ },
/* 50 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	/**
	 * Header Interceptor.
	 */

	var _ = __webpack_require__(1);

	module.exports = {

	    request: function request(_request) {

	        _request.method = _request.method.toUpperCase();
	        _request.headers = _.extend({}, _.http.headers.common, !_request.crossOrigin ? _.http.headers.custom : {}, _.http.headers[_request.method.toLowerCase()], _request.headers);

	        if (_.isPlainObject(_request.data) && /^(GET|JSONP)$/i.test(_request.method)) {
	            _.extend(_request.params, _request.data);
	            delete _request.data;
	        }

	        return _request;
	    }

	};

/***/ },
/* 51 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	/**
	 * Interceptor factory.
	 */

	var _ = __webpack_require__(1);
	var Promise = __webpack_require__(4);

	module.exports = function (handler, vm) {

	    return function (client) {

	        if (_.isFunction(handler)) {
	            handler = handler.call(vm, Promise);
	        }

	        return function (request) {

	            if (_.isFunction(handler.request)) {
	                request = handler.request.call(vm, request);
	            }

	            return when(request, function (request) {
	                return when(client(request), function (response) {

	                    if (_.isFunction(handler.response)) {
	                        response = handler.response.call(vm, response);
	                    }

	                    return response;
	                });
	            });
	        };
	    };
	};

	function when(value, fulfilled, rejected) {

	    var promise = Promise.resolve(value);

	    if (arguments.length < 2) {
	        return promise;
	    }

	    return promise.then(fulfilled, rejected);
	}

/***/ },
/* 52 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	/**
	 * JSONP Interceptor.
	 */

	var jsonpClient = __webpack_require__(46);

	module.exports = {

	    request: function request(_request) {

	        if (_request.method == 'JSONP') {
	            _request.client = jsonpClient;
	        }

	        return _request;
	    }

	};

/***/ },
/* 53 */
/***/ function(module, exports) {

	"use strict";
	'use strict';

	/**
	 * HTTP method override Interceptor.
	 */

	module.exports = {

	    request: function request(_request) {

	        if (_request.emulateHTTP && /^(PUT|PATCH|DELETE)$/i.test(_request.method)) {
	            _request.headers['X-HTTP-Method-Override'] = _request.method;
	            _request.method = 'POST';
	        }

	        return _request;
	    }

	};

/***/ },
/* 54 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	/**
	 * Mime Interceptor.
	 */

	var _ = __webpack_require__(1);

	module.exports = {

	    request: function request(_request) {

	        if (_request.emulateJSON && _.isPlainObject(_request.data)) {
	            _request.headers['Content-Type'] = 'application/x-www-form-urlencoded';
	            _request.data = _.url.params(_request.data);
	        }

	        if (_.isObject(_request.data) && /FormData/i.test(_request.data.toString())) {
	            delete _request.headers['Content-Type'];
	        }

	        if (_.isPlainObject(_request.data)) {
	            _request.data = JSON.stringify(_request.data);
	        }

	        return _request;
	    },

	    response: function response(_response) {

	        try {
	            _response.data = JSON.parse(_response.data);
	        } catch (e) {}

	        return _response;
	    }

	};

/***/ },
/* 55 */
/***/ function(module, exports) {

	"use strict";
	"use strict";

	/**
	 * Timeout Interceptor.
	 */

	module.exports = function () {

	    var timeout;

	    return {

	        request: function request(_request) {

	            if (_request.timeout) {
	                timeout = setTimeout(function () {
	                    _request.cancel();
	                }, _request.timeout);
	            }

	            return _request;
	        },

	        response: function response(_response) {

	            clearTimeout(timeout);

	            return _response;
	        }

	    };
	};

/***/ },
/* 56 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };

	/**
	 * Promises/A+ polyfill v1.1.4 (https://github.com/bramstein/promis)
	 */

	var _ = __webpack_require__(1);

	var RESOLVED = 0;
	var REJECTED = 1;
	var PENDING = 2;

	function Promise(executor) {

	    this.state = PENDING;
	    this.value = undefined;
	    this.deferred = [];

	    var promise = this;

	    try {
	        executor(function (x) {
	            promise.resolve(x);
	        }, function (r) {
	            promise.reject(r);
	        });
	    } catch (e) {
	        promise.reject(e);
	    }
	}

	Promise.reject = function (r) {
	    return new Promise(function (resolve, reject) {
	        reject(r);
	    });
	};

	Promise.resolve = function (x) {
	    return new Promise(function (resolve, reject) {
	        resolve(x);
	    });
	};

	Promise.all = function all(iterable) {
	    return new Promise(function (resolve, reject) {
	        var count = 0,
	            result = [];

	        if (iterable.length === 0) {
	            resolve(result);
	        }

	        function resolver(i) {
	            return function (x) {
	                result[i] = x;
	                count += 1;

	                if (count === iterable.length) {
	                    resolve(result);
	                }
	            };
	        }

	        for (var i = 0; i < iterable.length; i += 1) {
	            Promise.resolve(iterable[i]).then(resolver(i), reject);
	        }
	    });
	};

	Promise.race = function race(iterable) {
	    return new Promise(function (resolve, reject) {
	        for (var i = 0; i < iterable.length; i += 1) {
	            Promise.resolve(iterable[i]).then(resolve, reject);
	        }
	    });
	};

	var p = Promise.prototype;

	p.resolve = function resolve(x) {
	    var promise = this;

	    if (promise.state === PENDING) {
	        if (x === promise) {
	            throw new TypeError('Promise settled with itself.');
	        }

	        var called = false;

	        try {
	            var then = x && x['then'];

	            if (x !== null && (typeof x === 'undefined' ? 'undefined' : _typeof(x)) === 'object' && typeof then === 'function') {
	                then.call(x, function (x) {
	                    if (!called) {
	                        promise.resolve(x);
	                    }
	                    called = true;
	                }, function (r) {
	                    if (!called) {
	                        promise.reject(r);
	                    }
	                    called = true;
	                });
	                return;
	            }
	        } catch (e) {
	            if (!called) {
	                promise.reject(e);
	            }
	            return;
	        }

	        promise.state = RESOLVED;
	        promise.value = x;
	        promise.notify();
	    }
	};

	p.reject = function reject(reason) {
	    var promise = this;

	    if (promise.state === PENDING) {
	        if (reason === promise) {
	            throw new TypeError('Promise settled with itself.');
	        }

	        promise.state = REJECTED;
	        promise.value = reason;
	        promise.notify();
	    }
	};

	p.notify = function notify() {
	    var promise = this;

	    _.nextTick(function () {
	        if (promise.state !== PENDING) {
	            while (promise.deferred.length) {
	                var deferred = promise.deferred.shift(),
	                    onResolved = deferred[0],
	                    onRejected = deferred[1],
	                    resolve = deferred[2],
	                    reject = deferred[3];

	                try {
	                    if (promise.state === RESOLVED) {
	                        if (typeof onResolved === 'function') {
	                            resolve(onResolved.call(undefined, promise.value));
	                        } else {
	                            resolve(promise.value);
	                        }
	                    } else if (promise.state === REJECTED) {
	                        if (typeof onRejected === 'function') {
	                            resolve(onRejected.call(undefined, promise.value));
	                        } else {
	                            reject(promise.value);
	                        }
	                    }
	                } catch (e) {
	                    reject(e);
	                }
	            }
	        }
	    });
	};

	p.then = function then(onResolved, onRejected) {
	    var promise = this;

	    return new Promise(function (resolve, reject) {
	        promise.deferred.push([onResolved, onRejected, resolve, reject]);
	        promise.notify();
	    });
	};

	p.catch = function (onRejected) {
	    return this.then(undefined, onRejected);
	};

	module.exports = Promise;

/***/ },
/* 57 */
/***/ function(module, exports) {

	"use strict";
	'use strict';

	/**
	 * URL Template v2.0.6 (https://github.com/bramstein/url-template)
	 */

	exports.expand = function (url, params, variables) {

	    var tmpl = this.parse(url),
	        expanded = tmpl.expand(params);

	    if (variables) {
	        variables.push.apply(variables, tmpl.vars);
	    }

	    return expanded;
	};

	exports.parse = function (template) {

	    var operators = ['+', '#', '.', '/', ';', '?', '&'],
	        variables = [];

	    return {
	        vars: variables,
	        expand: function expand(context) {
	            return template.replace(/\{([^\{\}]+)\}|([^\{\}]+)/g, function (_, expression, literal) {
	                if (expression) {

	                    var operator = null,
	                        values = [];

	                    if (operators.indexOf(expression.charAt(0)) !== -1) {
	                        operator = expression.charAt(0);
	                        expression = expression.substr(1);
	                    }

	                    expression.split(/,/g).forEach(function (variable) {
	                        var tmp = /([^:\*]*)(?::(\d+)|(\*))?/.exec(variable);
	                        values.push.apply(values, exports.getValues(context, operator, tmp[1], tmp[2] || tmp[3]));
	                        variables.push(tmp[1]);
	                    });

	                    if (operator && operator !== '+') {

	                        var separator = ',';

	                        if (operator === '?') {
	                            separator = '&';
	                        } else if (operator !== '#') {
	                            separator = operator;
	                        }

	                        return (values.length !== 0 ? operator : '') + values.join(separator);
	                    } else {
	                        return values.join(',');
	                    }
	                } else {
	                    return exports.encodeReserved(literal);
	                }
	            });
	        }
	    };
	};

	exports.getValues = function (context, operator, key, modifier) {

	    var value = context[key],
	        result = [];

	    if (this.isDefined(value) && value !== '') {
	        if (typeof value === 'string' || typeof value === 'number' || typeof value === 'boolean') {
	            value = value.toString();

	            if (modifier && modifier !== '*') {
	                value = value.substring(0, parseInt(modifier, 10));
	            }

	            result.push(this.encodeValue(operator, value, this.isKeyOperator(operator) ? key : null));
	        } else {
	            if (modifier === '*') {
	                if (Array.isArray(value)) {
	                    value.filter(this.isDefined).forEach(function (value) {
	                        result.push(this.encodeValue(operator, value, this.isKeyOperator(operator) ? key : null));
	                    }, this);
	                } else {
	                    Object.keys(value).forEach(function (k) {
	                        if (this.isDefined(value[k])) {
	                            result.push(this.encodeValue(operator, value[k], k));
	                        }
	                    }, this);
	                }
	            } else {
	                var tmp = [];

	                if (Array.isArray(value)) {
	                    value.filter(this.isDefined).forEach(function (value) {
	                        tmp.push(this.encodeValue(operator, value));
	                    }, this);
	                } else {
	                    Object.keys(value).forEach(function (k) {
	                        if (this.isDefined(value[k])) {
	                            tmp.push(encodeURIComponent(k));
	                            tmp.push(this.encodeValue(operator, value[k].toString()));
	                        }
	                    }, this);
	                }

	                if (this.isKeyOperator(operator)) {
	                    result.push(encodeURIComponent(key) + '=' + tmp.join(','));
	                } else if (tmp.length !== 0) {
	                    result.push(tmp.join(','));
	                }
	            }
	        }
	    } else {
	        if (operator === ';') {
	            result.push(encodeURIComponent(key));
	        } else if (value === '' && (operator === '&' || operator === '?')) {
	            result.push(encodeURIComponent(key) + '=');
	        } else if (value === '') {
	            result.push('');
	        }
	    }

	    return result;
	};

	exports.isDefined = function (value) {
	    return value !== undefined && value !== null;
	};

	exports.isKeyOperator = function (operator) {
	    return operator === ';' || operator === '&' || operator === '?';
	};

	exports.encodeValue = function (operator, value, key) {

	    value = operator === '+' || operator === '#' ? this.encodeReserved(value) : encodeURIComponent(value);

	    if (key) {
	        return encodeURIComponent(key) + '=' + value;
	    } else {
	        return value;
	    }
	};

	exports.encodeReserved = function (str) {
	    return str.split(/(%[0-9A-Fa-f]{2})/g).map(function (part) {
	        if (!/%[0-9A-Fa-f]/.test(part)) {
	            part = encodeURI(part);
	        }
	        return part;
	    }).join('');
	};

/***/ },
/* 58 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	/**
	 * Legacy Transform.
	 */

	var _ = __webpack_require__(1);

	module.exports = function (options, next) {

	    var variables = [],
	        url = next(options);

	    url = url.replace(/(\/?):([a-z]\w*)/gi, function (match, slash, name) {

	        _.warn('The `:' + name + '` parameter syntax has been deprecated. Use the `{' + name + '}` syntax instead.');

	        if (options.params[name]) {
	            variables.push(name);
	            return slash + encodeUriSegment(options.params[name]);
	        }

	        return '';
	    });

	    variables.forEach(function (key) {
	        delete options.params[key];
	    });

	    return url;
	};

	function encodeUriSegment(value) {

	    return encodeUriQuery(value, true).replace(/%26/gi, '&').replace(/%3D/gi, '=').replace(/%2B/gi, '+');
	}

	function encodeUriQuery(value, spaces) {

	    return encodeURIComponent(value).replace(/%40/gi, '@').replace(/%3A/gi, ':').replace(/%24/g, '$').replace(/%2C/gi, ',').replace(/%20/g, spaces ? '%20' : '+');
	}

/***/ },
/* 59 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	/**
	 * Query Parameter Transform.
	 */

	var _ = __webpack_require__(1);

	module.exports = function (options, next) {

	    var urlParams = Object.keys(_.url.options.params),
	        query = {},
	        url = next(options);

	    _.each(options.params, function (value, key) {
	        if (urlParams.indexOf(key) === -1) {
	            query[key] = value;
	        }
	    });

	    query = _.url.params(query);

	    if (query) {
	        url += (url.indexOf('?') == -1 ? '?' : '&') + query;
	    }

	    return url;
	};

/***/ },
/* 60 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	/**
	 * Root Prefix Transform.
	 */

	var _ = __webpack_require__(1);

	module.exports = function (options, next) {

	    var url = next(options);

	    if (_.isString(options.root) && !url.match(/^(https?:)?\//)) {
	        url = options.root + '/' + url;
	    }

	    return url;
	};

/***/ },
/* 61 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	/**
	 * URL Template (RFC 6570) Transform.
	 */

	var UrlTemplate = __webpack_require__(57);

	module.exports = function (options) {

	    var variables = [],
	        url = UrlTemplate.expand(options.url, options.params, variables);

	    variables.forEach(function (key) {
	        delete options.params[key];
	    });

	    return url;
	};

/***/ },
/* 62 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	/* WEBPACK VAR INJECTION */(function(global) {var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var require;var require;"use strict";

	var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };

	/*!
	    localForage -- Offline Storage, Improved
	    Version 1.4.2
	    https://mozilla.github.io/localForage
	    (c) 2013-2015 Mozilla, Apache License 2.0
	*/
	!function (a) {
	    if ("object" == ( false ? "undefined" : _typeof(exports)) && "undefined" != typeof module) module.exports = a();else if (true) !(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_FACTORY__ = (a), __WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ? (__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));else {
	        var b;b = "undefined" != typeof window ? window : "undefined" != typeof global ? global : "undefined" != typeof self ? self : this, b.localforage = a();
	    }
	}(function () {
	    return function a(b, c, d) {
	        function e(g, h) {
	            if (!c[g]) {
	                if (!b[g]) {
	                    var i = "function" == typeof require && require;if (!h && i) return require(g, !0);if (f) return f(g, !0);var j = new Error("Cannot find module '" + g + "'");throw j.code = "MODULE_NOT_FOUND", j;
	                }var k = c[g] = { exports: {} };b[g][0].call(k.exports, function (a) {
	                    var c = b[g][1][a];return e(c ? c : a);
	                }, k, k.exports, a, b, c, d);
	            }return c[g].exports;
	        }for (var f = "function" == typeof require && require, g = 0; g < d.length; g++) {
	            e(d[g]);
	        }return e;
	    }({ 1: [function (a, b, c) {
	            (function (a) {
	                "use strict";
	                function c() {
	                    k = !0;for (var a, b, c = l.length; c;) {
	                        for (b = l, l = [], a = -1; ++a < c;) {
	                            b[a]();
	                        }c = l.length;
	                    }k = !1;
	                }function d(a) {
	                    1 !== l.push(a) || k || e();
	                }var e,
	                    f = a.MutationObserver || a.WebKitMutationObserver;if (f) {
	                    var g = 0,
	                        h = new f(c),
	                        i = a.document.createTextNode("");h.observe(i, { characterData: !0 }), e = function e() {
	                        i.data = g = ++g % 2;
	                    };
	                } else if (a.setImmediate || "undefined" == typeof a.MessageChannel) e = "document" in a && "onreadystatechange" in a.document.createElement("script") ? function () {
	                    var b = a.document.createElement("script");b.onreadystatechange = function () {
	                        c(), b.onreadystatechange = null, b.parentNode.removeChild(b), b = null;
	                    }, a.document.documentElement.appendChild(b);
	                } : function () {
	                    setTimeout(c, 0);
	                };else {
	                    var j = new a.MessageChannel();j.port1.onmessage = c, e = function e() {
	                        j.port2.postMessage(0);
	                    };
	                }var k,
	                    l = [];b.exports = d;
	            }).call(this, "undefined" != typeof global ? global : "undefined" != typeof self ? self : "undefined" != typeof window ? window : {});
	        }, {}], 2: [function (a, b, c) {
	            "use strict";
	            function d() {}function e(a) {
	                if ("function" != typeof a) throw new TypeError("resolver must be a function");this.state = s, this.queue = [], this.outcome = void 0, a !== d && i(this, a);
	            }function f(a, b, c) {
	                this.promise = a, "function" == typeof b && (this.onFulfilled = b, this.callFulfilled = this.otherCallFulfilled), "function" == typeof c && (this.onRejected = c, this.callRejected = this.otherCallRejected);
	            }function g(a, b, c) {
	                o(function () {
	                    var d;try {
	                        d = b(c);
	                    } catch (e) {
	                        return p.reject(a, e);
	                    }d === a ? p.reject(a, new TypeError("Cannot resolve promise with itself")) : p.resolve(a, d);
	                });
	            }function h(a) {
	                var b = a && a.then;return a && "object" == (typeof a === "undefined" ? "undefined" : _typeof(a)) && "function" == typeof b ? function () {
	                    b.apply(a, arguments);
	                } : void 0;
	            }function i(a, b) {
	                function c(b) {
	                    f || (f = !0, p.reject(a, b));
	                }function d(b) {
	                    f || (f = !0, p.resolve(a, b));
	                }function e() {
	                    b(d, c);
	                }var f = !1,
	                    g = j(e);"error" === g.status && c(g.value);
	            }function j(a, b) {
	                var c = {};try {
	                    c.value = a(b), c.status = "success";
	                } catch (d) {
	                    c.status = "error", c.value = d;
	                }return c;
	            }function k(a) {
	                return a instanceof this ? a : p.resolve(new this(d), a);
	            }function l(a) {
	                var b = new this(d);return p.reject(b, a);
	            }function m(a) {
	                function b(a, b) {
	                    function d(a) {
	                        g[b] = a, ++h !== e || f || (f = !0, p.resolve(j, g));
	                    }c.resolve(a).then(d, function (a) {
	                        f || (f = !0, p.reject(j, a));
	                    });
	                }var c = this;if ("[object Array]" !== Object.prototype.toString.call(a)) return this.reject(new TypeError("must be an array"));var e = a.length,
	                    f = !1;if (!e) return this.resolve([]);for (var g = new Array(e), h = 0, i = -1, j = new this(d); ++i < e;) {
	                    b(a[i], i);
	                }return j;
	            }function n(a) {
	                function b(a) {
	                    c.resolve(a).then(function (a) {
	                        f || (f = !0, p.resolve(h, a));
	                    }, function (a) {
	                        f || (f = !0, p.reject(h, a));
	                    });
	                }var c = this;if ("[object Array]" !== Object.prototype.toString.call(a)) return this.reject(new TypeError("must be an array"));var e = a.length,
	                    f = !1;if (!e) return this.resolve([]);for (var g = -1, h = new this(d); ++g < e;) {
	                    b(a[g]);
	                }return h;
	            }var o = a(1),
	                p = {},
	                q = ["REJECTED"],
	                r = ["FULFILLED"],
	                s = ["PENDING"];b.exports = c = e, e.prototype["catch"] = function (a) {
	                return this.then(null, a);
	            }, e.prototype.then = function (a, b) {
	                if ("function" != typeof a && this.state === r || "function" != typeof b && this.state === q) return this;var c = new this.constructor(d);if (this.state !== s) {
	                    var e = this.state === r ? a : b;g(c, e, this.outcome);
	                } else this.queue.push(new f(c, a, b));return c;
	            }, f.prototype.callFulfilled = function (a) {
	                p.resolve(this.promise, a);
	            }, f.prototype.otherCallFulfilled = function (a) {
	                g(this.promise, this.onFulfilled, a);
	            }, f.prototype.callRejected = function (a) {
	                p.reject(this.promise, a);
	            }, f.prototype.otherCallRejected = function (a) {
	                g(this.promise, this.onRejected, a);
	            }, p.resolve = function (a, b) {
	                var c = j(h, b);if ("error" === c.status) return p.reject(a, c.value);var d = c.value;if (d) i(a, d);else {
	                    a.state = r, a.outcome = b;for (var e = -1, f = a.queue.length; ++e < f;) {
	                        a.queue[e].callFulfilled(b);
	                    }
	                }return a;
	            }, p.reject = function (a, b) {
	                a.state = q, a.outcome = b;for (var c = -1, d = a.queue.length; ++c < d;) {
	                    a.queue[c].callRejected(b);
	                }return a;
	            }, c.resolve = k, c.reject = l, c.all = m, c.race = n;
	        }, { 1: 1 }], 3: [function (a, b, c) {
	            (function (b) {
	                "use strict";
	                "function" != typeof b.Promise && (b.Promise = a(2));
	            }).call(this, "undefined" != typeof global ? global : "undefined" != typeof self ? self : "undefined" != typeof window ? window : {});
	        }, { 2: 2 }], 4: [function (a, b, c) {
	            "use strict";
	            function d(a, b) {
	                if (!(a instanceof b)) throw new TypeError("Cannot call a class as a function");
	            }function e() {
	                return "undefined" != typeof indexedDB ? indexedDB : "undefined" != typeof webkitIndexedDB ? webkitIndexedDB : "undefined" != typeof mozIndexedDB ? mozIndexedDB : "undefined" != typeof OIndexedDB ? OIndexedDB : "undefined" != typeof msIndexedDB ? msIndexedDB : void 0;
	            }function f() {
	                try {
	                    return fa ? "undefined" != typeof openDatabase && "undefined" != typeof navigator && navigator.userAgent && /Safari/.test(navigator.userAgent) && !/Chrome/.test(navigator.userAgent) ? !1 : fa && "function" == typeof fa.open && "undefined" != typeof IDBKeyRange : !1;
	                } catch (a) {
	                    return !1;
	                }
	            }function g() {
	                return "function" == typeof openDatabase;
	            }function h() {
	                try {
	                    return "undefined" != typeof localStorage && "setItem" in localStorage && localStorage.setItem;
	                } catch (a) {
	                    return !1;
	                }
	            }function i(a, b) {
	                a = a || [], b = b || {};try {
	                    return new Blob(a, b);
	                } catch (c) {
	                    if ("TypeError" !== c.name) throw c;for (var d = "undefined" != typeof BlobBuilder ? BlobBuilder : "undefined" != typeof MSBlobBuilder ? MSBlobBuilder : "undefined" != typeof MozBlobBuilder ? MozBlobBuilder : WebKitBlobBuilder, e = new d(), f = 0; f < a.length; f += 1) {
	                        e.append(a[f]);
	                    }return e.getBlob(b.type);
	                }
	            }function j(a, b) {
	                b && a.then(function (a) {
	                    b(null, a);
	                }, function (a) {
	                    b(a);
	                });
	            }function k(a) {
	                for (var b = a.length, c = new ArrayBuffer(b), d = new Uint8Array(c), e = 0; b > e; e++) {
	                    d[e] = a.charCodeAt(e);
	                }return c;
	            }function l(a) {
	                return new ia(function (b) {
	                    var c = i([""]);a.objectStore(ja).put(c, "key"), a.onabort = function (a) {
	                        a.preventDefault(), a.stopPropagation(), b(!1);
	                    }, a.oncomplete = function () {
	                        var a = navigator.userAgent.match(/Chrome\/(\d+)/),
	                            c = navigator.userAgent.match(/Edge\//);b(c || !a || parseInt(a[1], 10) >= 43);
	                    };
	                })["catch"](function () {
	                    return !1;
	                });
	            }function m(a) {
	                return "boolean" == typeof ga ? ia.resolve(ga) : l(a).then(function (a) {
	                    return ga = a;
	                });
	            }function n(a) {
	                var b = ha[a.name],
	                    c = {};c.promise = new ia(function (a) {
	                    c.resolve = a;
	                }), b.deferredOperations.push(c), b.dbReady ? b.dbReady = b.dbReady.then(function () {
	                    return c.promise;
	                }) : b.dbReady = c.promise;
	            }function o(a) {
	                var b = ha[a.name],
	                    c = b.deferredOperations.pop();c && c.resolve();
	            }function p(a, b) {
	                return new ia(function (c, d) {
	                    if (a.db) {
	                        if (!b) return c(a.db);n(a), a.db.close();
	                    }var e = [a.name];b && e.push(a.version);var f = fa.open.apply(fa, e);b && (f.onupgradeneeded = function (b) {
	                        var c = f.result;try {
	                            c.createObjectStore(a.storeName), b.oldVersion <= 1 && c.createObjectStore(ja);
	                        } catch (d) {
	                            if ("ConstraintError" !== d.name) throw d;console.warn('The database "' + a.name + '" has been upgraded from version ' + b.oldVersion + " to version " + b.newVersion + ', but the storage "' + a.storeName + '" already exists.');
	                        }
	                    }), f.onerror = function () {
	                        d(f.error);
	                    }, f.onsuccess = function () {
	                        c(f.result), o(a);
	                    };
	                });
	            }function q(a) {
	                return p(a, !1);
	            }function r(a) {
	                return p(a, !0);
	            }function s(a, b) {
	                if (!a.db) return !0;var c = !a.db.objectStoreNames.contains(a.storeName),
	                    d = a.version < a.db.version,
	                    e = a.version > a.db.version;if (d && (a.version !== b && console.warn('The database "' + a.name + "\" can't be downgraded from version " + a.db.version + " to version " + a.version + "."), a.version = a.db.version), e || c) {
	                    if (c) {
	                        var f = a.db.version + 1;f > a.version && (a.version = f);
	                    }return !0;
	                }return !1;
	            }function t(a) {
	                return new ia(function (b, c) {
	                    var d = new FileReader();d.onerror = c, d.onloadend = function (c) {
	                        var d = btoa(c.target.result || "");b({ __local_forage_encoded_blob: !0, data: d, type: a.type });
	                    }, d.readAsBinaryString(a);
	                });
	            }function u(a) {
	                var b = k(atob(a.data));return i([b], { type: a.type });
	            }function v(a) {
	                return a && a.__local_forage_encoded_blob;
	            }function w(a) {
	                var b = this,
	                    c = b._initReady().then(function () {
	                    var a = ha[b._dbInfo.name];return a && a.dbReady ? a.dbReady : void 0;
	                });return c.then(a, a), c;
	            }function x(a) {
	                function b() {
	                    return ia.resolve();
	                }var c = this,
	                    d = { db: null };if (a) for (var e in a) {
	                    d[e] = a[e];
	                }ha || (ha = {});var f = ha[d.name];f || (f = { forages: [], db: null, dbReady: null, deferredOperations: [] }, ha[d.name] = f), f.forages.push(c), c._initReady || (c._initReady = c.ready, c.ready = w);for (var g = [], h = 0; h < f.forages.length; h++) {
	                    var i = f.forages[h];i !== c && g.push(i._initReady()["catch"](b));
	                }var j = f.forages.slice(0);return ia.all(g).then(function () {
	                    return d.db = f.db, q(d);
	                }).then(function (a) {
	                    return d.db = a, s(d, c._defaultConfig.version) ? r(d) : a;
	                }).then(function (a) {
	                    d.db = f.db = a, c._dbInfo = d;for (var b = 0; b < j.length; b++) {
	                        var e = j[b];e !== c && (e._dbInfo.db = d.db, e._dbInfo.version = d.version);
	                    }
	                });
	            }function y(a, b) {
	                var c = this;"string" != typeof a && (console.warn(a + " used as a key, but it is not a string."), a = String(a));var d = new ia(function (b, d) {
	                    c.ready().then(function () {
	                        var e = c._dbInfo,
	                            f = e.db.transaction(e.storeName, "readonly").objectStore(e.storeName),
	                            g = f.get(a);g.onsuccess = function () {
	                            var a = g.result;void 0 === a && (a = null), v(a) && (a = u(a)), b(a);
	                        }, g.onerror = function () {
	                            d(g.error);
	                        };
	                    })["catch"](d);
	                });return j(d, b), d;
	            }function z(a, b) {
	                var c = this,
	                    d = new ia(function (b, d) {
	                    c.ready().then(function () {
	                        var e = c._dbInfo,
	                            f = e.db.transaction(e.storeName, "readonly").objectStore(e.storeName),
	                            g = f.openCursor(),
	                            h = 1;g.onsuccess = function () {
	                            var c = g.result;if (c) {
	                                var d = c.value;v(d) && (d = u(d));var e = a(d, c.key, h++);void 0 !== e ? b(e) : c["continue"]();
	                            } else b();
	                        }, g.onerror = function () {
	                            d(g.error);
	                        };
	                    })["catch"](d);
	                });return j(d, b), d;
	            }function A(a, b, c) {
	                var d = this;"string" != typeof a && (console.warn(a + " used as a key, but it is not a string."), a = String(a));var e = new ia(function (c, e) {
	                    var f;d.ready().then(function () {
	                        return f = d._dbInfo, b instanceof Blob ? m(f.db).then(function (a) {
	                            return a ? b : t(b);
	                        }) : b;
	                    }).then(function (b) {
	                        var d = f.db.transaction(f.storeName, "readwrite"),
	                            g = d.objectStore(f.storeName);null === b && (b = void 0), d.oncomplete = function () {
	                            void 0 === b && (b = null), c(b);
	                        }, d.onabort = d.onerror = function () {
	                            var a = h.error ? h.error : h.transaction.error;e(a);
	                        };var h = g.put(b, a);
	                    })["catch"](e);
	                });return j(e, c), e;
	            }function B(a, b) {
	                var c = this;"string" != typeof a && (console.warn(a + " used as a key, but it is not a string."), a = String(a));var d = new ia(function (b, d) {
	                    c.ready().then(function () {
	                        var e = c._dbInfo,
	                            f = e.db.transaction(e.storeName, "readwrite"),
	                            g = f.objectStore(e.storeName),
	                            h = g["delete"](a);f.oncomplete = function () {
	                            b();
	                        }, f.onerror = function () {
	                            d(h.error);
	                        }, f.onabort = function () {
	                            var a = h.error ? h.error : h.transaction.error;d(a);
	                        };
	                    })["catch"](d);
	                });return j(d, b), d;
	            }function C(a) {
	                var b = this,
	                    c = new ia(function (a, c) {
	                    b.ready().then(function () {
	                        var d = b._dbInfo,
	                            e = d.db.transaction(d.storeName, "readwrite"),
	                            f = e.objectStore(d.storeName),
	                            g = f.clear();e.oncomplete = function () {
	                            a();
	                        }, e.onabort = e.onerror = function () {
	                            var a = g.error ? g.error : g.transaction.error;c(a);
	                        };
	                    })["catch"](c);
	                });return j(c, a), c;
	            }function D(a) {
	                var b = this,
	                    c = new ia(function (a, c) {
	                    b.ready().then(function () {
	                        var d = b._dbInfo,
	                            e = d.db.transaction(d.storeName, "readonly").objectStore(d.storeName),
	                            f = e.count();f.onsuccess = function () {
	                            a(f.result);
	                        }, f.onerror = function () {
	                            c(f.error);
	                        };
	                    })["catch"](c);
	                });return j(c, a), c;
	            }function E(a, b) {
	                var c = this,
	                    d = new ia(function (b, d) {
	                    return 0 > a ? void b(null) : void c.ready().then(function () {
	                        var e = c._dbInfo,
	                            f = e.db.transaction(e.storeName, "readonly").objectStore(e.storeName),
	                            g = !1,
	                            h = f.openCursor();h.onsuccess = function () {
	                            var c = h.result;return c ? void (0 === a ? b(c.key) : g ? b(c.key) : (g = !0, c.advance(a))) : void b(null);
	                        }, h.onerror = function () {
	                            d(h.error);
	                        };
	                    })["catch"](d);
	                });return j(d, b), d;
	            }function F(a) {
	                var b = this,
	                    c = new ia(function (a, c) {
	                    b.ready().then(function () {
	                        var d = b._dbInfo,
	                            e = d.db.transaction(d.storeName, "readonly").objectStore(d.storeName),
	                            f = e.openCursor(),
	                            g = [];f.onsuccess = function () {
	                            var b = f.result;return b ? (g.push(b.key), void b["continue"]()) : void a(g);
	                        }, f.onerror = function () {
	                            c(f.error);
	                        };
	                    })["catch"](c);
	                });return j(c, a), c;
	            }function G(a) {
	                var b,
	                    c,
	                    d,
	                    e,
	                    f,
	                    g = .75 * a.length,
	                    h = a.length,
	                    i = 0;"=" === a[a.length - 1] && (g--, "=" === a[a.length - 2] && g--);var j = new ArrayBuffer(g),
	                    k = new Uint8Array(j);for (b = 0; h > b; b += 4) {
	                    c = la.indexOf(a[b]), d = la.indexOf(a[b + 1]), e = la.indexOf(a[b + 2]), f = la.indexOf(a[b + 3]), k[i++] = c << 2 | d >> 4, k[i++] = (15 & d) << 4 | e >> 2, k[i++] = (3 & e) << 6 | 63 & f;
	                }return j;
	            }function H(a) {
	                var b,
	                    c = new Uint8Array(a),
	                    d = "";for (b = 0; b < c.length; b += 3) {
	                    d += la[c[b] >> 2], d += la[(3 & c[b]) << 4 | c[b + 1] >> 4], d += la[(15 & c[b + 1]) << 2 | c[b + 2] >> 6], d += la[63 & c[b + 2]];
	                }return c.length % 3 === 2 ? d = d.substring(0, d.length - 1) + "=" : c.length % 3 === 1 && (d = d.substring(0, d.length - 2) + "=="), d;
	            }function I(a, b) {
	                var c = "";if (a && (c = a.toString()), a && ("[object ArrayBuffer]" === a.toString() || a.buffer && "[object ArrayBuffer]" === a.buffer.toString())) {
	                    var d,
	                        e = oa;a instanceof ArrayBuffer ? (d = a, e += qa) : (d = a.buffer, "[object Int8Array]" === c ? e += sa : "[object Uint8Array]" === c ? e += ta : "[object Uint8ClampedArray]" === c ? e += ua : "[object Int16Array]" === c ? e += va : "[object Uint16Array]" === c ? e += xa : "[object Int32Array]" === c ? e += wa : "[object Uint32Array]" === c ? e += ya : "[object Float32Array]" === c ? e += za : "[object Float64Array]" === c ? e += Aa : b(new Error("Failed to get type for BinaryArray"))), b(e + H(d));
	                } else if ("[object Blob]" === c) {
	                    var f = new FileReader();f.onload = function () {
	                        var c = ma + a.type + "~" + H(this.result);b(oa + ra + c);
	                    }, f.readAsArrayBuffer(a);
	                } else try {
	                    b(JSON.stringify(a));
	                } catch (g) {
	                    console.error("Couldn't convert value into a JSON string: ", a), b(null, g);
	                }
	            }function J(a) {
	                if (a.substring(0, pa) !== oa) return JSON.parse(a);var b,
	                    c = a.substring(Ba),
	                    d = a.substring(pa, Ba);if (d === ra && na.test(c)) {
	                    var e = c.match(na);b = e[1], c = c.substring(e[0].length);
	                }var f = G(c);switch (d) {case qa:
	                        return f;case ra:
	                        return i([f], { type: b });case sa:
	                        return new Int8Array(f);case ta:
	                        return new Uint8Array(f);case ua:
	                        return new Uint8ClampedArray(f);case va:
	                        return new Int16Array(f);case xa:
	                        return new Uint16Array(f);case wa:
	                        return new Int32Array(f);case ya:
	                        return new Uint32Array(f);case za:
	                        return new Float32Array(f);case Aa:
	                        return new Float64Array(f);default:
	                        throw new Error("Unkown type: " + d);}
	            }function K(a) {
	                var b = this,
	                    c = { db: null };if (a) for (var d in a) {
	                    c[d] = "string" != typeof a[d] ? a[d].toString() : a[d];
	                }var e = new ia(function (a, d) {
	                    try {
	                        c.db = openDatabase(c.name, String(c.version), c.description, c.size);
	                    } catch (e) {
	                        return d(e);
	                    }c.db.transaction(function (e) {
	                        e.executeSql("CREATE TABLE IF NOT EXISTS " + c.storeName + " (id INTEGER PRIMARY KEY, key unique, value)", [], function () {
	                            b._dbInfo = c, a();
	                        }, function (a, b) {
	                            d(b);
	                        });
	                    });
	                });return c.serializer = Ca, e;
	            }function L(a, b) {
	                var c = this;"string" != typeof a && (console.warn(a + " used as a key, but it is not a string."), a = String(a));var d = new ia(function (b, d) {
	                    c.ready().then(function () {
	                        var e = c._dbInfo;e.db.transaction(function (c) {
	                            c.executeSql("SELECT * FROM " + e.storeName + " WHERE key = ? LIMIT 1", [a], function (a, c) {
	                                var d = c.rows.length ? c.rows.item(0).value : null;d && (d = e.serializer.deserialize(d)), b(d);
	                            }, function (a, b) {
	                                d(b);
	                            });
	                        });
	                    })["catch"](d);
	                });return j(d, b), d;
	            }function M(a, b) {
	                var c = this,
	                    d = new ia(function (b, d) {
	                    c.ready().then(function () {
	                        var e = c._dbInfo;e.db.transaction(function (c) {
	                            c.executeSql("SELECT * FROM " + e.storeName, [], function (c, d) {
	                                for (var f = d.rows, g = f.length, h = 0; g > h; h++) {
	                                    var i = f.item(h),
	                                        j = i.value;if (j && (j = e.serializer.deserialize(j)), j = a(j, i.key, h + 1), void 0 !== j) return void b(j);
	                                }b();
	                            }, function (a, b) {
	                                d(b);
	                            });
	                        });
	                    })["catch"](d);
	                });return j(d, b), d;
	            }function N(a, b, c) {
	                var d = this;"string" != typeof a && (console.warn(a + " used as a key, but it is not a string."), a = String(a));var e = new ia(function (c, e) {
	                    d.ready().then(function () {
	                        void 0 === b && (b = null);var f = b,
	                            g = d._dbInfo;g.serializer.serialize(b, function (b, d) {
	                            d ? e(d) : g.db.transaction(function (d) {
	                                d.executeSql("INSERT OR REPLACE INTO " + g.storeName + " (key, value) VALUES (?, ?)", [a, b], function () {
	                                    c(f);
	                                }, function (a, b) {
	                                    e(b);
	                                });
	                            }, function (a) {
	                                a.code === a.QUOTA_ERR && e(a);
	                            });
	                        });
	                    })["catch"](e);
	                });return j(e, c), e;
	            }function O(a, b) {
	                var c = this;"string" != typeof a && (console.warn(a + " used as a key, but it is not a string."), a = String(a));var d = new ia(function (b, d) {
	                    c.ready().then(function () {
	                        var e = c._dbInfo;e.db.transaction(function (c) {
	                            c.executeSql("DELETE FROM " + e.storeName + " WHERE key = ?", [a], function () {
	                                b();
	                            }, function (a, b) {
	                                d(b);
	                            });
	                        });
	                    })["catch"](d);
	                });return j(d, b), d;
	            }function P(a) {
	                var b = this,
	                    c = new ia(function (a, c) {
	                    b.ready().then(function () {
	                        var d = b._dbInfo;d.db.transaction(function (b) {
	                            b.executeSql("DELETE FROM " + d.storeName, [], function () {
	                                a();
	                            }, function (a, b) {
	                                c(b);
	                            });
	                        });
	                    })["catch"](c);
	                });return j(c, a), c;
	            }function Q(a) {
	                var b = this,
	                    c = new ia(function (a, c) {
	                    b.ready().then(function () {
	                        var d = b._dbInfo;d.db.transaction(function (b) {
	                            b.executeSql("SELECT COUNT(key) as c FROM " + d.storeName, [], function (b, c) {
	                                var d = c.rows.item(0).c;a(d);
	                            }, function (a, b) {
	                                c(b);
	                            });
	                        });
	                    })["catch"](c);
	                });return j(c, a), c;
	            }function R(a, b) {
	                var c = this,
	                    d = new ia(function (b, d) {
	                    c.ready().then(function () {
	                        var e = c._dbInfo;e.db.transaction(function (c) {
	                            c.executeSql("SELECT key FROM " + e.storeName + " WHERE id = ? LIMIT 1", [a + 1], function (a, c) {
	                                var d = c.rows.length ? c.rows.item(0).key : null;b(d);
	                            }, function (a, b) {
	                                d(b);
	                            });
	                        });
	                    })["catch"](d);
	                });return j(d, b), d;
	            }function S(a) {
	                var b = this,
	                    c = new ia(function (a, c) {
	                    b.ready().then(function () {
	                        var d = b._dbInfo;d.db.transaction(function (b) {
	                            b.executeSql("SELECT key FROM " + d.storeName, [], function (b, c) {
	                                for (var d = [], e = 0; e < c.rows.length; e++) {
	                                    d.push(c.rows.item(e).key);
	                                }a(d);
	                            }, function (a, b) {
	                                c(b);
	                            });
	                        });
	                    })["catch"](c);
	                });return j(c, a), c;
	            }function T(a) {
	                var b = this,
	                    c = {};if (a) for (var d in a) {
	                    c[d] = a[d];
	                }return c.keyPrefix = c.name + "/", c.storeName !== b._defaultConfig.storeName && (c.keyPrefix += c.storeName + "/"), b._dbInfo = c, c.serializer = Ca, ia.resolve();
	            }function U(a) {
	                var b = this,
	                    c = b.ready().then(function () {
	                    for (var a = b._dbInfo.keyPrefix, c = localStorage.length - 1; c >= 0; c--) {
	                        var d = localStorage.key(c);0 === d.indexOf(a) && localStorage.removeItem(d);
	                    }
	                });return j(c, a), c;
	            }function V(a, b) {
	                var c = this;"string" != typeof a && (console.warn(a + " used as a key, but it is not a string."), a = String(a));var d = c.ready().then(function () {
	                    var b = c._dbInfo,
	                        d = localStorage.getItem(b.keyPrefix + a);return d && (d = b.serializer.deserialize(d)), d;
	                });return j(d, b), d;
	            }function W(a, b) {
	                var c = this,
	                    d = c.ready().then(function () {
	                    for (var b = c._dbInfo, d = b.keyPrefix, e = d.length, f = localStorage.length, g = 1, h = 0; f > h; h++) {
	                        var i = localStorage.key(h);if (0 === i.indexOf(d)) {
	                            var j = localStorage.getItem(i);if (j && (j = b.serializer.deserialize(j)), j = a(j, i.substring(e), g++), void 0 !== j) return j;
	                        }
	                    }
	                });return j(d, b), d;
	            }function X(a, b) {
	                var c = this,
	                    d = c.ready().then(function () {
	                    var b,
	                        d = c._dbInfo;try {
	                        b = localStorage.key(a);
	                    } catch (e) {
	                        b = null;
	                    }return b && (b = b.substring(d.keyPrefix.length)), b;
	                });return j(d, b), d;
	            }function Y(a) {
	                var b = this,
	                    c = b.ready().then(function () {
	                    for (var a = b._dbInfo, c = localStorage.length, d = [], e = 0; c > e; e++) {
	                        0 === localStorage.key(e).indexOf(a.keyPrefix) && d.push(localStorage.key(e).substring(a.keyPrefix.length));
	                    }return d;
	                });return j(c, a), c;
	            }function Z(a) {
	                var b = this,
	                    c = b.keys().then(function (a) {
	                    return a.length;
	                });return j(c, a), c;
	            }function $(a, b) {
	                var c = this;"string" != typeof a && (console.warn(a + " used as a key, but it is not a string."), a = String(a));var d = c.ready().then(function () {
	                    var b = c._dbInfo;localStorage.removeItem(b.keyPrefix + a);
	                });return j(d, b), d;
	            }function _(a, b, c) {
	                var d = this;"string" != typeof a && (console.warn(a + " used as a key, but it is not a string."), a = String(a));var e = d.ready().then(function () {
	                    void 0 === b && (b = null);var c = b;return new ia(function (e, f) {
	                        var g = d._dbInfo;g.serializer.serialize(b, function (b, d) {
	                            if (d) f(d);else try {
	                                localStorage.setItem(g.keyPrefix + a, b), e(c);
	                            } catch (h) {
	                                "QuotaExceededError" !== h.name && "NS_ERROR_DOM_QUOTA_REACHED" !== h.name || f(h), f(h);
	                            }
	                        });
	                    });
	                });return j(e, c), e;
	            }function aa(a, b, c) {
	                "function" == typeof b && a.then(b), "function" == typeof c && a["catch"](c);
	            }function ba(a, b) {
	                a[b] = function () {
	                    var c = arguments;return a.ready().then(function () {
	                        return a[b].apply(a, c);
	                    });
	                };
	            }function ca() {
	                for (var a = 1; a < arguments.length; a++) {
	                    var b = arguments[a];if (b) for (var c in b) {
	                        b.hasOwnProperty(c) && (La(b[c]) ? arguments[0][c] = b[c].slice() : arguments[0][c] = b[c]);
	                    }
	                }return arguments[0];
	            }function da(a) {
	                for (var b in Ga) {
	                    if (Ga.hasOwnProperty(b) && Ga[b] === a) return !0;
	                }return !1;
	            }var ea = "function" == typeof Symbol && "symbol" == _typeof(Symbol.iterator) ? function (a) {
	                return typeof a === "undefined" ? "undefined" : _typeof(a);
	            } : function (a) {
	                return a && "function" == typeof Symbol && a.constructor === Symbol ? "symbol" : typeof a === "undefined" ? "undefined" : _typeof(a);
	            },
	                fa = e();"undefined" == typeof Promise && "undefined" != typeof a && a(3);var ga,
	                ha,
	                ia = Promise,
	                ja = "local-forage-detect-blob-support",
	                ka = { _driver: "asyncStorage", _initStorage: x, iterate: z, getItem: y, setItem: A, removeItem: B, clear: C, length: D, key: E, keys: F },
	                la = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/",
	                ma = "~~local_forage_type~",
	                na = /^~~local_forage_type~([^~]+)~/,
	                oa = "__lfsc__:",
	                pa = oa.length,
	                qa = "arbf",
	                ra = "blob",
	                sa = "si08",
	                ta = "ui08",
	                ua = "uic8",
	                va = "si16",
	                wa = "si32",
	                xa = "ur16",
	                ya = "ui32",
	                za = "fl32",
	                Aa = "fl64",
	                Ba = pa + qa.length,
	                Ca = { serialize: I, deserialize: J, stringToBuffer: G, bufferToString: H },
	                Da = { _driver: "webSQLStorage", _initStorage: K, iterate: M, getItem: L, setItem: N, removeItem: O, clear: P, length: Q, key: R, keys: S },
	                Ea = { _driver: "localStorageWrapper", _initStorage: T, iterate: W, getItem: V, setItem: _, removeItem: $, clear: U, length: Z, key: X, keys: Y },
	                Fa = {},
	                Ga = { INDEXEDDB: "asyncStorage", LOCALSTORAGE: "localStorageWrapper", WEBSQL: "webSQLStorage" },
	                Ha = [Ga.INDEXEDDB, Ga.WEBSQL, Ga.LOCALSTORAGE],
	                Ia = ["clear", "getItem", "iterate", "key", "keys", "length", "removeItem", "setItem"],
	                Ja = { description: "", driver: Ha.slice(), name: "localforage", size: 4980736, storeName: "keyvaluepairs", version: 1 },
	                Ka = {};Ka[Ga.INDEXEDDB] = f(), Ka[Ga.WEBSQL] = g(), Ka[Ga.LOCALSTORAGE] = h();var La = Array.isArray || function (a) {
	                return "[object Array]" === Object.prototype.toString.call(a);
	            },
	                Ma = function () {
	                function a(b) {
	                    d(this, a), this.INDEXEDDB = Ga.INDEXEDDB, this.LOCALSTORAGE = Ga.LOCALSTORAGE, this.WEBSQL = Ga.WEBSQL, this._defaultConfig = ca({}, Ja), this._config = ca({}, this._defaultConfig, b), this._driverSet = null, this._initDriver = null, this._ready = !1, this._dbInfo = null, this._wrapLibraryMethodsWithReady(), this.setDriver(this._config.driver);
	                }return a.prototype.config = function (a) {
	                    if ("object" === ("undefined" == typeof a ? "undefined" : ea(a))) {
	                        if (this._ready) return new Error("Can't call config() after localforage has been used.");for (var b in a) {
	                            "storeName" === b && (a[b] = a[b].replace(/\W/g, "_")), this._config[b] = a[b];
	                        }return "driver" in a && a.driver && this.setDriver(this._config.driver), !0;
	                    }return "string" == typeof a ? this._config[a] : this._config;
	                }, a.prototype.defineDriver = function (a, b, c) {
	                    var d = new ia(function (b, c) {
	                        try {
	                            var d = a._driver,
	                                e = new Error("Custom driver not compliant; see https://mozilla.github.io/localForage/#definedriver"),
	                                f = new Error("Custom driver name already in use: " + a._driver);if (!a._driver) return void c(e);if (da(a._driver)) return void c(f);for (var g = Ia.concat("_initStorage"), h = 0; h < g.length; h++) {
	                                var i = g[h];if (!i || !a[i] || "function" != typeof a[i]) return void c(e);
	                            }var j = ia.resolve(!0);"_support" in a && (j = a._support && "function" == typeof a._support ? a._support() : ia.resolve(!!a._support)), j.then(function (c) {
	                                Ka[d] = c, Fa[d] = a, b();
	                            }, c);
	                        } catch (k) {
	                            c(k);
	                        }
	                    });return aa(d, b, c), d;
	                }, a.prototype.driver = function () {
	                    return this._driver || null;
	                }, a.prototype.getDriver = function (a, b, c) {
	                    var d = this,
	                        e = ia.resolve().then(function () {
	                        if (!da(a)) {
	                            if (Fa[a]) return Fa[a];throw new Error("Driver not found.");
	                        }switch (a) {case d.INDEXEDDB:
	                                return ka;case d.LOCALSTORAGE:
	                                return Ea;case d.WEBSQL:
	                                return Da;}
	                    });return aa(e, b, c), e;
	                }, a.prototype.getSerializer = function (a) {
	                    var b = ia.resolve(Ca);return aa(b, a), b;
	                }, a.prototype.ready = function (a) {
	                    var b = this,
	                        c = b._driverSet.then(function () {
	                        return null === b._ready && (b._ready = b._initDriver()), b._ready;
	                    });return aa(c, a, a), c;
	                }, a.prototype.setDriver = function (a, b, c) {
	                    function d() {
	                        f._config.driver = f.driver();
	                    }function e(a) {
	                        return function () {
	                            function b() {
	                                for (; c < a.length;) {
	                                    var e = a[c];return c++, f._dbInfo = null, f._ready = null, f.getDriver(e).then(function (a) {
	                                        return f._extend(a), d(), f._ready = f._initStorage(f._config), f._ready;
	                                    })["catch"](b);
	                                }d();var g = new Error("No available storage method found.");return f._driverSet = ia.reject(g), f._driverSet;
	                            }var c = 0;return b();
	                        };
	                    }var f = this;La(a) || (a = [a]);var g = this._getSupportedDrivers(a),
	                        h = null !== this._driverSet ? this._driverSet["catch"](function () {
	                        return ia.resolve();
	                    }) : ia.resolve();return this._driverSet = h.then(function () {
	                        var a = g[0];return f._dbInfo = null, f._ready = null, f.getDriver(a).then(function (a) {
	                            f._driver = a._driver, d(), f._wrapLibraryMethodsWithReady(), f._initDriver = e(g);
	                        });
	                    })["catch"](function () {
	                        d();var a = new Error("No available storage method found.");return f._driverSet = ia.reject(a), f._driverSet;
	                    }), aa(this._driverSet, b, c), this._driverSet;
	                }, a.prototype.supports = function (a) {
	                    return !!Ka[a];
	                }, a.prototype._extend = function (a) {
	                    ca(this, a);
	                }, a.prototype._getSupportedDrivers = function (a) {
	                    for (var b = [], c = 0, d = a.length; d > c; c++) {
	                        var e = a[c];this.supports(e) && b.push(e);
	                    }return b;
	                }, a.prototype._wrapLibraryMethodsWithReady = function () {
	                    for (var a = 0; a < Ia.length; a++) {
	                        ba(this, Ia[a]);
	                    }
	                }, a.prototype.createInstance = function (b) {
	                    return new a(b);
	                }, a;
	            }(),
	                Na = new Ma();b.exports = Na;
	        }, { 3: 3 }] }, {}, [4])(4);
	});
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(3)))

/***/ },
/* 63 */
/***/ function(module, exports) {

	"use strict";
	'use strict';

	exports.toByteArray = toByteArray;
	exports.fromByteArray = fromByteArray;

	var lookup = [];
	var revLookup = [];
	var Arr = typeof Uint8Array !== 'undefined' ? Uint8Array : Array;

	function init() {
	  var code = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';
	  for (var i = 0, len = code.length; i < len; ++i) {
	    lookup[i] = code[i];
	    revLookup[code.charCodeAt(i)] = i;
	  }

	  revLookup['-'.charCodeAt(0)] = 62;
	  revLookup['_'.charCodeAt(0)] = 63;
	}

	init();

	function toByteArray(b64) {
	  var i, j, l, tmp, placeHolders, arr;
	  var len = b64.length;

	  if (len % 4 > 0) {
	    throw new Error('Invalid string. Length must be a multiple of 4');
	  }

	  // the number of equal signs (place holders)
	  // if there are two placeholders, than the two characters before it
	  // represent one byte
	  // if there is only one, then the three characters before it represent 2 bytes
	  // this is just a cheap hack to not do indexOf twice
	  placeHolders = b64[len - 2] === '=' ? 2 : b64[len - 1] === '=' ? 1 : 0;

	  // base64 is 4/3 + up to two characters of the original data
	  arr = new Arr(len * 3 / 4 - placeHolders);

	  // if there are placeholders, only get up to the last complete 4 chars
	  l = placeHolders > 0 ? len - 4 : len;

	  var L = 0;

	  for (i = 0, j = 0; i < l; i += 4, j += 3) {
	    tmp = revLookup[b64.charCodeAt(i)] << 18 | revLookup[b64.charCodeAt(i + 1)] << 12 | revLookup[b64.charCodeAt(i + 2)] << 6 | revLookup[b64.charCodeAt(i + 3)];
	    arr[L++] = tmp >> 16 & 0xFF;
	    arr[L++] = tmp >> 8 & 0xFF;
	    arr[L++] = tmp & 0xFF;
	  }

	  if (placeHolders === 2) {
	    tmp = revLookup[b64.charCodeAt(i)] << 2 | revLookup[b64.charCodeAt(i + 1)] >> 4;
	    arr[L++] = tmp & 0xFF;
	  } else if (placeHolders === 1) {
	    tmp = revLookup[b64.charCodeAt(i)] << 10 | revLookup[b64.charCodeAt(i + 1)] << 4 | revLookup[b64.charCodeAt(i + 2)] >> 2;
	    arr[L++] = tmp >> 8 & 0xFF;
	    arr[L++] = tmp & 0xFF;
	  }

	  return arr;
	}

	function tripletToBase64(num) {
	  return lookup[num >> 18 & 0x3F] + lookup[num >> 12 & 0x3F] + lookup[num >> 6 & 0x3F] + lookup[num & 0x3F];
	}

	function encodeChunk(uint8, start, end) {
	  var tmp;
	  var output = [];
	  for (var i = start; i < end; i += 3) {
	    tmp = (uint8[i] << 16) + (uint8[i + 1] << 8) + uint8[i + 2];
	    output.push(tripletToBase64(tmp));
	  }
	  return output.join('');
	}

	function fromByteArray(uint8) {
	  var tmp;
	  var len = uint8.length;
	  var extraBytes = len % 3; // if we have 1 byte left, pad 2 bytes
	  var output = '';
	  var parts = [];
	  var maxChunkLength = 16383; // must be multiple of 3

	  // go through the array every three bytes, we'll deal with trailing stuff later
	  for (var i = 0, len2 = len - extraBytes; i < len2; i += maxChunkLength) {
	    parts.push(encodeChunk(uint8, i, i + maxChunkLength > len2 ? len2 : i + maxChunkLength));
	  }

	  // pad the end with zeros, but make sure to not forget the extra bytes
	  if (extraBytes === 1) {
	    tmp = uint8[len - 1];
	    output += lookup[tmp >> 2];
	    output += lookup[tmp << 4 & 0x3F];
	    output += '==';
	  } else if (extraBytes === 2) {
	    tmp = (uint8[len - 2] << 8) + uint8[len - 1];
	    output += lookup[tmp >> 10];
	    output += lookup[tmp >> 4 & 0x3F];
	    output += lookup[tmp << 2 & 0x3F];
	    output += '=';
	  }

	  parts.push(output);

	  return parts.join('');
	}

/***/ },
/* 64 */
/***/ function(module, exports) {

	"use strict";
	'use strict';

	var toString = {}.toString;

	module.exports = Array.isArray || function (arr) {
	  return toString.call(arr) == '[object Array]';
	};

/***/ },
/* 65 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	/* WEBPACK VAR INJECTION */(function(Buffer) {/*!
	 * @description Recursive object extending
	 * @author Viacheslav Lotsmanov <lotsmanov89@gmail.com>
	 * @license MIT
	 *
	 * The MIT License (MIT)
	 *
	 * Copyright (c) 2013-2015 Viacheslav Lotsmanov
	 *
	 * Permission is hereby granted, free of charge, to any person obtaining a copy of
	 * this software and associated documentation files (the "Software"), to deal in
	 * the Software without restriction, including without limitation the rights to
	 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
	 * the Software, and to permit persons to whom the Software is furnished to do so,
	 * subject to the following conditions:
	 *
	 * The above copyright notice and this permission notice shall be included in all
	 * copies or substantial portions of the Software.
	 *
	 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
	 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
	 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
	 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
	 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
	 */

	'use strict';

	var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };

	function isSpecificValue(val) {
		return val instanceof Buffer || val instanceof Date || val instanceof RegExp ? true : false;
	}

	function cloneSpecificValue(val) {
		if (val instanceof Buffer) {
			var x = new Buffer(val.length);
			val.copy(x);
			return x;
		} else if (val instanceof Date) {
			return new Date(val.getTime());
		} else if (val instanceof RegExp) {
			return new RegExp(val);
		} else {
			throw new Error('Unexpected situation');
		}
	}

	/**
	 * Recursive cloning array.
	 */
	function deepCloneArray(arr) {
		var clone = [];
		arr.forEach(function (item, index) {
			if ((typeof item === 'undefined' ? 'undefined' : _typeof(item)) === 'object' && item !== null) {
				if (Array.isArray(item)) {
					clone[index] = deepCloneArray(item);
				} else if (isSpecificValue(item)) {
					clone[index] = cloneSpecificValue(item);
				} else {
					clone[index] = deepExtend({}, item);
				}
			} else {
				clone[index] = item;
			}
		});
		return clone;
	}

	/**
	 * Extening object that entered in first argument.
	 *
	 * Returns extended object or false if have no target object or incorrect type.
	 *
	 * If you wish to clone source object (without modify it), just use empty new
	 * object as first argument, like this:
	 *   deepExtend({}, yourObj_1, [yourObj_N]);
	 */
	var deepExtend = module.exports = function () /*obj_1, [obj_2], [obj_N]*/{
		if (arguments.length < 1 || _typeof(arguments[0]) !== 'object') {
			return false;
		}

		if (arguments.length < 2) {
			return arguments[0];
		}

		var target = arguments[0];

		// convert arguments to array and cut off target object
		var args = Array.prototype.slice.call(arguments, 1);

		var val, src, clone;

		args.forEach(function (obj) {
			// skip argument if it is array or isn't object
			if ((typeof obj === 'undefined' ? 'undefined' : _typeof(obj)) !== 'object' || Array.isArray(obj)) {
				return;
			}

			Object.keys(obj).forEach(function (key) {
				src = target[key]; // source value
				val = obj[key]; // new value

				// recursion prevention
				if (val === target) {
					return;

					/**
	     * if new value isn't object then just overwrite by new value
	     * instead of extending.
	     */
				} else if ((typeof val === 'undefined' ? 'undefined' : _typeof(val)) !== 'object' || val === null) {
						target[key] = val;
						return;

						// just clone arrays (and recursive clone objects inside)
					} else if (Array.isArray(val)) {
							target[key] = deepCloneArray(val);
							return;

							// custom cloning and overwrite for specific objects
						} else if (isSpecificValue(val)) {
								target[key] = cloneSpecificValue(val);
								return;

								// overwrite by new value if source isn't object or array
							} else if ((typeof src === 'undefined' ? 'undefined' : _typeof(src)) !== 'object' || src === null || Array.isArray(src)) {
									target[key] = deepExtend({}, val);
									return;

									// source value and new value is objects both, extending...
								} else {
										target[key] = deepExtend(src, val);
										return;
									}
			});
		});

		return target;
	};
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(10).Buffer))

/***/ },
/* 66 */
/***/ function(module, exports) {

	"use strict";
	"use strict";

	exports.read = function (buffer, offset, isLE, mLen, nBytes) {
	  var e, m;
	  var eLen = nBytes * 8 - mLen - 1;
	  var eMax = (1 << eLen) - 1;
	  var eBias = eMax >> 1;
	  var nBits = -7;
	  var i = isLE ? nBytes - 1 : 0;
	  var d = isLE ? -1 : 1;
	  var s = buffer[offset + i];

	  i += d;

	  e = s & (1 << -nBits) - 1;
	  s >>= -nBits;
	  nBits += eLen;
	  for (; nBits > 0; e = e * 256 + buffer[offset + i], i += d, nBits -= 8) {}

	  m = e & (1 << -nBits) - 1;
	  e >>= -nBits;
	  nBits += mLen;
	  for (; nBits > 0; m = m * 256 + buffer[offset + i], i += d, nBits -= 8) {}

	  if (e === 0) {
	    e = 1 - eBias;
	  } else if (e === eMax) {
	    return m ? NaN : (s ? -1 : 1) * Infinity;
	  } else {
	    m = m + Math.pow(2, mLen);
	    e = e - eBias;
	  }
	  return (s ? -1 : 1) * m * Math.pow(2, e - mLen);
	};

	exports.write = function (buffer, value, offset, isLE, mLen, nBytes) {
	  var e, m, c;
	  var eLen = nBytes * 8 - mLen - 1;
	  var eMax = (1 << eLen) - 1;
	  var eBias = eMax >> 1;
	  var rt = mLen === 23 ? Math.pow(2, -24) - Math.pow(2, -77) : 0;
	  var i = isLE ? 0 : nBytes - 1;
	  var d = isLE ? 1 : -1;
	  var s = value < 0 || value === 0 && 1 / value < 0 ? 1 : 0;

	  value = Math.abs(value);

	  if (isNaN(value) || value === Infinity) {
	    m = isNaN(value) ? 1 : 0;
	    e = eMax;
	  } else {
	    e = Math.floor(Math.log(value) / Math.LN2);
	    if (value * (c = Math.pow(2, -e)) < 1) {
	      e--;
	      c *= 2;
	    }
	    if (e + eBias >= 1) {
	      value += rt / c;
	    } else {
	      value += rt * Math.pow(2, 1 - eBias);
	    }
	    if (value * c >= 2) {
	      e++;
	      c /= 2;
	    }

	    if (e + eBias >= eMax) {
	      m = 0;
	      e = eMax;
	    } else if (e + eBias >= 1) {
	      m = (value * c - 1) * Math.pow(2, mLen);
	      e = e + eBias;
	    } else {
	      m = value * Math.pow(2, eBias - 1) * Math.pow(2, mLen);
	      e = 0;
	    }
	  }

	  for (; mLen >= 8; buffer[offset + i] = m & 0xff, i += d, m /= 256, mLen -= 8) {}

	  e = e << mLen | m;
	  eLen += mLen;
	  for (; eLen > 0; buffer[offset + i] = e & 0xff, i += d, e /= 256, eLen -= 8) {}

	  buffer[offset + i - d] |= s * 128;
	};

/***/ },
/* 67 */
/***/ function(module, exports) {

	"use strict";
	'use strict';

	if (typeof Object.create === 'function') {
	  // implementation from standard node.js 'util' module
	  module.exports = function inherits(ctor, superCtor) {
	    ctor.super_ = superCtor;
	    ctor.prototype = Object.create(superCtor.prototype, {
	      constructor: {
	        value: ctor,
	        enumerable: false,
	        writable: true,
	        configurable: true
	      }
	    });
	  };
	} else {
	  // old school shim for old browsers
	  module.exports = function inherits(ctor, superCtor) {
	    ctor.super_ = superCtor;
	    var TempCtor = function TempCtor() {};
	    TempCtor.prototype = superCtor.prototype;
	    ctor.prototype = new TempCtor();
	    ctor.prototype.constructor = ctor;
	  };
	}

/***/ },
/* 68 */
/***/ function(module, exports) {

	"use strict";
	'use strict';

	// shim for using process in browser

	var process = module.exports = {};
	var queue = [];
	var draining = false;
	var currentQueue;
	var queueIndex = -1;

	function cleanUpNextTick() {
	    draining = false;
	    if (currentQueue.length) {
	        queue = currentQueue.concat(queue);
	    } else {
	        queueIndex = -1;
	    }
	    if (queue.length) {
	        drainQueue();
	    }
	}

	function drainQueue() {
	    if (draining) {
	        return;
	    }
	    var timeout = setTimeout(cleanUpNextTick);
	    draining = true;

	    var len = queue.length;
	    while (len) {
	        currentQueue = queue;
	        queue = [];
	        while (++queueIndex < len) {
	            if (currentQueue) {
	                currentQueue[queueIndex].run();
	            }
	        }
	        queueIndex = -1;
	        len = queue.length;
	    }
	    currentQueue = null;
	    draining = false;
	    clearTimeout(timeout);
	}

	process.nextTick = function (fun) {
	    var args = new Array(arguments.length - 1);
	    if (arguments.length > 1) {
	        for (var i = 1; i < arguments.length; i++) {
	            args[i - 1] = arguments[i];
	        }
	    }
	    queue.push(new Item(fun, args));
	    if (queue.length === 1 && !draining) {
	        setTimeout(drainQueue, 0);
	    }
	};

	// v8 likes predictible objects
	function Item(fun, array) {
	    this.fun = fun;
	    this.array = array;
	}
	Item.prototype.run = function () {
	    this.fun.apply(null, this.array);
	};
	process.title = 'browser';
	process.browser = true;
	process.env = {};
	process.argv = [];
	process.version = ''; // empty string to avoid regexp issues
	process.versions = {};

	function noop() {}

	process.on = noop;
	process.addListener = noop;
	process.once = noop;
	process.off = noop;
	process.removeListener = noop;
	process.removeAllListeners = noop;
	process.emit = noop;

	process.binding = function (name) {
	    throw new Error('process.binding is not supported');
	};

	process.cwd = function () {
	    return '/';
	};
	process.chdir = function (dir) {
	    throw new Error('process.chdir is not supported');
	};
	process.umask = function () {
	    return 0;
	};

/***/ },
/* 69 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	/* WEBPACK VAR INJECTION */(function(module, global) {var __WEBPACK_AMD_DEFINE_RESULT__;'use strict';

	var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };

	/*! https://mths.be/punycode v1.4.1 by @mathias */
	;(function (root) {

		/** Detect free variables */
		var freeExports = ( false ? 'undefined' : _typeof(exports)) == 'object' && exports && !exports.nodeType && exports;
		var freeModule = ( false ? 'undefined' : _typeof(module)) == 'object' && module && !module.nodeType && module;
		var freeGlobal = (typeof global === 'undefined' ? 'undefined' : _typeof(global)) == 'object' && global;
		if (freeGlobal.global === freeGlobal || freeGlobal.window === freeGlobal || freeGlobal.self === freeGlobal) {
			root = freeGlobal;
		}

		/**
	  * The `punycode` object.
	  * @name punycode
	  * @type Object
	  */
		var punycode,


		/** Highest positive signed 32-bit float value */
		maxInt = 2147483647,
		    // aka. 0x7FFFFFFF or 2^31-1

		/** Bootstring parameters */
		base = 36,
		    tMin = 1,
		    tMax = 26,
		    skew = 38,
		    damp = 700,
		    initialBias = 72,
		    initialN = 128,
		    // 0x80
		delimiter = '-',
		    // '\x2D'

		/** Regular expressions */
		regexPunycode = /^xn--/,
		    regexNonASCII = /[^\x20-\x7E]/,
		    // unprintable ASCII chars + non-ASCII chars
		regexSeparators = /[\x2E\u3002\uFF0E\uFF61]/g,
		    // RFC 3490 separators

		/** Error messages */
		errors = {
			'overflow': 'Overflow: input needs wider integers to process',
			'not-basic': 'Illegal input >= 0x80 (not a basic code point)',
			'invalid-input': 'Invalid input'
		},


		/** Convenience shortcuts */
		baseMinusTMin = base - tMin,
		    floor = Math.floor,
		    stringFromCharCode = String.fromCharCode,


		/** Temporary variable */
		key;

		/*--------------------------------------------------------------------------*/

		/**
	  * A generic error utility function.
	  * @private
	  * @param {String} type The error type.
	  * @returns {Error} Throws a `RangeError` with the applicable error message.
	  */
		function error(type) {
			throw new RangeError(errors[type]);
		}

		/**
	  * A generic `Array#map` utility function.
	  * @private
	  * @param {Array} array The array to iterate over.
	  * @param {Function} callback The function that gets called for every array
	  * item.
	  * @returns {Array} A new array of values returned by the callback function.
	  */
		function map(array, fn) {
			var length = array.length;
			var result = [];
			while (length--) {
				result[length] = fn(array[length]);
			}
			return result;
		}

		/**
	  * A simple `Array#map`-like wrapper to work with domain name strings or email
	  * addresses.
	  * @private
	  * @param {String} domain The domain name or email address.
	  * @param {Function} callback The function that gets called for every
	  * character.
	  * @returns {Array} A new string of characters returned by the callback
	  * function.
	  */
		function mapDomain(string, fn) {
			var parts = string.split('@');
			var result = '';
			if (parts.length > 1) {
				// In email addresses, only the domain name should be punycoded. Leave
				// the local part (i.e. everything up to `@`) intact.
				result = parts[0] + '@';
				string = parts[1];
			}
			// Avoid `split(regex)` for IE8 compatibility. See #17.
			string = string.replace(regexSeparators, '\x2E');
			var labels = string.split('.');
			var encoded = map(labels, fn).join('.');
			return result + encoded;
		}

		/**
	  * Creates an array containing the numeric code points of each Unicode
	  * character in the string. While JavaScript uses UCS-2 internally,
	  * this function will convert a pair of surrogate halves (each of which
	  * UCS-2 exposes as separate characters) into a single code point,
	  * matching UTF-16.
	  * @see `punycode.ucs2.encode`
	  * @see <https://mathiasbynens.be/notes/javascript-encoding>
	  * @memberOf punycode.ucs2
	  * @name decode
	  * @param {String} string The Unicode input string (UCS-2).
	  * @returns {Array} The new array of code points.
	  */
		function ucs2decode(string) {
			var output = [],
			    counter = 0,
			    length = string.length,
			    value,
			    extra;
			while (counter < length) {
				value = string.charCodeAt(counter++);
				if (value >= 0xD800 && value <= 0xDBFF && counter < length) {
					// high surrogate, and there is a next character
					extra = string.charCodeAt(counter++);
					if ((extra & 0xFC00) == 0xDC00) {
						// low surrogate
						output.push(((value & 0x3FF) << 10) + (extra & 0x3FF) + 0x10000);
					} else {
						// unmatched surrogate; only append this code unit, in case the next
						// code unit is the high surrogate of a surrogate pair
						output.push(value);
						counter--;
					}
				} else {
					output.push(value);
				}
			}
			return output;
		}

		/**
	  * Creates a string based on an array of numeric code points.
	  * @see `punycode.ucs2.decode`
	  * @memberOf punycode.ucs2
	  * @name encode
	  * @param {Array} codePoints The array of numeric code points.
	  * @returns {String} The new Unicode string (UCS-2).
	  */
		function ucs2encode(array) {
			return map(array, function (value) {
				var output = '';
				if (value > 0xFFFF) {
					value -= 0x10000;
					output += stringFromCharCode(value >>> 10 & 0x3FF | 0xD800);
					value = 0xDC00 | value & 0x3FF;
				}
				output += stringFromCharCode(value);
				return output;
			}).join('');
		}

		/**
	  * Converts a basic code point into a digit/integer.
	  * @see `digitToBasic()`
	  * @private
	  * @param {Number} codePoint The basic numeric code point value.
	  * @returns {Number} The numeric value of a basic code point (for use in
	  * representing integers) in the range `0` to `base - 1`, or `base` if
	  * the code point does not represent a value.
	  */
		function basicToDigit(codePoint) {
			if (codePoint - 48 < 10) {
				return codePoint - 22;
			}
			if (codePoint - 65 < 26) {
				return codePoint - 65;
			}
			if (codePoint - 97 < 26) {
				return codePoint - 97;
			}
			return base;
		}

		/**
	  * Converts a digit/integer into a basic code point.
	  * @see `basicToDigit()`
	  * @private
	  * @param {Number} digit The numeric value of a basic code point.
	  * @returns {Number} The basic code point whose value (when used for
	  * representing integers) is `digit`, which needs to be in the range
	  * `0` to `base - 1`. If `flag` is non-zero, the uppercase form is
	  * used; else, the lowercase form is used. The behavior is undefined
	  * if `flag` is non-zero and `digit` has no uppercase form.
	  */
		function digitToBasic(digit, flag) {
			//  0..25 map to ASCII a..z or A..Z
			// 26..35 map to ASCII 0..9
			return digit + 22 + 75 * (digit < 26) - ((flag != 0) << 5);
		}

		/**
	  * Bias adaptation function as per section 3.4 of RFC 3492.
	  * https://tools.ietf.org/html/rfc3492#section-3.4
	  * @private
	  */
		function adapt(delta, numPoints, firstTime) {
			var k = 0;
			delta = firstTime ? floor(delta / damp) : delta >> 1;
			delta += floor(delta / numPoints);
			for (; /* no initialization */delta > baseMinusTMin * tMax >> 1; k += base) {
				delta = floor(delta / baseMinusTMin);
			}
			return floor(k + (baseMinusTMin + 1) * delta / (delta + skew));
		}

		/**
	  * Converts a Punycode string of ASCII-only symbols to a string of Unicode
	  * symbols.
	  * @memberOf punycode
	  * @param {String} input The Punycode string of ASCII-only symbols.
	  * @returns {String} The resulting string of Unicode symbols.
	  */
		function decode(input) {
			// Don't use UCS-2
			var output = [],
			    inputLength = input.length,
			    out,
			    i = 0,
			    n = initialN,
			    bias = initialBias,
			    basic,
			    j,
			    index,
			    oldi,
			    w,
			    k,
			    digit,
			    t,

			/** Cached calculation results */
			baseMinusT;

			// Handle the basic code points: let `basic` be the number of input code
			// points before the last delimiter, or `0` if there is none, then copy
			// the first basic code points to the output.

			basic = input.lastIndexOf(delimiter);
			if (basic < 0) {
				basic = 0;
			}

			for (j = 0; j < basic; ++j) {
				// if it's not a basic code point
				if (input.charCodeAt(j) >= 0x80) {
					error('not-basic');
				}
				output.push(input.charCodeAt(j));
			}

			// Main decoding loop: start just after the last delimiter if any basic code
			// points were copied; start at the beginning otherwise.

			for (index = basic > 0 ? basic + 1 : 0; index < inputLength;) /* no final expression */{

				// `index` is the index of the next character to be consumed.
				// Decode a generalized variable-length integer into `delta`,
				// which gets added to `i`. The overflow checking is easier
				// if we increase `i` as we go, then subtract off its starting
				// value at the end to obtain `delta`.
				for (oldi = i, w = 1, k = base;; /* no condition */k += base) {

					if (index >= inputLength) {
						error('invalid-input');
					}

					digit = basicToDigit(input.charCodeAt(index++));

					if (digit >= base || digit > floor((maxInt - i) / w)) {
						error('overflow');
					}

					i += digit * w;
					t = k <= bias ? tMin : k >= bias + tMax ? tMax : k - bias;

					if (digit < t) {
						break;
					}

					baseMinusT = base - t;
					if (w > floor(maxInt / baseMinusT)) {
						error('overflow');
					}

					w *= baseMinusT;
				}

				out = output.length + 1;
				bias = adapt(i - oldi, out, oldi == 0);

				// `i` was supposed to wrap around from `out` to `0`,
				// incrementing `n` each time, so we'll fix that now:
				if (floor(i / out) > maxInt - n) {
					error('overflow');
				}

				n += floor(i / out);
				i %= out;

				// Insert `n` at position `i` of the output
				output.splice(i++, 0, n);
			}

			return ucs2encode(output);
		}

		/**
	  * Converts a string of Unicode symbols (e.g. a domain name label) to a
	  * Punycode string of ASCII-only symbols.
	  * @memberOf punycode
	  * @param {String} input The string of Unicode symbols.
	  * @returns {String} The resulting Punycode string of ASCII-only symbols.
	  */
		function encode(input) {
			var n,
			    delta,
			    handledCPCount,
			    basicLength,
			    bias,
			    j,
			    m,
			    q,
			    k,
			    t,
			    currentValue,
			    output = [],

			/** `inputLength` will hold the number of code points in `input`. */
			inputLength,

			/** Cached calculation results */
			handledCPCountPlusOne,
			    baseMinusT,
			    qMinusT;

			// Convert the input in UCS-2 to Unicode
			input = ucs2decode(input);

			// Cache the length
			inputLength = input.length;

			// Initialize the state
			n = initialN;
			delta = 0;
			bias = initialBias;

			// Handle the basic code points
			for (j = 0; j < inputLength; ++j) {
				currentValue = input[j];
				if (currentValue < 0x80) {
					output.push(stringFromCharCode(currentValue));
				}
			}

			handledCPCount = basicLength = output.length;

			// `handledCPCount` is the number of code points that have been handled;
			// `basicLength` is the number of basic code points.

			// Finish the basic string - if it is not empty - with a delimiter
			if (basicLength) {
				output.push(delimiter);
			}

			// Main encoding loop:
			while (handledCPCount < inputLength) {

				// All non-basic code points < n have been handled already. Find the next
				// larger one:
				for (m = maxInt, j = 0; j < inputLength; ++j) {
					currentValue = input[j];
					if (currentValue >= n && currentValue < m) {
						m = currentValue;
					}
				}

				// Increase `delta` enough to advance the decoder's <n,i> state to <m,0>,
				// but guard against overflow
				handledCPCountPlusOne = handledCPCount + 1;
				if (m - n > floor((maxInt - delta) / handledCPCountPlusOne)) {
					error('overflow');
				}

				delta += (m - n) * handledCPCountPlusOne;
				n = m;

				for (j = 0; j < inputLength; ++j) {
					currentValue = input[j];

					if (currentValue < n && ++delta > maxInt) {
						error('overflow');
					}

					if (currentValue == n) {
						// Represent delta as a generalized variable-length integer
						for (q = delta, k = base;; /* no condition */k += base) {
							t = k <= bias ? tMin : k >= bias + tMax ? tMax : k - bias;
							if (q < t) {
								break;
							}
							qMinusT = q - t;
							baseMinusT = base - t;
							output.push(stringFromCharCode(digitToBasic(t + qMinusT % baseMinusT, 0)));
							q = floor(qMinusT / baseMinusT);
						}

						output.push(stringFromCharCode(digitToBasic(q, 0)));
						bias = adapt(delta, handledCPCountPlusOne, handledCPCount == basicLength);
						delta = 0;
						++handledCPCount;
					}
				}

				++delta;
				++n;
			}
			return output.join('');
		}

		/**
	  * Converts a Punycode string representing a domain name or an email address
	  * to Unicode. Only the Punycoded parts of the input will be converted, i.e.
	  * it doesn't matter if you call it on a string that has already been
	  * converted to Unicode.
	  * @memberOf punycode
	  * @param {String} input The Punycoded domain name or email address to
	  * convert to Unicode.
	  * @returns {String} The Unicode representation of the given Punycode
	  * string.
	  */
		function toUnicode(input) {
			return mapDomain(input, function (string) {
				return regexPunycode.test(string) ? decode(string.slice(4).toLowerCase()) : string;
			});
		}

		/**
	  * Converts a Unicode string representing a domain name or an email address to
	  * Punycode. Only the non-ASCII parts of the domain name will be converted,
	  * i.e. it doesn't matter if you call it with a domain that's already in
	  * ASCII.
	  * @memberOf punycode
	  * @param {String} input The domain name or email address to convert, as a
	  * Unicode string.
	  * @returns {String} The Punycode representation of the given domain name or
	  * email address.
	  */
		function toASCII(input) {
			return mapDomain(input, function (string) {
				return regexNonASCII.test(string) ? 'xn--' + encode(string) : string;
			});
		}

		/*--------------------------------------------------------------------------*/

		/** Define the public API */
		punycode = {
			/**
	   * A string representing the current Punycode.js version number.
	   * @memberOf punycode
	   * @type String
	   */
			'version': '1.4.1',
			/**
	   * An object of methods to convert from JavaScript's internal character
	   * representation (UCS-2) to Unicode code points, and back.
	   * @see <https://mathiasbynens.be/notes/javascript-encoding>
	   * @memberOf punycode
	   * @type Object
	   */
			'ucs2': {
				'decode': ucs2decode,
				'encode': ucs2encode
			},
			'decode': decode,
			'encode': encode,
			'toASCII': toASCII,
			'toUnicode': toUnicode
		};

		/** Expose `punycode` */
		// Some AMD build optimizers, like r.js, check for specific condition patterns
		// like the following:
		if ("function" == 'function' && _typeof(__webpack_require__(22)) == 'object' && __webpack_require__(22)) {
			!(__WEBPACK_AMD_DEFINE_RESULT__ = function () {
				return punycode;
			}.call(exports, __webpack_require__, exports, module), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
		} else if (freeExports && freeModule) {
			if (module.exports == freeExports) {
				// in Node.js, io.js, or RingoJS v0.8.0+
				freeModule.exports = punycode;
			} else {
				// in Narwhal or RingoJS v0.7.0-
				for (key in punycode) {
					punycode.hasOwnProperty(key) && (freeExports[key] = punycode[key]);
				}
			}
		} else {
			// in Rhino or a web browser
			root.punycode = punycode;
		}
	})(undefined);
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(21)(module), __webpack_require__(3)))

/***/ },
/* 70 */
/***/ function(module, exports) {

	"use strict";
	// Copyright Joyent, Inc. and other Node contributors.
	//
	// Permission is hereby granted, free of charge, to any person obtaining a
	// copy of this software and associated documentation files (the
	// "Software"), to deal in the Software without restriction, including
	// without limitation the rights to use, copy, modify, merge, publish,
	// distribute, sublicense, and/or sell copies of the Software, and to permit
	// persons to whom the Software is furnished to do so, subject to the
	// following conditions:
	//
	// The above copyright notice and this permission notice shall be included
	// in all copies or substantial portions of the Software.
	//
	// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
	// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
	// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
	// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
	// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
	// USE OR OTHER DEALINGS IN THE SOFTWARE.

	'use strict';

	// If obj.hasOwnProperty has been overridden, then calling
	// obj.hasOwnProperty(prop) will break.
	// See: https://github.com/joyent/node/issues/1707

	function hasOwnProperty(obj, prop) {
	  return Object.prototype.hasOwnProperty.call(obj, prop);
	}

	module.exports = function (qs, sep, eq, options) {
	  sep = sep || '&';
	  eq = eq || '=';
	  var obj = {};

	  if (typeof qs !== 'string' || qs.length === 0) {
	    return obj;
	  }

	  var regexp = /\+/g;
	  qs = qs.split(sep);

	  var maxKeys = 1000;
	  if (options && typeof options.maxKeys === 'number') {
	    maxKeys = options.maxKeys;
	  }

	  var len = qs.length;
	  // maxKeys <= 0 means that we should not limit keys count
	  if (maxKeys > 0 && len > maxKeys) {
	    len = maxKeys;
	  }

	  for (var i = 0; i < len; ++i) {
	    var x = qs[i].replace(regexp, '%20'),
	        idx = x.indexOf(eq),
	        kstr,
	        vstr,
	        k,
	        v;

	    if (idx >= 0) {
	      kstr = x.substr(0, idx);
	      vstr = x.substr(idx + 1);
	    } else {
	      kstr = x;
	      vstr = '';
	    }

	    k = decodeURIComponent(kstr);
	    v = decodeURIComponent(vstr);

	    if (!hasOwnProperty(obj, k)) {
	      obj[k] = v;
	    } else if (isArray(obj[k])) {
	      obj[k].push(v);
	    } else {
	      obj[k] = [obj[k], v];
	    }
	  }

	  return obj;
	};

	var isArray = Array.isArray || function (xs) {
	  return Object.prototype.toString.call(xs) === '[object Array]';
	};

/***/ },
/* 71 */
/***/ function(module, exports) {

	"use strict";
	// Copyright Joyent, Inc. and other Node contributors.
	//
	// Permission is hereby granted, free of charge, to any person obtaining a
	// copy of this software and associated documentation files (the
	// "Software"), to deal in the Software without restriction, including
	// without limitation the rights to use, copy, modify, merge, publish,
	// distribute, sublicense, and/or sell copies of the Software, and to permit
	// persons to whom the Software is furnished to do so, subject to the
	// following conditions:
	//
	// The above copyright notice and this permission notice shall be included
	// in all copies or substantial portions of the Software.
	//
	// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
	// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
	// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
	// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
	// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
	// USE OR OTHER DEALINGS IN THE SOFTWARE.

	'use strict';

	var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };

	var stringifyPrimitive = function stringifyPrimitive(v) {
	  switch (typeof v === 'undefined' ? 'undefined' : _typeof(v)) {
	    case 'string':
	      return v;

	    case 'boolean':
	      return v ? 'true' : 'false';

	    case 'number':
	      return isFinite(v) ? v : '';

	    default:
	      return '';
	  }
	};

	module.exports = function (obj, sep, eq, name) {
	  sep = sep || '&';
	  eq = eq || '=';
	  if (obj === null) {
	    obj = undefined;
	  }

	  if ((typeof obj === 'undefined' ? 'undefined' : _typeof(obj)) === 'object') {
	    return map(objectKeys(obj), function (k) {
	      var ks = encodeURIComponent(stringifyPrimitive(k)) + eq;
	      if (isArray(obj[k])) {
	        return map(obj[k], function (v) {
	          return ks + encodeURIComponent(stringifyPrimitive(v));
	        }).join(sep);
	      } else {
	        return ks + encodeURIComponent(stringifyPrimitive(obj[k]));
	      }
	    }).join(sep);
	  }

	  if (!name) return '';
	  return encodeURIComponent(stringifyPrimitive(name)) + eq + encodeURIComponent(stringifyPrimitive(obj));
	};

	var isArray = Array.isArray || function (xs) {
	  return Object.prototype.toString.call(xs) === '[object Array]';
	};

	function map(xs, f) {
	  if (xs.map) return xs.map(f);
	  var res = [];
	  for (var i = 0; i < xs.length; i++) {
	    res.push(f(xs[i], i));
	  }
	  return res;
	}

	var objectKeys = Object.keys || function (obj) {
	  var res = [];
	  for (var key in obj) {
	    if (Object.prototype.hasOwnProperty.call(obj, key)) res.push(key);
	  }
	  return res;
	};

/***/ },
/* 72 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	exports.decode = exports.parse = __webpack_require__(70);
	exports.encode = exports.stringify = __webpack_require__(71);

/***/ },
/* 73 */
/***/ function(module, exports) {

	"use strict";
	'use strict';

	var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };

	module.exports = {
	  isString: function isString(arg) {
	    return typeof arg === 'string';
	  },
	  isObject: function isObject(arg) {
	    return (typeof arg === 'undefined' ? 'undefined' : _typeof(arg)) === 'object' && arg !== null;
	  },
	  isNull: function isNull(arg) {
	    return arg === null;
	  },
	  isNullOrUndefined: function isNullOrUndefined(arg) {
	    return arg == null;
	  }
	};

/***/ },
/* 74 */
/***/ function(module, exports) {

	"use strict";
	'use strict';

	var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };

	module.exports = function isBuffer(arg) {
	  return arg && (typeof arg === 'undefined' ? 'undefined' : _typeof(arg)) === 'object' && typeof arg.copy === 'function' && typeof arg.fill === 'function' && typeof arg.readUInt8 === 'function';
	};

/***/ },
/* 75 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _toDate = __webpack_require__(7);

	var _toDate2 = _interopRequireDefault(_toDate);

	var _toFloat = __webpack_require__(20);

	var _toFloat2 = _interopRequireDefault(_toFloat);

	var _toInt = __webpack_require__(119);

	var _toInt2 = _interopRequireDefault(_toInt);

	var _toBoolean = __webpack_require__(118);

	var _toBoolean2 = _interopRequireDefault(_toBoolean);

	var _equals = __webpack_require__(77);

	var _equals2 = _interopRequireDefault(_equals);

	var _contains = __webpack_require__(76);

	var _contains2 = _interopRequireDefault(_contains);

	var _matches = __webpack_require__(114);

	var _matches2 = _interopRequireDefault(_matches);

	var _isEmail = __webpack_require__(14);

	var _isEmail2 = _interopRequireDefault(_isEmail);

	var _isURL = __webpack_require__(108);

	var _isURL2 = _interopRequireDefault(_isURL);

	var _isMACAddress = __webpack_require__(101);

	var _isMACAddress2 = _interopRequireDefault(_isMACAddress);

	var _isIP = __webpack_require__(18);

	var _isIP2 = _interopRequireDefault(_isIP);

	var _isFQDN = __webpack_require__(6);

	var _isFQDN2 = _interopRequireDefault(_isFQDN);

	var _isBoolean = __webpack_require__(85);

	var _isBoolean2 = _interopRequireDefault(_isBoolean);

	var _isAlpha = __webpack_require__(80);

	var _isAlpha2 = _interopRequireDefault(_isAlpha);

	var _isAlphanumeric = __webpack_require__(81);

	var _isAlphanumeric2 = _interopRequireDefault(_isAlphanumeric);

	var _isNumeric = __webpack_require__(106);

	var _isNumeric2 = _interopRequireDefault(_isNumeric);

	var _isLowercase = __webpack_require__(100);

	var _isLowercase2 = _interopRequireDefault(_isLowercase);

	var _isUppercase = __webpack_require__(110);

	var _isUppercase2 = _interopRequireDefault(_isUppercase);

	var _isAscii = __webpack_require__(82);

	var _isAscii2 = _interopRequireDefault(_isAscii);

	var _isFullWidth = __webpack_require__(15);

	var _isFullWidth2 = _interopRequireDefault(_isFullWidth);

	var _isHalfWidth = __webpack_require__(16);

	var _isHalfWidth2 = _interopRequireDefault(_isHalfWidth);

	var _isVariableWidth = __webpack_require__(111);

	var _isVariableWidth2 = _interopRequireDefault(_isVariableWidth);

	var _isMultibyte = __webpack_require__(104);

	var _isMultibyte2 = _interopRequireDefault(_isMultibyte);

	var _isSurrogatePair = __webpack_require__(107);

	var _isSurrogatePair2 = _interopRequireDefault(_isSurrogatePair);

	var _isInt = __webpack_require__(97);

	var _isInt2 = _interopRequireDefault(_isInt);

	var _isFloat = __webpack_require__(92);

	var _isFloat2 = _interopRequireDefault(_isFloat);

	var _isDecimal = __webpack_require__(90);

	var _isDecimal2 = _interopRequireDefault(_isDecimal);

	var _isHexadecimal = __webpack_require__(17);

	var _isHexadecimal2 = _interopRequireDefault(_isHexadecimal);

	var _isDivisibleBy = __webpack_require__(91);

	var _isDivisibleBy2 = _interopRequireDefault(_isDivisibleBy);

	var _isHexColor = __webpack_require__(93);

	var _isHexColor2 = _interopRequireDefault(_isHexColor);

	var _isJSON = __webpack_require__(98);

	var _isJSON2 = _interopRequireDefault(_isJSON);

	var _isNull = __webpack_require__(105);

	var _isNull2 = _interopRequireDefault(_isNull);

	var _isLength = __webpack_require__(99);

	var _isLength2 = _interopRequireDefault(_isLength);

	var _isByteLength = __webpack_require__(13);

	var _isByteLength2 = _interopRequireDefault(_isByteLength);

	var _isUUID = __webpack_require__(109);

	var _isUUID2 = _interopRequireDefault(_isUUID);

	var _isMongoId = __webpack_require__(103);

	var _isMongoId2 = _interopRequireDefault(_isMongoId);

	var _isDate = __webpack_require__(89);

	var _isDate2 = _interopRequireDefault(_isDate);

	var _isAfter = __webpack_require__(79);

	var _isAfter2 = _interopRequireDefault(_isAfter);

	var _isBefore = __webpack_require__(84);

	var _isBefore2 = _interopRequireDefault(_isBefore);

	var _isIn = __webpack_require__(96);

	var _isIn2 = _interopRequireDefault(_isIn);

	var _isCreditCard = __webpack_require__(86);

	var _isCreditCard2 = _interopRequireDefault(_isCreditCard);

	var _isISIN = __webpack_require__(95);

	var _isISIN2 = _interopRequireDefault(_isISIN);

	var _isISBN = __webpack_require__(94);

	var _isISBN2 = _interopRequireDefault(_isISBN);

	var _isMobilePhone = __webpack_require__(102);

	var _isMobilePhone2 = _interopRequireDefault(_isMobilePhone);

	var _isCurrency = __webpack_require__(87);

	var _isCurrency2 = _interopRequireDefault(_isCurrency);

	var _isISO = __webpack_require__(19);

	var _isISO2 = _interopRequireDefault(_isISO);

	var _isBase = __webpack_require__(83);

	var _isBase2 = _interopRequireDefault(_isBase);

	var _isDataURI = __webpack_require__(88);

	var _isDataURI2 = _interopRequireDefault(_isDataURI);

	var _ltrim = __webpack_require__(113);

	var _ltrim2 = _interopRequireDefault(_ltrim);

	var _rtrim = __webpack_require__(116);

	var _rtrim2 = _interopRequireDefault(_rtrim);

	var _trim = __webpack_require__(120);

	var _trim2 = _interopRequireDefault(_trim);

	var _escape = __webpack_require__(78);

	var _escape2 = _interopRequireDefault(_escape);

	var _unescape = __webpack_require__(121);

	var _unescape2 = _interopRequireDefault(_unescape);

	var _stripLow = __webpack_require__(117);

	var _stripLow2 = _interopRequireDefault(_stripLow);

	var _whitelist = __webpack_require__(122);

	var _whitelist2 = _interopRequireDefault(_whitelist);

	var _blacklist = __webpack_require__(12);

	var _blacklist2 = _interopRequireDefault(_blacklist);

	var _isWhitelisted = __webpack_require__(112);

	var _isWhitelisted2 = _interopRequireDefault(_isWhitelisted);

	var _normalizeEmail = __webpack_require__(115);

	var _normalizeEmail2 = _interopRequireDefault(_normalizeEmail);

	var _toString = __webpack_require__(8);

	var _toString2 = _interopRequireDefault(_toString);

	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { default: obj };
	}

	var version = '5.4.0';

	var validator = {
	  version: version,
	  toDate: _toDate2.default,
	  toFloat: _toFloat2.default, toInt: _toInt2.default,
	  toBoolean: _toBoolean2.default,
	  equals: _equals2.default, contains: _contains2.default, matches: _matches2.default,
	  isEmail: _isEmail2.default, isURL: _isURL2.default, isMACAddress: _isMACAddress2.default, isIP: _isIP2.default, isFQDN: _isFQDN2.default,
	  isBoolean: _isBoolean2.default,
	  isAlpha: _isAlpha2.default, isAlphanumeric: _isAlphanumeric2.default, isNumeric: _isNumeric2.default, isLowercase: _isLowercase2.default, isUppercase: _isUppercase2.default,
	  isAscii: _isAscii2.default, isFullWidth: _isFullWidth2.default, isHalfWidth: _isHalfWidth2.default, isVariableWidth: _isVariableWidth2.default,
	  isMultibyte: _isMultibyte2.default, isSurrogatePair: _isSurrogatePair2.default,
	  isInt: _isInt2.default, isFloat: _isFloat2.default, isDecimal: _isDecimal2.default, isHexadecimal: _isHexadecimal2.default, isDivisibleBy: _isDivisibleBy2.default,
	  isHexColor: _isHexColor2.default,
	  isJSON: _isJSON2.default,
	  isNull: _isNull2.default,
	  isLength: _isLength2.default, isByteLength: _isByteLength2.default,
	  isUUID: _isUUID2.default, isMongoId: _isMongoId2.default,
	  isDate: _isDate2.default, isAfter: _isAfter2.default, isBefore: _isBefore2.default,
	  isIn: _isIn2.default,
	  isCreditCard: _isCreditCard2.default,
	  isISIN: _isISIN2.default, isISBN: _isISBN2.default,
	  isMobilePhone: _isMobilePhone2.default,
	  isCurrency: _isCurrency2.default,
	  isISO8601: _isISO2.default,
	  isBase64: _isBase2.default, isDataURI: _isDataURI2.default,
	  ltrim: _ltrim2.default, rtrim: _rtrim2.default, trim: _trim2.default,
	  escape: _escape2.default, unescape: _unescape2.default, stripLow: _stripLow2.default,
	  whitelist: _whitelist2.default, blacklist: _blacklist2.default,
	  isWhitelisted: _isWhitelisted2.default,
	  normalizeEmail: _normalizeEmail2.default,
	  toString: _toString2.default
	};

	exports.default = validator;
	module.exports = exports['default'];

/***/ },
/* 76 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = contains;

	var _assertString = __webpack_require__(0);

	var _assertString2 = _interopRequireDefault(_assertString);

	var _toString = __webpack_require__(8);

	var _toString2 = _interopRequireDefault(_toString);

	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { default: obj };
	}

	function contains(str, elem) {
	  (0, _assertString2.default)(str);
	  return str.indexOf((0, _toString2.default)(elem)) >= 0;
	}
	module.exports = exports['default'];

/***/ },
/* 77 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = equals;

	var _assertString = __webpack_require__(0);

	var _assertString2 = _interopRequireDefault(_assertString);

	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { default: obj };
	}

	function equals(str, comparison) {
	  (0, _assertString2.default)(str);
	  return str === comparison;
	}
	module.exports = exports['default'];

/***/ },
/* 78 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	Object.defineProperty(exports, "__esModule", {
	      value: true
	});
	exports.default = escape;

	var _assertString = __webpack_require__(0);

	var _assertString2 = _interopRequireDefault(_assertString);

	function _interopRequireDefault(obj) {
	      return obj && obj.__esModule ? obj : { default: obj };
	}

	function escape(str) {
	      (0, _assertString2.default)(str);
	      return str.replace(/&/g, '&amp;').replace(/"/g, '&quot;').replace(/'/g, '&#x27;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/\//g, '&#x2F;').replace(/\`/g, '&#96;');
	}
	module.exports = exports['default'];

/***/ },
/* 79 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = isAfter;

	var _assertString = __webpack_require__(0);

	var _assertString2 = _interopRequireDefault(_assertString);

	var _toDate = __webpack_require__(7);

	var _toDate2 = _interopRequireDefault(_toDate);

	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { default: obj };
	}

	function isAfter(str) {
	  var date = arguments.length <= 1 || arguments[1] === undefined ? String(new Date()) : arguments[1];

	  (0, _assertString2.default)(str);
	  var comparison = (0, _toDate2.default)(date);
	  var original = (0, _toDate2.default)(str);
	  return !!(original && comparison && original > comparison);
	}
	module.exports = exports['default'];

/***/ },
/* 80 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = isAlpha;

	var _assertString = __webpack_require__(0);

	var _assertString2 = _interopRequireDefault(_assertString);

	var _alpha = __webpack_require__(11);

	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { default: obj };
	}

	function isAlpha(str) {
	  var locale = arguments.length <= 1 || arguments[1] === undefined ? 'en-US' : arguments[1];

	  (0, _assertString2.default)(str);
	  if (locale in _alpha.alpha) {
	    return _alpha.alpha[locale].test(str);
	  }
	  throw new Error('Invalid locale \'' + locale + '\'');
	}
	module.exports = exports['default'];

/***/ },
/* 81 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = isAlphanumeric;

	var _assertString = __webpack_require__(0);

	var _assertString2 = _interopRequireDefault(_assertString);

	var _alpha = __webpack_require__(11);

	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { default: obj };
	}

	function isAlphanumeric(str) {
	  var locale = arguments.length <= 1 || arguments[1] === undefined ? 'en-US' : arguments[1];

	  (0, _assertString2.default)(str);
	  if (locale in _alpha.alphanumeric) {
	    return _alpha.alphanumeric[locale].test(str);
	  }
	  throw new Error('Invalid locale \'' + locale + '\'');
	}
	module.exports = exports['default'];

/***/ },
/* 82 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = isAscii;

	var _assertString = __webpack_require__(0);

	var _assertString2 = _interopRequireDefault(_assertString);

	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { default: obj };
	}

	/* eslint-disable no-control-regex */
	var ascii = /^[\x00-\x7F]+$/;
	/* eslint-enable no-control-regex */

	function isAscii(str) {
	  (0, _assertString2.default)(str);
	  return ascii.test(str);
	}
	module.exports = exports['default'];

/***/ },
/* 83 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = isBase64;

	var _assertString = __webpack_require__(0);

	var _assertString2 = _interopRequireDefault(_assertString);

	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { default: obj };
	}

	var notBase64 = /[^A-Z0-9+\/=]/i;

	function isBase64(str) {
	  (0, _assertString2.default)(str);
	  var len = str.length;
	  if (!len || len % 4 !== 0 || notBase64.test(str)) {
	    return false;
	  }
	  var firstPaddingChar = str.indexOf('=');
	  return firstPaddingChar === -1 || firstPaddingChar === len - 1 || firstPaddingChar === len - 2 && str[len - 1] === '=';
	}
	module.exports = exports['default'];

/***/ },
/* 84 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = isBefore;

	var _assertString = __webpack_require__(0);

	var _assertString2 = _interopRequireDefault(_assertString);

	var _toDate = __webpack_require__(7);

	var _toDate2 = _interopRequireDefault(_toDate);

	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { default: obj };
	}

	function isBefore(str) {
	  var date = arguments.length <= 1 || arguments[1] === undefined ? String(new Date()) : arguments[1];

	  (0, _assertString2.default)(str);
	  var comparison = (0, _toDate2.default)(date);
	  var original = (0, _toDate2.default)(str);
	  return !!(original && comparison && original < comparison);
	}
	module.exports = exports['default'];

/***/ },
/* 85 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = isBoolean;

	var _assertString = __webpack_require__(0);

	var _assertString2 = _interopRequireDefault(_assertString);

	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { default: obj };
	}

	function isBoolean(str) {
	  (0, _assertString2.default)(str);
	  return ['true', 'false', '1', '0'].indexOf(str) >= 0;
	}
	module.exports = exports['default'];

/***/ },
/* 86 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = isCreditCard;

	var _assertString = __webpack_require__(0);

	var _assertString2 = _interopRequireDefault(_assertString);

	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { default: obj };
	}

	/* eslint-disable max-len */
	var creditCard = /^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})|62[0-9]{14}$/;
	/* eslint-enable max-len */

	function isCreditCard(str) {
	  (0, _assertString2.default)(str);
	  var sanitized = str.replace(/[^0-9]+/g, '');
	  if (!creditCard.test(sanitized)) {
	    return false;
	  }
	  var sum = 0;
	  var digit = void 0;
	  var tmpNum = void 0;
	  var shouldDouble = void 0;
	  for (var i = sanitized.length - 1; i >= 0; i--) {
	    digit = sanitized.substring(i, i + 1);
	    tmpNum = parseInt(digit, 10);
	    if (shouldDouble) {
	      tmpNum *= 2;
	      if (tmpNum >= 10) {
	        sum += tmpNum % 10 + 1;
	      } else {
	        sum += tmpNum;
	      }
	    } else {
	      sum += tmpNum;
	    }
	    shouldDouble = !shouldDouble;
	  }
	  return !!(sum % 10 === 0 ? sanitized : false);
	}
	module.exports = exports['default'];

/***/ },
/* 87 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = isCurrency;

	var _merge = __webpack_require__(5);

	var _merge2 = _interopRequireDefault(_merge);

	var _assertString = __webpack_require__(0);

	var _assertString2 = _interopRequireDefault(_assertString);

	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { default: obj };
	}

	function currencyRegex(options) {
	  var symbol = '(\\' + options.symbol.replace(/\./g, '\\.') + ')' + (options.require_symbol ? '' : '?'),
	      negative = '-?',
	      whole_dollar_amount_without_sep = '[1-9]\\d*',
	      whole_dollar_amount_with_sep = '[1-9]\\d{0,2}(\\' + options.thousands_separator + '\\d{3})*',
	      valid_whole_dollar_amounts = ['0', whole_dollar_amount_without_sep, whole_dollar_amount_with_sep],
	      whole_dollar_amount = '(' + valid_whole_dollar_amounts.join('|') + ')?',
	      decimal_amount = '(\\' + options.decimal_separator + '\\d{2})?';
	  var pattern = whole_dollar_amount + decimal_amount;

	  // default is negative sign before symbol, but there are two other options (besides parens)
	  if (options.allow_negatives && !options.parens_for_negatives) {
	    if (options.negative_sign_after_digits) {
	      pattern += negative;
	    } else if (options.negative_sign_before_digits) {
	      pattern = negative + pattern;
	    }
	  }

	  // South African Rand, for example, uses R 123 (space) and R-123 (no space)
	  if (options.allow_negative_sign_placeholder) {
	    pattern = '( (?!\\-))?' + pattern;
	  } else if (options.allow_space_after_symbol) {
	    pattern = ' ?' + pattern;
	  } else if (options.allow_space_after_digits) {
	    pattern += '( (?!$))?';
	  }

	  if (options.symbol_after_digits) {
	    pattern += symbol;
	  } else {
	    pattern = symbol + pattern;
	  }

	  if (options.allow_negatives) {
	    if (options.parens_for_negatives) {
	      pattern = '(\\(' + pattern + '\\)|' + pattern + ')';
	    } else if (!(options.negative_sign_before_digits || options.negative_sign_after_digits)) {
	      pattern = negative + pattern;
	    }
	  }

	  /* eslint-disable prefer-template */
	  return new RegExp('^' +
	  // ensure there's a dollar and/or decimal amount, and that
	  // it doesn't start with a space or a negative sign followed by a space
	  '(?!-? )(?=.*\\d)' + pattern + '$');
	  /* eslint-enable prefer-template */
	}

	var default_currency_options = {
	  symbol: '$',
	  require_symbol: false,
	  allow_space_after_symbol: false,
	  symbol_after_digits: false,
	  allow_negatives: true,
	  parens_for_negatives: false,
	  negative_sign_before_digits: false,
	  negative_sign_after_digits: false,
	  allow_negative_sign_placeholder: false,
	  thousands_separator: ',',
	  decimal_separator: '.',
	  allow_space_after_digits: false
	};

	function isCurrency(str, options) {
	  (0, _assertString2.default)(str);
	  options = (0, _merge2.default)(options, default_currency_options);
	  return currencyRegex(options).test(str);
	}
	module.exports = exports['default'];

/***/ },
/* 88 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = isDataURI;

	var _assertString = __webpack_require__(0);

	var _assertString2 = _interopRequireDefault(_assertString);

	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { default: obj };
	}

	var dataURI = /^\s*data:([a-z]+\/[a-z0-9\-\+]+(;[a-z\-]+\=[a-z0-9\-]+)?)?(;base64)?,[a-z0-9\!\$\&\'\,\(\)\*\+\,\;\=\-\.\_\~\:\@\/\?\%\s]*\s*$/i; // eslint-disable-line max-len

	function isDataURI(str) {
	  (0, _assertString2.default)(str);
	  return dataURI.test(str);
	}
	module.exports = exports['default'];

/***/ },
/* 89 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = isDate;

	var _assertString = __webpack_require__(0);

	var _assertString2 = _interopRequireDefault(_assertString);

	var _isISO = __webpack_require__(19);

	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { default: obj };
	}

	function getTimezoneOffset(str) {
	  var iso8601Parts = str.match(_isISO.iso8601);
	  var timezone = void 0,
	      sign = void 0,
	      hours = void 0,
	      minutes = void 0;
	  if (!iso8601Parts) {
	    str = str.toLowerCase();
	    timezone = str.match(/(?:\s|gmt\s*)(-|\+)(\d{1,4})(\s|$)/);
	    if (!timezone) {
	      return str.indexOf('gmt') !== -1 ? 0 : null;
	    }
	    sign = timezone[1];
	    var offset = timezone[2];
	    if (offset.length === 3) {
	      offset = '0' + offset;
	    }
	    if (offset.length <= 2) {
	      hours = 0;
	      minutes = parseInt(offset, 10);
	    } else {
	      hours = parseInt(offset.slice(0, 2), 10);
	      minutes = parseInt(offset.slice(2, 4), 10);
	    }
	  } else {
	    timezone = iso8601Parts[21];
	    if (!timezone) {
	      // if no hour/minute was provided, the date is GMT
	      return !iso8601Parts[12] ? 0 : null;
	    }
	    if (timezone === 'z' || timezone === 'Z') {
	      return 0;
	    }
	    sign = iso8601Parts[22];
	    if (timezone.indexOf(':') !== -1) {
	      hours = parseInt(iso8601Parts[23], 10);
	      minutes = parseInt(iso8601Parts[24], 10);
	    } else {
	      hours = 0;
	      minutes = parseInt(iso8601Parts[23], 10);
	    }
	  }
	  return (hours * 60 + minutes) * (sign === '-' ? 1 : -1);
	}

	function isDate(str) {
	  (0, _assertString2.default)(str);
	  var normalizedDate = new Date(Date.parse(str));
	  if (isNaN(normalizedDate)) {
	    return false;
	  }

	  // normalizedDate is in the user's timezone. Apply the input
	  // timezone offset to the date so that the year and day match
	  // the input
	  var timezoneOffset = getTimezoneOffset(str);
	  if (timezoneOffset !== null) {
	    var timezoneDifference = normalizedDate.getTimezoneOffset() - timezoneOffset;
	    normalizedDate = new Date(normalizedDate.getTime() + 60000 * timezoneDifference);
	  }

	  var day = String(normalizedDate.getDate());
	  var dayOrYear = void 0,
	      dayOrYearMatches = void 0,
	      year = void 0;
	  // check for valid double digits that could be late days
	  // check for all matches since a string like '12/23' is a valid date
	  // ignore everything with nearby colons
	  dayOrYearMatches = str.match(/(^|[^:\d])[23]\d([^:\d]|$)/g);
	  if (!dayOrYearMatches) {
	    return true;
	  }
	  dayOrYear = dayOrYearMatches.map(function (digitString) {
	    return digitString.match(/\d+/g)[0];
	  }).join('/');

	  year = String(normalizedDate.getFullYear()).slice(-2);
	  if (dayOrYear === day || dayOrYear === year) {
	    return true;
	  } else if (dayOrYear === '' + day / year || dayOrYear === '' + year / day) {
	    return true;
	  }
	  return false;
	}
	module.exports = exports['default'];

/***/ },
/* 90 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = isDecimal;

	var _assertString = __webpack_require__(0);

	var _assertString2 = _interopRequireDefault(_assertString);

	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { default: obj };
	}

	var decimal = /^[-+]?([0-9]+|\.[0-9]+|[0-9]+\.[0-9]+)$/;

	function isDecimal(str) {
	  (0, _assertString2.default)(str);
	  return str !== '' && decimal.test(str);
	}
	module.exports = exports['default'];

/***/ },
/* 91 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = isDivisibleBy;

	var _assertString = __webpack_require__(0);

	var _assertString2 = _interopRequireDefault(_assertString);

	var _toFloat = __webpack_require__(20);

	var _toFloat2 = _interopRequireDefault(_toFloat);

	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { default: obj };
	}

	function isDivisibleBy(str, num) {
	  (0, _assertString2.default)(str);
	  return (0, _toFloat2.default)(str) % parseInt(num, 10) === 0;
	}
	module.exports = exports['default'];

/***/ },
/* 92 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = isFloat;

	var _assertString = __webpack_require__(0);

	var _assertString2 = _interopRequireDefault(_assertString);

	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { default: obj };
	}

	var float = /^(?:[-+]?(?:[0-9]+))?(?:\.[0-9]*)?(?:[eE][\+\-]?(?:[0-9]+))?$/;

	function isFloat(str, options) {
	  (0, _assertString2.default)(str);
	  options = options || {};
	  if (str === '' || str === '.') {
	    return false;
	  }
	  return float.test(str) && (!options.hasOwnProperty('min') || str >= options.min) && (!options.hasOwnProperty('max') || str <= options.max);
	}
	module.exports = exports['default'];

/***/ },
/* 93 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = isHexColor;

	var _assertString = __webpack_require__(0);

	var _assertString2 = _interopRequireDefault(_assertString);

	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { default: obj };
	}

	var hexcolor = /^#?([0-9A-F]{3}|[0-9A-F]{6})$/i;

	function isHexColor(str) {
	  (0, _assertString2.default)(str);
	  return hexcolor.test(str);
	}
	module.exports = exports['default'];

/***/ },
/* 94 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = isISBN;

	var _assertString = __webpack_require__(0);

	var _assertString2 = _interopRequireDefault(_assertString);

	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { default: obj };
	}

	var isbn10Maybe = /^(?:[0-9]{9}X|[0-9]{10})$/;
	var isbn13Maybe = /^(?:[0-9]{13})$/;
	var factor = [1, 3];

	function isISBN(str) {
	  var version = arguments.length <= 1 || arguments[1] === undefined ? '' : arguments[1];

	  (0, _assertString2.default)(str);
	  version = String(version);
	  if (!version) {
	    return isISBN(str, 10) || isISBN(str, 13);
	  }
	  var sanitized = str.replace(/[\s-]+/g, '');
	  var checksum = 0;
	  var i = void 0;
	  if (version === '10') {
	    if (!isbn10Maybe.test(sanitized)) {
	      return false;
	    }
	    for (i = 0; i < 9; i++) {
	      checksum += (i + 1) * sanitized.charAt(i);
	    }
	    if (sanitized.charAt(9) === 'X') {
	      checksum += 10 * 10;
	    } else {
	      checksum += 10 * sanitized.charAt(9);
	    }
	    if (checksum % 11 === 0) {
	      return !!sanitized;
	    }
	  } else if (version === '13') {
	    if (!isbn13Maybe.test(sanitized)) {
	      return false;
	    }
	    for (i = 0; i < 12; i++) {
	      checksum += factor[i % 2] * sanitized.charAt(i);
	    }
	    if (sanitized.charAt(12) - (10 - checksum % 10) % 10 === 0) {
	      return !!sanitized;
	    }
	  }
	  return false;
	}
	module.exports = exports['default'];

/***/ },
/* 95 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = isISIN;

	var _assertString = __webpack_require__(0);

	var _assertString2 = _interopRequireDefault(_assertString);

	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { default: obj };
	}

	var isin = /^[A-Z]{2}[0-9A-Z]{9}[0-9]$/;

	function isISIN(str) {
	  (0, _assertString2.default)(str);
	  if (!isin.test(str)) {
	    return false;
	  }

	  var checksumStr = str.replace(/[A-Z]/g, function (character) {
	    return parseInt(character, 36);
	  });

	  var sum = 0;
	  var digit = void 0;
	  var tmpNum = void 0;
	  var shouldDouble = true;
	  for (var i = checksumStr.length - 2; i >= 0; i--) {
	    digit = checksumStr.substring(i, i + 1);
	    tmpNum = parseInt(digit, 10);
	    if (shouldDouble) {
	      tmpNum *= 2;
	      if (tmpNum >= 10) {
	        sum += tmpNum + 1;
	      } else {
	        sum += tmpNum;
	      }
	    } else {
	      sum += tmpNum;
	    }
	    shouldDouble = !shouldDouble;
	  }

	  return parseInt(str.substr(str.length - 1), 10) === (10000 - sum) % 10;
	}
	module.exports = exports['default'];

/***/ },
/* 96 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	var _typeof2 = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _typeof = typeof Symbol === "function" && _typeof2(Symbol.iterator) === "symbol" ? function (obj) {
	  return typeof obj === "undefined" ? "undefined" : _typeof2(obj);
	} : function (obj) {
	  return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj === "undefined" ? "undefined" : _typeof2(obj);
	};

	exports.default = isIn;

	var _assertString = __webpack_require__(0);

	var _assertString2 = _interopRequireDefault(_assertString);

	var _toString = __webpack_require__(8);

	var _toString2 = _interopRequireDefault(_toString);

	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { default: obj };
	}

	function isIn(str, options) {
	  (0, _assertString2.default)(str);
	  var i = void 0;
	  if (Object.prototype.toString.call(options) === '[object Array]') {
	    var array = [];
	    for (i in options) {
	      if ({}.hasOwnProperty.call(options, i)) {
	        array[i] = (0, _toString2.default)(options[i]);
	      }
	    }
	    return array.indexOf(str) >= 0;
	  } else if ((typeof options === 'undefined' ? 'undefined' : _typeof(options)) === 'object') {
	    return options.hasOwnProperty(str);
	  } else if (options && typeof options.indexOf === 'function') {
	    return options.indexOf(str) >= 0;
	  }
	  return false;
	}
	module.exports = exports['default'];

/***/ },
/* 97 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = isInt;

	var _assertString = __webpack_require__(0);

	var _assertString2 = _interopRequireDefault(_assertString);

	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { default: obj };
	}

	var int = /^(?:[-+]?(?:0|[1-9][0-9]*))$/;
	var intLeadingZeroes = /^[-+]?[0-9]+$/;

	function isInt(str, options) {
	  (0, _assertString2.default)(str);
	  options = options || {};

	  // Get the regex to use for testing, based on whether
	  // leading zeroes are allowed or not.
	  var regex = options.hasOwnProperty('allow_leading_zeroes') && options.allow_leading_zeroes ? intLeadingZeroes : int;

	  // Check min/max
	  var minCheckPassed = !options.hasOwnProperty('min') || str >= options.min;
	  var maxCheckPassed = !options.hasOwnProperty('max') || str <= options.max;

	  return regex.test(str) && minCheckPassed && maxCheckPassed;
	}
	module.exports = exports['default'];

/***/ },
/* 98 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	var _typeof2 = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _typeof = typeof Symbol === "function" && _typeof2(Symbol.iterator) === "symbol" ? function (obj) {
	  return typeof obj === "undefined" ? "undefined" : _typeof2(obj);
	} : function (obj) {
	  return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj === "undefined" ? "undefined" : _typeof2(obj);
	};

	exports.default = isJSON;

	var _assertString = __webpack_require__(0);

	var _assertString2 = _interopRequireDefault(_assertString);

	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { default: obj };
	}

	function isJSON(str) {
	  (0, _assertString2.default)(str);
	  try {
	    var obj = JSON.parse(str);
	    return !!obj && (typeof obj === 'undefined' ? 'undefined' : _typeof(obj)) === 'object';
	  } catch (e) {/* ignore */}
	  return false;
	}
	module.exports = exports['default'];

/***/ },
/* 99 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	var _typeof2 = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _typeof = typeof Symbol === "function" && _typeof2(Symbol.iterator) === "symbol" ? function (obj) {
	  return typeof obj === "undefined" ? "undefined" : _typeof2(obj);
	} : function (obj) {
	  return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj === "undefined" ? "undefined" : _typeof2(obj);
	};

	exports.default = isLength;

	var _assertString = __webpack_require__(0);

	var _assertString2 = _interopRequireDefault(_assertString);

	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { default: obj };
	}

	/* eslint-disable prefer-rest-params */
	function isLength(str, options) {
	  (0, _assertString2.default)(str);
	  var min = void 0;
	  var max = void 0;
	  if ((typeof options === 'undefined' ? 'undefined' : _typeof(options)) === 'object') {
	    min = options.min || 0;
	    max = options.max;
	  } else {
	    // backwards compatibility: isLength(str, min [, max])
	    min = arguments[1];
	    max = arguments[2];
	  }
	  var surrogatePairs = str.match(/[\uD800-\uDBFF][\uDC00-\uDFFF]/g) || [];
	  var len = str.length - surrogatePairs.length;
	  return len >= min && (typeof max === 'undefined' || len <= max);
	}
	module.exports = exports['default'];

/***/ },
/* 100 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = isLowercase;

	var _assertString = __webpack_require__(0);

	var _assertString2 = _interopRequireDefault(_assertString);

	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { default: obj };
	}

	function isLowercase(str) {
	  (0, _assertString2.default)(str);
	  return str === str.toLowerCase();
	}
	module.exports = exports['default'];

/***/ },
/* 101 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = isMACAddress;

	var _assertString = __webpack_require__(0);

	var _assertString2 = _interopRequireDefault(_assertString);

	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { default: obj };
	}

	var macAddress = /^([0-9a-fA-F][0-9a-fA-F]:){5}([0-9a-fA-F][0-9a-fA-F])$/;

	function isMACAddress(str) {
	  (0, _assertString2.default)(str);
	  return macAddress.test(str);
	}
	module.exports = exports['default'];

/***/ },
/* 102 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = isMobilePhone;

	var _assertString = __webpack_require__(0);

	var _assertString2 = _interopRequireDefault(_assertString);

	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { default: obj };
	}

	/* eslint-disable max-len */
	var phones = {
	  'ar-SY': /^(!?(\+?963)|0)?9\d{8}$/,
	  'en-US': /^(\+?1)?[2-9]\d{2}[2-9](?!11)\d{6}$/,
	  'cs-CZ': /^(\+?420)? ?[1-9][0-9]{2} ?[0-9]{3} ?[0-9]{3}$/,
	  'de-DE': /^(\+?49[ \.\-])?([\(]{1}[0-9]{1,6}[\)])?([0-9 \.\-\/]{3,20})((x|ext|extension)[ ]?[0-9]{1,4})?$/,
	  'da-DK': /^(\+?45)?(\d{8})$/,
	  'el-GR': /^(\+?30)?(69\d{8})$/,
	  'en-AU': /^(\+?61|0)4\d{8}$/,
	  'en-GB': /^(\+?44|0)7\d{9}$/,
	  'en-HK': /^(\+?852\-?)?[569]\d{3}\-?\d{4}$/,
	  'en-IN': /^(\+?91|0)?[789]\d{9}$/,
	  'en-NZ': /^(\+?64|0)2\d{7,9}$/,
	  'en-ZA': /^(\+?27|0)\d{9}$/,
	  'en-ZM': /^(\+?26)?09[567]\d{7}$/,
	  'es-ES': /^(\+?34)?(6\d{1}|7[1234])\d{7}$/,
	  'fi-FI': /^(\+?358|0)\s?(4(0|1|2|4|5)?|50)\s?(\d\s?){4,8}\d$/,
	  'fr-FR': /^(\+?33|0)[67]\d{8}$/,
	  'hu-HU': /^(\+?36)(20|30|70)\d{7}$/,
	  'ms-MY': /^(\+?6?01){1}(([145]{1}(\-|\s)?\d{7,8})|([236789]{1}(\s|\-)?\d{7}))$/,
	  'nb-NO': /^(\+?47)?[49]\d{7}$/,
	  'nn-NO': /^(\+?47)?[49]\d{7}$/,
	  'pt-BR': /^(\+?55|0)\-?[1-9]{2}\-?[2-9]{1}\d{3,4}\-?\d{4}$/,
	  'pt-PT': /^(\+?351)?9[1236]\d{7}$/,
	  'ru-RU': /^(\+?7|8)?9\d{9}$/,
	  'tr-TR': /^(\+?90|0)?5\d{9}$/,
	  'vi-VN': /^(\+?84|0)?((1(2([0-9])|6([2-9])|88|99))|(9((?!5)[0-9])))([0-9]{7})$/,
	  'zh-CN': /^(\+?0?86\-?)?1[345789]\d{9}$/,
	  'zh-TW': /^(\+?886\-?|0)?9\d{8}$/
	};
	/* eslint-enable max-len */

	// aliases
	phones['en-CA'] = phones['en-US'];

	function isMobilePhone(str, locale) {
	  (0, _assertString2.default)(str);
	  if (locale in phones) {
	    return phones[locale].test(str);
	  }
	  return false;
	}
	module.exports = exports['default'];

/***/ },
/* 103 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = isMongoId;

	var _assertString = __webpack_require__(0);

	var _assertString2 = _interopRequireDefault(_assertString);

	var _isHexadecimal = __webpack_require__(17);

	var _isHexadecimal2 = _interopRequireDefault(_isHexadecimal);

	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { default: obj };
	}

	function isMongoId(str) {
	  (0, _assertString2.default)(str);
	  return (0, _isHexadecimal2.default)(str) && str.length === 24;
	}
	module.exports = exports['default'];

/***/ },
/* 104 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = isMultibyte;

	var _assertString = __webpack_require__(0);

	var _assertString2 = _interopRequireDefault(_assertString);

	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { default: obj };
	}

	/* eslint-disable no-control-regex */
	var multibyte = /[^\x00-\x7F]/;
	/* eslint-enable no-control-regex */

	function isMultibyte(str) {
	  (0, _assertString2.default)(str);
	  return multibyte.test(str);
	}
	module.exports = exports['default'];

/***/ },
/* 105 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = isNull;

	var _assertString = __webpack_require__(0);

	var _assertString2 = _interopRequireDefault(_assertString);

	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { default: obj };
	}

	function isNull(str) {
	  (0, _assertString2.default)(str);
	  return str.length === 0;
	}
	module.exports = exports['default'];

/***/ },
/* 106 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = isNumeric;

	var _assertString = __webpack_require__(0);

	var _assertString2 = _interopRequireDefault(_assertString);

	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { default: obj };
	}

	var numeric = /^[-+]?[0-9]+$/;

	function isNumeric(str) {
	  (0, _assertString2.default)(str);
	  return numeric.test(str);
	}
	module.exports = exports['default'];

/***/ },
/* 107 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = isSurrogatePair;

	var _assertString = __webpack_require__(0);

	var _assertString2 = _interopRequireDefault(_assertString);

	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { default: obj };
	}

	var surrogatePair = /[\uD800-\uDBFF][\uDC00-\uDFFF]/;

	function isSurrogatePair(str) {
	  (0, _assertString2.default)(str);
	  return surrogatePair.test(str);
	}
	module.exports = exports['default'];

/***/ },
/* 108 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = isURL;

	var _assertString = __webpack_require__(0);

	var _assertString2 = _interopRequireDefault(_assertString);

	var _isFQDN = __webpack_require__(6);

	var _isFQDN2 = _interopRequireDefault(_isFQDN);

	var _isIP = __webpack_require__(18);

	var _isIP2 = _interopRequireDefault(_isIP);

	var _merge = __webpack_require__(5);

	var _merge2 = _interopRequireDefault(_merge);

	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { default: obj };
	}

	var default_url_options = {
	  protocols: ['http', 'https', 'ftp'],
	  require_tld: true,
	  require_protocol: false,
	  require_valid_protocol: true,
	  allow_underscores: false,
	  allow_trailing_dot: false,
	  allow_protocol_relative_urls: false
	};

	function isURL(url, options) {
	  (0, _assertString2.default)(url);
	  if (!url || url.length >= 2083 || /\s/.test(url)) {
	    return false;
	  }
	  if (url.indexOf('mailto:') === 0) {
	    return false;
	  }
	  options = (0, _merge2.default)(options, default_url_options);
	  var protocol = void 0,
	      auth = void 0,
	      host = void 0,
	      hostname = void 0,
	      port = void 0,
	      port_str = void 0,
	      split = void 0;

	  split = url.split('#');
	  url = split.shift();

	  split = url.split('?');
	  url = split.shift();

	  split = url.split('://');
	  if (split.length > 1) {
	    protocol = split.shift();
	    if (options.require_valid_protocol && options.protocols.indexOf(protocol) === -1) {
	      return false;
	    }
	  } else if (options.require_protocol) {
	    return false;
	  } else if (options.allow_protocol_relative_urls && url.substr(0, 2) === '//') {
	    split[0] = url.substr(2);
	  }
	  url = split.join('://');

	  split = url.split('/');
	  url = split.shift();
	  split = url.split('@');
	  if (split.length > 1) {
	    auth = split.shift();
	    if (auth.indexOf(':') >= 0 && auth.split(':').length > 2) {
	      return false;
	    }
	  }
	  hostname = split.join('@');
	  split = hostname.split(':');
	  host = split.shift();
	  if (split.length) {
	    port_str = split.join(':');
	    port = parseInt(port_str, 10);
	    if (!/^[0-9]+$/.test(port_str) || port <= 0 || port > 65535) {
	      return false;
	    }
	  }
	  if (!(0, _isIP2.default)(host) && !(0, _isFQDN2.default)(host, options) && host !== 'localhost') {
	    return false;
	  }
	  if (options.host_whitelist && options.host_whitelist.indexOf(host) === -1) {
	    return false;
	  }
	  if (options.host_blacklist && options.host_blacklist.indexOf(host) !== -1) {
	    return false;
	  }
	  return true;
	}
	module.exports = exports['default'];

/***/ },
/* 109 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = isUUID;

	var _assertString = __webpack_require__(0);

	var _assertString2 = _interopRequireDefault(_assertString);

	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { default: obj };
	}

	var uuid = {
	  3: /^[0-9A-F]{8}-[0-9A-F]{4}-3[0-9A-F]{3}-[0-9A-F]{4}-[0-9A-F]{12}$/i,
	  4: /^[0-9A-F]{8}-[0-9A-F]{4}-4[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i,
	  5: /^[0-9A-F]{8}-[0-9A-F]{4}-5[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i,
	  all: /^[0-9A-F]{8}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{12}$/i
	};

	function isUUID(str) {
	  var version = arguments.length <= 1 || arguments[1] === undefined ? 'all' : arguments[1];

	  (0, _assertString2.default)(str);
	  var pattern = uuid[version];
	  return pattern && pattern.test(str);
	}
	module.exports = exports['default'];

/***/ },
/* 110 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = isUppercase;

	var _assertString = __webpack_require__(0);

	var _assertString2 = _interopRequireDefault(_assertString);

	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { default: obj };
	}

	function isUppercase(str) {
	  (0, _assertString2.default)(str);
	  return str === str.toUpperCase();
	}
	module.exports = exports['default'];

/***/ },
/* 111 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = isVariableWidth;

	var _assertString = __webpack_require__(0);

	var _assertString2 = _interopRequireDefault(_assertString);

	var _isFullWidth = __webpack_require__(15);

	var _isHalfWidth = __webpack_require__(16);

	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { default: obj };
	}

	function isVariableWidth(str) {
	  (0, _assertString2.default)(str);
	  return _isFullWidth.fullWidth.test(str) && _isHalfWidth.halfWidth.test(str);
	}
	module.exports = exports['default'];

/***/ },
/* 112 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = isWhitelisted;

	var _assertString = __webpack_require__(0);

	var _assertString2 = _interopRequireDefault(_assertString);

	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { default: obj };
	}

	function isWhitelisted(str, chars) {
	  (0, _assertString2.default)(str);
	  for (var i = str.length - 1; i >= 0; i--) {
	    if (chars.indexOf(str[i]) === -1) {
	      return false;
	    }
	  }
	  return true;
	}
	module.exports = exports['default'];

/***/ },
/* 113 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = ltrim;

	var _assertString = __webpack_require__(0);

	var _assertString2 = _interopRequireDefault(_assertString);

	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { default: obj };
	}

	function ltrim(str, chars) {
	  (0, _assertString2.default)(str);
	  var pattern = chars ? new RegExp('^[' + chars + ']+', 'g') : /^\s+/g;
	  return str.replace(pattern, '');
	}
	module.exports = exports['default'];

/***/ },
/* 114 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = matches;

	var _assertString = __webpack_require__(0);

	var _assertString2 = _interopRequireDefault(_assertString);

	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { default: obj };
	}

	function matches(str, pattern, modifiers) {
	  (0, _assertString2.default)(str);
	  if (Object.prototype.toString.call(pattern) !== '[object RegExp]') {
	    pattern = new RegExp(pattern, modifiers);
	  }
	  return pattern.test(str);
	}
	module.exports = exports['default'];

/***/ },
/* 115 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = normalizeEmail;

	var _isEmail = __webpack_require__(14);

	var _isEmail2 = _interopRequireDefault(_isEmail);

	var _merge = __webpack_require__(5);

	var _merge2 = _interopRequireDefault(_merge);

	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { default: obj };
	}

	var default_normalize_email_options = {
	  lowercase: true,
	  remove_dots: true,
	  remove_extension: true
	};

	function normalizeEmail(email, options) {
	  options = (0, _merge2.default)(options, default_normalize_email_options);
	  if (!(0, _isEmail2.default)(email)) {
	    return false;
	  }
	  var parts = email.split('@', 2);
	  parts[1] = parts[1].toLowerCase();
	  if (parts[1] === 'gmail.com' || parts[1] === 'googlemail.com') {
	    if (options.remove_extension) {
	      parts[0] = parts[0].split('+')[0];
	    }
	    if (options.remove_dots) {
	      parts[0] = parts[0].replace(/\./g, '');
	    }
	    if (!parts[0].length) {
	      return false;
	    }
	    parts[0] = parts[0].toLowerCase();
	    parts[1] = 'gmail.com';
	  } else if (options.lowercase) {
	    parts[0] = parts[0].toLowerCase();
	  }
	  return parts.join('@');
	}
	module.exports = exports['default'];

/***/ },
/* 116 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = rtrim;

	var _assertString = __webpack_require__(0);

	var _assertString2 = _interopRequireDefault(_assertString);

	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { default: obj };
	}

	function rtrim(str, chars) {
	  (0, _assertString2.default)(str);
	  var pattern = chars ? new RegExp('[' + chars + ']+$', 'g') : /\s+$/g;
	  return str.replace(pattern, '');
	}
	module.exports = exports['default'];

/***/ },
/* 117 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = stripLow;

	var _assertString = __webpack_require__(0);

	var _assertString2 = _interopRequireDefault(_assertString);

	var _blacklist = __webpack_require__(12);

	var _blacklist2 = _interopRequireDefault(_blacklist);

	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { default: obj };
	}

	function stripLow(str, keep_new_lines) {
	  (0, _assertString2.default)(str);
	  var chars = keep_new_lines ? '\\x00-\\x09\\x0B\\x0C\\x0E-\\x1F\\x7F' : '\\x00-\\x1F\\x7F';
	  return (0, _blacklist2.default)(str, chars);
	}
	module.exports = exports['default'];

/***/ },
/* 118 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = toBoolean;

	var _assertString = __webpack_require__(0);

	var _assertString2 = _interopRequireDefault(_assertString);

	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { default: obj };
	}

	function toBoolean(str, strict) {
	  (0, _assertString2.default)(str);
	  if (strict) {
	    return str === '1' || str === 'true';
	  }
	  return str !== '0' && str !== 'false' && str !== '';
	}
	module.exports = exports['default'];

/***/ },
/* 119 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = toInt;

	var _assertString = __webpack_require__(0);

	var _assertString2 = _interopRequireDefault(_assertString);

	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { default: obj };
	}

	function toInt(str, radix) {
	  (0, _assertString2.default)(str);
	  return parseInt(str, radix || 10);
	}
	module.exports = exports['default'];

/***/ },
/* 120 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = trim;

	var _assertString = __webpack_require__(0);

	var _assertString2 = _interopRequireDefault(_assertString);

	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { default: obj };
	}

	function trim(str, chars) {
	  (0, _assertString2.default)(str);
	  var pattern = chars ? new RegExp('^[' + chars + ']+|[' + chars + ']+$', 'g') : /^\s+|\s+$/g;
	  return str.replace(pattern, '');
	}
	module.exports = exports['default'];

/***/ },
/* 121 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	Object.defineProperty(exports, "__esModule", {
	      value: true
	});
	exports.default = unescape;

	var _assertString = __webpack_require__(0);

	var _assertString2 = _interopRequireDefault(_assertString);

	function _interopRequireDefault(obj) {
	      return obj && obj.__esModule ? obj : { default: obj };
	}

	function unescape(str) {
	      (0, _assertString2.default)(str);
	      return str.replace(/&amp;/g, '&').replace(/&quot;/g, '"').replace(/&#x27;/g, "'").replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/&#x2F;/g, '\/').replace(/&#96;/g, '\`');
	}
	module.exports = exports['default'];

/***/ },
/* 122 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = whitelist;

	var _assertString = __webpack_require__(0);

	var _assertString2 = _interopRequireDefault(_assertString);

	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { default: obj };
	}

	function whitelist(str, chars) {
	  (0, _assertString2.default)(str);
	  return str.replace(new RegExp('[^' + chars + ']+', 'g'), '');
	}
	module.exports = exports['default'];

/***/ },
/* 123 */
/***/ function(module, exports) {

	module.exports = function() { throw new Error("define cannot be used indirect"); };


/***/ },
/* 124 */
/***/ function(module, exports) {

	"use strict";
	/**
	 Copyright © Oleg Bogdanov
	 Developer: Oleg Bogdanov
	 Contacts: olegbogdanov86@gmail.com
	 ------------------------------------------
	 */

	"use strict";

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	var Email = function Email() {
	  _classCallCheck(this, Email);
	};

	module.exports = new Email();

/***/ },
/* 125 */
/***/ function(module, exports, __webpack_require__) {

	var map = {
		"./email": 124,
		"./email.js": 124,
		"./time": 126,
		"./time.js": 126
	};
	function webpackContext(req) {
		return __webpack_require__(webpackContextResolve(req));
	};
	function webpackContextResolve(req) {
		var id = map[req];
		if(!(id + 1)) // check for number
			throw new Error("Cannot find module '" + req + "'.");
		return id;
	};
	webpackContext.keys = function webpackContextKeys() {
		return Object.keys(map);
	};
	webpackContext.resolve = webpackContextResolve;
	module.exports = webpackContext;
	webpackContext.id = 125;


/***/ },
/* 126 */
/***/ function(module, exports) {

	"use strict";
	/**
	 Copyright © Oleg Bogdanov
	 Developer: Oleg Bogdanov
	 Contacts: olegbogdanov86@gmail.com
	 ---------------------------------------------
	 класс для работы с датами
	 
	 */

	"use strict";

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	var Time = function () {
	    function Time() {
	        _classCallCheck(this, Time);
	    }

	    _createClass(Time, [{
	        key: "Expires",
	        value: function Expires(days) {
	            return new Date(Date.now() + this.Days(days));
	        }
	    }, {
	        key: "maxAge",
	        value: function maxAge(days) {
	            return this.Days(days);
	        }

	        /**
	         * Получаем значение равное кол-ву секунд
	         * @param val - кол-во минут
	         * @returns {number}
	         * @constructor
	         */

	    }, {
	        key: "Seconds",
	        value: function Seconds(val) {
	            return val * 1000;
	        }

	        /**
	         * Получаем значение равное кол-ву минут
	         * @param val - кол-во минут
	         * @returns {number}
	         * @constructor
	         */

	    }, {
	        key: "Minute",
	        value: function Minute(val) {
	            return val * this.Seconds(60);
	        }

	        /**
	         * Получаем значение равное кол-ву часов
	         * @param val - кол-во часов
	         * @returns {number}
	         * @constructor
	         */

	    }, {
	        key: "Hours",
	        value: function Hours(val) {
	            return val * this.Minute(60);
	        }

	        /**
	         * Получаем значение равное кол-ву дней
	         * @param val - кол-во дней
	         * @returns {number}
	         * @constructor
	         */

	    }, {
	        key: "Days",
	        value: function Days(val) {
	            return val * this.Hours(24);
	        }

	        /**
	         * Текущее время в формате TimeStamp
	         * @returns {string}
	         * @constructor
	         */

	    }, {
	        key: "DateToUnixtime",


	        /**
	         * Преобразование даты в формат unixtime
	         * @param date - фомат 24-Nov-2009 17:57:35
	         * @returns {number}
	         * @constructor
	         */
	        value: function DateToUnixtime(date) {
	            return Date.parse(date); // /1000
	        }
	    }, {
	        key: "TimeStamp",
	        get: function get() {
	            var now = new Date();
	            var date = [now.getFullYear(), now.getMonth() + 1, now.getDate()];
	            var time = [now.getHours(), now.getMinutes(), now.getSeconds()];

	            time[0] = time[0] < 12 ? time[0] : time[0] - 12;
	            time[0] = time[0] || 12;

	            for (var i = 1; i < 3; i++) {
	                if (time[i] < 10) time[i] = "0" + time[i];
	            }return date.join("-") + " " + time.join(":");
	        }
	    }, {
	        key: "Unix",
	        get: function get() {
	            return parseInt(new Date().getTime() / 1000);
	        }
	    }]);

	    return Time;
	}();

	module.exports = new Time();

/***/ },
/* 127 */
/***/ function(module, exports) {

	"use strict";
	/**
	 Copyright © Oleg Bogdanov
	 Developer: Oleg Bogdanov
	 Contacts: olegbogdanov86@gmail.com
	 ---------------------------------------------
	 класс для работы с датами

	 */

	"use strict";

	// const moment = require('moment');

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	var Time = function () {
	    function Time() {
	        _classCallCheck(this, Time);
	    }

	    _createClass(Time, [{
	        key: "Expires",
	        value: function Expires(days) {
	            return new Date(Date.now() + this.Days(days));
	        }
	    }, {
	        key: "maxAge",
	        value: function maxAge(days) {
	            return this.Days(days);
	        }

	        /**
	         * Получаем значение равное кол-ву секунд
	         * @param val - кол-во минут
	         * @returns {number}
	         * @constructor
	         */

	    }, {
	        key: "Seconds",
	        value: function Seconds(val) {
	            return val * 1000;
	        }

	        /**
	         * Получаем значение равное кол-ву минут
	         * @param val - кол-во минут
	         * @returns {number}
	         * @constructor
	         */

	    }, {
	        key: "Minute",
	        value: function Minute(val) {
	            return val * this.Seconds(60);
	        }

	        /**
	         * Получаем значение равное кол-ву часов
	         * @param val - кол-во часов
	         * @returns {number}
	         * @constructor
	         */

	    }, {
	        key: "Hours",
	        value: function Hours(val) {
	            return val * this.Minute(60);
	        }

	        /**
	         * Получаем значение равное кол-ву дней
	         * @param val - кол-во дней
	         * @returns {number}
	         * @constructor
	         */

	    }, {
	        key: "Days",
	        value: function Days(val) {
	            return val * this.Hours(24);
	        }

	        /**
	         * Текущее время в формате TimeStamp
	         * @returns {string}
	         * @constructor
	         */

	    }, {
	        key: "DateToUnixtime",


	        /**
	         * Преобразование даты в формат unixtime
	         * @param date - фомат 24-Nov-2009 17:57:35
	         * @returns {number}
	         * @constructor
	         */
	        value: function DateToUnixtime(date) {
	            return Date.parse(date); // /1000
	        }
	    }, {
	        key: "TimeStamp",
	        get: function get() {
	            var now = new Date();
	            var date = [now.getFullYear(), now.getMonth() + 1, now.getDate()];
	            var time = [now.getHours(), now.getMinutes(), now.getSeconds()];

	            time[0] = time[0] < 12 ? time[0] : time[0] - 12;
	            time[0] = time[0] || 12;

	            for (var i = 1; i < 3; i++) {
	                if (time[i] < 10) time[i] = "0" + time[i];
	            }return date.join("-") + " " + time.join(":");
	        }
	    }, {
	        key: "Unix",
	        get: function get() {
	            return parseInt(new Date().getTime() / 1000);
	        }
	    }]);

	    return Time;
	}();

	module.exports = new Time();

/***/ },
/* 128 */,
/* 129 */,
/* 130 */,
/* 131 */,
/* 132 */,
/* 133 */
/***/ function(module, exports, __webpack_require__) {

	var map = {
		"./time": 127,
		"./time.js": 127
	};
	function webpackContext(req) {
		return __webpack_require__(webpackContextResolve(req));
	};
	function webpackContextResolve(req) {
		var id = map[req];
		if(!(id + 1)) // check for number
			throw new Error("Cannot find module '" + req + "'.");
		return id;
	};
	webpackContext.keys = function webpackContextKeys() {
		return Object.keys(map);
	};
	webpackContext.resolve = webpackContextResolve;
	module.exports = webpackContext;
	webpackContext.id = 133;


/***/ }
/******/ ]);