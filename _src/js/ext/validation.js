var validator = require('validator');

validator.checkPass = function (str) {
    return /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$/.test(str);
};

validator.clearStr = function (str) {
    return /^[а-яА-ЯёЁa-zA-Z0-9 -_\.]+$/.test(str);
};

validator.Email = validator.isEmail;


module.exports = validator;