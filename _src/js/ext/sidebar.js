"use strict";

class API{
    constructor(){
        var v = 'v1';
        this.URL = `/api/${v}`;
    }

    logOut(opts, callback){
        Core.API.getData(`${this.URL}/auth/logout`, opts, callback);
    }
}

API = new API;



module.exports = new Vue({
    el: '#sidebar-blocks',
    data: {
        locale: $AppData.activeLocale,
        localeList: $AppData.localeList,
        sidebarLeft: {
            placement: 'left',
            show: false,
            title: '',
            extclass: '',
            width: 350
        },
        sidebarRight: {
            placement: 'right',
            show: false,
            title: '',
            extclass: '',
            width: 350
        }
    },

    ready: function(){
        var self = this;
        this.locale = Cookies.get('mst_locale') ? Cookies.get('mst_locale') : 'rus';

    },

    methods: {
        onSelectLang: function(lang){
            localStorage.setItem('locale', lang);
            Cookies.set('mst_locale', lang, { expires: 2, domain: window.location.hostname });
            setTimeout(function(){ window.location.reload(); },250);
        },

        LogOut: function(){
            Cookies.remove('mst_admin');
            API.logOut({type: 'admin'}, (data, status, request)=>{
                window.location.reload();
            });
        }
    },

    components: {
        sidebar: require('./../components/sidebar')
    }
});