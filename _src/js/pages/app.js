"use strict";

Vue.debug = false;//(NODE_ENV == 'dev');



var $url = Core.parseUrl();

class API{
    constructor(){
        var v = 'v1';
        this.URL = `/api/${v}`;
    }

    getLang(opts, callback){
        Core.API.getData(`${this.URL}/lang/getData`, opts, callback);
    }

    checkAuth(opts, callback){
        Core.API.getData(`${this.URL}/auth/check`, opts, callback);
    }
    
}

API = new API;


//----------------------------------------------------------------------------------------------------------------------

module.exports = new Vue({
    el: 'header',
    data: {
        locale: 'rus',
        lang: {},
        isUpdateLang: true,
        isCheckAuth: true,
        token: '',
        isAuth: true,
        alert: '',
        isLoading: false,
        user: {
            login: '',
            pass: ''
        }
    },
    ready: function() {
        var self = this;

        self.setLocale();
        setInterval(()=> {
            if (self.isCheckAuth)
                self.checkAuth();

            self.setLocale();
        }, Core.Time.Seconds(30));

        this.$watch('locale', ()=>{
            self.setLocale();
        });

        document.querySelector('#auth-block').addEventListener('keypress', function (e) {
            var key = e.which || e.keyCode;

            if (key === 13)
                self.onSingIn();
        });

        this.$emit('getLang');
    },

    events: {
        getLang(isLoad = false){//загружаем локализацию
            let self = this;

            setTimeout(()=>{
                self.$emit('getLang', true);
            }, Core.Time.Minute(10));

            if(isLoad || (Core.util.isObject(this.lang) && Object.keys(this.lang).length == 0))
                this.loadLangData();
        }
    },

    methods: {
        // Загрузка локализации
        loadLangData(){
            let self = this;
            
            this.locale = Cookies.get('mst_locale') ? Cookies.get('mst_locale') : 'rus';
            API.getLang({locale: this.locale}, (data, status, request)=>{
                if(status == 200){
                    self.$set('lang', data.result);
                    Core.Store.Set('lang', data.result);
                }
            });
        },

        setLocale: function () {
            localStorage.setItem('locale', this.locale);
        },

        checkAuth: function(){
            var self = this;

            API.checkAuth({type: 'admin'}, (data, status, request)=>{
                if(data.error == 'SESSION_END')
                    self.showAuthModal();
            });
        },

        showAuthModal: function(){
            UIkit.modal('#auth-block').show();
        },

        hideAuthModal: function(){
            UIkit.modal('#auth-block').hide();
        },

        onSingIn: function(){
            var self = this,
                url = '/api/v1/auth/singIn?type=admin';

            var formView = $('#auth-block').css('display');
            if(!formView.includes('block')) return;


            this.isLoading = true;

            this.$http.get(url, function(r) {},
                {headers: {'Authorization': 'Basic ' + btoa(self.user.login + ':' + self.user.pass)}}

            ).then(
                function(r){
                    self.isAuth = true;
                    self.hideAuthModal();
                    self.isLoading = false;
                },
                function(e){
                    self.isAuth = false;
                    self.isLoading = false;
                    window.location.reload();
                });
        },

        getMessage(){

        },

        getAlerts(){

        },

        onLangList(){
            Lib.sidebar('right', {show: true, title: this.lang.header.interface, extclass: 'locale-view'});
        },

        onUserMenu() {
            Lib.sidebar('right', {show: true, title: this.lang.header.account, extclass: 'user-menu-view'});
        }
    }
});


require('./../custom.js');
