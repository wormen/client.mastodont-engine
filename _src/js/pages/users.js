"use strict";

var $url = Core.parseUrl();

class API{
    constructor(){
        var v = 'v1',
            module = 'users';

        this.URL = `/api/${v}/${module}`;
    }

    loadList(opts, callback) {
        Core.API.getData(`${this.URL}/loadList`, opts, callback);
    }

    loadGroups(callback) {
        Core.API.getData(`/api/v1/groups/miniList`, {}, callback);
    }

    edit(id, callback) {
        Core.API.getData(`${this.URL}/edit/:id`, {id: id}, callback);
    }

    checkLogin(opts, callback) {
        Core.API.getData(`${this.URL}/checkLogin`, opts, callback);
    }

    checkEmail(opts, callback) {
        Core.API.getData(`${this.URL}/checkEmail`, opts, callback);
    }

    save(opts, data, callback) {
        Core.API.postResponse(`${this.URL}/save`, opts, data, callback);
    }

    update(id, opts, data, callback) {
        opts.id = id;
        Core.API.putResponse(`${this.URL}/update/:id`, opts, data, callback);
    }

    remove(id, callback){
        Core.API.deleteResponse(`${this.URL}/remove/:id`, {id: id}, callback);
    }
}

API = new API;



var p = new Vue({
    el: '#content.users',
    data: {
        lang: {},
        pfType: false,
        list: [],
        userData: $AppData.userData,
        groups: [],
        domains: [],
        domainsList: [],
        defaultAvatar: $AppData.defaultAvatar[$AppData.userData.gender],
        isLogin: false,
        isEmail: false,
        matchPass: false,
        searchUser: ''
    },
    ready() {
        var self = this;

        //    resource = this.$resource('/api/v1/setting?type=interface');
        //
        //resource.get({}, function (data, status, request) {
        //    if(status == 200)
        //        this.$set('$data', Vue.util.extend(this.$data, data));
        //}).error(function (data, status, request) {
        //    $Lib.Notify({type: 'error', text: self.$lang.alert.noGetData});
        //});

        if($url.query.action){
            this.loadGroups();
        }else
            this.loadList();

        if($url.query.id)
            this.loadUser();
    },

    methods: {

        // список пользователей
        loadList(page){
            var self = this,
                opts = {};
            if(page) opts.page = page;

            API.loadList(opts, (data, status, request)=>{
                if(status == 200 || data.statusCode == 200)
                    self.$set('list', data.result);
            });
        },

        // список групп пользователей
        loadGroups(){
            var self = this;

            API.loadGroups((data, status, request)=>{
                if(status == 200 || data.statusCode == 200)
                    self.$set('groups', data.result);
            });

        },

        loadUser(){
            var self = this;

            API.edit($url.query.id, (data, status, request)=>{
                if(status == 200 || data.statusCode == 200)
                    self.$set('userData', data.result);
                
                else
                    Lib.Notify({type: 'error', text: self.lang.alert.noGetData});
            });
        },

        showPass(){
            this.pfType = !this.pfType;
        },

        generatePass(){
            var pass = Lib.GenerateHash(10);
            this.$set('userData.password', pass);
            this.$set('userData.repassword', pass);
        },

        onCheckLogin(){
            var self = this,
                val = this.userData.login;

            if(val.length>=3){
                API.checkLogin({check: val}, (data, status, request)=>{
                    if(status == 200 || data.statusCode == 200)
                        self.$set('isLogin', data.result);
                });
            }
        },

        onCheckEmail(){
            var self = this,
                val = this.userData.email;

            if( this.checkmail(val) ){
                
                API.checkEmail({check: val}, (data, status, request)=>{
                    if(status == 200 || data.statusCode == 200)
                        self.$set('isEmail', data.result);
                });

            }
        },

        checkmail(value){
            var reg = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
            return value.match(reg);
        },

        onChangeGender(){
            this.defaultAvatar = $AppData.defaultAvatar[this.userData.gender];
        },


        OnOff(u){
            u.enabled = !u.enabled;
            API.update(u._id, {}, {enabled: u.enabled}, (data, status, request)=>{});
        },

        onEdit(p, u){
            var url = '/admin/?module=users&action=edit&id='+ u._id;

            if(p == false && u.isRoot)
                Lib.Notify({type: 'error', text: this.lang.alert.userNotEdit});
            else
                window.location.href = url;
        },

        onDelete(u, e){
            var self = this;
            UIkit.modal.confirm(self.lang.alert.del, ()=>{

                API.remove(u._id, (data, status, request)=>{
                    if(status == 200 || data.statusCode == 200){
                        $(e.target).closest('tr').remove();
                        Lib.Notify({type: 'success', text: self.lang.users.okDelText});
                    }else
                        Lib.Notify({type: 'error', text: self.lang.users.falseDelText});
                });

            },{ labels: { Ok: self.lang.btns.y, Cancel: self.lang.btns.n } });

            e.preventDefault();
        },

        preSave(){
            var isNext = [];


            if(this.userData.login.length == 0){
                Lib.Notify({type: 'error', text: this.lang.alert.noLogin});
                isNext.push(false);
            }else if(this.userData.login.length<3){
                Lib.Notify({type: 'error', text: this.lang.alert.smallLogin});
                isNext.push(false);
            }

            if(!$url.query.id){
                if(this.userData.password.length == 0){
                    Lib.Notify({type: 'error', text: this.lang.alert.noPass});
                    isNext = false;
                }else if(this.userData.password.length<7){
                    Lib.Notify({type: 'error', text: this.lang.alert.smallPass});
                    isNext.push(false);
                }
            }else{
                if(this.userData.password && this.userData.password.length<7){
                    Lib.Notify({type: 'error', text: this.lang.alert.smallPass});
                    isNext.push(false);
                }
            }


            if(!this.userData.email || this.userData.email.length == 0){
                Lib.Notify({type: 'error', text: this.lang.alert.noEmail});
                isNext.push(false);
            }

            if(!this.userData.email || this.userData.email.length>0 && !this.checkmail(this.userData.email)){
                Lib.Notify({type: 'error', text: this.lang.alert.inavlidEmail});
                isNext.push(false);
            }

            if(!this.userData.groups || this.userData.groups.length == 0){
                Lib.Notify({type: 'error', text: this.lang.alert.noGroups});
                isNext.push(false);
            }

            return isNext.length == 0;
        },

        onSave(){
            var self = this;

            if(!this.preSave()) return;
            delete this.userData.repassword;

            if($url.query.id){

                API.update($url.query.id, {type: 'admin'}, this.userData, (data, status, request)=>{
                    if(status == 200 || data.statusCode == 200)
                        Lib.Notify({type: 'success', text: self.lang.users.okSaveText});
                    else
                        Lib.Notify({type: 'error', text: self.lang.users.falseSaveText});
                });

            }else
                API.save({type: 'admin'}, this.userData, (data, status, request)=>{
                    if(status == 201 || data.statusCode == 201)
                        Lib.Notify({type: 'success', text: self.lang.users.okSaveText});
                    else
                        Lib.Notify({type: 'error', text: self.lang.users.falseSaveText});
                });

        }
    },

    components: {
        //editor: require('./../components/editor.js'),
        'switch-box': require('./../components/switch-box.js')
    }
});


setInterval(()=>{ Core.getLangData((val)=>{ p.$set('lang', Core.util.extend({}, val, true) ); }); }, Core.Time.Seconds(1));
module.exports = {};