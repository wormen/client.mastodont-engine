"use strict";

Vue.use(VueResource);

var resource;

var p = new Vue({
    el: '#main.auth',
    data: {
        lang: {},
        isAuth: true,
        alert: '',
        isLoading: false,
        user: {
            login: '',
            pass: ''
        }
    },
    ready: function() {
        var self = this;
        
        this.loadLocale();

        document.querySelector('.panel.auth').addEventListener('keypress', function (e) {
            var key = e.which || e.keyCode;

            if (key === 13)
                self.onSingIn();
        });
    },

    methods: {
        loadLocale: function(){
            var self = this;

            this.locale = Cookies.get('mst_locale') ? Cookies.get('mst_locale') : 'rus';
            resource = this.$resource('/api/v1/lang/getData?locale='+this.locale);
            resource.get({}, function (data, status, request) {
                if(status == 200){
                    self.lang = data.result;
                    Core.Store.Set('lang', data.result);
                }
            });
        },

        onSingIn: function(){
            var self = this,
                url = '/api/v1/auth/singIn?type=admin';

            this.isLoading = true;

            this.$http.get(url, function(r) {},
                {headers: {'Authorization': 'Basic ' + btoa(self.user.login + ':' + self.user.pass)}}

            ).then(
                function(r){
                    self.isAuth = true;
                    window.location.reload();
                    self.isLoading = false;
                },
                function(e){
                    self.isAuth = false;
                    self.isLoading = false;
                    self.$set('alert', self.lang.auth[e.data.error]);
                });

        }
    }
});


module.exports = {};