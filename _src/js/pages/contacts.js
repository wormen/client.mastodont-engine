"use strict";

import Sortable from 'vue-sortable';
Vue.use(Sortable);

var blockItemList = { // элемент списка блока контактов
        pos: 0,
        value: 'No data',
        type: 'address'
    },
    mapItem = {
        enabled: false,
        type: 'yandex',
        config: {
            hint: '',
            address: '',
            zoom: 12,
            center: [55.75340170835098, 37.6227951049804]
        }
    },
    blockItem = { // новый блок
        nameBlock: 'New Block',
        pos: 0,
        enabled: true,
        list: [],
        map: {enabled: false}
    };






class API{
    constructor(){
        var v = 'v1',
            module = 'contacts';

        this.URL = `/api/${v}/${module}`;

        this.mapInstances = {};
        this.mapInstancesCount = 0;
        this.mapActiveInstance = '';
    }

    $set(property, value){
        this[property] = value;
    }

    addMap(type, idx, data) {
        if(!type) return;

        if (!idx)
            idx = this.mapInstancesCount = this.mapInstances.length;
        else
            idx = idx ? idx : this.mapInstancesCount;


        data = data ? data : mapItem;
        return this.mapInstances[String(type + '-' + idx)] = Vue.util.extend({}, data);
    }

    ckeckMapItem(type, idx) {
        var inst = String(type + '-' + idx),
            m = this.mapInstances[inst];

        this.mapActiveInstance = inst;

        if (Core.util.isNullOrUndefined(m))
            this.addMap(type, idx);
    }

    getMap(type, idx) {
        if(!type) return;
        idx = idx ? idx : this.mapInstances.length;

        this.ckeckMapItem(type, idx);
        return this.mapInstances[this.activeMap];
    }

    get activeMap(){
        return this.mapActiveInstance;
    }

    get newEdit(){
        // blockItem.map = this.getMap(mapItem.type);
        blockItem.list = [Vue.util.extend({}, blockItemList)];

        var d = Vue.util.extend({}, blockItem);
        d.nameBlock = '';

        return d;
    }
    
    getData(callback) {
        Core.API.getData(`${this.URL}/getData`, {}, callback);
    }
    
    saveData(data, callback){
        Core.API.postResponse(`${this.URL}/saveData`, {}, data, callback);
    }

    deleteBlock(id, callback){
        Core.API.deleteResponse(`${this.URL}/deleteBlock/:id`, {id: id}, callback);
    }
}

API = new API;



var mapList = ['yandex', 'google'],
    apiKey = {};

for(let K of mapList)
    apiKey[K] = '';


var p = new Vue({
    el: '#content.contacts',
    data: {
        lang: {},
        isLoading: true,
        edit: API.newEdit,
        idxBlock: 0,
        sortStart: 0,
        all: [],
        apiKey: apiKey,
        mapList: mapList,
        modalMap: {}
    },
    ready() {
        var idx,
            self = this;

        if(this.all.length == 0)
            this.all.push(Vue.util.extend({pos: 0}, blockItem));
        
        

        // $(".contacts-list-wrap").sortable({
        //     axis: "y",
        //     opacity: 0.7,
        //     handle: ".uk-nestable-handle",
        //     stop: function (e, ui){
        //         var arr = [];
        //         $(".contacts-list").each(function(index, value){
        //             idx = $(value).index();
        //             idx++;
        //             self.all[parseInt($(value).data('idx'))].pos = idx;
        //             arr.push(Vue.util.extend({}, self.all[parseInt($(value).data('idx'))]));
        //
        //         });
        //
        //         setTimeout(function(){ self.$set('all', arr); }, 100);
        //     }
        //
        // }).disableSelection();
        //
        // $(".uk-nestable-list").sortable({
        //     axis: "y",
        //     opacity: 0.7,
        //     handle: ".uk-nestable-handle",
        //     stop: function (e, ui){
        //         var end = self.edit.list.length-1;
        //         $(".uk-nestable-list tr").each(function(index, value){
        //             idx = $(value).index();
        //             idx++;
        //
        //             self.edit.list[parseInt($(value).data('idx'))].pos = idx;
        //
        //         });
        //
        //         self.$set('edit.list', [].concat([], self.edit.list.sort(function(a,b){return a.pos-b.pos;})));
        //     }
        //
        // }).disableSelection();

        this.loadData();
    },

    watch: {

        'edit.list':{
            deep: true,
            handler(val, oldVal){
                var isMap = false;
                for(let L of val){
                    if(L.type.includes('map')){
                        if(isMap) L.type = 'address';
                        if(!isMap) isMap = true;
                    }
                }

                this.edit.map.enabled = isMap;
            }
        }
    },

    methods: {

        endSortBlock(e){

            if(this.all.length>1){
                let list = this.all,
                    itemOld = Core.util.extend({}, list[e.oldIndex], true),
                    itemNew = Core.util.extend({}, list[e.newIndex], true);

                list[e.newIndex] = itemOld;
                list[e.oldIndex] = itemNew;
                list = [].concat([], list);

                for(let i in list)
                    list[i].pos = Math.round(Number(i)+1);

                this.$set('all', list);
            }

        },

        endSortContacts(e){
            if(this.edit.list.length>1){
                let list = this.edit.list,
                    itemOld = Core.util.extend({}, list[e.oldIndex], true),
                    itemNew = Core.util.extend({}, list[e.newIndex], true);

                list[e.newIndex] = itemOld;
                list[e.oldIndex] = itemNew;
                list = [].concat([], list);

                for(let i in list)
                    list[i].pos = Math.round(Number(i)+1);

                this.$set('edit.list', list);
                this.$set('all[idxBlock].list', list);
            }
        },
        
        loadData(e) {
            var self = this;

            this.isLoading = true;

            API.getData((data, status, request)=>{
                self.isLoading = false;

                if( (status == 200 || data.statusCode == 200) && data.result.length>0){
                    var listData = data.result;

                    for(let i in listData){
                        if(!listData[i].enabled)
                            listData[i].enabled = true;

                        if(!listData[i].map)
                            API.addMap(mapList[0], i);
                        else
                            API.addMap(listData[i].map.type, i, listData[i].map);

                        for(let l in listData[i].list){
                            if(!listData[i].list[l].pos)
                                listData[i].list[l].pos = l;
                        }
                    }

                    self.$set('edit', API.newEdit);
                    self.$set('all', listData);

                }

            });

            if(e)
                e.preventDefault();
        },

        checkTypeIco(type){
            var ico = '';
            switch(type){
                case 'fax':
                    ico = 'fa fa-fax';
                    break;

                case 'phone':
                    ico = 'fa fa-phone';
                    break;

                case 'mobile':
                    ico = 'fa fa-mobile';
                    break;

                case 'email':
                    ico = 'fa fa-envelope-o';
                    break;

                case 'address':
                    ico = 'fa fa-map-marker';
                    break;

                case 'map':
                    ico = 'fa fa-map';
                    break;
            }

            return ico;
        },

        chanceType(e, idx, type){
            this.edit.list[idx].type = type;
            if(e) e.preventDefault();
        },

        newBlock(){
            return API.newEdit;
        },

        addBlock(e){
            this.edit = this.newBlock();
            this.all.push(this.newBlock());
            if(e) e.preventDefault();
        },

        onOffBlock(b){
            b.enabled = !b.enabled;
        },

        editBlock(e, idx){
            this.$set('idxBlock', idx);

            this.$set('edit', this.all[idx]);
            this.$set('edit.list', [].concat([], this.edit.list.sort(function(a,b){return a.pos-b.pos;})) );

            if(e) e.preventDefault();
        },

        saveBlock(){
            this.$set('edit', Vue.util.extend({}, this.newBlock()));
        },

        delBlock(e, idx){
            var self = this,
                id = this.all[idx]._id;

            if(id){
                UIkit.modal.confirm(self.lang.alert.del, ()=>{
                    self.isLoading = true;

                    API.deleteBlock(id, (data, status, request)=>{
                        self.isLoading = false;

                        if(status == 200 || data.statusCode == 200){
                            if(self.all.length <= 1)
                                self.$set('all', [self.newBlock()]);
                            else
                                self.all.splice(idx, 1);
                        }
                    });

                },{ labels: { Ok: self.lang.btns.y, Cancel: self.lang.btns.n } });
            }else{
                if(this.all.length <= 1)
                    this.$set('all', [this.newBlock()]);
                else
                    this.all.splice(idx, 1);
            }

            e.preventDefault();
        },

        onAddContact(e){
            blockItemList.pos = this.edit.list.length;
            this.edit.list.push(Vue.util.extend({}, blockItemList));
            if(e) e.preventDefault();
        },

        onDeleteContact(idx){
            if(this.edit.list.length <= 1)
                this.$set('edit.list', this.newBlock().list);
            else
                this.edit.list.splice(idx, 1);
        },
        
        isMap(idx){
            return Boolean(this.edit.list[idx].type.includes('map'));
        },

        openModal(){

            var m = this.all[this.idxBlock].map,
                type = m.type ? m.type : mapList[0];

            if(!m.config)
                m = API.getMap(type, this.idxBlock);

            this.$set('modalMap', Core.util.extend({}, m, true));
        },

        onSaveMap(){

            this.$set('all[idxBlock].map', Core.util.extend({}, this.modalMap, true));
            this.$set('modalMap', {});

        },

        onSave(e){
            var self = this;
            this.isLoading = true;

            if(this.edit.nameBlock)
                this.all[this.idxBlock] = this.edit;

            for(var i in this.all){
                for(var l in this.all[i].list)
                    delete this.all[i].list[l].isLast;
            }

            API.saveData({all: this.all}, (data, status, request)=>{
                Lib.Notify({type: 'success', text: self.lang.contacts.okSaveText});

                if(status > 201 || data.statusCode > 201)
                    Lib.Notify({type: 'error', text: self.lang.contacts.falseSaveText});

                if(status == 201 || data.statusCode == 201)
                    self.loadData();

                self.isLoading = false;
            });

            if(e) e.preventDefault();
        }
    },

    components: {
        'yandex': require('./../components/maps/yandex'),
        'google': require('./../components/maps/google')
    }
});


setInterval(()=>{ Core.getLangData((val)=>{ p.$set('lang', Core.util.extend({}, val, true) ); }); }, Core.Time.Seconds(1));
module.exports = {};