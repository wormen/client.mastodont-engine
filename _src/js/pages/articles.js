"use strict";

var $url = Core.parseUrl();

class API{
    constructor(){
        var v = 'v1',
            module = 'articles';

        this.URL = `/api/${v}/${module}`;
    }

    categoryMiniList(opts, callback) {
        Core.API.getData(`${this.URL}/categoryMiniList`, opts, callback);
    }

    categoryCheckUrl(opts, callback) {
        Core.API.getData(`${this.URL}/categoryCheckUrl`, opts, callback);
    }

    categoryEdit(id, callback) {
        Core.API.getData(`${this.URL}/categoryEdit/:id`, {id: id}, callback);
    }

    categorySave(data, callback) {
        Core.API.postResponse(`${this.URL}/categorySave`, {}, data, callback);
    }

    categoryUpdate(id, data, callback) {
        Core.API.putResponse(`${this.URL}/categoryUpdate/:id`, {id: id}, data, callback);
    }

    categoryDelete(id, callback) {
        Core.API.deleteResponse(`${this.URL}/categoryDelete/:id`, {id: id}, callback);
    }

    postMiniList(callback) {
        Core.API.getData(`${this.URL}/postMiniList`, {}, callback);
    }

    postCheckUrl(opts, callback) {
        Core.API.getData(`${this.URL}/postCheckUrl`, opts, callback);
    }

    postEdit(id, callback) {
        Core.API.getData(`${this.URL}/postEdit/:id`, {id: id}, callback);
    }

    postSave(data, callback) {
        Core.API.postResponse(`${this.URL}/postSave`, {}, data, callback);
    }

    postUpdate(id, data, callback) {
        Core.API.putResponse(`${this.URL}/postUpdate/:id`, {id: id}, data, callback);
    }

    postDelete(id, callback){
        Core.API.deleteResponse(`${this.URL}/postDelete/:id`, {id: id}, callback);
    }
}

API = new API;


var p = new Vue({
    el: '#content.articles',
    data: {
        // api: `/api/${v}/articles`,
        lang: {},
        isLoading: false,
        activeLocale: $AppData.activeLocale,
        categoryList: [],
        postList: [],
        category: {
            name: '',
            parent_id: 0,
            h1: '',
            url: '',
            title: '',
            ico: '',
            img: '',
            meta_description: '',
            description: '',
            enabled: false,
            pos: 0,
            locale: $AppData.activeLocale
        },
        post: {
            name: '',
            category_id: 0,
            create: 0,
            modify: 0,
            h1: '',
            url: '',
            title: '',
            meta_description: '',
            anonce: '',
            description: '',
            enabled: false,
            locale: $AppData.activeLocale
        }
    },
    ready() {
        var self = this;

        if(!$url.query.action){
            this.loadCategoryList();
            this.loadPostList();
        }

        if($url.query.action){
            switch($url.query.action){
                case 'edit-category':
                    this.categoryEdit();
                    break;

                case 'add-post':
                    this.loadCategoryList(true);
                    break;

                case 'edit-post':
                    this.loadCategoryList(true);
                    this.postEdit();
                    break;
            }
        }


        $('#list').on('stop.uk.nestable', (p,obj,el)=> {

            var id = $(el[0]).data('id'),
                parentId = $(el[0]).closest('.uk-nestable-list').closest('.uk-nestable-item').data('id');

            if(!id) return;
            if(!parentId) parentId = 0;

            self.onUpdateSortCategory(id, parentId, $(el[0]).index());
        });

        setTimeout(()=>{
            self.initList();
        }, 1000);

    },

    events: {

        categoryOnOff(id, status){

            API.categoryUpdate(id, {enabled: status}, (data, status, request)=>{
            });
        },
        
        categoryDelete(id, e){

            var count = ids.length;
            for(var i in ids)
                this.emit('catDel', ids[i], count, (ids.length-1 || ids.length == 1), e );
        },

        catDel(id, count, isEnd, e){
            var self = this;
            API.categoryDelete(id, (data, status, request)=>{

                if(isEnd){
                    $(e.target).closest('.uk-nestable-item').remove();

                    var text = self.lang.articles.okDelCat;
                    if(count>1)
                        text = self.lang.articles.okDelCatMore;

                    Lib.Notify({type: 'success', text: text});
                }

            });
        }
    },
    
    methods: {

        initList(){
            if($('#list li').length > 1 && $('.uk-nestable')){
                UIkit.nestable('#list', {
                    group:'widgets',
                    maxDepth: 10,
                    handleClass:'uk-nestable-handle',
                    threshold: 10
                });

            }
        },

        loadCategoryList(options){
            var self = this,
                opts = {tree: 1};

            if(options) opts.options = 1;

            self.isLoading = true;
            API.categoryMiniList(opts, (data, status, request)=>{
                if(status == 200 || data.statusCode == 200)
                    self.$set('categoryList', data.result);

                else
                    Lib.Notify({type: 'error', text: self.lang.alert.noGetData});

                self.isLoading = false;
            });

        },

        loadPostList(){
            var self = this;

            this.isLoading = true;
            API.postMiniList((data, status, request)=>{
                if(status == 200 || data.statusCode == 200)
                    self.$set('postList', data.result);

                else
                    Lib.Notify({type: 'error', text: self.lang.alert.noGetData});

                self.isLoading = false;
            });
        },

        onUpdateSortCategory(id, pid, pos){
            var self = this;

            API.categoryUpdate(id, {parent_id: pid, pos: pos}, (data, status, request)=>{
            });
        },

        newUrl(type){
            this.$set('category.url', this.category.url += '-'+Lib.GenerateHash(6));

            if(type == 'post')
                this.onCheckUrlPost();
            else
                this.onCheckUrlCategory();
        },

        onCheckUrlCategory(){
            var self = this,
                url = this.category.url;

            if(url.length>5)
                API.categoryCheckUrl({locale: this.activeLocale, url: url}, (data, status, request)=>{
                    if(status == 200 || data.statusCode == 200)
                        if(data.result == true)
                            self.newUrl();
                });

        },

        onCheckUrlPost(){
            var self = this,
                url = this.post.url;

            if(url.length>5)
                API.postCheckUrl({locale: this.activeLocale, url: url}, (data, status, request)=>{
                    if(status == 200 || data.statusCode == 200)
                        if(data.result == true)
                            self.newUrl('post');
                });
        },

        transtaleCategoryUrl(){
            this.$set('category.url', Core.Translite(this.category.name));
            this.onCheckUrlCategory();
        },

        transtalePostUrl(){
            this.$set('post.url', Core.Translite(this.post.name));
            this.transtalePostUrl();
        },

        categoryEdit(){
            var self = this,
                id = $url.query.id;

            API.categoryEdit(id, (data, status, request)=>{
                if(status == 200 || data.statusCode == 200)
                    self.$set('category', data.result);

                else
                    Lib.Notify({type: 'error', text: self.lang.alert.noGetData});
            });
            
        },

        postEdit(){
            var self = this,
                id = $url.query.id;

            API.postEdit(id, ()=>{
                if(status == 200 || data.statusCode == 200)
                    self.$set('post', data.result);

                else
                    Lib.Notify({type: 'error', text: self.lang.alert.noGetData});
            });

        },

        TabActive(e){
            $('.uk-modal-dialog .tab-block').hide();
            $($(e.target).attr('href')).show();
        },

        onSaveCategory(){
            var self = this;
            this.isLoading = true;

            if($url.query.id)
                API.categoryUpdate($url.query.id, this.category, (data, status, request)=>{
                    self.isLoading = false;

                    if(status == 201 || data.statusCode == 201)
                        Lib.Notify({type: 'success', text: self.lang.articles.okSaveCat});
                    else
                        Lib.Notify({type: 'error', text: self.lang.articles.falseSaveCat});
                });

            else
                API.categorySave(this.category, (data, status, request)=>{
                    self.isLoading = false;

                    if(status == 200 || data.statusCode == 200)
                        Lib.Notify({type: 'success', text: self.lang.articles.okSaveCat});
                    else
                        Lib.Notify({type: 'error', text: self.lang.articles.falseSaveCat});

                });

            //$Core.Content.Set(null);
        },

        onSavePost(){
            var self = this;
            this.isLoading = true;

            if($url.query.id)
                API.postUpdate($url.query.id, this.post, (data, status, request)=>{
                    if(status == 201 || data.statusCode == 201)
                        Lib.Notify({type: 'success', text: self.lang.articles.okSaveText});
                    else
                        Lib.Notify({type: 'error', text: self.lang.articles.falseSaveText});
                });

            else
                API.postSave(this.post, (data, status, request)=>{
                    self.isLoading = false;

                    if(status == 200 || data.statusCode == 200)
                        Lib.Notify({type: 'success', text: self.lang.articles.okSaveText});
                    else
                        Lib.Notify({type: 'error', text: self.lang.articles.falseSaveText});
                });

            //$Core.Content.Set(null);
        },

        OnOff(item){
            item.enabled = !item.enabled;

            API.postUpdate(item._id, {enabled: item.enabled}, (data, status, request)=>{
            });
        },

        onDelete(id, e){
            var self = this;
            UIkit.modal.confirm(self.lang.alert.del, function(){

                API.postDelete(id, ()=>{
                    if(status == 200 || data.statusCode == 200){
                        Lib.Notify({type: 'success', text: self.lang.articles.okDelText});
                        $(e.target).closest('tr').remove();
                    }else
                        Lib.Notify({type: 'error', text: self.lang.articles.falseDelText});
                });

            },{ labels: { Ok: self.lang.btns.y, Cancel: self.lang.btns.n } });

            e.preventDefault();
        }
    },

    components: {
        'articles-category-item': require('./../components/articles-category-item.js'),
        'editor': require('./../components/editor.js'),
        'switch-box': require('./../components/switch-box.js')
    }
});


setInterval(()=>{ Core.getLangData((val)=>{ p.$set('lang', Core.util.extend({}, val, true) ); }); }, Core.Time.Seconds(1));
module.exports = {};