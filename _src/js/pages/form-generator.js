"use strict";

/**
 * Git  https://github.com/icebob/vue-form-generator
 * Доки https://icebob.gitbooks.io/vueformgenerator/content/
 * Демо https://jsfiddle.net/icebob/0mg1v81e/
 */

const VueFormGenerator = require('vue-form-generator/dist/vue-form-generator.min');
Vue.use(VueFormGenerator);

class API{
    constructor(){
        var v = 'v1',
            module = 'logs';

        this.URL = `/api/${v}/${module}`;
    }

    // init(callback) {
    //     Core.API.getData(`${this.URL}/init`, {}, callback);
    // }
    //
    // remove(id, opts, callback) {
    //     opts.id = id;
    //     Core.API.deleteResponse(`${this.URL}/remove/:id`, opts, callback);
    // }
}

API = new API;

// class validators{
//
//     string(s){ // валидация строки
//         console.log('string', s);
//     }
//
//     url(s){ // валидация URL
//         console.log('url', s);
//     }
//
//     regexp(s){
//         console.log('regexp', s);
//     }
// }
// validators = new validators;


let fieldTypes = [
    'text',
    'number',
    'password',
    'email',
    'checkbox',
    'textArea',
    'label',
    'image',
    'checklist',
    'select',
    'selectEx',
    'switch',
    'dateTime',
    'masked',

    'range',
    'slider',

    'color',
    'spectrum'
];


let fieldList = [
    {
        type: "text",
        label: "Name",
        model: "name",
        featured: true,
        min: 3,
        max: 50,
        required: true,
        placeholder: "User's full name",
        pattern: "^\\+[0-9]{2}-[237]0-[0-9]{3}-[0-9]{4}$",
        hint: "Format: +36-(20|30|70)-000-0000",
        help: "You can use any <b>formatted</b> texts. Or place a <a target='_blank' href='https://github.com/icebob/vue-form-generator'>link</a> to another site.",
        validator: [
            // VueFormGenerator.validators.string,
            // VueFormGenerator.validators.url,
            // VueFormGenerator.validators.regexp
        ]
    },
    {
        type: "text",
        label: "Name",
        model: "name",
        featured: true,
        min: 3,
        max: 50,
        required: true,
        placeholder: "User's full name",
        pattern: "^\\+[0-9]{2}-[237]0-[0-9]{3}-[0-9]{4}$",
        hint: "Format: +36-(20|30|70)-000-0000",
        help: "You can use any <b>formatted</b> texts. Or place a <a target='_blank' href='https://github.com/icebob/vue-form-generator'>link</a> to another site.",
        validator: [
            // VueFormGenerator.validators.string,
            // VueFormGenerator.validators.url,
            // VueFormGenerator.validators.regexp
        ]
    }
];


var p = new Vue({
    el: '#content.form-generator',
    data: {
        lang: {},
        list: [],
        fieldTypes: Core.util.unlink(fieldTypes), // список доступных элементов
        isEdit: true,
        editItemType: 'text',
        editItem: {},

        schema: { // схема формы
            // поля формы
            fields: []
        },
        formModel: {}, // данные из формы
        formOptions: {
            validateAfterLoad: true,
            validateAfterChanged: true
        }
    },

    ready(){

    },

    watch: {
        editItem: {
            deep: true,
            handler(val, old){

            }
        }
    },

    methods: {

        setDefaultVals(){ // устанавливаем значения по умолчанию

        },

        onAdd(){
            this.schema.fields.push(Core.util.extend({}, this.editItem, true));
        },

        onCreate(){ // создаем новую форму
            this.$set('isEdit', true);
        },

        onSave(){
            this.$set('isEdit', false);
        }
    },

    components: {
        "vue-form-generator": VueFormGenerator.component
    }
});

setInterval(()=>{ Core.getLangData((val)=>{ p.$set('lang', Core.util.extend({}, val, true) ); }); }, Core.Time.Seconds(1));
module.exports = {};