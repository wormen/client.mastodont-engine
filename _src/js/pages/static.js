"use strict";

var $url = Core.parseUrl();

var content = {
    name: '',
    url: '',
    title: '',
    meta_description: '',
    description: '',
    enabled: true,
    city_id: 0,
    locale: $AppData.activeLocale
};

class API{
    constructor(){
        var v = 'v1',
            module = 'static';

        this.URL = `/api/${v}/${module}`;
    }

    loadList(callback) {
        Core.API.getData(`${this.URL}/loadList`, {}, callback);
    }

    checkUrl(opts, callback) {
        Core.API.getData(`${this.URL}/checkUrl`, opts, callback);
    }

    edit(id, callback) {
        Core.API.getData(`${this.URL}/edit/:id`, {id: id}, callback);
    }

    createTmpDir(data, callback) {
        Core.API.postResponse(`${this.URL}/createTmpDir`, {}, data, callback);
    }
    
    save(data, callback) {
        Core.API.postResponse(`${this.URL}/save`, {}, data, callback);
    }

    update(id, data, callback) {
        Core.API.putResponse(`${this.URL}/update/:id`, {id: id}, data, callback);
    }

    remove(id, callback) {
        Core.API.deleteResponse(`${this.URL}/remove/:id`, {id: id}, callback);
    }
}

API = new API;



var p = new Vue({
    el: '#content.static',
    data: {
        lang: {},
        activeLocale: $AppData.activeLocale,
        page: Vue.util.extend({}, content),
        tmpDir: 'tmp-'+Lib.GenerateHash(15),
        dir: '',
        pageList: []
    },
    ready() {

        if($url.query.id)
            this.loadEdit($url.query.id);
        else
            if(!$url.query.action)
                this.loadList();

        this.createTmpDir();
    },

    methods: {

        loadList(){
            var self = this;

            API.loadList((data, status, request)=>{
                if(status == 200 || data.statusCode == 200)
                    self.$set('pageList', data.result);

                else
                    Lib.Notify({type: 'error', text: self.lang.alert.noGetData});
            });
        },

        loadEdit: function(id){
            var self = this;

            API.edit(id, (data, status, request)=>{
                if(status == 200 || data.statusCode == 200)
                    self.$set('page', data.result);

                else
                    Lib.Notify({type: 'error', text: self.lang.alert.noGetData});
            });

        },

        createTmpDir: function(){

            if ($url.query.action == 'add')
                API.createTmpDir({dir: this.tmpDir}, (data, status, request)=>{});
        },

        OnOff: function(d){
            d.enabled = !d.enabled;
            
            API.update(d._id, {enabled: d.enabled}, (data, status, request)=>{});
        },

        newUrl: function(){
            this.$set('page.url', this.page.url += '-'+Lib.GenerateHash(6));
            this.checkLink();
        },

        transtaleUrl: function(){
            this.$set('page.url', Core.Translite(this.page.name));
            this.checkLink();
        },

        checkLink: function(){
            var self = this,
                url = this.page.url;

            if(url.length>5){

                API.checkUrl({locale: this.activeLocale, url: url}, (data, status, request)=>{
                    if(status == 200 || data.statusCode == 200)
                        if(data.result == true)
                            self.newUrl();
                });

            }

        },

        onDelete: function(id, e){
            var self = this;
            UIkit.modal.confirm(self.lang.alert.del, function(){

                API.remove(id, (data, status, request)=>{
                    if(status == 200 || data.statusCode == 200){
                        $(e.target).closest('tr').remove();
                        Lib.Notify({type: 'success', text: self.lang.static.okDelText});

                    }else
                        Lib.Notify({type: 'error', text: self.lang.static.falseDelText});

                });

            },{ labels: { Ok: self.lang.btns.y, Cancel: self.lang.btns.n } });

            e.preventDefault();
        },

        onSave: function(){
            var self = this;
            
            if($url.query.id)
                API.update($url.query.id, this.page, (data, status, request)=>{
                    if(status == 200 || data.statusCode == 200)
                        Lib.Notify({type: 'success', text: self.lang.static.okSaveText});
                    else
                        Lib.Notify({type: 'error', text: self.lang.static.falseSaveText});
                });

            else{
                this.page.tmp = this.tmpDir;

                API.save(this.page, (data, status, request)=>{
                    if(status == 201 || data.statusCode == 201)
                        Lib.Notify({type: 'success', text: self.lang.static.okSaveText});
                    else
                        Lib.Notify({type: 'error', text: self.lang.static.falseSaveText});
                });
            }

            //$Core.Content.Set(null);
        }
    },

    components: {
        'fm-btn': require('./../components/fm-btn.js'),
        'city-list': require('./../components/city-list.js'),
        'editor': require('./../components/editor.js'),
        'locale-content': require('./../components/locale-content.js'),
        //'editor-multilang': require('./../components/editor-multilang.js'),
        'switch-box': require('./../components/switch-box.js')
    }
});


setInterval(()=>{ Core.getLangData((val)=>{ p.$set('lang', Core.util.extend({}, val, true) ); }); }, Core.Time.Seconds(1));
module.exports = {};