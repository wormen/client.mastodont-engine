"use strict";

var resource;

var p = new Vue({
    el: '#content.fm',
    data: {
        lang: {}
    },
    ready: function() {

    },

    methods: {

    },

    components: {
        'fm-block': require('./../components/fm-block.js')
    }
});


setInterval(()=>{ Core.getLangData((val)=>{ p.$set('lang', Core.util.extend({}, val, true) ); }); }, Core.Time.Seconds(1));
module.exports = {};