"use strict";

var thumbBlockItem = {
    name: 'thumbnail',
    width: 100,
    height: 100,
    crop: true,
    enabled: false
};

class API{
    constructor(){
        var v = 'v1',
            module = 'setting';

        this.URL = `/api/${v}/${module}`;
    }
    
    getData(callback) {
        Core.API.getData(`${this.URL}/getData`, {}, callback);
    }
    
    saveData(data, callback) {
        Core.API.postResponse(`${this.URL}/saveData`, {}, data, callback);
    }

    getTemplates(callback) {
        Core.API.getData(`${this.URL}/getTemplates`, {}, callback);
    }

    userGroupList(callback) {
        Core.API.getData(`/api/v1/groups/miniList`, {}, callback);
    }
}

API = new API;


var p = new Vue({
    el: '#content.setting',
    data: {
        lang: {},
        general: {
            licKey: '',
            technicalDomain: '',
            emailAdmin: '',
            emailSupport: '',
            personalDepartment: '',
            salesDepartment: '',
            wholesaleDepartment: '',
            informationDepartment: '',
            marketingDepartment: '',
            advertisementDepartment: '',
            expires: 14,
            debug: false,
            botsEnable: true,
            botsList: ''
        },
        site: {
            name: '',
            defis: ' &raquo; ',
            mainPageTitle: '',
            mainPageSeoTitle: '',
            mainPageSeoText: '',
            description: '',
            closed: false,
            close_mes: '',
            multilang: false,
            multicity: false,
            cacheOn: true,
            cacheTimeout: 60,
            cookieDomain: '.*',
            cookiePrefix: 'mst',
            moduleNoPrefix: 'null',
            defaultLang: 'rus',
            template: 'default',
            userGroupDefault: 'users',
            scripts: {
                head: '',
                body: ''
            }
        },
        system: {
            serviceManager: {
                
            },
            files: {
                auto_orient: true,
                param_name: 'files',
                max_width: 1500,
                max_height: 1500,
                min_width: 800,
                min_height: 800,
                thumbs: []
            },
            payment: {
                successURL: '/payment?status=fail',
                failURL: '/payment?status=success',
                list: [
                    {
                        name: 'yandex',
                        enabled: false,
                        isTest: true,
                        shopId: '',
                        scid: '',
                        shopPassword: '',
                        formAction: {
                            test: 'https://demomoney.yandex.ru/eshop.xml',
                            production: 'https://money.yandex.ru/eshop.xml'
                        }
                    },
                    {
                        name: 'interkassa',
                        enabled: false
                    }
                ]
            }
        },
        modules: $AppData.settingModules,

        curencyList: [],
        discountList: [],
        userGroupList: [],
        templateList: [
            {v: 'default', l: 'Default'}
        ],

        thumbEdit: {
            name: '',
            width: 0,
            height: 0,
            crop: false,
            enabled: false
        },

        activeEditPayment: 'yandex' // текущая система для редактирования параметров
    },
    ready() {
        var self = this;
        if(self.discountList.length == 0)
            self.discountList.push(Vue.util.extend({}, $AppData.discountListItem));

        this.system.files.thumbs.push(Vue.util.extend({}, thumbBlockItem));

        this.loadData();
        this.getTemplates();
        this.userGroupsLoad();


        this.$watch('thumbEdit', function(newVal, oldVal){
            if(newVal == oldVal) return;
            self.setValueParamPreview();
        }, { deep: true });
    },

    methods: {

        loadData(){
            var self = this;

            API.getData((data, status, request)=>{
                if(status == 200 || data.statusCode == 200){
                    for(var i in data.result)
                        self.$data[i] = data.result[i];
                }else
                    Lib.Notify({type: 'error', text: self.lang.alert.noGetData});
            });

        },

        getTemplates(){
            var self = this;

            API.getTemplates((data, status, request)=>{
                if(status == 200 || data.statusCode == 200)
                    this.$set('templateList', [].concat(self.templateList, data.result));
                else
                    Lib.Notify({type: 'error', text: self.lang.alert.noGetData});
            });
        },

        userGroupsLoad(){
            var self = this;

            API.userGroupList((data, status, request)=>{
                if(status == 200 || data.statusCode == 200)
                    self.$set('userGroupList', data.result);
            });
        },

        OnOff(d){
            d.enabled = !d.enabled;
        },

        onSave(){
            var self = this, discountList = [];

            for(var i in this.discountList){
                if(this.discountList[i].percent==0) continue;
                discountList.push(this.discountList[i]);
            }

            API.saveData({
                general: this.general,
                site: this.site,
                modules: this.modules,
                system: this.system,
                discountList: discountList
            }, (data, status, request)=>{
                if(status == 201 || data.statusCode == 201)
                    Lib.Notify({type: 'success', text: self.lang.setting.okSaveText});
                else
                    Lib.Notify({type: 'error', text: self.lang.setting.falseSaveText});
            });

        },

        checkmail(value){
            var reg = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
            return value.match(reg);
        },

        CE(e){
            var is = true;

            if(e.length>1){
                is = this.checkmail(e);
            }

            return is;
        },

        onDiscountAddItem(){
            var line = Vue.util.extend({}, $AppData.discountListItem);
            this.discountList.push(line);
        },
        onDiscountDeleteItem(idx){
            if(this.discountList.length == 1) this.discountList = [Vue.util.extend({}, $AppData.discountListItem)];
            else this.discountList.splice(idx, 1);
        },

        addParamBlock(){
            thumbBlockItem.name = 'new_param';
            this.thumbEdit = Vue.util.extend({}, thumbBlockItem);
            this.system.files.thumbs.push(Vue.util.extend({}, thumbBlockItem));
        },

        editParamBlock(idx){
            this.thumbEdit = this.system.files.thumbs[idx];
        },

        deleteParamBlock(idx){
            if(this.system.files.thumbs[idx].name == 'thumbnail')
                Lib.Notify({type: 'error', text: this.lang.alert.noDelParam});
            else
                this.system.files.thumbs.splice(idx, 1);

        },

        onOffParamBlock(T){
            T.enabled = !T.enabled;
        },

        setValueParamPreview(){
            this.$set('thumbEdit.width', parseInt(String(this.thumbEdit.width).replace(/\D+/g,"")) );
            this.$set('thumbEdit.height', parseInt(String(this.thumbEdit.height).replace(/\D+/g,"")) );
        },

        paymentOnOff(P){
            P.enabled = !P.enabled;
        },

        paymentEdit(P){
            this.activeEditPayment = P.name;
        }
    },

    components: {
        editor: require('./../components/editor.js'),

        yandex: require('./../components/payment/yandex'),
        interkassa: require('./../components/payment/interkassa'),

        'switch-box': require('./../components/switch-box')
    }
});

setInterval(()=>{ Core.getLangData((val)=>{ p.$set('lang', Core.util.extend({}, val, true) ); }); }, Core.Time.Seconds(1));
module.exports = {};