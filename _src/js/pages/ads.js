"use strict";

Vue.use(VueResource);

var resource,
    $url = Core.parseUrl();

var categoryItem = {
    name: '',
    parent_id: 0,
    h1: '',
    url: '',
    title: '',
    meta_description: '',
    description: '',
    enabled: false,
    pos: 0,
    ico: '',
    style: ''
};


class API{
    constructor(){
        var v = 'v1',
            module = 'ads';

        this.URL = `/api/${v}/${module}`;
    }

    categoryMiniList(opts, callback) {
        Core.API.getData(`${this.URL}/categoryMiniList`, opts, callback);
    }

    categoryEdit(id, callback) {
        Core.API.getData(`${this.URL}/categoryEdit/:id`, {id: id}, callback);
    }

    categoryCheckUrl(opts, callback){
        Core.API.getData(`${this.URL}/categoryCheckUrl`, opts, callback);
    }

    categorySave(data, callback) {
        Core.API.postResponse(`${this.URL}/categorySave`, {}, data, callback);
    }

    categoryUpdate(id, data, callback) {
        Core.API.putResponse(`${this.URL}/categoryUpdate/:id`, {id: id}, data, callback);
    }

    categoryDelete(id, callback){
        Core.API.deleteResponse(`${this.URL}/categoryDelete/:id`, {id: id}, callback);
    }

    updateSortCategory(id, data, callback){
        Core.API.putResponse(`${this.URL}/categoryUpdate/:id`, {id: id}, data, callback);
    }

    adMiniList(callback) {
        Core.API.getData(`${this.URL}/adMiniList`, {}, callback);
    }

    adUpdate(id, data, callback){
        Core.API.putResponse(`${this.URL}/adUpdate/:id`, {id: id}, data, callback);
    }

    adDelete(id, callback){
        Core.API.deleteResponse(`${this.URL}/adDelete/:id`, {id: id}, callback);
    }
}

API = new API;


var p = new Vue({
    el: '#content.ads',
    data: {
        lang: {},
        isSave: false,
        activeLocale: $AppData.activeLocale,
        categoryList: [],
        adsList: {
            list: {}
        },
        category: Vue.util.extend({}, categoryItem),
        filter: {
            status: ''
        }

        // page: Vue.util.extend({}, content)
        // tmpDir: 'tmp-'+$Lib.GenerateHash(15),
        // dir: '',
        // pageList: []
    },

    ready() {
        var self = this;

        if(!$url.query.action){
            this.loadAdList();
            this.loadCategoryList();
        }

        $('#list').on('stop.uk.nestable', function(p,obj,el) {

            var id = $(el[0]).data('id'),
                parentId = $(el[0]).closest('.uk-nestable-list').closest('.uk-nestable-item').data('id');

            if(!id) return;
            if(!parentId) parentId = 0;

            self.onUpdateSortCategory(id, parentId, $(el[0]).index());
        });

        setTimeout(()=>{
            self.initList();
        }, 1000);

        // if($url.query.id)
        //     this.loadEdit($url.query.id);
        // else
        // if(!$url.query.action)
        //     this.loadList();

    },

    events: {
        categoryEdit(id){
            var self = this;

            API.categoryEdit(id, (data, status, request)=>{
                if(status == 200){
                    self.$set('category', Vue.util.extend({}, data.result));
                    UIkit.modal("#category-item").show();
                }else
                    Lib.Notify({type: 'error', text: self.lang.alert.noGetData});
            });

        },

        categoryUpdate(id, data){
            var self = this;
            
            API.categoryUpdate(id, data, (data, status, request)=>{
                self.isSave = false;

                if(status == 200){
                    self.loadCategoryList();
                    Lib.Notify({type: 'success', text: self.lang.ads.okSaveCat});
                }else
                    Lib.Notify({type: 'error', text: self.lang.ads.falseSaveCat});
            });
        },

        categoryDeleteAll(ids, e){
            var count = ids.length;
            for(var i in ids)
                this.$emit('categoryDelete', ids[i], count, (i == ids.length-1), e);
        },

        categoryDelete(id, count, isEnd, e){
            var self = this;

            API.categoryDelete(id, (data, status, request)=> {
                if (isEnd) {
                    $(e.target).closest('.uk-nestable-item').remove();

                    var text = self.lang.menu.okDelText;
                    if (count > 1)
                        text = self.lang.menu.okDelTextMore;

                    Lib.Notify({type: 'success', text: text});
                }
            });
        }
    },
    
    methods: {

        onAdd(e){
            this.$set('category', Vue.util.extend({pos: $('#list .uk-nestable-item').length}, categoryItem));
            e.preventDefault();
        },

        initList(){
            if($('#list li').length > 1 && $('.uk-nestable')){
                UIkit.nestable('#list', {
                    group:'widgets',
                    maxDepth: 10,
                    handleClass:'uk-nestable-handle',
                    threshold: 10
                });

            }
        },

        loadCategoryList(){
            var self = this;

            API.categoryMiniList({tree: 1}, (data, status, request)=>{
                if(status == 200)
                    self.$set('categoryList', data.result);

                else
                    Lib.Notify({type: 'error', text: self.lang.alert.noGetData});
            });
        },

        loadAdList(){
            var self = this;

            API.adMiniList((data, status, request)=>{
                if(status == 200){
                    self.$set('adsList.list', data.result.list);
                    self.$set('adsList.paginator', data.result.paginator);
                }
            });
        },

        onUpdateSortCategory(id, pid, pos){
            var self = this;
            API.updateSortCategory(id, {parent_id: pid, pos: pos}, (data, status, request)=>{
                if(status == 0 || status >= 400)
                    Lib.Notify({type: 'error', text: self.lang.alert.noGetData});
            });
        },

        newUrl(){
            this.$set('category.url', this.category.url += '-'+Lib.GenerateHash(6));
            this.onCheckUrlCategory();
        },

        onCheckUrlCategory(){
            var self = this,
                url = this.category.url;

            if(url.length>5){
                API.categoryCheckUrl({locale: this.activeLocale, 'check-url': url}, (data, status, request)=>{
                    if(status == 200)
                        if(data == true)
                            self.newUrl();
                });
            }
        },

        transtaleCategoryUrl(){
            this.$set('category.url', Core.Translite(this.category.name));
            this.onCheckUrlCategory();
        },

        OnOff(d){
            d.enabled = !d.enabled;
            
            API.adUpdate(d._id, {enabled: d.enabled}, (data, status, request)=>{
            });
        },

        onDelete(id, e){
            var self = this;
            UIkit.modal.confirm(self.lang.alert.del, function(){

                API.adDelete(id, (data, status, request)=>{
                    if(status == 200){
                        $Lib.Notify({type: 'success', text: self.lang.ads.okDelText});
                        $(e.target).closest('tr').remove();
                    }else
                        $Lib.Notify({type: 'error', text: self.lang.ads.falseDelText});
                });

            },{ labels: { Ok: self.lang.btns.y, Cancel: self.lang.btns.n } });

            e.preventDefault();
        },
        
        adStatus(status){
            var ico;

            switch(status){
                case 'moderation':
                    status = 'warning';
                    ico = 'glyphicon glyphicon-time';
                    break;

                case 'ok':
                    status = 'success';
                    ico = 'glyphicon glyphicon-ok';
                    break;

                case 'fail':
                    status = 'danger';
                    ico = 'glyphicon glyphicon-remove';
                    break;
            }

            return {
                class: status,
                ico: ico
            };
        },

        adSetStatus(item, status, e){
            item.moderateStatus = status;

            Lib.screenScrollTo(1000);
            API.adUpdate(item._id, {moderateStatus: status}, (data, status, request)=>{
                Lib.screenScrollTo(5);
            });

        },

        TabActive(e){
            $('.uk-modal-dialog .tab-block').hide();
            $($(e.target).attr('href')).show();
        },

        onSaveCategory(){
            var self = this;
            this.isSave = true;

            if(this.category._id){
                var id = this.category._id;
                delete this.category._id;

                self.emit('categoryUpdate', id, this.category);

            }else{

                API.categorySave(this.category, (data, status, request)=>{
                    self.isSave = false;

                    if(status == 201){
                        self.loadCategoryList();
                        Lib.Notify({type: 'success', text: self.lang.ads.okSaveCat});
                    }else
                        Lib.Notify({type: 'error', text: self.lang.ads.falseSaveCat});
                });

            }

            //$Core.Content.Set(null);
        },

        filterSetStatus(status){
            this.filter.status = status;
        }
    },

    components: {
        'ads-category-item': require('./../components/ads-category-item.js'),
        editor: require('./../components/editor.js'),
        'switch-box': require('./../components/switch-box.js'),
        paginator: require('./../components/paginator.js')
    }
});


setInterval(()=>{ Core.getLangData((val)=>{ p.$set('lang', Core.util.extend({}, val, true) ); }); }, Core.Time.Seconds(1));
module.exports = {};