"use strict";

// import Sortable from 'vue-sortable';
// Vue.use(Sortable);

var $url = Core.parseUrl();

var dataItem = {
    parent_id: 0,
    title: '',
    url: '',
    pos: 0,
    enabled: false,
    type: 'user'
};

var modules = [
    { value: "user", title: "user" },
    { value: "news", title: "news" },
    { value: "static", title: "static" },
    { value: "articles-category", title: "articles-category" },
    { value: "articles", title: "articles" },
    { value: "contacts", title: "contacts" }
];


class API{
    constructor(){
        var v = 'v1',
            module = 'menu';

        this.URL = `/api/${v}/${module}`;
    }

    miniList(opts, callback) {
        Core.API.getData(`${this.URL}/miniList`, opts, callback);
    }

    findContentUrl(opts, callback) {
        Core.API.getData(`${this.URL}/findContentUrl`, opts, callback);
    }

    edit(id, callback) {
        Core.API.getData(`${this.URL}/edit/:id`, {id: id}, callback);
    }

    save(data, callback) {
        Core.API.postResponse(`${this.URL}/save`, {}, data, callback);
    }

    update(id, data, callback) {
        Core.API.putResponse(`${this.URL}/update/:id`, {id: id}, data, callback);
    }

    remove(id, callback){
        Core.API.deleteResponse(`${this.URL}/remove/:id`, {id: id}, callback);
    }
}

API = new API;





var p = new Vue({
    el: '#content.menu',
    data: {
        lang: {},
        edit: Vue.util.extend({}, dataItem),
        list: [],
        isLoading: false,
        isViewLinksTab: false,
        ModuleList: [],
        inModuleUrl: '',
        docName: '',
        isResort: true,
        modules: modules,
        collapseState: {}
    },
    ready() {
        var self = this;
        this.onInitAction();

        $('#list').on('stop.uk.nestable', function(p, obj, el) {

            var id = $(el[0]).data('id'),
                parentId = $(el[0]).closest('.uk-nestable-list').closest('.uk-nestable-item').data('id');

            if(!id) return;
            if(!parentId) parentId = 0;

            self.$emit('endSort', id, parentId, $(el[0]).index()+1);
        });

        setTimeout(()=>{ self.initList(); }, 1000);
    },

    watch: {
        'edit.type'(val, oldVal){
            this.isViewLinksTab = false;
            this.edit.url = '';

            switch(val){
                case 'user':
                    break;

                case 'contacts':
                    this.edit.url = '/contacts';
                    break;

                default:
                    this.isViewLinksTab = true;
                    break;
            }

        }
    },

    events: {
        edit(id){
            var self = this;

            API.edit(id, (data, status, request)=>{
                if(status == 200 || data.statusCode == 200){

                    if(!data.result.type)
                        data.result.type = 'user';

                    self.$set('edit', data.result);
                    UIkit.modal("#menu", {bgclose:false, keyboard: false}).show();
                    
                }else
                    Lib.Notify({type: 'error', text: self.lang.alert.noGetData});
            });
        },

        onOff(id, status){
            API.update(id, {enabled: status}, (data, status, request)=>{});
        },

        deleteAll(ids, e){
            var count = ids.length;
            for(var i in ids)
                this.$emit('remove', ids[i], count, (i == ids.length-1), e);
        },

        remove(id, count, isEnd, e){
            var self = this;
            
            API.remove(id, (data, status, request)=> {
                if (isEnd) {
                    $(e.target).closest('.uk-nestable-item').remove();

                    var text = self.lang.menu.okDelText;
                    if (count > 1)
                        text = self.lang.menu.okDelTextMore;

                    Lib.Notify({type: 'success', text: text});
                }
            });
        },

        endSort(id, parentId, pos){
            this.onUpdateSort(id, parentId, pos);
        },

        stateCollapseSet(id){
            if(!this.collapseState[id])
                this.collapseState[id] = true;
            else
                this.collapseState[id] = !this.collapseState[id];

            this.$set('collapseState', Vue.util.extend({}, this.collapseState));
        }
    },

    methods: {

        initList(){
            if(document.querySelectorAll('#list li').length > 1){
                UIkit.nestable('#list', {
                    group:'widgets',
                    maxDepth: 10,
                    handleClass:'uk-nestable-handle',
                    threshold: 10
                });
            }
        },

        onInitAction(){
            this.onListLoad();
        },

        onListLoad(update){
            var self = this;

            API.miniList({type: 'tree'}, (data, status, request)=>{
                if(status == 200 || data.statusCode == 200){
                    self.$set('list', data.result);
                    setTimeout(()=>{ self.initList(); }, 500);
                }
            });
        },

        onUpdateSort(id, pid, pos){
            var self = this;

            API.update(id, {parent_id: pid, pos: pos}, (data, status, request)=>{
                if(status == 200 || data.statusCode == 200){
                    // self.onListLoad();
                }else
                    Lib.Notify({type: 'error', text: self.lang.alert.noGetData});
            });
        },

        onAdd: function(){
            this.$set('edit', Vue.util.extend({}, dataItem));
            this.edit.pos = this.list.length;
        },

        onSave: function(e){
            e.preventDefault();
            var self = this;

            var Null = function(){
                self.onListLoad();
                UIkit.modal("#menu").hide();
                self.edit = Vue.util.extend({}, dataItem);
                self.docName = '';
                self.ModuleList = [];
            };

            if(this.edit._id){
                var id = this.edit._id;
                delete this.edit._id;

                API.update(id, this.edit, (data, status, request)=>{
                    if(status == 200 || data.statusCode == 200){
                        Lib.Notify({type: 'success', text: self.lang.menu.okSaveText});

                        Null();
                    }else
                        Lib.Notify({type: 'error', text: self.lang.menu.falseSaveText});
                });
            }else
                API.save(this.edit, (data, status, request)=>{
                    if(status == 201 || data.statusCode == 201){
                        Lib.Notify({type: 'success', text: self.lang.menu.okSaveText});

                        Null();
                    }else
                        Lib.Notify({type: 'error', text: self.lang.menu.falseSaveText});
                });
 
        },

        TabActive: function(e){
            $('.uk-modal-dialog .tab-block').hide();
            $($(e.target).attr('href')).show();
        },

        findContent: function(e){
            var self = this;

            if(this.docName.length <3){
                Lib.Notify({type: 'error', text: this.lang.menu.smallDocName});
                return false;
            }

            if(!self.edit.type){
                Lib.Notify({type: 'error', text: this.lang.menu.noType});
                return false;
            }

            this.isLoading = true;

            API.findContentUrl({module: self.edit.type, docName: self.docName}, (data, status, request)=>{
                self.isLoading = false;

                if(status == 200 || data.statusCode == 200){
                    if(data.result)
                        self.$set('ModuleList', data.result);
                }else
                    Lib.Notify({type: 'error', text: self.lang.alert.noGetData});
            });

            e.preventDefault();
        },

        setContentUrl: function(url){
            this.edit.url = url;
            Lib.Notify({type: 'info', text: this.lang.menu.linkChange});
        }
    },

    components: {
        'menu-item': require('./../components/menu-item.js'),
        'switch-box': require('./../components/switch-box.js')
    }
});


setInterval(()=>{ Core.getLangData((val)=>{ p.$set('lang', Core.util.extend({}, val, true) ); }); }, Core.Time.Seconds(1));
module.exports = {};