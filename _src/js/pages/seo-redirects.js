"use strict";


var redirectItem = {
        oldUrl: '',
        newUrl: '',
        statusCode: 301
    };

class API{
    constructor(){
        var v = 'v1',
            module = 'seo';

        this.URL = `/api/${v}/${module}`;
    }

    redirectsLoad(callback) {
        Core.API.getData(`${this.URL}/redirectsLoad`, {}, callback);
    }

    redirectsSave(data, callback) {
        Core.API.postResponse(`${this.URL}/redirectsSave`, {}, data, callback);
    }
}

API = new API;




var p = new Vue({
    el: '#content.seo.redirects',
    data: {
        lang: {},
        isEdit: false,
        path: Vue.util.extend({}, redirectItem),
        codeList: [301,302,404],
        redirects: []
    },
    ready() {
        var self = this;

        this.$watch('path.statusCode', function(){
            if(self.path.statusCode == 404)
                self.$set('path.newUrl', '');
        });

        this.loadData();
    },

    methods: {

        loadData(){
            var self = this;

            API.redirectsLoad((data, status, request)=>{
                if( (status == 200 || data.statusCode == 200) && Core.util.isObject(data.result))
                    self.$set('redirects', data.result);
                else
                    Lib.Notify({type: 'error', text: self.lang.alert.noGetData});
            });
        },

        onAdd(){
            if(!this.isEdit)
                this.redirects.push(this.path);

            this.path = Vue.util.extend({}, redirectItem);
            this.isEdit = false;
        },

        onEdit(D){
            this.path = D;
            this.isEdit = true;
        },

        onDelete(idx){
            this.redirects.splice(idx, 1);
        },

        onSave(){
            var self = this;
            
            API.redirectsSave({r: this.redirects}, (data, status, request)=>{
                if(status == 201 || data.statusCode == 201)
                    Lib.Notify({type: 'success', text: self.lang.seo.okSaveText});
                else
                    Lib.Notify({type: 'error', text: self.lang.seo.falseSaveText});
            });
        }

    }
});


setInterval(()=>{ Core.getLangData((val)=>{ p.$set('lang', Core.util.extend({}, val, true) ); }); }, Core.Time.Seconds(1));
module.exports = {};