"use strict";

var $url = Core.parseUrl();

var varItem = {
    name: ''
};

var content = {
    name: '',
    sysName: '',
    subject: '',
    description: '',
    vars: ['new'],
    enabled: true,
    locale: $AppData.activeLocale
};


class API{
    constructor(){
        var v = 'v1',
            module = 'letters';

        this.URL = `/api/${v}/${module}`;
    }

    miniList(callback) {
        Core.API.getData(`${this.URL}/miniList`, {}, callback);
    }

    edit(id, callback) {
        Core.API.getData(`${this.URL}/edit/:id`, {id: id}, callback);
    }

    save(data, callback) {
        Core.API.postResponse(`${this.URL}/save`, {}, data, callback);
    }

    update(id, data, callback) {
        Core.API.putResponse(`${this.URL}/update/:id`, {id: id}, data, callback);
    }
}

API = new API;




var p = new Vue({
    el: '#content.letters',
    data: {
        lang: {},
        activeLocale: $AppData.activeLocale,
        page: Vue.util.extend({}, content),
        list: []
    },
    ready: function() {

        if($url.query.id)
            this.loadEdit($url.query.id);
        else
            if(!$url.query.action)
                this.loadList();
    },

    methods: {

        loadList: function(){
            var self = this;
            
            API.miniList((data, status, request)=>{
                if(status == 200 || data.statusCode == 200)
                    self.$set('list', data.result);
                else
                    Lib.Notify({type: 'error', text: self.lang.alert.noGetData});
            });

        },

        loadEdit: function(id){
            var self = this;

            API.edit(id, (data, status, request)=>{
                if(status == 200 || data.statusCode == 200){
                    if(!data.result.vars)
                        data.result.vars = ['new'];

                    self.$set('page', data.result);
                }else
                    Lib.Notify({type: 'error', text: self.lang.alert.noGetData});
            });

        },

        insertEditor: function(vars){
            var Editor;
            for(var i in CKEDITOR.instances)
                Editor = CKEDITOR.instances[i];
            Editor.insertText('{'+vars+'}');
        },

        checkLength: function (idx, str) {
            this.page.vars[idx] = str.substring(0,30);
        },

        addVar: function(){
            this.page.vars.push('');
        },

        deleteVar: function(idx){
            if(this.page.vars.length==1)
                this.page.vars = [''];
            else
                this.page.vars.splice(idx,1);
        },

        onSave: function(){
            var self = this;

            if($url.query.id)
                API.update($url.query.id, this.page, (data, status, request)=>{
                    if(status == 200 || data.statusCode == 200)
                        Lib.Notify({type: 'success', text: self.lang.letters.okSaveText});
                    else
                        Lib.Notify({type: 'error', text: self.lang.letters.falseSaveText});
                });

            else{
                this.page.tmp = this.tmpDir;

                API.save(this.page, (data, status, request)=>{
                    if(status == 201 || data.statusCode == 201)
                        Lib.Notify({type: 'success', text: self.lang.letters.okSaveText});
                    else
                        Lib.Notify({type: 'error', text: self.lang.letters.falseSaveText});
                });
            }

        }
    },

    components: {
        'editor': require('./../components/editor.js'),
        'switch-box': require('./../components/switch-box.js')
    }
});


setInterval(()=>{ Core.getLangData((val)=>{ p.$set('lang', Core.util.extend({}, val, true) ); }); }, Core.Time.Seconds(1));
module.exports = {};