"use strict";

var $url = Core.parseUrl();

var contentPost = {
    name: '',
    // url: '',
    // title: '',
    // meta_description: '',
    category_id: 0,
    description: '',
    enabled: true,
    locale: $AppData.activeLocale
};

var contentCategory = {
    name: '',
    // url: '',
    // title: '',
    // meta_description: '',
    description: '',
    enabled: true,
    locale: $AppData.activeLocale
};


class API{
    constructor(){
        var v = 'v1',
            module = 'faq';

        this.URL = `/api/${v}/${module}`;
    }

    categoryMiniList(opts, callback) {
        Core.API.getData(`${this.URL}/categoryMiniList`, opts, callback);
    }

    categoryEdit(id, callback){
        Core.API.getData(`${this.URL}/categoryEdit/:id`, {}, callback);
    }

    categorySave(data, callback) {
        Core.API.postResponse(`${this.URL}/categorySave`, {}, data, callback);
    }

    categoryUpdate(id, data, callback) {
        Core.API.putResponse(`${this.URL}/categoryUpdate/:id`, {id: id}, data, callback);
    }

    categoryDelete(id, callback) {
        Core.API.deleteResponse(`${this.URL}/categoryDelete/:id`, {id: id}, callback);
    }


    postMiniList(opts, callback) {
        Core.API.getData(`${this.URL}/postMiniList`, opts, callback);
    }

    postEdit(id, callback){
        Core.API.getData(`${this.URL}/postEdit/:id`, {}, callback);
    }

    postSave(data, callback) {
        Core.API.postResponse(`${this.URL}/postSave`, {}, data, callback);
    }

    postUpdate(id, data, callback) {
        Core.API.putResponse(`${this.URL}/postUpdate/:id`, {id: id}, data, callback);
    }

    postDelete(id, callback){
        Core.API.deleteResponse(`${this.URL}/postDelete/:id`, {id: id}, callback);
    }
}

API = new API;


var p = new Vue({
    el: '#content.faq',
    data: {
        lang: {},
        page: Vue.util.extend({}, contentPost),
        category: Vue.util.extend({}, contentCategory),
        isLoading: false,
        categoryList: [],
        postList: []
    },
    ready: function() {
        var self = this;

        if(!$url.query.id){
            self.loadCats();
            self.loadPosts();
        }

        if($url.query.id){
            if($url.query.action == 'edit-category')
                self.categoryEdit();

            if($url.query.action == 'edit-post'){
                self.loadCats();
                self.postEdit();
            }
        }
    },

    methods: {

        loadCats: function(){
            var self = this;

            this.isLoading = true;
            API.categoryMiniList({list: 'mini'}, (data, status, request)=>{
                self.isLoading = false;

                if(status == 200 || data.statusCode)
                    self.$set('categoryList', data.result);

                else
                    Lib.Notify({type: 'error', text: self.lang.alert.noGetData});
            });

        },

        loadPosts: function(){
            var self = this;
            
            API.postMiniList({list: 'mini'}, (data, status, request)=>{
                if(status == 200 || data.statusCode == 200)
                    self.$set('postList', data.result);

                else
                    Lib.Notify({type: 'error', text: self.lang.alert.noGetData});
            });
        },

        categoryEdit: function(){
            var self = this;

            API.categoryEdit($url.query.id, (data, status, request)=>{
                if(status == 200 || data.statusCode == 200)
                    self.$set('category', data.result);

                else
                    Lib.Notify({type: 'error', text: self.lang.alert.noGetData});
            });

        },

        postEdit: function(){
            var self = this;
            
            API.postEdit($url.query.id, (data, status, request)=>{
                if(status == 200 || data.statusCode)
                    self.$set('page', data.result);

                else
                    Lib.Notify({type: 'error', text: self.lang.alert.noGetData});
            });
        },

        onSavePost: function(){
            var self = this;
            

            if($url.query.id)
                API.postUpdate($url.query.id, this.page, (data, status, request)=>{
                    if(status == 201 || data.statusCode == 201)
                        Lib.Notify({type: 'success', text: self.lang.faq.okSaveText});
                    else
                        Lib.Notify({type: 'error', text: self.lang.faq.falseSaveText});
                });


            else
                API.postSave(this.page, (data, status, request)=>{
                    if(status == 201 || data.statusCode == 201)
                        Lib.Notify({type: 'success', text: self.lang.faq.okSaveText});
                    else
                        Lib.Notify({type: 'error', text: self.lang.faq.falseSaveText});
                });

        },

        onSaveCat: function(){
            var self = this;

            if($url.query.id)
                API.categoryUpdate($url.query.id, this.category, (data, status, request)=>{
                    if(status == 200 || data.statusCode == 200)
                        Lib.Notify({type: 'success', text: self.lang.faq.okSaveText});
                    else
                        Lib.Notify({type: 'error', text: self.lang.faq.falseSaveText});
                });

            else
                API.categorySave(this.category, (data, status, request)=>{
                    if(status == 201 || data.statusCode == 201)
                        Lib.Notify({type: 'success', text: self.lang.faq.okSaveText});
                    else
                        Lib.Notify({type: 'error', text: self.lang.faq.falseSaveText});
                });

        },

        onDeleteCat: function(id, e){
            var self = this;
            UIkit.modal.confirm(self.lang.alert.del, function(){

                API.categoryDelete(id, (data, status, request)=>{

                    if(status == 200 || data.statusCode == 200){
                        $(e.target).closest('tr').remove();
                        Lib.Notify({type: 'success', text: self.lang.faq.okDelText});
                    }else
                        Lib.Notify({type: 'error', text: self.lang.faq.falseDelText});

                });

            },{ labels: { Ok: self.lang.btns.y, Cancel: self.lang.btns.n } });

            e.preventDefault();
        },

        onDeletePost: function(id, e){
            var self = this;
            UIkit.modal.confirm(self.lang.alert.del, function(){

                API.postDelete(id, (data, status, request)=>{
                    if(status == 200 || data.statusCode == 200){
                        $(e.target).closest('tr').remove();
                        Lib.Notify({type: 'success', text: self.lang.faq.okDelText});
                    }else
                        Lib.Notify({type: 'error', text: self.lang.faq.falseDelText});
                });

            },{ labels: { Ok: self.lang.btns.y, Cancel: self.lang.btns.n } });

            e.preventDefault();
        }
    },

    components: {
        'editor': require('./../components/editor.js'),
        'switch-box': require('./../components/switch-box.js')
    }
});


setInterval(()=>{ Core.getLangData((val)=>{ p.$set('lang', Core.util.extend({}, val, true) ); }); }, Core.Time.Seconds(1));
module.exports = {};