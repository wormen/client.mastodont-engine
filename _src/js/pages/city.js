"use strict";

Vue.use(VueResource);

var resource,
    $url = Core.parseUrl();

var p = new Vue({
    el: '#content.list-cities',
    data: {
        api: '/api/v1/static',
        lang: {}
    },
    ready: function() {

    },

    methods: {

        // loadList: function(){
        //     var self = this;
        //     resource = this.$resource(this.api);
        //     resource.get({}, function (data, status, request) {
        //         if(status == 200)
        //             self.$set('pageList', data);
        //     }).error(function (data, status, request) {
        //         $Lib.Notify({type: 'error', text: self.lang.alert.noGetData});
        //     });
        // },
        //
        // loadEdit: function(id){
        //     var self = this;
        //     resource = this.$resource(this.api);
        //     resource.get({id: id}, function (data, status, request) {
        //         if(status == 200)
        //             self.$set('page', data);
        //
        //     }).error(function (data, status, request) {
        //         $Lib.Notify({type: 'error', text: self.lang.alert.noGetData});
        //     });
        // },
        //
        // createTmpDir: function(){
        //     resource = this.$resource(this.api);
        //     if ($url.query.action == 'add') {
        //         resource.save({tmp: this.tmpDir}, {}, function (data, status, request) {
        //         }).error(function (data, status, request) {});
        //     }
        // },
        //
        // OnOff: function(d){
        //     d.enabled = !d.enabled;
        //     resource.update({id: d._id}, {enabled: d.enabled}, function (data, status, request) {
        //     }).error(function (data, status, request) {
        //     });
        // },
        //
        // newUrl: function(){
        //     this.$set('page.url', this.page.url += '-'+$Lib.GenerateHash(6));
        //     this.checkLink();
        // },
        //
        // transtaleUrl: function(){
        //     this.$set('page.url', $Core.Translite(this.page.name));
        //     this.checkLink();
        // },
        //
        // checkLink: function(){
        //     var self = this,
        //         url = this.page.url;
        //
        //     if(url.length>5){
        //         resource = this.$resource(this.api);
        //         resource.get({locale: this.activeLocale, 'check-url': url}, function (data, status, request) {
        //             if(status == 200)
        //                 if(data == true)
        //                     self.newUrl();
        //
        //         }).error(function (data, status, request) {});
        //     }
        //
        // },
        //
        // onDelete: function(id, e){
        //     var self = this;
        //     UIkit.modal.confirm(self.lang.alert.del, function(){
        //
        //         resource = self.$resource(self.api);
        //         resource.delete({id: id}, function (data, status, request) {
        //             $Lib.Notify({type: 'success', text: self.lang.static.okDelText});
        //             if(status == 200)
        //                 $(e.target).closest('tr').remove();
        //
        //         }).error(function (data, status, request) {
        //             $Lib.Notify({type: 'error', text: self.lang.static.falseDelText});
        //         });
        //
        //     },{ labels: { Ok: self.lang.btns.y, Cancel: self.lang.btns.n } });
        //
        //     e.preventDefault();
        // },
        //
        // onSave: function(){
        //     var self = this;
        //     resource = this.$resource(this.api);
        //
        //     if($url.query.id){
        //
        //         resource.update({id: $url.query.id}, this.page, function (data, status, request) {
        //             $Lib.Notify({type: 'success', text: self.lang.static.okSaveText});
        //         }).error(function (data, status, request) {
        //             $Lib.Notify({type: 'error', text: self.lang.static.falseSaveText});
        //         });
        //
        //     }else{
        //         this.page.tmp = this.tmpDir;
        //         resource.save({}, this.page, function (data, status, request) {
        //             $Lib.Notify({type: 'success', text: self.lang.static.okSaveText});
        //         }).error(function (data, status, request) {
        //             $Lib.Notify({type: 'error', text: self.lang.static.falseSaveText});
        //         });
        //
        //     }
        //
        //     //$Core.Content.Set(null);
        // }
    },

    components: {
        // 'fm-btn': require('./../components/fm-btn.js'),
        // 'city-list': require('./../components/city-list.js'),
        // 'editor': require('./../components/editor.js'),
        // 'locale-content': require('./../components/locale-content.js'),
        // //'editor-multilang': require('./../components/editor-multilang.js'),
        // 'switch-box': require('./../components/switch-box.js')
    }
});


setInterval(()=>{ Core.getLangData((val)=>{ p.$set('lang', Core.util.extend({}, val, true) ); }); }, Core.Time.Seconds(1));
module.exports = {};