"use strict";

var $url = Core.parseUrl();

class API{
    constructor(){
        var v = 'v1',
            module = 'services';

        this.URL = `/api/${v}/${module}`;
    }

    loadList(callback) {
        Core.API.getData(`${this.URL}/loadList`, {}, callback);
    }

    remove(id, callback) {
        Core.API.deleteResponse(`${this.URL}/remove/:id`, {id: id}, callback);
    }
}

API = new API;



var p = new Vue({
    el: '#content.services',
    data: {
        lang: {},
        isLoading: true,
        list: [],
        service: {}
    },
    ready() {

        this.loadList();
    },

    methods: {
        loadList(){
            var self = this;
            this.isLoading = true;

            API.loadList((data, status, request)=>{
                self.isLoading = false;

                if(status == 200 || data.statusCode == 200)
                    self.$set('list', data.result);
                else
                    Lib.Notify({type: 'error', text: self.lang.alert.noGetData});
            });
            
        },

        onRefresh(e){
            e.preventDefault();
            this.loadList();
        },

        onChangeState(S, e){

            if(S.p == 'system')
                Lib.Notify({type: 'error', text: this.lang.services.noState});

            e.preventDefault();
        },

        onEdit(S, e){
            e.preventDefault();
        },

        onDelete(S, e){
            var self = this;
            e.preventDefault();

            if(S.p == 'system')
                Lib.Notify({type: 'error', text: this.lang.services.noDel});

            else{
                UIkit.modal.confirm(self.lang.alert.del, function(){
                    self.isLoading = true;

                    API.remove(S._id, (data, status, request)=>{
                        self.isLoading = false;

                        if(status == 200 || data.statusCode == 200){
                            $(e.target).closest('tr').remove();
                            Lib.Notify({type: 'success', text: self.lang.services.okDelText});
                        }else
                            Lib.Notify({type: 'error', text: self.lang.services.falseDelText});

                    });

                },{ labels: { Ok: self.lang.btns.y, Cancel: self.lang.btns.n } });
            }

        }
    },

    components: {
        cron: require('./../components/services/cron.js'),
        custom: require('./../components/services/custom.js'),
        backup: require('./../components/services/backup.js'),
        queues: require('./../components/services/queues.js'),
        updater: require('./../components/services/updater.js'),
        'user-state': require('./../components/services/user-state.js')
    }
});


setInterval(()=>{ Core.getLangData((val)=>{ p.$set('lang', Core.util.extend({}, val, true) ); }); }, Core.Time.Seconds(1));
module.exports = {};