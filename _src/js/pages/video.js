"use strict";

var $url = Core.parseUrl();

class API{
    constructor(){
        var v = 'v1',
            module = 'video';

        this.URL = `/api/${v}/${module}`;
    }

    categoryMiniList(opts, callback) {
        Core.API.getData(`${this.URL}/categoryMiniList`, opts, callback);
    }

    categoryEdit(id, callback) {
        Core.API.getData(`${this.URL}/categoryEdit/:id`, {id: id}, callback);
    }

    categorySave(data, callback) {
        Core.API.postResponse(`${this.URL}/categorySave`, {}, data, callback);
    }

    categoryUpdate(id, data, callback) {
        Core.API.putResponse(`${this.URL}/categoryUpdate/:id`, {id: id}, data, callback);
    }

    miniList(callback) {
        Core.API.getData(`${this.URL}/miniList`, {}, callback);
    }

    checkUrl(opts, callback) {
        Core.API.getData(`${this.URL}/checkUrl`, opts, callback);
    }
    
    videoRemove(id, callback){
        Core.API.deleteResponse(`${this.URL}/videoRemove/:id`, {id: id}, callback);
    }
}

API = new API;



var categoryItem = {
    name: '',
    parent_id: 0,
    h1: '',
    url: '',
    title: '',
    meta_description: '',
    description: '',
    enabled: false,
    channals: [],
    pos: 0,
    ico: '',
    style: ''
};

var videoItem = {
    name: '',
    h1: '',
    title: '',
    vid: '',
    type: 'youtube',
    category_id: '',
    meta_description: '',
    description: '',
    enabled: false
};

var channalItem = {
    id: ''
};

var p = new Vue({
    el: '#content.video',
    data: {
        lang: {},
        isSave: false,
        activeLocale: $AppData.activeLocale,
        categoryList: [],
        videoList: {
            list: []
        },
        category: Vue.util.extend({}, categoryItem),
        videoItem: {}
    },
    ready() {
        var self = this;

        if(!$url.query.action){
            this.loadVideoList();
            this.loadCategoryList();
        }

        if(this.category.channals.length == 0)
            this.addChannel();


        $('#list').on('stop.uk.nestable', (p, obj, el)=> {

            var id = $(el[0]).data('id'),
                parentId = $(el[0]).closest('.uk-nestable-list').closest('.uk-nestable-item').data('id');

            if (!id) return;
            if (!parentId) parentId = 0;

            self.onUpdateSortCategory(id, parentId, $(el[0]).index());
        });


        setTimeout(()=>{
            self.initList();
        }, 1000);
    },

    methods: {

        onAdd(e){
            this.$set('category', Vue.util.extend({pos: $('#list .uk-nestable-item').length}, categoryItem));
            e.preventDefault();
        },

        onAddVideo(){
            this.$set('videoItem', Vue.util.extend({}, videoItem));
        },

        initList(){
            if($('#list li').length > 1 && $('.uk-nestable')){
                UIkit.nestable('#list', {
                    group:'widgets',
                    maxDepth: 10,
                    handleClass:'uk-nestable-handle',
                    threshold: 10
                });
            }
        },

        loadCategoryList(){
            var self = this;

            API.categoryMiniList({tree: 1}, (data, status, request)=>{
                if(status == 200 || data.statusCode == 200)
                    self.$set('categoryList', data.result);
                
                else
                    Lib.Notify({type: 'error', text: self.lang.alert.noGetData});
            });

        },

        loadVideoList(){
            var self = this;

            API.miniList((data, status, request)=>{
                if(status == 200 || data.statusCode == 200){
                    self.$set('videoList.list', data.result.list);
                    self.$set('videoList.paginator', data.result.paginator);
                }
            });

        },

        onUpdateSortCategory(id, pid, pos){
            var self = this;

            API.categoryUpdate(id, {parent_id: pid, pos: pos}, (data, status, request)=>{
                if(status > 200 || data.statusCode >200)
                    Lib.Notify({type: 'error', text: self.lang.alert.noGetData});
            });

        },

        newUrl(){
            this.$set('category.url', this.category.url += '-'+Lib.GenerateHash(6));
            this.onCheckUrlCategory();
        },

        onCheckUrlCategory(){
            var self = this,
                url = this.category.url;

            if(url.length>5){
                
                API.checkUrl({locale: this.activeLocale, url: url}, (data, status, request)=>{
                    if(status == 200 || data.statusCode == 200)
                        if(data.result == true)
                            self.newUrl();
                });

            }
        },

        transtaleCategoryUrl(){
            this.$set('category.url', Core.Translite(this.category.name));
            this.onCheckUrlCategory();
        },

        addChannel(){
            this.category.channals.push(Vue.util.extend({}, channalItem));
        },

        deleteChannel(idx){
            this.category.channals.splice(idx, 1);

            if(this.category.channals.length == 0)
                this.addChannel();
        },

        OnOff(d){
            d.enabled = !d.enabled;
            API.categoryUpdate(d._id, {enabled: d.enabled}, (data, status, request)=>{});
        },

        onDelete(id, e){
            var self = this;
            UIkit.modal.confirm(self.lang.alert.del, function(){

                API.videoRemove(id, (data, status, request)=>{

                    if(status == 200 || data.statusCode == 200){
                        $(e.target).closest('tr').remove();
                        $Lib.Notify({type: 'success', text: self.lang.ads.okDelText});

                    }else
                        $Lib.Notify({type: 'error', text: self.lang.ads.falseDelText});
                });

            },{ labels: { Ok: self.lang.btns.y, Cancel: self.lang.btns.n } });

            e.preventDefault();
        },
        
        adStatus(status){
            switch(status){
                case 'moderation':
                    status = 'warning';
                    break;

                case 'ok':
                    status = 'success';
                    break;

                case 'fail':
                    status = 'danger';
                    break;
            }

            return `label-${status}`;
        },

        TabActive(e){
            $('.uk-modal-dialog .tab-block').hide();
            $($(e.target).attr('href')).show();
        },

        onSaveCategory(){
            var self = this;
            this.isSave = true;

            if(this.category._id){

                API.categoryUpdate(this.category._id, this.category, (data, status, request)=>{
                    self.isSave = false;

                    if(status == 200 || data.statusCode == 200 ){
                        self.loadCategoryList();
                        Lib.Notify({type: 'success', text: self.lang.ads.okSaveCat});
                    }else
                        Lib.Notify({type: 'error', text: self.lang.ads.falseSaveCat});
                });

            }else{

                API.categorySave(this.category, (data, status, request)=>{
                    self.isSave = false;

                    if(status == 201 || data.statusCode == 201 ){
                        self.loadCategoryList();
                        Lib.Notify({type: 'success', text: self.lang.ads.okSaveCat});
                    }else
                        Lib.Notify({type: 'error', text: self.lang.ads.falseSaveCat});

                });

            }
        }
    },

    components: {
        'video-category-item': require('./../components/video-category-item.js'),
        editor: require('./../components/editor.js'),
        'switch-box': require('./../components/switch-box.js'),
        paginator: require('./../components/paginator.js')
    }
});


setInterval(()=>{ Core.getLangData((val)=>{ p.$set('lang', Core.util.extend({}, val, true) ); }); }, Core.Time.Seconds(1));
module.exports = {};