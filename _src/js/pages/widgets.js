"use strict";

import Sortable from 'vue-sortable';
Vue.use(Sortable);

class API{
    constructor(){
        var v = 'v1',
            module = 'widgets';

        this.URL = `/api/${v}/${module}`;
    }

    getData(callback) {
        Core.API.getData(`${this.URL}/getData`, {}, callback);
    }

    save(data, callback) {
        Core.API.postResponse(`${this.URL}/save`, {}, data, callback);
    }
}

API = new API;

let components = {},
    compArr = ['widget-list-item'],
    widgetList = [
        'custom-text', 'custom-menu', 'last-news', 'last-articles'
        // , 'user-list'
    ];

for(let C of widgetList)
    components[C] = require(`../widgets/${C}`);

for(let C of compArr)
    components[C] = require(`../components/${C}`);

var p = new Vue({
    el: '#content.widgets',
    data: {
        lang: {},
        areaList: [],
        widgetList: [[],[]],
        showOverlay: false,
        showOverlay2: false,
        area: {
            title: '',
            name: ''
        }
    },

    ready(){
        let self = this;

        this.Init();
    },

    events: {
        Init(){
            let self = this;

            if(this.areaList.length == 0){

                // генерируем базовый список областей
                for(let i in this.lang.widgets.area)
                    this.areaList.push( this.onGetArea(i, this.lang.widgets.area[i]) );
            }

            API.getData((data, status, request)=>{
                if(status == 200 || data.statusCode == 200){
                    if(data.result.length>0)
                        self.$emit('setAreaData', data.result);
                }
                // else
                //     Lib.Notify({type: 'error', text: self.lang.alert.noGetData});
            });
        },

        setAreaData(list){

            for(let i in this.areaList)
                this.areaList[i].widgetList = [].concat([], list[i].widgetList);
        },

        onShowOverlay(){
            this.$set('showOverlay', true);
        },

        onHideOverlay(){
            this.$set('showOverlay', false);
        }
    },

    methods: {
        Init(){
            let self = this,
                a = setInterval(()=> {
                    if (Object.keys(self.lang).length > 0) {
                        self.$emit('Init');
                        clearInterval(a);
                    }
                }, 200);

            let i = 1;
            for(let w of widgetList){
                this.widgetList[i % 2 == 0 ? 0 : 1].push(w);
                i++;
            }
        },

        onGetArea(name, title = ''){
            var pr;

            switch(name){
                case 'top':
                case 'bottom':
                case 'right':
                case 'left':
                    pr = true;
                    break;

                default:
                    pr = false;
                    break;
            }

            return Core.util.extend({}, {
                id: 'area-'+Lib.GenerateHash(15),
                name: name,
                title: title,
                collapse: true,
                protected: pr,
                widgetList: []
            }, true);
        },

        validSysName(type){
            var name = Core.Translite(type.includes('name') ? this.area.name : this.area.title);
            this.$set('area.name', name);
        },
        
        showAreaForm(){
            this.$set('showOverlay2', true);
        },

        addArea(){

            this.areaList.push( this.onGetArea(this.area.name, this.area.title) );

            this.$set('area', {name: '', title: ''});
            this.$set('showOverlay2', false);
        },
        
        cancelArea(){
            this.$set('showOverlay2', false);
        },
        
        removeArea(idx){
            var self = this;
            UIkit.modal.confirm(self.lang.alert.del, ()=>{
                self.areaList.splice(idx, 1);
            },{ labels: { Ok: self.lang.btns.y, Cancel: self.lang.btns.n } });
        },
        
        onCollapse(item){
            item.collapse = !item.collapse;
        },

        widgetCollapse(item){
            item.collapsed = !item.collapsed;
        },

        widgetRemove(a_idx, w_idx, e){
            let self = this;
            $(e.target).closest('.widget.in-area').slideUp(200, ()=>{
                self.areaList[a_idx].widgetList.splice(w_idx, 1);
            });
        },

        endSort(e){
            let areaIdx = Number(e.item.dataset.areaIdx),
                list = this.areaList[areaIdx].widgetList;

            if(list.length>0){
                let itemOld = Core.util.extend({}, list[e.oldIndex], true),
                    itemNew = Core.util.extend({}, list[e.newIndex], true);

                list[e.newIndex] = itemOld;
                list[e.oldIndex] = itemNew;

                this.areaList[areaIdx].widgetList = [].concat([], list);
            }

        },
        
        onSave(){
            let self = this,
                data = [];

            for(let A of this.areaList){

                if(A.protected){
                    delete A.title;
                    delete A.id;
                }

                data.push(A);
            }

            API.save({list: data}, (data, status, request)=>{
                if(status == 201 || data.statusCode == 201)
                    Lib.Notify({type: 'success', text: self.lang.widgets.okSaveText});
                else
                    Lib.Notify({type: 'error', text: self.lang.widgets.falseSaveText});
            });
        }
    },

    components: components

});

setInterval(()=>{ Core.getLangData((val)=>{ p.$set('lang', Core.util.extend({}, val, true) ); }); }, Core.Time.Seconds(1));
module.exports = {};