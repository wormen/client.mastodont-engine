"use strict";

class API{
    constructor(){
        var v = 'v1',
            module = 'logs';

        this.URL = `/api/${v}/${module}`;
    }

    // init(callback) {
    //     Core.API.getData(`${this.URL}/init`, {}, callback);
    // }
    //
    // remove(id, opts, callback) {
    //     opts.id = id;
    //     Core.API.deleteResponse(`${this.URL}/remove/:id`, opts, callback);
    // }
}

API = new API;

var widgetArr = [
        // 'ga-realtime',
        'system-news'
    ],
    components = {
        'switch-box': require('./../components/switch-box.js')
    };

for(let W of widgetArr)
    components[W] = require(`./../widgets-adminpanel/${W}.js`);

var p = new Vue({
    el: '#content.main',
    data: {
        lang: {},
        widgetArr: widgetArr
    },

    ready(){

    },

    methods: {

    },

    components: components
});

setInterval(()=>{ Core.getLangData((val)=>{ p.$set('lang', Core.util.extend({}, val, true) ); }); }, Core.Time.Seconds(1));
module.exports = {};