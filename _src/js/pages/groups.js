"use strict";

var $url = Core.parseUrl();

class API{
    constructor(){
        var v = 'v1',
            module = 'groups';

        this.URL = `/api/${v}/${module}`;
    }

    loadList(callback) {
        Core.API.getData(`${this.URL}/loadList`, {}, callback);
    }

    edit(id, callback) {
        Core.API.getData(`${this.URL}/edit/:id`, {id: id}, callback);
    }

    save(data, callback) {
        Core.API.postResponse(`${this.URL}/save`, {}, data, callback);
    }

    update(id, data, callback) {
        Core.API.putResponse(`${this.URL}/update/:id`, {id: id}, data, callback);
    }

    remove(id, callback) {
        Core.API.deleteResponse(`${this.URL}/remove/:id`, {id: id}, callback);
    }
}

API = new API;





var p = new Vue({
    el: '#content.groups',
    data: {
        lang: {},
        group: {
            name: '',
            sys_name: '',
            access_cp: false,
            is_ban: false,
            captcha: false,
            moderate: false,
            sh_cls: false,
            protect: false, // защищенная
            enabled: true,
            group_discount: 0,
            description: '',
            type: 'site'
        },
        list: [],
        types: [
            {value: 'site', text: 'Site'},
            {value: 'system', text: 'System'}
        ]
    },
    ready: function() {

        if($url.query.id)
            this.loadEdit($url.query.id);
        else
            this.loadList();
    },

    methods: {
        loadList: function(){
            var self = this;

            API.loadList((data, status, request)=>{
                if(status == 200 || data.statusCode == 200)
                    self.$set('list', data.result);

                else
                    Lib.Notify({type: 'error', text: self.lang.alert.noGetData});
            });

        },

        loadEdit: function(id){
            var self = this;

            API.edit(id, (data, status, request)=>{
                if(status == 200 || data.statusCode == 200)
                    self.$set('group', data.result);

                else
                    Lib.Notify({type: 'error', text: self.lang.alert.noGetData});
            });

        },

        OnOff: function(g){
            g.enabled = !g.enabled;
            
            API.update(g._id, {enabled: g.enabled}, (data, status, request)=>{});
        },

        onDelete: function(g, e){
            e.preventDefault();
            var self = this;

            if(g.protect)
                Lib.Notify({type: 'error', text: util.format(self.lang.alert.groupNotDel, g.name) });

            else{

                UIkit.modal.confirm(self.lang.alert.del, function(){

                    API.remove(g._id, (data, status, request)=>{
                        if(status == 200 || data.statusCode == 200){
                            Lib.Notify({type: 'success', text: self.lang.groups.okDelText});
                            $(e.target).closest('tr').remove();
                        }else
                            Lib.Notify({type: 'error', text: self.lang.groups.falseDelText});
                    });

                },{ labels: { Ok: self.lang.btns.y, Cancel: self.lang.btns.n } });
            }

        },

        onSave: function(){
            var self = this;

            if($url.query.id)
                API.update(g._id, this.group, (data, status, request)=>{
                    if(status == 200 || data.statusCode == 200)
                        Lib.Notify({type: 'success', text: self.lang.groups.okSaveText});
                    else
                        Lib.Notify({type: 'error', text: self.lang.groups.falseSaveText});
                });

            else
                API.save(this.group, (data, status, request)=>{
                    if(status == 201 || data.statusCode == 201)
                        Lib.Notify({type: 'success', text: self.lang.groups.okSaveText});
                    else
                        Lib.Notify({type: 'error', text: self.lang.groups.falseSaveText});
                });

        }
    },

    components: {
        'switch-box': require('./../components/switch-box.js')
    }
});


setInterval(()=>{ Core.getLangData((val)=>{ p.$set('lang', Core.util.extend({}, val, true) ); }); }, Core.Time.Seconds(1));
module.exports = {};