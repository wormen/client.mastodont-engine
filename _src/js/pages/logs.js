"use strict";

class API{
    constructor(){
        var v = 'v1',
            module = 'logs';

        this.URL = `/api/${v}/${module}`;
    }

    init(callback) {
        Core.API.getData(`${this.URL}/init`, {}, callback);
    }

    remove(id, opts, callback) {
        opts.id = id;
        Core.API.deleteResponse(`${this.URL}/remove/:id`, opts, callback);
    }
}

API = new API;


var p = new Vue({
    el: '#content.logs',
    data: {
        lang: {},
        isLoading: true,
        all: []
    },
    ready: function() {
        var self = this;

        this.loadData();
    },

    methods: {
        loadData: function (e) {
            var self = this;

            API.init((data, status, request)=>{
                self.isLoading = false;
                if(status == 200 || data.statusCode == 200)
                    this.$set('all', data.result);
                else
                    Lib.Notify({type: 'error', text: self.$lang.alert.noGetData});
            });

            if(e)
                e.preventDefault();
        },

        onDelete: function(list, idx, id, k, e){
            var self = this;
            this.isLoading = true;

            API.remove({k: k}, (data, status, request)=>{
                self.isLoading = false;

                if(status == 200 || data.statusCode == 200){
                    // $(e.target).closest('li').remove();
                    list.splice(idx, 1);
                }
            });

        },

        checkInfo: function(str){
            if(str.xdebug_message)
                return str.xdebug_message;
            else return str;
        }
    },

    components: {

    }
});


setInterval(()=>{ Core.getLangData((val)=>{ p.$set('lang', Core.util.extend({}, val, true) ); }); }, Core.Time.Seconds(1));
module.exports = {};