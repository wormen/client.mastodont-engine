/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: olegbogdanov86@gmail.com
 ------------------------------------------
 24.01.2016
 */

var MainMinHeight = function(p){
    var H = window.innerHeight;
    $('#main, #content').css({'min-height': H+260});

    //Подсветим активные пункты меню
    var level, pv, path='', parent, now="";
    now+=location.protocol+"//"+location.hostname+(location.port ? ":"+location.port : "")+'/';
    now=location.href.substr(now.length);


    $("#sidebar-menu a").filter(function(){
        level = $(this).data('level');
        pv = now.split('/');
        if(Object.keys(pv).length>2) path = '/'+pv[0]+'/'+pv[1];
        if($(this).attr("href") == '/'+now || $(this).attr("href") == path){
            $(this).parent().addClass("active");

            //if(Number(level)>1){
            //    if(Number(level) == 2)
            //        parent = $(this).closest('ul').siblings('.accordion-toggle');
            //
            //    if(Number(level) == 3){
            //        parent = $(this).closest('ul').siblings('.accordion-toggle').addClass('menu-open')
            //            .closest('ul').siblings('.accordion-toggle');
            //    }
            //
            //    if( parent.is(':not(.menu-open)') )
            //        parent.addClass('menu-open');
            //}
        }
    });
};

var LoaderBlock = function(){
    var block = $('#loader-big'),
        H = block.height(),
        W = block.width();

    block.css({top: 60, left: (window.innerWidth-W)/2});
};



$(document).ready(function(){

    $('.nav-tabs:not(.sub)').on('click', 'a',function(e){
        e.preventDefault();

        $('.nav-tabs:not(.sub) a').each(function(){
            $(this).parent().removeClass('active');
            $($(this).attr('href')).css({display: 'none'});
        });
        $('.tab-pane:not(.sub)').removeClass('active');
        $(this).parent().addClass('active');
        $($(this).attr('href')).css({display: 'block'});
    });

    $('.panel-tabs:not(.sub)').on('click', 'a', function(e){
        e.preventDefault();

        $('.panel-tabs:not(.sub) a').each(function(){
            $(this).parent().removeClass('active');
            $($(this).attr('href')).css({display: 'none'});
        });

        $('.tab-pane:not(.sub)').removeClass('active');
        $(this).parent().addClass('active');
        $($(this).attr('href')).addClass('active').css({display: 'block'});
    });

    $('.panel-tabs.sub').on('click', 'a',function(e){
        e.preventDefault();

        var block = $(this).closest('.panel');
        block.find('li').removeClass('active');

        block.find('a').each(function(){
            $(this).parent();
            $($(this).attr('href')).css({display: 'none'});
        });

        block.find('.tab-pane.sub').removeClass('active');
        $(this).parent().addClass('active');
        $($(this).attr('href')).addClass('active').css({display: 'block'});
    });

    $(document).ready(function(){
        $(window).resize(function(){
            MainMinHeight();
            LoaderBlock();
        });

        MainMinHeight();
        LoaderBlock();
        setInterval(function(){
            MainMinHeight();
        },200);

        $('#page-loader').fadeOut();
    });

    $('#sidebar .sidebar-nav').on('click', 'a.accordion-toggle', function(e) {
        e.preventDefault();

        // If the sidebar is collapsed or in mobile mode(same thing) disable the ability to
        // collapse menu items as the entire menu has been converted for on hover use
        if ($('body').hasClass('mobile-viewport') || $('body').hasClass('sidebar-collapsed') ) {
            if ($('body').hasClass('sidebar-persist')) {}
            else if ($(this).parents('ul.sub-nav').hasClass('sub-nav')) { }
            else {return}
        }

        // Check to see if target menu is a third level nav menu. If so don't collapse parent menus
        if ($(this).parents('ul.sub-nav').hasClass('sub-nav')) {
            $(this).next('.sub-nav').slideUp('fast', 'swing', function() {
                $(this).attr('style','').prev().removeClass('menu-open');
            });
        }
        // If not a third level nav menu collapse all open menus, remove open-menu class and any animation attributes
        else {
            $('a.accordion-toggle.menu-open').next('.sub-nav').slideUp('fast', 'swing', function() {
                $(this).attr('style','').prev().removeClass('menu-open');
            });
        }

        // Expand targeted menu item, add open-menu class and remove
        // left over animation attributes
        if (!$(this).hasClass('menu-open')) {
            $(this).next('.sub-nav').slideToggle('fast', 'swing', function() {
                $(this).attr('style','').prev().toggleClass('menu-open');
            });
        }

    });

    $('#user-top-menu .user-menu').on('click', function(){
        if($(this).hasClass('open'))
            $(this).removeClass('open');
        else
            $(this).addClass('open');
    });

    $(document).ready(function(){
        setTimeout(function(){
            document.body.classList.remove("index-load");
        }, 2000);
    });


});
