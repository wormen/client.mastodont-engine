/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: olegbogdanov86@gmail.com
 */

var $Lib = new lib();

// todo исправить работу редактора

Vue.component('editor', {

    props: {
        instance: { type: 'Function' },
        id: { type: String, default: function(){ return $Lib.GenerateHash(15); } },
        lang: { type: String, default: 'ru' },
        isLoad: {type: Boolean, default: false},
        idHash: { type: String },
        name: { type: String },
        locale: { type: String, twoWay: true },
        opts: { type: String, default: function(){ return {}; } }
    },

    template: '<textarea id="{{idHash}}" v-model="value" style="display: none;"></textarea>',

    ready: function(){
        var self = this;
        self.onInit('/addons/ckeditor/ckeditor.js');

        document.addEventListener("DOMContentLoaded", function(){
            var loading = setInterval(function(){
                if (typeof(CKEDITOR) != 'undefined' && self.isLoad == false){
                    self.onLoad();
                    self.isLoad = true;
                    self.setData();
                    clearInterval(loading);
                }
            },150);
        });

        self.newID();

        if(self.$parent.$){
            if(!self.$parent.$.editor) self.$parent.$.editor = [];
            self.$parent.$.editor.push(self.idHash);
        }

        this.$watch('locale', function(newVal, oldVal){
            if(newVal == oldVal) return;
            self.setData();
        });

    },

    methods: {
        newID: function(){
            this.$set('idHash', 'editor-'+this.id);
        },

        setData: function(){
            var self = this;
            if(self.locale && self.name)
                self.instance.setData(self.$parent.page[self.locale][self.name]);
        },

        onInit: function(src){
            var newScript = document.createElement("script");
            newScript.type = "text/javascript";
            newScript.src =  src;
            var first = document.getElementsByTagName("head")[0].firstChild;
            document.getElementsByTagName("head")[0].insertBefore(newScript, first);
        },

        onLoad: function(){
            var self = this;
            var options = {
                language: self.lang,
                uiColor: '#EBEBEB',
                shiftEnterMode: CKEDITOR.ENTER_BR,
                removeButtons: 'About,Language,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,ShowBlocks'
            };
            self.opts = Vue.util.extend({}, options, (self.opts ? self.opts : {}));

            self.instance = CKEDITOR.replace(self.idHash, options);
            self.instance.on('change', self.onChange);
            self.instance.on('pasteState', self.onChange);
            self.instance.on('blur', self.onChange);
        },

        onChange: function(){
            var data = this.instance.getData();

            if (data == '') data = null;
            this.$parent.page[this.locale][this.name] = data;
        }
    }
});
//Vue.component('switch-box', );

module.exports = {
    props: {
        value: {
            type: Boolean,
            default: false,
            twoWay: true
        }
    },
    template:
    '<div class="msp-switchbox" :class="{switched: value}" @click="onChange">' +
    '  <div class="msp-switch-cont">' +
    '      <span class="msp-switch-off">Off</span>' +
    '      <div class="msp-switch-handle"></div>' +
    '      <span class="msp-switch-on">On</span>' +
    '  </div>' +
    '</div>',

    methods: {
        onChange: function(){
            var self = this;

            if(typeof self.value === 'boolean')
                self.value = !self.value;
            else
                self.value = self.value == 1 ? 0 : 1;
        }
    }
};

