"use strict";

var resource;

module.exports = {
    name: 'articles-category-item',
    props: {
        item: {
            type: Object,
            default(){ return {}; },
            twoWay: true
        },

        lang: {
            type: Object,
            default(){ return {}; }
        }
    },
    template: require('./tpl/articles-category-item.html'),

    ready(){
        var self = this;

        setInterval(()=>{ self.$set('lang', Vue.util.extend({}, app.lang) ); },150);
    },

    methods: {
        onEdit(item){
            var url = Core.parseUrl();
            url.query.action = 'edit-category';
            url.query.id = item._id;

            window.location.href = url.pathname+Core.param(url.query);
        },

        onDelete(e, id){

            var self = this;
            var ids = this.getChildList(id);

            var text = this.lang.articles.del1item;
            if(ids.length>1)
                text = this.lang.articles.delMoreItems;

            UIkit.modal.confirm(text, ()=>{

                self.$dispatch('categoryDelete', ids[i], e);

            },{ labels: { Ok: self.lang.btns.y, Cancel: self.lang.btns.n } });

            e.preventDefault();

        },

        onActiveStatus(item){
            item.enabled = !item.enabled;

            this.$dispatch('categoryOnOff', item._id, item.enabled);
        },

        getChildList(id){
            var arr=[id];
            $('[data-id="'+id+'"]').find('[data-id]').each(()=>{
                arr.push( $(this).data('id') );
            });
            return arr;
        }
    }

};