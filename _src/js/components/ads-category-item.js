"use strict";

var collapseState = {};

class API{

}
API = new API;

module.exports = {
    name: 'ads-category-item',
    props: {
        item: {
            type: Object,
            default(){ return {}; },
            twoWay: true
        },

        lang: {
            type: Object,
            default(){ return {}; }
        },

        api: {
            type: String,
            default: '/api/v1/ads'
        }
    },
    template: require('./tpl/ads-category-item.html'),

    ready(){
        var self = this;

        setInterval(()=>{ Core.getLangData((val)=>{ self.lang = Core.util.extend({}, val, true); }); }, Core.Time.Seconds(1));
    },

    methods: {
        onEdit(id){
            this.$dispatch('categoryEdit', id);
        },

        onDelete(e, id){
            var self = this,
                ids = this.getChildList(id),
                text = this.lang.ads.del1item;

            if(ids.length>1)
                text = this.lang.ads.delMoreItems;

            UIkit.modal.confirm(text, function(){

                self.$dispatch('categoryDeleteAll', ids, e);

            },{ labels: { Ok: self.lang.btns.y, Cancel: self.lang.btns.n } });

            e.preventDefault();
        },

        onActiveStatus(item){
            item.enabled = !item.enabled;
            this.$dispatch('categoryUpdate', item._id, {enabled: item.enabled});
        },

        getChildList(id){
            var arr=[id];
            $('[data-id="'+id+'"]').find('[data-id]').each(function(){
                arr.push( $(this).data('id') );
            });
            return arr;
        },

        stateCollapseSet(id){
            if(!collapseState[id])
                collapseState[id] = true;
            else
                collapseState[id] = !collapseState[id];
        },

        stateCollapseGet(id){
            return Boolean(collapseState[id]);
        }
    }

};