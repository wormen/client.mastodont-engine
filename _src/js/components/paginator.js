/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: olegbogdanov86@gmail.com
 ------------------------------------------
 29.03.2016
 */

"use strict";

module.exports = {
    name: 'paginator',
    props: {
        id: {
            type: String, default(){
                return 'paginator-' + Lib.GenerateHash(15);
            }
        },
        data: {
            type: Object,
            default(){
                return {
                    list: [],
                    paginator: {
                        next: null,
                        pages: [],
                        prev: null,
                        total: 0
                    }
                };
            },
            twoWay: true
        },
        thisPage: {
            type: Number,
            default: 1
        }
    },

    ready(){
        var self = this;
    },

    template: require('./tpl/paginator.html'),

    methods: {
        Go(url, e){
            e.preventDefault();
            e.stopPropagation();

            for(let i in this.data.paginator.pages){

                this.data.paginator.pages[i].isCurrent = false;

                if(this.data.paginator.pages[i].url.includes(url)){
                    this.thisPage = this.data.paginator.pages[i].num;
                    this.data.paginator.pages[i].isCurrent = true;
                }
            }

            var self = this;
            resource = this.$resource(url);
            resource.get({}, function (data, status, request) {
                if(status == 200){
                    self.$set('data.list', data.list);

                    for(let i in data.paginator.pages)
                        data.paginator.pages[i].isCurrent = (data.paginator.pages[i].num === self.thisPage);

                    self.$set('data.paginator', data.paginator);
                }
            }).error(function (data, status, request) {});
        }
    }
};