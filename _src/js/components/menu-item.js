"use strict";

var collapseState = {};

module.exports = {
    name: 'menu-item',
    props: {
        item: {
            type: Object,
            default(){ return {}; },
            twoWay: true
        }
    },

    data(){
        return {
            lang: {}
        }
    },

    template: require('./tpl/menu-item.html'),

    ready(){
        var self = this;
        setInterval(()=>{ Core.getLangData((val)=>{ self.$set('lang', Core.util.extend({}, val, true)); }); }, 150);
    },

    methods: {
        onEdit(id){
            var self = this;
            this.$dispatch('edit', id);
        },

        onDelete(e, id){
            var self = this;
            var ids = self.getChildList(id);

            UIkit.modal.confirm(self.lang.alert.del, function(){
                
                self.$dispatch('deleteAll', ids, e);

            },{ labels: { Ok: self.lang.btns.y, Cancel: self.lang.btns.n } });

        },

        onActiveStatus(item){
            item.enabled = !item.enabled;
            this.$dispatch('onOff', item._id, item.enabled);
        },

        getChildList(id){
            var arr=[id];
            $('[data-id="'+id+'"]').find('[data-id]').each(function(){
                arr.push( $(this).data('id') );
            });
            return arr;
        },

        stateCollapseSet(id){
            // this.$dispatch('stateCollapseSet', id);
        },

        stateCollapseGet(id){
            // return Boolean(this.$root.collapseState[id]);
        }
    }

};