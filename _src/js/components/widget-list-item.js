/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: olegbogdanov86@gmail.com
 ------------------------------------------
 */

"use strict";

module.exports = {
    template: require('./tpl/widget-list-item.html'),
    props: {
        name: { type: String }
    },

    data(){
        return {
            lang: {},
            areaList: [],
            isQuestion: false,
            chooserSelected: {},
            activeArea: ''
        }
    },

    ready(){
        var self = this;
        this.areaList = this.$parent.areaList;

        setInterval(()=>{ Core.getLangData((val)=>{ self.$set('lang', Core.util.extend({}, val, true)); })}, Core.Time.Seconds(1));
    },

    methods: {
        onSelectArea(){
            this.$set('isQuestion', true);
            this.$dispatch('onShowOverlay');
        },
        
        onAdd(name){
            this.$set('isQuestion', false);
            this.$dispatch('onHideOverlay');

            for(let i in this.areaList)
                if(this.areaList[i].name.includes(this.activeArea)){
                    this.inArea(i, name);
                    break;
                }
        },
        
        onCancel(){
            this.$set('isQuestion', false);
            this.$dispatch('onHideOverlay');
        },
        
        isSelected(name){
            if(Object.keys(this.chooserSelected).length == 0)
                this.chooserSelected[name] = true;
            return this.chooserSelected[name];
        },

        onSelect(name){
            this.$set('chooserSelected', {});
            this.$set('activeArea', name);
            return this.isSelected(name);
        },

        inArea(idx, name){
            let obj = {
                name: name,
                title: this.lang.widgets.widgetList[name].title,
                collapsed: true,
                data: {}
            };

            this.areaList[idx].widgetList.push(Core.util.extend({}, obj, true));
        }
    }
};

