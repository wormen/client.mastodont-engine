/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: olegbogdanov86@gmail.com
 ------------------------------------------
 */

"use strict";

var name = 'google';

class Map{

    constructor(){
        this.apiKey = '';

        var query = {};
        if(this.apiKey.length>0)
            query.key = this.apiKey;

        this.mapUrl = 'http://maps.google.com/maps/api/js'+Core.param(query);
        this.map = null;
        this.Placemark = null;
        this.geocode = [0, 0];
        this.markers = [];
    }

    $set(property, value){
        this[property] = value;
    }

    clearMarkers(){
        for(let i in this.markers){
            this.markers[i].setMap(null);
            this.markers.splice(i, 1);
        }
    }

    Update(config){

    }
}
Map = new Map;


module.exports = {
    name: name,
    template: require(`./tpl/${name}.html`),

    props: {
        id: {
            type: String,
            default: Lib.GenerateHash()
        },
        key: {
            type: String,
            twoWay: true
        },
        config: {
            type: Object,
            default(){ return {}; },
            twoWay: true
        }
    },

    ready(){
        var self = this;
        Map.$set('apiKey', this.key);
        
        Core.scriptLoad(Map.mapUrl).then(() => {
            self.$emit('onInit');
        });

        window.addEventListener('resize', ()=>{
            self.$emit('setCenter', [self.config.center[0], self.config.center[1]]);
        }, true);
    },

    watch: {
        key: {
            deep: true,
            handler(val, oldVal){
                Map.$set('apiKey', val);
            }
        }
    },

    events: {
        setCenter(data){
            this.$set('config.center', data);
            try{
                Map.map.setCenter(new google.maps.LatLng(data[0], data[1]));
            }catch(e){}
        },

        setZoom(zoom){
            this.$set('config.zoom', zoom);

            try{
                Map.map.setZoom(zoom);
            }catch(e){}
        },

        setHint(){
            this.$emit('clearMarkers');
            this.$emit('setMarker', this.config.center[0], this.config.center[1]);
        },

        mapUpdate(config){
            config = config ? config : this.config;
            Map.Update(config);
        },

        setMarker(x, y){
            var self = this;
            this.$emit('clearMarkers');

            try{

                var Placemark = new google.maps.Marker({
                    position: new google.maps.LatLng(x, y),
                    map: Map.map,
                    title: this.config.hint,
                    draggable: true
                });

                google.maps.event.addListener(Placemark, 'dragend', function (event) {
                    self.$emit('setCenter', [this.getPosition().lat(), this.getPosition().lng()]);
                });

                Map.$set('Placemark', Placemark);
                Map.markers.push(Placemark);

            }catch(e){}

            this.$emit('setCenter', [x, y]);
        },

        clearMarkers(){
            Map.clearMarkers();
        },

        checkAddress(){
            var self = this;
            try{

                var geocoder = new google.maps.Geocoder();
                geocoder.geocode({'address': this.config.address, 'partialmatch': true}, (results, status)=> {
                    if (status == 'OK' && results.length > 0) {
                        var r = results[0],
                            loc = r.geometry.location,
                            center = [loc.lat(), loc.lng()];

                        self.$emit('setCenter', center);
                    }
                });


            }catch(e){}
        },

        onInit(){
            var self = this;

            //(долгота, широта)
            var center = new google.maps.LatLng(self.config.center[0], self.config.center[1]),
                mapOptions = {
                    zoom: self.config.zoom,
                    center: center,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    zoomControl: true,
                    mapTypeControl: false,
                    scaleControl: false,
                    streetViewControl: false,
                    rotateControl: false,
                    zoomControlOptions: {
                        position: google.maps.ControlPosition.LEFT_TOP
                    }
                },
                map = new google.maps.Map(document.getElementById(`map-${self.id}`), mapOptions);//инициализация карты



            Map.$set('map', map);

            google.maps.event.addListener(map, 'click', (e)=> {
                var center = [e.latLng.lat(), e.latLng.lng()];
                self.$emit('setMarker', center[0], center[1]);
            });

            google.maps.event.addListener(map, 'zoom_changed', ()=> {
                self.$emit('setZoom', map.getZoom());
            });
            
            this.$emit('setMarker', this.config.center[0], this.config.center[1]);
        }
    },

    destroyed(){
        if(window.google)
            delete window.google;

        Map.$set('map', null);
        Map.$set('Placemark', null);
        Map.$set('geocode', [0, 0]);
    }
};