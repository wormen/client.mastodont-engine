/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: olegbogdanov86@gmail.com
 ------------------------------------------
 */

"use strict";

var name = 'yandex';

class Map{
    constructor(){
        this.apiKey = '';
        this.mapUrl = 'http://api-maps.yandex.ru/2.1/?lang=ru_RU';
        this.ymaps = null;
        this.map = null;
        this.isCreated = false;
        this.Placemark = null;
        this.geocode = [0, 0];
    }

    $set(property, value){
        this[property] = value;
    }

    checkGeoCode(newCode) {
        if (this.geocode.length !== newCode.length)
            return false;

        for (var i = this.geocode.length; i--;) {
            if (this.geocode[i] !== newCode[i])
                return false;
        }
        return true;
    }

    Update(config){
        var self = this;

        if(this.map && this.isCreated){

            this.map.setCenter(config.center, config.zoom);
            this.Placemark.geometry.setCoordinates(config.center);

            this.Placemark.properties.set({
                hintContent: config.hint
            });


            // console.log(this.Placemark);
        }
    }

}
Map = new Map;


module.exports = {
    name: name,
    template: require(`./tpl/${name}.html`),

    props: {
        id: {
            type: String,
            default: Lib.GenerateHash()
        },
        key: {
            type: String,
            twoWay: true
        },
        config: {
            type: Object,
            default(){ return {}; },
            twoWay: true
        }
    },

    ready(){
        var self = this;
        Map.$set('apiKey', this.key);

        Core.scriptLoad(Map.mapUrl).then(() => {
            self.$emit('onInit');
        });

    },

    watch: {
        key: {
            deep: true,
            handler(val, oldVal){
                Map.$set('apiKey', val);
            }
        },

        'config.zoom+config.center': {
            deep: true,
            handler(val, oldVal){
                this.onChangeConfig();
            }
        }
    },

    events: {
        onInit(){
            var self = this;

            if(!ymaps)
                setTimeout(()=>{ self.$emit('onInit'); }, 250);

            else{

                ymaps.ready(()=>{
                    Map.$set('isCreated', true);
                    self.$emit('onLoad');
                });
            }
        },

        mapUpdate(config) {
            config = config ? config : this.config;
            Map.Update(config);
        },

        setCenter(data){
            this.$set('config.center', data);
            this.$emit('mapUpdate', this.config);
        },

        setZoom(zoom){
            this.$set('config.zoom', zoom);
            this.$emit('mapUpdate', this.config);
        },

        setHint(){
            this.$emit('mapUpdate');
        },

        checkAddress(){
            var self = this;
            try{
                var geocode = ymaps.geocode(this.config.address);
                geocode.then((res)=> {
                    var center = res.geoObjects.get(0).geometry.getCoordinates();

                    if(!Map.checkGeoCode(center)){
                        self.$emit('setCenter', center);
                        Map.geocode = center;
                    }

                });
            }catch(e){}
        },

        onLoad(){
            var map, self = this;

            try{

                map = new ymaps.Map(`map-${self.id}`, {
                    center: self.config.center,
                    zoom: self.config.zoom,
                    controls: []
                });

                var zoomControl = new ymaps.control.ZoomControl({
                    options: {
                        size: "small",
                        position: { top: 10, left: 10 }
                    }
                });

                zoomControl.events.add([ 'click' ], (e)=> {
                    self.config.zoom = e.get('target').state._data.zoom;
                });

                map.controls.add(zoomControl);

                var Placemark = new ymaps.Placemark(self.config.center,
                    { hintContent: self.config.hint },
                    {
                        draggable: true, // метку можно перемещать
                        preset: 'islands#blueIcon'
                    });

                Placemark.events.add([ 'mapchange', 'dragend' ], (e)=> {
                    var center = e.get('target').geometry._coordinates;
                    self.$emit('setCenter', center);
                });

                map.geoObjects.add(Placemark);
                
                // Выставлям координаты из точки клика по карте
                map.events
                    .add('click', (e)=> {
                        var coords = e.get('coords');
                        self.$emit('setCenter', coords);
                    })
                    .add('wheel', (e)=> {
                        self.$emit('setZoom', map.getZoom());
                    });

                Map.$set('Placemark', Placemark);
            }catch(e){ }

            Map.$set('map', map);
        }
    },

    methods: {
        onChangeConfig(){
            if(Map.map){
                Map.map.container.fitToViewport(); // привести свои размеры к размерам контейнера
                this.$emit('mapUpdate');
            }
        }
    },

    destroyed(){

        if(Map.map)
            Map.map.destroy();

        Map.$set('map', null);
        Map.$set('ymaps', null);
        Map.$set('isCreated', false);
        Map.$set('Placemark', null);
        Map.$set('geocode', [0, 0]);
    }
};
