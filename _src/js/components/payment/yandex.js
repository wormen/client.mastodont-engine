"use strict";

module.exports = {
    name: 'yandex',
    props: {
        lang:{
            type: Object,
            default(){ return {}; }
        },
        item:{
            type: Object,
            default(){ return {}; },
            twoWay: true
        }
    },
    template: require('./tpl/yandex.html'),

    ready(){

    },

    methods: {

    },

    components: {
        'switch-box': require('./../../components/switch-box')
    }
};