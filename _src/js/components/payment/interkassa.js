"use strict";

export default {
    name: 'interkassa',
    props: {
        lang:{
            type: Object,
            default(){ return {}; }
        },
        item:{
            type: Object,
            default(){ return {}; },
            twoWay: true
        }
    },
    template: require('./tpl/interkassa.html'),

    ready(){

    },

    methods: {

    },

    components: {
        'switch-box': require('./../../components/switch-box')
    }
}