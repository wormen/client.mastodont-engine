"use strict";

module.exports = {

    props: {
        show: {type: Boolean, default: false},
        locale: { type: String, default: 'rus' },
        lang: { type: Object, default(){ return {}; } },
        localeList: { type: Array, default(){ return []; } },
        current: {type: Object, default(){ return {title: '', locale: ''}; }}
    },

    template: require('./tpl/locale-content.html'),

    ready(){
        var self = this;
        //socket.on('appData', function(r){
        //    switch(r.action){
        //        case 'load:langs':
        //            self.$set('localeList', r.data);
        //            break;
        //    }
        //});

        this.$watch('localeList', (newVal, oldVal)=> {
            if (newVal == oldVal) return;
            self.onSelect(Lib.contentLocale(), null);
        });

        this.$watch('locale', (newVal, oldVal)=> {
            if (newVal == oldVal) return;

            var locale = Lib.contentLocale();
            self.locale = locale != null ? locale : self.locale;

            self.onSelect(self.locale, null);
        });

        self.setLocale(self.locale);
    },

    methods: {
        getImg(locale){
            return locale ? '/img/lang/24/'+locale+'.png' : '';
        },

        onShow(){
            this.show = !this.show;
        },

        setLocale(locale){
            sessionStorage.setItem('contentLocale', locale);
        },

        onSelect(locale, isHide){
            var self = this;

            if(locale != null)
                self.setLocale(locale);

            locale = locale ? locale : self.locale;

            for(var i in self.localeList){
                if(self.localeList[i].locale == locale)
                    self.current = {title: self.localeList[i].title, locale: locale}
            }

            if(isHide != null){
                self.onShow();
                self.locale = locale;
            }
        }

    }
};