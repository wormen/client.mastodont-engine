
var getScrollBarWidth = function(){
    var inner = document.createElement('p');
    inner.style.width = '100%';
    inner.style.height = '200px';

    var outer = document.createElement('div');
    outer.style.position = 'absolute';
    outer.style.top = '0px';
    outer.style.left = '0px';
    outer.style.visibility = 'hidden';
    outer.style.width = '200px';
    outer.style.height = '150px';
    outer.style.overflow = 'hidden';
    outer.appendChild(inner);

    document.body.appendChild(outer);
    var w1 = inner.offsetWidth;
    outer.style.overflow = 'scroll';
    var w2 = inner.offsetWidth;
    if (w1 === w2) w2 = outer.clientWidth;

    document.body.removeChild(outer);

    return (w1 - w2)
};

var EventListener = {
    /**
     * Listen to DOM events during the bubble phase.
     *
     * @param {DOMEventTarget} target DOM element to register listener on.
     * @param {string} eventType Event type, e.g. 'click' or 'mouseover'.
     * @param {function} callback Callback function.
     * @return {object} Object with a `remove` method.
     */
    listen: function (target, eventType, callback) {
        var className = 'no-scroll';

        if (target.addEventListener) {
            target.addEventListener(eventType, callback, false);
            return {
                remove: function () {
                    target.removeEventListener(eventType, callback, false);
                    document.body.classList.remove(className);
                }
            }
        } else if (target.attachEvent) {
            target.attachEvent('on' + eventType, callback);
            return {
                remove: function () {
                    target.detachEvent('on' + eventType, callback);
                    document.body.classList.remove(className);
                }
            }
        }
    }
};

module.exports = {
    name: 'sidebar',
    props: {
        show: {
            type: Boolean,
            require: true,
            twoWay: true
        },
        placement: {
            type: String,
            default: 'right'
        },
        header: {
            type: String,
            default: ''
        },
        extclass: {
            type: String,
            default: '',
            twoWay: true
        },
        width: {
            type: Number,
            default: 320
        },
        isTransition: {
            type: String,
            default: 'slideright'
        }
    },

    template:
    '<div id="aside-{{placement}}" class="aside {{extclass}} {{placement}}" :style="{width:width + \'px\'}" v-show="show" :transition="\'slide\'+placement">' +
    '   <div class="aside-dialog">' +
    '       <div class="aside-content">' +
    '           <div class="aside-header">' +
    '               <button type="button" class="close" @click="close"><span>&times;</span></button>' +
    '               <h4 class="aside-title">{{header}}</h4>' +
    '           </div>' +
    '           <div class="aside-body">' +
    '               <slot></slot>' +
    '           </div>' +
    '       </div>' +
    '   </div>' +
    '</div>',

    ready(){
        var self = this;
        this.$watch('show', function(newVal, oldVal){
            if(newVal == oldVal)return;

            var backdrop = document.createElement('div'),
                body = document.body;

            backdrop.className = 'aside-backdrop';
            var scrollBarWidth = getScrollBarWidth();

            if (newVal) {
                body.appendChild(backdrop);
                body.classList.add('modal-open');
                if (scrollBarWidth !== 0) {
                    body.style.paddingRight = scrollBarWidth + 'px'
                }
                // request property that requires layout to force a layout
                var x = backdrop.clientHeight;
                backdrop.className += ' in';
                this._clickEvent = EventListener.listen(backdrop, 'click', this.close)
            } else {
                if (this._clickEvent) this._clickEvent.remove();
                backdrop = document.querySelector('.aside-backdrop');
                backdrop.className = 'aside-backdrop';
                setTimeout(function(){
                    body.classList.remove('modal-open');
                    body.style.paddingRight = '0';
                    body.removeChild(backdrop);
                },300);
            }

            self.isTransition = 'slide'+self.placement;
        });
    },

    methods: {
        close: function () {
            this.show = false;
            $('body').removeClass('no-scroll');
        },

        onSelect: function(D){
            // if(D.name != App.activeDomain)
            //     socket.emit('domains', {action: 'select', data: {_id: D._id, name: D.name, balans: D.balans}});
        }
    }
};

