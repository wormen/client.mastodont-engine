"use strict";

var $url = Core.parseUrl();

var lang = document.createElement('script');
lang.src = "/fm/js/i18n/elfinder."+(Cookies.get('mst_locale') == 'rus' ? 'ru' : 'en')+".js";
lang.type = 'text/javascript';
lang.charset='UTF-8';
document.head.appendChild(lang);

module.exports = {
    name: 'fm-block',
    props: {
        lang: { type: Object, default() { return $AppData.lang; } },
        id: { type: String, default: Lib.GenerateHash(15) },
        locale: {
            type: String,
            default(){
                var loc = localStorage.getItem('locale');
                if(!loc) loc = 'rus';
                return loc;
            }
        },
        dir: {type: String, default: ''}
    },

    ready(){
        var self = this,
            URL = '/fm/php/connector.php';

        this.elfinder = null;

        this.opts = {
                url  : URL, // connector URL (REQUIRED)
                lang : self.locale == 'rus' ? 'ru' : 'en',
                uplMaxSize: '1G',
                separator: '/',
                rememberLastDir : false,

                handlers: {

                },
                contextmenu: {
                    files: self.onContextmenu()
                }
            };

        self.goDir();
        setTimeout(()=>{ self.onInit(); }, 1000);

        setInterval(()=>{ self.$set('lang', Vue.util.extend({}, app.lang) ); },100);
    },

    template: '<div id="elfinder-{{id}}"></div>',

    methods: {

        onInit(){
            var _self = this;

            //todo проверяем mime, если не FM

            window.elFinder.prototype._options.commands.push('inDescription');
            window.elFinder.prototype._options.contextmenu.files.push('inDescription');
            window.elFinder.prototype._options.commandsOptions.inDescription = {
                onlyURL  : false,
                multiple : true, // allow to return multiple files info
                folders  : false // allow to return filers info
            };

            window.elFinder.prototype._options.commands.push('inAnonce');
            window.elFinder.prototype._options.contextmenu.files.push('inAnonce');
            window.elFinder.prototype._options.commandsOptions.inAnonce = window.elFinder.prototype._options.commandsOptions.inDescription;

            window.elFinder.prototype.i18[self.locale == 'rus' ? 'ru' : 'en'].messages['cmdinAnonce'] = this.lang.fm.inAnonce;
            window.elFinder.prototype.i18[self.locale == 'rus' ? 'ru' : 'en'].messages['cmdinDescription'] = this.lang.fm.inDescription;

            window.elFinder.prototype.commands.inAnonce = function() {
                var self = this,
                    filter = function(files) {
                        var o = self.options;

                        files = $.map(files, function(file) {
                            return file.mime != 'directory' || o.folders ? file : null;
                        });

                        return o.multiple || files.length == 1 ? files : [];
                    };

                this.getstate = function(sel) {
                    return filter(this.files(sel)).length == 0 ? -1 : 0;
                };

                this.exec = function(hashes) {
                    var files = this.files(hashes);
                    _self.inEditor(0, files);
                };
            };

            window.elFinder.prototype.commands.inDescription = function() {
                var self = this,
                    filter = function(files) {
                        var o = self.options;

                        files = $.map(files, function(file) {
                            return file.mime != 'directory' || o.folders ? file : null;
                        });

                        return o.multiple || files.length == 1 ? files : [];
                    };

                this.getstate = function(sel) {
                    return filter(this.files(sel)).length == 0 ? -1 : 0;
                };

                this.exec = function(hashes) {
                    var files = this.files(hashes);
                    var editors = 0;

                    if(window.CKEDITOR != undefined)
                        editors = Object.keys(window.CKEDITOR.instances);

                    _self.inEditor((editors>0 ? 1 : 0), files);
                };
            };

            this.elfinder = $('#elfinder-'+this.id).elfinder(this.opts).elfinder('instance');

            _self.checkEditor(); // редактор
            this.elfinder.bind('contextmenu', function(event, elfinderInstance) {
                _self.checkEditor(); // редактор
            });

        },

        checkEditor(){
            return window.CKEDITOR != undefined;
        },

        onContextmenu(){
            var self = this,
                arr = [
                '|', 'open', 'quicklook', '|', 'download', '|', 'copy', 'cut', 'paste', 'duplicate', '|',
                'rm', '|', 'edit', 'rename', 'resize', '|', 'archive', 'extract', '|', 'info'
            ];

            var file = ['getfile'];

            switch($url.query.module){

                case 'static':
                    file.push('inDescription');
                    break;

                case 'news':
                    file.push('inAnonce');
                    file.push('inDescription');
                    break;
            }

            return [].concat(file, arr);
        },

        getEditor(num){
            var i = 0,
                editors = window.CKEDITOR.instances;
            
            for(var k in editors){
                if(i == num)
                    return editors[k];

                i++;
            }
        },

        checkMime(file) {
            var isOk = false;

            if(file.mime.indexOf('image/') != -1) isOk = true;
            else{
                var mimeArr = [];
                for(var i in mimeArr)
                    if(file.mime == mimeArr[i])
                        return true;

                isOk = 'no';
            }

            return isOk;
        },

        inEditor(num, files) {
            var self = this;
            if(!this.checkEditor()) return;

            var checkFile, tag, file, href,
                isInsert = true,
                Editor = this.getEditor(num);

            for(var i in files){
                if(!files[i]) return;

                file = files[i];
                href = '/uploads/'+Core.Base64.decode(file.hash.split('_')[1]).replace(/\\/g, '/');
                checkFile = this.checkMime(file);

                if(checkFile == true){
                    tag = Editor.document.createElement('img');
                    tag.setAttribute( 'src', href );
                    tag.setAttribute( 'alt', '' );
                }

                else{
                    if(checkFile == 'no'){
                        isInsert = false;
                        Lib.Notify({type: 'error', text: self.lang.fm.noInsert.replace('%s', file.name) });

                    }

                    else{
                        tag = Editor.document.createElement('a');
                        tag.setAttribute( 'href', href );
                        tag.setAttribute( 'target', '_blank' );
                        tag.setText( 'File link' );
                    }
                }

                if(isInsert){
                    Editor.insertElement(tag);
                    Editor.insertHtml(' ');
                }

            }

        },

        goDir(){
            var self = this,
                path = [];

            if($url.query.module != 'fm'){
                path.push($url.query.module);

                switch($url.query.section){

                    //path.push($url.query.section);

                    default:
                        break;
                }

                if($url.query.id)
                    path.push($url.query.id);
                else
                    path.push(self.dir);

                path = path.join('/');

                window.location.hash = 'elf_l1_' + Core.Base64.encode(path);
            }else
                return '';
        }
    }
};