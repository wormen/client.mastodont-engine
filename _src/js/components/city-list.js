"use strict";

module.exports = {

    props: {
        show: {type: Boolean, default: false},
        lang: { type: Object, default(){ return {}; } },
        city: {type: Number, default: 0},
        cityList: { type: Array, default(){ return []; } },
        N: {type: Object, default(){ return {_id: 0, title: '', region: ''}; }},
        current: {type: Object, default(){ return {_id: 0, title: '', region: ''}; }},
        locale: { type: String, default: 'rus' }
    },

    template: require('./tpl/city-list.html'),

    ready(){
        var self = this,
            city = Lib.contentCity();

        //socket.on('appData', function(r){
        //    switch(r.action){
        //        case 'load:lang':
        //            self.$set('lang', r.data);
        //            if(self.current.title.length==0)
        //                self.current.title = self.N.title = r.data.btns.cityUnknown;
        //            break;
        //
        //        case 'get:city:list':
        //            self.$set('cityList', r.data);
        //            self.setCity(city);
        //            break;
        //    }
        //});

        this.$watch('locale', (val)=>{
            self.setCity(self.city);
        });

        this.setCity(city);
        //socket.emit('init', {action: 'get:city'});
    },

    methods: {

        onShow(){
            this.show = !this.show;
        },

        setCity(id){
            var self = this;

            if(id > 0){
                for(var i in self.cityList){
                    if(self.cityList[i]._id == id){
                        self.current = self.cityList[i];
                        break;
                    }
                }
            }else{
                self.current = self.N;
                id = self.N._id;
            }

            self.$set('city', id);
            sessionStorage.setItem('contentCity', id);
        },

        onSelect(city, isHide){

            this.setCity(city);
            if(isHide != null) this.onShow();
        }
    }
};