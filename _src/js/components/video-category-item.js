"use strict";

Vue.use(VueResource);

var resource;

module.exports = {
    name: 'video-category-item',
    props: {
        item: {
            type: Object,
            default(){ return {}; },
            twoWay: true
        },

        lang: {
            type: Object,
            default(){ return {}; }
        },

        api: {
            type: String,
            default: '/api/v1/video'
        }
    },
    template: require('./tpl/video-category-item.html'),

    ready: function(){
        var self = this;

        setInterval(()=>{ Core.getLangData((val)=>{ self.lang = Core.util.extend({}, val, true); }); }, Core.Time.Seconds(1));
    },

    methods: {
        onEdit: function(id){
            var self = this;

            var resource = this.$resource(this.api);
            resource.get({type: 'category', id: id}, function (data, status, request) {
                if(status == 200)
                    self.$root.category = Vue.util.extend({}, data);

                if(self.$root.category.channals.length == 0)
                    self.$root.addChannel();

                UIkit.modal("#category-item").show();
            }).error(function (data, status, request) {
                Lib.Notify({type: 'error', text: self.lang.alert.noGetData});
            });
        },

        onDelete: function(e, id){
            var self = this;
            var ids = this.getChildList(id);

            var text = this.lang.video.del1item;
            if(ids.length>1)
                text = this.lang.video.delMoreItems;

            resource = this.$resource(this.api);
            UIkit.modal.confirm(text, function(){

                for(var i in ids){
                    resource.delete({type: 'category', id: ids[i]}, function (data, status, request) {

                        if(ids.length-1 || ids.length == 1){
                            $(e.target).closest('.uk-nestable-item').remove();

                            text = self.lang.video.okDelCat;
                            if(ids.length>1)
                                text = self.lang.video.okDelCatMore;

                            Lib.Notify({type: 'success', text: text});
                        }

                    }).error(function (data, status, request) {

                    });
                }

            },{ labels: { Ok: self.lang.btns.y, Cancel: self.lang.btns.n } });

            e.preventDefault();

        },

        onActiveStatus: function(item){
            item.enabled = !item.enabled;

            resource = this.$resource(this.api);
            resource.update({type: 'category', id: item._id}, {enabled: item.enabled}, function (data, status, request) {
            }).error(function (data, status, request) {
            });
        },

        getChildList: function(id){
            var arr=[id];
            $('[data-id="'+id+'"]').find('[data-id]').each(function(){
                arr.push( $(this).data('id') );
            });
            return arr;
        }
    }

};