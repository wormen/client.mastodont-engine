"use strict";

module.exports = {
    props: {
        id: { type: String, default(){ return 'fm-'+Lib.GenerateHash(15); } },
        label: { type: String },
        dir: {type: String, default: ''}
    },

    ready(){
        var self = this;
    },

    template: require('./tpl/fm-btn.html'),

    components: {
        'fm-block': require('./fm-block.js')
    }
};