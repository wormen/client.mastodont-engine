module.exports = {
    props: {
        value: {
            type: Boolean,
            default: false,
            twoWay: true
        }
    },
    template:
    '<div class="msp-switchbox" :class="{switched: value}" @click="onChange">' +
    '  <div class="msp-switch-cont">' +
    '      <span class="msp-switch-off">Off</span>' +
    '      <div class="msp-switch-handle"></div>' +
    '      <span class="msp-switch-on">On</span>' +
    '  </div>' +
    '</div>',

    methods: {
        onChange(){
            var self = this;

            if(typeof self.value === 'boolean')
                self.value = !self.value;
            else
                self.value = self.value == 1 ? 0 : 1;
        }
    }
};

