
module.exports = {
    account: {
        enabled: true,
        linkModule: 'account',
        links: {
            ads: 'ads',
            'create-ad': 'create-ad',
            messages: 'messages',
            'pay-service': 'pay-service',
            'pay-history': 'pay-history',
            register: 'register',
            lostpass: 'lostpass',
            changepass: 'changepass',
            settings: 'settings',
            orders: 'orders',
            reservation: 'reservation',
            wishlist: 'wishlist',
            subscription: 'subscription',
            support: 'support',
            'user-view': 'user-view'
        },
        antibruteCnt: 5,
        antibruteTime: 15,
        on: false,
        loginLength: 25,
        passLength: 7,
        regType: 'none',
        regActiveTime: 24,
        regUnactivated: 'none'
    },
    news: {
        enabled: true,
        linkModule: 'news',
        inPage: 20,
        remark: 3,
        lowmark: -3,
        highmark: 3,
        onRating: false,
        mark_details: false,
        mark_users: false
    },
    static: {
        enabled: true,
        linkModule: 'static',
        onRating: false
    },
    ads: {
        enabled: true,
        linkModule: 'ads',
        moderateStatus: 'moderation',
        links: {
            category: 'category',
            tag: 'tag',
            ad: 'ad',
            new: 'new',
            user: 'user'
        },
        inPage: 10,
        inPageAdmin: 1000,
        defaultStatus: 'moderation'
    },
    articles: {
        enabled: true,
        linkModule: 'articles',
        links: {
            category: 'category',
            tag: 'tag',
            post: 'post'
        },
        inPage: 10
    },
    shop: {
        enabled: true,
        linkModule: 'shop',
        links: {
            category: 'category',
            product: 'product',
            tag: 'tag',
            cart: 'cart',
            order: 'order',
            cancelOrder: 'cancel-order',
            newProducts: 'new-products',
            sells: 'sells',
            collection: 'collection',
            collections: 'collections'
        },
        inPage: 16,
        inPageSearch: 32,
        inPageNewProduct: 50,
        cancelTimeOrder: 24,
        commentOn: true,
        one_click: false,
        viewChildCat: true,
        grossMargin: 0,
        grossMarginOn: false,
        totalDiscount: 0,
        totalDiscountOn: false,
        priorityDiscountsOn: false,
        priorityDiscount: 'null',
        curencyDefault: 0,
        setDiscountsOn: false,
        autoReMath: false,
        markUpRate: 0,
        summarizeDiscounts: false,
        SummarizeDiscounts: {
            category: false,
            product: false,
            setDiscounts: false,
            priorityDiscount: false
        },
        ratingOn: false,
        lowmark: -5,
        highmark: 5
    },
    support: {
        enabled: true,
        linkModule: 'support',
        links:{
            tiket: 'ticket'
        },
        inPage: 20,
        themeTemplate: 'Ticket#',
        sign: ''
    },
    comments: {
        enabled: true,
        inPage: 20,
        AdminInPage: 100,
        commentsTimeLimit: 86400,
        commentsSort: 'desc',
        preModerationOn: true,
        on: false,
        addingGuests: false
    },
    brands: {
        enabled: true
    },
    video: {
        enabled: true,
        linkModule: 'video',
        youtubeAPI: '',
        token: '',
        inPage: 20,
        useSystemApi: false,
        links: {
            category: 'category',
            video: 'video'
        }
    },
    services: {
        enabled: true
    },
    employees: {
        enabled: true
    },
    gallary: {
        enabled: true
    },
    faq: {
        enabled: true,
        linkModule: 'faq'
    },
    contacts: {
        enabled: true,
        linkModule: 'contacts',
        data: []
    }
};