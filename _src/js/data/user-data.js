/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: olegbogdanov86@gmail.com
 ------------------------------------------
 14.02.2016
 */

module.exports = {
    balans: 0,
    login: '',
    surname: '',
    name: '',
    password: '',
    repassword: '',
    pantonumic: '',
    gender: 'male',
    avatar: '',
    email: '',
    type_face: 'fiz',
    access: false,
    protectSetting: false,
    mainRoot: false,
    is_ban: false,
    enabled: true,
    isRoot: false, // главный юзер
    groups: [],
    domains: [],
    connect: {
        phone: '',
        fax: '',
        jabber: '',
        skype: '',
        icq: '',
        vk: '',
        facebook: '',
        twitter: ''
    },
    address: {
        region: '',
        district: '',
        city: '',
        street: '',
        korp: '',
        dom: '',
        kv: '',
        zip: ''
    },
    ur_data: {
        company_name: '',
        ogrn: '',
        inn: '',
        bik: '',
        kpp: '',
        bank_name: '',
        rs: '',
        krs: '',
        u_address: '',
        f_address: ''
    },
    defaultLocale: 'rus',
    isAuth: {
        count: 0,
        next: 0
    },
    favorites: {
        users: [],
        ads: []
    },
    rating: {
        plus: 0,
        minus: 0
    },
    modulesSetting: {
        shop: {
            discount: 0
        }
    },
    map: {
        type: 'yandex',
        position: [0, 0],
        zoom: 12
    },
    sysMenu: []
};