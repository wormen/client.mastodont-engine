/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: olegbogdanov86@gmail.com
 ------------------------------------------
 */

"use strict";

let dd = {
    title: '',
    count: 5
};

module.exports = {

    props: {
        name: {type: String},
        title: {type: String},
        data: {
            type: Object,
            twoWay: true,
            default(){
                return {};
            }
        }
    },

    data(){
        return {
            lang: {}
        }
    },

    ready(){
        var self = this;
        this.$set('data', Core.util.extend(dd, this.data));
        setInterval(()=>{ Core.getLangData((val)=>{ self.$set('lang', Core.util.extend({}, val, true)); }); }, Core.Time.Seconds(1));
    },

    template: require('./tpl/user-list.html')
};
