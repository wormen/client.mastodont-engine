/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: olegbogdanov86@gmail.com
 ------------------------------------------
 */

"use strict";

let dd = {
    title: '',
    menu_id: 0
};

class API{
    constructor(){
        var v = 'v1',
            module = 'menu';

        this.URL = `/api/${v}/${module}`;
    }

    miniList(opts, callback) {
        Core.API.getData(`${this.URL}/miniList`, opts, callback);
    }
}
API = new API;

module.exports = {

    props: {
        name: {type: String},
        title: {type: String},
        data: {
            type: Object,
            twoWay: true,
            default(){
                return {};
            }
        }
    },

    data(){
        return {
            lang: {},
            list: [],
            isLoading: true
        }
    },

    ready(){
        var self = this;
        this.$set('data', Core.util.extend(dd, this.data));

        this.onListLoad();

        setInterval(()=>{ Core.getLangData((val)=>{ self.$set('lang', Core.util.extend({}, val, true)); }); }, Core.Time.Seconds(1));
    },

    methods: {
        onListLoad(){
            var self = this;

            API.miniList({type: 'tree'}, (data, status, request)=>{
                if(status == 200 || data.statusCode == 200){
                    self.$set('list', data.result);

                    if(this.data.menu_id == 0)
                        this.$set('data.menu_id', data.result[0]._id);
                }

                self.isLoading = false;
            });
        }
    },

    template: require('./tpl/custom-menu.html')
};
