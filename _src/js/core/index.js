"use strict";

const $url = require('url');
const $util = require('util');
const Base64 = require('../ext/base64');

//,
//    jwt = require('jwt-simple');


const localforage = require('localforage/dist/localforage.min.js');

var _util = require('./util/index.js');

// ---------------------------------------------------------------------------------------------------------------------

let storeName = 'mastodont';
localforage.config({
    name: storeName
});

localforage.setDriver([
    localforage.INDEXEDDB,
    localforage.WEBSQL,
    localforage.LOCALSTORAGE
]);

class Store{
    constructor(){

    }

    Get(key, callback){
        if(callback)
            localforage.getItem(key, callback);
        else
            return localforage.getItem(key);
    }

    Set(key, value, callback){
        if(callback)
            localforage.setItem(key, value, callback);
        else
            return localforage.setItem(key, value);
    }

    Remove(key, callback){
        if(callback)
            localforage.removeItem(key, callback);
        else
            return localforage.removeItem(key);
    }

    Clear(){
        localforage.clear();
    }

    Length(){
        return localforage.length();
    }

    Key(keyIndex){
        return localforage.key(keyIndex);
    }

    Keys(){
        return localforage.keys();
    }
}

var $Store = new Store;

// ---------------------------------------------------------------------------------------------------------------------

class Core{
    constructor() {
        this.lang = {};
        this.Base64 = Base64;

        this.util = _util;
    }

    get(property, inStorage = false){

        if(inStorage){

            var data = sessionStorage.getItem(property);
            if(data)
                return JSON.parse(data);
            else
                return this[property];

        }else
            return this[property];
    }

    set(property, value, inStorage = false){

        if(value){
            this[property] = value;

            if(inStorage)
                $Store.Set(property, value);
        }
    }

    parseUrl(url) {
        url = url ? url : window.location.href;
        return $url.parse(url, true);
    }

    /**
     * Построение, JSON -> Query string
     * @param json
     * @returns {string}
     */
    param(json){
        if(Object.keys(json).length == 0) return '';
        else
            return '?' +
                Object.keys(json).map(function(key) {
                    return encodeURIComponent(key) + '=' +
                        encodeURIComponent(json[key]);
                }).join('&');
    }

    args(id) {
        var parseQuery = function (query) {
            var Params = new Object();
            if (!query) return Params; // return empty object

            var Pairs = query.split(/[;&]/);

            for (let i = 0; i < Pairs.length; i++) {
                var KeyVal = Pairs[i].split('=');
                if (!KeyVal || KeyVal.length != 2) continue;

                var key = unescape(KeyVal[0]);
                var val = unescape(KeyVal[1]);

                val = val.replace(/\+/g, ' ');
                Params[key] = val;
            }

            return Params;
        };

        var script = document.getElementById(id);
        return parseQuery(script.src.replace(/^[^\?]+\??/, ''));
    }

    Translite(str) {
        var newStr = new String();
        var ch, cyr2latChars = [
            ['а', 'a'], ['б', 'b'], ['в', 'v'], ['г', 'g'],
            ['д', 'd'], ['е', 'e'], ['ё', 'yo'], ['ж', 'zh'], ['з', 'z'],
            ['и', 'i'], ['й', 'y'], ['к', 'k'], ['л', 'l'],
            ['м', 'm'], ['н', 'n'], ['о', 'o'], ['п', 'p'], ['р', 'r'],
            ['с', 's'], ['т', 't'], ['у', 'u'], ['ф', 'f'],
            ['х', 'h'], ['ц', 'c'], ['ч', 'ch'], ['ш', 'sh'], ['щ', 'shch'],
            ['ъ', ''], ['ы', 'y'], ['ь', ''], ['э', 'e'], ['ю', 'yu'], ['я', 'ya'],

            ['А', 'A'], ['Б', 'B'], ['В', 'V'], ['Г', 'G'],
            ['Д', 'D'], ['Е', 'E'], ['Ё', 'YO'], ['Ж', 'ZH'], ['З', 'Z'],
            ['И', 'I'], ['Й', 'Y'], ['К', 'K'], ['Л', 'L'],
            ['М', 'M'], ['Н', 'N'], ['О', 'O'], ['П', 'P'], ['Р', 'R'],
            ['С', 'S'], ['Т', 'T'], ['У', 'U'], ['Ф', 'F'],
            ['Х', 'H'], ['Ц', 'C'], ['Ч', 'CH'], ['Ш', 'SH'], ['Щ', 'SHCH'],
            ['Ъ', ''], ['Ы', 'Y'],
            ['Ь', ''],
            ['Э', 'E'],
            ['Ю', 'YU'],
            ['Я', 'YA'],

            ['a', 'a'], ['b', 'b'], ['c', 'c'], ['d', 'd'], ['e', 'e'],
            ['f', 'f'], ['g', 'g'], ['h', 'h'], ['i', 'i'], ['j', 'j'],
            ['k', 'k'], ['l', 'l'], ['m', 'm'], ['n', 'n'], ['o', 'o'],
            ['p', 'p'], ['q', 'q'], ['r', 'r'], ['s', 's'], ['t', 't'],
            ['u', 'u'], ['v', 'v'], ['w', 'w'], ['x', 'x'], ['y', 'y'],
            ['z', 'z'],

            ['A', 'A'], ['B', 'B'], ['C', 'C'], ['D', 'D'], ['E', 'E'],
            ['F', 'F'], ['G', 'G'], ['H', 'H'], ['I', 'I'], ['J', 'J'], ['K', 'K'],
            ['L', 'L'], ['M', 'M'], ['N', 'N'], ['O', 'O'], ['P', 'P'],
            ['Q', 'Q'], ['R', 'R'], ['S', 'S'], ['T', 'T'], ['U', 'U'], ['V', 'V'],
            ['W', 'W'], ['X', 'X'], ['Y', 'Y'], ['Z', 'Z'],

            [' ', '-'], ['0', '0'], ['1', '1'], ['2', '2'], ['3', '3'],
            ['4', '4'], ['5', '5'], ['6', '6'], ['7', '7'], ['8', '8'], ['9', '9'],
            ['-', '-']

        ];

        for (let i = 0; i < str.length; i++) {

            ch = str.charAt(i);
            var newCh = '';

            for (let j = 0; j < cyr2latChars.length; j++) {
                if (ch == cyr2latChars[j][0])
                    newCh = cyr2latChars[j][1];
            }

            // Если найдено совпадение, то добавляется соответствие, если нет - пустая строка
            newStr += newCh;
        }
        // Удаляем повторяющие знаки - Именно на них заменяются пробелы.
        // Так же удаляем символы перевода строки, но это наверное уже лишнее
        return newStr.replace(/[-]{2,}/gim, '-').replace(/\n/gim, '');
    }

    scriptLoad(url) {
        if (Array.isArray(url)) {
            let self = this;
            let prom = [];
            url.forEach(function (item) {
                prom.push(self.script(item));
            });
            return Promise.all(prom);
        }

        return new Promise(function (resolve, reject) {
            let r = false;
            let t = document.getElementsByTagName('script')[0];
            let s = document.createElement('script');

            s.type = 'text/javascript';
            s.src = url;
            s.async = true;
            s.onload = s.onreadystatechange = function () {
                if (!r && (!this.readyState || this.readyState === 'complete')) {
                    r = true;
                    resolve(this);
                }
            };
            s.onerror = s.onabort = reject;
            t.parentNode.insertBefore(s, t);
        });
    }

    getLangData(callback){
        var obj = {};
        try{
            $Store.Get('lang', (err, result)=>{
                obj = result;
                callback(obj);
            });
        }catch(e){
            callback(obj);
        }
    }

    /**
     * Делаем первый символ заглавным
     * @param s - входная строка
     * @returns {string}
     */
    ucFirst(s) {
        return s.charAt(0).toUpperCase() + s.substr(1);
    }
}


var $Core = new Core;
installWithVue($Core);

module.exports = $Core;

// ---------------------------------------------------------------------------------------------------------------------

/**
 * Работа с запросами
 */
var resource = null;
class API{
    conctructor(){
    }

    getData(url, opts, callback) {
        resource = $Core.$resource(url);
        resource.get(opts, (data, status, request)=> {
            callback(data, status, request);
        }).error((data, status, request)=> {
            callback(data, status, request);
        });
    }

    postResponse(url, opts, data, callback) {
        resource = $Core.$resource(url);
        resource.save(opts, data, (data, status, request)=> {
            callback(data, status, request);
        }).error((data, status, request)=> {
            callback(data, status, request);
        });
    }

    putResponse(url, opts, data, callback) {
        resource = $Core.$resource(url);
        resource.update(opts, data, (data, status, request)=> {
            callback(data, status, request);
        }).error((data, status, request)=> {
            callback(data, status, request);
        });
    }

    deleteResponse(url, opts, callback) {
        resource = $Core.$resource(url);
        resource.delete(opts, (data, status, request)=> {
            callback(data, status, request);
        }).error((data, status, request)=> {
            callback(data, status, request);
        });
    }
}

module.exports.API = new API;

// ---------------------------------------------------------------------------------------------------------------------
module.exports.Store = $Store;
module.exports.Time = require('./ext/time');

// ---------------------------------------------------------------------------------------------------------------------


function installWithVue(Core){

    var _ = require('./../../libs/vue/util');

    _.config = Vue.config;
    _.warning = Vue.util.warn;
    _.nextTick = Vue.util.nextTick;

    Vue.url = require('./../../libs/vue/url');
    Vue.http = require('./../../libs/vue/http');
    Vue.resource = require('./../../libs/vue/resource');
    Vue.Promise = require('./../../libs/vue/promise');

    var obj = {

        $url: {
            get: function () {
                return _.options(Vue.url, this, (Vue.$options && Vue.$options.url ? Vue.$options.url : ''));
            }
        },

        $http: {
            get: function () {
                return _.options(Vue.http, this, (Vue.$options && Vue.$options.http ? Vue.$options.http : {}));
            }
        },

        $resource: {
            get: function () {
                return Vue.resource.bind(this);
            }
        },

        $promise: {
            get: function () {
                return function (executor) {
                    return new Vue.Promise(executor, this);
                }.bind(this);
            }
        }
    };

    Object.defineProperties(Core, obj);
}
