/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: olegbogdanov86@gmail.com
 ---------------------------------------------
 класс для работы с датами

 */

"use strict";

// const moment = require('moment');

class Time{

    Expires(days) {
        return new Date(Date.now() + this.Days(days));
    }

    maxAge(days) {
        return this.Days(days);
    }

    /**
     * Получаем значение равное кол-ву секунд
     * @param val - кол-во минут
     * @returns {number}
     * @constructor
     */
    Seconds(val){
        return val * 1000;
    }

    /**
     * Получаем значение равное кол-ву минут
     * @param val - кол-во минут
     * @returns {number}
     * @constructor
     */
    Minute(val){
        return val * this.Seconds(60);
    }

    /**
     * Получаем значение равное кол-ву часов
     * @param val - кол-во часов
     * @returns {number}
     * @constructor
     */
    Hours(val){
        return val * this.Minute(60);
    }

    /**
     * Получаем значение равное кол-ву дней
     * @param val - кол-во дней
     * @returns {number}
     * @constructor
     */
    Days(val){
        return val * this.Hours(24);
    }

    /**
     * Текущее время в формате TimeStamp
     * @returns {string}
     * @constructor
     */
    get TimeStamp() {
        var now = new Date();
        var date = [now.getFullYear(), now.getMonth() + 1, now.getDate()];
        var time = [now.getHours(), now.getMinutes(), now.getSeconds()];

        time[0] = ( time[0] < 12 ) ? time[0] : time[0] - 12;
        time[0] = time[0] || 12;

        for (var i = 1; i < 3; i++)
            if (time[i] < 10)
                time[i] = "0" + time[i];

        return date.join("-") + " " + time.join(":");
    }

    get Unix(){
        return parseInt(new Date().getTime()/1000);
    }

    /**
     * Преобразование даты в формат unixtime
     * @param date - фомат 24-Nov-2009 17:57:35
     * @returns {number}
     * @constructor
     */
    DateToUnixtime(date){
        return Date.parse(date);// /1000
    }
}

module.exports = new Time;