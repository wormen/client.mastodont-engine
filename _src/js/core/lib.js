"use strict";
Vue.use(require('./../../vendor/vue-resource.min.js'));


class Lib{
    constructor() {

    }

    Notify(d) {

        if (!d.type || !d.text) return;
        if (d.type == 'error') d.type = 'danger';
        window.UIkit.notify({message: d.text, status: d.type, timeout: 5000, pos: 'top-right'});
    }

    GenerateHash(length = 20) {
        var n, S = 'x',
            hash = (s)=> {

                if (Core.util.isNumber(s) && s === parseInt(s, 10))
                    s = Array(s + 1).join('x');

                return s.replace(/x/g, ()=> {
                    n = Math.round(Math.random() * 61) + 48;
                    n = n > 57 ? (n + 7 > 90 ? n + 13 : n + 7) : n;
                    return String.fromCharCode(n);
                });
            };

        for (let i = 0; i < length; i++) S += 'x';
        return hash(S);
    }

    CheckLangs(content) {
        var url = Core.parseUrl(),
            D = {};

        if ($AppData.multilang) {
            for (var i in $AppData.langs)
                D[$AppData.langs[i]] = content;
        }

        else if (url.action == 'add')
            D[$AppData.activeLocale] = content;


        return D;
    }

    contentLocale() {
        return sessionStorage.getItem('contentLocale');
    }

    contentCity() {
        var city = sessionStorage.getItem('contentCity');
        return Number(city == null ? 0 : city);
    }

    sidebar(type, obj) {
        obj.placement = String(type);
        type = type == 'left' ? 'sidebarLeft' : 'sidebarRight';

        var Set = function () {
            sidebar[type] = $.extend({}, $AppData.sidebar);

            for (var i in obj)
                sidebar[type][i] = obj[i];

            $('body').addClass('no-scroll');
        };

        if (sidebar[type].show) {
            sidebar[type].show = false;
            setTimeout(function () {
                Set();
            }, 300);
        }
        else
            Set();
    }

    /**
     * Получение позиции экрана
     * @returns {*}
     */
    screenPosTop(){
        return (window.pageYOffset !== undefined) ? window.pageYOffset : (document.documentElement || document.body.parentNode || document.body).scrollTop;
    }

    screenScrollTo(speed = 500, posTop = null){
        posTop = posTop ? posTop : this.screenPosTop();

        $('body,html').animate({
            scrollTop: posTop
        }, speed);
    }

    /**
     * Получаем позицию элемента
     * @param element
     */
    getElementPos(element){
        let result = {screen: {}, parentBlock: {}};



        return result;
    }
}


var $Lib = Lib;
if (!(this instanceof Lib))
    $Lib = new Lib;

module.exports = $Lib;

