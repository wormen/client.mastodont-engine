/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: olegbogdanov86@gmail.com
 ------------------------------------------
 */
"use strict";

var $util = require('util'),
    deepExtend = require('deep-extend');


$util.clearStr = (str, patern = /[^A-Za-zА-Яа-яЁё!?.,0-9\+\"()\- \s]/gim)=>{
    return str.replace(patern, "").replace('  ', ' ');
};

$util.getNumber = (str)=>{
    var num = 0;

    if(str && $util.isString(str)){
        num = parseInt(str.replace(/[^0-9]/gim,''));
        num = ($util.isNumber(num) ? num : 0);

        if(isNaN(num)) num = 0;
    }else
        num = str;

    return num;
};

/**
 * 
 * @param origin
 * @param add
 * @param deep
 * @returns {*}
 */
$util.extend = (origin, add, deep = false) => {
    if(!add) return;
    
    var keys = Object.keys(add),
        i = keys.length;

    while (i--) {
        origin[keys[i]] = add[keys[i]];
    }

    if(deep)
        return JSON.parse(JSON.stringify(origin));
    else
        return origin;
};

/**
 * Отвязываем от ссылок
 * @param origin
 */
$util.unlink = (origin) =>{
    return JSON.parse(JSON.stringify(origin));
};

    /**
 * Рекурсивное слияние объектов
 * @param origin
 * @param add
 */
$util.extendRecurs = (origin, add)=>{
    return deepExtend(origin, add);
};


module.exports = $util;