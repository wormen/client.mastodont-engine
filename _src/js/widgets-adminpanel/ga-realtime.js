/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: olegbogdanov86@gmail.com
 ------------------------------------------
 */

"use strict";

var name = 'ga-realtime';

module.exports = {
    name: name,
    template: require(`./tpl/${name}.html`),

    props: {

    },

    ready(){

    },

    methods: {

    }
};