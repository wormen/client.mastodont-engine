"use strict";

let dd = {
    title: '',
    count: 10
};

module.exports = {
    name: 'system-news',
    template: require('./tpl/system-news.html'),
    
    data(){
        return {
            lang: {}
        }
    },

    ready(){
        var self = this;
        // this.$set('data', Core.util.extend(dd, this.data));
        setInterval(()=>{ Core.getLangData((val)=>{ self.$set('lang', Core.util.extend({}, val, true)); }); }, Core.Time.Seconds(1));
    }

};
