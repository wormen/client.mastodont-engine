/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: olegbogdanov86@gmail.com
 ------------------------------------------
 */

"use strict";

var menuState = {
    level1: '',
    level2: ''
};

try{
    var M = localStorage.getItem('menuState');
    if(M)
        menuState = JSON.parse(M);
}catch(e){}


class API{
    constructor(){
        var v = 'v1',
            module = 'system';

        this.URL = `/api/${v}/${module}`;
    }

    loadList(opts = {}, callback) {
        Core.API.getData(`${this.URL}/getMenu`, opts, callback);
    }
}

API = new API;

var b = module.exports = new Vue({
    el: 'aside#sidebar',
    data: {
        lang: {},
        isLoading: true,
        list: []
    },

    ready(){
        var self = this;
        this.$emit('loadList');
    },

    events: {
        loadList(){
            var self = this;
            API.loadList({}, (data, status, request)=>{
                if(status == 200 || data.statusCode == 200)
                    self.$set('list', data.result);

                self.$set('isLoading', false);
                setTimeout(()=>{ self.$emit('loadList'); }, Core.Time.Minute(3));

            });
        }
    },

    methods: {

        setState(isSub, name, level){
            if(isSub){
                menuState[`level${level}`] = menuState[`level${level}`].includes(name) ? '' : name;
                localStorage.setItem('menuState', JSON.stringify(menuState));
            }
        },

        checkState(name, level){
            return Boolean(menuState[`level${level}`].includes(name));
        },
        
        getUrl(uri){
            return (uri.includes('#') ? uri : `/admin/${uri}`);
        }
    }
});

setInterval(()=>{ Core.getLangData((val)=>{ b.$set('lang', Core.util.extend({}, val, true) ); }); }, Core.Time.Seconds(1));
module.exports = b;