/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: olegbogdanov86@gmail.com
 ------------------------------------------
 11.03.2016
 */

"use strict";

var $util = require('util');

var util = {
    extend(origin, add) {
        var keys = Object.keys(add);
        var i = keys.length;
        while (i--) {
            origin[keys[i]] = add[keys[i]];
        }
        return origin;
    },

    clearStr(str){
        return str.replace(/[^A-Za-zА-Яа-яЁё!?.,0-9\+\"()\- \s]/gim, "").replace('  ', ' ');
    },

    getNumber(str){
        var num = 0;

        if(str && typeof str === 'string'){
            var num = parseInt(str.replace(/[^0-9]/gim,''));
            num = ($util.isNumber(num) ? num : 0);

            if(isNaN(num)) num = 0;
        }else
            num = str;
        
        return num;
    },

    format: $util.format
};

module.exports = util;