"use strict";

var resource,
    $url = Core.parseUrl();

module.exports = new Vue({
    el: '[rel="ads"]',
    data: {
        sortData: {
            date: 'desc',
            price: '',
            'rating-ad': 'desc',
            'rating-seller': ''
        },
        filterData: {

        }
    },

    ready: function(){
        

    },

    methods: {

        LoadID(id, callback){
            console.log('загружаем объявление', id);

            var opts = {
                type: 'post',
                'user-edit': id
            };

            resource = Core.$resource('/api/v1/ads');
            resource.get(opts, callback).error(callback);
        },
        

        // получить URL для сортировки
        getSortUrl(){

        },

        // получить данные по API
        onSort(){

        },

        // получить URL для фильтра
        getFilterUrl(){

        },

        // получить данные по API
        onFilter(){

        },

        Save(data, callback){
            data = data ? data : Account.ad;

            var id = null,
                opts = {type: 'post'};

            if($url.query && $url.query.id)
                opts.id = $url.query.id;

            resource = Core.$resource('/api/v1/ads');
            resource[opts.id ? 'update' : 'save'](opts, data, function (data, status, request) {

                callback(data, status, request);
            }).error(function (data, status, request) {

                callback(data, status, request);
            });
        }
    }
});