"use strict";

var resource,
    $url = Core.parseUrl();

module.exports = {
    name: 'ads-category-select',
    props: {
        api: {
            type: String,
            default: '/api/v1/ads'
        },

        activeListCategory: {
            default: 0
        },

        categoryMultiLevel: {
            default: function(){ return []; }
        },

        categoryTree: {
            default: function(){ return []; }
        }
    },
    template: (function(){
        var t = document.getElementById('ads-category-select-t');
        if(t)
            return t.innerHTML;
        else
            return '';

    })(),

    ready: function(){
        var self = this;

        if($url.query && $url.query.id)
            Ads.LoadID($url.query.id, (data, status, request)=>{
                self.$set('categoryTree', data.categoryTree);

                setTimeout(()=>{
                    var cat = data.categoryTree[data.categoryTree.length-1];
                    self.onLoadCategoryLevel(cat.parent_id, cat.level);
                },1000);

                Account.ad = data;
                Account.previewList = data.imgs;

                Account.adCreated = Core.moment.unix(data.create).locale('ru').format('LL')
            });
        
        self.onLoadCategoryLevel();
    },

    methods: {
        autoSelected(){

            for(let CM of this.categoryMultiLevel)
                this.selectedInList(CM);

        },

        selectedInList(item){

            for(let C of item.list){

                for(let CT of this.categoryTree)
                    if(C._id.includes(CT._id) && CT.isChecked)
                        C.isChecked = CT.isChecked;
            }

        },

        onLoadCategoryLevel(parent_id, level){
            var self = this;

            level = level ? level : 0;
            parent_id = parent_id ? parent_id: 0;
            var opts = {user: 1, mini: 1, type: 'category', parent_id: parent_id};

            if(parent_id != 0)
                self.activeListCategory = parent_id;

            resource = Core.$resource(this.api);
            resource.get(opts, function (data, status, request) {

                var lv = {
                    level: level,
                    list: data
                };

                if(!self.categoryMultiLevel[level])
                    self.categoryMultiLevel.push(lv);
                else
                    self.$set('categoryMultiLevel['+level+']', lv);

                self.autoSelected();

            }).error(function (data, status, request) {
            });
        },

        onSelectCatagory(item, level){
            var self = this,
                List = this.categoryMultiLevel;

            for(var i in List){
                if(List[i].level == level){
                    for(var l in List[i].list)
                        List[i].list[l].isChecked = false;
                }
            }

            self.onLoadCategoryLevel(item._id, level);

            item.isChecked = true;
            self.generateBreamsCategory(level, item);


            this.$root.ad.category_id = item._id;
        },

        generateBreamsCategory(level, item){
            item = Vue.util.extend({}, item);
            --level;

            item.level = level;

            if(!this.categoryTree[level])
                this.categoryTree.push(item);
            else
                this.$set('categoryTree['+level+']', item);

            this.$set('$root.ad.category_id', item._id );
            this.$set('$root.ad.categoryTree', [].concat([], this.categoryTree) );
        },

        onSelectBreamsCatagory(idx, item){
            var arr = [];

            for(var i in this.categoryTree){
                if(i <= idx)
                    arr.push(this.categoryTree[i]);

                if(i == idx)
                    this.generateBreamsCategory(this.categoryTree[i].level, item);
            }

            this.$set('categoryTree', arr);
        }
    }
};
