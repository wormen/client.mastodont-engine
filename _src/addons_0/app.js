/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: olegbogdanov86@gmail.com
 ------------------------------------------
 09.02.2016
 */

"use strict";

Vue.debug = (NODE_ENV == 'dev');

var A = new Vue({
    el: '',
    data: {

    },

    ready: function(){

    }
});

module.exports = {
    detect: new MobileDetect(navigator.userAgent)
};