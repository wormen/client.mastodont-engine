/**
Функционал аккаунта пользователя
 */

"use strict";

var resource,
    $url = Core.parseUrl(),
    dir = Core.GenerateHash(15);

var payType = {

};


module.exports = new Vue({
    el: '[rel="account"]',
    data: {
        userAPI: '/api/v1/users',
        defaultSumm: 30, // сумма пополнения баланса по умолчанию
        isCustomSumm: false,
        paySumm: 0,
        balans: 0,
        userData: require('../js/data/user-data'),
        alert: {
            isMinSumm: false // суммы не может быть меньше минимальной
        },

        dir: dir,

        module: {},

        isLoading: true,

        isLoadedProfile: false,
        isSaved: false,
        profile: {},
        isValidUserData: false,
        oldPass: '',
        oldPassIsOk: true,

        // параметры объявления
        ad: {
            name: '',
            category_id: 0,
            categoryTree: [],
            user_id: '',
            price: {
                isRange: false,
                from: '',
                to: ''
            },
            geo: {
                isCountry: true,
                isDistrict: false,
                isRegion: false,
                districtData: {},
                regionData: [],
                city: []
            },
            text: '',
            contacts: {
                email: '',
                phone: '',
                skype: '',
                facebook: '',
                vk: '',
                ok: ''
            },
            tmp: dir
        },

        isMoreCity: false,
        isMorePreview: false,
        maxCountPreview: 10,
        previewList: [], // список превьюх объявления

        adCreated: ''
    },

    ready(){
        var self = this;

        if(this.paySumm < this.defaultSumm)
            this.paySumm = this.defaultSumm;

        if($url.query && $url.query.id)
            this.$set('dir', $url.query.id);

        this.$watch('paySumm', function(newVal, oldVal){
            if(newVal == oldVal) return;

            self.alert.isMinSumm = false;
            if(newVal < self.defaultSumm){
                self.alert.isMinSumm = true;
                // self.paySumm = this.defaultSumm;
            }
        });

        // получаем кол-во сообщений
        // v-el:msgCount

        

        self.onInit();
    },

    methods: {
        // инициализация
        onInit(){
            var self = this;

            try{

                var module = Core.GetTplData();


                self.$set('module', module);

                if(module.name == 'account'){
                    // Загружаем данные профиля
                    self.loadProfile();
                    
                }

            }catch(e){
                console.error(e);
            }

        },

        // выбор суммы для поплнения баланса
        selectSumm(e, summ){
            this.paySumm = summ;
            e.preventDefault();
        },

        // Инициализация процесса оплаты
        onPay(type){
            if(payType['type'])
                payType['type']();
        },

        // получаем кол-во сообщений пользователя
        GetMsgCount () {
            
        },

        checkOldPassword(){
            var self = this;

            try{

                var user = Core.GetUserCookie();

                if(this.oldPass){
                    self.isLoading = true;
                    self.oldPassIsOk = true;

                    resource = Core.$resource(this.userAPI);
                    resource.save({type: 'user', 'check-pass': 1}, {uid: user.u, password: this.oldPass}, function (data, status, request) {
                        self.isLoading = false;
                        self.oldPassIsOk = (data.status && data.status == 'OK' ? true : false);
                    }).error(function (data, status, request) {
                        self.isLoading = false;
                    });
                }

            }catch(e){
                console.error(e);
            }

        },

        changePassword(){

        },

        loadProfile(){
            var self = this;

            try{
                var user = Core.GetUserCookie();

                if(user.u){
                    self.isLoading = true;
                    resource = Core.$resource(this.userAPI);
                    resource.get({'get-id': user.u}, function (data, status, request) {
                        if(status == 200){
                            self.$set('profile', data);
                        }

                        self.isLoadedProfile = true;
                        self.isLoading = false;

                        self.$set('ad.user_id', user.u);
                    }).error(function (data, status, request) {
                        self.isLoading = false;
                    });
                }

            }catch(e){
                console.error(e);
            }

        },

        saveProfile(profile){
            var self = this;

            try {
                var user = Core.GetUserCookie();

                if(user.u && this.checkUserData()){

                    self.isLoading = true;
                    self.isSaved = false;
                    profile = profile ? profile : self.profile;
                    resource = Core.$resource(this.userAPI);
                    resource.update({type: 'user', id: user.u}, profile, function (data, status, request) {
                        // профиль сохранен
                        self.isLoading = false;
                        self.isSaved = true;
                    }).error(function (data, status, request) {
                        // ошибка при сохранении
                        self.isLoading = false;
                    });
                }

            }catch(e){
                console.error(e);
            }
            
        },

        // проверяем данные пользователя
        checkUserData(){
            return this.isValidUserData;
        },

        setPreviewList(data){
            this.isMorePreview = data.length>=this.maxCountPreview;

            for(let i in data)
                data[i].primary = false;

            if(!this.isMorePreview)
                this.$set('previewList', data);
        },

        // удаление фотки объявления
        DeleteAdImage(idx) {
            var id = null,
                self = this;

            if(self.previewList[idx]._id){
                id = self.previewList[idx]._id;

                resource = Core.$resource('/api/v1/upload');
                resource.delete({id: id, module: 'ads'}, function (data, status, request) {
                    if(status == 200){
                        for(var i in self.previewList)
                            if(self.previewList[i]._id == id){
                                self.previewList.splice(i, 1);
                                break;
                            }
                    }
                }).error(function (data, status, request) {});

            }else
                self.previewList.splice(idx, 1);

        },

        PrimaryImage(item){
            var module = null,
                md = Core.GetTplData();

            for(var i in this.previewList)
                this.previewList[i].primary = this.previewList[i]._id.includes(item._id);

            switch(md.section){
                case 'create-ad':
                    module = 'ads';
                    break;
            }

            var opts = {
                'set-prime': item._id
            };

            var data = {
                dir: Account.dir,
                module: module
            };

            if($url.query.id)
                data.doc_id = $url.query.id;


            // resource = Core.$resource('/api/v1/upload');
            // resource.update(opts, data, function (data, status, request) {
            // }).error(function (data, status, request) {
            // });
        }

        // deleteCity: function(e, idx) {
        //     e.preventDefault();
        //     Geo.deleteCity(e, idx);
        // }
    },

    components: {
        'ad-geo': require('./components/ad-geo'),
        'ads-category-select': require('./components/ads-category-select')
    }
});