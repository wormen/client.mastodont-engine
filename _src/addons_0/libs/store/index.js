"use strict";

localforage.config({
    driver: [
        localforage.INDEXEDDB,
        localforage.WEBSQL,
        localforage.LOCALSTORAGE
    ],
    name: 'mastodont'
});

var Store = {
    Get: localStorage.getItem,
    Set: localStorage.setItem,
    Remove: localStorage.removeItem,
    Clear: localStorage.clear,
    key: localStorage.key,
    keys: localStorage.keys,
    length: localStorage.length
};
module.exports = Store;