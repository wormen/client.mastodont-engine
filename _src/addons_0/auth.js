"use strict";

var resource;

module.exports = new Vue({
    el: '[rel="auth"]',
    data: {
        api: '/api/v1/auth?type=user',

        isLoading: false,
        isAuthLoading: false,
        isRestoreLoading: false,
        isRegisterLoading: false,

        isLogin: false,
        isLoginF: false,
        isLoginT: '',

        isEmail: false,
        isEmailF: false,
        isEmailT: '',

        isAuthE: false,
        isAuthT: '',

        isRestoreS: false,
        isRestoreE: false,
        isRestoreT: '',

        noPass: false,
        noPassT: '',

        isAgree: false,
        loginOrEmail: '',
        user: {
            login: '',
            password: ''
        },
        reg: {
            login: '',
            email: '',
            password: '',
            repass: '',
            iAgree: false
        },
        regRes: {
            isErr: false,
            text: ''
        }
    },

    ready: function(){

    },

    methods: {

        onAuth: function(user){
            var self = this,
                url = '/api/v1/auth?type=user';

            user = user ? user : this.user;

            this.isAuthLoading = true;
            Core.$http.get(url, function(r) {},
                {headers: {'Authorization': 'Basic ' + btoa(self.user.login + ':' + self.user.password)}}

            ).then(
                function(r){
                    self.onAuthResponse(r);
                },
                function(e){
                    self.onAuthResponse(e);
                });
        },

        onAuthResponse: function(r){
            this.isAuthE = false;

            if(r.data && r.data.error){
                this.isAuthE = true;
                this.isAuthT = r.data.text;
            }

            if(r.data.result && r.data.result == 'ok')
                window.location.reload();

            this.isAuthLoading = false;
        },

        onLogOut: function(e){
            resource = Core.$resource('/api/v1/auth');
            resource.get({logout: 1, type: 'user'}, function (data, status, request) {
                Core.Cookies.remove('mst_user');
                window.location.reload();
            }).error(function (data, status, request) {
            });
            
            e.preventDefault();
        },

        onRepairPass: function(data){
            var self = this;

            this.isRestoreS = this.isRestoreE = false;
            this.isRestoreT = '';

            this.isRestoreLoading = true;
            resource = Core.$resource('/api/v1/users');
            resource.save({repair: 'pass'}, {d: (data ? data : this.loginOrEmail)}, function (data, status, request) {
                self.isRestoreLoading = false;

                switch(data.code){
                    case 'OK':
                        self.isRestoreS = true;
                        self.isRestoreT = data.text;
                        break;

                    case 'USER_NOT_FOUND':
                        self.isRestoreE = true;
                        self.isRestoreT = data.text;
                        break;
                }

            }).error(function (data, status, request) {
                self.isRestoreLoading = false;
            });

        },

        // проверка существующего логина
        onCheckLogin: function () {
            var self = this,
                val = this.reg.login;

            if(val.length>=3){
                resource = Core.$resource('/api/v1/users');
                resource.get({check: val, type: 'login'}, function (data, status, request) {
                    if(status == 200)
                        self.$set('isLogin', data);
                }).error(function (data, status, request) {
                });
            }
        },

        // проверка существующего Email
        onCheckEmail: function(){
            var self = this,
                val = this.reg.email;

            if( Core.Validation.Email(val) ){
                resource = Core.$resource('/api/v1/users');
                resource.get({check: val, type: 'email'}, function (data, status, request) {
                    if(status == 200)
                        self.$set('isEmail', data);
                }).error(function (data, status, request) {
                });
            }
        },

        onRegister: function (e) {
            var self = this,
                isReg = [];
            this.noPass = this.isAgree = this.isEmailF = this.isLoginF = false;

            var U = this.checkRegData();
            if( U.isNext ){
                delete this.reg.repass;
                delete this.reg.iAgree;

                this.isRegisterLoading = true;
                resource = Core.$resource('/api/v1/users');
                resource.save({new: 'user'}, U.data, function (data, status, request) {
                    self.regResponse(data, status);
                    self.isRegisterLoading = false;
                }).error(function (data, status, request) {
                    self.regResponse(data, status);
                    self.isRegisterLoading = false;
                });
            }

            e.preventDefault();
        },

        checkRegData: function(data){
            if(data)
                this.$set('reg', data);
            else
                data = this.reg;

            var isReg = [];

            if(data.login.length == 0){
                this.isLoginF = true;
                this.isLoginT = 'Не указан логин';
                isReg.push('');
            }

            if(data.login.length>0 && data.login.length <3){
                this.isLoginF = true;
                this.isLoginT = 'Логин слишком короткий';
                isReg.push('');
            }

            if(data.password.length == 0){
                this.noPass = true;
                this.noPassT = 'Не указан пароль';
                isReg.push('');
            }

            if(data.password.length > 0 && data.password.length <5){
                this.noPass = true;
                this.noPassT = 'Пароль слишком короткий';
                isReg.push('');
            }

            if(data.repass != this.reg.password){
                this.noPass = true;
                this.noPassT = 'Неверный проверочный проль';
                isReg.push('');
            }

            if(data.email.length == 0){
                this.isEmailF = true;
                this.isEmailT = 'Не указан Email';
                isReg.push('');
            }

            if(data.email.length>0 && !Core.Validation.Email(data.email)){
                this.isEmailF = true;
                this.isEmailT = 'Некорректный Email';
                isReg.push('');
            }

            if(!data.iAgree){
                this.isAgree = true;
                isReg.push('');
            }

            return {
                data: data,
                isNext: (isReg.length == 0)
            };
        },

        regResponse: function(data, status){
            this.regRes.isErr = false;
            this.regRes.text = '';

            if(data.statusCode && data.statusCode == 'ok') this.regRes.isErr = false;
            else this.regRes.isErr = true;

            if(data.code && !this.regRes.isErr){
                switch(data.code){
                    case 'IS_AUTH':
                        // автоматическая авторизация после регистрации
                        this.onAuth({
                            login: this.reg.login,
                            password: this.reg.password
                        });
                        break;

                    case 'EMAIL_ACTIVATE':
                    case 'ADMIN_ACTIVATE':
                        this.regRes.text = data.text;
                        break;
                }
            }else
                this.regRes.text = 'При регистрации произошла ошибка';
        }

    }
});