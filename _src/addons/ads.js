"use strict";

var $url = Core.parseUrl();

class API{
    constructor(){
        var v = 'v1',
            module = 'ads';

        this.URL = `/api/${v}/${module}`;
    }

    adUserEdit(id, callback) {
        Core.API.getData(`${this.URL}/adUserEdit/:id`, {id: id}, callback);
    }

    adSave(data, callback) {
        Core.API.postResponse(`${this.URL}/adSave`, {}, data, callback);
    }

    adUpdate(id, data, callback) {
        Core.API.putResponse(`${this.URL}/adUpdate/:id`, {id: id}, data, callback);
    }
}

API = new API;



module.exports = new Vue({
    el: '[rel="ads"]',
    data: {
        sortData: {
            date: 'desc',
            price: '',
            'rating-ad': 'desc',
            'rating-seller': ''
        },
        filterData: {

        }
    },

    ready: function(){
        

    },

    methods: {

        LoadID(id, callback){
            // console.log('загружаем объявление', id);

            API.adUserEdit(id, callback);
        },
        

        // получить URL для сортировки
        getSortUrl(){

        },

        // получить данные по API
        onSort(){

        },

        // получить URL для фильтра
        getFilterUrl(){

        },

        // получить данные по API
        onFilter(){

        },

        Save(data, callback){
            data = data ? data : Account.ad;

            if($url.query && $url.query.id)
                API.adUpdate($url.query.id, data, (data, status, request)=>{
                    callback(data, status, request);
                });

            else
                API.adSave(data, (data, status, request)=>{
                    callback(data, status, request);
                });
            
        }
    }
});