/**
Функционал аккаунта пользователя
 */

"use strict";

var $url = Core.parseUrl(),
    dir = Core.GenerateHash(15);

class API{
    constructor(){
        var v = 'v1',
            module = 'users';

        this.URL = `/api/${v}/${module}`;
        this.PAY_URL = `/api/${v}/payment`;
    }

    checkPass(opts, callback) {
        Core.API.getData(`${this.URL}/checkPass`, opts, callback);
    }

    getID(id, callback) {
        Core.API.getData(`${this.URL}/getID/:id`, {id: id, type: 'user'}, callback);
    }

    update(id, opts, data, callback) {
        opts.id = id;
        Core.API.putResponse(`${this.URL}/update/:id`, opts, data, callback);
    }
}

API = new API;


var payType = {
    addBalance(summ, system, callback){
        system = system ? system : 'yandex';
        Core.API.getData(`${API.PAY_URL}/getPayLink`, {
            pay_summ: summ,
            system: system,
            payment_method: 'PC',
            descr: 'Пополнение баланса'
        }, (data, status, request)=>{
            if(data.result && data.result.link){
                window.open(data.result.link, '_blank');
            }
        });
    }
};


module.exports = new Vue({
    el: '[rel="account"]',
    data: {
        defaultSumm: 30, // сумма пополнения баланса по умолчанию
        isCustomSumm: false,
        paySumm: 300,
        balans: 0,
        userData: require('../js/data/user-data'),
        alert: {
            isMinSumm: false // суммы не может быть меньше минимальной
        },

        dir: dir,

        module: {},

        isLoading: true,

        isLoadedProfile: false,
        isSaved: false,
        profile: {map: {}},
        isValidUserData: false,
        oldPass: '',
        oldPassIsOk: true,

        // параметры объявления
        ad: {
            name: '',
            category_id: 0,
            categoryTree: [],
            user_id: '',
            price: {
                isRange: false,
                from: '',
                to: ''
            },
            geo: {
                isCountry: true,
                isDistrict: false,
                isRegion: false,
                districtData: {},
                regionData: [],
                city: []
            },
            text: '',
            contacts: {
                email: '',
                phone: '',
                skype: '',
                facebook: '',
                vk: '',
                ok: ''
            },
            tmp: dir
        },

        isMoreCity: false,
        isMorePreview: false,
        maxCountPreview: 10,
        previewList: [], // список превьюх объявления

        adCreated: ''
    },

    ready(){
        var self = this;

        if(this.paySumm < this.defaultSumm)
            this.paySumm = this.defaultSumm;

        if($url.query && $url.query.id)
            this.$set('dir', $url.query.id);

        this.$watch('paySumm', function(newVal, oldVal){
            if(newVal == oldVal) return;

            self.alert.isMinSumm = false;
            if(newVal < self.defaultSumm){
                self.alert.isMinSumm = true;

                // setTimeout(()=>{
                //     self.paySumm = this.defaultSumm;
                // }, 5000);
            }
        });

        // получаем кол-во сообщений
        // v-el:msgCount

        

        self.onInit();
    },

    methods: {
        // инициализация
        onInit(){
            var self = this;

            try{

                var module = Core.GetTplData();
                if(Object.keys(module).length == 0) return;

                self.$set('module', module);

                if (module.name.includes('account')){
                    // Загружаем данные профиля
                    self.loadProfile();
                    
                }

            }catch(e){
                console.error(e);
            }

        },

        // выбор суммы для поплнения баланса
        selectSumm(e, summ){
            this.paySumm = summ;
            e.preventDefault();
        },

        // Инициализация процесса оплаты
        onPay(type){
            if(this.paySumm < this.defaultSumm)
                this.paySumm = this.defaultSumm;

            if(payType[type])
                payType[type](this.paySumm);

        },

        // получаем кол-во сообщений пользователя
        GetMsgCount () {
            
        },

        checkOldPassword(){
            var self = this;

            try{

                var user = Core.GetUserCookie();

                if(this.oldPass){
                    self.isLoading = true;
                    self.oldPassIsOk = true;

                    API.checkPass({type: 'user', uid: user.u, password: this.oldPass}, (data, status, request)=>{
                        self.isLoading = false;
                        self.oldPassIsOk = (data.result && data.result.status && data.result.status == 'OK' ? true : false);
                    });

                }

            }catch(e){
                console.error(e);
            }

        },

        changePassword(){

        },

        loadProfile(){
            var self = this;

            try{
                var user = Core.GetUserCookie();

                if(user.u){
                    self.isLoading = true;

                    API.getID(user.u, (data, status, request)=>{
                        self.isLoading = false;

                        if(status == 200 || data.statusCode == 200){
                            self.$set('profile', data.result);
                        }

                        self.isLoadedProfile = true;
                        self.$set('ad.user_id', user.u);
                    });

                }

            }catch(e){
                console.error(e);
            }

        },

        saveProfile(profile){
            var self = this;

            try {
                var user = Core.GetUserCookie();

                if(user.u && this.checkUserData()){

                    self.isLoading = true;
                    self.isSaved = false;
                    profile = profile ? profile : self.profile;

                    API.update(user.u, {type: 'user'}, profile, (data, status, request)=>{
                        // профиль сохранен
                        self.isLoading = false;
                        self.isSaved = true;
                    });

                }

            }catch(e){
                console.error(e);
            }
            
        },

        // проверяем данные пользователя
        checkUserData(){
            return this.isValidUserData;
        },

        setPreviewList(data){
            this.isMorePreview = data.length>=this.maxCountPreview;

            for(let i in data)
                data[i].primary = false;

            if(!this.isMorePreview)
                this.$set('previewList', data);
        },

        // удаление фотки объявления
        DeleteAdImage(idx) {
            var id = null,
                self = this;

            if(self.previewList[idx]._id){
                id = self.previewList[idx]._id;

                var resource = Core.$resource('/api/v1/upload');
                resource.delete({id: id, module: 'ads'}, function (data, status, request) {
                    if(status == 200){
                        for(var i in self.previewList)
                            if(self.previewList[i]._id == id){
                                self.previewList.splice(i, 1);
                                break;
                            }
                    }
                }).error(function (data, status, request) {});

            }else
                self.previewList.splice(idx, 1);

        },

        PrimaryImage(item){
            var module = null,
                md = Core.GetTplData();

            for(var i in this.previewList)
                this.previewList[i].primary = this.previewList[i]._id.includes(item._id);

            switch(md.section){
                case 'create-ad':
                    module = 'ads';
                    break;
            }

            var opts = {
                'set-prime': item._id
            };

            var data = {
                dir: Account.dir,
                module: module
            };

            if($url.query.id)
                data.doc_id = $url.query.id;


            // resource = Core.$resource('/api/v1/upload');
            // resource.update(opts, data, function (data, status, request) {
            // }).error(function (data, status, request) {
            // });
        }

        // deleteCity: function(e, idx) {
        //     e.preventDefault();
        //     Geo.deleteCity(e, idx);
        // }
    },

    components: {
        'ad-geo': require('./components/ad-geo'),
        'ads-category-select': require('./components/ads-category-select')
    }
});