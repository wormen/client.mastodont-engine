"use strict";

var resource = null,
    events = require('events').EventEmitter;

import MobileDetect from 'mobile-detect';
import Vue from './../vendor/vue.min.js'


var className = [],
    md = new MobileDetect(navigator.userAgent),
    $url = require('url'),
    Base64 = require('../js/ext/base64');

Vue.config.debug = false;//(NODE_ENV == 'dev');
Vue.config.devtools = false;

// чекаем платформу
if(md.phone()) className.push('mobile');
if(md.tablet()) className.push('tablet');


if(className.length>0)
    document.getElementsByTagName( 'html' )[0].className += ' '+className.join(' ');



var get_html_translation_table = function(table, quote_style) {
    var entities = {},
        hash_map = {},
        decimal;
    var constMappingTable = {},
        constMappingQuoteStyle = {};
    var useTable = {},
        useQuoteStyle = {};

    // Translate arguments
    constMappingTable[0] = 'HTML_SPECIALCHARS';
    constMappingTable[1] = 'HTML_ENTITIES';
    constMappingQuoteStyle[0] = 'ENT_NOQUOTES';
    constMappingQuoteStyle[2] = 'ENT_COMPAT';
    constMappingQuoteStyle[3] = 'ENT_QUOTES';

    useTable = !isNaN(table) ? constMappingTable[table] : table ? table.toUpperCase() : 'HTML_SPECIALCHARS';
    useQuoteStyle = !isNaN(quote_style) ? constMappingQuoteStyle[quote_style] : quote_style ? quote_style.toUpperCase() : 'ENT_COMPAT';

    if (useTable !== 'HTML_SPECIALCHARS' && useTable !== 'HTML_ENTITIES') {
        throw new Error("Table: " + useTable + ' not supported');
        // return false;
    }

    entities['38'] = '&amp;';
    if (useTable === 'HTML_ENTITIES') {
        entities['160'] = '&nbsp;';
        entities['161'] = '&iexcl;';
        entities['162'] = '&cent;';
        entities['163'] = '&pound;';
        entities['164'] = '&curren;';
        entities['165'] = '&yen;';
        entities['166'] = '&brvbar;';
        entities['167'] = '&sect;';
        entities['168'] = '&uml;';
        entities['169'] = '&copy;';
        entities['170'] = '&ordf;';
        entities['171'] = '&laquo;';
        entities['172'] = '&not;';
        entities['173'] = '&shy;';
        entities['174'] = '&reg;';
        entities['175'] = '&macr;';
        entities['176'] = '&deg;';
        entities['177'] = '&plusmn;';
        entities['178'] = '&sup2;';
        entities['179'] = '&sup3;';
        entities['180'] = '&acute;';
        entities['181'] = '&micro;';
        entities['182'] = '&para;';
        entities['183'] = '&middot;';
        entities['184'] = '&cedil;';
        entities['185'] = '&sup1;';
        entities['186'] = '&ordm;';
        entities['187'] = '&raquo;';
        entities['188'] = '&frac14;';
        entities['189'] = '&frac12;';
        entities['190'] = '&frac34;';
        entities['191'] = '&iquest;';
        entities['192'] = '&Agrave;';
        entities['193'] = '&Aacute;';
        entities['194'] = '&Acirc;';
        entities['195'] = '&Atilde;';
        entities['196'] = '&Auml;';
        entities['197'] = '&Aring;';
        entities['198'] = '&AElig;';
        entities['199'] = '&Ccedil;';
        entities['200'] = '&Egrave;';
        entities['201'] = '&Eacute;';
        entities['202'] = '&Ecirc;';
        entities['203'] = '&Euml;';
        entities['204'] = '&Igrave;';
        entities['205'] = '&Iacute;';
        entities['206'] = '&Icirc;';
        entities['207'] = '&Iuml;';
        entities['208'] = '&ETH;';
        entities['209'] = '&Ntilde;';
        entities['210'] = '&Ograve;';
        entities['211'] = '&Oacute;';
        entities['212'] = '&Ocirc;';
        entities['213'] = '&Otilde;';
        entities['214'] = '&Ouml;';
        entities['215'] = '&times;';
        entities['216'] = '&Oslash;';
        entities['217'] = '&Ugrave;';
        entities['218'] = '&Uacute;';
        entities['219'] = '&Ucirc;';
        entities['220'] = '&Uuml;';
        entities['221'] = '&Yacute;';
        entities['222'] = '&THORN;';
        entities['223'] = '&szlig;';
        entities['224'] = '&agrave;';
        entities['225'] = '&aacute;';
        entities['226'] = '&acirc;';
        entities['227'] = '&atilde;';
        entities['228'] = '&auml;';
        entities['229'] = '&aring;';
        entities['230'] = '&aelig;';
        entities['231'] = '&ccedil;';
        entities['232'] = '&egrave;';
        entities['233'] = '&eacute;';
        entities['234'] = '&ecirc;';
        entities['235'] = '&euml;';
        entities['236'] = '&igrave;';
        entities['237'] = '&iacute;';
        entities['238'] = '&icirc;';
        entities['239'] = '&iuml;';
        entities['240'] = '&eth;';
        entities['241'] = '&ntilde;';
        entities['242'] = '&ograve;';
        entities['243'] = '&oacute;';
        entities['244'] = '&ocirc;';
        entities['245'] = '&otilde;';
        entities['246'] = '&ouml;';
        entities['247'] = '&divide;';
        entities['248'] = '&oslash;';
        entities['249'] = '&ugrave;';
        entities['250'] = '&uacute;';
        entities['251'] = '&ucirc;';
        entities['252'] = '&uuml;';
        entities['253'] = '&yacute;';
        entities['254'] = '&thorn;';
        entities['255'] = '&yuml;';
    }

    if (useQuoteStyle !== 'ENT_NOQUOTES') {
        entities['34'] = '&quot;';
    }
    if (useQuoteStyle === 'ENT_QUOTES') {
        entities['39'] = '&#39;';
    }
    entities['60'] = '&lt;';
    entities['62'] = '&gt;';


    // ascii decimals to real symbols
    for (decimal in entities) {
        if (entities.hasOwnProperty(decimal)) {
            hash_map[String.fromCharCode(decimal)] = entities[decimal];
        }
    }

    return hash_map;
};

var html_entity_decode = function(string, quote_style) {

    var hash_map = {},
        symbol = '',
        tmp_str = '',
        entity = '';
    tmp_str = string.toString();

    if (false === (hash_map = this.get_html_translation_table('HTML_ENTITIES', quote_style))) {
        return false;
    }

    // fix &amp; problem
    // http://phpjs.org/functions/get_html_translation_table:416#comment_97660
    delete(hash_map['&']);
    hash_map['&'] = '&amp;';

    for (symbol in hash_map) {
        entity = hash_map[symbol];
        tmp_str = tmp_str.split(entity).join(symbol);
    }
    tmp_str = tmp_str.split('&#039;').join("'");

    return tmp_str;
};

var Cookies = require('./../vendor/js.cookie.js');//https://github.com/js-cookie/js-cookie
var Validation = require('./../js/ext/validation.js');
var moment = require('moment/min/moment-with-locales.min');

var _util = require('./../js/core/util/index.js');

class Core{
    constructor(config) {

        this.detect = md;
        this.Base64 = Base64;

        this.moment = moment;

        this.Store = require('./libs/store');
        // this.SyncStore = require('./libs/syncStore');

        this.Validation = Validation;
        this.Cookies = Cookies;

        this.events = new events.EventEmitter();

        this.html_entity_decode = html_entity_decode;
        this.get_html_translation_table = get_html_translation_table;

        this.yandexCounterID = null;


        if(config){
            this.yandexCounterID = config.yandexCounterID;
        }

        this.util = _util;
    }

    get(property, inStorage = false){

        if(inStorage){

            var data = sessionStorage.getItem(property);
            if(data)
                return JSON.parse(data);
            else
                return this[property];

        }else
            return this[property];
    }

    has(obj, name) {
        return name in obj;
    }

    set(property, value, inStorage = false){

        if(value){
            this[property] = value;

            if(inStorage){
                value = JSON.stringify(value);
                sessionStorage.setItem(property, value);
            }
        }
    }

    init(){

    }

    parseUrl(url){
        url = url ? url : window.location.href;
        return $url.parse(url, true);
    }

    GetUserCookie(){
        var C = Cookies.get('mst_user');
        if(C){
            try{
                return JSON.parse(Base64.decode(C));
            }catch(e){
                return {};
            }
        }else
            return {};
    }

    GetTplData(){
        var C = Cookies.get('mst_tpl');
        if(C){
            try{
                return JSON.parse(Base64.decode(C));
            }catch(e){
                return {};
            }
        }else
            return {};
    }

    GenerateHash(length = 32){
        var n,
            S = 'x',
            self = this;

        var hash = (s)=>{

            if (typeof(s) == 'number' && s === parseInt(s, 10))
                s = Array(s + 1).join('x');

            return s.replace(/x/g, ()=>{
                n = Math.round(Math.random() * 61) + 48;
                n = n > 57 ? (n + 7 > 90 ? n + 13 : n + 7) : n;
                return String.fromCharCode(n);
            });

        };

        for (let i = 0; i < length; i++)
            S = S + 'x';

        return hash(S);
    }

    getFormData(form){
        var rv = {}, obj, elements, element, index, names, nameIndex, name, value;

        elements = form.elements;

        if(elements){
            for (index = 0; index < elements.length; ++index) {
                element = elements[index];
                name = element.name;

                if (name) {

                    value = $(element).val();

                    if($(element).is('[type="checkbox"]'))
                        value = $(element).prop('checked');

                    names = name.split(".");
                    obj = rv;

                    for (nameIndex = 0; nameIndex < names.length; ++nameIndex) {
                        name = names[nameIndex];

                        if (nameIndex == names.length - 1)
                            obj[name] = value;
                        else
                            obj = obj[name] = obj[name] || {};
                    }
                }
            }
        }

        return rv;
    }

    param(json){
        return '?' +
            Object.keys(json).map(function(key) {
                return encodeURIComponent(key) + '=' +
                    encodeURIComponent(json[key]);
            }).join('&');
    }

    CountSymbols(str, count){
        return Math.round(count - str.length);
    }

    yandexEvent(nameEvent = null, CounterID = null){
        var id = CounterID ? CounterID : this.yandexCounterID;
        if(id && nameEvent){

        }
    }

    /**
     * Загрузка скриптов
     * @param src - ссылка на файл
     * @param async - ассинхронная загрузка
     * @param callback
     */
    scriptLoad(src, async = false, callback){
        var el = document.createElement("script");
        el.type = "text/javascript";
        el.src = src;

        if(async)
            el.async = async;

        document.head.appendChild(el);

        el.onload = ()=>{
            console.debug('script loaded');
            if(callback)
                callback(null, 'ok');
        };

        el.onerror = ()=>{
            if(callback)
                callback(new Error('Fail load script'), null);
        };
    }

    /**
     * Делаем первый символ заглавным
     * @param s - входная строка
     * @returns {string}
     */
    ucFirst(s) {
        return s.charAt(0).toUpperCase() + s.substr(1);
    }
}


var $Core = Core;
if (!(this instanceof Core))
    $Core = new Core;

$Core.init();
installWithVue($Core);

module.exports = $Core;

class API{
    getData(url, opts, callback) {
        resource = $Core.$resource(url);
        resource.get(opts, function (data, status, request) {
            callback(data, status, request);
        }).error(function (data, status, request) {
            callback(data, status, request);
        });
    }

    postResponse(url, opts, data, callback) {
        resource = $Core.$resource(url);
        resource.save(opts, data, function (data, status, request) {
            callback(data, status, request);
        }).error(function (data, status, request) {
            callback(data, status, request);
        });
    }

    putResponse(url, opts, data, callback) {
        resource = $Core.$resource(url);
        resource.update(opts, data, function (data, status, request) {
            callback(data, status, request);
        }).error(function (data, status, request) {
            callback(data, status, request);
        });
    }

    deleteResponse(url, opts, callback) {
        resource = $Core.$resource(url);
        resource.delete(opts, function (data, status, request) {
            callback(data, status, request);
        }).error(function (data, status, request) {
            callback(data, status, request);
        });
    }
}

module.exports.API = new API;

// ---------------------------------------------------------------------------------------------------------------------
let modules = ['email'];
for(let M of modules)
    module.exports[ $Core.ucFirst(M) ] = require(`./core/${M}`);

let extModules = ['time'];
for(let M of extModules)
    module.exports[ $Core.ucFirst(M) ] = require(`./../js/core/ext/${M}`);

// ---------------------------------------------------------------------------------------------------------------------


if(isProduction){

    // (function(x) { (function(f) { (function a() {
    //     try {
    //         var b = (i)=> {
    //             if (('' + (i / i)).length !== 1 || i % 20 === 0)
    //                 (()=> {}).constructor('debugger')();
    //             else
    //                 debugger ;
    //
    //             b(++i);
    //         };
    //         b(0);
    //     } catch (e) { f.setTimeout(a, x) }
    // })(); })(document.body.appendChild(document.createElement('frame')).contentWindow); })();

}


// -------------------------------------------
function installWithVue(Core){

    var _ = require('./../libs/vue/util');

    _.config = Vue.config;
    _.warning = Vue.util.warn;
    _.nextTick = Vue.util.nextTick;

    Vue.url = require('./../libs/vue/url');
    Vue.http = require('./../libs/vue/http');
    Vue.resource = require('./../libs/vue/resource');
    Vue.Promise = require('./../libs/vue/promise');

    var obj = {

        $url: {
            get: function () {
                return _.options(Vue.url, this, (Vue.$options && Vue.$options.url ? Vue.$options.url : ''));
            }
        },

        $http: {
            get: function () {
                return _.options(Vue.http, this, (Vue.$options && Vue.$options.http ? Vue.$options.http : {}));
            }
        },

        $resource: {
            get: function () {
                return Vue.resource.bind(this);
            }
        },

        $promise: {
            get: function () {
                return function (executor) {
                    return new Vue.Promise(executor, this);
                }.bind(this);
            }
        }
    };

    Core.util.nextTick = Vue.util.nextTick;

    Object.defineProperties(Core, obj);
}

function addProps(orig, ext) {
    Object.defineProperties(orig, ext);
}