"use strict";

class API{
    constructor(){
        var v = 'v1',
            module = 'auth';

        this.v = v;
        this.URL = `/api/${v}/${module}`;
        this.URL_users = `/api/${v}/users`;
    }

    singIn(user, callback){
        Core.$http.get(`${this.URL}/singIn?type=user`, (r)=> {},
            {headers: {'Authorization': 'Basic ' + btoa(user.login + ':' + user.password)}}
        ).then( (r)=>{ callback(r); }, (e)=>{ callback(e); });
    }

    logout(opts, callback) {
        Core.API.getData(`${this.URL}/logout`, opts, callback);
    }

    repairPass(data, callback) {
        Core.API.postResponse(`${this.URL_users}/repairPass`, {}, data, callback);
    }

    checkLogin(opts, callback) {
        Core.API.getData(`${this.URL_users}/checkLogin`, opts, callback);
    }

    checkEmail(opts, callback) {
        Core.API.getData(`${this.URL_users}/checkEmail`, opts, callback);
    }

    register(data, callback) {
        Core.API.postResponse(`${this.URL_users}/register`, {}, data, callback);
    }
}

API = new API;

module.exports = new Vue({
    el: '[rel="auth"]',
    data: {
        isLoading: false,
        isAuthLoading: false,
        isRestoreLoading: false,
        isRegisterLoading: false,

        isLogin: false,
        isLoginF: false,
        isLoginT: '',

        isEmail: false,
        isEmailF: false,
        isEmailT: '',

        isAuthE: false,
        isAuthT: '',

        isRestoreS: false,
        isRestoreE: false,
        isRestoreT: '',

        noPass: false,
        noPassT: '',

        isAgree: false,
        loginOrEmail: '',
        user: {
            login: '',
            password: ''
        },
        reg: {
            login: '',
            email: '',
            password: '',
            repass: '',
            iAgree: false
        },
        regRes: {
            isErr: false,
            text: ''
        }
    },

    ready(){

    },

    methods: {

        onAuth(user, callback){
            var self = this;

            user = user ? user : this.user;
            this.isAuthLoading = true;

            API.singIn(user, (r)=>{
                self.onAuthResponse(r, callback);
            });

        },

        onAuthResponse(r, callback){
            this.isAuthE = false;

            if(r.data && r.data.error){
                this.isAuthE = true;
                this.isAuthT = r.data.text;
            }

            if(r.data.result && r.data.result == 'ok')
                window.location.reload();

            this.isAuthLoading = false;

            callback(r.data);
        },

        onLogOut(e){

            API.logout({type: 'user'}, (data, status, request)=>{
                Core.Cookies.remove('mst_user');
                window.location.reload();
            });
            
            e.preventDefault();
        },

        onRepairPass(data, callback){
            var self = this;

            this.isRestoreS = this.isRestoreE = false;
            this.isRestoreT = '';

            this.isRestoreLoading = true;

            API.repairPass({d: (data ? data : this.loginOrEmail)}, (data, status, request)=>{
                self.isRestoreLoading = false;

                if(data.result && data.result.code){
                    switch(data.result.code){
                        case 'OK':
                            self.isRestoreS = true;
                            self.isRestoreT = data.result.text;
                            break;

                        case 'USER_NOT_FOUND':
                            self.isRestoreE = true;
                            self.isRestoreT = data.result.text;
                            break;
                    }

                    callback(data.result);
                }

            });

        },

        // проверка существующего логина
        onCheckLogin () {
            var self = this,
                val = this.reg.login;

            if(val.length>=3){
                
                API.checkLogin({check: val}, (data, status, request)=>{
                    if(status == 200 || data.statusCode == 200)
                        self.$set('isLogin', data.result);
                });

            }
        },

        // проверка существующего Email
        onCheckEmail(){
            var self = this,
                val = this.reg.email;

            if( Core.Validation.Email(val) ){

                API.checkEmail({check: val}, (data, status, request)=>{
                    if(status == 200)
                        self.$set('isEmail', data.result);
                });

            }
        },

        onRegister (e) {
            var self = this,
                isReg = [];
            this.noPass = this.isAgree = this.isEmailF = this.isLoginF = false;

            var U = this.checkRegData();
            if( U.isNext ){
                delete this.reg.repass;
                delete this.reg.iAgree;

                this.isRegisterLoading = true;
                
                API.register(U.data, (data, status, request)=>{
                    self.regResponse(data, status);
                    self.isRegisterLoading = false;
                });

            }

            e.preventDefault();
        },

        checkRegData(data){
            if(data)
                this.$set('reg', data);
            else
                data = this.reg;

            var isReg = [];

            if(data.login.length == 0){
                this.isLoginF = true;
                this.isLoginT = 'Не указан логин';
                isReg.push('');
            }

            if(data.login.length>0 && data.login.length <3){
                this.isLoginF = true;
                this.isLoginT = 'Логин слишком короткий';
                isReg.push('');
            }

            if(data.password.length == 0){
                this.noPass = true;
                this.noPassT = 'Не указан пароль';
                isReg.push('');
            }

            if(data.password.length > 0 && data.password.length <5){
                this.noPass = true;
                this.noPassT = 'Пароль слишком короткий';
                isReg.push('');
            }

            if(data.repass != this.reg.password){
                this.noPass = true;
                this.noPassT = 'Неверный проверочный проль';
                isReg.push('');
            }

            if(data.email.length == 0){
                this.isEmailF = true;
                this.isEmailT = 'Не указан Email';
                isReg.push('');
            }

            if(data.email.length>0 && !Core.Validation.Email(data.email)){
                this.isEmailF = true;
                this.isEmailT = 'Некорректный Email';
                isReg.push('');
            }

            if(!data.iAgree){
                this.isAgree = true;
                isReg.push('');
            }

            return {
                data: data,
                isNext: (isReg.length == 0)
            };
        },

        regResponse(data, status){
            this.regRes.isErr = false;
            this.regRes.text = '';

            if(data.statusCode && data.statusCode == 'ok') this.regRes.isErr = false;
            else this.regRes.isErr = true;

            if(data.code && !this.regRes.isErr){
                switch(data.code){
                    case 'IS_AUTH':
                        // автоматическая авторизация после регистрации
                        this.onAuth({
                            login: this.reg.login,
                            password: this.reg.password
                        });
                        break;

                    case 'EMAIL_ACTIVATE':
                    case 'ADMIN_ACTIVATE':
                        this.regRes.text = data.text;
                        break;
                }
            }else
                this.regRes.text = 'При регистрации произошла ошибка';
        }

    }
});