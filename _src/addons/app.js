/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: olegbogdanov86@gmail.com
 ------------------------------------------
 */

"use strict";

Vue.debug = (NODE_ENV == 'dev');

var A = new Vue({
    el: '',
    data: {

    },

    ready(){

    }
});

module.exports = {
    detect: new MobileDetect(navigator.userAgent)
};