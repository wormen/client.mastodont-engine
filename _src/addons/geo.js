"use strict";

class API{
    constructor(){
        var v = 'v1',
            module = 'geo';

        this.URL = `/api/${v}/${module}`;
    }

    districtInit(opts, callback) {
        Core.API.getData(`${this.URL}/districtInit`, opts, callback);
    }

    getRegionsList(opts, callback) {
        Core.API.getData(`${this.URL}/getRegionsList`, opts, callback);
    }

    getCityList(opts, callback) {
        Core.API.getData(`${this.URL}/getCityList`, opts, callback);
    }
}

API = new API;

var moduleData = {
    isLoading: false,
    districts: [], // список округов
    regions: [], // список регионов
    cityList: [], // список городов
    popularCity: [], // популярные города
    defaultDistrict: 4,
    showModal: false,
    isMoreCity: false,
    filterCity: '',
    isActive: {
        region: 0,
        city: 0
    },
    set: {
        country: 1,
        allCountry: false, // вся страна
        allRegionCity: false, // все города региона
        district_id: 0,
        region_id: 0,
        regionName: '',
        city_id: 0,
        cityName: ''
    },
    ad: {geo: { list: [] }}
};

// import store from './stores/geo'

var instances = [].map.call(document.querySelectorAll('[rel="geo"]'), function(el, $idx) {
    return new Vue({
        el: el,
        data(){ return moduleData; },
        ready(){
            var self = this;

            if($idx == 0)
                this.onLoad();


            this.$watch('set', (newVal, oldVal)=> {
                self.fixGeoData(true);
            });


            setInterval(()=>{
                try{
                    if(Account){
                        self.$set('ad.geo', Core.util.extend({}, Account.ad.geo));
                        self.isMoreCity = Account.isMoreCity;
                    }
                }catch(e){}
            }, 150);
        },

        events: {
            setData(key, value){
                this.$set(key, value);
            }
        },

        methods: {
            onLoad(){
                this.fullDistrictLoad();
            },

            getData(){
                var str = Core.Cookies.get('mst_geo');
                if(str)
                    str = JSON.parse(Core.Base64.decode(str));
                else
                    str = null;
                return str;
            },

            fixGeoData(isSave){
                var str = Core.Base64.encode(JSON.stringify(this.set));

                if(isSave)
                    Core.Cookies.set('mst_geo', str, {expires: 2});
            },

            fullDistrictLoad(did){
                var self = this;
                this.isLoading = true;

                did = did ? did : this.defaultDistrict;

                API.districtInit({district: did}, (data, status, request)=>{
                    self.isLoading = false;

                    if(status == 200 || data.statusCode == 200)
                        self.onChangeData(data.result);
                });

            },

            getDistrictRegions: function(district_id){ // регионы округа
                var self = this;
                this.isLoading = true;

                API.getRegionsList({did: district_id}, (data, status, request)=>{
                    self.isLoading = false;

                    if(status == 200 || data.statusCode == 200)
                        self.onChangeData(data.result);
                });

            },

            getRegionCity: function(region_id) { // города региона
                var self = this;
                this.isLoading = true;

                API.getCityList({rid: region_id}, (data, status, request)=>{
                    self.isLoading = false;

                    if(status == 200 || data.statusCode == 200)
                        self.onChangeData(data.result);
                });

            },

            onChangeData: function(data){
                if(data.districts){
                    for(var d in data.districts)
                        data.districts[d].isActive = d==0;

                    this.$set('districts', data.districts);
                }

                if(data.regions){
                    for(var r in data.regions)
                        data.regions[r].isActive = r==0;

                    this.$set('isActive.region', data.regions[0].id);
                    this.$set('regions', data.regions);
                }

                if(data.cityList){
                    for(var c in data.cityList)
                        data.cityList[c].isActive = c==0;

                    this.$set('isActive.city', data.cityList[0].id);
                    this.$set('cityList', data.cityList);
                }

                if(data.popularCity){
                    for(var p in data.popularCity)
                        data.popularCity[p].isActive = p==0;

                    this.$set('popularCity', data.popularCity);
                }

            },

            allCountrySet: function (e, cid) { // вся страна
                cid = cid ? cid : 1;
                this.$set('set.country', cid);
                this.$set('set.allCountry', true);
                this.$set('set.allRegionCity', false);
            },

            allCityRegion: function(e, region_id){ // все города региона
                region_id = region_id ? region_id : this.set.region_id;
                this.$set('set.region_id', region_id);
                this.$set('set.allCountry', false);
                this.$set('set.allRegionCity', true);
            },

            onActiveDistrict: function(e, idx, id){ // активнвый округ
                this.getDistrictRegions(id);
                // this.getDistrictCity(id);

                this.$set('set.district_id', id);

                for(var i in this.districts)
                    this.districts[i].isActive = i==idx;

                this.fixGeoData(true);
                e.preventDefault();
            },

            onActiveRegion: function(e, idx, id){ // активнвый регион
                this.getRegionCity(id);

                for(var i in this.regions){
                    this.regions[i].isActive = i==idx;
                    if(i==idx){
                        this.$set('set.region_id', id);
                        this.$set('set.district_id', this.regions[i].district_id);
                        this.$set('set.regionName', this.regions[i].title);
                    }
                }

                this.fixGeoData(true);
                e.preventDefault();
            },

            onActiveCity: function(e, idx, id, isReload){

                for(var i in this.cityList){
                    this.cityList[i].isActive = i==idx;
                    if(i==idx){
                        this.$set('set.city_id', id);
                        this.$set('set.region_id', this.cityList[i].region_id);
                        this.$set('set.district_id', this.cityList[i].district_id);
                        this.$set('set.cityName', this.cityList[i].title);

                        this.$set('isActive.city', this.cityList[i].id);
                    }
                }

                this.fixGeoData(isReload);

                if(isReload)
                    window.location.reload();

                e.preventDefault();
            },

            // поиск города по ID
            findCity: function(id){
                var item,
                    list = this.cityList;

                for(var i in list){
                    item = list[i];
                    if(item.id == id)
                        return Core.util.extend({}, item);
                }

            },

            setCity: function (data) {
                if (!moduleData.ad.geo.city)
                    this.ad.geo.city = moduleData.ad.geo.city = [];

                this.ad.geo.city = moduleData.ad.geo.city = data;
            }

        }
    });
});



module.exports = instances[0];

