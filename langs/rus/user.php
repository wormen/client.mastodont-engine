<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Локализация для шаблонов
 */

$url = parse_url($_SERVER['REQUEST_URI']);
if( explode('/', trim($url['path'], '/'))[0] == 'admin') return [];
else
    return [
        'btns'=>[
            'logIn'=>'Войти',
            'repair'=>'Восстановить',
            'repairPass'=>'Восстановить пароль',

            'y'=>'Да',
            "n"=>"Нет",
            'cancel'=>'Отмена',
            'save'=>'Сохранить',
            "add"=>"Добавить",
            "add_widget"=>'Добавить виджет',
            "fm"=>"Файловый менеджер",
            "login"=>"Войти",
            "addMenu"=>"Добавить меню",
            "search"=>"Найти",
            "cityUnknown"=>"Город не указан",
            "LogOut"=>"Выход",
            "all"=>"Все"
        ],
        'account'=>[
            'login'=>'Логин',
            'email'=>'Email',
            'password'=>'Пароль',
            'forgotPass'=>'Забыли пароль?',
            'repeat-password'=>'Повторите пароль',
            'activateState'=>[
                'OK'=>'Ваш профиль успешно активирован',
                'INACTIVE_LINK'=>'Ваша ссылка недействительна',
                'IS_ACTIVE_PROFILE'=>'Ваш профиль уже активирован',
            ],
            'activateText'=>[
                'OK'=>'Авторизация будет выполнена автоматически',
                'INACTIVE_LINK'=>'Ваш профиль был удален или время ссылки истекло',
                'IS_ACTIVE_PROFILE'=>'Возможно вы перешли по устаревшей ссылке',
            ],
        ],
        'geo'=>[
            'allCountryR'=>'Вся Россия',
        ],
    ];