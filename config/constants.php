<?php
/**
 * Copyright © Oleg Bogdanov
 * Developer: Oleg Bogdanov
 * Contacts: olegbogdanov86@gmail.com
 * -------------------------------------
 * Date: 19.01.2016
 * Time: 20:40
 */

session_start();

$DS = DIRECTORY_SEPARATOR;
define('RootDir', substr(str_replace(basename(__DIR__), '', __DIR__), 0, -1) ); // корневой каталог

define('ApiDir', implode($DS, [RootDir, 'api']));
define('AdminDir', implode($DS, [RootDir, 'admin']));
define('AdminTemplate', implode($DS, [AdminDir, 'template']));

define('EngineDir', implode($DS, [RootDir, 'engine']));
define('ModuleDir', implode($DS, [RootDir, 'modules']));
define('SdkDir', implode($DS, [EngineDir, 'mastodont-sdk-php']));

define('LangDir', implode($DS, [RootDir, 'langs']));
define('CoreDir', implode($DS, [EngineDir, 'core']));
define('DataDir', implode($DS, [RootDir, 'data']));

define('CacheDir', implode($DS, [RootDir, '.cache'])); // кэш сайта
define('ConfigDir', implode($DS, [RootDir, 'config'])); // каталог с конфигами

//define('StaticDir', implode($DS, [RootDir, 'static']));
define('UploadsDir', implode($DS, [RootDir, 'uploads']));
define('UploadsHTTP', '/uploads/'); // начальный URL для статических файлов

define('TemplateDir', implode($DS, [RootDir, 'template'])); // шаблон сайта

define('BackupDir', implode($DS, [RootDir, 'backup']));
define('TmpDir', implode($DS, [RootDir, '.tmp']));

define('ImportDir', implode($DS, [RootDir, 'import']));

define('CliDir', implode($DS, [RootDir, 'cli'])); // cli mode deamons
define('PidDir', implode($DS, [RootDir, 'cli', 'pids']));
define('ServiceDir', implode($DS, [RootDir, 'cli', 'service']));
define('CliStateDir', implode($DS, [RootDir, 'cli', 'service'])); // файлы-маркеры демонов для ( run status | restart status )

define('ToolDir', implode($DS, [RootDir, 'tools']));

// ---------------------------------------------------------------------------------------------------------------------
$GLOBALS['title'] = $title = '';
$mеta = []; // массив мета тегов

$locale = (isset($_REQUEST['locale']) ? $_REQUEST['locale'] : 'rus');  // локализация
$lang = []; // текущая локализация

$js = []; // глобальынй массив для скриптов
$css = []; // глобалный массив для стилей

$GLOBALS['domain_id'] = null;
$GLOBALS['T'] = [
    'statusCode'=>200,
    'url'=>parse_url($_SERVER['REQUEST_URI']), // текущий URL
    'baseUrl'=>'//'.$_SERVER['HTTP_HOST'],
    'locale'=>$locale,  // локализация
    'is404'=>false,
    'is500'=>false,
    'vars'=>[], // настройки системы
    'module'=>[ // данные текущего модуля
        'name'=>null,
        'section'=>null,
        'item'=>null,
        'url'=>null
    ],
    'auth'=>false,
    'noLK'=>false,
    'user'=>[], // данные пользователя
    'permisions'=>[], // права пользователя
    'geo'=>[], // геоданные ползователя
    'content'=>[], // контент текущей страницы
    'tplPath'=>'',
    'tplPage'=>'', // название шаблона страницы
    'pageContent'=>'', // путь к файлу шаблона страницы
];

// ---------------------------------------------------------------------------------------------------------------------
// создаем каталоги, если их нет

$dirArr = [TmpDir, DataDir, CacheDir, BackupDir, ImportDir, ToolDir];

foreach ($dirArr as $dir)
    if(!file_exists($dir))
        mkdir($dir, 0755, true);

// ---------------------------------------------------------------------------------------------------------------------

define('CookieDays', 2);

$st = time() + CookieDays * 86400;
ini_set('session.auto_start', 1);
ini_set('session.cookie_lifetime', $st);
ini_set('session.gc_maxlifetime', $st);

//ini_set('session.cookie_domain', $_SERVER['HTTP_HOST']);
//ini_set('session.cookie_secure', 1);
//ini_set('session.cookie_httponly', 1);

// ---------------------------------------------------------------------------------------------------------------------


/**
 *  Подключение основных библиотек
 */
require implode($DS, [RootDir, 'vendor', 'autoload.php']);
require implode($DS, [EngineDir, 'autoload.php']);
require implode($DS, [SdkDir, 'autoload.php']);

// ---------------------------------------------------------------------------------------------------------------------
if(isset($runCli) && $runCli == true){
    $Cli = new Cli();
    $Cli->init(); // инициализация cli демона-менеджера
}

// ---------------------------------------------------------------------------------------------------------------------
$Core = new \Core\Index();
$Tpl = new \Core\Tpl();
$Log = new \Core\Log();
$Setting = new \Core\Setting();
$Security = new \Core\Security();

$GLOBALS['domain'] = $Setting->getDomainDATA();
$ev->emit('set:lang');